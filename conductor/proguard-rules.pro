-dontwarn com.opencsv.**
-dontwarn org.apache.commons.beanutils.**
-dontwarn org.apache.commons.collections.**
-dontwarn butterknife.internal.**
-dontwarn com.squareup.okhttp.**
-dontwarn org.osmdroid.**
-dontwarn com.facebook.android.**
-dontwarn com.google.android.gms.**
-dontwarn com.github.siyamed.**
-dontwarn com.github.nkzawa.**
-dontwarn com.squareup.retrofit2.**
-dontwarn com.michaelpardo.**
-dontwarn com.github.orangegangsters.**
-dontwarn com.jakewharton.**
-dontwarn com.nostra13.universalimageloader.**
-dontwarn rx.internal.util.**
-dontwarn com.google.common.**
-dontwarn okio.**
-dontwarn android.support.v4.app.**
-dontwarn android.support.v7.**
-dontwarn org.apache.http.**
-dontwarn android.net.http.AndroidHttpClient
-dontwarn retrofit2.**
-dontwarn sun.misc.Unsafe
-dontwarn com.octo.android.robospice.retrofit.RetrofitJackson**
-dontwarn retrofit.appengine.UrlFetchClient
-dontwarn android.support.**
-dontwarn java.lang.**
-dontwarn org.codehaus.**
-dontwarn com.google.**
-dontwarn com.caverock.androidsvg.*
-dontwarn org.osmdroid.*
-dontwarn okhttp3.**
-dontwarn okio.**
-dontwarn javax.annotation.Nullable
-dontwarn javax.annotation.ParametersAreNonnullByDefault
-dontoptimize

-keep class okhttp3.** { *; }
-keep class org.apache.commons.** { *; }
-keep class com.google.gson.** { *; }
-keep class com.google.inject.** { *; }
-keep class org.apache.http.** { *; }
-keep class org.apache.james.mime4j.** { *; }
-keep class javax.inject.** { *; }
-keep class retrofit2.** { *; }
-keep class com.activeandroid.** { *; }
-keep class com.activeandroid.*.* { *; }
-keep class * extends com.activeandroid.Model
-keep class * extends com.activeandroid.serializer.TypeSerializer
-keep class info.hoang8f.**{*;}
-keep class com.jakewharton.** {*;}
-keep class io.socket.**{*;}
-keep class com.nostra13.universalimageloader.**{*;}
-keep class com.google.android.gms.**{*;}
-keep class com.google.**
-keep class org.osmdroid.**
-keep class com.kradac.ktaxy_driver.modelo.** { *; }
-keep class com.facebook.android.*

-keep class com.facebook.FacebookSdk {
   boolean isInitialized();
}
-keep class com.facebook.appevents.AppEventsLogger {
   com.facebook.appevents.AppEventsLogger newLogger(android.content.Context);
   void logSdkEvent(java.lang.String, java.lang.Double, android.os.Bundle);
}

-keepattributes Signature
-keepattributes Exceptions
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}
-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}
-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}

-keepattributes Exceptions,InnerClasses,Signature,Deprecated, SourceFile,LineNumberTable,*Annotation*,EnclosingMethod
