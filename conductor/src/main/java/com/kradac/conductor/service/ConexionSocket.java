package com.kradac.conductor.service;

import java.net.URISyntaxException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import io.socket.client.IO;
import io.socket.client.Socket;
import okhttp3.OkHttpClient;

public class ConexionSocket {

    public Socket inciarConexion(String IP_SERVIDOR) {
        Socket mSocket = null;
        SSLContext sc = new SslConfig().getSSLContext();
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                })
                .sslSocketFactory(sc.getSocketFactory())
                .build();
        IO.setDefaultOkHttpWebSocketFactory(okHttpClient);
        IO.setDefaultOkHttpCallFactory(okHttpClient);
        IO.Options opts = new IO.Options();
        opts.callFactory = okHttpClient;
        opts.webSocketFactory = okHttpClient;
        opts.secure = true;
        opts.forceNew = true;
        try {
            mSocket = IO.socket(IP_SERVIDOR, opts);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        mSocket.io().reconnection(true);
        mSocket.connect();
        return mSocket;
    }
}
