package com.kradac.conductor.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.modelo.DatosTaximetro;
import com.kradac.conductor.modelo.DatosTaximetroServicioActivo;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TaximetroService extends Service {
    private static final double VELOCIDAD_LIMITE = 120;
    private static final int TIEMPO_ACTUALIZA = 1;
    private static final String TAG = TaximetroService.class.getName();
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> taximetroHandle;
    private Location locationInicial;
    private boolean isInicial = true;
    private int horas, minutos, segundos, totalSegundos, horaEspera, minutoEspera, segundoEspera, tipo, editarCostoC;
    private double distanciaRecorrida, kilometroRec, costoValorDistancia, costoMinEspera,
            costoValorTiempo, costoArranque, subTotal, velocidad, distancia, costoMinimo;
    private String horaInicio;
    private final IBinder mBinder = new LocalBinder();
    private ServicioSockets servicio;
    private boolean mBound;
    private Utilidades utilidades;
    /**
     * Conexion con el servicio
     */
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            ServicioSockets.LocalBinder binder = (ServicioSockets.LocalBinder) service;
            servicio = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            mBound = false;
        }
    };

    public class LocalBinder extends Binder {
        public TaximetroService getService() {
            return TaximetroService.this;
        }
    }

    public TaximetroService() {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Intent intent = new Intent(TaximetroService.this, ServicioSockets.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        utilidades = new Utilidades();
        cargarPreferecias();
        inciarTaximetro();
    }

    /***
     * Sirve para actualizar la localización este se llama cada segundo.
     * @param location
     */
    public void onLocationChanged(Location location) {
        if (location != null) {
            if (location.getAccuracy() <= 40) {
                if (isInicial) {
                    locationInicial = location;
                    isInicial = false;
                } else {
                    if (location.getLatitude() != 0 && location.getLongitude() != 0) {
                        distancia = utilidades.getDistance(locationInicial.getLatitude(),
                                locationInicial.getLongitude(), location.getLatitude(), location.getLongitude());
                        if (location.hasSpeed()) {
                            velocidad = (location.getSpeed() * 3.6) > VELOCIDAD_LIMITE ? VELOCIDAD_LIMITE : location.getSpeed() * 3.6;
                        } else {
                            velocidad = retornarVelocidad(locationInicial.getSpeed() * 3.6);
                        }
                        locationInicial = location;
                    }
                }
            } else {
                if (location.hasSpeed()) {
                    velocidad = (location.getSpeed() * 3.6) > VELOCIDAD_LIMITE ? VELOCIDAD_LIMITE : location.getSpeed() * 3.6;
                } else {
                    velocidad = retornarVelocidad(locationInicial.getSpeed() * 3.6);
                }
                distancia = 0;
            }
        }
    }

    /**
     * Metodo que a que la velocidad no sea mayor al limite permitido
     *
     * @param velocidad
     * @return
     */
    private double retornarVelocidad(double velocidad) {
        if (velocidad > VELOCIDAD_LIMITE) {
            return 90;
        } else if (velocidad > 90) {
            return 70;
        } else if (velocidad > 70) {
            return 50;
        } else if (velocidad > 50) {
            return 30;
        } else if (velocidad > 30) {
            return 20;
        } else if (velocidad > 20) {
            return 7;
        }
        return 0;
    }

    /**
     * Este metodo inicia el taximetro
     */
    public void inciarTaximetro() {
        horaInicio = utilidades.obtenerHora();
        taximetroHandle = scheduler.scheduleAtFixedRate(tareaEjecutarTaximetro(),
                TIEMPO_ACTUALIZA, TIEMPO_ACTUALIZA, TimeUnit.SECONDS);
    }


    private static double VELOCIDAD_MINIMA = 8;

    /**
     * Tarea que se encarga de actulizar los valores y la notifica a la vista estos cambios.
     * * @return
     */
    public Runnable tareaEjecutarTaximetro() {
        return new Runnable() {
            @Override
            public void run() {
                onLocationChanged(servicio.getLocationService());
                cronomretroTiempoTotal();

                if (tipo == 1) {
                    if (velocidad >= VELOCIDAD_MINIMA) {
                        distanciaRecorrida = distanciaRecorrida + distancia;
                        costoValorDistancia = (distanciaRecorrida * kilometroRec) / 1000;
                    } else {
                        cronometroTiempoEspera();
                        totalSegundos++;
                        costoValorTiempo = (totalSegundos * costoMinEspera) / 60;
                    }
                } else if (tipo == 2) {
                    //if (velocidad >= VELOCIDAD_MINIMA) {
                    distanciaRecorrida = distanciaRecorrida + distancia;

                    costoValorDistancia = (distanciaRecorrida * kilometroRec) / 1000;
                    cronometroTiempoEspera();
                    totalSegundos++;
                    costoValorTiempo = (totalSegundos * costoMinEspera) / 60;
                    //}
                }

                subTotal = costoArranque + costoValorTiempo + costoValorDistancia;
                enviarData();

            }
        };
    }

    /**
     * Metodo para enviar llos datos a la vista
     */
    private void enviarData() {
        String tiempoEspera = cronometroString(horaEspera, minutoEspera, segundoEspera);
        String valorTiempo = utilidades.dosDecimales(TaximetroService.this, costoValorTiempo);
        String velocidadAuto = utilidades.dosDecimales(TaximetroService.this, velocidad);
        String distanciaRec = String.valueOf((int) distanciaRecorrida);
        String valorPrincipal = utilidades.dosDecimales(TaximetroService.this, subTotal);
        String valDistancia = utilidades.dosDecimales(TaximetroService.this, costoValorDistancia);
        String cronometro = cronometroString(horas, minutos, segundos);
        String costoArranqueT = utilidades.dosDecimales(TaximetroService.this, costoArranque);
        String totalCarreraS = (subTotal > costoMinimo) ?
                utilidades.dosDecimales(TaximetroService.this, subTotal) :
                utilidades.dosDecimales(TaximetroService.this, costoMinimo);
        if (mBound) {
            servicio.activarTaximetro(editarCostoC, tipo, tiempoEspera, valorTiempo, velocidadAuto, distanciaRec, valorPrincipal, valDistancia, cronometro, horaInicio, costoArranqueT, totalCarreraS);
        }
    }


    /**
     * Combierte en una formato de hora que se pueda visualizar bien
     *
     * @param horas
     * @param minutos
     * @param segundos
     * @return
     */
    public String cronometroString(int horas, int minutos, int segundos) {
        String hC = (horas < 10) ? "0".concat(String.valueOf(horas)) : String.valueOf(horas);
        String mC = (minutos < 10) ? "0".concat(String.valueOf(minutos)) : String.valueOf(minutos);
        String sC = (segundos < 10) ? "0".concat(String.valueOf(segundos)) : String.valueOf(segundos);
        return hC.concat(":").concat(mC).concat(":").concat(sC);
    }

    /**
     * Cronometro que setea los valores del cornometro con el tiempo total de arranque
     */
    public void cronomretroTiempoTotal() {
        segundos++;
        if (segundos == 60) {
            minutos++;
            segundos = 0;
        }
        if (minutos == 60) {
            horas++;
            minutos = 0;
        }
    }

    /**
     * Sirve como cronometro para cuando corre los valores por la espera.
     */
    public void cronometroTiempoEspera() {
        segundoEspera++;
        if (segundoEspera == 60) {
            minutoEspera++;
            segundoEspera = 0;
        }
        if (minutoEspera == 60) {
            horaEspera++;
            minutoEspera = 0;
        }
    }

    /**
     * Carga las preferencias del taximetro.
     */
    private void cargarPreferecias() {
        DatosTaximetroServicioActivo datosTaximetroServicioActivo = DatosTaximetroServicioActivo.obtenerDatosTaximetro(this);
        if (datosTaximetroServicioActivo != null && datosTaximetroServicioActivo.getR() != null) {
            VELOCIDAD_MINIMA = datosTaximetroServicioActivo.getR().getB();
            if (utilidades.compararHorasFin(datosTaximetroServicioActivo.getR().gethN(),
                    datosTaximetroServicioActivo.getR().gethD())) {
                seterarDatos(datosTaximetroServicioActivo.getR().getaD(),
                        datosTaximetroServicioActivo.getR().getKmD(),
                        datosTaximetroServicioActivo.getR().getMinD(),
                        datosTaximetroServicioActivo.getR().getcD(),
                        datosTaximetroServicioActivo.getR().getTipo(),
                        datosTaximetroServicioActivo.getR().getEditarCostoC());

            } else {
                seterarDatos(datosTaximetroServicioActivo.getR().getaN(),
                        datosTaximetroServicioActivo.getR().getKmN(),
                        datosTaximetroServicioActivo.getR().getMinN(),
                        datosTaximetroServicioActivo.getR().getcN(),
                        datosTaximetroServicioActivo.getR().getTipo(),
                        datosTaximetroServicioActivo.getR().getEditarCostoC());
            }
        } else {
            DatosTaximetro datosTaximetro = DatosTaximetro.obtenerDatosTaximetro(this);
            if (datosTaximetro != null && datosTaximetro.getR() != null) {
                VELOCIDAD_MINIMA = datosTaximetro.getR().getB();
                if (utilidades.compararHorasFin(datosTaximetro.getR().gethN(), datosTaximetro.getR().gethD())) {
                    seterarDatos(datosTaximetro.getR().getaD(),
                            datosTaximetro.getR().getKmD(),
                            datosTaximetro.getR().getMinD(),
                            datosTaximetro.getR().getcD(),
                            datosTaximetro.getR().getTipo(),
                            datosTaximetro.getR().getEditarCostoC());
                } else {
                    seterarDatos(datosTaximetro.getR().getaN(),
                            datosTaximetro.getR().getKmN(),
                            datosTaximetro.getR().getMinN(),
                            datosTaximetro.getR().getcN(),
                            datosTaximetro.getR().getTipo(),
                            datosTaximetro.getR().getEditarCostoC());

                }
            }
        }

    }

    /**
     * Plasma los valores que seran usados en el algoritmo de taximetro.
     *
     * @param costoArranque
     * @param kilometroRec
     * @param costoMinEspera
     * @param costoMinimo
     */
    public void seterarDatos(double costoArranque, double kilometroRec,
                             double costoMinEspera, double costoMinimo, int tipo, int editarCostoC) {
        this.costoArranque = costoArranque;
        this.kilometroRec = kilometroRec;
        this.costoMinEspera = costoMinEspera;
        this.costoMinimo = costoMinimo;
        this.tipo = tipo;
        this.editarCostoC = editarCostoC;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        taximetroHandle.cancel(true);
        unbindService(serviceConnection);
        stopSelf();
    }

}
