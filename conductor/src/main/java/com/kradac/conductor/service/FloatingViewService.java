package com.kradac.conductor.service;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.kradac.conductor.R;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.vista.ListaSolicitudesActivity;
import com.kradac.conductor.vista.MapaBaseActivity;


public class FloatingViewService extends Service {
    private static final String TAG = FloatingViewService.class.getName();
    private WindowManager mWindowManager;
    private View mImgFloatingView;
    private boolean isMapa = true;
    private long touchStartTime = 0;
    private long lastTouchTime = 0;
    private Point szWindow = new Point();
    @Override
    public IBinder onBind(Intent intent) {
        //Not use this method


        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                isMapa = bundle.getBoolean("isMapa");
            }
        }

//        if (!mIsFloatingViewAttached) {
//            mWindowManager.addView(mImgFloatingView, mImgFloatingView.getLayoutParams());
//        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mImgFloatingView = LayoutInflater.from(this).inflate(R.layout.layout_floating_widget, null);
        int LAYOUT_FLAG;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = obtenerUltimaPosicion()[0];
        params.y = obtenerUltimaPosicion()[1];
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert mWindowManager != null;
        mWindowManager.addView(mImgFloatingView, params);

        ImageView viewClose = mImgFloatingView.findViewById(R.id.imv_cerrar);
        viewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Utilidades().hideFloatingView(FloatingViewService.this);
            }
        });

        getWindowManagerDefaultDisplay();
        mImgFloatingView.findViewById(R.id.root_container).setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;


            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int x_cord = (int) event.getRawX();
                int y_cord = (int) event.getRawY();
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        touchStartTime = System.currentTimeMillis();
                        //remember the initial position.
                        initialX = params.x;
                        initialY = params.y;

                        //get the touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        lastTouchTime = System.currentTimeMillis();
                        if (lastTouchTime - touchStartTime < 250) {
                            //Open the application  click.
                            Intent intent = new Intent(FloatingViewService.this, MapaBaseActivity.class);
                            if (!isMapa) {
                                intent = new Intent(FloatingViewService.this, ListaSolicitudesActivity.class);
                            }
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                            //close the service and remove view from the view hierarchy
                            stopSelf();
                        } else {
                            resetPosition(x_cord);
                        }
                        return true;
                    case MotionEvent.ACTION_MOVE:

                        lastTouchTime = System.currentTimeMillis();
                        x_cord = initialX + (int) (event.getRawX() - initialTouchX);
                        //Calculate the X and Y coordinates of the view.
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);

                        //Update the layout with new X & Y coordinate
                        mWindowManager.updateViewLayout(mImgFloatingView, params);

                        return true;
                }

                return false;
            }
        });
    }

    /**
     * Sirve para majenar el inicio
     */
    private void getWindowManagerDefaultDisplay() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
            mWindowManager.getDefaultDisplay().getSize(szWindow);
        else {
            int w = mWindowManager.getDefaultDisplay().getWidth();
            int h = mWindowManager.getDefaultDisplay().getHeight();
            szWindow.set(w, h);
        }
    }



    /*  Reset position of Floating Widget view on dragging  */
    private void resetPosition(int x_cord_now) {
        Log.e(TAG, "resetPosition: ");
        if (x_cord_now <= szWindow.x / 2) {
            moveToLeft(x_cord_now);
            Log.e(TAG, "resetPosition: 1");
        } else {
            moveToRight(x_cord_now);
            Log.e(TAG, "resetPosition: 2");
        }
    }

    /*  Method to move the Floating widget view to Left  */
    private void moveToLeft(final int current_x_cord) {
        final int x = szWindow.x - current_x_cord;

        new CountDownTimer(500, 5) {
            //get params of Floating Widget view
            WindowManager.LayoutParams mParams = (WindowManager.LayoutParams) mImgFloatingView.getLayoutParams();

            public void onTick(long t) {
                long step = (500 - t) / 5;

                mParams.x = 0 - (int) (current_x_cord * current_x_cord * step);

                //If you want bounce effect uncomment below line and comment above line
                // mParams.x = 0 - (int) (double) bounceValue(step, x);


                //Update window manager for Floating Widget
                if (mWindowManager != null)
                    mWindowManager.updateViewLayout(mImgFloatingView, mParams);
            }

            public void onFinish() {
                mParams.x = 0;
                //Update window manager for Floating Widget
                mWindowManager.updateViewLayout(mImgFloatingView, mParams);
                guardarUltimasPosiciones(mParams.x, mParams.y);
            }
        }.start();
    }

    /*  Method to move the Floating widget view to Right  */
    private void moveToRight(final int current_x_cord) {

        new CountDownTimer(500, 5) {
            //get params of Floating Widget view
            WindowManager.LayoutParams mParams = (WindowManager.LayoutParams) mImgFloatingView.getLayoutParams();

            public void onTick(long t) {
                long step = (500 - t) / 5;

                mParams.x = (int) (szWindow.x + (current_x_cord * current_x_cord * step) - mImgFloatingView.getWidth());

                //If you want bounce effect uncomment below line and comment above line
                //  mParams.x = szWindow.x + (int) (double) bounceValue(step, x_cord_now) - mFloatingWidgetView.getWidth();

                //Update window manager for Floating Widget
                if (mWindowManager != null)
                    mWindowManager.updateViewLayout(mImgFloatingView, mParams);
            }

            public void onFinish() {
                mParams.x = szWindow.x - mImgFloatingView.getWidth();

                //Update window manager for Floating Widget
                mWindowManager.updateViewLayout(mImgFloatingView, mParams);
                guardarUltimasPosiciones(mParams.x, mParams.y);
            }
        }.start();
    }

    public void guardarUltimasPosiciones(int... data) {
        SharedPreferences spUltimaPosicion = getSharedPreferences("ultima_posicion", MODE_PRIVATE);
        SharedPreferences.Editor editor = spUltimaPosicion.edit();
        editor.putInt("x", data[0]);
        editor.putInt("y", data[1]);
        editor.apply();
    }

    public Integer[] obtenerUltimaPosicion() {
        Integer[] a = new Integer[2];
        SharedPreferences spUltimaPosicion = getSharedPreferences("ultima_posicion", MODE_PRIVATE);
        a[0] = spUltimaPosicion.getInt("x", 0);
        a[1] = spUltimaPosicion.getInt("y", 100);
        return a;
    }

    public void removeView() {
        if (mImgFloatingView != null) {
            mWindowManager.removeView(mImgFloatingView);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        removeView();
    }
}
