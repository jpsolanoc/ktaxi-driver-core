package com.kradac.conductor.service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.kradac.conductor.R;
import com.kradac.conductor.extras.ExtraLog;
import com.kradac.conductor.extras.Gps;
import com.kradac.conductor.extras.LatLng;
import com.kradac.conductor.extras.MetodosValidacion;
import com.kradac.conductor.extras.ReproducirTextAudio;
import com.kradac.conductor.extras.UrlKtaxiConductor;
import com.kradac.conductor.extras.UtilStream;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComuniacionTaximetro;
import com.kradac.conductor.interfaces.OnComunicacionAgregarTarjeta;
import com.kradac.conductor.interfaces.OnComunicacionCancelar;
import com.kradac.conductor.interfaces.OnComunicacionCompraServicios;
import com.kradac.conductor.interfaces.OnComunicacionCompraServiciosMapa;
import com.kradac.conductor.interfaces.OnComunicacionIniciarSesion;
import com.kradac.conductor.interfaces.OnComunicacionMapa;
import com.kradac.conductor.interfaces.OnComunicacionRecargas;
import com.kradac.conductor.interfaces.OnComunicacionSaldoKtaxiPagerActity;
import com.kradac.conductor.interfaces.OnComunicacionServicio;
import com.kradac.conductor.interfaces.OnComunicacionSocketLista;
import com.kradac.conductor.interfaces.OnComunicacionSocketMensajeria;
import com.kradac.conductor.interfaces.onComunicaionMensajesBroadCast;
import com.kradac.conductor.modelo.CalendarHistorial;
import com.kradac.conductor.modelo.ChatMessage;
import com.kradac.conductor.modelo.ConfiguracionServidor;
import com.kradac.conductor.modelo.CorazonMalvado;
import com.kradac.conductor.modelo.DatosSolicitud;
import com.kradac.conductor.modelo.Destino;
import com.kradac.conductor.modelo.ItemHistorialCompras;
import com.kradac.conductor.modelo.ItemHistorialSolicitud;
import com.kradac.conductor.modelo.MensajeDeuda;
import com.kradac.conductor.modelo.PosicionNueva;
import com.kradac.conductor.modelo.RespuestaPosibleSolicitudes;
import com.kradac.conductor.modelo.Solicitud;
import com.kradac.conductor.modelo.VideosPropaganda;
import com.kradac.conductor.notificaciones.SolicitudTemporal;
import com.kradac.conductor.presentador.MainPresentador;
import com.kradac.conductor.vista.ChatActivity;
import com.kradac.conductor.vista.HistorialActivity;
import com.kradac.conductor.vista.HistorialComprasActivity;
import com.kradac.conductor.vista.ListaSolicitudesActivity;
import com.kradac.conductor.vista.MapaBaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Creado por Ing. John Solano
 */

public class ServicioSockets extends Service implements Gps.LocationListener, OnComunicacionServicio, SensorEventListener {
    private static final String TAG = ServicioSockets.class.getName();
    private final int NOTIFICATION_ID = 101;
    public static boolean isRunService = false;
    private String IP_SERVIDOR;
    private IBinder mBinder = new LocalBinder();
    private int idSolicitudSeleccionada = 0, idPedidoSeleccionado = 0, tipoFormaPago = 0, temperaturaBateria;
    public Solicitud solicitudSeleccionada;
    public boolean isCronometroTiempo = false, tiempoEnviado = false, precioEnviado = false;
    public int estadoBoton = 0, tiempoEnvio, estadoSolicitud = 0, razonCancelada = 0,
            estadoPedido = 0;
    public boolean isMensajeAbordo = false, isEstadoBoton = true // El estado en el cual se encuantra el boton libre u ocupado
            , isEstadoAbordo = false,
            isComprando = false, isComprado = false, isMensajeNuevo = false;//Variable de solicitud reiniciar esta variable
    protected OnComunicacionMapa actividadMapa;
    protected OnComunicacionSaldoKtaxiPagerActity actividadSaldoKtaxiPager;
    protected OnComunicacionSocketLista actividadLista;
    protected OnComunicacionCompraServicios compraServicios;
    protected OnComunicacionCompraServiciosMapa compraServiciosMapa;
    protected OnComunicacionSocketMensajeria actividadMensajes;
    protected OnComunicacionCancelar actividadCancelar;
    protected onComunicaionMensajesBroadCast actividadMensajeriaBroadCast;
    protected OnComunicacionIniciarSesion actividadIniciarSesion;
    protected OnComuniacionTaximetro actividadTaximetro;
    protected OnComunicacionRecargas actividadRecargas;
    protected OnComunicacionAgregarTarjeta onComunicacionAgregarTarjeta;
    protected Socket mSocket;
    public ArrayList<Solicitud> solicitudesEntrantes;
    private SoundPool soundPool;
    private int flujodemusica, idMensaje = 0, flujoMensaje, flujocancelar, minuto, segundos, flujoPosiblesSolicitudes;
    protected SharedPreferences spobtener, spSolicitud, spParametrosConfiguracion, configuracionApp, spEstadobtn;
    protected Utilidades utilidades;
    public ArrayList<ChatMessage> chatHistory, chatCallCenter, chatBroadCastEmpresa, chatBroadCast;
    private boolean isServerOnline, isBanderaLlamada = true, isClienteVoucherEstado, isMensajeDeudaAbordo, isMensajeDeudaFinCarrera;
    public static boolean isBanderaRun = false;
    private double latitud = 0, longitud = 0;
    String datosLogeo, fechaHoraConexion;
    private String idAndroid;
    private boolean enLineaRastreo, isClienteAceptoTiempo = false, isEnviarComprarPedido = false,
            isEnviarllevandoPedido = false, isConnect = false;
    private ArrayList<PosicionNueva> tempPosiciones = new ArrayList<>();
    private PowerManager powerManager;
    private PowerManager.WakeLock wakeLock;
    private KeyguardManager keyguardManager;
    private Set<Solicitud> jSonSolicitud = new HashSet();
    public ReproducirTextAudio reproducirTextAudio;
    private Status status;
    private Gps gps;
    private GpsLocationReceiver wfs;

    private Emitter.Listener onNuevaSolicitud = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            Log.e(TAG, "call: NUEVA SOLICITUD" + args[0].toString());
            guardarEstadoEnvio(false);
            if (solicitudesEntrantes.size() < 15 && solicitudSeleccionada == null) {
                if (spEstadobtn.getBoolean(VariablesGlobales.ESTADO_BTN, true)) {
                    if (data.has(VariablesGlobales.ID_SOLICITUD) || data.has(VariablesGlobales.ID_PEDIDO)) {
                        if (isEstadoBoton) {
                            if (!data.has(VariablesGlobales.ID_PEDIDO)) {
                                try {
                                    enviarEvento(location, 3, data.getInt(VariablesGlobales.ID_SOLICITUD), 0);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                parcearSolicitud(data);
                            } else {
                                try {
                                    enviarEvento(location, 4, data.getInt(VariablesGlobales.ID_PEDIDO), 0);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                parcearPedido(data);
                            }
                        }
                    }
                }
            }
        }
    };


    public void prenderPantalla() {
        Intent intent = new Intent(GpsLocationReceiver.PRENDER_PANTALLA);
        sendBroadcast(intent);
    }


    private Emitter.Listener onDesconectar = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (mSocket != null) {
                if (!mSocket.connected()) {
                    isServerOnline = mSocket.connected();
                    notificarCambioEnServidor();
                    solicitudesEntrantes.clear();
                    retomarCarreras();
                }
                isConnect = false;
            }

        }
    };

    public void apagarEscuchaPosibleSolicitud() {
        mSocket.off(VariablesGlobales.ESCUCHA_EN_POSIBLES_SOLICITUDES, onPosibleSolicitud);
    }

    public void prenderEscuchaPosibleSolicitud() {
        if (mSocket != null) {
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_POSIBLES_SOLICITUDES)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_POSIBLES_SOLICITUDES, onPosibleSolicitud);
            }
        }
    }

    private Emitter.Listener onPosibleSolicitud = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            RespuestaPosibleSolicitudes respuestaPosibleSolicitudes = RespuestaPosibleSolicitudes.createPosibleSolicitudes(args[0].toString());
            if (respuestaPosibleSolicitudes != null) {
                switch (respuestaPosibleSolicitudes.getT()) {
                    case 2:
                        if (location != null) {
                            enviarEvento(location, 5, 0, respuestaPosibleSolicitudes.getiPS());
                        }
                        if (respuestaPosibleSolicitudes.getM() != null) {
                            reproducirTextAudio.speak(respuestaPosibleSolicitudes.getM());
                        }
                        if (actividadMapa != null) {
                            actividadMapa.posiblesSolicitudes(respuestaPosibleSolicitudes);
                        }
                        break;
                    default:

                        if (actividadMapa != null) {
                            actividadMapa.posiblesSolicitudes(respuestaPosibleSolicitudes);
                        }
                        break;
                }
            }
        }
    };

    private final ScheduledExecutorService scheduler_panico = Executors.newScheduledThreadPool(2);
    ScheduledFuture<?> beeperHandle_panico = null;

    public void actualizarRastreoPanico(int tiempoRastreo, int tiempoFinRastreo, final int idVehiculoRastreo) {
        final Runnable beeper = new Runnable() {
            public void run() {
                enviarConsultarRastreoPanico(idVehiculoRastreo);
            }
        };
        beeperHandle_panico = scheduler_panico.scheduleAtFixedRate(beeper, tiempoRastreo, tiempoRastreo, TimeUnit.SECONDS);
        scheduler_panico.schedule(new Runnable() {
            public void run() {
                beeperHandle_panico.cancel(true);
                prenderEscuchaPosibleSolicitud();
                if (actividadMapa != null) {
                    actividadMapa.dejarRastrearPanico();
                }
            }
        }, tiempoFinRastreo, TimeUnit.MINUTES);
    }

    public void enviarConsultarRastreoPanico(int idVehiculo) {
        if (mSocket != null && isServerOnline) {
            JSONObject data = new JSONObject();
            try {
                data.put("idVehiculo", idVehiculo);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit(VariablesGlobales.EMIT_COSULTAR_LOCALIZACION_PANICO, data, new Ack() {
                @Override
                public void call(Object... args) {
                    try {
                        JSONObject data = new JSONObject(args[0].toString());
                        DatosSolicitud datosSolicitud = DatosSolicitud.datosSolicitud(data.getJSONObject("datosSolicitud").toString());
                        if (actividadMapa != null) {
                            actividadMapa.actualizarRastreoPanico(new LatLng(datosSolicitud.getLatitud(), datosSolicitud.getLongitud()));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void dejarRastrearPanico() {
        if (beeperHandle_panico != null) {
            beeperHandle_panico.cancel(true);
        }
    }

    private int estadoPosibleSolicitud;
    private RespuestaPosibleSolicitudes respuestaPosibleSolicitudes;

    public void cronometroPosiblesSolicitudes(final int estado, final RespuestaPosibleSolicitudes respuestaPosibleSolicitudes) {
        if (respuestaPosibleSolicitudes != null) {
            if (estado <= 4) {
                this.respuestaPosibleSolicitudes = respuestaPosibleSolicitudes;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            switch (estado) {
                                case 1:
                                    Thread.sleep(respuestaPosibleSolicitudes.getT1());
                                    break;
                                case 2:
                                    Thread.sleep(respuestaPosibleSolicitudes.getT2());
                                    break;
                                case 3:
                                    Thread.sleep(respuestaPosibleSolicitudes.getT3());
                                    break;

                            }
                            estadoPosibleSolicitud = estado + 1;
                            if (actividadMapa != null) {
                                actividadMapa.cambioIconoPosibleSolicitude(estadoPosibleSolicitud, respuestaPosibleSolicitudes);
                            }
                            switch (estadoPosibleSolicitud) {
                                case 2:
                                    cronometroPosiblesSolicitudes(estadoPosibleSolicitud, respuestaPosibleSolicitudes);
                                    break;
                                case 3:
                                    cronometroPosiblesSolicitudes(estadoPosibleSolicitud, respuestaPosibleSolicitudes);
                                    break;
                                case 4:
                                    prenderEscuchaPosibleSolicitud();
                                    ServicioSockets.this.respuestaPosibleSolicitudes = null;
                                    break;
                            }
                        } catch (InterruptedException e) {
                            e.getMessage();
                        }
                    }
                }).start();
            }
        }
    }

    public void getEstadoPosibleSolicitud() {
        if (actividadMapa != null && respuestaPosibleSolicitudes != null) {
            actividadMapa.cambioIconoPosibleSolicitude(estadoPosibleSolicitud, respuestaPosibleSolicitudes);
        }
    }

    private Emitter.Listener onMensajes = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                JSONObject objetData = (JSONObject) args[0];
                if (objetData.has("transaccion")) {
                    if (idTransaccionSaldoKtaxi == objetData.getInt("idTransaccion")) {
                        reproducirTextAudio.speak(objetData.getString("m"));
                        isEjecutarCancelar = false;
                        hiloCancelarServicio.interrupt();
                        hiloCancelarServicio = null;
                        switch (objetData.getInt("e")) {
                            case 1:
                                if (compraServicios != null) {
                                    compraServicios.onRespuestaCompraServicio(objetData.getString("m"));
                                } else if (compraServiciosMapa != null) {
                                    compraServiciosMapa.onRespuestaCompraServicio(objetData.getString("m"));
                                }
                                break;
                            case -1:
                                if (compraServicios != null) {
                                    compraServicios.onRespuestaCompraServicio(objetData.getString("m"));
                                } else if (compraServiciosMapa != null) {
                                    compraServiciosMapa.onRespuestaCompraServicio(objetData.getString("m"));
                                }
                                break;
                        }
                    }
                }
                if (objetData.has(VariablesGlobales.ESTADO)) {
                    if (objetData.getInt(VariablesGlobales.ESTADO) == 2) {
                        if (actividadMapa != null) {
                            actividadMapa.mensajeDesconeccion(objetData.getString(VariablesGlobales.MENSAJE) + " ,el usuario: " + objetData.getString(VariablesGlobales.NOMBRES));
                        }
                        if (mSocket != null) {
                            mSocket.disconnect();
                        }
                    }
                    if (objetData.getInt(VariablesGlobales.ESTADO) == 3) {
                        if (actividadMapa != null) {
                            actividadMapa.boletinKtaxi(objetData.getString("boletin"), objetData.getString("asunto"));
                        } else {
                            NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), objetData.getString("asunto") + objetData.getString("boletin"), MapaBaseActivity.class);
                        }
                    }

                } else {
                    if (solicitudSeleccionada != null) {
                        if (objetData.has("audio")) {
                            if (objetData.getBoolean("audio")) {
                                String nombreAudio = nameAudio();
                                UtilStream.byteArrayToFile((byte[]) args[1], ChatActivity.path + "audioIn.gzip");
                                byte[] data = UtilStream.decompressGzip(UtilStream.fileToByteArray(ChatActivity.path + "audioIn.gzip"), getApplicationContext());
                                File file = new File(ChatActivity.path + "audioIn.gzip");
                                file.delete();
                                if (data.length < 50) {
                                    return;
                                }
                                if (UtilStream.byteArrayToFile(data, ChatActivity.path + nombreAudio)) {
                                    ChatMessage msg = new ChatMessage();
                                    msg.setTipo(1);
                                    msg.setId(1);
                                    msg.setMe(false);
                                    msg.setNameArchivo(nombreAudio);
                                    // msg.setMessage(message);
                                    DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                                    msg.setDate(hourdateFormat.format(new Date()));
                                    chatHistory.add(msg);
                                    if (isBanderaLlamada) {
                                        sonarSolicitud(2);
                                    }
                                    if (actividadMapa != null) {
                                        actividadMapa.mensajesUsuario(chatHistory);
                                        isMensajeNuevo = true;
                                    } else if (actividadMensajes != null && ChatActivity.estadoChat) {
                                        actividadMensajes.mensajesUsuario(chatHistory);
                                    } else {
                                        isMensajeNuevo = true;
                                        NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), getString(R.string.mensaje_usuario_audio), MapaBaseActivity.class);
                                    }
                                }

                            }
                        }
                        if (!solicitudSeleccionada.isPedido()) {
                            if (solicitudSeleccionada.getIdSolicitud() == objetData.getInt(VariablesGlobales.ID_SOLICITUD)) {
                                if (objetData.has("message")) {
                                    ChatMessage msg = new ChatMessage();
                                    msg.setId(idMensaje);
                                    msg.setMe(false);
                                    msg.setMessage(objetData.getString("message"));
                                    DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                                    msg.setDate(hourdateFormat.format(new Date()));
                                    chatHistory.add(msg);
                                    if (configuracionApp.getBoolean(VariablesGlobales.CONFIGURACION_CHAT_AUDIOS, true)) {
                                        reproducirTextAudio.speak("El cliente ha escrito. " + objetData.getString("message"));
                                    } else {
                                        if (isBanderaLlamada) {
                                            sonarSolicitud(2);
                                        }
                                    }
                                }
                                idMensaje++;

                                if (actividadMapa != null) {
                                    actividadMapa.mensajesUsuario(chatHistory);
                                    isMensajeNuevo = true;
                                } else if (actividadMensajes != null && ChatActivity.estadoChat) {
                                    actividadMensajes.mensajesUsuario(chatHistory);
                                } else {
                                    isMensajeNuevo = true;
                                    NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), getString(R.string.mensaje_usuario) + objetData.getString("message"), MapaBaseActivity.class);
                                }

                            }
                        } else {
                            if (solicitudSeleccionada.getIdPedido() == objetData.getInt(VariablesGlobales.ID_PEDIDO)) {
                                if (configuracionApp.getBoolean(VariablesGlobales.CONFIGURACION_CHAT_AUDIOS, true)) {
                                    String mensaje = "El cliente ha escrito. ".concat(objetData.getString("message"));
                                    reproducirTextAudio.speak(mensaje);
                                    ExtraLog.Log(TAG, mensaje);
                                } else {
                                    if (isBanderaLlamada) {
                                        sonarSolicitud(2);
                                    }
                                }
                                JSONObject object = new JSONObject();
                                object.put("idChatPedido", objetData.getInt("idChatPedido"));
                                if (mSocket != null) {
                                    mSocket.emit("notificar_lectura_chat", object, new Ack() {
                                        @Override
                                        public void call(Object... args) {

                                        }
                                    });
                                }
                                ChatMessage msg = new ChatMessage();
                                msg.setId(idMensaje);
                                msg.setMe(false);
                                msg.setMessage(objetData.getString("message"));
                                DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                                msg.setDate(hourdateFormat.format(new Date()));
                                chatHistory.add(msg);
                                idMensaje++;
                                if (actividadMapa != null) {
                                    actividadMapa.mensajesUsuario(chatHistory);
                                    isMensajeNuevo = true;
                                } else if (actividadMensajes != null && ChatActivity.estadoChat) {
                                    actividadMensajes.mensajesUsuario(chatHistory);
                                } else {
                                    isMensajeNuevo = true;
                                    NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), getString(R.string.mensaje_usuario) + objetData.getString("message"), MapaBaseActivity.class);
                                }

                            }
                        }

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    };


    private Emitter.Listener onAtendiendoSolicitud = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            ExtraLog.Log(TAG, "call AtendiendoSolicitud: " + args[0].toString());
            try {
                JSONObject data = (JSONObject) args[0];
                if (data.has(VariablesGlobales.ESTADO)) {
                    String mensajeSolicitudCancelada = "";
                    switch (data.getInt(VariablesGlobales.ESTADO)) {
                        case 2:
                            if (data.has(VariablesGlobales.ID_SOLICITUD)) {
                                limpiarLista(data.getInt(VariablesGlobales.ID_SOLICITUD));
                            }
                            if (data.has(VariablesGlobales.ID_PEDIDO)) {
                                limpiarLista(data.getInt(VariablesGlobales.ID_PEDIDO));
                            }
                            break;
                        case 3:
                            if (data.has(VariablesGlobales.ID_PEDIDO)) {
                                if (solicitudSeleccionada != null) {
                                    if (data.getInt(VariablesGlobales.ID_PEDIDO) == solicitudSeleccionada.getIdPedido()) {
                                        isCronometroTiempo = false;
                                        estadoPedido = 3;
                                        solicitudSeleccionada.setEstadoPedido(3);
                                        guardarEstadoSolicitud();
                                        confirmarEstado(solicitudSeleccionada.getIdPedido(), estadoPedido);
                                        prenderPantallaMapa();
                                        if (!isBanderaRun) {
                                            isBanderaRun = true;
                                            getHora(tiempoEnvio);
                                        }
                                        precioEnviado = false;
                                        if (actividadMapa != null) {
                                            actividadMapa.clienteAceptoTiempo(true, solicitudSeleccionada, "");
                                        } else if (actividadLista != null) {
                                            actividadLista.clienteAceptoTiempo(true, solicitudSeleccionada, "");
                                        } else {
                                            NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), getString(R.string.cliente_acepto_pedido), MapaBaseActivity.class);
                                        }
                                    }
                                }
                            }
                            break;
                        case 4:
                            if (solicitudSeleccionada != null) {
                                if (solicitudSeleccionada.getIdSolicitud() == data.getInt(VariablesGlobales.ID_SOLICITUD)) {
                                    tiempoEnviado = true;
                                    tiempoEnvio = 3;
                                    if (actividadMapa != null) {
                                        actividadMapa.tiempoEnviado(tiempoEnvio, solicitudSeleccionada);
                                        actividadMapa.cambiarEstadoComprando();
                                    }
                                }
                            }
                            break;
                        case 5:
                            prenderPantallaMapa();
                            clienteAceptoElTiempo(data);
                            guardarEstadoEnvio(true);
                            break;
                        case 6:
                            if (solicitudSeleccionada != null && !solicitudSeleccionada.isPedido()) {
                                if (solicitudSeleccionada.getIdSolicitud() == data.getInt(VariablesGlobales.ID_SOLICITUD)) {
                                    razonCancelada = data.has(VariablesGlobales.JSON_RAZON) ? data.getInt(VariablesGlobales.JSON_RAZON) : 0;
                                    mensajeSolicitudCancelada = "Cliente cancelo la solicitud.";
                                    solicitudSeleccionada.setRazonCancelada(razonCancelada);
                                    switch (solicitudSeleccionada.getRazonCancelada()) {
                                        case 4:
                                            mensajeSolicitudCancelada = "Cliente canceló la solicitud.";
                                            reproducirTextAudio.speak(mensajeSolicitudCancelada);
                                            break;
                                        case 8:
                                            mensajeSolicitudCancelada = "Cliente no aceptó tiempo.";
                                            reproducirTextAudio.speak(mensajeSolicitudCancelada);
                                            break;
                                        default:
                                            solicitudSeleccionada.setCalificar(true);
                                            mensajeSolicitudCancelada = "Cliente canceló la solicitud, califique al cliente por favor.";
                                            reproducirTextAudio.speak(mensajeSolicitudCancelada);
                                            break;
                                    }
                                    solicitudSeleccionada.setEstadoSolicitud(6);
                                    solicitudSeleccionada.setMensajeSolicitudCancelada(mensajeSolicitudCancelada);
                                    guardarEstadoSolicitud();
                                    if (actividadMapa != null) {
                                        prenderPantallaMapa();
                                        actividadMapa.clienteCanceloSolicitud(solicitudSeleccionada, mensajeSolicitudCancelada, solicitudSeleccionada.isCalificar());
                                    } else if (actividadLista != null) {
                                        prenderPantalla();
                                        actividadLista.clienteCanceloSolicitud(solicitudSeleccionada, mensajeSolicitudCancelada, solicitudSeleccionada.isCalificar());
                                    } else if (actividadMensajes != null) {
                                        actividadMensajes.clienteCancelo();
                                    } else {
                                        prenderPantallaMapa();
                                        NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), mensajeSolicitudCancelada, MapaBaseActivity.class);
                                    }
                                    if (!solicitudSeleccionada.isCalificar()) {
                                        activarAlCancelar();
                                    }
                                    break;
                                }
                            } else {  // Estado para pedidos
                                if (data.has(VariablesGlobales.ID_SOLICITUD)) {
                                    activaEnNuevaSolicitud();
                                    limpiarLista(data.getInt(VariablesGlobales.ID_SOLICITUD));
                                    if (data.has(VariablesGlobales.ID_SOLICITUD) && data.has(VariablesGlobales.ESTADO)) {
                                        if (actividadMapa != null) {
                                            actividadMapa.alertasDialogos(data.getInt(VariablesGlobales.ID_SOLICITUD));
                                        }
                                        if (actividadLista != null) {
                                            actividadLista.alertasDialogos(data.getInt(VariablesGlobales.ID_SOLICITUD));
                                        }
                                    }
                                }
                            }
                            break;
                        case 7: //Estado para pedidos
                            if (data.has(VariablesGlobales.ID_PEDIDO)) {
                                if (solicitudSeleccionada != null) {
                                    if (data.getInt(VariablesGlobales.ID_PEDIDO) == solicitudSeleccionada.getIdPedido()) {
                                        razonCancelada = data.has(VariablesGlobales.JSON_RAZON) ? data.getInt(VariablesGlobales.JSON_RAZON) : 0;
                                        String mensajePedidoCancelado = (data.has("mensaje")) ? data.getString("mensaje") : "El cliente canceló el pedido.";
                                        solicitudSeleccionada.setRazonCancelada(razonCancelada);
                                        switch (solicitudSeleccionada.getRazonCancelada()) {
                                            case 4:
                                                if (solicitudSeleccionada.getEstadoPedido() == 3) {
                                                    mensajePedidoCancelado = "Cliente canceló el pedido, califique al cliente por favor.";
                                                    solicitudSeleccionada.setCalificar(true);
                                                }
                                                reproducirTextAudio.speak(mensajePedidoCancelado);
                                                break;
                                            case 6:
                                                mensajePedidoCancelado = (data.has("mensaje")) ? data.getString("mensaje") : "Cliente no aceptó el precio.";
                                                reproducirTextAudio.speak(mensajePedidoCancelado);
                                                break;
                                            default:
                                                if (solicitudSeleccionada.getEstadoPedido() == 3) {
                                                    solicitudSeleccionada.setCalificar(true);
                                                }
                                                mensajePedidoCancelado = "Cliente canceló el pedido, califique al cliente por favor.";
                                                reproducirTextAudio.speak(mensajePedidoCancelado);
                                                break;
                                        }
                                        solicitudSeleccionada.setEstadoPedido(7);
                                        solicitudSeleccionada.setMensajePedidoCancelado(mensajePedidoCancelado);
                                        guardarEstadoSolicitud();
                                        if (actividadMapa != null) {
                                            prenderPantallaMapa();
                                            actividadMapa.clienteCanceloPedido(solicitudSeleccionada, mensajePedidoCancelado, solicitudSeleccionada.isCalificar());
                                        } else if (actividadLista != null) {
                                            prenderPantalla();
                                            actividadLista.clienteCanceloPedido(solicitudSeleccionada, mensajePedidoCancelado, solicitudSeleccionada.isCalificar());
                                        } else if (actividadMensajes != null) {
                                            actividadMensajes.clienteCancelo();
                                        } else {
                                            prenderPantallaMapa();
                                            NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), mensajePedidoCancelado, MapaBaseActivity.class);
                                        }
                                        if (!solicitudSeleccionada.isCalificar()) {
                                            activarAlCancelar();
                                        }
                                    }
                                } else {
                                    if (data.getInt("razon") == 4 || data.getInt("razon") == 5) {
                                        limpiarLista(data.getInt(VariablesGlobales.ID_PEDIDO));
                                    }
                                }
                            }
                            break;
                        case 9:
                            if (solicitudSeleccionada != null) {
                                if (data.getInt(VariablesGlobales.ID_SOLICITUD) == solicitudSeleccionada.getIdSolicitud()) {
                                    prenderPantalla();
                                    isEstadoAbordo = true;
                                    isMensajeAbordo = false;
                                    estadoSolicitud = 9;
                                    solicitudSeleccionada.setEstadoSolicitud(9);
                                    guardarEstadoSolicitud();
                                    if (actividadMapa != null) {
                                        actividadMapa.clienteAbordo(getString(R.string.cliente_informa_abordo), solicitudSeleccionada);
                                    } else if (actividadLista != null) {
                                        actividadLista.clienteAceptoTiempo(true, solicitudSeleccionada, "");
                                    } else if (actividadMensajes != null && ChatActivity.estadoChat) {
                                        actividadMensajes.clienteAbordo(getString(R.string.cliente_informa_abordo), solicitudSeleccionada);
                                    } else {
                                        NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), getString(R.string.cliente_informa_abordo), MapaBaseActivity.class);
                                    }
                                }
                                break;
                            }

                        case 10:
                            if (solicitudSeleccionada != null) {
                                if (solicitudSeleccionada.getIdPedido() == 0) {
                                    if (data.getInt(VariablesGlobales.ID_SOLICITUD) == solicitudSeleccionada.getIdSolicitud()) {
//                                        borrarPreferencia();
                                        chatHistory.clear();
                                        estadoSolicitud = 10;
                                        solicitudSeleccionada.setEstadoSolicitud(10);
                                        guardarEstadoSolicitud();
                                        isBanderaRun = false;
                                        setClienteAceptoTiempo(false);
                                        if (actividadMapa != null) {
                                            if (data.getJSONObject("calificacion").getString("observacion").equals("")) {
                                                actividadMapa.clienteFinCarrera(getString(R.string.cliente_finalizo_carrera), "1", solicitudSeleccionada);
                                            } else {
                                                actividadMapa.clienteFinCarrera(getString(R.string.cliente_finalizo_carrera), "1", solicitudSeleccionada);
                                            }
                                            return;
                                        } else if (actividadMensajes != null && ChatActivity.estadoChat) {
                                            if (data.getJSONObject("calificacion").getString("observacion").equals("")) {
                                                actividadMensajes.clienteFinCarrera(getString(R.string.cliente_finalizo_carrera), "1");
                                            } else {
                                                actividadMensajes.clienteFinCarrera(getString(R.string.cliente_finalizo_carrera), "1");
                                            }
                                            return;
                                        } else {
                                            NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), getString(R.string.cliente_finalizo_carrera), MapaBaseActivity.class);

                                        }
                                    }
                                } else {
                                    if (data.has(VariablesGlobales.ID_PEDIDO)) {
                                        if (data.getInt(VariablesGlobales.ID_PEDIDO) == solicitudSeleccionada.getIdPedido()) {
                                            isBanderaRun = false;
                                            minuto = 0;
                                            segundos = 0;
                                            chatHistory.clear();
                                            estadoPedido = 6;
                                            solicitudSeleccionada.setEstadoPedido(6);
                                            guardarEstadoSolicitud();
                                            confirmarEstado(solicitudSeleccionada.getIdPedido(), estadoPedido);
                                            if (actividadMapa != null) {
                                                data.getJSONObject("calificacion").getInt("valoracion");
                                                data.getJSONObject("calificacion").getString("observacion");
                                                actividadMapa.clienteFinCarrera(getString(R.string.cliente_finalizo_carrera), "1", solicitudSeleccionada);
                                                return;
                                            } else if (actividadMensajes != null && ChatActivity.estadoChat) {
                                                data.getJSONObject("calificacion").getInt("valoracion");
                                                data.getJSONObject("calificacion").getString("observacion");
                                                actividadMensajes.clienteFinCarrera(getString(R.string.cliente_finalizo_carrera), "1");
                                            } else {
                                                NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), getString(R.string.cliente_finalizo_carrera), MapaBaseActivity.class);

                                            }
                                        }
                                    }
                                }
                                break;
                            }

                        case 11:
                            if (data.has(VariablesGlobales.ID_SOLICITUD)) {
                                limpiarLista(data.getInt(VariablesGlobales.ID_SOLICITUD));
                            }
                            break;
                        case 14:
                            if (solicitudSeleccionada != null) {
                                if (data.getInt(VariablesGlobales.ID_SOLICITUD) == solicitudSeleccionada.getIdSolicitud()) {
                                    JSONObject object = new JSONObject(data.getJSONObject("datosAenviar").toString());
                                    if (actividadMapa != null) {
                                        actividadMapa.cobrarDineroElectronico(object);
                                    }
                                }
                            }
                            break;
                        case 15:
                            if (solicitudSeleccionada != null) {
                                if (data.getInt(VariablesGlobales.ID_SOLICITUD) == solicitudSeleccionada.getIdSolicitud()) {
                                    razonCancelada = data.has(VariablesGlobales.JSON_RAZON) ? data.getInt(VariablesGlobales.JSON_RAZON) : 0;
                                    mensajeSolicitudCancelada = data.has("mensaje") ? data.getString("mensaje") : "Cliente cancelo la solicitud.";
                                    solicitudSeleccionada.setRazonCancelada(razonCancelada);
                                    solicitudSeleccionada.setEstadoSolicitud(15);
                                    solicitudSeleccionada.setMensajeSolicitudCancelada(mensajeSolicitudCancelada);
                                    guardarEstadoSolicitud();
                                    reproducirTextAudio.speak(mensajeSolicitudCancelada);
                                    if (actividadMapa != null) {
                                        prenderPantallaMapa();
                                        actividadMapa.clienteCanceloSolicitud(solicitudSeleccionada, mensajeSolicitudCancelada, solicitudSeleccionada.isCalificar());
                                    } else if (actividadLista != null) {
                                        prenderPantalla();
                                        actividadLista.clienteCanceloSolicitud(solicitudSeleccionada, mensajeSolicitudCancelada, solicitudSeleccionada.isCalificar());
                                    } else if (actividadMensajes != null) {
                                        actividadMensajes.clienteCancelo();
                                    } else {
                                        NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), mensajeSolicitudCancelada, MapaBaseActivity.class);
                                    }
                                    if (!solicitudSeleccionada.isCalificar()) {
                                        activarAlCancelar();
                                    }
                                }
                            }
                            break;

                        case 16: //Acepto cobro
                            if (actividadMapa != null) {
                                voucherOperadorReciveConfirmacionPago(getIdSolicitudSeleccionada());
                                actividadMapa.cobroAceptado(true, "", 0);
                                isClienteVoucherEstado = true;
                            }
                            break;
                        case 17: // No acepta
                            if (actividadMapa != null) {
                                actividadMapa.cobroAceptado(false, "Pago no aceptado por el cliente", 1);
                                isClienteVoucherEstado = true;
                            }
                            break;
                        case 20:
                            if (actividadMapa != null) {
                                actividadMapa.cobroAceptado(true, "", 2);
                            }
                            break;
                        case 21:
                            if (actividadMapa != null) {
                                actividadMapa.cobroAceptado(true, "", 2);
                            }
                            break;
                        case 22:
                            if (actividadMapa != null) {
                                actividadMapa.cobroAceptado(false, "", 2);
                            }
                            break;
                        case 23:
                            if (actividadMapa != null) {
                                actividadMapa.cobroAceptado(false, "", 2);
                            }
                            break;
                        case 100:
                            if (solicitudSeleccionada != null) {
                                if (data.getInt(VariablesGlobales.ID_PEDIDO) == solicitudSeleccionada.getIdPedido()) {
                                    switch (data.getInt("confirmo")) {
                                        case 4:
                                            if (actividadMapa != null) {
                                                estadoComprandoSemaforo = 3;
                                                solicitudSeleccionada.setEstadoComprandoSemaforo(3);
                                                guardarEstadoSolicitud();
                                                actividadMapa.cambiarEstadoComprandoUsuario();
                                            }
                                            break;
                                        case 5:
                                            if (actividadMapa != null) {
                                                estadoCompradoSemaforo = 3;
                                                solicitudSeleccionada.setEstadoCompradoSemaforo(3);
                                                guardarEstadoSolicitud();
                                                actividadMapa.cambiarEstadoCompradoUsuario();
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }

                            break;
                        case 101:
                            if (solicitudSeleccionada != null) {
                                if (data.getInt(VariablesGlobales.ID_PEDIDO) == solicitudSeleccionada.getIdPedido()) {
                                    switch (data.getInt("confirmo")) {
                                        case 4:
                                            if (actividadMapa != null) {
                                                actividadMapa.cambiarEstadoComprandoUsuario();
                                                isComprando = true;
                                            }
                                            break;
                                        case 5:
                                            if (actividadMapa != null) {
                                                actividadMapa.cambiarEstadoCompradoUsuario();
                                                isComprado = true;
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            break;
                        case 112:

                            break;
                        default:
                            borrarCarreraSeleccionada();
                            borrarPreferencia();
                            break;
                    }
                } else if (data.has("estadoPago")) {
                    switch (data.getInt("estadoPago")) {
                        case 1: //Cobro con confirmacion
                            break;
                        case 2: // EL cliente acepto la forma de pago
                            if (getIdSolicitudSeleccionada() == data.getInt("idS")) {
                                if (actividadMapa != null) {
                                    voucherOperadorReciveConfirmacionPago(getIdSolicitudSeleccionada());
                                    actividadMapa.cobroAceptado(true, "", 1);
                                    isClienteVoucherEstado = true;
                                }
                            }
                            break;
                        case 3: // El operador cobro con ppin
                            if (actividadMapa != null) {
                                actividadMapa.cobroAceptado(true, data.getString("m"), 1);
                                reproducirTextAudio.speak(data.getString("m"));
                            }
                            break;
                        case 4:// El operador corbro con cliente uido
                            break;
                        case 5:// Cobro con dinero electronico en proceso
                            //{"estadoCobro":5,"idS":93,"estado":9,"idSolicitud":93}
//                            if (actividadMapa != null) {
//                                if(data.has("m")) {
//                                    actividadMapa.cobroAceptado(true, data.getString("m"), 2);
//                                    reproducirTextAudio.speak(data.getString("m"));
//                                }
//                            }
                            break;
                        case 6: // Cobro con dinero electronico completado
                            if (actividadMapa != null) {
                                if (data.has("m")) {
                                    actividadMapa.cobroAceptado(true, data.getString("m"), 2);
                                    reproducirTextAudio.speak(data.getString("m"));
                                }
                            }
                            break;
                        case 7: // Cobro con dinero electronico fallido no reintentar
                            if (actividadMapa != null) {
                                if (data.has("m")) {
                                    actividadMapa.cobroAceptado(false, data.getString("m"), 2);
                                    reproducirTextAudio.speak(data.getString("m"));
                                }
                            }
                            break;
                        case 8: // Cobro con dinero electronico se puede reintentar
                            if (actividadMapa != null) {
                                if (data.has("m")) {
                                    actividadMapa.cobroAceptado(false, data.getString("m"), 2);
                                    reproducirTextAudio.speak(data.getString("m"));
                                }
                            }
                            break;
                        case 10: // Cobro con efectivo.
                            if (actividadMapa != null) {
                                if (data.getInt("e") == 1) {
                                    if (data.has("m")) {
                                        reproducirTextAudio.speak(data.getString("m"));
                                    }
                                }
                            }
                            break;
                    }
                }

            } catch (JSONException | NullPointerException e) {
                ExtraLog.Log(TAG, "call: NULL POINTER" + e.getMessage());
            }
        }
    };

    public void activarAlCancelar() {
        isEstadoAbordo = false;
        isMensajeAbordo = false;
        activaEnNuevaSolicitud();
        activarEscuchas();
        setEnviarComprarPedido(false);
        setEnviarllevandoPedido(false);
        resetIdEnvioCalificacion();
        borrarCarreraSeleccionada();
        estadoSolicitud = 0;
        estadoPedido = 0;
        solicitudSeleccionada = null;
        razonCancelada = -1;
        setIdSolicitudSeleccionada(0);
        setIdPedidoSeleccionado(0);
        borrarPreferencia();
        guardarEstadoEnvio(false);
    }

    @SuppressLint("InvalidWakeLockTag")
    public void prenderPantallaMapa() {
        powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "INFO");
        wakeLock.acquire(1);
        Intent intent = new Intent(this, MapaBaseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }


    public ServicioSockets() {
    }

    public void registerCliente(Activity activity, String nombreClase) {
        actividadIniciarSesion = null;
        actividadMensajes = null;
        actividadLista = null;
        actividadCancelar = null;
        actividadMensajeriaBroadCast = null;
        actividadMapa = null;
        actividadRecargas = null;
        actividadSaldoKtaxiPager = null;
        actividadTaximetro = null;
        compraServicios = null;
        compraServiciosMapa = null;
        onComunicacionAgregarTarjeta = null;
        switch (nombreClase) {
            case "IniciarSesionBaseActivity":
                actividadIniciarSesion = (OnComunicacionIniciarSesion) activity;
                break;
            case "MapaBaseActivity":
                actividadMapa = (OnComunicacionMapa) activity;
                break;
            case "ListaSolicitudesActivity":
                actividadLista = (OnComunicacionSocketLista) activity;
                break;
            case "ChatActivity":
                actividadMensajes = (OnComunicacionSocketMensajeria) activity;
                break;
            case "CancelarSolicitudActivity":
                actividadCancelar = (OnComunicacionCancelar) activity;
                break;
            case "ChatActivityBroadCast":
                actividadMensajeriaBroadCast = (onComunicaionMensajesBroadCast) activity;
                break;
            case "TaximetroActivity":
                actividadTaximetro = (OnComuniacionTaximetro) activity;
                break;
            case "RecargasActivity":
                actividadRecargas = (OnComunicacionRecargas) activity;
                break;
            case "SaldoKtaxiPagerActity":
                actividadSaldoKtaxiPager = (OnComunicacionSaldoKtaxiPagerActity) activity;
                break;

            case "DetalleCompraServicioActivity":
                compraServicios = (OnComunicacionCompraServicios) activity;
                break;
            case "MapaSaldoKtaxi":
                compraServiciosMapa = (OnComunicacionCompraServiciosMapa) activity;
                break;
            case "AgregarTarjeta":
                onComunicacionAgregarTarjeta = (OnComunicacionAgregarTarjeta) activity;
                break;
        }
    }

    public void enviarAudio(String filename) {
        UtilStream.compressGzipFile(ChatActivity.path + filename, ChatActivity.path + "audioOut.gzip");
        byte[] data = UtilStream.fileToByteArray(ChatActivity.path + "audioOut.gzip");
        File file = new File(ChatActivity.path + "audioOut.gzip");
        file.delete();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setTipo(1);
        chatMessage.setId(2);
        chatMessage.setNameArchivo(filename);
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        chatMessage.setDate(hourdateFormat.format(new Date()));
        chatMessage.setMe(true);
        chatHistory.add(chatMessage);
        JSONObject object = new JSONObject();
        try {
            if (solicitudSeleccionada.getIdPedido() == 0) {
                object.put("tipo", 1);
                object.put("to", solicitudSeleccionada.getUsername());
                object.put("idSolicitud", solicitudSeleccionada.getIdSolicitud());
            } else {
                object.put("tipo", 2);
                object.put("to", solicitudSeleccionada.getUsername());
                object.put(VariablesGlobales.ID_PEDIDO, solicitudSeleccionada.getIdPedido());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (mSocket != null && solicitudSeleccionada != null) {
            mSocket.emit(VariablesGlobales.EMIT_OPERADOR_ENVIA_MENSAJE_DATA, object, data, new Ack() {
                @Override
                public void call(Object... args) {
                    obtenerMensajes();
                }
            });
        }

    }

    public String nameAudio() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            String currentTimeStamp = dateFormat.format(new Date());
            return "ktaxi_" + currentTimeStamp + ".mp3";
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public void clienteAceptoElTiempo(JSONObject data) {
        isCronometroTiempo = false;
        if (solicitudSeleccionada != null) {
            try {
                if (solicitudSeleccionada.getIdSolicitud() == data.getInt(VariablesGlobales.ID_SOLICITUD)) {
                    estadoSolicitud = 5;
                    solicitudSeleccionada.setEstadoSolicitud(5);
                    guardarEstadoSolicitud();
                    setClienteAceptoTiempo(true);
                    if (!isBanderaRun) {
                        isBanderaRun = true;
                        getHora(tiempoEnvio);
                    }
                    tiempoEnviado = false;
                    if (actividadMapa != null) {
                        actividadMapa.clienteAceptoTiempo(true, solicitudSeleccionada, "");
                    } else if (actividadLista != null) {
                        actividadLista.clienteAceptoTiempo(true, solicitudSeleccionada, "");
                    } else {
                        NotificacionConIntent(getBaseContext(), 4, getString(R.string.app_name), getString(R.string.cliente_acepto_solicitud), MapaBaseActivity.class);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void borrarPreferencia() {
        spSolicitud = getSharedPreferences("estado_solicitud", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = spSolicitud.edit();
        editor.putString("key_objeto_solicitud", "");
        editor.putBoolean("key_estado_solicitud", false);
        editor.apply();
        isEstadoAbordo = false;
        isMensajeAbordo = false;
    }

    public StringBuilder listaConsulta(int id) {
        StringBuilder objetodato;
        if (id > 0) {
            objetodato = new StringBuilder("{lista:[");
            objetodato.append(id);
            objetodato.append("]}");
        } else {
            objetodato = new StringBuilder("{lista:[]}");
        }
        return objetodato;
    }

    public void recuperarSolicitud() {
        if (spobtener.getBoolean(VariablesGlobales.LOGEADO, false)) {
            if (spSolicitud.getBoolean("key_estado_solicitud", false)) {
                solicitudSeleccionada = Solicitud.createSolicitud(spSolicitud.getString("key_objeto_solicitud", null));
                if (solicitudSeleccionada != null) {
                    if (solicitudSeleccionada.isPedido()) {
                        idPedidoSeleccionado = solicitudSeleccionada.getIdPedido();
                        estadoPedido();
                    } else {
                        idSolicitudSeleccionada = solicitudSeleccionada.getIdSolicitud();
                        estadoSolicitud();
                    }
                }
            }
        }
    }

    public void probarIniciarConexion() {
        if (!mSocket.connected()) {
            iniciarConexion();
        }
    }


    public void iniciarConexion() {
        setearInicio();
        ConexionSocket conexionSocket = new ConexionSocket();
        mSocket = conexionSocket.inciarConexion(IP_SERVIDOR);
        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                if (mSocket != null && !isConnect) {
                    isConnect = true;
                    isServerOnline = true;
                    notificarCambioEnServidor();
                    if (spobtener.getBoolean(VariablesGlobales.LOGEADO, false)) {
                        try {
                            JSONObject dataEnvio = null;
                            if (spSolicitud.getBoolean("key_estado_solicitud", false)) {
                                solicitudSeleccionada = Solicitud.createSolicitud(spSolicitud.getString("key_objeto_solicitud", null));
                            }
                            if (solicitudSeleccionada != null && !solicitudSeleccionada.isPedido()) {
                                if (solicitudSeleccionada.getIdSolicitud() != 0) {
                                    dataEnvio = new JSONObject(listaConsulta(solicitudSeleccionada.getIdSolicitud()).toString());
                                } else {
                                    dataEnvio = new JSONObject(listaConsulta(0).toString());
                                }
                            } else {
                                dataEnvio = new JSONObject(listaConsulta(0).toString());
                            }
                            mSocket.emit(VariablesGlobales.EMIT_RECONECTAR_KTAXI, spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0), spobtener.getInt(VariablesGlobales.ID_CIUDAD, 0), spobtener.getInt(VariablesGlobales.ID_EMPRESA, 0), spobtener.getInt(VariablesGlobales.ID_USUARIO, 0), spobtener.getString(VariablesGlobales.NOMBRES, "") + " " + spobtener.getString(VariablesGlobales.APELLIDOS, ""), idAndroid, dataEnvio, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    activarEscuchas();
                                    if (solicitudSeleccionada != null) {
                                        if (solicitudSeleccionada.isPedido()) {
                                            idPedidoSeleccionado = solicitudSeleccionada.getIdPedido();
                                            estadoPedido();
                                            try {
                                                if (solicitudSeleccionada != null) {
                                                    consultarEstadoPedido(solicitudSeleccionada.getIdPedido());
                                                }
                                            } catch (NullPointerException e) {
                                                ExtraLog.Log(TAG, "call: " + VariablesGlobales.EMIT_RECONECTAR_KTAXI + e);
                                            }

                                        } else {
                                            idSolicitudSeleccionada = solicitudSeleccionada.getIdSolicitud();
                                            estadoSolicitud();
                                        }
                                    }
                                    try {
                                        final JSONObject data = new JSONObject(args[0].toString());
                                        switch (data.getInt(VariablesGlobales.ESTADO)) {
                                            case 1:
                                                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (actividadMapa != null) {
                                                            try {
                                                                actividadMapa.mensajeDesconeccion(data.getString(VariablesGlobales.MENSAJE) + ", el usuario: " + data.getString(VariablesGlobales.NOMBRES));
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                        mSocket.disconnect();
                                                    }
                                                }, 10000);
                                                break;
                                            case 3:
                                                cambiosEnUsuario(args[0].toString());
                                                cargarConfiguraciones(args[0].toString());
                                                break;
                                            case -404:
//                                                    cambiarPreferenciasLogin(data.getString("ip"));
                                                break;
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    validarTiemposRastreo(args[0].toString());
                                    enviarCache();
                                    if (runnableSincroniza == null) {
                                        sincronizarRastreo();
                                    } else {
                                        scheduler.execute(runnableSincroniza);
                                    }
                                    enviarSolicitudPendiente();
                                }
                            });
                        } catch (JSONException e) {
                            e.getMessage();
                        }

                    }
                    probarEstadoCobro();
                }
            }
        });
    }


    private int nivel = 0;


    @Override
    public void respuestaPin(boolean isRespuesta, String ipRespuesta) {
        SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
        if (isRespuesta) {
            Toast.makeText(this, "pin ok" + nivel + "ip:" + ipRespuesta, Toast.LENGTH_SHORT).show();
            new MainPresentador(null, null, null, this, this).obtenerConfiguracionServerNew(ipRespuesta, VariablesGlobales.NUM_ID_APLICATIVO, VariablesGlobales.ID_PLATAFORMA, VariablesGlobales.APP, utilidades.obtenerVersion(this));
            nivel = 0;
        } else {
            switch (nivel) {
                case 0:
                    Toast.makeText(this, "pin false" + nivel + "ip:" + ipRespuesta, Toast.LENGTH_SHORT).show();
                    nivel++;
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(null, null, null, this, this).getConfiuracionInicial(urlKtaxiConductor.getUrl_produccion_http());
                    } else {
                        new MainPresentador(null, null, null, this, this).getConfiuracionInicial(urlKtaxiConductor.getUrl_desarrollo_http());
                    }
                    break;
                case 1:
                    Toast.makeText(this, "pin false" + nivel + "ip:" + ipRespuesta, Toast.LENGTH_SHORT).show();
                    nivel++;
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(null, null, null, this, this).getConfiuracionInicial(urlKtaxiConductor.getUrl_produccion_ip().replace(":8080", ""));
                    } else {
                        new MainPresentador(null, null, null, this, this).getConfiuracionInicial(urlKtaxiConductor.getUrl_desarrollo_ip().replace(":8080", ""));
                    }

                    break;
                case 2:
                    Toast.makeText(this, "pin false" + nivel + "ip:" + ipRespuesta, Toast.LENGTH_SHORT).show();
                    nivel++;
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(null, null, null, this, this).getConfiuracionInicial(urlKtaxiConductor.getUrl_produccion_ip());
                    } else {
                        new MainPresentador(null, null, null, this, this).getConfiuracionInicial(urlKtaxiConductor.getUrl_desarrollo_ip());
                    }
                    break;
                case 3:
                    Toast.makeText(this, "pin false" + nivel + "HELP", Toast.LENGTH_SHORT).show();
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(null, null, null, this, this).getHelp();
                    } else {
                        new MainPresentador(null, null, null, this, this).getHelp();
                    }
                    break;
            }
        }
    }

    @Override
    public void respuestaHelp(String json) {
        if (json != null) {
            try {
                JSONObject object = new JSONObject(json);
                if (object.has("en")) {
                    switch (object.getInt("en")) {
                        case 0:
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(5000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    nivel = 0;
                                    SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                                        new MainPresentador(null, null, null, ServicioSockets.this, ServicioSockets.this).getConfiuracionInicial(urlKtaxiConductor.getUrl_produccion_https());
                                    } else {
                                        new MainPresentador(null, null, null, ServicioSockets.this, ServicioSockets.this).getConfiuracionInicial(urlKtaxiConductor.getUrl_desarrollo_https());
                                    }
                                }
                            }).start();
                            break;
                        case 1:
                            if (actividadMapa != null) {
                                actividadMapa.configuracionServidorHelp(object.getString("m"));
                            } else if (actividadLista != null) {
                                actividadLista.configuracionServidorHelp(object.getString("m"));
                            }
                            nivel = 0;
                            break;
                        case 2:
                            new MainPresentador(null, null, null, this, this).getConfiuracionInicial(object.getString("url"));
                            break;
                    }
                }
            } catch (JSONException e) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(5000);
                            nivel = 0;
                            SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                            if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                                new MainPresentador(null, null, null, ServicioSockets.this, ServicioSockets.this).getConfiuracionInicial(urlKtaxiConductor.getUrl_produccion_https());
                            } else {
                                new MainPresentador(null, null, null, ServicioSockets.this, ServicioSockets.this).getConfiuracionInicial(urlKtaxiConductor.getUrl_desarrollo_https());
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(5000);
                        SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                        if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                            new MainPresentador(null, null, null, ServicioSockets.this, ServicioSockets.this).getConfiuracionInicial(urlKtaxiConductor.getUrl_produccion_https());
                        } else {
                            new MainPresentador(null, null, null, ServicioSockets.this, ServicioSockets.this).getConfiuracionInicial(urlKtaxiConductor.getUrl_desarrollo_https());
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            nivel = 0;

        }
    }


    public void consultaMensaje(int tipo) {
        final JSONObject object = new JSONObject();
        try {
            object.put("iV", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
            object.put("iC", spobtener.getInt(VariablesGlobales.ID_CIUDAD, 0));
            object.put("iA", VariablesGlobales.NUM_ID_APLICATIVO);
            object.put("e", estadoBoton);
            object.put("t", tipo);
            object.put("iU", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mSocket != null) {
            mSocket.emit(VariablesGlobales.EMIT_RECOMENDACION, object, new Ack() {
                @Override
                public void call(Object... args) {
                    try {
                        JSONObject data = new JSONObject(args[0].toString());
                        if (data.has("en")) {
                            switch (data.getInt("en")) {
                                case 1:
                                    reproducirTextAudio.speak(data.getString("r"));
                                    break;
                                case 2:
                                    if (actividadMapa != null && !iscreateVideoDialogo) {
                                        posicion = 0;
                                        uriPath = Environment.getExternalStorageDirectory() + File.separator + "Video/" + data.getString("v") + ".mp4";
                                        actividadMapa.createVideoDialogo(uriPath);
                                    }
                                    break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public String uriPath;
    public int posicion;


    @Override

    public void configuracionServidorNew(boolean isEstado) {
        cambiarIpServer();
    }

    int estadoConfimacion = 0;

    public void consultarEstadoPedido(int idPedido) {
        if (mSocket != null) {
            mSocket.emit(VariablesGlobales.EMIT_CONSULTAR_PEDIDO_CONDUCTOR, idPedido, spobtener.getInt(VariablesGlobales.ID_USUARIO, 0), new Ack() {
                @Override
                public void call(Object... args) {
                    try {
                        JSONObject object = new JSONObject(args[0].toString());
                        estadoPedido = object.getInt("en");
                        solicitudSeleccionada.setEstadoPedido(object.getInt("en"));
                        guardarEstadoSolicitud();
                        if (object.has("confirmo")) {
                            estadoConfimacion = object.getInt("confirmo");
                        }
                        if (estadoPedido == 2) {
                            precioEnviado = true;
                            if (!isCronometroTiempo) {
                                contadorServicioTiempo();
                            }
                        }
                        estadoPedido();
                    } catch (JSONException e) {
                        e.getMessage();
                    }
                }
            });
        }
    }

    public void activarConexion() {
        if (mSocket != null) {
            if (!mSocket.connected()) {
                iniciarConexion();
            }
        }

    }

    public void activarEscuchas() {
        if (mSocket != null) {
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_NUEVA_SOLICITUD) && isEstadoBoton) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_NUEVA_SOLICITUD, onNuevaSolicitud);
            }
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_SOLICITUD)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_SOLICITUD, onAtendiendoSolicitud);
            }
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_KTAXI_DRIVER_ESCUCHA_MENSAJES)) {
                mSocket.on(VariablesGlobales.ESCUCHA_KTAXI_DRIVER_ESCUCHA_MENSAJES, onMensajes);
            }
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_POSIBLES_SOLICITUDES)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_POSIBLES_SOLICITUDES, onPosibleSolicitud);
            }
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_AUDIO_BROADCAST_POR_EMPRESA)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_AUDIO_BROADCAST_POR_EMPRESA, onAdioVehiculoPorUnidadEmpresa);
            }

            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_AUDIO_VEHICULO_POR_UNIDAD)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_AUDIO_VEHICULO_POR_UNIDAD, onChatBroadCastCallCenter);
            }
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_AUDIO_BROADCAST)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_AUDIO_BROADCAST, onAudioBroadCast);
            }
            if (!mSocket.hasListeners(Socket.EVENT_DISCONNECT)) {
                mSocket.on(Socket.EVENT_DISCONNECT, onDesconectar);
            }
        }
    }


    public void enviarAudioBroasCastEmpresa(String filename) {
        UtilStream.compressGzipFile(ChatActivity.path + filename, ChatActivity.path + "audioOut.gzip");
        byte[] data = UtilStream.fileToByteArray(ChatActivity.path + "audioOut.gzip");
        File file = new File(ChatActivity.path + "audioOut.gzip");
        file.delete();
        JSONObject jsonObject = new JSONObject();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setTipo(1);
        chatMessage.setId(2);
        chatMessage.setNameArchivo(filename);
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        chatMessage.setDate(hourdateFormat.format(new Date()));
        chatMessage.setMe(true);
        chatBroadCastEmpresa.add(chatMessage);
        try {
            jsonObject.put("n", spobtener.getString(VariablesGlobales.NOMBRES, "TEST"));
            jsonObject.put("iE", spobtener.getInt(VariablesGlobales.ID_EMPRESA, 0));
            jsonObject.put("iU", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            jsonObject.put("u", spobtener.getInt(VariablesGlobales.UNIDAD_VEHICULO, 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("enviar_audio_broadcast_por_empresa", jsonObject, data, new Ack() {
            @Override
            public void call(Object... args) {
                obtenerMensajesBroadCastEmpresa();
            }
        });
    }

    public void enviarAudioBroasCast(String filename) {
        UtilStream.compressGzipFile(ChatActivity.path + filename, ChatActivity.path + "audioOut.gzip");
        byte[] data = UtilStream.fileToByteArray(ChatActivity.path + "audioOut.gzip");
        File file = new File(ChatActivity.path + "audioOut.gzip");
        file.delete();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setTipo(1);
        chatMessage.setId(2);
        chatMessage.setNameArchivo(filename);
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        chatMessage.setDate(hourdateFormat.format(new Date()));
        chatMessage.setMe(true);
        chatBroadCast.add(chatMessage);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("n", spobtener.getString(VariablesGlobales.NOMBRES, "TEST"));
            jsonObject.put("iE", spobtener.getInt(VariablesGlobales.ID_EMPRESA, 0));
            jsonObject.put("iU", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            jsonObject.put("u", spobtener.getInt(VariablesGlobales.UNIDAD_VEHICULO, 0));
            jsonObject.put("iC", spobtener.getInt(VariablesGlobales.ID_CIUDAD, 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("enviar_audio_broadcast", jsonObject, data, new Ack() {
            @Override
            public void call(Object... args) {
                obtenerMensajesBroadCast();
            }
        });
    }


    public void enviarAudioCallCenter(String filename) {
        UtilStream.compressGzipFile(ChatActivity.path + filename, ChatActivity.path + "audioOut.gzip");
        byte[] data = UtilStream.fileToByteArray(ChatActivity.path + "audioOut.gzip");
        File file = new File(ChatActivity.path + "audioOut.gzip");
        file.delete();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setTipo(1);
        chatMessage.setId(2);
        chatMessage.setNameArchivo(filename);
        DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
        chatMessage.setDate(hourdateFormat.format(new Date()));
        chatMessage.setMe(true);
        chatCallCenter.add(chatMessage);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("n", spobtener.getString(VariablesGlobales.NOMBRES, "TEST"));
            jsonObject.put("iE", spobtener.getInt(VariablesGlobales.ID_EMPRESA, 0));
            jsonObject.put("iU", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            jsonObject.put("u", spobtener.getInt(VariablesGlobales.UNIDAD_VEHICULO, 0));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit("enviar_audio_call_center", jsonObject, data, new Ack() {
            @Override
            public void call(Object... args) {
                obtenerMensajesCallCenter();
            }
        });
    }


    int count = 0;
    private Emitter.Listener onChatBroadCastCallCenter = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
            if (dataConfig.length() > 0) {
                try {
                    JSONArray jsonArray = new JSONArray(dataConfig);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject objetoConfig = jsonArray.getJSONObject(i);
                        if (objetoConfig.has("id")) {
                            switch (objetoConfig.getInt("id")) {
                                case 10: //Configuracion de mensajeria con audios
                                    if (objetoConfig.getInt("h") == 1) {
                                        JSONObject jsonObject = null;
                                        try {
                                            jsonObject = new JSONObject(args[0].toString());
                                            String nombreAudio = nameAudio();
                                            UtilStream.byteArrayToFile((byte[]) args[1], ChatActivity.path + count + "audioIn.gzip");
                                            byte[] data = UtilStream.decompressGzip(UtilStream.fileToByteArray(ChatActivity.path + count + "audioIn.gzip"), getApplicationContext());
                                            File file = new File(ChatActivity.path + count + "audioIn.gzip");
                                            file.delete();
                                            if (data.length < 50) {
                                                return;
                                            }
                                            if (UtilStream.byteArrayToFile(data, ChatActivity.path + nombreAudio)) {
                                                escucharAudios(ChatActivity.path + nombreAudio);
                                                ChatMessage msg = new ChatMessage();
                                                msg.setTipo(1);
                                                msg.setId(1);
                                                msg.setMe(false);
                                                msg.setNameArchivo(nombreAudio);
                                                // msg.setMessage(message);
                                                if (jsonObject.has("n")) {
                                                    msg.setDate(jsonObject.getString("n") + "\n" + DateFormat.getDateTimeInstance().format(new Date()));
                                                } else {
                                                    DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                                                    msg.setDate(hourdateFormat.format(new Date()));
                                                }
                                                chatCallCenter.add(msg);
                                                obtenerMensajesCallCenter();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }
    };


    private Emitter.Listener onAdioVehiculoPorUnidadEmpresa = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
            if (dataConfig.length() > 0) {
                try {
                    JSONArray jsonArray = new JSONArray(dataConfig);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject objetoConfig = jsonArray.getJSONObject(i);
                        if (objetoConfig.has("id")) {
                            switch (objetoConfig.getInt("id")) {
                                case 10: //Configuracion de mensajeria con audios
                                    if (objetoConfig.getInt("h") == 1) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(args[0].toString());
                                            String nombreAudio = nameAudio();
                                            UtilStream.byteArrayToFile((byte[]) args[1], ChatActivity.path + count + "audioIn.gzip");
                                            byte[] data = UtilStream.decompressGzip(UtilStream.fileToByteArray(ChatActivity.path + count + "audioIn.gzip"), getApplicationContext());
                                            File file = new File(ChatActivity.path + count + "audioIn.gzip");
                                            file.delete();
                                            if (data.length < 50) {
                                                return;
                                            }
                                            if (UtilStream.byteArrayToFile(data, ChatActivity.path + nombreAudio)) {
                                                escucharAudios(ChatActivity.path + nombreAudio);
                                                ChatMessage msg = new ChatMessage();
                                                msg.setTipo(1);
                                                msg.setId(1);
                                                msg.setMe(false);
                                                msg.setNameArchivo(nombreAudio);
                                                // msg.setMessage(message);
                                                if (jsonObject.has("n")) {
                                                    msg.setDate(jsonObject.getString("n") + "\n" + DateFormat.getDateTimeInstance().format(new Date()));
                                                } else {
                                                    DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                                                    msg.setDate(hourdateFormat.format(new Date()));
                                                }
                                                chatBroadCastEmpresa.add(msg);
                                                obtenerMensajesBroadCastEmpresa();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                    break;
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };


    private Emitter.Listener onAudioBroadCast = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
            if (dataConfig.length() > 0) {
                try {
                    JSONArray jsonArray = new JSONArray(dataConfig);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject objetoConfig = jsonArray.getJSONObject(i);
                        if (objetoConfig.has("id")) {
                            switch (objetoConfig.getInt("id")) {
                                case 10: //Configuracion de mensajeria con audios
                                    if (objetoConfig.getInt("h") == 1) {
                                        JSONObject jsonObject = null;
                                        try {
                                            jsonObject = new JSONObject(args[0].toString());
                                            String nombreAudio = nameAudio();
                                            UtilStream.byteArrayToFile((byte[]) args[1], ChatActivity.path + count + "audioIn.gzip");
                                            byte[] data = UtilStream.decompressGzip(UtilStream.fileToByteArray(ChatActivity.path + count + "audioIn.gzip"), getApplicationContext());
                                            File file = new File(ChatActivity.path + count + "audioIn.gzip");
                                            file.delete();
                                            if (data.length < 50) {
                                                return;
                                            }
                                            if (UtilStream.byteArrayToFile(data, ChatActivity.path + nombreAudio)) {
                                                escucharAudios(ChatActivity.path + nombreAudio);
                                                ChatMessage msg = new ChatMessage();
                                                msg.setTipo(1);
                                                msg.setId(1);
                                                msg.setMe(false);
                                                msg.setNameArchivo(nombreAudio);
                                                // msg.setMessage(message);
                                                if (jsonObject.has("n")) {
                                                    msg.setDate(jsonObject.getString("n") + "\n" + DateFormat.getDateTimeInstance().format(new Date()));
                                                } else {
                                                    DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                                                    msg.setDate(hourdateFormat.format(new Date()));
                                                }
                                                chatBroadCast.add(msg);
                                                obtenerMensajesBroadCast();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                    break;
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    Map<Integer, Integer> tipoRastreoMap;

    public void validarTiemposRastreo(String data) {
        tipoRastreoMap.clear();
        try {
            JSONObject object = new JSONObject(data);
            if (object.has("t")) {
                JSONArray tipoRastreo = object.getJSONArray("t");
                for (int i = 0; i < tipoRastreo.length(); i++) {
                    tipoRastreoMap.put(tipoRastreo.getJSONObject(i).getInt("t"), tipoRastreo.getJSONObject(i).getInt("s") * 1000);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void escucharAudios(String nombre) {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM); // this is important.
            mediaPlayer.setDataSource(nombre);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isLogin = false, isTimeOut = true;
    private int contadorLogin = 0;

    public void iniciarSesion(final String usuario, final String contrasenia) {
        activarConexion();
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isLogin) {
                    if (mSocket != null) {
                        if (mSocket.connected()) {
                            JSONObject data = new JSONObject();
                            try {
                                data.put("usuario", usuario);
                                data.put("contrasenia", new MetodosValidacion().MD5(contrasenia));
                                data.put("idDispositivo", idAndroid);
                                data.put("idAplicativo", VariablesGlobales.NUM_ID_APLICATIVO);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            mSocket.emit(VariablesGlobales.EMIT_LOGEAR_KTAXI, data, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    datosLogeo = args[0].toString();
                                    if (actividadIniciarSesion != null) {
                                        actividadIniciarSesion.iniciarSesionServidor(datosLogeo);
                                    }
                                    cargarConfiguraciones(args[0].toString());
                                    validarTiemposRastreo(args[0].toString());
                                    isTimeOut = true;
                                }
                            });
                            isTimeOut = false;
                            ejecutarTimeOut();
                        } else {
                            if (utilidades.estaConectado(ServicioSockets.this)) {
                                if (utilidades.pingGoogle()) {
                                    if (contadorLogin <= 10) {
                                        try {
                                            Thread.sleep(1000);
                                            iniciarSesion(usuario, contrasenia);
                                            contadorLogin++;
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        contadorLogin = 0;
                                        enviarMensajeErrorLogin("Existe inconveniente al iniciar sesión, inténtelo nuevamente.");
                                    }
                                } else {
                                    enviarMensajeErrorLogin("No tiene conexión a internet.");
                                }
                            } else {
                                enviarMensajeErrorLogin("No tiene encendido Wifi o plan de internet.");
                            }
                        }
                    }
                } else {
                    enviarMensajeErrorLogin("Existe inconveniente al iniciar sesión, inténtelo nuevamente.");
                }
            }
        }).start();
    }

    public void ejecutarTimeOut() {
        try {
            Thread.sleep(30000);
            if (!isTimeOut) {
                enviarMensajeErrorLogin("Existe inconveniente al iniciar sesión, inténtelo nuevamente.");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enviarMensajeErrorLogin(String mensaje) {
        JSONObject objeto = new JSONObject();
        try {
            objeto.put(VariablesGlobales.MENSAJE, mensaje);
            if (actividadIniciarSesion != null) {
                actividadIniciarSesion.iniciarSesionServidor(objeto.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void loguearOperadorPorIdVehiculo(int idVehiculo, int idCiudad, int idEmpresa, int idUsuario) {
        if (mSocket != null) {
            mSocket.emit(VariablesGlobales.EMIT_LOGEAR_KTAXI_POR_VEHICULO, idVehiculo, idCiudad, idEmpresa, idUsuario, idAndroid, new Ack() {
                @Override
                public void call(Object... args) {
                    datosLogeo = args[0].toString();
                    if (actividadIniciarSesion != null) {
                        actividadIniciarSesion.iniciarSesionServidor(datosLogeo);
                    }
                    cargarConfiguraciones(args[0].toString());
                }
            });
        }
    }

    private String videos;
    private List<String> videosPorDescargar;

    public void cargarConfiguraciones(String datos) {
        ExtraLog.Log(TAG, "cargarConfiguraciones: " + datos);
        try {
            JSONObject object = new JSONObject(datos);
            if (object.has(VariablesGlobales.CNF)) {
                String data = object.getJSONArray(VariablesGlobales.CNF).toString();
                SharedPreferences.Editor editor = spParametrosConfiguracion.edit();
                editor.putString(VariablesGlobales.DATOS_CONFIGURACION, data);
                editor.putString(VariablesGlobales.DATOS_CONFIGURACION_GLOBAL, datos);
                editor.apply();
            }
            if (object.has("v")) {
                if (object.getString("v") != null) {
                    videos = object.getString("v");
                    if (!videos.isEmpty() && videosPorDescargar == null) {
                        String[] videosServer = videos.split(",");
                        videosPorDescargar = new ArrayList<>();
                        for (String aVideosServer : videosServer) {
                            if (!VideosPropaganda.isVideo(aVideosServer) || !isFile(aVideosServer)) {
                                videosPorDescargar.add(aVideosServer);
                                VideosPropaganda videosPropaganda = new VideosPropaganda();
                                videosPropaganda.setNombreVideo(aVideosServer);
                                if (!VideosPropaganda.isVideo(videosPropaganda.getNombreVideo())) {
                                    videosPropaganda.save();
                                }
                            }
                        }
                        boolean isDelete = true;
                        for (VideosPropaganda local : VideosPropaganda.listaVideos()) {
                            for (String server : videosServer) {
                                if (local.getNombreVideo().equals(server)) {
                                    isDelete = false;
                                }
                            }
                            if (isDelete) {
                                //eliminar de disco y de base
                                borrarVideosPulicitarios(local.getNombreVideo());
                            }
                            isDelete = true;
                        }
                        if (videosPorDescargar != null && videosPorDescargar.size() > 0) {
                            descargarVideo(videosPorDescargar.get(0));
                        } else {
                            videosPorDescargar = null;
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void cambiosEnUsuario(String datos) {
        try {
            JSONObject object = new JSONObject(datos);
            if (object.has("usuario")) {
                SharedPreferences.Editor localEditor = spobtener.edit();
                localEditor.putString("usuario", object.getJSONObject("usuario").getString("usuario"));
                localEditor.putInt("idUsuario", object.getJSONObject("usuario").getInt("idUsuario"));
                localEditor.putInt("idVehiculo", object.getJSONObject("usuario").getInt("idVehiculo"));
                localEditor.putInt("idEquipo", object.getJSONObject("usuario").getInt("idEquipo"));
                localEditor.putString("nombres", object.getJSONObject("usuario").getString("nombres"));
                localEditor.putString("apellidos", object.getJSONObject("usuario").getString("apellidos"));
                localEditor.putString("correo", object.getJSONObject("usuario").getString("correo"));
                localEditor.putString("celular", object.getJSONObject("usuario").getString("celular"));
                localEditor.putString("empresa", object.getJSONObject("usuario").getString("empresa"));
                localEditor.putString("placaVehiculo", object.getJSONObject("usuario").getString("placaVehiculo"));
                localEditor.putString("regMunVehiculo", object.getJSONObject("usuario").getString("regMunVehiculo"));
                localEditor.putString("marcaVehiculo", object.getJSONObject("usuario").getString("marcaVehiculo"));
                localEditor.putString("modeloVehiculo", object.getJSONObject("usuario").getString("modeloVehiculo"));
                localEditor.putInt("idCiudad", object.getJSONObject("usuario").getInt("idCiudad"));
                localEditor.putInt("idEmpresa", object.getJSONObject("usuario").getInt("idEmpresa"));
                localEditor.putInt("anioVehiculo", object.getJSONObject("usuario").getInt("anioVehiculo"));
                localEditor.putInt("unidadVehiculo", object.getJSONObject("usuario").getInt("unidadVehiculo"));
                localEditor.putString("imagenConductor", object.getJSONObject("usuario").getString("imagenConductor"));
                localEditor.apply();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public SharedPreferences getSpParametrosConfiguracion() {
        return spParametrosConfiguracion;
    }

    public void desconectarUserRemoto(int idVehiculo, int idCiudad, int idEmpresa, int idUsuario, String nombres) {
        if (mSocket != null) {
            mSocket.emit(VariablesGlobales.EMIT_DESCONECTAR_KTAXI, idVehiculo, idCiudad, idEmpresa, idUsuario, nombres, idAndroid, new Ack() {
                @Override
                public void call(Object... args) {
                    if (actividadIniciarSesion != null) {
                        actividadIniciarSesion.iniciarSesionServidor(args[0].toString());
                    }
                }
            });
        }
    }

    public void borrarCarreraSeleccionada() {
        if (solicitudSeleccionada != null) {
            solicitudSeleccionada = null;
        }
        chatHistory.clear();
        deleteFilesAudio();
    }

    private void deleteFilesAudio() {
        if (chatHistory != null) {
            if (chatHistory.size() > 0) {
                for (ChatMessage chatMessage : chatHistory) {
                    if (chatMessage.getTipo() == 1) {
                        File file = new File(ChatActivity.path + chatMessage.getNameArchivo());
                        file.delete();
                    }
                }
            }
        }
    }

    public void activaEnNuevaSolicitud() {
        if (mSocket != null) {
            isEstadoBoton = true;
            if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_EN_NUEVA_SOLICITUD)) {
                mSocket.on(VariablesGlobales.ESCUCHA_EN_NUEVA_SOLICITUD, onNuevaSolicitud);
            }
        }
    }

    public void desactivaEnNuevaSolicitud() {
        isEstadoBoton = false;
        if (mSocket != null) {
            mSocket.off(VariablesGlobales.ESCUCHA_EN_NUEVA_SOLICITUD, onNuevaSolicitud);
        }
    }


    public void sonarSolicitud(int dato) {
        switch (dato) {
            case 1:
                flujodemusica = soundPool.load(ServicioSockets.this, R.raw.silbido_taxi, 1);
                soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                    @Override
                    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                        soundPool.play(flujodemusica, 1, 1, 0, 0, 1);
                    }

                    @Override
                    protected void finalize() throws Throwable {
                        super.finalize();
                    }
                });
                break;
            case 2:
                flujoMensaje = soundPool.load(ServicioSockets.this, R.raw.sms1, 1);
                soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                    @Override
                    public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                        soundPool.play(flujoMensaje, 1, 1, 0, 0, 1);
                    }

                    @Override
                    protected void finalize() throws Throwable {
                        super.finalize();
                    }
                });
                break;
        }

    }

    private boolean isConsultaSolicitudes = true;

    public void consultarSolicitudesEntrantes() {
        if (isConsultaSolicitudes && isEstadoBoton) {
            isConsultaSolicitudes = false;
            if (mSocket != null) {
                if (latitud != 0 && longitud != 0) {
                    if (solicitudesEntrantes.size() == 0) {
                        mSocket.emit(VariablesGlobales.EMIT_CONSULTAR_SOLICITUDES_CERCANAS_VEHICULO,
                                spobtener.getInt(VariablesGlobales.ID_CIUDAD, 0),
                                spobtener.getInt(VariablesGlobales.ID_EMPRESA, 0),
                                spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0),
                                spobtener.getInt(VariablesGlobales.ID_USUARIO, 0), latitud, longitud, new Ack() {
                                    @Override
                                    public void call(Object... args) {
                                        if (isEstadoBoton) {
                                            try {
                                                JSONObject objetData = new JSONObject(args[0].toString());
                                                JSONArray Arraydata = objetData.getJSONArray("listSolicitudes");
                                                if (Arraydata.length() > 0 && solicitudesEntrantes.size() != Arraydata.length()) {
                                                    solicitudesEntrantes.clear();
                                                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                                    v.vibrate(2000);
                                                    if (isBanderaLlamada) {
                                                        sonarSolicitud(1);
                                                    }
                                                }
                                                for (int i = 0; i < Arraydata.length(); i++) {
                                                    JSONObject data = Arraydata.getJSONObject(i);
                                                    parcearSolicitud(data);
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            if (mSocket != null) {
                                                mSocket.emit("consultar_pedidos_pendientes_vehiculo", spobtener.getInt(VariablesGlobales.ID_CIUDAD, 0), spobtener.getInt(VariablesGlobales.ID_USUARIO, 0), spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0), new Ack() {
                                                    @Override
                                                    public void call(Object... args) {
                                                        if (isEstadoBoton) {
                                                            try {
                                                                JSONObject objetData = new JSONObject(args[0].toString());
                                                                if (objetData.has("en")) {
                                                                    switch (objetData.getInt("en")) {
                                                                        case 1:
                                                                            JSONArray Arraydata = objetData.getJSONArray("lP");
                                                                            if (Arraydata.length() > 0 && solicitudesEntrantes.size() != Arraydata.length()) {
                                                                                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                                                                                v.vibrate(2000);
                                                                                if (isBanderaLlamada) {
                                                                                    sonarSolicitud(1);
                                                                                }
                                                                            }
                                                                            for (int i = 0; i < Arraydata.length(); i++) {
                                                                                JSONObject data = Arraydata.getJSONObject(i);
                                                                                parcearPedido(data);
                                                                            }
                                                                            break;
                                                                        case -1:
                                                                            break;
                                                                        default:
                                                                            break;
                                                                    }
                                                                }
                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });
                    }
                }
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        isConsultaSolicitudes = true;
                    }
                }
            }).start();
        }
    }

    public void notificarCambioEnServidor() {
        if (null != actividadMapa) {
            actividadMapa.cambiarEstado(isServerOnline);
        } else if (null != actividadLista) {
            actividadLista.cambiarEstado(isServerOnline);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        activarEscuchas();
        return mBinder;
    }

    public boolean enCarrera() {
        return solicitudSeleccionada != null;
    }

    public void cerrarSesion() {
        SharedPreferences.Editor editor = spParametrosConfiguracion.edit();
        editor.putString(VariablesGlobales.DATOS_CONFIGURACION, "");
        editor.apply();
        limpiarListaSolicitudes();
        if (mSocket != null) {
            mSocket.disconnect();
        }
        borrarData();
        borrarCuantosLocal();
    }

    public void borrarData() {
        SharedPreferences.Editor spHistorial = getSharedPreferences(HistorialActivity.NOMBRE_PREFERENCIAS, Context.MODE_PRIVATE).edit();
        spHistorial.putInt("cuantosLocal", 0);
        spHistorial.apply();
        SharedPreferences.Editor spHistorialCompras = getSharedPreferences(HistorialComprasActivity.NOMBRE_PREFERENCIAS, Context.MODE_PRIVATE).edit();
        spHistorialCompras.putInt("cuantosLocalCompras", 0);
        spHistorialCompras.apply();
        CalendarHistorial.deleteAll();
        ItemHistorialSolicitud.deleteAll();
        ItemHistorialCompras.deleteAll();
    }

    private void borrarCuantosLocal() {
        SharedPreferences.Editor spHistorial = getSharedPreferences(HistorialActivity.NOMBRE_PREFERENCIAS, Context.MODE_PRIVATE).edit();
        spHistorial.putInt("cuantosLocal", 0);
        spHistorial.apply();
    }

    public void salirSistema() {
        apagaEscuchas();
        if (mSocket != null) mSocket.disconnect();
        stopService(new Intent(this, TaximetroService.class));
        stopForeground(true);
        stopSelf();
    }

    private UrlKtaxiConductor urlKtaxiConductor;

    private Sensor s;
    private SensorManager sensorM;

    public void activarSensorProximidad() {
        sensorM = (SensorManager) getSystemService(SENSOR_SERVICE);
        if (sensorM == null) {
            ExtraLog.Log(TAG, "Proximity sensor not available.");
            return;
        }
        s = sensorM.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        sensorM.registerListener(this, s, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public boolean isVibrarSonar = false;

    @Override
    public void onSensorChanged(SensorEvent event) {
        float valor = Float.parseFloat(String.valueOf(event.values[0]));
        if (valor <= 0.0) {
            isVibrarSonar = true;
        } else {
            isVibrarSonar = false;
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        registerReciver();
        isRunService = true;
        utilidades = new Utilidades();
        reproducirTextAudio = new ReproducirTextAudio(this);
        spobtener = this.getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        spParametrosConfiguracion = this.getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        spSolicitud = getSharedPreferences("estado_solicitud", Context.MODE_PRIVATE);
        configuracionApp = getSharedPreferences(VariablesGlobales.CONFIGURACION_APP, Context.MODE_PRIVATE);
        spEstadobtn = getSharedPreferences(VariablesGlobales.CONFIG_ESTADO_BTN, Context.MODE_PRIVATE);
        solicitudesEntrantes = new ArrayList<>();
        soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        tipoRastreoMap = new HashMap<>();
        chatHistory = new ArrayList<>();
        chatCallCenter = new ArrayList<>();
        chatBroadCastEmpresa = new ArrayList<>();
        chatBroadCast = new ArrayList<>();
        isServerOnline = false;
        idAndroid = utilidades.getImei(this);
        fechaHoraConexion = spobtener.getString("fechaHoraConexion", "");
        gps = new Gps(getApplicationContext());
        gps.setLocationListener(this);
        if (rastreoHandle == null) {
            actualizarRastreoTaxi();
        }
        urlKtaxiConductor = new UrlKtaxiConductor();
        if (mSocket == null) {
            iniciarConexion();
        }
        activarSensorProximidad();
    }

    public void setearInicio() {
        SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
        ConfiguracionServidor configuracionServidor = ConfiguracionServidor.createConfiguracionServidor(spConfigServerNew.getString(VariablesGlobales.CONFIG_IP_NEW, VariablesGlobales.CONFIGURACION_INICIAL));
        configuracionServidor.setIpUsada(VariablesGlobales.IP_SERVICIOS_SOCKET);
        VariablesGlobales.setIpServer(spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true), configuracionServidor.getIpUsada());
        IP_SERVIDOR = VariablesGlobales.IP_SERVICIOS_SOCKET;
    }

    public void cambiarIpServer() {
        SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
        ConfiguracionServidor configuracionServidor = ConfiguracionServidor.createConfiguracionServidor(spConfigServerNew.getString(VariablesGlobales.CONFIG_IP_NEW, VariablesGlobales.CONFIGURACION_INICIAL));
        VariablesGlobales.setIpServer(spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true), configuracionServidor.getIpUsada());
        IP_SERVIDOR = VariablesGlobales.IP_SERVICIOS_SOCKET;
        if (mSocket != null) {
            mSocket.disconnect();
            mSocket.close();
            mSocket = null;
        }
        iniciarConexion();
    }

    public void apagaEscuchas() {
        if (mSocket != null) {
            mSocket.off(VariablesGlobales.ESCUCHA_EN_NUEVA_SOLICITUD, onNuevaSolicitud);
            mSocket.off(VariablesGlobales.ESCUCHA_EN_SOLICITUD, onAtendiendoSolicitud);
            mSocket.off(VariablesGlobales.ESCUCHA_EN_POSIBLES_SOLICITUDES, onPosibleSolicitud);
        }
    }


    @Override
    public void onDestroy() {
        if (mSocket != null) {
            apagaEscuchas();
            mSocket.disconnect();
            mSocket = null;
        }
        if (rastreoHandle != null) {
            rastreoHandle.cancel(true);
        }
        gps.onstop();
        if (gps != null) {
            gps = null;
        }
        stopForeground(true);
        unregisterReceiver(wfs);
        stopSelf();
        reproducirTextAudio.terminarProceso();
        isRunService = false;
        super.onDestroy();
    }

    private JSONObject objetoConDatosEnvio(Solicitud solicitud, int tiempo, double precio, int estado, double costo) {
        JSONObject object = new JSONObject();
        spobtener = this.getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        try {
            if (solicitud.getIdPedido() != 0) {
                object.put(VariablesGlobales.ID_PEDIDO, solicitudSeleccionada.getIdPedido());
            } else {
                object.put(VariablesGlobales.ID_SOLICITUD, solicitudSeleccionada.getIdSolicitud());
            }
            object.put(VariablesGlobales.ATENDIDO_DESDE, 1);
            object.put(VariablesGlobales.ID_EMPRESA, spobtener.getInt(VariablesGlobales.ID_EMPRESA, 0));
            object.put(VariablesGlobales.ID_VEHICULO, spobtener.getInt("idVehiculo", 0));
            object.put(VariablesGlobales.EMPRESA, spobtener.getString(VariablesGlobales.EMPRESA, ""));
            object.put(VariablesGlobales.UNIDAD, spobtener.getInt(VariablesGlobales.UNIDAD_VEHICULO, 0));
            object.put(VariablesGlobales.PLACA, spobtener.getString(VariablesGlobales.PLACA_VEHICULO, ""));
            object.put(VariablesGlobales.REG_MUNICIPAL, spobtener.getString(VariablesGlobales.REG_MUN_VEHICULO, ""));
            object.put(VariablesGlobales.NOMBRES, spobtener.getString(VariablesGlobales.NOMBRES, ""));
            object.put(VariablesGlobales.APELLIDOS, spobtener.getString(VariablesGlobales.APELLIDOS, ""));
            object.put(VariablesGlobales.TELEFONO, spobtener.getString(VariablesGlobales.CELULAR, ""));
            object.put(VariablesGlobales.IMAGEN, spobtener.getString(VariablesGlobales.IMAGEN_CONDUCTOR, ""));
            object.put("costo", costo);
            object.put("distancia", solicitudSeleccionada.getDistancia());
            object.put("c", costo);
            switch (estado) {
                case 4:
                    object.put(VariablesGlobales.ESTADO, 4);
                    break;
                case 2:
                    object.put(VariablesGlobales.ESTADO, 2);
                    break;
                case 5:
                    object.put(VariablesGlobales.ESTADO, 5);
                    break;
                case 6:
                    object.put(VariablesGlobales.ESTADO, 6);
                    break;
            }
            if (tiempo != 0) {
                object.put(VariablesGlobales.TIEMPO, tiempo);
            } else {
                object.put("precio", precio);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }


    private MensajeDeuda mensajeDeuda;

    public MensajeDeuda getMensajeDeuda() {
        return mensajeDeuda;
    }

    public void setMensajeDeudaAbordo(MensajeDeuda mensajeDeudaAbordo) {
        this.mensajeDeuda = mensajeDeudaAbordo;
    }

    /**
     * Atender solicitud
     *
     * @param tiempo
     * @param costo
     */
    public void enviarTiempo(final int tiempo, double costo) {
        if (solicitudSeleccionada != null) {
            if (!isCronometroTiempo) {
                contadorServicioTiempo();
                isCronometroTiempo = true;
            }
            limpiarLista(solicitudSeleccionada.getIdSolicitud());
            if (mSocket != null) {
                estadoSolicitud = 3;
                solicitudSeleccionada.setEstadoSolicitud(3);
                guardarEstadoSolicitud();
                solicitudSeleccionada.setTiempo(tiempo);
                solicitudSeleccionada.setCostoCarrera(costo);
                jSonSolicitud.add(solicitudSeleccionada);
                ExtraLog.Log(TAG, "enviarTiempo: " + "se hace el emit" + jSonSolicitud.toString());
                mSocket.emit(VariablesGlobales.EMIT_ATENDER_SOLICITUD, solicitudSeleccionada.getUsername(), objetoConDatosEnvio(solicitudSeleccionada, tiempo, 0, 4, costo), spobtener.getInt(VariablesGlobales.ID_USUARIO, 0), new Ack() {
                    @Override
                    public void call(Object... args) {
                        mensajeDeuda = MensajeDeuda.createMensajeDeuda(args[0].toString());
                        tiempoEnviado = true;
                        tiempoEnvio = tiempo;
                        jSonSolicitud.clear();
                        if (solicitudSeleccionada != null) {
                            if (solicitudSeleccionada.getDatosCancelarSolicitud().isCancelar()) {
                                JSONObject object = new JSONObject();
                                try {
                                    object.put("idSolicitud", solicitudSeleccionada.getIdSolicitud());
                                    clienteAceptoElTiempo(object);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        try {
                            JSONObject data = new JSONObject(args[0].toString());
                            if (data.has(VariablesGlobales.ESTADO) && data.getInt(VariablesGlobales.ESTADO) == -1) {
                                borrarPreferencia();
                                isCronometroTiempo = false;
                                estadoSolicitud = 2;
                                if (solicitudSeleccionada != null) {
                                    solicitudSeleccionada.setEstadoSolicitud(2);
                                }
                                guardarEstadoSolicitud();
                                resetIdEnvioCalificacion();
                                if (actividadMapa != null) {
                                    actividadMapa.solicitudAtendida(data.getString("m"));
                                }
                                if (actividadLista != null) {
                                    actividadLista.solicitudAtendida(data.getString("m"));
                                }
                            } else {
                                if (mensajeDeuda != null && mensajeDeuda.getL() != null && mensajeDeuda.getL().getP() != null) {
                                    if (mensajeDeuda.getL().getP().getRP() == 1 && mensajeDeuda.getL().getP().getMs() != null) {
                                        reproducirTextAudio.speak(mensajeDeuda.getL().getP().getMs());
                                    }
                                    if (actividadLista != null) {
                                        if (mensajeDeuda.getL().getP().getNP() == 2 || mensajeDeuda.getL().getP().getNP() == 3) {
                                            actividadLista.solicitudAtendidaACK(mensajeDeuda);
                                        }
                                    }
                                    if (actividadMapa != null) {
                                        if (mensajeDeuda.getL().getP().getNP() == 2 || mensajeDeuda.getL().getP().getNP() == 3) {
                                            actividadMapa.solicitudAtendidaACK(mensajeDeuda);
                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });


            }
        } else {
            borrarPreferencia();
            resetIdEnvioCalificacion();
            if (actividadMapa != null) {
                actividadMapa.clienteAceptoTiempo(false, null, getString(R.string.solicitud_atendida));
            }
            if (actividadLista != null) {
                actividadLista.clienteAceptoTiempo(false, null, getString(R.string.solicitud_atendida));
            }
        }
    }

    public void enviarAceptarPedido(final double precio) {
        if (solicitudSeleccionada != null) {
            contadorServicioTiempo();
            limpiarLista(solicitudSeleccionada.getIdSolicitud());
            if (mSocket != null) {
                estadoPedido = 2;
                solicitudSeleccionada.setEstadoPedido(2);
                guardarEstadoSolicitud();
                solicitudSeleccionada.setPrecioPedido(precio);
                jSonSolicitud.add(solicitudSeleccionada);
                mSocket.emit("atender_pedido", solicitudSeleccionada.getUsername(), objetoConDatosEnvio(solicitudSeleccionada, 0, precio, 2, 0), spobtener.getInt(VariablesGlobales.ID_USUARIO, 0), new Ack() {
                    @Override
                    public void call(Object... args) {
                        ExtraLog.Log(TAG, "call: " + args[0].toString());
                        // {"estado":1,"mensaje":"Pedido atendido correctamente."}
                        mensajeDeuda = MensajeDeuda.createMensajeDeuda(args[0].toString());
                        jSonSolicitud.clear();
                        try {
                            JSONObject data = new JSONObject(args[0].toString());
                            if (data.getInt(VariablesGlobales.ESTADO) == -1) {
                                isCronometroTiempo = false;
                                if (actividadMapa != null) {
                                    actividadMapa.solicitudAtendida(data.has("mensaje") ? data.getString("mensaje") : getString(R.string.solicitud_atendida_pedido));
                                }
                                if (actividadLista != null) {
                                    actividadLista.solicitudAtendida(data.has("mensaje") ? data.getString("mensaje") : getString(R.string.solicitud_atendida_pedido));
                                }
                            } else {
                                precioEnviado = true;
                                if (mensajeDeuda != null && mensajeDeuda.getL() != null && mensajeDeuda.getL().getP() != null) {
                                    if (mensajeDeuda.getL().getP().getRP() == 1 && mensajeDeuda.getL().getP().getMs() != null) {
                                        reproducirTextAudio.speak(mensajeDeuda.getL().getP().getMs());
                                    }
                                    if (actividadLista != null) {
                                        if (mensajeDeuda.getL().getP().getNP() == 2 || mensajeDeuda.getL().getP().getNP() == 3) {
                                            actividadLista.solicitudAtendidaACK(mensajeDeuda);
                                        }
                                    }
                                    if (actividadMapa != null) {
                                        if (mensajeDeuda.getL().getP().getNP() == 2 || mensajeDeuda.getL().getP().getNP() == 3) {
                                            actividadMapa.solicitudAtendidaACK(mensajeDeuda);
                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                });
            }


        } else {
            solicitudSeleccionada = null;
            borrarPreferencia();
            estadoBoton = 0;
            if (actividadMapa != null) {
                actividadMapa.cambiarEstadoLibre();
                actividadMapa.clienteAceptoTiempo(false, null, getString(R.string.solicitud_atendida_pedido));
            }
            if (actividadLista != null) {
                actividadLista.clienteAceptoTiempo(false, null, getString(R.string.solicitud_atendida_pedido));
            }
        }
    }

    private int estadoComprandoSemaforo = 0; // 1 enviado //2 rojo  espera de confirmacion//3 verde confirmado
    private int estadoCompradoSemaforo = 0; // 1 enviado //2 rojo  espera de confirmacion//3 verde confirmado

    public void enviarComprandoPedido() {
        if (solicitudSeleccionada != null) {
            solicitudSeleccionada.setEstadoPedido(4);
            estadoPedido = 4;
            guardarEstadoSolicitud();
            if (estadoComprandoSemaforo == 0) {
                estadoComprandoSemaforo = 1;
                solicitudSeleccionada.setEstadoComprandoSemaforo(1);
                guardarEstadoSolicitud();
            }
            jSonSolicitud.add(solicitudSeleccionada);
            if (mSocket != null) {
                mSocket.emit("comprar_pedido", solicitudSeleccionada.getUsername(), objetoConDatosEnvio(solicitudSeleccionada, 0, solicitudSeleccionada.getPrecioPedido(), 4, 0), spobtener.getInt(VariablesGlobales.ID_USUARIO, 0), new Ack() {
                    @Override
                    public void call(Object... args) {
                        jSonSolicitud.clear();
                        try {
                            JSONObject object = new JSONObject(args[0].toString());
                            switch (object.getInt("estado")) {
                                case -1:
                                    if (actividadMapa != null) {
                                        actividadMapa.clienteAceptoTiempo(false, null, "El pedido ha sido cancelado");
                                    }
                                    break;
                                //{"estado":3,"mensaje":"Pedido comprado previamente. Al cliente se le entrego tu mensaje."}
                                case 3:
                                    if (actividadMapa != null) {
                                        estadoComprandoSemaforo = 3;
                                        solicitudSeleccionada.setEstadoComprandoSemaforo(3);
                                        guardarEstadoSolicitud();
                                        actividadMapa.cambiarEstadoComprandoUsuario();
                                    }
                                    break;
                                default:
                                    if (estadoComprandoSemaforo == 1) {
                                        estadoComprandoSemaforo = 2;
                                        solicitudSeleccionada.setEstadoComprandoSemaforo(2);
                                        guardarEstadoSolicitud();
                                    }
                                    if (actividadMapa != null) {
                                        actividadMapa.cambiarEstadoComprando();
                                        isEnviarComprarPedido = true;
                                    }
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_KTAXI_DRIVER_ESCUCHA_MENSAJES)) {
                            mSocket.on(VariablesGlobales.ESCUCHA_KTAXI_DRIVER_ESCUCHA_MENSAJES, onMensajes);
                        }
                    }

                });
            }
        } else {
            if (actividadMapa != null) {
                actividadMapa.clienteAceptoTiempo(false, null, getString(R.string.solicitud_atendida_pedido));
                actividadMapa.cambiarEstadoLibre();
            } else if (actividadLista != null) {
                actividadLista.clienteAceptoTiempo(false, null, getString(R.string.solicitud_atendida_pedido));
            }
        }
    }

    public void enviarPedidoComprado(double costo) {
        if (solicitudSeleccionada != null) {
            solicitudSeleccionada.setEstadoPedido(5);
            guardarEstadoSolicitud();
            estadoPedido = 5;
            if (estadoCompradoSemaforo == 0) {
                estadoCompradoSemaforo = 1;
                solicitudSeleccionada.setEstadoCompradoSemaforo(1);
                guardarEstadoSolicitud();
            }
            jSonSolicitud.add(solicitudSeleccionada);
            if (mSocket != null) {
                mSocket.emit("llevando_pedido", solicitudSeleccionada.getUsername(), objetoConDatosEnvio(solicitudSeleccionada, 0, solicitudSeleccionada.getPrecioPedido(), 5, costo), spobtener.getInt(VariablesGlobales.ID_USUARIO, 0), new Ack() {
                    @Override
                    public void call(Object... args) {
                        precioEnviado = true;
                        jSonSolicitud.clear();
                        try {
                            JSONObject object = new JSONObject(args[0].toString());
                            switch (object.getInt("estado")) {
                                case -1:
                                    if (actividadMapa != null) {
                                        actividadMapa.clienteAceptoTiempo(false, null, "El pedido ha sido cancelado");
                                    }
                                    break;
                                case 3:
                                    if (actividadMapa != null) {
                                        estadoCompradoSemaforo = 3;
                                        solicitudSeleccionada.setEstadoCompradoSemaforo(3);
                                        guardarEstadoSolicitud();
                                        actividadMapa.cambiarEstadoCompradoUsuario();
                                    }
                                    break;
                                default:
                                    if (actividadMapa != null && solicitudSeleccionada != null) {
                                        actividadMapa.cambiarEstadoComprado();
                                        solicitudSeleccionada.setBotonAbordo(true);
                                        if (estadoCompradoSemaforo == 1) {
                                            estadoCompradoSemaforo = 2;
                                            solicitudSeleccionada.setEstadoCompradoSemaforo(2);
                                            guardarEstadoSolicitud();
                                        }
                                    }
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                });
            }
        } else {
            if (actividadMapa != null) {
                actividadMapa.clienteAceptoTiempo(false, null, getString(R.string.solicitud_atendida_pedido));
                actividadMapa.cambiarEstadoLibre();
            }
            if (actividadLista != null) {
                actividadMapa.clienteAceptoTiempo(false, null, getString(R.string.solicitud_atendida_pedido));

            }
        }
    }

    public void enviarEvento(Location location, int tipo, int id, int iPS) {
        ExtraLog.Log(TAG, "enviarEvento: ENTRO A ENVIAR EL EVENTO: " + tipo + "idSolictud: " + id);
        if (location != null) {
            JSONObject object = new JSONObject();
            try {
                // tipo 1 panico
                // tipo 2 posibles solicitudes
                // tipo 3 ack de llego solicitud
                // tipo 4 recivo pedido
                // tipo 5 entregado de posible solicitud
                object.put("usuario", spobtener.getString("nombres", "") + " " + spobtener.getString("apellidos", ""));
                object.put("empresa", spobtener.getString("empresa", ""));
                object.put("numeroUnidad", spobtener.getInt("unidadVehiculo", 0));
                object.put("idAplicativo", VariablesGlobales.NUM_ID_APLICATIVO);
                object.put("t", tipo);
                object.put("iPS", iPS); //Id Posible solicitud
                if (location == null) {
                    object.put("lt", 0);
                    object.put("lg", 0);
                } else {
                    object.put("lt", location.getLatitude());
                    object.put("lg", location.getLongitude());

                }
                if (tipo == 4) {
                    object.put("iP", id);
                } else {
                    object.put("iS", id);
                }
                if (mSocket != null) {

                    mSocket.emit("notificar_evento",
                            spobtener.getInt(VariablesGlobales.ID_USUARIO, 0),
                            spobtener.getInt(VariablesGlobales.ID_CIUDAD, 0),
                            spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0),
                            object);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void cancelarSolicitud(int idEventualidad, int estado, Solicitud solicitud, boolean isCalificar, int calificacion,
                                  String comentario) {
        isBanderaRun = false;
        if (solicitud != null) {
            solicitudSeleccionada = solicitud;
        }
        final JSONObject object = new JSONObject();
        if (solicitudSeleccionada != null) {
            try {
                object.put(VariablesGlobales.ID_SOLICITUD, solicitudSeleccionada.getIdSolicitud());
                object.put(VariablesGlobales.ESTADO, estado);
                object.put(VariablesGlobales.ESTADO, estado);
                object.put(VariablesGlobales.ID_EVENTUALIDAD, idEventualidad);
                object.put(VariablesGlobales.ID_USUARIO, spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
                solicitudSeleccionada.setEstadoSolicitud(estado);
                guardarEstadoSolicitud();
                solicitudSeleccionada.setEventualidadCancelacion(idEventualidad);
                isEstadoAbordo = false;
                jSonSolicitud.add(solicitudSeleccionada);
                if (mSocket != null) {
                    mSocket.emit(VariablesGlobales.EMIT_CANCELAR_SOLICITUD_KTAXI, solicitudSeleccionada.getUsername(), object, new Ack() {
                        @Override
                        public void call(Object... args) {
                            tiempoEnvio = 0;
                            try {
                                jSonSolicitud.clear();
                                JSONObject objeto = new JSONObject(args[0].toString());
                                if (objeto.getInt(VariablesGlobales.ESTADO) == 1) {
                                    if (!configuracionApp.getBoolean(VariablesGlobales.ASISTENTE_VOZ, true)) {
                                        if (isBanderaLlamada) {
                                            sonarSolicitud(3);
                                        }
                                    } else {
                                        if (isBanderaLlamada) {
                                            reproducirTextAudio.speak(objeto.getString(VariablesGlobales.MENSAJE));
                                        }
                                    }
                                    if (actividadMapa != null) {
                                        actividadMapa.mensajeAlertaCancelar(objeto.getString(VariablesGlobales.MENSAJE));
                                    }
                                    if (actividadLista != null) {
                                        actividadLista.mensajeAlertaCancelar(objeto.getString(VariablesGlobales.MENSAJE));
                                    }
                                }
                                prenderEscuchaPosibleSolicitud();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                if (isCalificar) {
                    calificaConductorSolicitud(calificacion, comentario, solicitudSeleccionada.getIdSolicitud());
                }
                setClienteAceptoTiempo(false);
                isCronometroTiempo = false;
                borrarPreferencia();
                borrarCarreraSeleccionada();
                resetIdEnvioCalificacion();
                estadoSolicitud = 0;
                estadoPedido = 0;
                chatHistory.clear();
                if (actividadMapa != null) {
                    actividadMapa.borraCarreraAlCancelar();
                }
                solicitudSeleccionada = null;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void cancelarPedido(int razon, int estado, Solicitud solicitud, boolean isCalificar, String comentario) {
        if (solicitud != null) {
            solicitudSeleccionada = solicitud;
        }
        final JSONObject object = new JSONObject();
        if (solicitudSeleccionada != null) {
            try {
                object.put(VariablesGlobales.ID_PEDIDO, solicitudSeleccionada.getIdPedido());
                object.put(VariablesGlobales.ESTADO, estado);
                object.put("razon", razon);
                object.put(VariablesGlobales.ID_USUARIO, spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
                solicitudSeleccionada.setEstadoPedido(estado);
                guardarEstadoSolicitud();
                solicitudSeleccionada.setEventualidadCancelacion(razon);
                isEstadoAbordo = false;
                isEnviarComprarPedido = false;
                isEnviarllevandoPedido = false;
                jSonSolicitud.add(solicitudSeleccionada);
                if (mSocket != null) {
                    mSocket.emit(VariablesGlobales.EMIT_CANCELAR_PEDIDO_KTAXI,
                            solicitudSeleccionada.getUsername(), object, spobtener.getInt(VariablesGlobales.ID_USUARIO, 0), new Ack() {
                                @Override
                                public void call(Object... args) {
                                    tiempoEnvio = 0;
                                    try {
                                        jSonSolicitud.clear();
                                        JSONObject objeto = new JSONObject(args[0].toString());
                                        if (objeto.getInt(VariablesGlobales.ESTADO) == 1) {
                                            if (isBanderaLlamada) {
                                                sonarSolicitud(3);
                                            }
                                            if (actividadMapa != null) {
                                                actividadMapa.mensajeAlertaCancelar(objeto.getString(VariablesGlobales.MENSAJE));
                                            }
                                            if (actividadLista != null) {
                                                actividadLista.mensajeAlertaCancelar(objeto.getString(VariablesGlobales.MENSAJE));
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    solicitudSeleccionada = null;
                                    borrarPreferencia();
                                    if (actividadMapa != null) {
                                        actividadMapa.borraCarreraAlCancelar();
                                    }
                                }
                            });
                }
                if (isCalificar) {
                    calificaConductorPedido(0, comentario, solicitudSeleccionada.getIdPedido());
                }
                setClienteAceptoTiempo(false);
                isCronometroTiempo = false;
                estadoSolicitud = 0;
                estadoPedido = 0;
                borrarPreferencia();
                borrarCarreraSeleccionada();
                resetIdEnvioCalificacion();
                chatHistory.clear();
                solicitudSeleccionada = null;

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void enviarMensaje(String aviso) {
        if (mSocket != null) {
            if (solicitudSeleccionada != null) {
                mSocket.emit(VariablesGlobales.EMIT_ENVIAR_AVISO, solicitudSeleccionada.getIdSolicitud(), solicitudSeleccionada.getUsername(), aviso, spobtener.getInt(VariablesGlobales.ID_USUARIO, 0), new Ack() {
                    @Override
                    public void call(Object... args) {
                        ExtraLog.Log("ServicioSocket", "call: " + args[0].toString());
                    }
                });
            }
        }
    }

    public void enviarCache() {
        for (Solicitud s : jSonSolicitud) {
            if (s.getIdPedido() != 0) {
                switch (s.getEstadoPedido()) {
                    case 2:
                        enviarAceptarPedido(s.getPrecioPedido());
                        break;
                    case 4:
                        enviarComprandoPedido();
                        break;
                    case 5:
                        enviarPedidoComprado(obtenerPrecioPedido());
                        break;
                    case 6:
                        enviarPedidoEntregado(s);
                        break;
                    case 7:
                        cancelarPedido(s.getEventualidadCancelacion(), s.getEstadoPedido(), s, false, null);
                        break;

                }
            } else {
                switch (s.getEstadoSolicitud()) {
                    case 3:
                        enviarTiempo(s.getTiempo(), s.getCostoCarrera());
                        break;
                    case 9:
                        enviarAbordo(s);
                        break;
                    case 7:
                        cancelarSolicitud(s.getEventualidadCancelacion(), s.getEstadoSolicitud(), s, false, 0, null);
                        break;

                }
            }

        }
    }

    public void enviarAbordo(Solicitud solicitudSelectec) {
        isBanderaRun = false;
        solicitudSeleccionada = solicitudSelectec;
        JSONObject object = new JSONObject();
        if (mSocket != null) {
            if (solicitudSeleccionada != null) {
                if (solicitudSeleccionada.getIdSolicitud() != 0) {
                    try {
                        solicitudSeleccionada.setEstadoSolicitud(9);
                        guardarEstadoSolicitud();
                        solicitudSeleccionada.setAbordadoPor(5);
                        jSonSolicitud.add(solicitudSeleccionada);
                        object.put(VariablesGlobales.ID_SOLICITUD, solicitudSeleccionada.getIdSolicitud());
                        object.put(VariablesGlobales.ESTADO, 9);
                        object.put(VariablesGlobales.ABORDADO_POR, 5);
                        if (mSocket != null) {
                            mSocket.emit(VariablesGlobales.EMIT_ABORDAR_VEHICULO, solicitudSeleccionada.getUsername(), object, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    try {
                                        JSONObject data = new JSONObject(args[0].toString());
                                        if (data.has("en")) {
                                            if (data.getInt("en") == 1) {
                                                if (data.has("m")) {
                                                    reproducirTextAudio.speak(data.getString("m"));
                                                }
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    jSonSolicitud.clear();
                                    prenderEscuchaPosibleSolicitud();
                                }
                            });
                        }
                        solicitudSeleccionada = null;
                        borrarPreferencia();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    public void enviarPedidoEntregado(Solicitud solicitudSelectec) {
        isBanderaRun = false;
        solicitudSeleccionada = solicitudSelectec;
        if (solicitudSeleccionada != null) {
            solicitudSeleccionada.setEstadoPedido(6);
            guardarEstadoSolicitud();
            jSonSolicitud.add(solicitudSeleccionada);
            if (mSocket != null) {
                mSocket.emit("entregar_pedido", solicitudSeleccionada.getUsername(), objetoConDatosEnvio(solicitudSeleccionada, 0, 0, 6, 0), spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0), new Ack() {
                    @Override
                    public void call(Object... args) {
                        jSonSolicitud.clear();
                    }

                });
            }
        } else {
            if (actividadMapa != null) {
                actividadMapa.clienteAceptoTiempo(false, null, getString(R.string.solicitud_atendida_pedido));
                actividadMapa.cambiarEstadoLibre();
            }
            if (actividadLista != null) {
                actividadLista.clienteAceptoTiempo(false, null, getString(R.string.solicitud_atendida_pedido));
            }
        }
    }

    public void enviarChat(String mensaje) {
        if (solicitudSeleccionada != null) {
            if (solicitudSeleccionada.getIdPedido() == 0) {
                ChatMessage msg = new ChatMessage();
                msg.setId(122);
                msg.setMe(true);
                msg.setMessage(mensaje);
                DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                msg.setDate(hourdateFormat.format(new Date()));
                chatHistory.add(msg);
                if (mSocket != null && solicitudSeleccionada != null) {
                    mSocket.emit(VariablesGlobales.EMIT_OPERADOR_ENVIA_MENSAJE, solicitudSeleccionada.getIdSolicitud(), solicitudSeleccionada.getUsername(), mensaje, new Ack() {
                        @Override
                        public void call(Object... args) {
                            obtenerMensajes();
                        }
                    });
                }
            } else {
                ChatMessage msg = new ChatMessage();
                msg.setId(122);
                msg.setMe(true);
                msg.setMessage(mensaje);
                DateFormat hourdateFormat = new SimpleDateFormat("HH:mm:ss");
                msg.setDate(hourdateFormat.format(new Date()));
                chatHistory.add(msg);
                JSONObject object = new JSONObject();
                try {
                    object.put(VariablesGlobales.ID_PEDIDO, solicitudSeleccionada.getIdPedido());
                    object.put("to", solicitudSeleccionada.getUsername());
                    object.put("mensaje", mensaje);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mSocket != null && solicitudSeleccionada != null) {
                    mSocket.emit("enviar_mensaje_cliente", object, new Ack() {
                        @Override
                        public void call(Object... args) {
                            obtenerMensajes();
                        }
                    });
                }
            }
        }


    }

    public void obtenerMensajes() {
        if (actividadMensajes != null && ChatActivity.estadoChat) {
            actividadMensajes.mensajesUsuario(chatHistory);
        }
        if (actividadMapa != null) {
            actividadMapa.mensajesUsuario(chatHistory);
        }
    }

    public void obtenerMensajesCallCenter() {
        if (actividadMensajeriaBroadCast != null) {
            actividadMensajeriaBroadCast.mensajesCallCenter(chatCallCenter);
        }
    }

    public void obtenerMensajesBroadCastEmpresa() {
        if (actividadMensajeriaBroadCast != null) {
            actividadMensajeriaBroadCast.mensajesBroadCastEmpresa(chatBroadCastEmpresa);
        }
    }

    public void obtenerMensajesBroadCast() {
        if (actividadMensajeriaBroadCast != null) {
            actividadMensajeriaBroadCast.mensajesBroadCast(chatBroadCast);
        }
    }

    /**
     * 1 Nomal
     * 2 CallCenter
     * 3 Call Center carrera desde el aplicativo
     *
     * @param idSolicitud
     * @return
     */
    public int definirDesde(int idSolicitud) {
        if (idSolicitud > 0) {
            return 1;
        } else if (idSolicitud == 0) {
            return 2;
        } else if (idSolicitud < 0) {
            return 3;
        }
        return 1;
    }

    public void enviarSolicitudPendiente() {
        SolicitudTemporal solicitudTemporal = new SolicitudTemporal();
        String data = solicitudTemporal.getSolicitud(this);
        if (data != null) {
            try {
                JSONObject object = new JSONObject(data);
                parcearSolicitud(object);
                solicitudTemporal.guardarSolicitud(this, null, false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void cargarDestinos(Solicitud solicitud, JSONObject data) {
        List<Destino> destinos = null;
        if (data.has("lD")) {
            try {
                destinos = new ArrayList<>();
                JSONArray datosDestino = data.getJSONArray("lD");
                for (int i = 0; i < datosDestino.length(); i++) {
                    JSONObject destinosJ = datosDestino.getJSONObject(i);
                    Destino destino = new Destino();
                    destino.setDesBar(destinosJ.getString("desBar"));
                    destino.setDesRef(destinosJ.getString("desRef"));
                    destino.setDesCp(destinosJ.getString("desCp"));
                    destino.setDesCs(destinosJ.getString("desCs"));
                    destino.setDesDis(destinosJ.getInt("desDis"));
                    destino.setDesC(destinosJ.getDouble("desC"));
                    destino.setDesT(destinosJ.getString("desT"));
                    destino.setLtD(destinosJ.getDouble("ltD"));
                    destino.setLgD(destinosJ.getDouble("lgD"));
                    destinos.add(destino);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        solicitud.setlD(destinos);
    }

    public synchronized void parcearSolicitud(JSONObject solicitud) {
        try {
            if (solicitud.has(VariablesGlobales.ID_SOLICITUD)) {
                JSONObject datoSolicitud = solicitud.getJSONObject(VariablesGlobales.DATOS_SOLICITUD);
                Solicitud solis = new Solicitud();
                solis.setIdSolicitud(solicitud.getInt(VariablesGlobales.ID_SOLICITUD));
                solis.setAsignadaDesdeSolicitud(definirDesde(solis.getIdSolicitud()));
                if (datoSolicitud.has(VariablesGlobales.ID_CLIENTE)) {
                    solis.setUsername(datoSolicitud.getInt(VariablesGlobales.USERNAME));
                } else {
                    solis.setUsername(solicitud.getInt(VariablesGlobales.USERNAME));
                }
                solis.setReferencia(datoSolicitud.getString(VariablesGlobales.REFERENCIA_CLIENTE));
                solis.setLatitud(datoSolicitud.getDouble(VariablesGlobales.LATITUD));
                solis.setCalleSecundaria(datoSolicitud.getString(VariablesGlobales.CALLE_SECUNDARIA));
                if (datoSolicitud.has("saldo")) {
                    solis.setSaldo(datoSolicitud.getDouble("saldo"));
                }
                solis.setCalleSecundaria(datoSolicitud.getString(VariablesGlobales.CALLE_SECUNDARIA));
                if (solis.getCalleSecundaria().isEmpty()) {
                    solis.setAsignadaDesdeSolicitud(0);
                }
                solis.setCallePrincipal(datoSolicitud.getString(VariablesGlobales.CALLE_PRINCIPAL));
                solis.setBarrio(datoSolicitud.getString(VariablesGlobales.BARRIO_CLIENTE));
                solis.setLongitud(datoSolicitud.getDouble(VariablesGlobales.LONGITUD));
                solis.setNombresCliente(datoSolicitud.getString(VariablesGlobales.NOMBRES));
                solis.setCelular(datoSolicitud.getString(VariablesGlobales.CELULAR));
                if (datoSolicitud.has(VariablesGlobales.LATITUD_DESTINO) && datoSolicitud.has(VariablesGlobales.LONGITUD_DESTINO)) {
                    solis.setLatitudDestino(datoSolicitud.getDouble(VariablesGlobales.LATITUD_DESTINO));
                    solis.setLongitudDestino(datoSolicitud.getDouble(VariablesGlobales.LONGITUD_DESTINO));
                }
                solis.setPin(datoSolicitud.has(VariablesGlobales.PIN) ? datoSolicitud.getInt(VariablesGlobales.PIN) : 0);
                if (datoSolicitud.has(VariablesGlobales.PROPINA)) {
                    try {
                        solis.setPropina(datoSolicitud.getDouble(VariablesGlobales.PROPINA));
                    } catch (Exception e) {
                        solis.setPropina(0);
                    }
                }
                if (datoSolicitud.has("c")) {
                    solis.setEstadoChat(datoSolicitud.getInt("c"));
                }
                if (datoSolicitud.has(VariablesGlobales.DISTANCIA) && datoSolicitud.has(VariablesGlobales.TIEMPO)) {
                    solis.setDistancia(datoSolicitud.getDouble(VariablesGlobales.DISTANCIA));
                    solis.setTiempo(datoSolicitud.getInt(VariablesGlobales.TIEMPO));
                    String dat = datoSolicitud.getJSONArray("tiempos").toString();
                    JSONArray respJSON = new JSONArray(dat);
                    ArrayList<Integer> tiempos = new ArrayList<>();
                    for (int i = 0; i < respJSON.length(); i++) {
                        tiempos.add((Integer) respJSON.get(i));
                    }
                    solis.setTiempos(tiempos);
                }
                if (solicitud.has("isQuiosco")) {
                    solis.setQuiosco(true);
                }
                if (solicitud.has("datosTaxista")) {
                    JSONObject datosCancelar = solicitud.getJSONObject("datosTaxista");
                    solis.getDatosCancelarSolicitud().setEmpresa(datosCancelar.getString("empresa"));
                    solis.getDatosCancelarSolicitud().setPlaca(datosCancelar.getString("placa"));
                    solis.getDatosCancelarSolicitud().setNumeroUnidad(datosCancelar.getInt("numeroUnidad"));
                    solis.getDatosCancelarSolicitud().setRegistroMunicipal(datosCancelar.getString("registroMunicipal"));
                    solis.getDatosCancelarSolicitud().setCancelar(true);
                }
                if (datoSolicitud.has("color")) {
                    solis.setColor(datoSolicitud.getString("color"));
                }
                if (datoSolicitud.has("servicio")) {
                    solis.setServicio(datoSolicitud.getString("servicio"));
                }
                if (datoSolicitud.has("conP")) {
                    solis.setConP(datoSolicitud.getInt("conP"));
                }
                if (datoSolicitud.has(VariablesGlobales.TIPO_FORMA_PAGO)) {
                    solis.setTipoFormaPago(datoSolicitud.getInt(VariablesGlobales.TIPO_FORMA_PAGO));
                    //El has prv Precio Voucher
                    if (datoSolicitud.has("prV") && datoSolicitud.getInt(VariablesGlobales.TIPO_FORMA_PAGO) == VariablesGlobales.VOUCHER) {
                        solis.setPrecioVoucher(datoSolicitud.getDouble("prV"));
                    }
                }
                if (datoSolicitud.has("idServicioActivo")) {
                    solis.setIdServicioActivo(datoSolicitud.getInt("idServicioActivo"));
                }

                cargarDestinos(solis, datoSolicitud);
                Log.e(TAG, "parcearSolicitud: " + solis.toString());
                agregarSolicitudesALista(solis);
            } else {
                Solicitud solis = new Solicitud();
                solis.setIdSolicitud(solicitud.getInt(VariablesGlobales.ID_SOLICITUD_GENERAL));
                solis.setAsignadaDesdeSolicitud(definirDesde(solis.getIdSolicitud()));
                solis.setUsername(solicitud.getInt("id_cliente"));
                solis.setReferencia(solicitud.getString(VariablesGlobales.REFERENCIA_CLIENTE_2));
                solis.setLatitud(solicitud.getDouble(VariablesGlobales.LATITUD));
                solis.setCalleSecundaria(solicitud.getString("calle_secundaria"));
                if (solicitud.has(VariablesGlobales.LATITUD_DESTINO) && solicitud.has(VariablesGlobales.LONGITUD_DESTINO)) {
                    solis.setLatitudDestino(solicitud.getDouble(VariablesGlobales.LATITUD_DESTINO));
                    solis.setLongitudDestino(solicitud.getDouble(VariablesGlobales.LONGITUD_DESTINO));
                }
                if (solis.getCalleSecundaria().isEmpty()) {
                    solis.setAsignadaDesdeSolicitud(0);
                }
                solis.setCallePrincipal(solicitud.getString("calle_principal"));
                solis.setBarrio(solicitud.getString("barrio_cliente"));
                solis.setLongitud(solicitud.getDouble(VariablesGlobales.LONGITUD));
                if (solicitud.has("saldo")) {
                    solis.setSaldo(solicitud.getDouble("saldo"));
                }
                solis.setNombresCliente(solicitud.getString(VariablesGlobales.NOMBRES));
                solis.setCelular(solicitud.getString(VariablesGlobales.CELULAR));
                if (solicitud.has(VariablesGlobales.PROPINA)) {
                    try {
                        solis.setPropina(solicitud.getDouble(VariablesGlobales.PROPINA));
                    } catch (Exception e) {
                        solis.setPropina(0);
                    }
                }
                if (solicitud.has(VariablesGlobales.TIPO_FORMA_PAGO)) {
                    solis.setTipoFormaPago(solicitud.getInt(VariablesGlobales.TIPO_FORMA_PAGO));
                }
                if (solicitud.has("c")) {
                    solis.setEstadoChat(solicitud.getInt("c"));
                }
                if (solicitud.has(VariablesGlobales.DISTANCIA) && solicitud.has(VariablesGlobales.TIEMPO)) {
                    solis.setDistancia(solicitud.getDouble(VariablesGlobales.DISTANCIA));
                    solis.setTiempo(solicitud.getInt(VariablesGlobales.TIEMPO));
                    String dat = solicitud.getString("tiempos");
                    JSONArray respJSON = new JSONArray(dat);
                    ArrayList<Integer> tiempos = new ArrayList<>();
                    for (int i = 0; i < respJSON.length(); i++) {
                        tiempos.add((Integer) respJSON.get(i));
                    }
                    solis.setTiempos(tiempos);
                }
                if (solicitud.has("color")) {
                    solis.setColor(solicitud.getString("color"));
                }
                if (solicitud.has("servicio")) {
                    solis.setServicio(solicitud.getString("servicio"));
                }
                if (solicitud.has("datosTaxista")) {
                    JSONObject datosCancelar = solicitud.getJSONObject("datosTaxista");
                    solis.getDatosCancelarSolicitud().setEmpresa(datosCancelar.getString("empresa"));
                    solis.getDatosCancelarSolicitud().setPlaca(datosCancelar.getString("placa"));
                    solis.getDatosCancelarSolicitud().setNumeroUnidad(datosCancelar.getInt("numeroUnidad"));
                    solis.getDatosCancelarSolicitud().setRegistroMunicipal(datosCancelar.getString("registroMunicipal"));
                    solis.getDatosCancelarSolicitud().setCancelar(true);
                }
                if (solicitud.has("conP")) {
                    solis.setConP(solicitud.getInt("conP"));
                }
                //El has prv Precio Voucher
                if (solicitud.has(VariablesGlobales.TIPO_FORMA_PAGO)) {
                    solis.setTipoFormaPago(solicitud.getInt(VariablesGlobales.TIPO_FORMA_PAGO));
                    //El has prv Precio Voucher
                    if (solicitud.has("prV") && solicitud.getInt(VariablesGlobales.TIPO_FORMA_PAGO) == VariablesGlobales.VOUCHER) {
                        solis.setPrecioVoucher(solicitud.getDouble("prV"));
                    }
                }
                cargarDestinos(solis, solicitud);
                agregarSolicitudesALista(solis);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        retomarCarreras();


    }

    public void eliminarDeLaLista(Solicitud solicitud) {
        ExtraLog.Log(TAG, "eliminarDeLaLista: " + solicitud.getIdSolicitud());
        solicitudesEntrantes.remove(solicitud);
        retomarCarreras();
    }

    /**
     * Metodo para agregar solicitudes que no sean repetidas en la lista de solicitudes entrantes.
     *
     * @param solicitud solicitud entrante
     */
    public synchronized void agregarSolicitudesALista(final Solicitud solicitud) {
        prenderPantalla();
        if (solicitud != null) {
            if (!solicitud.isPedido() && solicitud.getIdSolicitud() <= 0) {
                solicitudSeleccionada = solicitud;
                estadoSolicitud = 5;
                solicitudSeleccionada.setEstadoSolicitud(5);
                solicitudSeleccionada.setTiempo(0);
                solicitudesEntrantes.clear();
                guardarEstadoSolicitud();
                AgregarSolicitudCallCenter(solicitud);
                hablarEntrada("Solicitud asignada desde call center.");
                return;
            }
        }
        if (solicitudesEntrantes.size() == 0) {
            solicitudesEntrantes.add(solicitud);
            if (solicitud.getIdPedido() == 0) {
                hablarEntrada(getResources().getString(R.string.audio_app) + solicitud.getBarrio());
            } else {
                if (solicitud.getT() == 2) {
                    hablarEntrada("Encomienda en el barrio " + solicitud.getBarrio());
                } else {
                    hablarEntrada("Compra en " + solicitud.getLugarCompra() + " hasta barrio " + solicitud.getBarrio());
                }
            }
        } else {
            if (!solicitudesEntrantes.contains(solicitud)) {
                solicitudesEntrantes.add(solicitud);
                if (solicitud.getIdPedido() == 0) {
                    hablarEntrada(getResources().getString(R.string.audio_app) + solicitud.getBarrio());
                } else {
                    if (solicitud.getT() == 2) {
                        hablarEntrada("Encomienda en el barrio " + solicitud.getBarrio());
                    } else {
                        hablarEntrada("Compra en " + solicitud.getLugarCompra() + " hasta barrio " + solicitud.getBarrio());
                    }
                }
            }
        }
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (solicitud.isPedido()) {
                    limpiarLista(solicitud.getIdPedido());
                } else {
                    limpiarLista(solicitud.getIdSolicitud());

                }
            }
        }, 185000);
    }

    public void hablarEntrada(String msj) {
        if (configuracionApp.getBoolean(VariablesGlobales.EN_LLAMADA, true)) {
            if (configuracionApp.getBoolean(VariablesGlobales.ASISTENTE_VOZ, true)) {
                if (!isVibrarSonar) {
                    reproducirTextAudio.speak(msj);
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    v.vibrate(2000);
                }
            } else {
                if (!isVibrarSonar) {
                    sonarSolicitud(1);
                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    v.vibrate(2000);
                }
            }
        } else {
            if (configuracionApp.getBoolean(VariablesGlobales.ASISTENTE_VOZ, true)) {
                reproducirTextAudio.speak(msj);
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(2000);
            } else {
                sonarSolicitud(1);
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                v.vibrate(2000);
            }
        }
    }

    public void AgregarSolicitudCallCenter(Solicitud solicitud) {
        if (actividadMapa != null) {
            actividadMapa.clienteAceptoTiempo(true, solicitud, null);
        }
        if (actividadLista != null) {
            actividadLista.solicitudCallCenter(solicitud);
        }
    }

    public void reiniciarVariablesPedido() {
        estadoComprandoSemaforo = 0;
        estadoCompradoSemaforo = 0;
        isEnviarComprarPedido = false;
    }

    public synchronized void parcearPedido(JSONObject solicitud) {
        reiniciarVariablesPedido();
        try {
            if (solicitud.has(VariablesGlobales.DATOS_SOLICITUD)) {
                JSONObject datoSolicitud = solicitud.getJSONObject(VariablesGlobales.DATOS_SOLICITUD);
                Solicitud solis = new Solicitud();
                solis.setIdSolicitud(1);
                solis.setIdPedido(solicitud.getInt(VariablesGlobales.ID_PEDIDO));
                if (datoSolicitud.has(VariablesGlobales.ID_CLIENTE)) {
                    solis.setUsername(datoSolicitud.getInt(VariablesGlobales.USERNAME));
                } else {
                    solis.setUsername(solicitud.getInt(VariablesGlobales.USERNAME));
                }
                solis.setPedido(datoSolicitud.getString("pedido"));
                solis.setReferencia(datoSolicitud.getString(VariablesGlobales.REFERENCIA_CLIENTE));
                solis.setLatitud(datoSolicitud.getDouble(VariablesGlobales.LATITUD));
                solis.setCalleSecundaria(datoSolicitud.getString(VariablesGlobales.CALLE_SECUNDARIA));
                solis.setCallePrincipal(datoSolicitud.getString(VariablesGlobales.CALLE_PRINCIPAL));
                solis.setBarrio(datoSolicitud.getString(VariablesGlobales.BARRIO_CLIENTE));
                solis.setLongitud(datoSolicitud.getDouble(VariablesGlobales.LONGITUD));
                if (solicitud.has("saldo")) {
                    solis.setSaldo(solicitud.getDouble("saldo"));
                }
                solis.setNombresCliente(datoSolicitud.getString(VariablesGlobales.NOMBRES));
                solis.setCelular(datoSolicitud.getString(VariablesGlobales.CELULAR));
                if (datoSolicitud.has("lugarCompra")) {
                    solis.setLugarCompra(datoSolicitud.getString("lugarCompra"));
                }
                if (datoSolicitud.has("c")) {
                    solis.setEstadoChat(datoSolicitud.getInt("c"));
                } else {
                    solis.setEstadoChat(1);
                }
                if (datoSolicitud.has("color")) {
                    solis.setColor(datoSolicitud.getString("color"));
                }
                if (datoSolicitud.has("servicio")) {
                    solis.setServicio(datoSolicitud.getString("servicio"));
                }
                solis.setT(datoSolicitud.getInt("t"));
                if (!datoSolicitud.getString("ltE").equals("null") || !datoSolicitud.getString("lgE").equals("null") || !datoSolicitud.getString("bE").equals("null")) {
                    solis.setLtE(datoSolicitud.getDouble("ltE"));
                    solis.setLgE(datoSolicitud.getDouble("lgE"));
                    solis.setbE(datoSolicitud.getString("bE"));
                }
                String[] precios = datoSolicitud.getString("precios").split(",");
                ArrayList<Double> prec = new ArrayList<>();
                for (int i = 0; i < precios.length; i++) {
                    prec.add(Double.parseDouble(precios[i]));
                }
                solis.setLp(prec);
                solis.setAu(datoSolicitud.getDouble("au"));
                solis.setM(datoSolicitud.getDouble("m"));
                agregarSolicitudesALista(solis);
            } else {
                Solicitud solis = new Solicitud();
                solis.setIdSolicitud(1);
                solis.setIdPedido(solicitud.getInt(VariablesGlobales.ID_PEDIDO));
                solis.setUsername(solicitud.getInt("idCliente"));
                solis.setPedido(solicitud.getString("pedido"));
                solis.setReferencia(solicitud.getString(VariablesGlobales.REFERENCIA_CLIENTE));
                solis.setLatitud(solicitud.getDouble(VariablesGlobales.LATITUD));
                solis.setCalleSecundaria(solicitud.getString(VariablesGlobales.CALLE_SECUNDARIA));
                solis.setCallePrincipal(solicitud.getString(VariablesGlobales.CALLE_PRINCIPAL));
                solis.setBarrio(solicitud.getString(VariablesGlobales.BARRIO_CLIENTE));
                solis.setLongitud(solicitud.getDouble(VariablesGlobales.LONGITUD));
                solis.setNombresCliente(solicitud.getString(VariablesGlobales.NOMBRES));

                if (solicitud.has("saldo")) {
                    solis.setSaldo(solicitud.getDouble("saldo"));
                }
                if (solicitud.has("color")) {
                    solis.setColor(solicitud.getString("color"));
                }
                if (solicitud.has("servicio")) {
                    solis.setServicio(solicitud.getString("servicio"));
                }
                solis.setCelular(solicitud.getString(VariablesGlobales.CELULAR));
                if (solicitud.has("lugarCompra")) {
                    solis.setLugarCompra(solicitud.getString("lugarCompra"));
                }
                ArrayList<Double> precios = new ArrayList<>();
                if (solicitud.has("c")) {
                    solis.setEstadoChat(solicitud.getInt("c"));
                } else {
                    solis.setEstadoChat(1);
                }
                solis.setT(solicitud.getInt("t"));
                if (!solicitud.getString("ltE").equals("null") || !solicitud.getString("lgE").equals("null") || !solicitud.getString("bE").equals("null")) {
                    solis.setLtE(solicitud.getDouble("ltE"));
                    solis.setLgE(solicitud.getDouble("lgE"));
                    solis.setbE(solicitud.getString("bE"));
                }

                String[] precios_dos = solicitud.getString("precios").split(",");
                ArrayList<Double> prec = new ArrayList<>();
                for (int i = 0; i < precios_dos.length; i++) {
                    prec.add(Double.parseDouble(precios_dos[i]));
                }
                solis.setLp(prec);
                solis.setAu(solicitud.getDouble("au"));
                solis.setM(solicitud.getDouble("m"));
                agregarSolicitudesALista(solis);
                agregarSolicitudesALista(solis);

            }
        } catch (JSONException e) {
            e.getMessage();
        }
        retomarCarreras();


    }


    public void limpiarLista(int idSolicitud) {
        int contador = 0;
        if (solicitudesEntrantes.size() > 0) {
            for (Solicitud i : solicitudesEntrantes) {
                if (idSolicitud == i.getIdSolicitud() || idSolicitud == i.getIdPedido()) {
                    solicitudesEntrantes.remove(contador);
                    retomarCarreras();
                    break;
                }
                contador++;
            }
        }
        if (solicitudesEntrantes.size() == 0) {
            isSoPantalla = true;
        }
    }

    public void limpiarListaSolicitudes() {
        if (solicitudesEntrantes != null) {
            solicitudesEntrantes.clear();
        }
        retomarCarreras();
    }


    public void rechazarSolicitud() {
        isSoPantalla = false;
    }

    public boolean isSoPantalla = true;

    public void retomarCarreras() {
        if (actividadMapa != null) {
            actividadMapa.nuevaSolicitud(solicitudesEntrantes);
        }
        if (actividadLista != null) {
            if (isSoPantalla) {
                actividadLista.nuevasolicitud(solicitudesEntrantes, solicitudesEntrantes.size() == 1);
            } else {
                actividadLista.nuevasolicitud(solicitudesEntrantes, false);
            }

        }
    }

    public void contadorServicioTiempo() {
        isCronometroTiempo = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                int tiempo = 0;
                while (isCronometroTiempo) {
                    tiempo++;
                    try {
                        if (tiempo >= 90) {
                            isCronometroTiempo = false;
                            if (solicitudSeleccionada != null) {
                                estadoSolicitud = 7;
                                solicitudSeleccionada.setEstadoSolicitud(7);
                                guardarEstadoSolicitud();
                                if (solicitudSeleccionada.getIdPedido() != 0) {
                                    cancelarPedido(11, 7, solicitudSeleccionada, false, null);
                                } else {
                                    cancelarSolicitud(11, 7, solicitudSeleccionada, false, 0, null);
                                }

                                if (actividadMapa != null) {
                                    actividadMapa.clienteAceptoTiempo(false, null, getString(R.string.no_confimacion_tiempo));
                                }
                                if (actividadLista != null) {
                                    actividadLista.clienteAceptoTiempo(false, null, getString(R.string.no_confimacion_tiempo));
                                }
                                isCronometroTiempo = false;
                                solicitudSeleccionada = null;
                                borrarPreferencia();
                                estadoSolicitud = 0;
                                estadoPedido = 0;
                                chatHistory.clear();
                            }
                        }
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void cambioActividad() {
        if (actividadMapa != null) {
            actividadMapa.clienteAceptoTiempo(true, solicitudSeleccionada, "");
        }
    }

    private void runAsForeground() {
        if (spobtener.getBoolean(VariablesGlobales.LOGEADO, false)) {
            Intent notificationIntent = new Intent(this, MapaBaseActivity.class);
            @SuppressLint("WrongConstant") PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                String CHANNEL_ID = "KtaxiConductor_02";
                CharSequence name = "Ktaxi Conductor";
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                notification = new Notification.Builder(this, CHANNEL_ID)
                        .setContentTitle(getString(R.string.app_name))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setChannelId(CHANNEL_ID)
                        .setAutoCancel(true)
                        .setContentIntent(PendingIntent.getActivity(this, 0, notificationIntent, 0))
                        .build();
                notificationManager.createNotificationChannel(mChannel);
            } else {
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
                builder.setSmallIcon(R.mipmap.ic_launcher);
                builder.setContentTitle(getString(R.string.app_name));
                builder.setContentIntent(pendingIntent);
                notification = builder.build();
            }
            this.startForeground(1000, notification);
        } else {
            stopForeground(true);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        runAsForeground();
        if (rastreoHandle == null) {
            actualizarRastreoTaxi();
        }
        return START_STICKY;
    }

    private String horaAnterior = "";

    public void enviarRastreoServer(PosicionNueva position, boolean isButton) {

        if (!horaAnterior.equals(position.getFecha())) {
            horaAnterior = position.getFecha();
            if (position.getIdEquipo() > 0 && position.getIdVehiculo() > 0) {
                if (mSocket != null && isServerOnline) {
                    mSocket.emit(VariablesGlobales.EMIT_ENVIAR_LOCALIZACION, posicionLoteNueva(position), new Ack() {
                        @Override
                        public void call(Object... args) {
                            Log.i(TAG, VariablesGlobales.EMIT_ENVIAR_LOCALIZACION.concat(args[0].toString()));
                        }
                    });
                    return;
                }
                if (!isButton) {
                    position.save();
                }
            }
        }

    }


    Runnable runnableSincroniza;

    public void sincronizarRastreo() {
        runnableSincroniza = new Runnable() {
            @Override
            public void run() {
                StringBuilder lote = new StringBuilder();
                final List<PosicionNueva> posicionNuevaList = PosicionNueva.listaPosiciones();
                Log.e("trama", posicionNuevaList.toString());
                if (posicionNuevaList.size() > 0) {
                    for (PosicionNueva s : posicionNuevaList) {
                        lote.append("(");
                        lote.append(s.getIdVehiculo());
                        lote.append(",'");
                        lote.append(s.getFecha().split(" ")[0]);
                        lote.append("','");
                        lote.append(s.getFecha().split(" ")[1]);
                        lote.append("',");
                        lote.append(s.getIdEquipo());
                        lote.append(",");
                        lote.append(s.getLatitud());
                        lote.append(",");
                        lote.append(s.getLongitud());
                        lote.append(",");
                        lote.append(s.getAltitud());
                        lote.append(",");
                        lote.append(s.getVelocidad());
                        lote.append(",");
                        lote.append(s.getAcury());
                        lote.append(",");
                        lote.append(s.getDireccion());
                        lote.append(",");
                        lote.append(s.getBateria());
                        lote.append(",");
                        lote.append(s.getConexion());
                        lote.append(",");
                        lote.append(s.getGps());
                        lote.append(",");
                        lote.append(s.getEstado());
                        lote.append(",");
                        lote.append(s.getTemperatura());
                        lote.append(",");
                        lote.append(s.getConsumo());
                        lote.append(",");
                        lote.append(s.getTipoRed());
                        lote.append("),");
                    }
                    PosicionNueva.removerPosiciones(posicionNuevaList);
                    if (mSocket != null) {
                        mSocket.emit(VariablesGlobales.EMIT_ENVIAR_LOCALIZACION_LOTE, lote.toString().substring(0, lote.length() - 1), new Ack() {
                            @Override
                            public void call(Object... args) {
                                if (Integer.parseInt(args[0].toString()) == 1) {
                                    posicionNuevaList.clear();
                                    scheduler.schedule(runnableSincroniza, 60, SECONDS);
                                }
                            }
                        });
                    }
                }
            }
        };
        scheduler.execute(runnableSincroniza);
    }

    public StringBuilder posicionLoteNueva(PosicionNueva s) {
        StringBuilder lote = new StringBuilder();
        lote.append(s.getIdVehiculo());
        lote.append(",'");
        lote.append(s.getFecha().split(" ")[0]);
        lote.append("','");
        lote.append(s.getFecha().split(" ")[1]);
        lote.append("',");
        lote.append(s.getIdEquipo());
        lote.append(",");
        lote.append(s.getLatitud());
        lote.append(",");
        lote.append(s.getLongitud());
        lote.append(",");
        lote.append(s.getAltitud());
        lote.append(",");
        lote.append(s.getVelocidad());
        lote.append(",");
        lote.append(s.getAcury());
        lote.append(",");
        lote.append(s.getDireccion());
        lote.append(",");
        lote.append(s.getBateria());
        lote.append(",");
        lote.append(s.getConexion());
        lote.append(",");
        lote.append(s.getGps());
        lote.append(",");
        lote.append(s.getEstado());
        lote.append(",");
        lote.append(s.getTemperatura());
        lote.append(",");
        lote.append(s.getConsumo());
        lote.append(",");
        lote.append(s.getTipoRed());
        return lote;
    }

    public void guardarEstadoSolicitud() {
        if (solicitudSeleccionada != null) {
            String jSonSolicitud = solicitudSeleccionada.serializeSolicitud();
            SharedPreferences.Editor editor = spSolicitud.edit();
            editor.putString("key_objeto_solicitud", jSonSolicitud);
            editor.putBoolean("key_estado_solicitud", true);
            editor.apply();
        }
    }

    public void seleccionarSolicitud(int idSolicitud, int tipoSolicitud) {
        //Tipo de solicitud
        //1 : Solicitud Normal
        //2 : Pedido de compra
        try {
            if (tipoSolicitud == 1) {
                int contador = 0;
                if (solicitudesEntrantes.size() > 0) {
                    for (Solicitud i : solicitudesEntrantes) {
                        if (idSolicitud == i.getIdSolicitud()) {
                            solicitudSeleccionada = solicitudesEntrantes.get(contador);
                            idSolicitudSeleccionada = solicitudSeleccionada.getIdSolicitud();
                            tipoFormaPago = solicitudSeleccionada.getTipoFormaPago();
                            String jSonSolicitud = solicitudSeleccionada.serializeSolicitud();
                            SharedPreferences.Editor editor = spSolicitud.edit();
                            editor.putString("key_objeto_solicitud", jSonSolicitud);
                            editor.putBoolean("key_estado_solicitud", true);
                            editor.apply();
                            limpiarLista(solicitudSeleccionada.getIdSolicitud());
                        }
                        contador++;
                    }
                }
            }
            if (tipoSolicitud == 2) {
                int contador = 0;
                if (solicitudesEntrantes.size() > 0) {
                    for (Solicitud i : solicitudesEntrantes) {
                        if (idSolicitud == i.getIdPedido()) {
                            solicitudSeleccionada = solicitudesEntrantes.get(contador);
                            idPedidoSeleccionado = solicitudSeleccionada.getIdPedido();
                            String jSonSolicitud = solicitudSeleccionada.serializeSolicitud();
                            SharedPreferences.Editor editor = spSolicitud.edit();
                            editor.putString("key_objeto_solicitud", jSonSolicitud);
                            editor.putBoolean("key_estado_solicitud", true);
                            editor.apply();
                            limpiarLista(solicitudSeleccionada.getIdPedido());
                            break;
                        }
                        contador++;
                    }
                }
            }
        } catch (java.util.ConcurrentModificationException e) {
            e.printStackTrace();
        }

    }

    public int getIdSolicitudSeleccionada() {
        return idSolicitudSeleccionada;
    }

    public void estadoDelTaxi(int estado) {
        Log.e("prueba", "enviando estado");
        JSONObject objeto = new JSONObject();
        spobtener = this.getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        try {
            objeto.put(VariablesGlobales.ID_EMPRESA, spobtener.getInt(VariablesGlobales.ID_EMPRESA, 0));
            objeto.put(VariablesGlobales.NUMERO_UNIDAD, spobtener.getInt(VariablesGlobales.UNIDAD_VEHICULO, 0));
            objeto.put(VariablesGlobales.ESTADO, estado);
            objeto.put(VariablesGlobales.ID_VEHICULO, spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
            if (mSocket != null) {
                mSocket.emit(VariablesGlobales.EMIT_ENVIAR_ESTADO_KTAXI, objeto);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void estadoBotonDelTaxi(int estado) {
        if (estado != 8) {
            if (estado != estadoBoton) {
                estadoBoton = estado;
                enviarRastreo(estadoBoton, true);
            }
        }
    }

    public boolean isTaximetroRun;

    public void finalizarTaximetro() {
        stopService(new Intent(this, TaximetroService.class));
        isTaximetroRun = false;
    }

    public void iniciarTaximetro() {
        startService(new Intent(this, TaximetroService.class));
        isTaximetroRun = true;
    }

    public void activarTaximetro(int editarCostoC,int tipo, String tiempoEspera, String valorTiempo, String velocidadtxt,
                                 String distanciaRec, String valorPrincipal, String valDistancia, String cronometro,
                                 String horaInicioTxt, String tarArr, String totalCarrera) {

        if (actividadTaximetro != null) {
            actividadTaximetro.notificarRastreo(editarCostoC,tipo, tiempoEspera, valorTiempo, velocidadtxt,
                    distanciaRec, valorPrincipal, valDistancia, cronometro,
                    horaInicioTxt, tarArr, totalCarrera);
        } else if (actividadMapa != null) {
            actividadMapa.notificarRastreo(editarCostoC,tipo, tiempoEspera, valorTiempo, velocidadtxt,
                    distanciaRec, valorPrincipal, valDistancia, cronometro,
                    horaInicioTxt, tarArr, totalCarrera);
        } else if (actividadLista != null) {
            actividadLista.notificarRastreo(editarCostoC,tipo, tiempoEspera, valorTiempo, velocidadtxt,
                    distanciaRec, valorPrincipal, valDistancia, cronometro,
                    horaInicioTxt, tarArr, totalCarrera);
        }
    }

    public void NotificacionConIntent(Context context, Integer id, String titulo, String contenido, Class<?> actividad) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        Intent intent = new Intent(getApplicationContext(), actividad);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        long[] pattern = new long[]{1000, 500, 1000};
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(titulo)
                .setContentText(contenido)
                .setAutoCancel(true)
                .setVibrate(pattern)
                .setContentIntent(PendingIntent.getActivity(context, 0, intent, 0));
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(id, builder.build());
    }

    public void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }

    private String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        return formateador.format(ahora);
    }


    public void getHora(final int min) {
        minuto = min;
        segundos = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (isBanderaRun) {
                    if (actividadMapa != null) {
                        String dato = (tiempoEnvio < 10) ? "0".concat(String.valueOf(tiempoEnvio)) : String.valueOf(tiempoEnvio);
                        actividadMapa.mostrarTiempo(dato + ":00/" + (minuto < 10 ? "0" + minuto : minuto) + ":" + (segundos < 10 ? "0" + segundos : segundos));
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    segundos--;
                    if (segundos < 0) {
                        segundos = 59;
                        minuto--;
                        if (minuto < 0) {
                            minuto = 0;
                            segundos = 0;
                            isBanderaRun = false;
                            if (actividadMapa != null) {
                                actividadMapa.cambiarAbordo();
                            }
                        }
                    }
                }
            }
        }).start();
    }

    public void getEstadoHora() {
        if (actividadMapa != null) {
            if (!isBanderaRun) {
                String dato = (tiempoEnvio < 10) ? "0".concat(String.valueOf(tiempoEnvio)) : String.valueOf(tiempoEnvio);
                actividadMapa.mostrarTiempo(dato + ":00/" + (minuto < 10 ? "0" + minuto : minuto) + ":" + (segundos < 10 ? "0" + segundos : segundos));
                actividadMapa.cambiarAbordo();
            }
        }
    }

    public void reiniciarCronometro() {
        tiempoEnvio = 0;
        minuto = 0;
        segundos = 0;
    }

    public void estadoSolicitud() {
        if (solicitudSeleccionada != null) {
            switch (solicitudSeleccionada.getEstadoSolicitud()) {
                case 2:
                    if (actividadMapa != null) {
                        actividadMapa.solicitudAtendida(getString(R.string.solicitud_atendida));
                    }
                    if (actividadLista != null) {
                        actividadLista.solicitudAtendida(getString(R.string.solicitud_atendida));
                    }
                    break;
                case 3://Tiempo Enviado al servidor
                    if (tiempoEnviado) {
                        if (mensajeDeuda != null && mensajeDeuda.getL() != null && mensajeDeuda.getL().getP() != null) {
                            if (mensajeDeuda.getL().getP().getRP() == 1) {
                                reproducirTextAudio.speak(mensajeDeuda.getL().getP().getMs());
                                break;
                            }
                            if (actividadLista != null) {
                                if (mensajeDeuda.getL().getP().getNP() == 2 || mensajeDeuda.getL().getP().getNP() == 3) {
                                    actividadLista.solicitudAtendidaACK(mensajeDeuda);
                                    break;
                                }
                            }
                            if (actividadMapa != null) {
                                if (mensajeDeuda.getL().getP().getNP() == 2 || mensajeDeuda.getL().getP().getNP() == 3) {
                                    actividadMapa.solicitudAtendidaACK(mensajeDeuda);
                                    break;
                                }
                            }
                        }
                        if (actividadLista != null) {
                            actividadLista.tiempoEnviado(tiempoEnvio, solicitudSeleccionada);
                        }
                        if (actividadMapa != null) {
                            actividadMapa.tiempoEnviado(tiempoEnvio, solicitudSeleccionada);
                        }
                    }
                    break;
                case 5://EL cliente acepto el tiempo
                    if (actividadMapa != null) {
                        actividadMapa.clienteAceptoTiempo(true, solicitudSeleccionada, "");
                    }
                    if (actividadLista != null) {
                        actividadLista.clienteAceptoTiempo(true, solicitudSeleccionada, "");
                    }
                    break;
                case 6:
                    if (actividadMapa != null) {
                        actividadMapa.clienteCanceloSolicitud(solicitudSeleccionada, solicitudSeleccionada.getMensajeSolicitudCancelada(), solicitudSeleccionada.isCalificar());
                    } else if (actividadLista != null) {
                        actividadLista.clienteCanceloSolicitud(solicitudSeleccionada, solicitudSeleccionada.getMensajeSolicitudCancelada(), solicitudSeleccionada.isCalificar());
                    }
                    break;
                case 7:
                    if (actividadMapa != null) {
                        actividadMapa.clienteAceptoTiempo(false, null, getString(R.string.no_confimacion_tiempo));
                    }
                    if (actividadLista != null) {
                        actividadLista.clienteAceptoTiempo(false, null, getString(R.string.no_confimacion_tiempo));
                    }
                    break;
                case 9:
                    if (actividadLista != null) {
                        actividadLista.clienteAceptoTiempo(true, solicitudSeleccionada, null);
                    }
                    if (actividadMapa != null) {
                        actividadMapa.clienteAceptoTiempo(true, solicitudSeleccionada, null);
                        actividadMapa.clienteAbordo(getString(R.string.cliente_informa_abordo), solicitudSeleccionada);
                    }
                    if (actividadMensajes != null && ChatActivity.estadoChat) {
                        actividadMensajes.clienteAbordo(getString(R.string.cliente_informa_abordo), solicitudSeleccionada);
                    }
                    break;
                case 10:
                    if (idSolicitudSeleccionada != 0 && solicitudSeleccionada != null) {
                        if (actividadLista != null) {
                            actividadLista.clienteAceptoTiempo(true, solicitudSeleccionada, null);
                        }
                        if (actividadMapa != null) {
                            actividadMapa.clienteFinCarrera(getString(R.string.cliente_finalizo_carrera), "1", solicitudSeleccionada);
                        }
                        if (actividadMensajes != null && ChatActivity.estadoChat) {
                            actividadMensajes.clienteFinCarrera(getString(R.string.cliente_finalizo_carrera), "1");
                        }
                        isMensajeAbordo = false; //
                    } else if (idSolicitudSeleccionada != 0) {
                        if (actividadMapa != null) {
                            actividadMapa.cambiarOcupado();
                        }
                    }
                    break;
                case 15:
                    if (actividadMapa != null) {
                        actividadMapa.clienteAceptoTiempo(false, null, "La solicitud ya ha sido atendida.");
                    }
                    if (actividadLista != null) {
                        actividadLista.clienteAceptoTiempo(false, null, "La solicitud ya ha sido atendida.");
                    }
            }
        }
    }


    public void estadoPedido() {
        if (solicitudSeleccionada != null) {
            switch (solicitudSeleccionada.getEstadoPedido()) {
                case 2:
                    if (precioEnviado) {
                        if (actividadLista != null) {
                            actividadLista.tiempoEnviado(0, solicitudSeleccionada);
                        }
                        if (actividadMapa != null) {
                            actividadMapa.tiempoEnviado(0, solicitudSeleccionada);
                        }
                    }
                    break;
                case 3:
                    isCronometroTiempo = false;
                    estadoPedido = 3;
                    solicitudSeleccionada.setEstadoPedido(3);
                    guardarEstadoSolicitud();
                    confirmarEstado(solicitudSeleccionada.getIdPedido(), 3);
                    if (actividadMapa != null) {
                        actividadMapa.clienteAceptoTiempo(true, solicitudSeleccionada, "");
                    } else if (actividadLista != null) {
                        actividadLista.clienteAceptoTiempo(true, solicitudSeleccionada, "");
                    }
                    if (precioEnviado) {
                        if (mensajeDeuda != null && mensajeDeuda.getL() != null && mensajeDeuda.getL().getP() != null) {
                            if (mensajeDeuda.getL().getP().getRP() == 1) {
                                reproducirTextAudio.speak(mensajeDeuda.getL().getP().getMs());
                                break;
                            }
                            if (actividadLista != null) {
                                if (mensajeDeuda.getL().getP().getNP() == 2 || mensajeDeuda.getL().getP().getNP() == 3) {
                                    actividadLista.solicitudAtendidaACK(mensajeDeuda);
                                    break;
                                }
                            }
                            if (actividadMapa != null) {
                                if (mensajeDeuda.getL().getP().getNP() == 2 || mensajeDeuda.getL().getP().getNP() == 3) {
                                    actividadMapa.solicitudAtendidaACK(mensajeDeuda);
                                    break;
                                }
                            }
                        }
                        if (actividadLista != null) {
                            actividadLista.tiempoEnviado(tiempoEnvio, solicitudSeleccionada);
                        }
                        if (actividadMapa != null) {
                            actividadMapa.tiempoEnviado(tiempoEnvio, solicitudSeleccionada);
                        }
                    }
                    break;
                case 4:
                    if (actividadMapa != null) {
                        actividadMapa.clienteAceptoTiempo(true, solicitudSeleccionada, "");
                        switch (solicitudSeleccionada.getEstadoComprandoSemaforo()) {
                            case 2:
                                actividadMapa.cambiarEstadoComprando();
                                break;
                            case 3:
                                actividadMapa.cambiarEstadoComprandoUsuario();
                                break;
                        }
                    }
                    break;
                case 5:
                    if (actividadMapa != null) {
                        actividadMapa.clienteAceptoTiempo(true, solicitudSeleccionada, "");
                        switch (solicitudSeleccionada.getEstadoCompradoSemaforo()) {
                            case 1:
                                actividadMapa.cambiarEstadoComprandoUsuario();
                                break;
                            case 2:
                                actividadMapa.cambiarEstadoComprandoUsuario();
                                actividadMapa.cambiarEstadoComprado();
                                break;
                            case 3:
                                actividadMapa.cambiarEstadoComprandoUsuario();
                                actividadMapa.cambiarEstadoCompradoUsuario();
                                break;
                        }
                    }
                    break;
                case 6:
                    if (actividadMapa != null) {
                        actividadMapa.clienteFinCarrera(getString(R.string.cliente_finalizo_carrera), "1", solicitudSeleccionada);
                    } else if (actividadMensajes != null && ChatActivity.estadoChat) {
                        actividadMensajes.clienteFinCarrera(getString(R.string.cliente_finalizo_carrera), "1");
                    }
                    break;
                case 7:
                    if (actividadMapa != null) {
                        actividadMapa.clienteCanceloPedido(
                                solicitudSeleccionada,
                                solicitudSeleccionada.getMensajePedidoCancelado() != null ? solicitudSeleccionada.getMensajePedidoCancelado() : "Cliente cancelo pedido",
                                solicitudSeleccionada.isCalificar());
                    } else if (actividadLista != null) {
                        actividadLista.clienteCanceloPedido(solicitudSeleccionada,
                                solicitudSeleccionada.getMensajePedidoCancelado() != null ? solicitudSeleccionada.getMensajePedidoCancelado() : "Cliente cancelo pedido",
                                solicitudSeleccionada.isCalificar());
                    }
                    break;
                case 10:
                    if (solicitudSeleccionada != null) {
                        isBanderaRun = false;
                        minuto = 0;
                        segundos = 0;
                        chatHistory.clear();
                        confirmarEstado(solicitudSeleccionada.getIdPedido(), estadoPedido);
                        if (actividadMapa != null) {

                            actividadMapa.clienteFinCarrera("El cliente finalizo el pedido", "1", solicitudSeleccionada);
                            return;
                        } else if (actividadMensajes != null && ChatActivity.estadoChat) {
                            actividadMensajes.clienteFinCarrera("El cliente finalizo el pedido", "1");
                        }
                    }
                    break;
            }
        }

    }

    public void confirmarEstado(int idPedido, final int estado) {
        if (mSocket != null) {
            JSONObject object = new JSONObject();
            try {
                object.put("confirmoEstado", estado);
                object.put(VariablesGlobales.ID_PEDIDO, idPedido);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit("confirma_recepcionp_a_cliente", object, new Ack() {
                @Override
                public void call(Object... args) {
                }
            });
        }
    }

    private void registerReciver() {
        wfs = new GpsLocationReceiver();
        IntentFilter iFilter = new IntentFilter();
        iFilter.addAction("android.location.PROVIDERS_CHANGED");
        iFilter.addAction("android.intent.category.DEFAULT");
        iFilter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        iFilter.addAction(GpsLocationReceiver.PRENDER_PANTALLA);
        iFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        iFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        iFilter.addAction(Intent.ACTION_BATTERY_LOW);
//        iFilter.addAction(GpsLocationReceiver.ACTION_IGNITION);//PRUEBA BORRAR
        registerReceiver(wfs, iFilter);
    }

    public boolean isClienteAceptoTiempo() {
        return isClienteAceptoTiempo;
    }

    public void setClienteAceptoTiempo(boolean clienteAceptoTiempo) {
        isClienteAceptoTiempo = clienteAceptoTiempo;
    }

    public boolean isEnviarComprarPedido() {
        return isEnviarComprarPedido;
    }

    public void setEnviarComprarPedido(boolean enviarComprarPedido) {
        isEnviarComprarPedido = enviarComprarPedido;
    }

    public boolean isEnviarllevandoPedido() {
        return isEnviarllevandoPedido;
    }

    public void setEnviarllevandoPedido(boolean enviarllevandoPedido) {
        isEnviarllevandoPedido = enviarllevandoPedido;
    }

    public void resetIdEnvioCalificacion() {
        idSolicitudSeleccionada = 0;
        idPedidoSeleccionado = 0;
    }

    public int getIdPedidoSeleccionado() {
        return idPedidoSeleccionado;
    }

    public class LocalBinder extends Binder {
        public ServicioSockets getService() {
            return ServicioSockets.this;
        }
    }

    public class GpsLocationReceiver extends BroadcastReceiver {
        public static final String PRENDER_PANTALLA = "com.kradac.ktaxy_driver.service.PRENDER_PANTALLA";
        private static final String ACTION_IGNITION = "hk.topicon.intent.action.IGNITION";

        public GpsLocationReceiver() {
        }


        private int statusLevel;

        @SuppressLint({"WrongConstant", "InvalidWakeLockTag"})
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(ACTION_IGNITION)) {
                boolean status = intent.getBooleanExtra("state", false);
                if (actividadMapa != null) {
                    ExtraLog.Log(TAG, "onReceive: " + status);
                    if (status) {
                        actividadMapa.encenderPantalla();
                        powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                        keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("INFO");
                        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "INFO");
                        wakeLock.acquire(3000);
                    } else {
                        actividadMapa.apagarPantalla();
                    }
                }
            }

            if (intent.getExtras() != null) {
                final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo info = connectivityManager.getActiveNetworkInfo();
                if ((info == null || !info.isConnected() && !info.isAvailable())) {
                    if (actividadMapa != null) {
                        actividadMapa.cambioInterenet(true);
                    }
                } else {
                    if (actividadMapa != null) {
                        actividadMapa.cambioInterenet(false);
                    }
                }
            }


            if (intent.getAction().equals(PRENDER_PANTALLA)) {
                ExtraLog.Log(TAG, "onReceive: PRENDER LISTA");
                if (utilidades.comprobarGPS(ServicioSockets.this)) {
                    powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                    keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
                    KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("INFO");
                    wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "INFO");
                    wakeLock.acquire(3000);
                    Intent intent2 = new Intent(context, ListaSolicitudesActivity.class);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent2.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
                    intent2.putExtra("pantalla", true);
                    intent2.putExtra("tamanioLista", solicitudesEntrantes.size());
                    context.startActivity(intent2);
                }
            }
            if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {
                statusLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                temperaturaBateria = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0) / 10;
            }
            if (intent.getAction().equals(Intent.ACTION_BATTERY_LOW)) {
                if (actividadMapa != null) {
                    actividadMapa.nofificarBateriaBaja(statusLevel);
                }
            }
            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    if (actividadMapa != null) {
                        actividadMapa.probarGPSInicio();
                    } else if (actividadLista != null) {
                        actividadLista.probarGPSInicio();
                    }
                }
            }
            if (intent.getAction().equals(TelephonyManager.ACTION_PHONE_STATE_CHANGED)) {
                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    isBanderaLlamada = false;
                }
                if ((state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))) {
                    isBanderaLlamada = false;
                }
                if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    isBanderaLlamada = true;
                }
            }
        }
    }

    private Location location;

    public int getTipoFormaPago() {
        return tipoFormaPago;
    }

    public void setTipoFormaPago(int tipoFormaPago) {
        this.tipoFormaPago = tipoFormaPago;
    }


    public void enviarEvento(int tipo, int estado) {
        JSONObject object = new JSONObject();
        try {
            object.put("tipo", tipo);
            object.put("estado", estado);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (isServerOnline) {
            if (mSocket != null) {
                mSocket.emit("registrar_evento",
                        getFechaActual(),
                        utilidades.getHoraActual(),
                        VariablesGlobales.NUM_ID_APLICATIVO,
                        spobtener.getInt(VariablesGlobales.ID_CIUDAD, 0),
                        spobtener.getInt(VariablesGlobales.ID_USUARIO, 0),
                        spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0),
                        object);
            }
        }
    }


    public void enviarEventoConductor(int idSolicitud, final int idEvento, Location location) {
        JSONObject object = new JSONObject();
        try {
            object.put("idSolicitud", idSolicitud);
            object.put("idEvento", idEvento);
            if (location != null) {
                object.put("latitud", location.getLatitude());
                object.put("longitud", location.getLongitude());
            } else {
                object.put("latitud", 0);
                object.put("longitud", 0);
            }
        } catch (JSONException e) {
            e.getMessage();
        }
        if (isServerOnline) {
            if (mSocket != null) {
                switch (idEvento) {
                    case 1:
                        if (solicitudSeleccionada != null && !solicitudSeleccionada.isEnvioBotonAbordo()) {
                            mSocket.emit("E_c", object, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    if (solicitudSeleccionada != null) {
                                        solicitudSeleccionada.setBotonAbordo(true);
                                    }
                                }
                            });
                        }
                        break;
                    default:
                        mSocket.emit("E_c", object, new Ack() {
                            @Override
                            public void call(Object... args) {

                            }
                        });
                        break;
                }

            }
        }
    }

    /**
     * @param estadoCobro    Guarda estados en el cual esta el cobro de la tarjeta de credito.
     * @param respuestaCobro Es la respuesta emitida por el servidor.
     * @param solicitud      Guarda La solicitud en tarjeta de credito.
     */
    public void setCobroEfectivo(int estadoCobro, String respuestaCobro, Solicitud solicitud) {
        SharedPreferences spCobroTarjetaCredito = getSharedPreferences("CobroEfectivo", MODE_PRIVATE);
        SharedPreferences.Editor editor = spCobroTarjetaCredito.edit();
        editor.putInt("estadoCobro", estadoCobro);
        if (respuestaCobro != null) {
            editor.putString("respuestaCobro", respuestaCobro);
        } else {
            editor.putString("respuestaCobro", null);
        }
        if (solicitud != null) {
            editor.putString("solicitud", solicitud.serializeSolicitud());
        } else {
            editor.putString("solicitud", "");
        }
        editor.apply();
    }

    public int getEstadoEfecitvo() {
        SharedPreferences spCobroTarjetaCredito = getSharedPreferences("CobroEfectivo", MODE_PRIVATE);
        return spCobroTarjetaCredito.getInt("estadoCobro", 0);
        // 0 = sin cobro 1= mostrar cobro 2= Cobro enviado 3= cobro respondido 4 = califica
    }

    public Solicitud getSolicitudEfectivo() {
        SharedPreferences spCobroTarjetaCredito = getSharedPreferences("CobroEfectivo", MODE_PRIVATE);
        return Solicitud.createSolicitud(spCobroTarjetaCredito.getString("solicitud", null));
    }

    public void ejecutarRespuestaEfectivoCobro() {
        SharedPreferences spCobroTarjetaCredito = getSharedPreferences("CobroEfectivo", MODE_PRIVATE);
        String datoRespuesta = spCobroTarjetaCredito.getString("respuestaCobro", "");
        try {
            JSONObject object = new JSONObject(datoRespuesta);
            if (object.getInt("e") == 1) {
                if (object.has("m")) {
                    reproducirTextAudio.speak(object.getString("m"));
                }
                if (actividadMapa != null) {
                    actividadMapa.envioCobro(object.getString("m"), 1);
                }
            }
            if (object.getInt("e") == -1) {
                if (object.has("m")) {
                    reproducirTextAudio.speak(object.getString("m"));
                }
                if (actividadMapa != null) {
                    actividadMapa.envioCobro(object.getString("m"), -1);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private CountDownTimer countDownTimer;


    public void cobroReloadEfectivo() {
        countDownTimer = new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (getEstadoEfecitvo() != 2) {
                    if (countDownTimer != null) {
                        countDownTimer.cancel();
                    }
                }
            }

            public void onFinish() {
                if (getEstadoEfecitvo() == 2) {
                    if (actividadMapa != null) {
                        actividadMapa.envioCobro("Ocurrió algo mientras se realizaba el proceso, intente nuevamente.", -2);
                    }
                    if (actividadLista != null) {
                        actividadLista.envioCobro("Ocurrió algo mientras se realizaba el proceso, intente nuevamente.", -2);
                    }
                }
            }

        }.start();
    }

    /**
     * Efectivo
     *
     * @param costo
     * @param saldoKtaxi
     * @param propina
     * @param numeroPasajero
     */

    public void efectivoCobrar(double costo, double saldoKtaxi, double propina, int numeroPasajero, final Solicitud solicitud) {
        if (!mSocket.connected()) {
            if (actividadMapa != null) {
                actividadMapa.envioCobro("Usted no se encuentra conectado a nuestro servicio, intente nuevamente.", -2);
            }
            if (actividadLista != null) {
                actividadLista.envioCobro("Usted no se encuentra conectado a nuestro servicio, intente nuevamente.", -2);
            }
            return;
        }
        cobroReloadEfectivo();
        setCobroEfectivo(2, "", solicitud);
        JSONObject aviso = new JSONObject();
        try {
            aviso.put("v", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
            aviso.put("u", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            if (location != null) {
                aviso.put("lt", location.getLatitude());
                aviso.put("lg", location.getLongitude());
            } else {
                aviso.put("lt", 0);
                aviso.put("lg", 0);
            }
            aviso.put("np", numeroPasajero);
            aviso.put("pro", propina);
            aviso.put("sal", saldoKtaxi);
            if (solicitud != null) {
                if (solicitud.isSolicitudCC()) {
                    if (solicitud.getIdSolicitud() == 0) {
                        aviso.put("isCC", 1); // esto se pone cuando es de call center
                        aviso.put("s", 0);
                    } else {
                        aviso.put("isCC", 1); // esto se pone cuando es de call center
                        aviso.put("s", (solicitud.getIdSolicitud()));
                    }
                } else {
                    if (solicitud.getIdSolicitud() != 0) {
                        aviso.put("s", solicitud.getIdSolicitud());
                    }
                }
            } else {
                aviso.put("s", 0);
            }

            aviso.put("c", costo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mSocket != null) {
            mSocket.emit("cobrar_con_driver", aviso, new Ack() {
                @Override
                public void call(Object... args) {
                    setCobroEfectivo(3, args[0].toString(), solicitud);
                    try {
                        JSONObject object = new JSONObject(args[0].toString());
                        if (object.getInt("e") == 1) {
                            if (object.has("m")) {
                                reproducirTextAudio.speak(object.getString("m"));
                            }
                            if (actividadMapa != null) {
                                actividadMapa.envioCobro(object.getString("m"), 1);
                            }
                            if (actividadLista != null) {
                                actividadLista.envioCobro(object.getString("m"), 1);
                            }
                        }
                        if (object.getInt("e") == -1) {
                            if (object.has("m")) {
                                reproducirTextAudio.speak(object.getString("m"));
                            }
                            if (actividadMapa != null) {
                                actividadMapa.envioCobro(object.getString("m"), -1);
                            }
                            if (actividadLista != null) {
                                actividadLista.envioCobro(object.getString("m"), -1);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }


    }

    /**
     * @param estadoCobro    Guarda estados en el cual esta el cobro de la tarjeta de credito.
     * @param respuestaCobro Es la respuesta emitida por el servidor.
     * @param solicitud      Guarda La solicitud en tarjeta de credito.
     */
    public void setCobroTarjetaCredito(int estadoCobro, String respuestaCobro, Solicitud solicitud) {
        SharedPreferences spCobroTarjetaCredito = getSharedPreferences("CobroTarjetaCredito", MODE_PRIVATE);
        SharedPreferences.Editor editor = spCobroTarjetaCredito.edit();
        editor.putInt("estadoCobro", estadoCobro);
        if (respuestaCobro != null) {
            editor.putString("respuestaCobro", respuestaCobro);
        } else {
            editor.putString("respuestaCobro", "");
        }
        if (solicitud != null) {
            editor.putString("solicitud", solicitud.serializeSolicitud());
        } else {
            editor.putString("solicitud", "");
        }
        editor.apply();
    }

    public int getEstadoCobroTarjetaCredito() {
        SharedPreferences spCobroTarjetaCredito = getSharedPreferences("CobroTarjetaCredito", MODE_PRIVATE);
        return spCobroTarjetaCredito.getInt("estadoCobro", 0);
        // 0 = sin cobro 1= cobro 2= Cobro enviado 3= cobro respondido 4 = califica
    }

    public Solicitud getSolicitudTarjetaCredito() {
        SharedPreferences spCobroTarjetaCredito = getSharedPreferences("CobroTarjetaCredito", MODE_PRIVATE);
        return Solicitud.createSolicitud(spCobroTarjetaCredito.getString("solicitud", null));
    }

    public void ejecutarRespuestaCobroTarjetaCredito() {
        SharedPreferences spCobroTarjetaCredito = getSharedPreferences("CobroTarjetaCredito", MODE_PRIVATE);
        String datoRespuesta = spCobroTarjetaCredito.getString("respuestaCobro", "");
        if (!datoRespuesta.isEmpty()) {
            try {
                JSONObject object = new JSONObject(datoRespuesta);
                if (object.has("success")) {
                    if (actividadMapa != null) {
                        actividadMapa.respuestaCobroTarjetCredito(1, 0, "Cobre en efectivo.");
                        setCobroTarjetaCredito(1, "", getSolicitudTarjetaCredito());
                    }
                    return;
                }
                if (object.has("error")) {
                    if (actividadMapa != null) {
                        actividadMapa.respuestaCobroTarjetCredito(2, 0, object.getString("m"));
                        setCobroTarjetaCredito(1, "", getSolicitudTarjetaCredito());
                    }
                    return;
                }
                if (object.has("e")) {
                    switch (object.getInt("e")) {
                        case -1:
                            if (actividadMapa != null) {
                                actividadMapa.respuestaCobroTarjetCredito(0, -1, object.getString("m"));
                                setCobroTarjetaCredito(1, "", getSolicitudTarjetaCredito());
                            }
                            break;
                        case 1:
                            if (actividadMapa != null) {
                                reproducirTextAudio.speak(object.getString("m"));
                                actividadMapa.respuestaCobroTarjetCredito(0, 1, object.getString("m"));
                                setCobroTarjetaCredito(4, "", getSolicitudTarjetaCredito());
                            }
                            break;
                        case 2:
                            if (actividadMapa != null) {
                                actividadMapa.respuestaCobroTarjetCredito(0, 2, object.getString("m"));
                                setCobroTarjetaCredito(1, "", getSolicitudTarjetaCredito());
                            }
                            break;
                        case 3:
                            if (actividadMapa != null) {
                                actividadMapa.respuestaCobroTarjetCredito(0, 3, object.getString("m"));
                                setCobroTarjetaCredito(1, "", getSolicitudTarjetaCredito());
                            }
                            break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void cobroReloadTarjetaCredito() {
        countDownTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (getEstadoCobroTarjetaCredito() != 2) {
                    countDownTimer.cancel();
                }
            }

            public void onFinish() {
                actividadMapa.envioCobro("Ocurrió algo mientras se realizaba el proceso, intente nuevamente.", -2);
            }
        }.start();
    }

    /**
     * Tarjeta Credito
     *
     * @param costo
     * @param saldoKtaxi
     * @param propina
     * @param numeroPasajero
     */
    public void cobroTarjetaCredito(double costo, double saldoKtaxi, double propina, int numeroPasajero, final Solicitud solicitud) {
        if (!mSocket.connected()) {
            if (actividadMapa != null) {
                actividadMapa.envioCobro("Usted no se encuentra conectado a nuestro servicio, intente nuevamente.", -2);
            }
            return;
        }
        setCobroTarjetaCredito(2, "", solicitud);
        cobroReloadTarjetaCredito();
        int idUsuario = spobtener.getInt(VariablesGlobales.ID_USUARIO, 0);
        String timeStanD = String.valueOf(Calendar.getInstance().getTime().getTime());
        String token = utilidades.SHA256(timeStanD + MetodosValidacion.MD5(idUsuario + ""));
        String key = MetodosValidacion.MD5(utilidades.SHA256(idUsuario + "") + timeStanD);
        JSONObject aviso = new JSONObject();
        try {
            aviso.put("v", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
            aviso.put("u", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            if (location != null) {
                aviso.put("lt", location.getLatitude());
                aviso.put("lg", location.getLongitude());
            } else {
                aviso.put("lt", 0);
                aviso.put("lg", 0);
            }
            aviso.put("np", numeroPasajero);
            aviso.put("pro", propina);
            aviso.put("sal", saldoKtaxi);
            if (solicitud.getIdSolicitud() != 0) {
                aviso.put("s", solicitud.getIdSolicitud());
            } else {
                aviso.put("s", 0);
            }
            aviso.put("c", utilidades.dosDecimales(ServicioSockets.this, costo));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mSocket != null) {
            mSocket.emit("cobrar_con_paymentez", String.valueOf(idUsuario).replace(" ", ""), timeStanD, token, key, aviso, new Ack() {
                @Override
                public void call(Object... args) {
                    setCobroTarjetaCredito(3, args[0].toString(), solicitud);
                    try {
                        JSONObject object = new JSONObject(args[0].toString());
                        if (object.has("success")) {
                            if (actividadMapa != null) {
                                actividadMapa.respuestaCobroTarjetCredito(1, 0, "Cobre en efectivo.");
                            }
                            return;
                        }
                        if (object.has("error")) {
                            if (actividadMapa != null) {
                                actividadMapa.respuestaCobroTarjetCredito(2, 0, object.getString("m"));
                            }
                            return;
                        }
                        if (object.has("e")) {
                            switch (object.getInt("e")) {
                                case -1:
                                    if (actividadMapa != null) {
                                        actividadMapa.respuestaCobroTarjetCredito(0, -1, object.getString("m"));
                                    }
                                    break;
                                case 1:
                                    if (actividadMapa != null) {
                                        reproducirTextAudio.speak(object.getString("m"));
                                        actividadMapa.respuestaCobroTarjetCredito(0, 1, object.getString("m"));
                                    }
                                    break;
                                case 2:
                                    if (actividadMapa != null) {
                                        actividadMapa.respuestaCobroTarjetCredito(0, 2, object.getString("m"));
                                    }
                                    break;
                                case 3:
                                    if (actividadMapa != null) {
                                        actividadMapa.respuestaCobroTarjetCredito(0, 3, object.getString("m"));
                                    }
                                    break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * @param precio el precio cual se cobro por el pedido. si es 0  este es por que aun no se a agrega ningun precio.
     */
    public void definirPrecioPedido(double precio) {
        SharedPreferences spPrecioPedido = getSharedPreferences(VariablesGlobales.SP_PRECIO_COMPRA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = spPrecioPedido.edit();
        editor.putLong("precio", (long) precio);
        editor.apply();
    }

    /***
     * Se obtiene el precio que ingreso el ususario por el pedido que esta realizando
     * @return
     */
    public long obtenerPrecioPedido() {
        SharedPreferences spPrecioPedido = getSharedPreferences(VariablesGlobales.SP_PRECIO_COMPRA, Context.MODE_PRIVATE);
        return spPrecioPedido.getLong("precio", 0);
    }

    public void efectivoCobrarConDriverPedido(double costo, double saldoKtaxi, double propina, int numeroPasajero, final Solicitud solicitud) {
        if (!mSocket.connected()) {
            if (actividadMapa != null) {
                actividadMapa.envioCobro("Usted no se encuentra conectado a nuestro servicio, intente nuevamente.", -2);
            }
            return;
        }
        setCobroEfectivo(2, "", solicitud);
        JSONObject aviso = new JSONObject();
        try {
            aviso.put("u", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            aviso.put("v", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
            if (location != null) {
                aviso.put("lt", location.getLatitude());
                aviso.put("lg", location.getLongitude());
            } else {
                aviso.put("lt", 0);
                aviso.put("lg", 0);
            }
            aviso.put("np", numeroPasajero);
            aviso.put("pro", propina);
            aviso.put("sal", saldoKtaxi);
            if (solicitud.getIdPedido() != 0) {
                aviso.put("p", solicitud.getIdPedido());
            } else {
                aviso.put("p", 0);
            }
            aviso.put("c", costo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mSocket != null) {
            mSocket.emit("cobrar_con_driver_pedido", aviso, new Ack() {
                @Override
                public void call(Object... args) {
                    setCobroEfectivo(3, args[0].toString(), solicitud);
                    try {
                        JSONObject object = new JSONObject(args[0].toString());
                        if (object.getInt("e") == 1) {
                            if (object.has("m")) {
                                reproducirTextAudio.speak(object.getString("m"));
                            }
                            if (actividadMapa != null) {
                                actividadMapa.envioCobro(object.getString("m"), 1);
                            }
                        }
                        if (actividadMapa != null) {
                            actividadMapa.envioCobro(object.getString("m"), 1);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private String mensajeDineroElectronico;

    private String usuario, pin, respuestaDinero;

    private int tokenDineroElectronico;

    public void DineroElectronicoCobrar(double monto, int idSolicitud, boolean isreenvio, final String usuario, final String pin, double saldoKtaxi, double propina, int numeroPasajero) {
        if (location != null) {
            estadoCobro = 3;
            JSONObject object = new JSONObject();
            if (location != null) {
                try {
                    this.usuario = usuario;
                    this.pin = pin;
                    object.put("lt", location.getLatitude());
                    object.put("lg", location.getLongitude());
                    object.put("c", monto);
                    object.put("idS", idSolicitud);
                    object.put("usuario", usuario);
                    object.put("pin", pin);
                    object.put("np", numeroPasajero);
                    object.put("pro", propina);
                    object.put("sal", saldoKtaxi);
                    object.put("u", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (mSocket.connected()) {
                if (mSocket != null) {
                    mSocket.emit("cobra_con_dinero_electronico", object, new Ack() {
                        @Override
                        public void call(Object... args) {
                            try {
                                JSONObject object = new JSONObject(args[0].toString());
                                if (object.has("error")) {
                                    if (actividadMapa != null) {
                                        if (object.has("m")) {
                                            actividadMapa.respuestaDineroElectronico(object.getString("m"), 7);
                                            reproducirTextAudio.speak(object.getString("m"));
                                            return;
                                        }
                                    }
                                }
                                if (object.has("e")) {
                                    switch (object.getInt("e")) {
                                        case 1:
                                            if (object.has("m")) {
                                                respuestaDinero = object.getString("m");
                                                mensajeDineroElectronico = object.getString("m");
                                                reproducirTextAudio.speak(mensajeDineroElectronico);
                                                if (actividadMapa != null) {
                                                    actividadMapa.respuestaDineroElectronico(mensajeDineroElectronico, 1);
                                                }
                                            }
                                            estadoCobro = 1;
                                            break;
                                        case 2:
                                            if (object.has("m")) {
                                                mensajeDineroElectronico = object.getString("m");
                                                respuestaDinero = object.getString("m");
                                                if (actividadMapa != null) {
                                                    actividadMapa.respuestaDineroElectronico(mensajeDineroElectronico, 2);
                                                }
                                                reproducirTextAudio.speak(mensajeDineroElectronico);
                                            }
                                            estadoCobro = 2;
                                            break;
                                        case 3: //Nuevo estado para contemplar volver ha hacer el emit con el recurso por debajo
                                            if (object.has("m")) {
                                                respuestaDinero = object.getString("m");
                                                tokenDineroElectronico = object.getInt("token");
                                                if (actividadMapa != null) {
                                                    actividadMapa.respuestaDineroElectronico(object.getString("m"), 7);
                                                }
                                                mensajeDineroElectronico = object.getString("m");
                                                reproducirTextAudio.speak(mensajeDineroElectronico);
                                            }
                                            estadoCobro = 7;
                                            break;
                                        case 6:
                                            if (object.has("m")) {
                                                respuestaDinero = object.getString("m");
                                                mensajeDineroElectronico = object.getString("m");
                                                reproducirTextAudio.speak(mensajeDineroElectronico);
                                                if (actividadMapa != null) {
                                                    actividadMapa.respuestaDineroElectronico(mensajeDineroElectronico, 7);
                                                }
                                            }
                                            estadoCobro = 6;
                                            break;
                                        default:
                                            if (actividadMapa != null) {
                                                if (object.has("m")) {
                                                    respuestaDinero = object.getString("m");
                                                    if (actividadMapa != null) {
                                                        actividadMapa.respuestaDineroElectronico(object.getString("m"), 7);
                                                    }
                                                    mensajeDineroElectronico = object.getString("m");
                                                    reproducirTextAudio.speak(mensajeDineroElectronico);
                                                }
                                            }
                                            estadoCobro = 6;
                                            break;
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } else {
                if (actividadMapa != null) {
                    actividadMapa.respuestaDineroElectronico("Esta desconectado del servidor cobre en efectivo", 10);
                }
            }
        }
    }

    public void guardarSaldo(float valor) {
        SharedPreferences spSaldoDineroElectronico = getSharedPreferences("dineroElectronico", MODE_PRIVATE);
        SharedPreferences.Editor editor = spSaldoDineroElectronico.edit();
        editor.putFloat("saldo", valor);
        editor.apply();
    }

    public float getSaldoDinero() {
        SharedPreferences spSaldoDineroElectronico = getSharedPreferences("dineroElectronico", MODE_PRIVATE);
        return spSaldoDineroElectronico.getFloat("saldo", 0);
    }


    public void verificarPago(double costo) {
        JSONObject dataEnvio = new JSONObject();
        try {
            dataEnvio.put("idS", idSolicitudSeleccionada);
            dataEnvio.put("u", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            dataEnvio.put("sD", costo);
            mSocket.emit("asignar_dinero_a_cuenta", dataEnvio, tokenDineroElectronico, new Ack() {
                @Override
                public void call(Object... args) {
                    try {
                        JSONObject resp = new JSONObject(args[0].toString());
                        if (resp.has("m")) {
                            reproducirTextAudio.speak(resp.getString("m"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void probarEstadoCobro() {
        if (estadoCobro == 3) {
            if (actividadMapa != null && usuario != null && pin != null) {
                actividadMapa.reenvioDineroElectronico(usuario, pin);
            }
        }
    }


    public int estadoCobro = 0;

    public void estadoPago() {
        switch (estadoCobro) {
            case 3:
                if (actividadMapa != null) {
                    actividadMapa.presentarDialogoCobro("Esperando confirmación");
                }
                break;
            case 1:
                if (actividadMapa != null) {
                    if (mensajeDineroElectronico.length() > 1) {
                        actividadMapa.respuestaDineroElectronico(mensajeDineroElectronico, 1);
                    }
                }
                break;
            case 2:
                if (actividadMapa != null) {
                    actividadMapa.respuestaDineroElectronico(mensajeDineroElectronico, 2);

                }
                break;
            case 6:
                if (actividadMapa != null) {
                    if (mensajeDineroElectronico.length() > 1) {
                        actividadMapa.respuestaDineroElectronico(mensajeDineroElectronico, 6);
                    }
                }
                estadoCobro = 5;
                break;
            case 7:
                if (actividadMapa != null) {
                    if (mensajeDineroElectronico.length() > 1) {
                        actividadMapa.respuestaDineroElectronico(mensajeDineroElectronico, 7);
                    }
                }
                break;
            case 4:
                if (actividadMapa != null) {
                    if (mensajeDineroElectronico.length() > 1) {
                        actividadMapa.respuestaDineroElectronico(mensajeDineroElectronico, 4);
                    }
                }
        }
    }


    public void voucherCobraVaucherClienteUido(double cobro, int idSolicitud, double saldoKtaxi, double propina, int numeroPasajero) {
        if (location != null) {
            JSONObject object = new JSONObject();
            if (location != null) {
                try {
                    object.put("lt", location.getLatitude());
                    object.put("lg", location.getLongitude());
                    object.put("c", cobro);
                    object.put("idS", idSolicitud);
                    object.put("np", numeroPasajero);
                    object.put("pro", propina);
                    object.put("sal", saldoKtaxi);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mSocket != null) {
                    mSocket.emit(VariablesGlobales.COBRA_OPERADOR_VAUCHER_CLIENTE_UIDO,
                            object, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    try {
                                        JSONObject respuesta = new JSONObject(args[0].toString());
                                        switch (respuesta.getInt("estado")) {
                                            case 19:
                                                if (actividadMapa != null) {
                                                    if (respuesta.has("m")) {
                                                        actividadMapa.cobroAceptado(true, respuesta.getString("m"), 1);
                                                    } else {
                                                        actividadMapa.cobroAceptado(true, "", 1);
                                                    }

                                                }
                                                break;
                                            default:
                                                if (actividadMapa != null) {
                                                    actividadMapa.cobroAceptado(false, "No se puede realizar el cobro.", 1);
                                                }
                                                break;
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            });
                }
            }
        }
    }

    public void voucherCobraVaucherConConfirmacion(double cobro, int idSolicitud, double saldoKtaxi, double propina, int numeroPasajero) {
        if (location != null) {
            isClienteVoucherEstado = false;
            JSONObject object = new JSONObject();
            try {
                object.put("c", cobro);
                object.put("lt", location.getLatitude());
                object.put("lg", location.getLongitude());
                object.put("idS", idSolicitud);
                object.put("np", numeroPasajero);
                object.put("pro", propina);
                object.put("sal", saldoKtaxi);
                if (mSocket != null) {
                    mSocket.emit(VariablesGlobales.COBRA_OPERADOR_VAUCHER_CON_CONFIRMACION,
                            object, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                Thread.sleep(15000);
                                                if (!isClienteVoucherEstado) {
                                                    if (actividadMapa != null) {
                                                        actividadMapa.cobroAceptado(false, "Pago no verificado", 1);
                                                    }
                                                }
                                            } catch (InterruptedException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }).start();
                                }
                            });
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void voucherConPin(int pin, double cobro, int idSolicitud, double saldoKtaxi, double propina, int numeroPasajero) {
        if (location != null) {
            JSONObject object = new JSONObject();
            if (location != null) {
                try {
                    object.put("pin", pin);
                    object.put("lt", location.getLatitude());
                    object.put("lg", location.getLongitude());
                    object.put("c", cobro);
                    object.put("u", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
                    object.put("v", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
                    object.put("idS", idSolicitud);
                    object.put("np", numeroPasajero);
                    object.put("pro", propina);
                    object.put("sal", saldoKtaxi);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mSocket != null) {
                    mSocket.emit(VariablesGlobales.COBRA_OPERADOR_VAUCHER_CON_PIN,
                            object, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    try {
                                        JSONObject respuesta = new JSONObject(args[0].toString());
                                        switch (respuesta.getInt("e")) {
                                            case 1:
                                                if (actividadMapa != null) {
                                                    actividadMapa.cobroAceptado(true, respuesta.getString("m"), 1);
                                                    reproducirTextAudio.speak(respuesta.getString("m"));
                                                }
                                                break;
                                            case -1:
                                                if (actividadMapa != null) {
                                                    actividadMapa.cobroAceptado(false, respuesta.getString("m"), 1);
                                                    reproducirTextAudio.speak(respuesta.getString("m"));
                                                }
                                                break;
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                }
            }
        }

    }


    public void voucherConPinCallCenter(int pin, double cobro, int idSolicitud, double saldoKtaxi, double propina, int numeroPasajero) {
        if (location != null) {
            JSONObject object = new JSONObject();
            if (location != null) {
                try {
                    object.put("pin", pin);
                    object.put("lt", location.getLatitude());
                    object.put("lg", location.getLongitude());
                    object.put("c", cobro);
                    object.put("u", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
                    object.put("v", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
                    object.put("idS", idSolicitud);
                    object.put("np", numeroPasajero);
                    object.put("pro", propina);
                    object.put("sal", saldoKtaxi);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mSocket != null) {
                    mSocket.emit(VariablesGlobales.COBRA_OPERADOR_VAUCHER_CON_PIN_CALL_CENTER,
                            object, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    try {
                                        JSONObject respuesta = new JSONObject(args[0].toString());
                                        switch (respuesta.getInt("e")) {
                                            case 1:
                                                if (actividadMapa != null) {
                                                    actividadMapa.cobroAceptado(true, respuesta.getString("m"), 1);
                                                    reproducirTextAudio.speak(respuesta.getString("m"));
                                                }
                                                break;
                                            case -1:
                                                if (actividadMapa != null) {
                                                    actividadMapa.cobroAceptado(false, respuesta.getString("m"), 1);
                                                    reproducirTextAudio.speak(respuesta.getString("m"));
                                                }
                                                break;
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                }
            }
        }

    }

    public void voucherOperadorReciveConfirmacionPago(int idSolicitud) {
        if (location != null) {
            JSONObject object = new JSONObject();
            if (location != null) {
                try {
                    object.put("lt", location.getLatitude());
                    object.put("lg", location.getLongitude());
                    object.put("idS", idSolicitud);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (mSocket != null) {
                    mSocket.emit(VariablesGlobales.OPERADOR_RECIVE_CONFIRMACION_PAGO,
                            object, new Ack() {
                                @Override
                                public void call(Object... args) {
                                }
                            });
                }
            }
        }

    }

    //Calificaion
    public void calificaConductorSolicitud(int calificacion, String observacion, int idSolicitud) {
        if (location != null) {
            resetIdEnvioCalificacion();
            setCobroTarjetaCredito(0, "", null);
            setCobroEfectivo(0, "", null);
            solicitudSeleccionada = null;
            estadoSolicitud = 0;
            JSONObject object = new JSONObject();
            try {
                object.put("lt", location.getLatitude());
                object.put("lg", location.getLongitude());
                object.put("vl", calificacion); //entero corto estrellas
                object.put("ob", observacion);// observacion
                object.put("idS", idSolicitud);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            guardarEstadoEnvio(false);
            if (mSocket != null) {
                mSocket.emit("califica_conductor_solicitud", object, new Ack() {
                    @Override
                    public void call(Object... args) {
                        Log.e(TAG, "call: " + args[0].toString());
                        try {
                            JSONObject data = new JSONObject(args[0].toString());
                            if (data.has("en")) {
                                if (data.getInt("en") == 1) {
                                    if (data.has("m")) {
                                        reproducirTextAudio.speak(data.getString("m"));
                                        return;
                                    }
                                }
                            }
                            reproducirTextAudio.speak("Gracias por tu comentario.");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            setClienteAceptoTiempo(false);
            isCronometroTiempo = false;
            borrarPreferencia();
            borrarCarreraSeleccionada();
            resetIdEnvioCalificacion();
            estadoSolicitud = 0;
            estadoPedido = 0;
            chatHistory.clear();
            if (actividadMapa != null) {
                actividadMapa.borraCarreraAlCancelar();
            }
        } else {
            resetIdEnvioCalificacion();//Cambiar  a cuando se califica la carrera
        }
    }

    public void calificaConductorPedido(int calificacion, String observacion, int idPedido) {
        resetIdEnvioCalificacion();
        setCobroTarjetaCredito(0, "", null);
        setCobroEfectivo(0, "", null);
        estadoSolicitud = 0;
        JSONObject object = new JSONObject();
        try {
            if (location != null) {
                object.put("lt", location.getLatitude());
                object.put("lg", location.getLongitude());
            }
            object.put("vl", calificacion); //entero corto estrellas
            object.put("ob", observacion);// observacion
            object.put("idP", idPedido);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mSocket != null) {
            mSocket.emit("califica_conductor_pedido", object, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e(TAG, "call: " + args[0].toString());
                    try {
                        JSONObject data = new JSONObject(args[0].toString());
                        if (data.has("en")) {
                            if (data.getInt("en") == 1) {
                                if (data.has("m")) {
                                    reproducirTextAudio.speak(data.getString("m"));
                                    return;
                                }
                            }
                        }
                        reproducirTextAudio.speak("Gracias por tu comentario.");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        setClienteAceptoTiempo(false);
        isCronometroTiempo = false;
        borrarPreferencia();
        borrarCarreraSeleccionada();
        resetIdEnvioCalificacion();
        estadoSolicitud = 0;
        estadoPedido = 0;
        chatHistory.clear();
        if (actividadMapa != null) {
            actividadMapa.borraCarreraAlCancelar();
        }
    }

    /***
     * RASTREO
     */
    public void enviarRastreo(int estado, boolean isButton) {
        PosicionNueva posicionNueva;
        if (location != null) {
            posicionNueva = new PosicionNueva();
            posicionNueva.setIdVehiculo(spobtener.getInt("idVehiculo", 0));
            posicionNueva.setFecha(utilidades.getTimeDeLocation(location, this));
            posicionNueva.setIdEquipo(spobtener.getInt("idEquipo", 0));
            posicionNueva.setLatitud(utilidades.formatDecimales(location.getLatitude(), 6));
            posicionNueva.setLongitud(utilidades.formatDecimales(location.getLongitude(), 6));
            posicionNueva.setAltitud((int) location.getAltitude());
            posicionNueva.setVelocidad((int) ((location.getSpeed() * 3600) / 1000));
            posicionNueva.setAcury(utilidades.getAcuryTramaEnvio((double) location.getAccuracy()));
            posicionNueva.setDireccion(utilidades.valorMaximo((int) location.getBearing(), 32767));
            posicionNueva.setBateria(utilidades.valorMaximo(utilidades.getBatteryLevel(this), 127));
            posicionNueva.setConexion(utilidades.getConnectivityStatus(getBaseContext()));
            posicionNueva.setGps(utilidades.statusGPS(this));
            posicionNueva.setEstado(estado);
            posicionNueva.setTemperatura(utilidades.valorMaximo(temperaturaBateria, 127));
            posicionNueva.setConsumo(utilidades.formatDecimales(utilidades.getPakagesUsoDatos(this), 2));
            posicionNueva.setTipoRed(utilidades.getTipoRed(this));
        } else {
            posicionNueva = new PosicionNueva();
            posicionNueva.setIdVehiculo(spobtener.getInt("idVehiculo", 0));
            posicionNueva.setFecha(utilidades.getTimeDeLocation(location, this));
            posicionNueva.setIdEquipo(spobtener.getInt("idEquipo", 0));
            posicionNueva.setLatitud(0);
            posicionNueva.setLongitud(0);
            posicionNueva.setAltitud(0);
            posicionNueva.setVelocidad(0);
            posicionNueva.setAcury(0);
            posicionNueva.setDireccion(0);
            posicionNueva.setBateria(utilidades.valorMaximo(utilidades.getBatteryLevel(this), 127));
            posicionNueva.setConexion(utilidades.getConnectivityStatus(getBaseContext()));
            posicionNueva.setGps(utilidades.statusGPS(this));
            posicionNueva.setEstado(estado);
            posicionNueva.setTemperatura(utilidades.valorMaximo(temperaturaBateria, 127));
            posicionNueva.setConsumo(utilidades.formatDecimales(utilidades.getPakagesUsoDatos(this), 2));
            posicionNueva.setTipoRed(utilidades.getTipoRed(this));
        }
        if (spobtener.getInt("idVehiculo", 0) != 0 && spobtener.getInt("idVehiculo", 0) != 0) {
            enviarRastreoServer(posicionNueva, isButton);
        }
    }


    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> rastreoHandle = null;
    private Runnable rastreoActualiza;

    /**
     * Comentado para lunding el rastreo.
     * En ktaxi deve estar activo
     */
    private void actualizarRastreoTaxi() {
        rastreoActualiza = new Runnable() {
            @Override
            public void run() {
                configuracionConrazonMalvado(definirCorazonMalvado(), false);
                ejecutaDataCorazon(definirCorazonMalvado());
                enviarRastreo(estadoBoton, false);
                ejecutarAcciones();
                if (tipoRastreoMap != null) {
                    if (tipoRastreoMap.containsKey(estadoBoton)) {
                        VariablesGlobales.TIEMPORASTREO = tipoRastreoMap.get(estadoBoton);
                    } else {
                        VariablesGlobales.TIEMPORASTREO = (VariablesGlobales.TIEMPORASTREO <= 5000 ? 17000 : VariablesGlobales.TIEMPORASTREO);
                    }
                }
                rastreoHandle.cancel(true);
                if (obtenerEstadoEnvio()) {
                    VariablesGlobales.TIEMPORASTREO = 5000;
                }
                rastreoHandle = scheduler.scheduleAtFixedRate(rastreoActualiza, VariablesGlobales.TIEMPORASTREO, VariablesGlobales.TIEMPORASTREO, TimeUnit.MILLISECONDS);
            }
        };
        rastreoHandle = scheduler.scheduleAtFixedRate(rastreoActualiza, VariablesGlobales.TIEMPORASTREO, VariablesGlobales.TIEMPORASTREO, TimeUnit.MILLISECONDS);
    }

    public void ejecutarCorazonMalvado() {
        configuracionConrazonMalvado(definirCorazonMalvado(), true);
    }


    public void configuracionConrazonMalvado(String[] definirCorazonMalvado, boolean isMapa) {
        String paquete = null;
        String nombreApp = null;
        boolean isDesintalar = false;
        boolean isCambiarOcupado = false;
        if (definirCorazonMalvado != null) {
            for (String dato : definirCorazonMalvado) {
                if (dato.split("&").length == 5) {
                    if (isMapa) {
                        paquete = dato.split("&")[1];
                        if (utilidades != null && utilidades.isInstaladaAplicacion(paquete, ServicioSockets.this)) {
                            if (dato.split("&")[2].equals("1")) {
                                isDesintalar = true;
                            }
                            nombreApp = dato.split("&")[3];
                            if (dato.split("&")[4].equals("1")) {
                                isCambiarOcupado = true;
                            }
                            break;
                        }
                    } else {
                        if (dato.split("&")[0].equals("1")) {
                            paquete = dato.split("&")[1];
                            if (utilidades != null && utilidades.isInstaladaAplicacion(paquete, ServicioSockets.this)) {
                                if (dato.split("&")[2].equals("1")) {
                                    isDesintalar = true;
                                }
                                nombreApp = dato.split("&")[3];
                                if (dato.split("&")[4].equals("1")) {
                                    isCambiarOcupado = true;
                                }
                                break;
                            } else {
                                return;
                            }
                        }
                    }
                    if (utilidades != null) {

                    }
                }
            }

        }
        if (paquete != null) {
            if (actividadMapa != null) {
                if (isDesintalar) {
                    actividadMapa.otraApp(nombreApp + " está instalada en su sistema, esto puede ocasionar que " + getResources().getString(R.string.app_name) + " no funcione al 100%, se recomienda desinstalar el app.", paquete, isDesintalar, isCambiarOcupado);
                } else {
                    actividadMapa.otraApp(nombreApp + " está instalada en su sistema, esto puede ocasionar que " + getResources().getString(R.string.app_name) + " no funcione al 100%, se recomienda forzar cierre del app.", paquete, isDesintalar, isCambiarOcupado);
                }
            } else if (actividadLista != null) {
                if (isDesintalar) {
                    actividadLista.otraApp(nombreApp + " está instalada en su sistema, esto puede ocasionar que " + getResources().getString(R.string.app_name) + " no funcione al 100%, se recomienda desinstalar el app.", paquete, isDesintalar, isCambiarOcupado);
                } else {
                    actividadLista.otraApp(nombreApp + " está instalada en su sistema, esto puede ocasionar que " + getResources().getString(R.string.app_name) + " no funcione al 100%, se recomienda forzar cierre del app.", paquete, isDesintalar, isCambiarOcupado);
                }
            } else if (actividadIniciarSesion != null) {
                if (isDesintalar) {
                    actividadIniciarSesion.otraApp(nombreApp + " está instalada en su sistema, esto puede ocasionar que " + getResources().getString(R.string.app_name) + " no funcione al 100%, se recomienda desinstalar el app.", paquete, isDesintalar);
                } else {
                    actividadIniciarSesion.otraApp(nombreApp + " está instalada en su sistema, esto puede ocasionar que " + getResources().getString(R.string.app_name) + " no funcione al 100%, se recomienda forzar cierre del app.", paquete, isDesintalar);
                }
            }
        }
    }

    public int numeroRamdom;
    public int numeroIntentos = 1;

    public void ejecutarAcciones() {
        String paquetes[] = null;
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 28:
                                if (objetoConfig.getInt("h") == 1) {
                                    if (!objetoConfig.getString("vd").isEmpty()) {
                                        paquetes = objetoConfig.getString("vd").split(",");
                                        for (String dato : paquetes) {
                                            if (dato.split("&").length == 7) {
                                                String paquete = dato.split("&")[1];
                                                int tipoAccion = Integer.parseInt(dato.split("&")[6]);
                                                if (utilidades.isInstaladaAplicacion(paquete, ServicioSockets.this)) {
                                                    switch (tipoAccion) {
                                                        case 1: //Cambia a estado ocupado
                                                            if (numeroRamdom == 0) {
                                                                numeroRamdom = (int) ((Math.random() * 30) + 1);
                                                            }
                                                            if (numeroIntentos == numeroRamdom) {
                                                                if (actividadMapa != null) {
                                                                    actividadMapa.corazonMalvadoCambiarOcupado();
                                                                } else if (actividadLista != null) {
                                                                    actividadLista.corazonMalvadoCambiarOcupado();
                                                                }
                                                                numeroIntentos = 1;
                                                                numeroRamdom = 0;
                                                            } else {
                                                                numeroIntentos++;
                                                            }
                                                            break;
                                                        case 2: //Mustra dialogo aleatorio
                                                            if (numeroRamdom == 0) {
                                                                numeroRamdom = (int) ((Math.random() * 30) + 1);
                                                            }
                                                            if (numeroIntentos == numeroRamdom) {
                                                                if (actividadMapa != null) {
                                                                    actividadMapa.corazonMalvadoDialogo(false);
                                                                } else if (actividadLista != null) {
                                                                    actividadLista.corazonMalvadoDialogo(false);
                                                                }
                                                                numeroIntentos = 1;
                                                                numeroRamdom = 0;
                                                            } else {
                                                                numeroIntentos++;
                                                            }
                                                            break;
                                                        case 3://Mustra dialogo y finaliza el aplicativo
                                                            if (numeroRamdom == 0) {
                                                                numeroRamdom = (int) ((Math.random() * 30) + 1);
                                                            }
                                                            if (numeroIntentos == numeroRamdom) {
                                                                if (actividadMapa != null) {
                                                                    actividadMapa.corazonMalvadoDialogo(true);
                                                                } else if (actividadLista != null) {
                                                                    actividadLista.corazonMalvadoDialogo(true);
                                                                }
                                                                numeroIntentos = 1;
                                                                numeroRamdom = 0;
                                                            } else {
                                                                numeroIntentos++;
                                                            }
                                                            break;

                                                        case 4: //Apaga escucha
                                                            if (actividadMapa != null) {
                                                                actividadMapa.corazonMalvadoCambiarOcupado();
                                                            } else if (actividadLista != null) {
                                                                actividadMapa.corazonMalvadoCambiarOcupado();
                                                            }
                                                            apagaEscuchas();
                                                            break;
                                                        case 5: //Cuelgue el aplicativo.
                                                            if (actividadMapa != null) {
                                                                actividadMapa.corazonMalvadoDialogoError(true);
                                                            } else if (actividadLista != null) {
                                                                actividadLista.corazonMalvadoDialogoError(true);
                                                            }
                                                            apagaEscuchas();
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.getMessage();
            }
        }
    }


    public void ejecutaDataCorazon(String[] definirCorazonMalvado) {
        if (definirCorazonMalvado != null && utilidades != null) {
            comprobarNuevos(definirCorazonMalvado);
            if (CorazonMalvado.getAll().size() >= definirCorazonMalvado.length) {
                for (CorazonMalvado c : CorazonMalvado.getAll()) {
                    if (spobtener.getInt(VariablesGlobales.ID_USUARIO, 0) != c.getIdUsuario()) {
                        CorazonMalvado.borrarTodos();
                        return;
                    }
                    if (!c.isEnvio()) {
                        envioServidorCorazon(c.getIdAplicativo(), c.getEstadoInstalada(), c);
                    } else {
                        if (c.isInstalada() != utilidades.isInstaladaAplicacion(c.getPaquete(), ServicioSockets.this)) {
                            if (c.isInstalada()) {
                                c.setEstadoInstalada(2);
                                c.setInstalada(false);
                                envioServidorCorazon(c.getIdAplicativo(), c.getEstadoInstalada(), c);
                            } else {
                                c.setEstadoInstalada(1);
                                c.setInstalada(true);
                                envioServidorCorazon(c.getIdAplicativo(), c.getEstadoInstalada(), c);
                            }
                        }
                    }
                }
            } else {
                for (String dato : definirCorazonMalvado) {
                    if (dato.split("&").length == 7) {
                        String paquete = dato.split("&")[1];
                        int idAplicativo = Integer.parseInt(dato.split("&")[5]);
                        int tipoAccion = Integer.parseInt(dato.split("&")[6]);
                        if (CorazonMalvado.isDentroLista(paquete) == null) {
                            if (utilidades.isInstaladaAplicacion(paquete, ServicioSockets.this)) {
                                cargarDataCorazon(paquete, false, utilidades.isInstaladaAplicacion(paquete, ServicioSockets.this), 1, idAplicativo, tipoAccion);
                            } else {
                                cargarDataCorazon(paquete, false, utilidades.isInstaladaAplicacion(paquete, ServicioSockets.this), 0, idAplicativo, tipoAccion);
                            }
                        }
                    }
                }
            }
        }
    }

    public void comprobarNuevos(String[] definirCorazonMalvado) {
        List<String> datosEliminados = new ArrayList<>();
        for (String dato : definirCorazonMalvado) {
            if (dato.split("&").length == 6) {
                String paquete = dato.split("&")[1];
                datosEliminados.add(paquete);
            }
        }
        if (datosEliminados.size() > 0) {
            CorazonMalvado.borrarYaNoEsta(datosEliminados);
        }
    }

    public void cargarDataCorazon(String paquete, boolean isEnvio, boolean isInstalada, int estadoInstalada, int idAplicativo, int tipoAccion) {
        CorazonMalvado corazonMalvado = new CorazonMalvado();
        corazonMalvado.setIdUsuario(spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
        corazonMalvado.setPaquete(paquete);
        corazonMalvado.setEnvio(isEnvio);
        corazonMalvado.setInstalada(isInstalada);
        corazonMalvado.setEstadoInstalada(estadoInstalada);
        corazonMalvado.setIdAplicativo(idAplicativo);
        corazonMalvado.setTipoAccion(tipoAccion);
        corazonMalvado.save();
    }


    public void envioServidorCorazon(int idApp, int evento, final CorazonMalvado corazonMalvado) {
        JSONObject object = new JSONObject();
        try {
            object.put("imei", utilidades.getImei(this));
            object.put("idUsuario", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            object.put("idVehiculo", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
            object.put("idApp", idApp);
            object.put("evento", evento);
            if (location != null) {
                object.put("latitud", location.getLatitude());
                object.put("longitud", location.getLongitude());
            } else {
                object.put("latitud", 0);
                object.put("longitud", 0);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (isServerOnline) {
            if (mSocket != null) {
                mSocket.emit("I_c", object, new Ack() {
                    @Override
                    public void call(Object... args) {
                        try {
                            JSONObject jsonObject = new JSONObject(args[0].toString());
                            if (jsonObject.getInt("en") == 1) {
                                corazonMalvado.setEnvio(true);
                                corazonMalvado.save();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    }


    public String[] definirCorazonMalvado() {
        String paquetes[] = null;
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 28:
                                if (objetoConfig.getInt("h") == 1) {
                                    if (!objetoConfig.getString("vd").isEmpty()) {
                                        paquetes = objetoConfig.getString("vd").split(",");
                                        return paquetes;
                                    }
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.getMessage();
            }
        }
        return paquetes;
    }


    public Location getLocationService() {
        return location;
    }


    @Override
    public void onLocationChange(Location location) {
        if (location != null) {
            if (actividadMapa != null) {
                actividadMapa.onLocation(location);
            } else if (actividadSaldoKtaxiPager != null) {
                actividadSaldoKtaxiPager.onLocation(location);
            } else if (compraServicios != null) {
                compraServicios.onLocation(location);
            } else if (compraServiciosMapa != null) {
                compraServiciosMapa.onLocation(location);
            }
            this.location = location;
        }
    }


    @Override
    public void onGpsDisable() {

    }

    @Override
    public void onGooglePlayServicesDisable(int status) {

    }

    @Override
    public void onGpsDisableNewApi(Status status) {
        this.status = status;
    }


    public Socket getmSocket() {
        return mSocket;
    }

    public boolean isCambiarLibre = true, isCambioLibreConsultado;

    public void controlCambioLibre() {
        if (!iscreateVideoDialogo) {
            scheduler.schedule(new Runnable() {
                public void run() {
                    isCambiarLibre = true;
                    isCambioLibreConsultado = true;
                }
            }, 8, SECONDS);
        }
    }

    public boolean iscreateVideoDialogo = false;


    public void descargarVideo(String dowload) {
        new ProgressBack().execute("http://testktaxi.kradac.com/media/mp4/" + dowload + ".mp4", dowload);
    }

    private class ProgressBack extends AsyncTask<String, Void, Boolean> {
        VideosPropaganda videosPropaganda;

        @Override
        protected void onPreExecute() {
            videosPropaganda = new VideosPropaganda();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            videosPropaganda.setNombreVideo(params[1]);
            return downloadFile(params[0], params[1] + ".mp4");
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                videosPorDescargar.remove(videosPropaganda.getNombreVideo());
                if (videosPorDescargar != null && videosPorDescargar.size() > 0) {
                    descargarVideo(videosPorDescargar.get(0));
                } else {
                    videosPorDescargar = null;
                }
            } else {
                if (videosPorDescargar != null && videosPorDescargar.size() > 0) {
                    descargarVideo(videosPorDescargar.get(0));
                } else {
                    videosPorDescargar = null;
                }
            }
            super.onPostExecute(aBoolean);
        }

    }


    public boolean isFile(String name) {
        String rootDir = Environment.getExternalStorageDirectory()
                + File.separator + "Video";
        File f = new File(rootDir, name + ".mp4");
        return f.exists();
    }

    private boolean downloadFile(String fileURL, String fileName) {
        try {
            InputStream is = null;
            String rootDir = Environment.getExternalStorageDirectory()
                    + File.separator + "Video";
            File rootFile = new File(rootDir);
            rootFile.mkdir();
            URL url = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.connect();
            is = url.openStream();
            if (c != null) {
                File f = new File(rootDir, fileName);
                FileOutputStream fos = new FileOutputStream(f);
                byte[] buffer = new byte[1024];
                int len1 = 0;
                if (is != null) {
                    while ((len1 = is.read(buffer)) > 0) {
                        fos.write(buffer, 0, len1);
                    }
                }
                if (fos != null) {
                    fos.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public void borrarVideosPulicitarios(String nombreArchivo) {
        String rootDir = Environment.getExternalStorageDirectory()
                + File.separator + "Video";
        File dir = new File(rootDir, nombreArchivo + ".mp4");
        dir.delete();
        try {
            VideosPropaganda.deleteVideo(nombreArchivo);
        } catch (NullPointerException e) {
            e.getMessage();
        }
    }

    public boolean isMensajeDeudaAbordo() {
        return isMensajeDeudaAbordo;
    }

    public void setMensajeDeudaAbordo(boolean mensajeDeuda) {
        isMensajeDeudaAbordo = mensajeDeuda;
    }

    public boolean isMensajeDeudaFinCarrera() {
        return isMensajeDeudaFinCarrera;
    }

    public void setMensajeDeudaFinCarrera(boolean mensajeDeudaFinCarrera) {
        isMensajeDeudaFinCarrera = mensajeDeudaFinCarrera;
    }

    public void setIdSolicitudSeleccionada(int idSolicitudSeleccionada) {
        this.idSolicitudSeleccionada = idSolicitudSeleccionada;
    }

    public void setIdPedidoSeleccionado(int idPedidoSeleccionado) {
        this.idPedidoSeleccionado = idPedidoSeleccionado;
    }

    private boolean isEjecutarCancelar;
    private int idTransaccionSaldoKtaxi;

    public void compraServicio(String saldo, int idEmpresa) {
        if (!mSocket.hasListeners(VariablesGlobales.ESCUCHA_KTAXI_DRIVER_ESCUCHA_MENSAJES)) {
            mSocket.on(VariablesGlobales.ESCUCHA_KTAXI_DRIVER_ESCUCHA_MENSAJES, onMensajes);
        }
        final JSONObject object = new JSONObject();
        try {
            object.put("idUsuario", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            object.put("idEmpresa", idEmpresa);
            object.put("saldo", saldo);
            object.put("latitud", location.getLatitude());
            object.put("longitud", location.getLongitude());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mSocket != null) {
            mSocket.emit("u_compra_servicio", object, new Ack() {
                @Override
                public void call(Object... args) {
                    try {
                        final JSONObject objectEntrada = new JSONObject(args[0].toString());
                        switch (objectEntrada.getInt("e")) {
                            case 1:
                                if (compraServicios != null) {
                                    compraServicios.onRespuestaCompra(objectEntrada.getString("pin"), objectEntrada.getLong("t"));
                                } else if (compraServiciosMapa != null) {
                                    compraServiciosMapa.onRespuestaCompra(objectEntrada.getString("pin"), objectEntrada.getLong("t"));
                                }
                                cancelarServicio(args[0].toString());
                                isEjecutarCancelar = true;
                                break;
                            case -1:
                                reproducirTextAudio.speak(objectEntrada.getString("m"));
                                if (compraServicios != null) {
                                    compraServicios.onRespuestaCompraServicio(objectEntrada.getString("m"));
                                } else if (compraServiciosMapa != null) {
                                    compraServiciosMapa.onRespuestaCompraServicio(objectEntrada.getString("m"));
                                }
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    public Thread hiloCancelarServicio;

    public void cancelarServicio(String dato) {
        try {
            final JSONObject objectEntrada = new JSONObject(dato);
            final long tiempo = objectEntrada.getLong("t");
            idTransaccionSaldoKtaxi = objectEntrada.getInt("idTransaccion");
            hiloCancelarServicio = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(tiempo);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (isEjecutarCancelar && hiloCancelarServicio != null && !hiloCancelarServicio.isInterrupted()) {
                        final JSONObject object = new JSONObject();
                        try {
                            object.put("idUsuario", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
                            object.put("idTransaccion", idTransaccionSaldoKtaxi);
                            object.put("latitud", location.getLatitude());
                            object.put("longitud", location.getLongitude());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (mSocket != null) {
                            mSocket.emit("u_cancelar_servicio", object, new Ack() {
                                @Override
                                public void call(Object... args) {
                                    JSONObject objectEntrada = null;
                                    try {
                                        objectEntrada = new JSONObject(args[0].toString());
                                        reproducirTextAudio.speak(objectEntrada.getString("m"));
                                        switch (objectEntrada.getInt("e")) {
                                            case 1:
                                                if (compraServicios != null) {
                                                    compraServicios.onRespuestaCompraServicio(objectEntrada.getString("m"));
                                                } else if (compraServiciosMapa != null) {
                                                    compraServiciosMapa.onRespuestaCompraServicio(objectEntrada.getString("m"));
                                                }
                                                break;
                                            case -1:
                                                if (compraServicios != null) {
                                                    compraServicios.onRespuestaCompraServicio(objectEntrada.getString("m"));
                                                } else if (compraServiciosMapa != null) {
                                                    compraServiciosMapa.onRespuestaCompraServicio(objectEntrada.getString("m"));
                                                }
                                                break;
                                            case -2:
                                                if (compraServicios != null) {
                                                    compraServicios.onRespuestaCompraServicio(objectEntrada.getString("m"));
                                                } else if (compraServiciosMapa != null) {
                                                    compraServiciosMapa.onRespuestaCompraServicio(objectEntrada.getString("m"));
                                                }
                                                break;
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            });
                        }
                    }
                }
            });
            hiloCancelarServicio.start();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void enviarValidacion(int idSolicitud) {
        if (location != null && location.getLatitude() != 0) {
            JSONObject data = new JSONObject();
            try {
                data.put("idSolicitud", idSolicitud);
                data.put("idEmpresa", spobtener.getInt(VariablesGlobales.ID_EMPRESA, 0));
                data.put("idVehiculo", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
                data.put("idUsuario", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
                data.put("latitud", location.getLatitude());
                data.put("longitud", location.getLongitude());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit(VariablesGlobales.EMIT_ESTADO_GPS_VALIDACION, data, new Ack() {
                @Override
                public void call(Object... args) {
                    if (actividadMapa != null) {
                        actividadMapa.respuestaValidacion("Gracias por el aporte.");
                    }
                }
            });
        }
    }

    public void enviarValidacionCallCenter(int tipo) {
        if (location != null && location.getLatitude() != 0) {
            JSONObject data = new JSONObject();
            try {
                data.put("t", tipo);
                data.put("idEmpresa", spobtener.getInt(VariablesGlobales.ID_EMPRESA, 0));
                data.put("idVehiculo", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
                data.put("idUsuario", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
                data.put("latitud", location.getLatitude());
                data.put("longitud", location.getLongitude());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit(VariablesGlobales.EMIT_ESTADO_GPS_VALIDACION_CALLCENTER, data, new Ack() {
                @Override
                public void call(Object... args) {
                    if (actividadMapa != null) {
                        actividadMapa.respuestaValidacion("Gracias por el aporte.");
                    }
                }
            });
        }
    }

    public void guardarEstadoEnvio(boolean estado) {
        SharedPreferences spTiempoEnvioRastreo = getSharedPreferences("spTiempoEnvioRastreo", MODE_PRIVATE);
        SharedPreferences.Editor editor = spTiempoEnvioRastreo.edit();
        editor.putBoolean("estado", estado);
        editor.apply();
    }

    public boolean obtenerEstadoEnvio() {
        SharedPreferences spTiempoEnvioRastreo = getSharedPreferences("spTiempoEnvioRastreo", MODE_PRIVATE);
        return spTiempoEnvioRastreo.getBoolean("estado", false);
    }


    public boolean isServerOnline() {
        return isServerOnline;
    }


    /**
     * Tarjeta Credito
     *
     * @param costo
     * @param saldoKtaxi
     * @param propina
     * @param numeroPasajero
     */
    public void cobroTarjetaCreditoNew(double costo, double saldoKtaxi, double propina, int numeroPasajero, final Solicitud solicitud) {
        if (!mSocket.connected()) {
            if (actividadMapa != null) {
                actividadMapa.envioCobro("Usted no se encuentra conectado a nuestro servicio, intente nuevamente.", -2);
            }
            return;
        }
        setCobroTarjetaCredito(2, "", solicitud);
        cobroReloadTarjetaCredito();
        int idUsuario = spobtener.getInt(VariablesGlobales.ID_USUARIO, 0);
        String timeStanD = String.valueOf(Calendar.getInstance().getTime().getTime());
        String token = utilidades.SHA256(timeStanD + MetodosValidacion.MD5(idUsuario + ""));
        String key = MetodosValidacion.MD5(utilidades.SHA256(idUsuario + "") + timeStanD);
        JSONObject aviso = new JSONObject();
        try {
            aviso.put("v", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
            aviso.put("u", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            if (location != null) {
                aviso.put("lt", location.getLatitude());
                aviso.put("lg", location.getLongitude());
            } else {
                aviso.put("lt", 0);
                aviso.put("lg", 0);
            }
            aviso.put("np", numeroPasajero);
            aviso.put("pro", propina);
            aviso.put("sal", saldoKtaxi);
            if (solicitud.getIdSolicitud() != 0) {
                aviso.put("s", solicitud.getIdSolicitud());
            } else {
                aviso.put("s", 0);
            }
            aviso.put("c", utilidades.dosDecimales(ServicioSockets.this, costo));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "cobroTarjetaCreditoNew: " + aviso.toString());
        if (mSocket != null) {
            mSocket.emit("cobrar_con_payphone_invitado", String.valueOf(idUsuario).replace(" ", ""), timeStanD, token, key, aviso, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e(TAG, "call cobroTarjetaCreditoNew: " + args[0].toString());
                    setCobroTarjetaCredito(3, args[0].toString(), solicitud);
                    try {
                        JSONObject object = new JSONObject(args[0].toString());
                        if (object.has("success")) {
                            if (actividadMapa != null) {
                                actividadMapa.respuestaCobroTarjetCredito(1, 0, "Cobre en efectivo.");
                            }
                            return;
                        }
                        if (object.has("error")) {
                            if (actividadMapa != null) {
                                if (object.has("m")) {
                                    actividadMapa.respuestaCobroTarjetCredito(2, 0, object.getString("m"));
                                } else {
                                    actividadMapa.respuestaCobroTarjetCredito(0, -1, "Se presento un error al realizar el cobro.");
                                }
                            }
                            return;
                        }
                        if (object.has("e")) {
                            if (object.getInt("e") <= -1) {
                                if (actividadMapa != null) {
                                    actividadMapa.respuestaCobroTarjetCredito(0, -1, object.getString("m"));
                                    return;
                                }
                            }
                            switch (object.getInt("e")) {
                                case 1:
                                    if (actividadMapa != null) {
                                        reproducirTextAudio.speak(object.getString("m"));
                                        actividadMapa.respuestaCobroTarjetCredito(0, 1, object.getString("m"));
                                    }
                                    break;
                                case 2:
                                    if (actividadMapa != null) {
                                        actividadMapa.respuestaCobroTarjetCredito(0, 2, object.getString("m"));
                                    }
                                    break;
                                case 3:
                                    if (actividadMapa != null) {
                                        actividadMapa.respuestaCobroTarjetCredito(0, 3, object.getString("m"));
                                    }
                                    break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    public void cobroTarjetaCreditoDirecto(double costo, double saldoKtaxi, double propina,
                                           int numeroPasajero, final Solicitud solicitud, String imei, String con,
                                           String number, String expirationMonth, String expirationYear, String verificationCode) {
        if (!mSocket.connected()) {
            if (onComunicacionAgregarTarjeta != null) {
                onComunicacionAgregarTarjeta.envioCobro("Usted no se encuentra conectado a nuestro servicio, intente nuevamente.", -2);
            }
            return;
        }
        setCobroTarjetaCredito(2, "", solicitud);
        cobroReloadTarjetaCredito();
        int idUsuario = spobtener.getInt(VariablesGlobales.ID_USUARIO, 0);
        String timeStanD = String.valueOf(Calendar.getInstance().getTime().getTime());
        String token = utilidades.SHA256(timeStanD + MetodosValidacion.MD5(idUsuario + ""));
        String key = MetodosValidacion.MD5(utilidades.SHA256(idUsuario + "") + timeStanD);
        JSONObject aviso = new JSONObject();
        try {
            aviso.put("v", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
            aviso.put("u", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            if (location != null) {
                aviso.put("lt", location.getLatitude());
                aviso.put("lg", location.getLongitude());
            } else {
                aviso.put("lt", 0);
                aviso.put("lg", 0);
            }
            aviso.put("np", numeroPasajero);
            aviso.put("pro", propina);
            aviso.put("sal", saldoKtaxi);
            if (solicitud.getIdSolicitud() != 0) {
                aviso.put("s", solicitud.getIdSolicitud());
            } else {
                aviso.put("s", 0);
            }
            aviso.put("imei", imei);
            aviso.put("con", con);
            aviso.put("c", utilidades.dosDecimales(ServicioSockets.this, costo));
            aviso.put("number", number);
            aviso.put("expirationMonth", expirationMonth);
            aviso.put("expirationYear", expirationYear);
            aviso.put("verificationCode", verificationCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "cobrar_con_payphone_con_tarjeta: " + aviso.toString());
        if (mSocket != null) {
            mSocket.emit("cobrar_con_payphone_con_tarjeta", String.valueOf(idUsuario).replace(" ", ""), timeStanD, token, key, aviso, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e(TAG, "call cobrar_con_payphone_con_tarjeta: " + args[0].toString());
                    setCobroTarjetaCredito(3, args[0].toString(), solicitud);
                    try {
                        JSONObject object = new JSONObject(args[0].toString());
                        if (object.has("success")) {
                            if (onComunicacionAgregarTarjeta != null) {
                                onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(1, 0, "Cobre en efectivo.");
                            }
                            return;
                        }
                        if (object.has("error")) {
                            if (onComunicacionAgregarTarjeta != null) {
                                if (object.has("m")) {
                                    onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(2, 0, object.getString("m"));
                                } else {
                                    onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(0, -1, "Se presento un error al realizar el cobro.");
                                }
                            }
                            return;
                        }
                        if (object.has("e")) {
                            if (object.getInt("e") <= -1) {
                                if (onComunicacionAgregarTarjeta != null) {
                                    onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(0, -1, object.getString("m"));
                                    return;
                                }
                            }
                            switch (object.getInt("e")) {
                                case 1:
                                    if (onComunicacionAgregarTarjeta != null) {
                                        reproducirTextAudio.speak(object.getString("m"));
                                        onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(0, 1, object.getString("m"));
                                    }
                                    break;
                                case 2:
                                    if (onComunicacionAgregarTarjeta != null) {
                                        onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(0, 2, object.getString("m"));
                                    }
                                    break;
                                case 3:
                                    if (onComunicacionAgregarTarjeta != null) {
                                        actividadMapa.respuestaCobroTarjetCredito(0, 3, object.getString("m"));
                                    }
                                    break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }


    public void cobroTarjetaCreditoDirectoCalle(double costo, double saldoKtaxi, double propina,
                                                int numeroPasajero, final Solicitud solicitud, String imei, String con,
                                                String number, String expirationMonth, String expirationYear, String verificationCode,
                                                String email, String phoneNumber, String optionalParameter) {

        if (!mSocket.connected()) {
            if (onComunicacionAgregarTarjeta != null) {
                onComunicacionAgregarTarjeta.envioCobro("Usted no se encuentra conectado a nuestro servicio, intente nuevamente.", -2);
            }
            return;
        }
        setCobroTarjetaCredito(2, "", solicitud);
        cobroReloadTarjetaCredito();
        int idUsuario = spobtener.getInt(VariablesGlobales.ID_USUARIO, 0);
        String timeStanD = String.valueOf(Calendar.getInstance().getTime().getTime());
        String token = utilidades.SHA256(timeStanD + MetodosValidacion.MD5(idUsuario + ""));
        String key = MetodosValidacion.MD5(utilidades.SHA256(idUsuario + "") + timeStanD);
        JSONObject aviso = new JSONObject();
        try {
            aviso.put("email", email);
            aviso.put("phoneNumber", phoneNumber);
            aviso.put("optionalParameter", optionalParameter);
            aviso.put("v", spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0));
            aviso.put("u", spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            if (location != null) {
                aviso.put("lt", location.getLatitude());
                aviso.put("lg", location.getLongitude());
            } else {
                aviso.put("lt", 0);
                aviso.put("lg", 0);
            }
            aviso.put("np", numeroPasajero);
            aviso.put("pro", propina);
            aviso.put("sal", saldoKtaxi);
            aviso.put("s", 0);
            aviso.put("imei", imei);
            aviso.put("con", con);
            aviso.put("c", utilidades.dosDecimales(ServicioSockets.this, costo));
            aviso.put("number", number);
            aviso.put("expirationMonth", expirationMonth);
            aviso.put("expirationYear", expirationYear);
            aviso.put("verificationCode", verificationCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "cobrar_calle_payphone_con_tarjeta: " + aviso.toString());
        if (mSocket != null) {
            mSocket.emit("cobrar_calle_payphone_con_tarjeta", String.valueOf(idUsuario).replace(" ", ""), timeStanD, token, key, aviso, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e(TAG, "call cobrar_calle_payphone_con_tarjeta: " + args[0].toString());
                    setCobroTarjetaCredito(3, args[0].toString(), solicitud);
                    try {
                        JSONObject object = new JSONObject(args[0].toString());
                        if (object.has("success")) {
                            if (onComunicacionAgregarTarjeta != null) {
                                onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(1, 0, "Cobre en efectivo.");
                            }
                            return;
                        }
                        if (object.has("error")) {
                            if (onComunicacionAgregarTarjeta != null) {
                                if (object.has("m")) {
                                    onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(2, 0, object.getString("m"));
                                } else {
                                    onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(0, -1, "Se presento un error al realizar el cobro.");
                                }
                            }
                            return;
                        }
                        if (object.has("e")) {
                            if (object.getInt("e") <= -1) {
                                if (onComunicacionAgregarTarjeta != null) {
                                    onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(0, -1, object.getString("m"));
                                    return;
                                }
                            }
                            switch (object.getInt("e")) {
                                case 1:
                                    if (onComunicacionAgregarTarjeta != null) {
                                        reproducirTextAudio.speak(object.getString("m"));
                                        onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(0, 1, object.getString("m"));
                                    }
                                    break;
                                case 2:
                                    if (onComunicacionAgregarTarjeta != null) {
                                        onComunicacionAgregarTarjeta.respuestaCobroTarjetCredito(0, 2, object.getString("m"));
                                    }
                                    break;
                                case 3:
                                    if (onComunicacionAgregarTarjeta != null) {
                                        actividadMapa.respuestaCobroTarjetCredito(0, 3, object.getString("m"));
                                    }
                                    break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}

