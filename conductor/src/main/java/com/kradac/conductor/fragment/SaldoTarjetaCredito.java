package com.kradac.conductor.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.RvAdapterSaldoKtaxi;
import com.kradac.conductor.adaptadoreslista.RvAdapterSaldoTarjetaCredito;
import com.kradac.conductor.extras.MetodosValidacion;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionSaldoKtaxi;
import com.kradac.conductor.modelo.*;
import com.kradac.conductor.modelo.SaldoKtaxi;
import com.kradac.conductor.presentador.SaldoKtaxiPresentador;
import com.kradac.conductor.presentador.TarjetaCreditoPresentador;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SaldoTarjetaCredito extends Fragment implements TarjetaCreditoPresentador.OnComunicacionTarjetaCredito {


    @BindView(R2.id.lista_item_saldo)
    RecyclerView listaItemSaldo;
    @BindView(R2.id.tv_total_cuenta)
    TextView tvTotalCuenta;
    Unbinder unbinder;
    @BindView(R2.id.progress_espera)
    ProgressBar progressEspera;

    private RecyclerView.LayoutManager lManager;
    private RvAdapterSaldoTarjetaCredito rvAdapterSaldoKtaxi;
    private TarjetaCreditoPresentador saldoKtaxiPresentador;
    private SharedPreferences spLogin;
    private boolean isActivo;
    private Utilidades utilidades;

    public SaldoTarjetaCredito() {
        // Required empty public constructor
        utilidades = new Utilidades();
        saldoKtaxiPresentador = new TarjetaCreditoPresentador(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_saldo_ktaxi, container, false);
        spLogin = getActivity().getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        unbinder = ButterKnife.bind(this, view);
        lManager = new LinearLayoutManager(getActivity());
        listaItemSaldo.setLayoutManager(lManager);
        pedirDatosServidor();
        isActivo = true;
        getDatos();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        isActivo = false;
    }

    public void getDatos() {
        rvAdapterSaldoKtaxi = new RvAdapterSaldoTarjetaCredito(getActivity(), new ArrayList<ConsultarSaldoTarjetaCredito.LS>());
        listaItemSaldo.setAdapter(rvAdapterSaldoKtaxi);
    }

    @Override
    public void respuestaSaldoTarjetaCredito(ConsultarSaldoTarjetaCredito consultarSaldoTarjetaCredito) {
        if (isActivo) {
            progressEspera.setVisibility(View.GONE);
            if (consultarSaldoTarjetaCredito != null) {
                if (consultarSaldoTarjetaCredito.getEn() == 1) {
                    rvAdapterSaldoKtaxi.addAll((ArrayList<ConsultarSaldoTarjetaCredito.LS>) consultarSaldoTarjetaCredito.getLS());
                    tvTotalCuenta.setText("Total: ".concat(utilidades.dosDecimales(getContext(),totalSaldo(consultarSaldoTarjetaCredito.getLS()))).concat(" USD"));
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(consultarSaldoTarjetaCredito.getM())
                            .setTitle("Alerta")
                            .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getDatos();
                                }
                            })
                            .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getActivity().finish();
                                }
                            }).show();
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("No se puede obtener los datos, ¿desea intentar nuevamente?.")
                        .setTitle("Alerta")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                getDatos();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                getActivity().finish();
                            }
                        }).show();
            }
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        getDatos();
        pedirDatosServidor();
    }

    public void pedirDatosServidor() {
        int idUsuario = spLogin.getInt(VariablesGlobales.ID_USUARIO, 0);
        String timeStanD = String.valueOf(Calendar.getInstance().getTime().getTime());
        String token = utilidades.SHA256(idUsuario + MetodosValidacion.MD5(timeStanD));
        String key = MetodosValidacion.MD5(timeStanD + idUsuario + timeStanD);
        tvTotalCuenta.setText("Total: 0.00 USD");
        saldoKtaxiPresentador.creditoConsultarSaldo(idUsuario, token, key, timeStanD, spLogin.getInt("idVehiculo", 0), spLogin.getInt("idCiudad", 0));
        progressEspera.setVisibility(View.VISIBLE);
    }

    public double total;

    public double totalSaldo(List<ConsultarSaldoTarjetaCredito.LS> datos) {
        total = 0;
        for (ConsultarSaldoTarjetaCredito.LS ls : datos) {
            total = total + ls.getCredito();
        }
        return total;
    }
}
