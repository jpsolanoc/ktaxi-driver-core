package com.kradac.conductor.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.RvAdapterSaldoKtaxi;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionSaldoKtaxi;
import com.kradac.conductor.presentador.SaldoKtaxiPresentador;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SaldoKtaxi extends Fragment implements OnComunicacionSaldoKtaxi {


    @BindView(R2.id.lista_item_saldo)
    RecyclerView listaItemSaldo;
    @BindView(R2.id.tv_total_cuenta)
    TextView tvTotalCuenta;
    Unbinder unbinder;
    @BindView(R2.id.progress_espera)
    ProgressBar progressEspera;

    private RecyclerView.LayoutManager lManager;
    private RvAdapterSaldoKtaxi rvAdapterSaldoKtaxi;
    private SaldoKtaxiPresentador saldoKtaxiPresentador;
    private SharedPreferences spLogin;
    private boolean isActivo;
    private Utilidades utilidades;

    public SaldoKtaxi() {
        // Required empty public constructor
        utilidades = new Utilidades();
        saldoKtaxiPresentador = new SaldoKtaxiPresentador(this, null, null);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_saldo_ktaxi, container, false);
        spLogin = getActivity().getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        unbinder = ButterKnife.bind(this, view);
        lManager = new LinearLayoutManager(getActivity());
        listaItemSaldo.setLayoutManager(lManager);
        pedirDatosServidor();
        isActivo = true;
        getDatos();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        isActivo = false;
    }

    public void getDatos() {
        rvAdapterSaldoKtaxi = new RvAdapterSaldoKtaxi(getActivity(), new ArrayList<com.kradac.conductor.modelo.SaldoKtaxi.ItemSaldoKtaxi.LS>());
        listaItemSaldo.setAdapter(rvAdapterSaldoKtaxi);
    }

    @Override
    public void repsuestaListaSaldoKtaxi(com.kradac.conductor.modelo.SaldoKtaxi.ItemSaldoKtaxi itemSaldoKtaxi) {
        if (isActivo) {
            progressEspera.setVisibility(View.GONE);
            if (itemSaldoKtaxi != null) {
                if (itemSaldoKtaxi.getEn() == 1) {
                    rvAdapterSaldoKtaxi.addAll((ArrayList<com.kradac.conductor.modelo.SaldoKtaxi.ItemSaldoKtaxi.LS>) itemSaldoKtaxi.getLS());
                    tvTotalCuenta.setText("Total: ".concat(utilidades.dosDecimales(getContext(),totalSaldo(itemSaldoKtaxi.getLS()))).concat(" " + definirHindCobro()));
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(itemSaldoKtaxi.getM())
                            .setTitle("Alerta")
                            .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getDatos();
                                }
                            })
                            .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getActivity().finish();
                                }
                            }).show();
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("No se puede obtener los datos, ¿desea intentar nuevamente?.")
                        .setTitle("Alerta")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                getDatos();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                getActivity().finish();
                            }
                        }).show();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        getDatos();
        pedirDatosServidor();
    }

    public void pedirDatosServidor() {
        tvTotalCuenta.setText("Total: 0.00 " + definirHindCobro());
        saldoKtaxiPresentador.saldoConsultar(spLogin.getInt("idVehiculo", 0), spLogin.getInt("idUsuario", 0), spLogin.getInt("idCiudad", 0));
        progressEspera.setVisibility(View.VISIBLE);
    }

    public double total;

    public double totalSaldo(List<com.kradac.conductor.modelo.SaldoKtaxi.ItemSaldoKtaxi.LS> datos) {
        total = 0;
        for (com.kradac.conductor.modelo.SaldoKtaxi.ItemSaldoKtaxi.LS ls : datos) {
            total = total + ls.getSaldo();
        }
        return total;
    }

    private SharedPreferences spParametrosConfiguracion;

    public String definirHindCobro() {
        String respuesta = "Usd";
        spParametrosConfiguracion = getActivity().getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 12:
                                if (objetoConfig.getInt("h") == 1) {
                                    respuesta = objetoConfig.getString("nb");
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                Log.e("", "definirMoneda: ", e);
            }
        }
        return respuesta;
    }
}
