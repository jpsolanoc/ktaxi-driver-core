package com.kradac.conductor.fragment;

import android.content.ContentValues;
import android.content.Context;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.ChatAdapter;
import com.kradac.conductor.interfaces.OnHandleEnviarAudio;
import com.kradac.conductor.modelo.chatBroadCast;
import com.kradac.conductor.modelo.ChatMessage;
import com.kradac.conductor.vista.ChatActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AudioCallCenter extends Fragment implements chatBroadCast.OnMensajesCallCenter {

    private static final String TAG = AudioCallCenter.class.getName();
    @BindView(R2.id.messagesContainer)
    ListView messagesContainer;
    private ChatAdapter adapter;
    private OnHandleEnviarAudio mCallback;
    private ChatActivity chatActivity = new ChatActivity();


    public AudioCallCenter() {

    }

    @BindView(R2.id.btn_audio_call_center)
    ImageButton btnAudioCallCenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_audio_call_center, container, false);
        ButterKnife.bind(this, view);
        adapter = new ChatAdapter(getActivity(), new ArrayList<ChatMessage>());
        btnAudioCallCenter.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_UP:
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                if (mediaRecorder != null) {
                                    if (isPlay) {
                                        try {
                                            Thread.sleep(1000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        mediaRecorder.stop();
                                    }
                                    mCallback.onGrabarEnviarAudio(nameAudio, 1);
                                    isPlay = false;
                                    mediaRecorder = null;
                                    mensajesUsuarioCallCenter();
                                }
                            }
                        }).start();


                        break;
                }
                return false;
            }
        });
        btnAudioCallCenter.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                nameAudio = mCallback.onServicioSocket().nameAudio();
                recordAudio(ChatActivity.path + nameAudio);
                return false;
            }
        });
        messagesContainer.setAdapter(adapter);
        if (mCallback.onServicioSocket() != null) {
            mensajesUsuarioCallCenter();
        }
        return view;
    }

    private void scroll() {
        messagesContainer.setSelection(messagesContainer.getCount() - 1);
    }

    public void mensajesUsuarioCallCenter() {
        Log.e(TAG, "mensajesUsuarioCallCenter: ANTES");
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (adapter != null) {
                        if (mCallback.getChatCallCenter() != null) {
                            Log.e(TAG, "mensajesUsuarioCallCenter: " + mCallback.getChatCallCenter().size());
                            adapter.removeAll();
                            adapter.notifyDataSetChanged();
                            adapter.add(mCallback.getChatCallCenter());
                            adapter.notifyDataSetChanged();
                            scroll();
                        }
                    }
                }
            });
        }


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnHandleEnviarAudio) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " debe implementar OnHeadlineSelectedListener");
        }
    }

    private MediaRecorder mediaRecorder;
    private boolean isPlay;
    String nameAudio;

    public void recordAudio(String fileName) {
        mediaRecorder = new MediaRecorder();
        ContentValues values = new ContentValues(3);
        values.put(MediaStore.MediaColumns.TITLE, fileName);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        mediaRecorder.setAudioSamplingRate(16000);
        mediaRecorder.setAudioEncodingBitRate(256);
        mediaRecorder.setOutputFile(fileName);
        try {
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mediaRecorder.start();
            isPlay = true;
        } catch (IllegalStateException ignored) {
        }
    }

    @Override
    public void cargarMensajes() {
        mensajesUsuarioCallCenter();
    }
}
