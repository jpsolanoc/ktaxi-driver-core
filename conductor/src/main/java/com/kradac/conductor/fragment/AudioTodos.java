package com.kradac.conductor.fragment;

import android.content.ContentValues;
import android.content.Context;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.ChatAdapter;
import com.kradac.conductor.interfaces.OnHandleEnviarAudio;
import com.kradac.conductor.modelo.ChatMessage;
import com.kradac.conductor.vista.ChatActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AudioTodos extends Fragment {

    private static final String TAG = AudioTodos.class.getName();

    @BindView(R2.id.btn_audio_todos)
    ImageButton btnAudioTodos;
    @BindView(R2.id.messagesContainer)
    ListView messagesContainer;
    private ChatAdapter adapter;
    private ChatActivity chatActivity = new ChatActivity();

    private OnHandleEnviarAudio mCallback;

    public AudioTodos() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_audio_todos, container, false);
        ButterKnife.bind(this, view);
        adapter = new ChatAdapter(getActivity(), new ArrayList<ChatMessage>());
        btnAudioTodos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_UP:
                        if (mediaRecorder != null) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    if (isPlay) {
                                        try {
                                            Thread.sleep(1000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        mediaRecorder.stop();
                                    }
                                    mCallback.onGrabarEnviarAudio(nameAudio, 3);
                                    isPlay = false;
                                    mediaRecorder = null;
                                    mensajesUsuarioTodos();
                                }
                            }).start();

                        }
                        break;
                }
                return false;
            }
        });
        btnAudioTodos.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                nameAudio = mCallback.onServicioSocket().nameAudio();
                recordAudio(ChatActivity.path + nameAudio);
                return false;
            }
        });
        messagesContainer.setAdapter(adapter);
        if (mCallback.onServicioSocket() != null) {
            mensajesUsuarioTodos();
        }
        return view;
    }

    private void scroll() {
        messagesContainer.setSelection(messagesContainer.getCount() - 1);
    }

    public void mensajesUsuarioTodos() {
        Log.e(TAG, "mensajesUsuarioTodos: ANTES" );
        if (getActivity()!=null){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (adapter != null) {
                        if (mCallback.getChatBroadCast()!=null){
                            Log.e(TAG, "mensajesUsuarioTodos: "+  + mCallback.getChatBroadCast().size());
                            adapter.removeAll();
                            adapter.notifyDataSetChanged();
                            adapter.add(mCallback.getChatBroadCast());
                            adapter.notifyDataSetChanged();
                            scroll();
                        }
                    }
                }
            });
        }


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnHandleEnviarAudio) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " debe implementar OnHeadlineSelectedListener");
        }
    }

    private MediaRecorder mediaRecorder;
    private boolean isPlay;
    String nameAudio;

    public void recordAudio(String fileName) {
        mediaRecorder = new MediaRecorder();
        ContentValues values = new ContentValues(3);
        values.put(MediaStore.MediaColumns.TITLE, fileName);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        mediaRecorder.setAudioSamplingRate(16000);
        mediaRecorder.setAudioEncodingBitRate(256);
        mediaRecorder.setOutputFile(fileName);
        try {
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mediaRecorder.start();
            isPlay = true;
        } catch (IllegalStateException ignored) {
        }
    }


}
