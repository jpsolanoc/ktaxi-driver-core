package com.kradac.conductor.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.RvAdapterTransaccionesKtaxi;
import com.kradac.conductor.adaptadoreslista.RvAdapterTransaccionesTarjetaCredito;
import com.kradac.conductor.extras.MetodosValidacion;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionTransaccionesSaldoKtaxi;
import com.kradac.conductor.modelo.ConsultarMovimientos;
import com.kradac.conductor.modelo.TransaccionesSaldoKtaxi;
import com.kradac.conductor.presentador.TarjetaCreditoPresentador;
import com.kradac.conductor.presentador.TransaccionesTarjetaCreditoPresentador;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransaccionesTarjetaCreditoKtaxiFragment extends Fragment implements TransaccionesTarjetaCreditoPresentador.OnComunicacionMovimientos {


    @BindView(R2.id.btn_anterior)
    ImageButton btnAnterior;
    @BindView(R2.id.tv_mes_anio)
    TextView tvMesAnio;
    @BindView(R2.id.btn_siguiente)
    ImageButton btnSiguiente;
    @BindView(R2.id.reciclador)
    RecyclerView reciclador;
    Unbinder unbinder;
    @BindView(R2.id.progress_espera)
    ProgressBar progressEspera;
    @BindView(R2.id.tv_sin_datos)
    TextView tvSinDatos;
    @BindView(R2.id.tv_total_ingresos)
    TextView tvTotalIngresos;
    @BindView(R2.id.tv_total_egresos)
    TextView tvTotalEgresos;
    private Utilidades utilidades;
    private SharedPreferences spLogin;
    private TransaccionesTarjetaCreditoPresentador consultar_transacciones;
    private RvAdapterTransaccionesTarjetaCredito rvAdapterTransaccionesKtaxi;
    private RecyclerView.LayoutManager lManager;
    private boolean isActivo;


    private int anio, mes;

    public TransaccionesTarjetaCreditoKtaxiFragment() {
        // Required empty public constructor

        utilidades = new Utilidades();
        consultar_transacciones = new TransaccionesTarjetaCreditoPresentador(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transacciones_saldo_ktaxi, container, false);
        spLogin = getActivity().getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        anio = utilidades.getAnio();
        mes = utilidades.getMes();
        unbinder = ButterKnife.bind(this, view);
        lManager = new LinearLayoutManager(getActivity());
        reciclador.setLayoutManager(lManager);
        isActivo = true;
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getDataServer(anio, mes + 1);
        cargarData();
    }

    public void cargarData() {
        rvAdapterTransaccionesKtaxi = new RvAdapterTransaccionesTarjetaCredito(getActivity(), new ArrayList<ConsultarMovimientos.LT>());
        reciclador.setAdapter(rvAdapterTransaccionesKtaxi);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        isActivo = false;
    }

    @OnClick({R2.id.btn_anterior, R2.id.btn_siguiente})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.btn_anterior) {
            if (mes == 0) {
                mes = 11;
                anio = anio - 1;
            } else {
                mes--;
            }
            if (utilidades.getAnio() == anio) {
                tvMesAnio.setText(utilidades.getMesString(mes).toUpperCase());
            } else {
                tvMesAnio.setText(anio + " " + utilidades.getMesString(mes).toUpperCase());
            }

            getDataServer(anio, mes + 1);

        } else if (i == R.id.btn_siguiente) {
            if (mes == 11) {
                mes = 0;
                anio = anio + 1;
            } else {
                mes++;
            }
            if (utilidades.getAnio() == anio) {
                tvMesAnio.setText(utilidades.getMesString(mes).toUpperCase());
            } else {
                tvMesAnio.setText(anio + " " + utilidades.getMesString(mes).toUpperCase());
            }
            getDataServer(anio, mes + 1);

        }
    }

    public void getDataServer(int anio, int mes) {
        tvTotalIngresos.setText("Total ingresos: 0.00");
        tvTotalEgresos.setText("Total egresos: 0.00");
        if (rvAdapterTransaccionesKtaxi != null) {
            rvAdapterTransaccionesKtaxi.clear();
        }
        int idUsuario = spLogin.getInt(VariablesGlobales.ID_USUARIO, 0);
        String timeStanD = String.valueOf(Calendar.getInstance().getTime().getTime());
        String token = utilidades.SHA256(idUsuario + MetodosValidacion.MD5(timeStanD));
        String key = MetodosValidacion.MD5(timeStanD + idUsuario + timeStanD);
        consultar_transacciones.consultarMovimientos(idUsuario, token, key, timeStanD, spLogin.getInt("idVehiculo", 0), spLogin.getInt("idCiudad", 0), anio, mes);
        progressEspera.setVisibility(View.VISIBLE);
        tvSinDatos.setVisibility(View.GONE);

    }

    @Override
    public void respuestaConsultarMovimientos(ConsultarMovimientos consultarMovimientos) {
        if (isActivo) {
            progressEspera.setVisibility(View.GONE);
            if (consultarMovimientos != null) {
                if (consultarMovimientos.getEn() == 1) {
                    if (consultarMovimientos.getLT().size() > 0) {
                        rvAdapterTransaccionesKtaxi.addAll((ArrayList<ConsultarMovimientos.LT>) consultarMovimientos.getLT());
                        totalIngresos(consultarMovimientos.getLT());
                        totalEgresos(consultarMovimientos.getLT());
                    } else {
                        tvSinDatos.setVisibility(View.VISIBLE);
                        tvTotalEgresos.setText("Total egresos: 0.00");
                        tvTotalEgresos.setText("Total egresos: 0.00");
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(consultarMovimientos.getM())
                            .setTitle("Alerta")
                            .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getDataServer(anio, mes);
                                }
                            })
                            .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getActivity().finish();
                                }
                            }).show();
                    tvSinDatos.setVisibility(View.VISIBLE);
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("No se puede obtener los datos, ¿desea intentar nuevamente?.")
                        .setTitle("Alerta")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                getDataServer(anio, mes);
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                getActivity().finish();
                            }
                        }).show();
                tvSinDatos.setVisibility(View.VISIBLE);
            }
        }
    }


    public void totalIngresos(List<ConsultarMovimientos.LT> transaccionesSaldoKtaxi) {
        double saldoIngresos = 0;
        for (ConsultarMovimientos.LT lt : transaccionesSaldoKtaxi) {
            if (lt.getIT() == 1) {
                if (lt.getIE() == 2 && lt.getReverso() == 0) {
                    saldoIngresos = lt.getCredito() + saldoIngresos;
                }
            }
        }
        tvTotalIngresos.setText("Total ingresos: " + utilidades.dosDecimales(getContext(),saldoIngresos));
    }

    public void totalEgresos(List<ConsultarMovimientos.LT> transaccionesSaldoKtaxi) {
        double saldoEgresos = 0;
        for (ConsultarMovimientos.LT lt : transaccionesSaldoKtaxi) {
            if (lt.getIT() == 2) {
                if (lt.getIE() == 2 && lt.getReverso() == 0) {
                    saldoEgresos = lt.getCredito() + saldoEgresos;
                }
            }
        }
        tvTotalEgresos.setText("Total egresos: " + utilidades.dosDecimales(getContext(),saldoEgresos));
    }

}
