package com.kradac.conductor.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.RvAdapterCompraServiciosCategoria;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionCompraServicio;
import com.kradac.conductor.modelo.CompraServiciosCategorias;
import com.kradac.conductor.presentador.CompraServiciosPresentador;
import com.kradac.conductor.vista.DetalleCompraServicioActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompraServiciosTarjetaCredito extends Fragment implements OnComunicacionCompraServicio {


    private static final String TAG = CompraServiciosTarjetaCredito.class.getName();
    @BindView(R2.id.lista_item)
    RecyclerView listaItem;
    Unbinder unbinder;
    @BindView(R2.id.progress_espera)
    ProgressBar progressEspera;

    private CompraServiciosPresentador compraServiciosPresentador;
    private SharedPreferences spLogin;

    private Utilidades utilidades;

    private RvAdapterCompraServiciosCategoria rvAdapterCompraServiciosCategoria;
    private RecyclerView.LayoutManager lManager;
    private boolean isActivo;


    public CompraServiciosTarjetaCredito() {
        // Required empty public constructor
        utilidades = new Utilidades();
        compraServiciosPresentador = new CompraServiciosPresentador(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_compra_servicios, container, false);
        unbinder = ButterKnife.bind(this, view);
        lManager = new LinearLayoutManager(getActivity());
        listaItem.setLayoutManager(lManager);
        spLogin = getActivity().getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        getDatos();
        cargarDatos();
        isActivo = true;
        return view;
    }

    public void getDatos() {
        compraServiciosPresentador.consultar_categorias(spLogin.getInt("idVehiculo", 0), spLogin.getInt("idUsuario", 0), spLogin.getInt("idCiudad", 0));
        progressEspera.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        isActivo = false;
    }

    public void cargarDatos() {
        rvAdapterCompraServiciosCategoria = new RvAdapterCompraServiciosCategoria(getActivity(), new ArrayList<CompraServiciosCategorias.LC>());
        listaItem.setAdapter(rvAdapterCompraServiciosCategoria);
        rvAdapterCompraServiciosCategoria.setOnItemClickListener(new RvAdapterCompraServiciosCategoria.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), DetalleCompraServicioActivity.class);
                Log.e(TAG, "onItemClick: " + rvAdapterCompraServiciosCategoria.getDatos().get(position).toString());
                intent.putExtra("categoria", rvAdapterCompraServiciosCategoria.getDatos().get(position));
                startActivity(intent);
            }
        });
    }


    @Override
    public void respuestaServidorCategorias(CompraServiciosCategorias compraServicios) {
        if (isActivo) {
            progressEspera.setVisibility(View.GONE);
            if (compraServicios != null) {
                if (compraServicios.getEn() == 1) {
                    rvAdapterCompraServiciosCategoria.addAll((ArrayList<CompraServiciosCategorias.LC>) compraServicios.getLC());
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(compraServicios.getM())
                            .setTitle("Alerta")
                            .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getDatos();
                                }
                            })
                            .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getActivity().finish();
                                }
                            }).show();
                }
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("No se puede obtener los datos, ¿desea intentar nuevamente?.")
                        .setTitle("Alerta")
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                getDatos();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                getActivity().finish();
                            }
                        }).show();
            }
        }
    }
}
