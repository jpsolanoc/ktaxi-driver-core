package com.kradac.conductor.adaptadoreslista;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.modelo.ConfiguracionServidor;
import com.kradac.conductor.modelo.Insignia;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.List;

public class InsigniaAdapter extends RecyclerView.Adapter<InsigniaAdapter.DataObjectHolder> {
    private static final String TAG = InsigniaAdapter.class.getName();
    private List<Insignia> mDataset;
    private OnItemClick onItemClick;
    private SparseBooleanArray selectedItems;
    private Context context;
    private ConfiguracionServidor configuracionServidor;

    public InsigniaAdapter(List<Insignia> myDataset, Context context, OnItemClick onItemClick) {
        this.mDataset = myDataset;
        this.selectedItems = new SparseBooleanArray();
        this.onItemClick = onItemClick;
        this.context = context;
        SharedPreferences spConfigServerNew = context.getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
        configuracionServidor = ConfiguracionServidor.createConfiguracionServidor(spConfigServerNew.getString(VariablesGlobales.CONFIG_IP_NEW, VariablesGlobales.CONFIGURACION_INICIAL));
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_insignia, parent, false);
        return new DataObjectHolder(view, onItemClick);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, int position) {
        Insignia insignia = mDataset.get(position);
        Log.e(TAG, "onBindViewHolder: " + insignia.toString() );
        holder.tvNumero.setText(insignia.getNumero() + "");
        holder.tvNombre.setText(insignia.getNombre());
        holder.tvMensajeInsignia.setText(insignia.getMensajeInsignia());
        DisplayImageOptions options = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.RGB_565).build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));

        imageLoader.displayImage(configuracionServidor.getDns().getImgC() + "uploads/insignias/" + insignia.getInsignia(), holder.ivInsignia, options, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                holder.ivInsignia.setImageResource(R.drawable.img_insignia);
                holder.pbCargandoImagen.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                holder.pbCargandoImagen.setVisibility(View.GONE);
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                holder.pbCargandoImagen.setVisibility(View.GONE);
                holder.ivInsignia.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                holder.pbCargandoImagen.setVisibility(View.GONE);
            }
        });

    }


    public void updateAdapter() {
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mDataset == null) {
            return 0;
        }
        return mDataset.size();
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public interface OnItemClick {
        void OnClickItem(Insignia insignia);
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvNumero;
        TextView tvNombre;
        TextView tvMensajeInsignia;
        ImageView ivInsignia;

        ProgressBar pbCargandoImagen;
        OnItemClick onItemClick;

        public DataObjectHolder(View itemView, OnItemClick onItemClick) {
            super(itemView);
            this.onItemClick = onItemClick;
            tvNumero = (TextView) itemView.findViewById(R.id.tvNumero);
            tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
            tvMensajeInsignia = (TextView) itemView.findViewById(R.id.tvMensajeInsignia);
            pbCargandoImagen = (ProgressBar) itemView.findViewById(R.id.pbCargandoImagen);
            ivInsignia = (ImageView) itemView.findViewById(R.id.ivInsignia);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            this.onItemClick.OnClickItem(mDataset.get(getAdapterPosition()));
        }
    }

}