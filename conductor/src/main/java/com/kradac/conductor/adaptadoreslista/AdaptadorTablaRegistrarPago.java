package com.kradac.conductor.adaptadoreslista;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.modelo.ItemRegistrarPago;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by John on 26/09/2016.
 */

public class AdaptadorTablaRegistrarPago extends ArrayAdapter<ItemRegistrarPago> {

    private static final String TAG = AdaptadorTablaRegistrarPago.class.getName();
    ArrayList<ItemRegistrarPago> datos;
    private Activity context;


    public AdaptadorTablaRegistrarPago(Activity context, ArrayList<ItemRegistrarPago> datos) {
        super(context, R.layout.dialog_registro_dispositivo, datos);
        this.context = context;
        this.datos = datos;
    }

    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        LayoutInflater inflater = context.getLayoutInflater();
        view = inflater.inflate(R.layout.item_verifica_pago, null);
        ViewHolder holder = new ViewHolder(view);
        ItemRegistrarPago itemRegistrarPago = datos.get(position);
        holder.tvPagareDe.setText(itemRegistrarPago.getPagareDe());
        holder.tvMonto.setText(itemRegistrarPago.getMonto());
        return view;
    }


    static class ViewHolder {
        @BindView(R2.id.tv_pagare_de)
        TextView tvPagareDe;
        @BindView(R2.id.tv_monto)
        TextView tvMonto;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
