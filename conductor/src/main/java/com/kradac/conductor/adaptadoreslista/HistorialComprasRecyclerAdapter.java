package com.kradac.conductor.adaptadoreslista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.modelo.ItemHistorialCompras;
import com.kradac.conductor.modelo.ItemHistorialSolicitud;
import com.kradac.conductor.vista.DetalleCompraActivity;
import com.kradac.conductor.vista.DetalleSolicitudActivity;

import java.util.List;

/**
 * Created by fabricio on 01/04/16.
 */
public class HistorialComprasRecyclerAdapter extends RecyclerView.Adapter<HistorialComprasRecyclerAdapter.DataObjectHolder> {
    private  List<ItemHistorialCompras> mDataset;

    class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView tvDireccion;
        TextView tvBarrio;
        TextView tvEmpresa;
        TextView tvPlaca;
        TextView tvFecha;
        Button btnDetalle;
        ImageView ivFormaPago;
        TextView lblBarrio;
        LinearLayout llPinVaucher;
        TextView tvPinVaucher;

        private final Context context;

        public DataObjectHolder(View itemView) {
            super(itemView);
            context = itemView.getContext();
            tvDireccion = (TextView) itemView.findViewById(R.id.tvDireccion);
            tvBarrio = (TextView) itemView.findViewById(R.id.tvBarrio);
            tvEmpresa = (TextView) itemView.findViewById(R.id.tvEmpresa);
            tvPlaca = (TextView) itemView.findViewById(R.id.tvPlaca);
            tvFecha = (TextView) itemView.findViewById(R.id.tvFecha);
            lblBarrio = (TextView) itemView.findViewById(R.id.lblBarrio);
            ivFormaPago = (ImageView) itemView.findViewById(R.id.ivFormaPago);
            llPinVaucher = (LinearLayout) itemView.findViewById(R.id.llPinVaucher);
            tvPinVaucher = (TextView) itemView.findViewById(R.id.tvPinVaucher);

            btnDetalle = (Button) itemView.findViewById(R.id.btnDetalle);
            btnDetalle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemHistorialCompras itemHistorialCompras =mDataset.get(getAdapterPosition());
                    Intent intentDetalle = new Intent(context, DetalleCompraActivity.class);
                    intentDetalle.putExtra("itemHistorialCompras", itemHistorialCompras);
                    context.startActivity(intentDetalle);

                }
            });

            SharedPreferences datosUsuario = context.getSharedPreferences("login", Context.MODE_PRIVATE);
            if(datosUsuario.contains("barrio")) {
                lblBarrio.setText(datosUsuario.getString("barrio", "Barrio")+":");
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                if (itemView.isActivated()) {
                    itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.color_primary));
                } else {
                    itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.color_accent));
                }
            } else {

            }
        }
    }

    public HistorialComprasRecyclerAdapter(List<ItemHistorialCompras> myDataset) {
        mDataset = myDataset;
        selectedItems = new SparseBooleanArray();
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_historial, parent, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        ItemHistorialCompras itemHistorialCompras = mDataset.get(position);

        holder.tvDireccion.setText(itemHistorialCompras.getcP() + " y " + itemHistorialCompras.getcS() + ".");
        holder.tvBarrio.setText(itemHistorialCompras.getB());
        holder.tvEmpresa.setText(itemHistorialCompras.getNbE());
        holder.tvPlaca.setText(itemHistorialCompras.getPl());
        holder.tvFecha.setText(itemHistorialCompras.getFecha()+" "+ itemHistorialCompras.getH());


        switch (itemHistorialCompras.getT()){
            case 0:{
                holder.ivFormaPago.setImageResource(R.mipmap.ic_efectivo);
            }break;
            case 1:{
                holder.llPinVaucher.setVisibility(View.VISIBLE);
//                holder.tvPinVaucher.setText(itemHistorialCompras.ge);
                holder.ivFormaPago.setImageResource(R.mipmap.voucherapp);
            }break;
            case 2:{
                holder.ivFormaPago.setImageResource(R.mipmap.ic_dinero_electronico);
            }break;
            case 3:{
                holder.ivFormaPago.setImageResource(R.mipmap.ic_dinero_electronico);
            }break;
            case 4:{
                holder.ivFormaPago.setImageResource(R.mipmap.ic_cuentas);   ///CAMBIAR ESTO
            }break;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            holder.itemView.setActivated(selectedItems.get(position, false));
        } else {

        }

    }


    @Override
    public int getItemCount() {
        if (mDataset == null) {
            return 0;
        }
        return mDataset.size();
    }

    private SparseBooleanArray selectedItems;

}
