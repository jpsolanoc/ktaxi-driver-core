package com.kradac.conductor.adaptadoreslista;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionPago;
import com.kradac.conductor.modelo.PagosConductor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by Ing.John Patricio Solano Cabrera on 12/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class RvRazonezCancelar extends RecyclerView.Adapter<RvRazonezCancelar.MyViewHolder> {


    public Activity context;
    private ArrayList<PagosConductor.LD> datos;
    private OnComunicacionPago onComunicacionPago;
    private Utilidades utilidades;
    private SharedPreferences spParametrosConfiguracion;

    public RvRazonezCancelar(Activity context, ArrayList<PagosConductor.LD> datos, OnComunicacionPago onComunicacionPago) {
        this.context = context;
        this.datos = datos;
        this.onComunicacionPago = onComunicacionPago;
        utilidades = new Utilidades();
        spParametrosConfiguracion = context.getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_pago, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final PagosConductor.LD dato = datos.get(position);
        holder.tvTitulo.setText(dato.getDescripccion());
        holder.tvAnio.setText(String.valueOf(dato.getAnio()));
        holder.tvMes.setText(obtenerMes(dato.getMes()));
        holder.tvCarrerasCorrectas.setText(String.valueOf(dato.getCorrectas()));
        holder.tvDeuda.setText(utilidades.dosDecimales(context,dato.getMonto()).concat(ejecutarMoneda()));
        holder.tvIvaPorcentaje.setText("IVA ".concat(String.valueOf(dato.getImpuestos())).concat("%"));
        holder.tvIva.setText(utilidades.dosDecimales(context,dato.getDeudaConIva()).concat(ejecutarMoneda()));
        double total = 0;
        if (dato.getPago() == -1) {
            total = (dato.getDeuda() - dato.getObtenerPagoDescuento()) + dato.getMoraImpuestos();
        } else {
            holder.tvTotalPago.setText("Total pago:");
            total = dato.getPago();
        }
        holder.tvTotal.setText(utilidades.dosDecimales(context,total).concat(ejecutarMoneda()));
        holder.tvEstadoPago.setText(String.valueOf(dato.getDeudaEstado()));
        obtenerColor(dato.getIdDeudaEstado(), holder.lyGlobal);
        if (dato.getMoraImpuestos() != 0) {
            holder.tbrMora.setVisibility(View.VISIBLE);
            holder.tvMora.setText(utilidades.dosDecimales(context,dato.getMoraImpuestos()).concat(ejecutarMoneda()));
        } else {
            holder.tbrMora.setVisibility(View.GONE);
        }

        if (dato.getDescuento() != 0) {
            holder.tbrDescuento.setVisibility(View.VISIBLE);
            holder.tvDescuento.setText(String.valueOf((int) dato.getDescuento()).concat(" %"));
        } else {
            holder.tbrDescuento.setVisibility(View.GONE);
        }
        if (dato.getDeudaEstado().equals("Debe")) {
            holder.chkRegistroPago.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onComunicacionPago.clickDeudaPendiente(dato, holder.chkRegistroPago.isChecked());
                }
            });
        } else {
            holder.chkRegistroPago.setEnabled(false);
        }
    }


    @Override
    public int getItemCount() {
        return datos.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitulo;
        TextView tvAnio;
        TextView tvMes;
        TextView tvCarrerasAtendidas;
        TextView tvCarrerasCorrectas;
        TextView tvCarrerasErroneas;
        TextView tvDeuda;
        TextView tvIva;
        TextView tvIvaPorcentaje;
        TextView tvTotal;
        TextView tvTotalPago;
        TextView tvEstadoPago;
        TextView tvMora;
        TableRow tbrMora;
        TextView tvDescuento;
        TableRow tbrDescuento;
        CheckBox chkRegistroPago;
        LinearLayout lyGlobal;


        public MyViewHolder(View view) {
            super(view);
            tvTitulo = (TextView) view.findViewById(R.id.tv_titulo);
            tvAnio = (TextView) view.findViewById(R.id.tv_anio);
            tvMes = (TextView) view.findViewById(R.id.tv_mes);
            tvCarrerasAtendidas = (TextView) view.findViewById(R.id.tv_carreras_atendidas);
            tvCarrerasCorrectas = (TextView) view.findViewById(R.id.tv_carreras_correctas);
            tvCarrerasErroneas = (TextView) view.findViewById(R.id.tv_carreras_erroneas);
            tvDeuda = (TextView) view.findViewById(R.id.tv_deuda);
            tvIvaPorcentaje = (TextView) view.findViewById(R.id.tv_iva_porcentaje);
            tvIva = (TextView) view.findViewById(R.id.tv_iva);
            tvTotal = (TextView) view.findViewById(R.id.tv_total);
            tvTotalPago = (TextView) view.findViewById(R.id.tv_total_pago);
            tvEstadoPago = (TextView) view.findViewById(R.id.tv_estado_pago);
            tvMora = (TextView) view.findViewById(R.id.tv_mora);
            tbrMora = (TableRow) view.findViewById(R.id.tbr_mora);
            tvDescuento = (TextView) view.findViewById(R.id.tv_descuento);
            tbrDescuento = (TableRow) view.findViewById(R.id.tbr_descuento);
            chkRegistroPago = (CheckBox) view.findViewById(R.id.chk_registro_pago);
            lyGlobal = (LinearLayout) view.findViewById(R.id.ly_global);
        }
    }

    public void clear() {
        datos.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<PagosConductor.LD> comentarios) {
        clear();
        this.datos = comentarios;
        notifyDataSetChanged();
    }

    public void add(PagosConductor.LD comentario) {
        this.datos.add(comentario);
        notifyDataSetChanged();
    }

    public ArrayList<PagosConductor.LD> getDatos() {
        return datos;
    }

    private void obtenerColor(int tipo, LinearLayout linearLayout) {
        switch (tipo) {
            case 1:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    linearLayout.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.style_caja_debe));
                } else {
                    linearLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.style_caja_debe));
                }
                break;
            case 5:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    linearLayout.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.style_caja_pagado));
                } else {
                    linearLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.style_caja_pagado));
                }
                break;
            case 6:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    linearLayout.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.style_caja_rechazado));
                } else {
                    linearLayout.setBackground(ContextCompat.getDrawable(context, R.drawable.style_caja_rechazado));
                }
                break;
        }
    }

    private String obtenerMes(int mes) {
        switch (mes) {
            case 1:
                return "ENERO";
            case 2:
                return "FEBRERO";
            case 3:
                return "MARZO";
            case 4:
                return "ABRIL";
            case 5:
                return "MAYO";
            case 6:
                return "JUNIO";
            case 7:
                return "JULIO";
            case 8:
                return "AGOSTO";
            case 9:
                return "SEPTIEMBRE";
            case 10:
                return "OCTUBRE";
            case 11:
                return "NOVIEMBRE";
            case 12:
                return "DICIEMBRES";
            default:
                return "";
        }
    }

    public String ejecutarMoneda() {
        String dataRetorno = " Usd";
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 12:
                                if (objetoConfig.getInt("h") == 1) {
                                    dataRetorno = " ".concat(objetoConfig.getString("nb"));
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                Log.e(TAG, "presentarValorCobro: ", e);
            }
        }
        return dataRetorno;
    }
}
