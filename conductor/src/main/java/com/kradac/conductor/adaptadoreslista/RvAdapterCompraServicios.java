package com.kradac.conductor.adaptadoreslista;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.OnComunicacionCompraServicios;
import com.kradac.conductor.modelo.SaldoKtaxi;
import com.kradac.conductor.vista.MapaSaldoKtaxi;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ing.John Patricio Solano Cabrera on 12/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class RvAdapterCompraServicios extends RecyclerView.Adapter<RvAdapterCompraServicios.MyViewHolder> {


    public Activity context;

    private ArrayList<SaldoKtaxi.CompraServicios.LS> datos;
    private OnComunicacionCompraServicios onComunicacionCompraServicios;

    public RvAdapterCompraServicios(Activity context, ArrayList<SaldoKtaxi.CompraServicios.LS> datos, OnComunicacionCompraServicios onComunicacionCompraServicios) {
        this.context = context;
        this.datos = datos;
        this.onComunicacionCompraServicios = onComunicacionCompraServicios;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_compra_servicios, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final SaldoKtaxi.CompraServicios.LS dato = datos.get(position);
        holder.tvNombreLocal.setText(dato.getEmpresa());
        holder.btnComprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dato.isLugar()) {
                    onComunicacionCompraServicios.lanzarDialogoCompra(position);
                } else {
                    onComunicacionCompraServicios.aunNoEnLugar(position);
                }
            }
        });
        holder.tvDistanciaLocal.setText("Distancia: ".concat(new Utilidades().dosDecimales(context,dato.getDistancia())).concat(" "+"KM"));
        holder.imgBtnLocalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, MapaSaldoKtaxi.class);
//                intent.putExtra("CompraServicios", dato);
//                context.startActivity(intent);
                Toast.makeText(context, "Próximamente", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String datoKmMt(double dato){
        if (dato>1){
            return "Km";
        }else{
            return "Metros";
        }
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.tv_nombre_local)
        TextView tvNombreLocal;
        @BindView(R2.id.img_btn_localizar)
        ImageButton imgBtnLocalizar;
        @BindView(R2.id.btn_comprar)
        Button btnComprar;
        @BindView(R2.id.tv_distancia_local)
        TextView tvDistanciaLocal;
        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void clear() {
        datos.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<SaldoKtaxi.CompraServicios.LS> datos) {
        clear();
        this.datos = datos;
        notifyDataSetChanged();
    }

    public void add(SaldoKtaxi.CompraServicios.LS comentario) {
        this.datos.add(comentario);
        notifyDataSetChanged();
    }

    public ArrayList<SaldoKtaxi.CompraServicios.LS> getDatos() {
        return datos;
    }

}
