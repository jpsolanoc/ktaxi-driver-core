package com.kradac.conductor.adaptadoreslista;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.interfaces.OnComunicacionIniciarSesion;
import com.kradac.conductor.modelo.ItemTelefono;

import java.util.ArrayList;

/**
 * Created by John on 26/09/2016.
 */

public class AdaptadorNumerosTelefono extends ArrayAdapter<ItemTelefono> {

    private static final String TAG = AdaptadorNumerosTelefono.class.getName();
    ArrayList<ItemTelefono> datos;
    private Activity context;
    private OnComunicacionIniciarSesion onComunicacionIniciarSesion;


    public AdaptadorNumerosTelefono(Activity context, ArrayList<ItemTelefono> datos, OnComunicacionIniciarSesion onComunicacionIniciarSesion) {
        super(context, R.layout.dialog_registro_dispositivo, datos);
        this.context = context;
        this.datos = datos;
        this.onComunicacionIniciarSesion = onComunicacionIniciarSesion;
    }

    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        LayoutInflater inflater = context.getLayoutInflater();
        view = inflater.inflate(R.layout.custom_item_numeros_telefono, null);
        final Button deleteBtn = (Button) view.findViewById(R.id.btn_llamar);
        deleteBtn.setTag(position);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_CALL,
                        Uri.parse("tel:" + datos.get(position).getTelefono()));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context,"No se puede llamar debe activar los permisos.",Toast.LENGTH_LONG).show();
                    return;
                } else {
                    onComunicacionIniciarSesion.clickItemLlamar();
                    context.startActivity(i);
                }
            }
        });
        TextView nombre = (TextView) view.findViewById(R.id.tv_nombre_cliente);
        nombre.setText(datos.get(position).getNombre());

        return view;
    }



}
