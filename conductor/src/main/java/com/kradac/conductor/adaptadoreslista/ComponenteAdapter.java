package com.kradac.conductor.adaptadoreslista;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.modelo.ComponenteBase;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;


public class ComponenteAdapter extends RecyclerView.Adapter<ComponenteAdapter.DataObjectHolder> {
    private Context context;
    private List<ComponenteBase> mDataset;
    private OnItemClick onItemClick;

    public ComponenteBase getItem(int position) {
        return mDataset.get(position);
    }

    public interface OnItemClick {
        void OnClickItem(int position);
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtTitulo;
        TextView txtDescripcion;
        ImageView img;
        OnItemClick onItemClick;

        public DataObjectHolder(View itemView, OnItemClick onItemClick) {
            super(itemView);
            this.onItemClick = onItemClick;
            this.txtTitulo = (TextView) itemView.findViewById(R.id.txtTitulo);
            this.txtDescripcion = (TextView) itemView.findViewById(R.id.txtDescripcion);
            this.img = (ImageView) itemView.findViewById(R.id.img);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            this.onItemClick.OnClickItem(getPosition());
        }
    }

    public ComponenteAdapter(Context context,List<ComponenteBase> myDataset, OnItemClick onItemClick) {
        this.context=context;
        this.mDataset = myDataset;
        this.selectedItems = new SparseBooleanArray();
        this.onItemClick = onItemClick;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_componente, parent, false);
        return new DataObjectHolder(view, onItemClick);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, int position) {
        ComponenteBase componenteBase= mDataset.get(position);
        holder.txtTitulo.setText(componenteBase.getT());
        holder.txtDescripcion.setText(componenteBase.getdC());
        DisplayImageOptions options = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.RGB_565).build();

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));

            imageLoader.displayImage(componenteBase.getImg(), holder.img, options, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    super.onLoadingComplete(imageUri, view, loadedImage);
                    holder.img.setImageBitmap(loadedImage);
                }
            });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            holder.itemView.setActivated(selectedItems.get(position, false));
        } else {

        }
    }

    public void addItem(ComponenteBase dataObj, int index) {
        mDataset.add(index, dataObj);

    }

    public void updateAdapter() {
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mDataset == null) {
            return 0;
        }
        return mDataset.size();
    }

    private SparseBooleanArray selectedItems;

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

}
