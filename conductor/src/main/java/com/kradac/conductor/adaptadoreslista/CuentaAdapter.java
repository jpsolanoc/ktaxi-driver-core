package com.kradac.conductor.adaptadoreslista;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.modelo.Cuenta;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by John on 04/01/2017.
 */

public class CuentaAdapter extends RecyclerView.Adapter<CuentaAdapter.CuentaViewHolder> {

    private List<Cuenta.C> items;
    private Context context;
    private Utilidades utilidades;
    private String moneda;

    public static class CuentaViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public ImageView imagen;
        public TextView cartera;
        public TextView cuenta;
        public TextView dinero;
        public TextView tvSaldoKtaxi;
        public TextView tvPropina;
        public CardView cardFondo;

        public CuentaViewHolder(View v) {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.imagen);
            cartera = (TextView) v.findViewById(R.id.nombre);
            cuenta = (TextView) v.findViewById(R.id.descripcion);
            dinero = (TextView) v.findViewById(R.id.dinero);
            tvSaldoKtaxi = (TextView) v.findViewById(R.id.tv_saldo_ktaxi);
            tvPropina = (TextView) v.findViewById(R.id.tv_propina);
            cardFondo = (CardView) v.findViewById(R.id.cart_fondo);
        }
    }

    public CuentaAdapter(ArrayList<Cuenta.C> items, Context context,String moneda) {
        utilidades = new Utilidades();
        this.items = items;
        this.context = context;
        this.moneda = moneda;
    }

    public void updateItem(List<Cuenta.C> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }

    @Override
    public CuentaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grid_header, parent, false);
        return new CuentaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CuentaViewHolder holder, int position) {
        switch (items.get(position).getTipo()) {
            case VariablesGlobales.VOUCHER:
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        holder.imagen.setBackgroundDrawable(ContextCompat.getDrawable(context, R.mipmap.voucherapp));
                    } else {
                        holder.imagen.setBackground(ContextCompat.getDrawable(context, R.mipmap.voucherapp));
                    }
                break;
            case VariablesGlobales.VOUCHER_CALL_CENTER:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    holder.imagen.setBackgroundDrawable(ContextCompat.getDrawable(context, R.mipmap.vouchercall));
                } else {
                    holder.imagen.setBackground(ContextCompat.getDrawable(context, R.mipmap.vouchercall));
                }
                break;
            case VariablesGlobales.DINERO_ELECTRONICO:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    holder.imagen.setBackgroundDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_dinero_electronico));
                } else {
                    holder.imagen.setBackground(ContextCompat.getDrawable(context, R.mipmap.ic_dinero_electronico));
                }
                break;
            case VariablesGlobales.PAYPHONE:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    holder.imagen.setBackgroundDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_icon_payphone));
                } else {
                    holder.imagen.setBackground(ContextCompat.getDrawable(context, R.mipmap.ic_icon_payphone));
                }
                break;
            case VariablesGlobales.TARJETA_CREDITO:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    holder.imagen.setBackgroundDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_icon_tarjeta_credito));
                } else {
                    holder.imagen.setBackground(ContextCompat.getDrawable(context, R.mipmap.ic_icon_tarjeta_credito));
                }
                break;
            default:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    holder.imagen.setBackgroundDrawable(ContextCompat.getDrawable(context, R.mipmap.ic_efectivo));
                } else {
                    holder.imagen.setBackground(ContextCompat.getDrawable(context, R.mipmap.ic_efectivo));
                }
                break;
        }
        holder.cartera.setText(items.get(position).getCartera());
        holder.cuenta.setText(items.get(position).getCuenta());
        holder.dinero.setTextColor(context.getResources().getColor(R.color.negro_letra));
        holder.dinero.setTextColor(context.getResources().getColor(R.color.negro_letra));
        holder.dinero.setTextColor(context.getResources().getColor(R.color.negro_letra));
        String data = utilidades.dosDecimales(context, items.get(position).getDinero()) + " " + moneda;
        colorCart(getNumero(items.get(position).getE()), items.get(position).getSaldo(), holder.tvSaldoKtaxi);
        String dataSaldoktaxi = "Saldo ktaxi " + utilidades.dosDecimales(context, items.get(position).getSaldo()) + " " + moneda;
        String dataPropina = "Propina " + utilidades.dosDecimales(context, items.get(position).getPropina()) + " " + moneda;
        holder.dinero.setText(data);
        holder.tvSaldoKtaxi.setText(dataSaldoktaxi);
        holder.tvPropina.setText(dataPropina);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public int getNumero(String numero) {
        int num = 0;
        if (numero != null && !numero.isEmpty()) {
            num = Integer.parseInt(numero);
        }
        return num;
    }

    public void colorCart(int valor, double saldoKtaxi, TextView fondo) {
        if (valor <= 0 && saldoKtaxi > 0) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                fondo.setBackgroundDrawable(context.getResources().getDrawable(R.color.ktaxi));
            } else {
                fondo.setBackground(context.getResources().getDrawable(R.color.ktaxi));
            }
        } else if (valor == 1) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                fondo.setBackgroundDrawable(context.getResources().getDrawable(R.color.verde));
            } else {
                fondo.setBackground(context.getResources().getDrawable(R.color.verde));
            }
        } else if (valor == 2) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                fondo.setBackgroundDrawable(context.getResources().getDrawable(R.color.rojo));
            } else {
                fondo.setBackground(context.getResources().getDrawable(R.color.rojo));
            }
        }
    }


}
