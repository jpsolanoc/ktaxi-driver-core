package com.kradac.conductor.adaptadoreslista;

/**
 * Created by John on 28/12/2016.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.modelo.ModeloItemTiempos;

import java.util.ArrayList;
/**
 * {@link BaseAdapter} personalizado para el gridview
 */
public class GridAdapterSeleccionarTiempo extends BaseAdapter {

    private final Context mContext;
    private ArrayList<ModeloItemTiempos> items;

    public GridAdapterSeleccionarTiempo(Context c, ArrayList<ModeloItemTiempos> items) {
        mContext = c;
        this.items = items;
    }

    @Override
    public int getCount() {
        // Decremento en 1, para no contar el header view
        return items.size();
    }

    @Override
    public ModeloItemTiempos getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_tiempo, viewGroup, false);
        }
        ModeloItemTiempos item = getItem(position);
        TextView title = (TextView) view.findViewById(R.id.tv_size);
        title.setText(item.getTiempo());
        return view;
    }
}