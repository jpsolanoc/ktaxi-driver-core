package com.kradac.conductor.adaptadoreslista;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.modelo.CompraServiciosCategorias;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ing.John Patricio Solano Cabrera on 12/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class RvAdapterCompraServiciosCategoria extends RecyclerView.Adapter<RvAdapterCompraServiciosCategoria.MyViewHolder> {


    public Activity context;


    private ArrayList<CompraServiciosCategorias.LC> datos;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    private OnItemClickListener mItemClickListener;

    public RvAdapterCompraServiciosCategoria(Activity context, ArrayList<CompraServiciosCategorias.LC> datos) {
        this.context = context;
        this.datos = datos;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_servicios, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final CompraServiciosCategorias.LC dato = datos.get(position);
        Log.e("RVCAT", "onBindViewHolder: " + dato.getIdCategoria() );
        holder.tvCategoria.setText(dato.getCategoria());
        holder.tvDetalleCategoria.setText(dato.getDetalle());
        holder.tvNumeroSubCategoria.setText("Numero servicios: ".concat(String.valueOf(dato.getTotal())));
        if (dato.getIdCategoria()==1){
            holder.imgDetalle.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_gasolina));
        }
        if (dato.getIdCategoria()==2){
            holder.imgDetalle.setImageDrawable(context.getResources().getDrawable(R.mipmap.ic_repuestos));
        }
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R2.id.tv_categoria)
        TextView tvCategoria;
        @BindView(R2.id.img_detalle)
        ImageView imgDetalle;
        @BindView(R2.id.tv_detalle_categoria)
        TextView tvDetalleCategoria;
        @BindView(R2.id.tv_numero_sub_categoria)
        TextView tvNumeroSubCategoria;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        if (getAdapterPosition() != RecyclerView.NO_POSITION) {
                            mItemClickListener.onItemClick(v, getAdapterPosition());
                        }
                    }
                }
            });
        }
    }

    public void clear() {
        datos.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<CompraServiciosCategorias.LC> datos) {
        clear();
        this.datos = datos;
        notifyDataSetChanged();
    }

    public void add(CompraServiciosCategorias.LC dato) {
        this.datos.add(dato);
        notifyDataSetChanged();
    }

    public ArrayList<CompraServiciosCategorias.LC> getDatos() {
        return datos;
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

}
