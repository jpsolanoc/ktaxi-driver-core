package com.kradac.conductor.adaptadoreslista;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.modelo.TransaccionesSaldoKtaxi;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ing.John Patricio Solano Cabrera on 12/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class RvAdapterTransaccionesKtaxi extends RecyclerView.Adapter<RvAdapterTransaccionesKtaxi.MyViewHolder> {


    public Activity context;


    private ArrayList<TransaccionesSaldoKtaxi.LT> datos;

    public RvAdapterTransaccionesKtaxi(Activity context, ArrayList<TransaccionesSaldoKtaxi.LT> datos) {
        this.context = context;
        this.datos = datos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_transacciones_saldo_ktaxi, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final TransaccionesSaldoKtaxi.LT dato = datos.get(position);
        holder.tvMotivo.setText(dato.getRazon());
        holder.tvSaldo.setText(new Utilidades().dosDecimales(context,dato.getSaldo()));
        holder.tvEstado.setText(dato.getEstado());
        holder.tvCategoria.setText(dato.getFecha_registro());
        holder.tvTipo.setText(dato.getTipo());
        setColor(dato.getIT(),holder.lyPrincipal);
        setColorEstado(dato.getIE(),holder.tvEstado);
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.tv_motivo)
        TextView tvMotivo;
        @BindView(R2.id.tv_saldo)
        TextView tvSaldo;
        @BindView(R2.id.tv_estado)
        TextView tvEstado;
        @BindView(R2.id.tv_categoria)
        TextView tvCategoria;
        @BindView(R2.id.tv_tipo)
        TextView tvTipo;
        @BindView(R2.id.ly_principal)
        LinearLayout lyPrincipal;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void clear() {
        datos.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<TransaccionesSaldoKtaxi.LT> dataEntra) {
        clear();
        datos = dataEntra;
        notifyDataSetChanged();
    }

    public void add(TransaccionesSaldoKtaxi.LT comentario) {
        datos.add(comentario);
        notifyDataSetChanged();
    }

    public ArrayList<TransaccionesSaldoKtaxi.LT> getDatos() {
        return datos;
    }

    public void setColor(int id, LinearLayout linearLayout) {
        switch (id) {
            case 1:
                linearLayout.setBackgroundColor(context.getResources().getColor(R.color.verde_bajo));
                break;
            case 2:
                linearLayout.setBackgroundColor(context.getResources().getColor(R.color.rojo_bajo));
                break;
        }
    }

    public void setColorEstado(int id, TextView tvEstado) {
        switch (id) {
            case 1:
                tvEstado.setBackgroundColor(context.getResources().getColor(R.color.color_fondo));
                break;
            case 2:
                tvEstado.setBackgroundColor(context.getResources().getColor(R.color.verde));
                break;
            case 3:
                tvEstado.setBackgroundColor(context.getResources().getColor(R.color.rojo));
                break;
        }
    }

}
