package com.kradac.conductor.adaptadoreslista;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.modelo.ModeloItemTiempos;

import java.util.ArrayList;

/**
 * Created by John on 10/08/2016.
 */
public class ListAdapterTiempos extends ArrayAdapter<ModeloItemTiempos> {

    private ArrayList<ModeloItemTiempos> objects;

    public ListAdapterTiempos(Context context, int resource, ArrayList<ModeloItemTiempos> objects) {
        super(context, resource, objects);
        this.objects = objects;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            view = vi.inflate(R.layout.custom_tiempo, null);
        }
        TextView title = (TextView) view.findViewById(R.id.tv_size);
        title.setText(objects.get(position).getTiempo());
        return view;
    }
}
