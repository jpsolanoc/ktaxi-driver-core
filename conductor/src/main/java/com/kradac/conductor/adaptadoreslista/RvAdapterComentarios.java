package com.kradac.conductor.adaptadoreslista;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.modelo.RespuestaComentarios;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by Ing.John Patricio Solano Cabrera on 12/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class RvAdapterComentarios extends RecyclerView.Adapter<RvAdapterComentarios.MyViewHolder> {


    public Activity context;
    private ArrayList<RespuestaComentarios.Ob> datos;

    public RvAdapterComentarios(Activity context, ArrayList<RespuestaComentarios.Ob> datos) {
        this.context = context;
        this.datos = datos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comentario, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final RespuestaComentarios.Ob dato = datos.get(position);
        holder.tvComentario.setText(dato.getoB());
        Hashtable<Integer, Integer> contenedor = new Hashtable();
        contenedor.put(0, 5);
        contenedor.put(1, 5);
        contenedor.put(2, 4);
        contenedor.put(3, 3);
        contenedor.put(4, 2);
        contenedor.put(5, 1);
        contenedor.put(5, 1);
        holder.ratingBarCalificacion.setMax(contenedor.get(dato.getV()));
        holder.ratingBarCalificacion.setRating(contenedor.get(dato.getV()));
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        protected TextView tvComentario;
        protected RatingBar ratingBarCalificacion;

        public MyViewHolder(View view) {
            super(view);
            tvComentario = (TextView) view.findViewById(R.id.tv_comentario);
            ratingBarCalificacion = (RatingBar) view.findViewById(R.id.rating_promedio);
        }
    }

    public void clear() {
        datos.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<RespuestaComentarios.Ob> comentarios) {
        clear();
        this.datos = comentarios;
        notifyDataSetChanged();
    }

    public void add(RespuestaComentarios.Ob comentario) {
        this.datos.add(comentario);
        notifyDataSetChanged();
    }

    public ArrayList<RespuestaComentarios.Ob> getDatos() {
        return datos;
    }

}
