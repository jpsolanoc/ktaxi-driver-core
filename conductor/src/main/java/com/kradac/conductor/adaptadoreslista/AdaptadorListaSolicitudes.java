package com.kradac.conductor.adaptadoreslista;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionListaAdapter;
import com.kradac.conductor.modelo.RespuestaConfiguracion;
import com.kradac.conductor.modelo.Solicitud;
import com.kradac.conductor.vista.MapaBaseActivity;

import java.util.ArrayList;

/**
 * Created by John on 26/09/2016.
 */

public class AdaptadorListaSolicitudes extends ArrayAdapter<Solicitud> {

    private static final String TAG = AdaptadorListaSolicitudes.class.getName();
    ArrayList<Solicitud> datos;
    private OnComunicacionListaAdapter onComunicacionListaAdapter;
    private Activity context;
    private SharedPreferences spParametrosConfiguracion;
    private Utilidades utilidades;


    public AdaptadorListaSolicitudes(Activity context, ArrayList<Solicitud> datos, OnComunicacionListaAdapter onComunicacionListaAdapter) {
        super(context, R.layout.activity_lista_solicitudes, datos);
        this.context = context;
        this.datos = datos;
        utilidades = new Utilidades();
        this.onComunicacionListaAdapter = onComunicacionListaAdapter;
        spParametrosConfiguracion = context.getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
    }

    public View getView(final int position, View view, ViewGroup parent) {
        Solicitud solicitudSeleccionada = datos.get(position);
        LayoutInflater inflater = context.getLayoutInflater();
        view = inflater.inflate(R.layout.custom_item_solicitudes_entrantes, null);
        final ImageView tipoSolicitud = view.findViewById(R.id.btnInfoMapa);
        final LinearLayout linearColor = view.findViewById(R.id.linearColor);
        TextView tvServicio = view.findViewById(R.id.tv_servicio);
        tipoSolicitud.setTag(position);
        tipoSolicitud.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(context, MapaBaseActivity.class);
                intent.putExtra("enSolicitud", false);
                intent.putExtra("tiempo", 0);
                intent.putExtra("idSolicitud", String.valueOf(datos.get((int) v.getTag()).getIdSolicitud()));
                intent.putExtra("latitud", datos.get((int) v.getTag()).getLatitud());
                intent.putExtra("longitud", datos.get((int) v.getTag()).getLongitud());
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.left_in, R.anim.left_out);
                notifyDataSetChanged();
                context.finish();
            }
        });
        final Button btnEnviarTiempo = view.findViewById(R.id.btn_envia_tiempo);
        final Button btnMasTiempo = view.findViewById(R.id.btn_mas_tiempo);
        final LinearLayout lyTiempos = view.findViewById(R.id.ly_tiempos);
        if (datos.get(position).getTiempos() != null) {
            try {
                lyTiempos.setVisibility(View.VISIBLE); // Cambiar esto para ver los tiempos de las solicitudes.
                btnEnviarTiempo.setText(datos.get(position).getTiempos().get(1) + " Minutos");
                btnEnviarTiempo.setTag(position);
                btnEnviarTiempo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onComunicacionListaAdapter.enviarTiempo(datos.get(position).getTiempos().get(1), datos.get(position));
                    }
                });
                btnMasTiempo.setTag(position);
                btnMasTiempo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onComunicacionListaAdapter.masTiempo(datos.get(position));
                    }
                });
            } catch (IndexOutOfBoundsException e) {
                lyTiempos.setVisibility(View.GONE);
            }

        }
        TextView tv_propina = view.findViewById(R.id.tv_propina);
        TextView tvBarrio = view.findViewById(R.id.tv_barrio);
        if (utilidades.ocularMostrarCampos(solicitudSeleccionada.getBarrio())) {
            tvBarrio.setVisibility(View.VISIBLE);
            tvBarrio.setText(solicitudSeleccionada.getBarrio());
        } else {
            tvBarrio.setVisibility(View.GONE);
        }
        TextView tvCallePrincipal = view.findViewById(R.id.tv_calle_principal);
        if (utilidades.ocularMostrarCampos(solicitudSeleccionada.getCallePrincipal())) {
            tvCallePrincipal.setVisibility(View.VISIBLE);
            tvCallePrincipal.setText(solicitudSeleccionada.getCallePrincipal());
        } else {
            tvCallePrincipal.setVisibility(View.GONE);
        }
        TextView tvCalleSecundaria = view.findViewById(R.id.tv_calle_secundaria);
        if (utilidades.ocularMostrarCampos(solicitudSeleccionada.getCalleSecundaria())) {
            tvCalleSecundaria.setVisibility(View.VISIBLE);
            tvCalleSecundaria.setText(solicitudSeleccionada.getCalleSecundaria());
        } else {
            tvCalleSecundaria.setVisibility(View.GONE);
        }
        if (!solicitudSeleccionada.isPedido()){
            ((TextView) view.findViewById(R.id.tv_distancia_tiempo)).setText(utilidades.tiempoDistancia(solicitudSeleccionada));
        }
        ImageView imageFormaPago = view.findViewById(R.id.iv_forma_pago);
        ImageView imageEstadoGps = view.findViewById(R.id.img_estado_gps);
        if (RespuestaConfiguracion.isActivarIconoEstadoGpsSolicitud(context)) {
            imageEstadoGps.setVisibility(View.VISIBLE);
            imageEstadoGps.setImageResource(utilidades.iconEstadoGpsEnSolicitud(solicitudSeleccionada.getConP()));
        } else {
            imageEstadoGps.setVisibility(View.GONE);
        }

        LinearLayout rlColor = view.findViewById(R.id.relativeLayoutPrincipal);
        if (solicitudSeleccionada.getPropina() != 0) {
            String text = "Propina: " + datos.get(position).getPropina();
            tv_propina.setText(text);
        } else {
            tv_propina.setVisibility(View.GONE);
        }
        imageFormaPago.setImageResource(utilidades.iconFormaPagoSolicitud(solicitudSeleccionada.getTipoFormaPago()));
        if (!solicitudSeleccionada.isPedido()) {
            tipoSolicitud.setImageResource(utilidades.iconTipoSolitud(0));
        } else {
            tipoSolicitud.setImageResource(utilidades.iconTipoSolitud(solicitudSeleccionada.getT()));
        }
        rlColor.setBackgroundDrawable(utilidades.drawableFondoSolicitud(solicitudSeleccionada.isQuiosco(),
                solicitudSeleccionada.getTipoFormaPago(), solicitudSeleccionada.isPedido(), solicitudSeleccionada.getT(), context));
        if (solicitudSeleccionada.getColor() != null) {
            linearColor.setBackgroundColor(Color.parseColor(solicitudSeleccionada.getColor()));
        } else {
            linearColor.setBackgroundColor(Color.parseColor("#dddddd"));
        }
        /***
         * TIpo de servicio
         */
        utilidades.definirNombreServicio(solicitudSeleccionada, tvServicio);
        return view;
    }


}
