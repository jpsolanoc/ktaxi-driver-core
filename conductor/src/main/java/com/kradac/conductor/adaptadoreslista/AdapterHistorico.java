package com.kradac.conductor.adaptadoreslista;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.modelo.Historial;

import java.util.ArrayList;

/**
 * Created by John on 02/12/2016.
 */

public class AdapterHistorico extends RecyclerView.Adapter<AdapterHistorico.MyViewHolder> {

    private ArrayList<Historial> historiales;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_historial_carreras,parent,false);
        return new MyViewHolder(itemView);
    }


    @Override
    public int getItemCount() {
        return historiales.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvCliente;
        TextView tvCallePrincipal;
        TextView tvCalleSecundaria;
        TextView tvBarrioCliente;
        TextView tvFechaSoli;
        MyViewHolder(View itemView) {
            super(itemView);
            tvCliente = (TextView) itemView.findViewById(R.id.tv_cliente);
            tvCallePrincipal = (TextView) itemView.findViewById(R.id.tv_calle_p);
            tvCalleSecundaria = (TextView) itemView.findViewById(R.id.tv_calle_s);
            tvBarrioCliente = (TextView) itemView.findViewById(R.id.tv_barrio_s);
            tvFechaSoli = (TextView) itemView.findViewById(R.id.tv_fecha_s);
        }
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvCliente.setText("Cliente: "+ historiales.get(position).getNombres());
        holder.tvCallePrincipal.setText("Calle P: "+ historiales.get(position).getCallePrincipal());
        holder.tvCalleSecundaria.setText("Calle S: "+ historiales.get(position).getCalleSecundaria());
        holder.tvBarrioCliente.setText("Barrio: "+ historiales.get(position).getBarrioCliente());
        holder.tvFechaSoli.setText("Fecha: "+ historiales.get(position).getFecha());
    }


    public void updateData(ArrayList<Historial> viewModels) {
        historiales.clear();
        historiales.addAll(viewModels);
        notifyDataSetChanged();
    }

    public AdapterHistorico(ArrayList<Historial> historiales) {
        this.historiales = historiales;
    }
}
