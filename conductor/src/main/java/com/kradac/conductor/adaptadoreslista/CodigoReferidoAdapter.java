package com.kradac.conductor.adaptadoreslista;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.modelo.CodigoReferido;

import java.util.List;


public class CodigoReferidoAdapter extends RecyclerView
        .Adapter<CodigoReferidoAdapter
        .DataObjectHolder> {
    private List<CodigoReferido> mDataset;
    private CodigoReferidoListener codigoReferidoListener;
    private Context context;

    public interface CodigoReferidoListener {
        void onClick(CodigoReferido codigoReferido);
    }

    public CodigoReferidoAdapter(List<CodigoReferido> myDataset, CodigoReferidoListener codigoReferidoListener, Context context) {
        mDataset = myDataset;
        this.codigoReferidoListener = codigoReferidoListener;
        this.context = context;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_codigo_referido, parent, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        CodigoReferido codigoReferido = mDataset.get(position);
        holder.tvCodigoReferido.setText(codigoReferido.getCodigo());
    }

    @Override
    public int getItemCount() {
        if (mDataset == null) {
            return 0;
        }
        return mDataset.size();
    }

    class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView tvCodigoReferido;
        TextView tvDescripcion;
        Button btnCompartirCodigoReferido;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tvCodigoReferido = (TextView) itemView.findViewById(R.id.tvCodigoReferido);
            tvDescripcion = (TextView) itemView.findViewById(R.id.tvDescripcion);

            btnCompartirCodigoReferido = (Button) itemView.findViewById(R.id.btnCompartirCodigoReferido);
            btnCompartirCodigoReferido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    CodigoReferido codigoReferido = mDataset.get(getAdapterPosition());
                    codigoReferidoListener.onClick(codigoReferido);

                }
            });
            tvCodigoReferido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setClipboard(tvCodigoReferido.getText().toString());
                    Toast.makeText(context, "Código copiado.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    protected void setClipboard(String text) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            clipboard.setPrimaryClip(clip);
        }
    }

}
