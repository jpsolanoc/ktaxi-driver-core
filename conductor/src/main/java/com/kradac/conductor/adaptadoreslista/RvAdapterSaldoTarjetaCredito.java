package com.kradac.conductor.adaptadoreslista;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.modelo.ConsultarSaldoTarjetaCredito;
import com.kradac.conductor.modelo.SaldoKtaxi;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ing.John Patricio Solano Cabrera on 12/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class RvAdapterSaldoTarjetaCredito extends RecyclerView.Adapter<RvAdapterSaldoTarjetaCredito.MyViewHolder> {


    public Activity context;
    private ArrayList<ConsultarSaldoTarjetaCredito.LS> datos;

    public RvAdapterSaldoTarjetaCredito(Activity context, ArrayList<ConsultarSaldoTarjetaCredito.LS> datos) {
        this.context = context;
        this.datos = datos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_saldo_ktaxi, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ConsultarSaldoTarjetaCredito.LS dato = datos.get(position);
        holder.tvRazon.setText(dato.getRazon());
        holder.tvMnto.setText(new Utilidades().dosDecimales(context,dato.getCredito()) + " USD");
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.tv_razon)
        TextView tvRazon;
        @BindView(R2.id.tv_mnto)
        TextView tvMnto;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void clear() {
        datos.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<ConsultarSaldoTarjetaCredito.LS> dataEntra) {
        clear();
        datos = dataEntra;
        notifyDataSetChanged();
    }

    public void add(ConsultarSaldoTarjetaCredito.LS comentario) {
        datos.add(comentario);
        notifyDataSetChanged();
    }

    public ArrayList<ConsultarSaldoTarjetaCredito.LS> getDatos() {
        return datos;
    }

}
