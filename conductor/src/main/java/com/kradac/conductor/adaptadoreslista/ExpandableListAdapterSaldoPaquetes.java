package com.kradac.conductor.adaptadoreslista;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.modelo.RespuestaSaldoPaquetes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Ing.John Patricio Solano Cabrera on 21/4/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class ExpandableListAdapterSaldoPaquetes extends BaseExpandableListAdapter {

    private Utilidades utilidades;

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final RespuestaSaldoPaquetes.LP lp = (RespuestaSaldoPaquetes.LP) getChild(groupPosition, childPosition);
        Log.e(TAG, "getChildView: " + lp.toString());
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.item_paquetes_hijo, null);
        }
        TextView tvCredito, tvPromocional, tvTotal, tvCostoPorCarrera, tvDesde, tvHasta, tvRealcion;
        LinearLayout ly_item_des = convertView.findViewById(R.id.ly_item_descuento);
        tvCredito = convertView.findViewById(R.id.tv_credito);
        tvPromocional = convertView.findViewById(R.id.tv_promocional);
        tvTotal = convertView.findViewById(R.id.tv_total);
        tvCostoPorCarrera = convertView.findViewById(R.id.tv_costo_por_carrera);
        tvDesde = convertView.findViewById(R.id.tv_desde);
        tvHasta = convertView.findViewById(R.id.tv_hasta);
        tvRealcion = convertView.findViewById(R.id.tv_relacion);
        tvTotal.setText(utilidades.dosDecimales(_context,lp.getCreditoTotal()));
        tvCredito.setText(utilidades.dosDecimales(_context,lp.getCredito()));
        tvPromocional.setText(utilidades.dosDecimales(_context,lp.getCreditoPromocion()));
        if (lp.getReferenciaD() == -1) {
            ly_item_des.setVisibility(View.GONE);
        } else {
            ly_item_des.setVisibility(View.VISIBLE);
        }
        if (lp.getRelacionPorcentaje() == 1) {
            tvCostoPorCarrera.setText(utilidades.dosDecimales(_context,lp.getReferenciaP()).concat(" %"));
            tvRealcion.setText(utilidades.dosDecimales(_context,lp.getReferenciaP()).concat(" %"));
        } else {
            tvCostoPorCarrera.setText(utilidades.dosDecimales(_context,lp.getReferenciaP()));
            tvRealcion.setText(utilidades.dosDecimales(_context,lp.getReferenciaP()));
        }
        tvDesde.setText(lp.getDesde() != null ? lp.getDesde() : "2018/02/12");
        tvHasta.setText(lp.getHasta() != null ? lp.getHasta() : "2018/02/12");


        return convertView;
    }

    private static final String TAG = ExpandableListAdapterSaldoPaquetes.class.getName();
    private Activity _context;
    private ArrayList<RespuestaSaldoPaquetes.LPt> padrePaquetes; // header titles
    // child data in format of header title, child title
    private HashMap<RespuestaSaldoPaquetes.LPt, List<RespuestaSaldoPaquetes.LP>> _listDataChildHorario;

    public ExpandableListAdapterSaldoPaquetes(Activity context,
                                              ArrayList<RespuestaSaldoPaquetes.LPt> listDataHeader,
                                              HashMap<RespuestaSaldoPaquetes.LPt, List<RespuestaSaldoPaquetes.LP>> listChildData) {
        this._context = context;
        this.padrePaquetes = listDataHeader;
        this._listDataChildHorario = listChildData;
        utilidades = new Utilidades();
    }

    public void clear() {
        this._listDataChildHorario.clear();
        notifyDataSetChanged();
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChildHorario.get(this.padrePaquetes.get(groupPosition)).get(childPosititon);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return this._listDataChildHorario.get(this.padrePaquetes.get(groupPosition)).size();
        } catch (NullPointerException e) {

            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.padrePaquetes.get(groupPosition);
    }


    @Override
    public int getGroupCount() {
        return this.padrePaquetes.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }
        TextView tvTitulo = convertView.findViewById(R.id.tv_titulo);
        TextView tvDescripcion = convertView.findViewById(R.id.tv_descripcion);
        RespuestaSaldoPaquetes.LPt lPt = padrePaquetes.get(groupPosition);
        tvTitulo.setText(lPt.getPaqueteTipo());
        tvDescripcion.setText(lPt.getDescripcion());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
