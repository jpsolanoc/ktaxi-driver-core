package com.kradac.conductor.adaptadoreslista;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.modelo.ChatMessage;
import com.kradac.conductor.vista.ChatActivity;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class ChatAdapter extends BaseAdapter {

    private static final String TAG = ChatAdapter.class.getName();
    private final List<ChatMessage> chatMessages;
    private Activity context;
    private MediaPlayer mediaPlayer = new MediaPlayer();
    private ChatActivity chatActivity = new ChatActivity();

    public ChatAdapter(Activity context, List<ChatMessage> chatMessages) {
        this.context = context;
        this.chatMessages = chatMessages;
        this.chatActivity = chatActivity;
    }

    @Override
    public int getCount() {
        if (chatMessages != null) {
            return chatMessages.size();
        } else {
            return 0;
        }
    }

    @Override
    public ChatMessage getItem(int position) {
        if (chatMessages != null) {
            return chatMessages.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static final int MY_PERMISSIONS_REQUEST_CALL = 100;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final ChatMessage chatMessage = getItem(position);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (chatMessage.getTipo() == 0) {
            convertView = vi.inflate(R.layout.custom_chat_message, null);
            holder = createViewHolder(convertView);
            convertView.setTag(holder);
            boolean myMsg = chatMessage.getIsme();
            setAlignment(holder, myMsg);
            if (myMsg) {
                holder.txtMessage.setText(chatMessage.getMessage());
            } else {
                if (isNumero(chatMessage.getMessage())) {
                    holder.txtMessage.setText(chatMessage.getMessage());
                    subrayarTextoConAcciones(chatMessage.getMessage(), holder.txtMessage);
                    holder.txtMessage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL);
                                return;
                            } else {
                                context.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + obtenerNumero(chatMessage.getMessage()))));
                            }
                        }
                    });
                } else {
                    holder.txtMessage.setText(chatMessage.getMessage());
                }
            }
            holder.txtInfo.setText(chatMessage.getDate());
        }

        if (chatMessage.getTipo() == 1) {
            convertView = vi.inflate(R.layout.custom_chat_audio, null);
            final ViewHolderAudio holderAudio = createViewHolderAudio(convertView);
            convertView.setTag(holderAudio);
            boolean myMsg = chatMessage.getIsme();
            setAlignment(holderAudio, myMsg);
            holderAudio.txtInfo.setText(chatMessage.getDate());
            holderAudio.prepare(chatMessage.getNameArchivo(),context);
            holderAudio.ibtnAudioPause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holderAudio.ibtnAudioPause.setVisibility(View.GONE);
                    holderAudio.ibtnAudioPlay.setVisibility(View.VISIBLE);
                    holderAudio.pause();
                }
            });

            holderAudio.ibtnAudioPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holderAudio.ibtnAudioPause.setVisibility(View.VISIBLE);
                    holderAudio.ibtnAudioPlay.setVisibility(View.GONE);
                    holderAudio.play();
                    new Thread(holderAudio).start();
                }
            });
            holderAudio.setOnProgressListener(new ViewHolderAudio.OnProgressListener() {
                @Override
                public void changeProgress(final String progress) {
                    Log.e("cambio", progress);
                    context.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            holderAudio.txtDuracion.setText(progress);
                        }
                    });
                }
            });

        }

        return convertView;
    }


    public boolean isNumero(String str) {
        if (str == null) {
            return false;
        }
        int count = 0;
        for (char a : str.toCharArray()) {
            if (Character.isDigit(a)) {
                count++;
            }
        }
        if (count >= 5) {
            return true;
        } else {
            return false;
        }
    }

    public String obtenerNumero(String str) {
        String numCelular = "";
        for (char a : str.toCharArray()) {
            if (Character.isDigit(a)) {
                numCelular = numCelular.concat(String.valueOf(a));
            }
        }
        return numCelular;
    }

    public void subrayarTextoConAcciones(String text, TextView tvHide) {
        SpannableString spanString = new SpannableString(text);
        spanString.setSpan(new UnderlineSpan(), indice(text), indice(text) + obtenerNumero(text).length(), 0);
        tvHide.setText(spanString);
    }

    private int indice(String str) {
        int count = 0;
        for (char a : str.toCharArray()) {
            if (Character.isDigit(a)) {
                return count;
            } else {
                count++;
            }
        }
        return count;
    }

    private ViewHolderAudio createViewHolderAudio(View v) {
        ViewHolderAudio holder = new ViewHolderAudio();
        holder.ibtnAudioPlay = (ImageButton) v.findViewById(R.id.ibtnAudioPlay);
        holder.ibtnAudioPause = (ImageButton) v.findViewById(R.id.btn_Pausa);
        holder.txtInfo = (TextView) v.findViewById(R.id.txtMessage);
        holder.content = (LinearLayout) v.findViewById(R.id.content);
        holder.contentWithBG = (LinearLayout) v.findViewById(R.id.contentWithBackground);
        holder.txtInfo = (TextView) v.findViewById(R.id.txtInfo);
        holder.txtDuracion = (TextView) v.findViewById(R.id.txtDuracion);
        holder.skProgres = (SeekBar) v.findViewById(R.id.skProgres);
        return holder;
    }

    private void setAlignment(ViewHolderAudio holder, boolean isMe) {
        if (!isMe) {
            holder.contentWithBG.setBackgroundResource(R.drawable.in_message_bg);

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.txtInfo.setLayoutParams(layoutParams);
            holder.txtInfo.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.txtInfo.setLayoutParams(layoutParams);
        } else {
            holder.contentWithBG.setBackgroundResource(R.drawable.out_message_bg);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtInfo.setLayoutParams(layoutParams);
            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtInfo.setLayoutParams(layoutParams);
        }
    }

    public void add(ChatMessage message) {
        chatMessages.add(message);
    }

    public void add(List<ChatMessage> messages) {
        chatMessages.addAll(messages);
    }

    public void removeAll() {
        chatMessages.clear();
    }

    private void setAlignment(ViewHolder holder, boolean isMe) {
        if (!isMe) {
            holder.contentWithBG.setBackgroundResource(R.drawable.in_message_bg);

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.txtInfo.setLayoutParams(layoutParams);
        } else {
            holder.contentWithBG.setBackgroundResource(R.drawable.out_message_bg);

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.txtMessage.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.txtInfo.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.txtInfo.setLayoutParams(layoutParams);
        }
    }

    private ViewHolder createViewHolder(View v) {
        ViewHolder holder = new ViewHolder();
        holder.txtMessage = (TextView) v.findViewById(R.id.txtMessage);
        holder.content = (LinearLayout) v.findViewById(R.id.content);
        holder.contentWithBG = (LinearLayout) v.findViewById(R.id.contentWithBackground);
        holder.txtInfo = (TextView) v.findViewById(R.id.txtInfo);
        return holder;
    }

    public static class ViewHolderAudio implements Runnable {
        public TextView txtDuracion;
        public TextView txtInfo;
        public LinearLayout content;
        public LinearLayout contentWithBG;
        public ImageButton ibtnAudioPlay;
        public ImageButton ibtnAudioPause;
        public SeekBar skProgres;
        MediaPlayer mediaPlayer;
        OnProgressListener onProgressListener;
        String nombre;
        public Context context;

        public void setOnProgressListener(OnProgressListener progressListener) {
            this.onProgressListener = progressListener;
        }

        public void prepare(String nombre,Context context) {
            this.nombre = nombre;
            this.context = context;

            String intStorageDirectory = context.getFilesDir().toString();
            File file = new File(intStorageDirectory, "KtaxiDriver/Audio");
            String dirAudio = (file.getAbsolutePath() + "/");

            try {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM); // this is important.
                mediaPlayer.setDataSource(dirAudio + nombre);
                mediaPlayer.prepare();
                int totalSecs = 1 + mediaPlayer.getDuration() / 1000;
                int minutes = (totalSecs % 3600) / 60;
                int seconds = totalSecs % 60;

                txtDuracion.setText(String.format("%02d:%02d", minutes, seconds));
                skProgres.setMax(mediaPlayer.getDuration());
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer1) {
                        ibtnAudioPlay.setVisibility(View.VISIBLE);
                        ibtnAudioPause.setVisibility(View.GONE);
                        skProgres.setProgress(skProgres.getMax());
                        stop();
                        mediaPlayer = null;

                    }
                });
                skProgres.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mediaPlayer != null) {
                            //   mediaPlayer.seekTo(skProgres.getProgress());
                        }
                    }
                });
                skProgres.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        if (mediaPlayer != null) {
                            mediaPlayer.seekTo(seekBar.getProgress());
                        }
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void play() {
            if (mediaPlayer == null) {
                prepare(nombre,context);
            }
            mediaPlayer.start();
        }

        public void pause() {
            ibtnAudioPause.setVisibility(View.GONE);
            ibtnAudioPlay.setVisibility(View.VISIBLE);
            if (mediaPlayer != null) {
                mediaPlayer.pause();
            }
        }

        public void stop() {
            mediaPlayer.stop();
            mediaPlayer.release();
        }

        @Override
        public void run() {
            try {
                while (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    skProgres.setProgress(mediaPlayer.getCurrentPosition());
                    int totalSecs = 1 + mediaPlayer.getCurrentPosition() / 1000;
                    int minutes = (totalSecs % 3600) / 60;
                    int seconds = totalSecs % 60;
                    if (onProgressListener != null) {
                        onProgressListener.changeProgress(String.format("%02d:%02d", minutes, seconds));
                    }
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }

        public interface OnProgressListener {
            void changeProgress(String progress);
        }
    }

    private static class ViewHolder {
        public TextView txtMessage;
        public TextView txtInfo;
        public LinearLayout content;
        public LinearLayout contentWithBG;
    }

}
