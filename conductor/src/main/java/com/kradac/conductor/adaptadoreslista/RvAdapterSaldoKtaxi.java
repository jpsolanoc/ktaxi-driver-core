package com.kradac.conductor.adaptadoreslista;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.modelo.RespuestaComentarios;
import com.kradac.conductor.modelo.SaldoKtaxi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ing.John Patricio Solano Cabrera on 12/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class RvAdapterSaldoKtaxi extends RecyclerView.Adapter<RvAdapterSaldoKtaxi.MyViewHolder> {


    public Activity context;
    private ArrayList<SaldoKtaxi.ItemSaldoKtaxi.LS> datos;

    public RvAdapterSaldoKtaxi(Activity context, ArrayList<SaldoKtaxi.ItemSaldoKtaxi.LS> datos) {
        this.context = context;
        this.datos = datos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_saldo_ktaxi, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final SaldoKtaxi.ItemSaldoKtaxi.LS dato = datos.get(position);
        holder.tvRazon.setText(dato.getRazon());
        holder.tvMnto.setText(new Utilidades().dosDecimales(context,dato.getSaldo()) + " " + definirHindCobro());
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R2.id.tv_razon)
        TextView tvRazon;
        @BindView(R2.id.tv_mnto)
        TextView tvMnto;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void clear() {
        datos.clear();
        notifyDataSetChanged();
    }

    public void addAll(ArrayList<SaldoKtaxi.ItemSaldoKtaxi.LS> dataEntra) {
        clear();
        datos = dataEntra;
        notifyDataSetChanged();
    }

    public void add(SaldoKtaxi.ItemSaldoKtaxi.LS comentario) {
        datos.add(comentario);
        notifyDataSetChanged();
    }

    public ArrayList<SaldoKtaxi.ItemSaldoKtaxi.LS> getDatos() {
        return datos;
    }

    private SharedPreferences spParametrosConfiguracion;

    public String definirHindCobro() {
        String respuesta = "Usd";
        spParametrosConfiguracion = context.getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 12:
                                if (objetoConfig.getInt("h") == 1) {
                                    respuesta = objetoConfig.getString("nb");
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                Log.e("", "definirMoneda: ", e);
            }
        }
        return respuesta;
    }

}
