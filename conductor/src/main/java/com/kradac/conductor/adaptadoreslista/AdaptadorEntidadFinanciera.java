package com.kradac.conductor.adaptadoreslista;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.modelo.EntidadFinanciera;

import java.util.List;

/**
 * Created by John on 10/08/2016.
 */
public class AdaptadorEntidadFinanciera extends BaseAdapter {
    List<EntidadFinanciera.LT> values;
    Context context;

    public AdaptadorEntidadFinanciera(Context context, List<EntidadFinanciera.LT> values) {
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<EntidadFinanciera.LT> getValues() {
        return values;
    }

    public EntidadFinanciera.LT getEntidadFinanciera(int posicion) {
        return getValues().get(posicion);
    }

    @Override
    public View getView(int pos, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.item_entidad_financiera, null);
        TextView tv_nombre = (TextView) view.findViewById(R.id.tv_nombre);
        TextView tv_descripcion = (TextView) view.findViewById(R.id.tv_descripcion);
        tv_nombre.setText(values.get(pos).getNombre());
        tv_descripcion.setText(values.get(pos).getDescripcion());
        return view;
    }
}
