package com.kradac.conductor.extras;

import android.content.Context;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by John on 16/09/2016.
 */
public class UtilStream {

    // retorna un array de bytes a partir de un archivo en disc
    public static byte[] fileToByteArray(String filename) {
        try {
            FileInputStream fin = new FileInputStream(filename);
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            int len = 0;
            while ((len = fin.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            fin.close();
            byteBuffer.close();
            return byteBuffer.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    // convierte un array de bytes a un archivo en disco
    public static boolean byteArrayToFile(byte[] data, String filename) {
        try {
            ByteArrayInputStream in = new ByteArrayInputStream(data);
            FileOutputStream fout = new FileOutputStream(filename);

            int i;
            while ((i = in.read()) != -1) {
                fout.write((byte) i);
                fout.flush();
            }
            in.close();
            fout.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //comprime un archivo a gzip
    public static void compressGzipFile(String filename, String gzipFilename) {
        try {
            FileInputStream fis = new FileInputStream(filename);
            FileOutputStream fos = new FileOutputStream(gzipFilename);
            GZIPOutputStream gzipOS = new GZIPOutputStream(fos);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = fis.read(buffer)) != -1) {
                gzipOS.write(buffer, 0, len);
            }
            gzipOS.close();
            fos.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static byte[] decompressGzip(byte[] myByteArray, Context context) {
        try {
            File tempMp3 = File.createTempFile("tempfilemp3", "mp3", context.getCacheDir());
            tempMp3.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(tempMp3);
            GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(myByteArray));
            byte[] buffer = new byte[1024];
            int len;
            while ((len = gis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
            //close resources
            fos.close();
            gis.close();
            return fileToByteArray(tempMp3.getAbsolutePath());
        } catch (IOException e) {
            Log.e("ERROR", "decompressGzip: " ,e );
        }
        return new byte[0];
    }
}
