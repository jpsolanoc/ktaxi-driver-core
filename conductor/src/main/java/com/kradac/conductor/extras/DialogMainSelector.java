package com.kradac.conductor.extras;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.kradac.conductor.R;

public class DialogMainSelector  extends Dialog {
    private TextView tvHotmail;
    private TextView tvGmail;
    private TextView tvYahoo;
    private TextView tvOutlook;
    private TextView tvMail;

    private Button btnOtro;
    private MailSelectorListener mailSelectorListener;

    public interface MailSelectorListener {
        void onMainSelected(String main);
    }

    public DialogMainSelector(@NonNull Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.layout_dialog_mail_selector);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        tvHotmail = (TextView) findViewById(R.id.tvHotmail);
        tvGmail = (TextView) findViewById(R.id.tvGmail);
        tvYahoo = (TextView) findViewById(R.id.tvYahoo);
        tvOutlook = (TextView) findViewById(R.id.tvOutlook);
        tvMail = (TextView) findViewById(R.id.tvMail);

        btnOtro = (Button) findViewById(R.id.btnOtro);
        View.OnClickListener onClickListener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView=(TextView) v;
                if (mailSelectorListener != null) {
                    mailSelectorListener.onMainSelected(textView.getText().toString());
                }
                dismiss();
            }
        };
        tvHotmail.setOnClickListener(onClickListener);
        tvGmail.setOnClickListener(onClickListener);
        tvYahoo.setOnClickListener(onClickListener);
        tvOutlook.setOnClickListener(onClickListener);
        tvMail.setOnClickListener(onClickListener);


        btnOtro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mailSelectorListener != null) {
                    mailSelectorListener.onMainSelected("");
                }
                dismiss();
            }
        });
    }

    public MailSelectorListener getMailSelectorListener() {
        return mailSelectorListener;
    }

    public void setMailSelectorListener(MailSelectorListener mailSelectorListener) {
        this.mailSelectorListener = mailSelectorListener;
    }

}
