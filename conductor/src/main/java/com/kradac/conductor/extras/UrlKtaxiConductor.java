package com.kradac.conductor.extras;

/**
 * Created by Ing.John Patricio Solano Cabrera on 29/6/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class UrlKtaxiConductor {

    private String url_produccion_https;
    private String url_produccion_http;
    private String url_produccion_ip;
    private String url_desarrollo_https;
    private String url_desarrollo_http;
    private String url_desarrollo_ip;
    private String url_utilizada;


    public UrlKtaxiConductor() {
        this.url_produccion_https = "https://ktaxifacilsegurorapido.kradac.com/";
        this.url_produccion_http = "http://ktaxifacilsegurorapido.kradac.com/";
        this.url_produccion_ip = "http://169.60.159.25:8080/";
        this.url_desarrollo_https = "https://testktaxi.kradac.com/";
        this.url_desarrollo_http = "http://testktaxi.kradac.com/";
        this.url_desarrollo_ip = "http://169.60.159.19:8080/";

    }

    public String getUrl_produccion_https() {
        return url_produccion_https;
    }

    public void setUrl_produccion_https(String url_produccion_https) {
        this.url_produccion_https = url_produccion_https;
    }

    public String getUrl_desarrollo_https() {
        return url_desarrollo_https;
    }

    public void setUrl_desarrollo_https(String url_desarrollo_https) {
        this.url_desarrollo_https = url_desarrollo_https;
    }

    public String getUrl_produccion_http() {
        return url_produccion_http;
    }

    public void setUrl_produccion_http(String url_produccion_http) {
        this.url_produccion_http = url_produccion_http;
    }

    public String getUrl_desarrollo_http() {
        return url_desarrollo_http;
    }

    public void setUrl_desarrollo_http(String url_desarrollo_http) {
        this.url_desarrollo_http = url_desarrollo_http;
    }

    public String getUrl_utilizada() {
        return url_utilizada;
    }

    public void setUrl_utilizada(String url_utilizada) {
        this.url_utilizada = url_utilizada;
    }

    public String getUrl_produccion_ip() {
        return url_produccion_ip;
    }

    public void setUrl_produccion_ip(String url_produccion_ip) {
        this.url_produccion_ip = url_produccion_ip;
    }

    public String getUrl_desarrollo_ip() {
        return url_desarrollo_ip;
    }

    public void setUrl_desarrollo_ip(String url_desarrollo_ip) {
        this.url_desarrollo_ip = url_desarrollo_ip;
    }
}
