package com.kradac.conductor.extras;

import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.Locale;

/**
 * Created by John on 28/09/2016.
 */

public class ReproducirTextAudio implements TextToSpeech.OnInitListener {
    private static final String TAG = ReproducirTextAudio.class.getName();
    private Context context;
    private TextToSpeech textToSpeech;

    public ReproducirTextAudio(Context context) {
        this.context = context;
        textToSpeech = new TextToSpeech(context, this);
        textToSpeech.setLanguage(new Locale("spa", "ESP"));

    }

    @SuppressWarnings("deprecation")
    public void speak(String str) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            textToSpeech.speak(str, TextToSpeech.QUEUE_FLUSH, null, null);
        } else {
            textToSpeech.speak(str, TextToSpeech.QUEUE_FLUSH, null);
        }
        textToSpeech.setSpeechRate(0.0f);
        textToSpeech.setPitch(0.0f);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.LANG_MISSING_DATA | status == TextToSpeech.LANG_NOT_SUPPORTED) {
            Log.e(TAG, "onInit: " + "ERROR LANG_MISSING_DATA | LANG_NOT_SUPPORTED");
        }
    }

    public TextToSpeech getTextToSpeech() {
        return textToSpeech;
    }

    public void terminarProceso() {
        if (textToSpeech != null) {
            textToSpeech.shutdown();
            textToSpeech.stop();
        }
    }
}
