package com.kradac.conductor.extras;

/**
 * Created by DellKradac on 23/02/2018.
 */

public class LatLng extends com.mapbox.mapboxsdk.geometry.LatLng {

    private double latitude;
    private double longitude;

    public LatLng() {
    }

    public LatLng(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
