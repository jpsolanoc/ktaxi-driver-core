package com.kradac.conductor.extras;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.support.multidex.MultiDex;

import com.activeandroid.ActiveAndroid;
//import com.cumberland.weplansdk.WeplanSdk;
import com.kradac.conductor.mapagestion.SerialesMapBox;

import java.io.File;


/**
 * Created by John on 28/10/2016.
 */

public class ktaxiDriverAplication extends Application {


    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        new SerialesMapBox().cargarDatos();
        StrictMode.VmPolicy.Builder buil = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(buil.build());
        org.osmdroid.config.Configuration.getInstance().setUserAgentValue(getPackageName());
        org.osmdroid.config.Configuration.getInstance().setOsmdroidBasePath(new File(getFilesDir(), "osmdroid"));
        org.osmdroid.config.Configuration.getInstance().setOsmdroidTileCache(new File(getFilesDir(), "osmdroid/tiles"));
    }

}
