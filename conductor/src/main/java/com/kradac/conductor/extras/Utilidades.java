package com.kradac.conductor.extras;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.boletines.BadgeDrawable;
import com.kradac.conductor.modelo.RespuestaConfiguracion;
import com.kradac.conductor.modelo.Solicitud;
import com.kradac.conductor.service.FloatingViewService;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import okhttp3.ResponseBody;


public class Utilidades {

    private static final String TAG = Utilidades.class.getName();

    public void mostrarMensajeCorto(Activity a, String mensaje) {
        Toast.makeText(a.getBaseContext(), mensaje, Toast.LENGTH_SHORT).show();
    }

    public void customToast(Activity a, String texto) {
        try {
            LayoutInflater inflater = a.getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast,
                    (ViewGroup) a.findViewById(R.id.toast_layout_root));
            TextView textToast = (TextView) layout.findViewById(R.id.text_toast);
            textToast.setText(texto);
            Toast toast = new Toast(a.getBaseContext());
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout);
            toast.show();
        } catch (RuntimeException e) {
            Log.e(TAG, "customToast: ", e);
        }
    }

    /**
     * Actividad desde la cual se va a llamar
     * mensaje
     * la imagen que se va a mostrar y el color de fondo que preferible debe ser un drawable personalizado
     *
     * @param context
     * @param mensaje
     * @param imgRecurso
     * @param recursoFondo
     */
    public void customToastSolicitud(Activity context, String mensaje, int imgRecurso, int recursoFondo) {
        LayoutInflater inflater = context.getLayoutInflater();
        View layout = inflater.inflate(R.layout.cutom_toast_error,
                (ViewGroup) context.findViewById(R.id.toast_layout_root));
        LinearLayout ly_fondo = layout.findViewById(R.id.toast_layout_root);
        ly_fondo.setBackgroundResource(recursoFondo);
        ImageView imgPersonalizado = layout.findViewById(R.id.imv_icon);
        imgPersonalizado.setImageResource(imgRecurso);
        TextView text = layout.findViewById(R.id.text);
        text.setText(mensaje);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.TOP, 0, 100);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public void customToastCorto(Activity a, String texto) {
        try {
            LayoutInflater inflater = a.getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast,
                    (ViewGroup) a.findViewById(R.id.toast_layout_root));
            TextView textToast = (TextView) layout.findViewById(R.id.text_toast);
            textToast.setText(texto);
            Toast toast = new Toast(a.getBaseContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setView(layout);
            toast.show();
        } catch (RuntimeException e) {
            Log.e(TAG, "customToastCorto: ", e);
        }
    }


    public String obtenerVersion(Context context) {
        String versionName = "";
        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return versionName;
        }
        return versionName;
    }

    public int obtenerVersionCode(Context context) {
        int versionName = 0;
        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            return versionName;
        }
        return versionName;
    }

    public double distancia(double velocidad, double tiempo) {
        return velocidad * tiempo;
    }


    public LatLng calcularP2(double angulo, double distancia, LatLng latLng) {
        Log.e(TAG, "calcularP2: Angulo:" + angulo + "distancia:" + distancia);
        double x2 = (Math.cos(angulo)) * distancia + latLng.getLatitude();
        double y2 = (Math.sin(angulo)) * distancia + latLng.getLongitude();
        return new LatLng(x2, y2);
    }

    public double calculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius = 6371;// radius of earth in Km
        double lat1 = StartP.getLatitude();
        double lat2 = EndP.getLatitude();
        double lon1 = StartP.getLongitude();
        double lon2 = EndP.getLongitude();
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult = Radius * c;
        double km = valueResult / 1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec = Integer.valueOf(newFormat.format(km));
        double meter = valueResult % 1000;
        int meterInDec = Integer.valueOf(newFormat.format(meter));
        return Radius * c;
    }

    public void neutralizarDialogos(Dialog dialogo) {
        if (dialogo != null && dialogo.isShowing()) {
            dialogo.dismiss();
            dialogo = null;
        }
    }

    public boolean comprobarTamanioClave(String dato) {
        return dato.length() >= 6;
    }


    /*Función que obtiene el IMEI de un dispositivo en Android.*/
    @SuppressLint("HardwareIds")
    public String getImei(Context c) {
        TelephonyManager telephonyManager = (TelephonyManager) c
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(c, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return idDispositivoUni(c);
        }
        if (telephonyManager.getDeviceId() != null) {
            return telephonyManager.getDeviceId();
        } else {
            return idDispositivoUni(c);
        }
    }


    /**
     * Metodo para comprar Horas entre intervalos dia y noche
     *
     * @return
     */
    public boolean compararHorasFin(String horaNocturno, String horaDiurno) {
        if (horaDiurno == null && horaNocturno == null) {
            horaDiurno = "06:00";
            horaNocturno = "19:00";
        }
        int hora_diurno = Integer.parseInt(horaDiurno.split(":")[0]);
        int min_diurno = Integer.parseInt(horaDiurno.split(":")[1]);
        int hora_nocturno = Integer.parseInt(horaNocturno.split(":")[0]);
        int min_nocturno = Integer.parseInt(horaNocturno.split(":")[1]);
        Calendar calendario = new GregorianCalendar();
        int am_pm = calendario.get(Calendar.AM_PM);
        int hora = validar((am_pm == 1 ? "PM" : "AM"), calendario.get(Calendar.HOUR_OF_DAY));
        int minutos = calendario.get(Calendar.MINUTE);
        if ((hora == hora_diurno && minutos >= min_diurno)) {
            return true;
        } else if ((hora == hora_diurno && minutos < min_diurno) || (hora == hora_nocturno && minutos >= min_nocturno)) {
            return false;
        } else if (hora >= hora_diurno && hora <= hora_nocturno) {
            return true;
        } else {
            return false;
        }
    }

    public int validar(String formato, int hora) {
        return ((formato.toUpperCase().equalsIgnoreCase("PM") && hora < 12) ? 12 + hora : hora);
    }

    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


    public void neutralizarDialogo(Dialog dialog) {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    /**
     * Comprueba el estado del gps en las diferentes clases.
     *
     * @param context
     * @return
     */
    public boolean comprobarGPS(Context context) {
        try {
            final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception e) {
            Log.e(TAG, "comprobarGPS: " + e);
            return true;
        }
    }

    public boolean estaConectado(Context context) {
        final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        return !(info == null || !info.isConnected() && !info.isAvailable());

    }


    public boolean isVerificarDialogo(Dialog dialog) {
        if (dialog != null) {
            return !dialog.isShowing();
        } else {
            return true;
        }
    }


    public String obtenerVersionSo() {
        return android.os.Build.VERSION.RELEASE;
    }

    public String obtenerModelo() {
        return android.os.Build.MODEL;
    }


    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }

    public String modeloDispositivo() {
        return Build.MODEL;
    }


    @SuppressLint("HardwareIds")
    public String idDispositivoUni(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }


    public void calificarApp(Context context) {
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    public class LatLngCiudad {
        double latitud;
        double longitud;

        public LatLngCiudad() {
        }

        public double getLatitud() {
            return latitud;
        }

        public void setLatitud(double latitud) {
            this.latitud = latitud;
        }

        public double getLongitud() {
            return longitud;
        }

        public void setLongitud(double longitud) {
            this.longitud = longitud;
        }

        @Override
        public String toString() {
            return "LatLngCiudad{" +
                    "latitud=" + latitud +
                    ", longitud=" + longitud +
                    '}';
        }
    }

    /**
     * Determina la localización en base al id de la ciudad en la cual el taxista es registrado.
     *
     * @param idCiudad
     * @return
     */
    public LatLngCiudad getDefinirLocalizacionInicial(int idCiudad) {
        LatLngCiudad latLngCiudad = new LatLngCiudad();
        switch (idCiudad) {
            case 1:
                latLngCiudad.setLatitud(-4.0086322);
                latLngCiudad.setLongitud(-79.2068151);
                break;
            case 2:
                latLngCiudad.setLatitud(-0.15106184);
                latLngCiudad.setLongitud(-78.46744537);
                break;
            case 3:
                latLngCiudad.setLatitud(-4.06418966);
                latLngCiudad.setLongitud(-78.95123005);
                break;
            case 4:
                latLngCiudad.setLatitud(6.28117348);
                latLngCiudad.setLongitud(-75.59005737);
                break;
            case 7:
                latLngCiudad.setLatitud(-3.82640286);
                latLngCiudad.setLongitud(-78.76150131);
                break;
            case 9:
                latLngCiudad.setLatitud(0.35705335);
                latLngCiudad.setLongitud(-78.10146332);
                break;
            case 10:
                latLngCiudad.setLatitud(-1.01961476);
                latLngCiudad.setLongitud(-79.46411133);
                break;
            case 12:
                latLngCiudad.setLatitud(-1.24805116);
                latLngCiudad.setLongitud(-78.62588882);
                break;
            case 13:
                latLngCiudad.setLatitud(-0.25096813);
                latLngCiudad.setLongitud(-79.17537689);
                break;
            case 15:
                latLngCiudad.setLatitud(-3.25809225);
                latLngCiudad.setLongitud(-79.95471954);
                break;
            case 16:
                latLngCiudad.setLatitud(0.96709427);
                latLngCiudad.setLongitud(-79.65568542);
                break;
            case 18:
                latLngCiudad.setLatitud(-3.4513087);
                latLngCiudad.setLongitud(-79.96210098);
                break;
            case 20:
                latLngCiudad.setLatitud(-1.779499);
                latLngCiudad.setLongitud(-79.57946777);
                break;
            case 21:
                latLngCiudad.setLatitud(3.44162737);
                latLngCiudad.setLongitud(-76.53213501);
                break;
            case 22:
                latLngCiudad.setLatitud(0.8207708);
                latLngCiudad.setLongitud(-77.71290779);
                break;
            case 23:
                latLngCiudad.setLatitud(-2.90395306);
                latLngCiudad.setLongitud(-79.00611877);
                break;
            case 25:
                latLngCiudad.setLatitud(-0.95164692);
                latLngCiudad.setLongitud(-80.71938515);
                break;
            default:
                latLngCiudad.setLatitud(-1.362265);
                latLngCiudad.setLongitud(-77.418327);
                break;
        }
        return latLngCiudad;
    }

    public int getAnio() {
        java.util.Date fecha = new Date();
        return 1900 + fecha.getYear();
    }

    public int getMes() {
        java.util.Date fecha = new Date();
        return fecha.getMonth();
    }

    public String getMesString(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }

    public double formatDecimales(double num, int decimales) {
        if (decimales < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, decimales);
        num = num * factor;
        long tmp = Math.round(num);
        return (double) tmp / factor;
    }


    /**
     * Anterior dos decimales
     *
     * @param numero
     * @return
     */
    public String dosDecimales(Context context, double numero) {
        String[] split = (formatDecimales(numero, RespuestaConfiguracion.numeroDecimales(context)) + "").split("\\.");
        return (split.length == 2) ? split[0] + "." + split[1] + (split[1].length() == 1 ? "0" : "") : "00.00";
    }


    public double getPakagesUsoDatos(Context context) {
        try {
            ApplicationInfo app = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            int UID = app.uid;
            double received = (double) TrafficStats.getUidRxBytes(UID) / (1024 * 1024);
            double send = (double) TrafficStats.getUidTxBytes(UID) / (1024 * 1024);
            return received + send;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }


    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;


    public int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }


    public int getTipoRed(Context context) {
        TelephonyManager teleMan =
                (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return teleMan.getNetworkType();
    }


    /**
     * Metodo para obtener el cartera de la operadora con la cual esta trabajando.
     *
     * @return El cartera de la operadora
     */
    public String getNameOperator(Context context) {
        TelephonyManager telephonyManager = ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE));
        String operatorName = telephonyManager.getNetworkOperatorName();
        if (!operatorName.equalsIgnoreCase("")) {
            return operatorName;
        } else {
            return "SN";
        }
    }


    public int getBatteryLevel(Context context) {
        Intent batteryIntent = context.registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        int level = batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        // Error checking that probably isn't needed but I added just in case.
        if (level == -1 || scale == -1) {
            return (int) 50.0f;
        }
        float a = ((float) level / (float) scale) * 100.0f;
        return (int) a;
    }

    public int statusGPS(Context context) {
        if (this.comprobarGPS(context)) {
            return 1;
        } else {
            return 0;
        }
    }


    public ArrayList<String> extraerTramaTaximetro(String trama) {
        trama = trama + ";";
        String totl = "";
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < trama.length(); i++) {
            char tr = trama.charAt(i);
            if (tr != '#') {
                if (tr != '$') {
                    if (tr != ';') {
                        totl = totl + tr;
                    } else {
                        list.add(totl);
                        totl = "";
                    }
                } else {
                    list.add(totl);
                    totl = "";
                }
            }
        }
        return list;
    }

    @SuppressLint("BatteryLife")
    public void abrirIntentBatery(Context context) {
        Intent intentBatteryUsage = new Intent();
        Log.e(TAG, "abrirIntentBatery: 1");
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                try {
                    Log.e(TAG, "abrirIntentBatery: 2");
                    String packageName = context.getPackageName();
                    PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
                    if (pm.isIgnoringBatteryOptimizations(packageName))
                        intentBatteryUsage.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
                    else {
                        intentBatteryUsage.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                        intentBatteryUsage.setData(Uri.parse("package:" + packageName));
                    }
                    context.startActivity(intentBatteryUsage);
                } catch (ActivityNotFoundException e) {
                    Log.e(TAG, "abrirIntentBatery: 3", e);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                        intentBatteryUsage = new Intent(Settings.ACTION_BATTERY_SAVER_SETTINGS);
                        context.startActivity(intentBatteryUsage);
                    }
                }
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                Log.e(TAG, "abrirIntentBatery: 4");
                intentBatteryUsage = new Intent(Settings.ACTION_BATTERY_SAVER_SETTINGS);
                context.startActivity(intentBatteryUsage);
            }
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "Por favor desactive modo ahorro de energia", Toast.LENGTH_LONG).show();
        }
    }

    private double getBearing(LatLng from, LatLng to) {
        double degrees2radians = Math.PI / 180;
        double radians2degrees = 180 / Math.PI;

        double lon1 = degrees2radians * from.getLongitude();
        double lon2 = degrees2radians * to.getLongitude();
        double lat1 = degrees2radians * from.getLatitude();
        double lat2 = degrees2radians * to.getLatitude();
        double a = Math.sin(lon2 - lon1) * Math.cos(lat2);
        double b = Math.cos(lat1) * Math.sin(lat2)
                - Math.sin(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1);

        return radians2degrees * Math.atan2(a, b);
    }

    public String getTimeDeLocation(Location location, Context context) {
        SimpleDateFormat sdfBase = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date itemDate;
        if (location != null) {
            if (location.getTime() != 0 && comprobarGPS(context)) {
                itemDate = new Date(location.getTime());
            } else {
                itemDate = new Date();
            }
        } else {
            itemDate = new Date();
        }
        return sdfBase.format(itemDate);
    }

    public String getDateGps(Location location) {
        SimpleDateFormat sdfBase = new SimpleDateFormat("yyyy-MM-dd");
        Date itemDate;
        if (location != null) {
            if (location.getTime() != 0) {
                itemDate = new Date(location.getTime());
            } else {
                itemDate = new Date();
            }
        } else {
            itemDate = new Date();
        }
        return sdfBase.format(itemDate);
    }

    public String getHourGps(Location location) {
        SimpleDateFormat sdfBase = new SimpleDateFormat("HH:mm:ss");
        Date itemDate;
        if (location != null) {
            if (location.getTime() != 0) {
                itemDate = new Date(location.getTime());
            } else {
                itemDate = new Date();
            }
        } else {
            itemDate = new Date();
        }
        return sdfBase.format(itemDate);
    }

    public int getAcuryTramaEnvio(double acury) {
        int envio = 0;
        if (acury <= 50) {
            envio = 1;
        } else if (acury >= 51 && acury <= 100) {
            envio = 2;
        } else if (acury >= 101 && acury <= 250) {
            envio = 3;
        } else if (acury >= 251 && acury <= 500) {
            envio = 4;
        } else if (acury >= 501 && acury <= 650) {
            envio = 5;
        } else if (acury >= 651 && acury <= 1000) {
            envio = 6;
        } else if (acury >= 1001 && acury <= 2000) {
            envio = 7;
        } else if (acury >= 2001 && acury <= 4000) {
            envio = 8;
        } else if (acury >= 4001) {
            envio = 9;
        }
        return envio;
    }

    public int valorMaximo(int numero, int valorMaximo) {
        if (numero < valorMaximo) {
            return numero;
        } else {
            return 0;
        }
    }

    public boolean pingGoogle() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process mIpAddrProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int mExitValue = mIpAddrProcess.waitFor();
            return mExitValue == 0;
        } catch (InterruptedException | IOException e) {
            Log.e(TAG, "pingGoogle: ", e);
        }
        return false;
    }

    /***
     * Utilidades de taximetro
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return
     */
    public double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double radius = 6371000; // Radio de la tierra
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2)
                * Math.sin(dLon / 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return radius * c;
    }

    /**
     * Utilidades de taximetro
     *
     * @return
     */
    public String obtenerHora() {
        Calendar cal = new GregorianCalendar();
        java.util.Date date = cal.getTime();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        String formatteDate = df.format(date);
        return formatteDate;
    }

    /**
     * Utilidades taximetro
     *
     * @return
     */
    private String obtenerHoraUnica() {
        Calendar cal = new GregorianCalendar();
        java.util.Date date = cal.getTime();
        SimpleDateFormat df = new SimpleDateFormat("HH");
        return df.format(date);
    }

    public String getTimeFormat(Date date) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return sdf2.format(date);
    }

    /**
     * if respuesta < 0 fecha anterior a la del sistema false
     * * if respuesta = 0 Fecha Actual a la adel sistema false
     * * if respuesta > 0 fecha posterior a la del sistema true
     *
     * @param fecha
     * @param formatoFecha
     * @return
     */
    public boolean compararFechas(String fecha, String formatoFecha) {
        SimpleDateFormat formatter = new SimpleDateFormat(formatoFecha);
        try {
            Date date = formatter.parse(fecha);
            int respuesta = date.compareTo(new Date());
            return (respuesta > 0);
        } catch (ParseException ex) {
            Log.e(TAG, "compararFechas: ", ex);
        }
        return false;
    }


    public boolean isInstaladaAplicacion(String nombrePaquete, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(nombrePaquete, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


    /**
     * "br.com.easytaxista" ejemplo
     *
     * @param context
     * @param nombrePaquete
     */
    public void startInstalledAppDetailsActivity(final Activity context, String nombrePaquete) {
        if (context == null && !isInstaladaAplicacion(nombrePaquete, context)) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + nombrePaquete));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    /**
     * @param paquete Nombre del paquete
     * @param context Contexto de la aplicación
     * @return
     */
    public boolean verificarEjecucionApp(String paquete, Context context) {
        ActivityManager am = (ActivityManager) context.getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo rsi : am.getRunningServices(Integer.MAX_VALUE)) {
            String pkgName = rsi.service.getPackageName();
            if (pkgName != null) {
                if (pkgName.equals(paquete)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Detectar si una aplicacion esta instalada.
     *
     * @param nombrePaquete
     * @param context
     * @return
     */
    public boolean estaInstaladaAplicacion(String nombrePaquete, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(nombrePaquete, PackageManager.GET_ACTIVITIES);
            pm.getPackageInfo(nombrePaquete, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    /**
     * false = la activiadad esta finalizada
     * true = la activiadad esta activa
     *
     * @param clase
     * @return
     */
    public boolean isActividadRun(Activity clase) {
        return !clase.isFinishing();
    }


    public String SHA256(String stringAEncriptar) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] encodedhash = digest.digest(stringAEncriptar.getBytes(Charset.forName("UTF-8")));
        return bytesToHex(encodedhash);
    }

    public String getTimeStanD() {
        return String.valueOf(Calendar.getInstance().getTime().getTime());
    }

    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    //Comprueba si esta en desarrollo o produccion
    public boolean isDesarrolloPorduccion(Context context) {
        SharedPreferences spConfigServerNew = context.getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
        return spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true);
    }

    //Devuelve el color en base al que se envie
    public int getColorWrapper(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getColor(id);
        } else {
            //noinspection deprecation
            return context.getResources().getColor(id);
        }
    }

    /**
     * @param texto
     * @param recurso
     * @return
     */
    public Drawable setBadgeCount(String texto, Drawable recurso) {
        Drawable reuse = recurso;
        BadgeDrawable badge;
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
            Log.e(TAG, "setBadgeCount: 1");
        } else {
            badge = new BadgeDrawable();
            Log.e(TAG, "setBadgeCount: 2");
        }
        badge.setLetter(texto);
        Drawable[] capas = new Drawable[2];
        capas[0] = reuse;
        capas[1] = badge;
        LayerDrawable capasDrawable = new LayerDrawable(capas);
        return capasDrawable;
    }


    public String tiempoDistancia(Solicitud solicitud) {
        double distancia;
        if (solicitud.getDistancia() < 1) {
            distancia = (int) (solicitud.getDistancia() * 1000);
            if (solicitud.getTiempo() < 1) {
                return "1 min / " + distancia + " m";
            } else {
                return solicitud.getTiempo() + " min / " + distancia + " m";
            }
        } else {
            distancia = round(solicitud.getDistancia(), 2);
            if (solicitud.getTiempo() < 1) {
                return "1 min / " + distancia + " Km";
            } else {
                return solicitud.getTiempo() + " min / " + distancia + " Km";
            }
        }
    }


    public boolean ocularMostrarCampos(String dato) {
        return !dato.trim().equals("NONE");
    }


    public int iconFormaPagoSolicitud(int tipoFormaPago) {
        switch (tipoFormaPago) {
            case VariablesGlobales.VOUCHER:
                return R.mipmap.voucherapp;
            case VariablesGlobales.VOUCHER_CALL_CENTER:
                return R.mipmap.vouchercall;
            case VariablesGlobales.DINERO_ELECTRONICO:
                return R.mipmap.ic_dinero_electronico;
            case VariablesGlobales.PAYPHONE:
                return R.mipmap.ic_icon_payphone;
            case VariablesGlobales.TARJETA_CREDITO:
                return R.mipmap.ic_icon_tarjeta_credito;
            case VariablesGlobales.TARJETA_CREDITO_NUEVO:
                return R.mipmap.ic_icon_tarjeta_credito;
            default:
                return (R.mipmap.ic_efectivo);
        }
    }


    public int iconEstadoGpsEnSolicitud(int confiabilidad) {
        switch (confiabilidad) {
            case 0:
                return R.drawable.ic_0;
            case 1:
                return R.drawable.ic_1;
            case 2:
                return R.drawable.ic_2;
            case 3:
                return R.drawable.ic_3;
            default:
                return R.drawable.ic_4;
        }
    }

    public int iconEstadoGpsEnSolicitudMarket(int confiabilidad) {
        switch (confiabilidad) {
            case 0:
                return R.mipmap.ic_solicitud0;
            case 1:
                return R.mipmap.ic_solicitud1;
            case 2:
                return R.mipmap.ic_solicitud2;
            case 3:
                return R.mipmap.ic_solicitud3;
            default:
                return R.mipmap.ic_solicitud4;
        }
    }


    public int iconTipoSolitud(int tipo) {
        switch (tipo) {
            case 1:
                return R.mipmap.icon_pedido;
            case 2:
                return R.mipmap.icon_encomienda;
            default:
                return R.mipmap.icon_solicitud;
        }
    }

    public Drawable drawableFondoSolicitud(boolean isQuiosco, int formaPago, boolean isPedido, int tipoPedido, Context context) {
        if (isQuiosco) {
            return ContextCompat.getDrawable(context, R.drawable.style_solicitud_quiosco);
        } else if (!isPedido) {
            switch (formaPago) {
                case VariablesGlobales.VOUCHER:
                    return ContextCompat.getDrawable(context, R.drawable.style_solicitud_normal_vaucher);
                case VariablesGlobales.VOUCHER_CALL_CENTER:
                    return ContextCompat.getDrawable(context, R.drawable.style_solicitud_normal_vaucher);
                case VariablesGlobales.DINERO_ELECTRONICO:
                    return ContextCompat.getDrawable(context, R.drawable.style_solicitud_normal_dinero_electronico);
                case VariablesGlobales.PAYPHONE:
                    return ContextCompat.getDrawable(context, R.drawable.style_solicitud_normal_efectivo);
                case VariablesGlobales.TARJETA_CREDITO:
                    return ContextCompat.getDrawable(context, R.drawable.style_solicitud_normal_efectivo);
                case VariablesGlobales.TARJETA_CREDITO_NUEVO:
                    return ContextCompat.getDrawable(context, R.drawable.style_solicitud_normal_efectivo);
                default:
                    return ContextCompat.getDrawable(context, R.drawable.style_solicitud_normal_efectivo);
            }
        } else {
            switch (tipoPedido) {
                case 2:
                    return ContextCompat.getDrawable(context, R.drawable.style_solicitud_pedido_efectivo);
                default:
                    return ContextCompat.getDrawable(context, R.drawable.style_solicitud_encomienda_efectivo);
            }
        }
    }


    public double valorDecimalTaximetro(double valor, double valorVariable) {
        String datoEntrada = String.valueOf(valor);
        double valorEntero = (Double.parseDouble(datoEntrada.split("\\.")[0]));
        double valorDecimal = (Double.parseDouble("0." + datoEntrada.split("\\.")[1]));
        double resultadoDecimal = Math.round(((valorDecimal / valorVariable))) * valorVariable;
        return valorEntero + resultadoDecimal;
    }


    public void showFloatingView(final Context context, final boolean isMapa) {
        SharedPreferences configuracionApp = context.getSharedPreferences(VariablesGlobales.CONFIGURACION_APP, Context.MODE_PRIVATE);
        if (configuracionApp.getBoolean(VariablesGlobales.CONFIGURACION_BOTON_FLOTANTE, true)) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!Settings.canDrawOverlays(context)) {
                    Toast.makeText(context, "Por favor de los permisos para mostrar la ventana emergente.", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + (context.getPackageName())));
                    context.startActivity(intent);

                } else {
                    Intent intent = new Intent(context.getApplicationContext(), FloatingViewService.class);
                    intent.putExtra("isMapa", isMapa);
                    context.startService(intent);
                }
            } else {
                Intent intent = new Intent(context.getApplicationContext(), FloatingViewService.class);
                intent.putExtra("isMapa", isMapa);
                context.startService(intent);
            }
        }
    }

    public void hideFloatingView(Context context) {
        context.stopService(new Intent(context.getApplicationContext(), FloatingViewService.class));
    }

    public void cargarImagenPublicitaria(final ImageView imageView, String url, Context context, final ProgressBar progressBar) {
        DisplayImageOptions options = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.RGB_565).cacheOnDisk(true).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        imageLoader.displayImage(url, imageView, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                imageView.setImageBitmap(loadedImage);
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
            }
        });
    }

    public boolean isVertical(Context context) {
        int rotacion = context.getResources().getConfiguration().orientation;
        if (rotacion == Surface.ROTATION_0 || rotacion == Surface.ROTATION_180) {
            return true;
        } else {
            return false;
        }
    }

    public String writeResponseBodyToDisk(ResponseBody body, int tipo) {
        try {
            File futureStudioIconFile = createImageFile(tipo);
            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;
                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();
                return futureStudioIconFile.getAbsolutePath();
            } catch (IOException e) {
                return null;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return null;
        }
    }

    private File createImageFile(int tipo) throws IOException {
        String imageFileName = "publicidad-";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS);
        String tipoArchivo = ".jpg";
        switch (tipo) {
            case 1:
                tipoArchivo = ".mp4";
                break;
            case 3:
                tipoArchivo = ".gif";
                break;
        }
        return File.createTempFile(
                imageFileName,  // prefix
                tipoArchivo,         // suffix
                storageDir      // directory
        );
    }

    public void definirNombreServicio(Solicitud solicitudSeleccionada, TextView tvNombreServicio) {
        if (solicitudSeleccionada.getServicio() != null) {
            tvNombreServicio.setText(solicitudSeleccionada.getServicio());
        } else {
            tvNombreServicio.setText("Solicitud taxi");
            tvNombreServicio.setVisibility(View.GONE);
            //El textView puede estar invisible si no llega
        }
    }

    public void subrayarTextoConAcciones(String text, TextView tvHide) {
        SpannableString spanString = new SpannableString(text);
        spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
        tvHide.setText(spanString);
    }

    public LatLng puntoMedioDosPuntos(double x, double y, double w, double z) {
        double xFinal = (x + w) / 2;
        double yFinal = (y + z) / 2;
        return new LatLng(xFinal, yFinal);
    }

    public String obtenerNumero(String numero) {
        String num;
        num = numero.substring(1);
        return "593".concat(num);
    }

    public String getHoraActual() {
        Date ahora = new Date();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formateador = new SimpleDateFormat("HH:mm:ss");
        return formateador.format(ahora);
    }

}
