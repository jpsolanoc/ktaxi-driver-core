package com.kradac.conductor.extras;

public class VariablesGlobales {

    public static final String ID_USUARIO = "idUsuario";
    public static final String ID_PEDIDO = "idPedido";
    public static final String ID_VEHICULO = "idVehiculo";
    public static final String ID_CIUDAD = "idCiudad";
    public static final String CIUDAD = "ciudad";
    public static final String ID_EMPRESA = "idEmpresa";
    public static final String ID_EVENTUALIDAD = "idEventualidad";
    public static final String NOMBRES = "nombres";
    public static final String MENSAJE = "mensaje";
    public static final String APELLIDOS = "apellidos";
    public static final String EMPRESA = "empresa";
    public static final String UNIDAD = "unidad";
    public static final String UNIDAD_VEHICULO = "unidadVehiculo";
    public static final String PLACA = "placa";
    public static final String PLACA_VEHICULO = "placaVehiculo";
    public static final String CEDULA = "cedula";
    public static final String REG_MUNICIPAL = "regMunicipal";
    public static final String REG_MUN_VEHICULO = "regMunVehiculo";
    public static final String TELEFONO = "telefono";
    public static final String CELULAR = "celular";
    public static final String PIN = "pin";
    public static final String PROPINA = "p";
    public static final String CNF = "cnf";
    public static final String IMAGEN = "imagen";
    public static final String IMAGEN_CONDUCTOR = "imagenConductor";
    public static final String TIEMPO = "tiempo";
    public static final String ATENDIDO_DESDE = "atendidaDesde";
    public static final String ESTADO = "estado";
    public static final String ID_SOLICITUD = "idSolicitud";
    public static final String LOGEADO = "logeado";
    public static final String CONFIG_IP = "configIP";
    public static final String CONFIG_ESTADO_BTN = "estadoBotonLibreOcupado";
    public static final String PARAMETRO_TAXIMETRO_TEMPORAL = "parametroTaximetroTemporal";
    public static final String CONFIG_IP_NEW = "configuraciones_nuevas";
    public static final String ESTADO_BTN = "estadoBoton";
    public static final String LOGIN = "login";
    public static final String PARAMETROS_CONFIGURACION = "parametros_configuracion";
    public static final String SP_PRECIO_COMPRA = "PRECIO_COMPRA";
    public static final String SP_PRE_REGISTRO = "PRE_REGISTRO";
    public static final String ABORDADO_POR = "abordadoPor";
    public static final String ESCUCHA_EN_NUEVA_SOLICITUD = "en_nueva_solicitud";
    public static final String ESCUCHA_EN_SOLICITUD = "en_solicitud";
    public static final String ESCUCHA_KTAXI_DRIVER_ESCUCHA_MENSAJES = "operador_escucha_mensaje";
    public static final String ESCUCHA_EN_POSIBLES_SOLICITUDES = "en_escuchar_evento";
    public static final String ESCUCHA_EN_AUDIO_BROADCAST_POR_EMPRESA = "en_audio_broadcast_por_empresa";
    public static final String ESCUCHA_EN_AUDIO_VEHICULO_POR_UNIDAD = "en_audio_vehiculo_por_unidad";
    public static final String ESCUCHA_EN_AUDIO_BROADCAST = "en_audio_broadcast";
    public static final String EMIT_RECONECTAR_KTAXI = "reconectar_ktaxi";
    public static final String EMIT_LOGEAR_KTAXI = "logear_ktaxi_aplicativo";
    public static final String EMIT_LOGEAR_KTAXI_POR_VEHICULO = "logear_ktaxi_por_vehiculo";
    public static final String EMIT_DESCONECTAR_KTAXI = "desconectar_ktaxi";
    public static final String EMIT_CONSULTAR_SOLICITUDES_CERCANAS_VEHICULO = "consultar_solicitudes_cercanas_por_vehiculo";
    public static final String EMIT_ATENDER_SOLICITUD = "atender_solicitud";
    public static final String EMIT_CANCELAR_SOLICITUD_KTAXI = "cancelar_solicitud_ktaxi";
    public static final String EMIT_CANCELAR_PEDIDO_KTAXI = "cancelar_pedido_operador";
    public static final String EMIT_ENVIAR_AVISO = "enviar_aviso";
    public static final String EMIT_ESTADO_GPS_VALIDACION = "n_c_gps";
    public static final String EMIT_ESTADO_GPS_VALIDACION_CALLCENTER = "aceptacion_solicitud_cc";

    public static final String EMIT_RECOMENDACION = "obtener_recomendacion";
    public static final String EMIT_ABORDAR_VEHICULO = "abordar_vehiculo";
    public static final String EMIT_CONSULTAR_PEDIDO_CONDUCTOR = "consultar_pedido_conductor";

    public static final String EMIT_OPERADOR_ENVIA_MENSAJE = "operador_envia_mensaje";
    public static final String EMIT_OPERADOR_ENVIA_MENSAJE_DATA = "operador_envia_audio";
    public static final String TIPO_FORMA_PAGO = "tFp";
    public static final String EMIT_ENVIAR_LOCALIZACION = "t";
    public static final String EMIT_COSULTAR_LOCALIZACION_PANICO = "consultar_localizacion_ack";
    public static final String EMIT_ENVIAR_LOCALIZACION_LOTE = "l";
    public static final String EMIT_ENVIAR_ESTADO_KTAXI = "enviar_estado_ktaxi";
    public static final String DATOS_SOLICITUD = "datosSolicitud";
    public static final String ID_CLIENTE = "idCliente";
    public static final String USERNAME = "username";
    public static final String REFERENCIA_CLIENTE = "referenciaCliente";

    public static final String CALLE_SECUNDARIA = "calleSecundaria";
    public static final String CALLE_PRINCIPAL = "callePrincipal";
    public static final String BARRIO_CLIENTE = "barrioCliente";
    public static final String LATITUD = "latitud";
    public static final String LONGITUD = "longitud";

    public static final String LATITUD_DESTINO = "ltD";
    public static final String LONGITUD_DESTINO = "lgD";

    public static final String DISTANCIA = "distancia";
    public static final String ID_SOLICITUD_GENERAL = "id_solicitud_general";
    public static final String REFERENCIA_CLIENTE_2 = "referencia_cliente";
    public static final String NUMERO_UNIDAD = "numeroUnidad";
    public static final String DATOS_CONFIGURACION = "datos_configuracion";
    public static final String DATOS_CONFIGURACION_GLOBAL = "datos_configuracion_global";

    public static final String COBRA_OPERADOR_VAUCHER_CLIENTE_UIDO = "cobra_vaucher_cliente_uido";
    public static final String COBRA_OPERADOR_VAUCHER_CON_CONFIRMACION = "cobra_vaucher_con_confirmacion";
    public static final String COBRA_OPERADOR_VAUCHER_CON_PIN = "cobra_vaucher_con_pin";
    public static final String COBRA_OPERADOR_VAUCHER_CON_PIN_CALL_CENTER = "cobra_vaucher_call_center_pin";
    public static final String OPERADOR_RECIVE_CONFIRMACION_PAGO = "operador_recive_confirmacion_pago";


    public static final String MODO_RASTREO = "MODO_RASTREO";
    public static final String IS_MODO_CONTADOR = "IS_MODO_CONTADOR";

    /**
     * Variables gloabales para metodo de validacion de firebase
     */
    public static final String ESTADO_FIREBASE = "estado_firebase";
    public static final String KEY_FIREBASE_ENVIADO = "key_firebase_enviado";
    /**
     * Formas de pago
     */
    public static final int VOUCHER = 1;
    public static final int DINERO_ELECTRONICO = 2;
    public static final int PAYPHONE = 3;
    public static final int SALDO_KTAXI = 4;
    public static final int TARJETA_CREDITO = 6;
    public static final int TARJETA_CREDITO_NUEVO = 7;
    public static final int VOUCHER_CALL_CENTER = 5;


    /**
     * Variables de configuracion del sistema
     */
    public static final String CONFIGURACION_APP = "configuracion_app_new";
    public static final String TIPO_MAPA = "tipoMapa_new";
    public static final String TIPO_MAPA_ULTIMO_USADO = "tipoMapaUltimoUsado";
    public static final String ASISTENTE_VOZ = "asistenteVoz";
    public static final String CONFIGURACION_ESTILO_MAPA = "configuracionEstiloMapa";
    public static final String CONFIGURACION_PANTALLA = "configuracionPantalla";
    public static final String CONFIGURACION_ORIENTACION = "configuracionOrientacion";
    public static final String CONFIGURACION_BOTON_FLOTANTE = "configuracionBotonFlotante";
    public static final String CONFIGURACION_CHAT_AUDIOS = "configuracionChatAudios";
    public static final String CONFIGURACION_TAXIMETRO = "configuracionTaximetro";
    public static final String SOLICITUDES_NORMALES = "solicitudesNormales";
    public static final String SOLICITUDES_PEDIDOS = "solicitudesPedidos";
    public static final String SOLICITUDES_QUIOSCO = "solicitudesQuiosco";
    public static final String JSON_RAZON = "razon";
    public static final String ID_APLICATIVO = "idAplicativo";
    public static final String EN_LLAMADA = "sensor_en_llamada";
    public static final String OPCION_PUSH = "activar_push";

    public static final int APP = 2;
    public static final int ID_PLATAFORMA = 1;

    public static void setNumIdAplicativo(int numIdAplicativo) {
        NUM_ID_APLICATIVO = numIdAplicativo;
    }

    /**
     * ktaxi = 1
     * Azutaxi = 2
     * PinosChiclayo = 13
     * PinosSeguro = 14
     * fedotaxi = 5
     * MiTaxi = 9
     */
    public static int NUM_ID_APLICATIVO = 1;


    public static final String FUNCIONALIDADES = "funcionalidades";
    public static final String FUNCIONALIDADES_REFERIDO = "funcionalidades_referido";

    public static String IP_SERVICIOS_DIRECCIONES = "http://router.project-osrm.org/";

    public static final String AMBIENTE = "ambiente";

    /***
     * Variables para taximetro
     */
    public static final String SP_TAXIMETRO = "taximetro";
    public static final String SP_TAXIMETRO_DATOS = "taximetro_datos";

    public static final String SP_TAXIMETRO_SERVICIO_ACTIVO = "taximetro_id_servicio_activo";
    public static final String SP_TAXIMETRO_DATOS_SERVICIO_ACTIVO = "taximetro_datos_id_servicio_activo";



    private static final String URL_SERVIDOR_PRODCUCCION = "https://ktaxifacilsegurorapido.kradac.com/";
    public static final String URL_SERVIDOR_HELP = "http://ktaxifacilsegurorapido.kradac.com/";

    public static String IP_SERVICIOS_SOCKET = URL_SERVIDOR_PRODCUCCION;


    public static final String CONFIGURACION_INICIAL = "{\"IpUsada\":\"https://ktaxifacilsegurorapido.kradac.com/\",\"dns\":{\"dnsD\":\"http://174.37.68.36:3000/\",\"dnsE\":\"http://174.37.68.38:8080/\",\"dnsN\":\"http://173.192.13.12:9090/\",\"en\":0,\"imgA\":\"\",\"imgC\":\"http://www.web.ktaxi.com.ec/ktaxides/img/uploads/people/\",\"imgPub\":\"http://www.web.ktaxi.com.ec/ktaxides/img/ktaxi/img/uploads/publicidades/\",\"imgSer\":\"http://www.web.ktaxi.com.ec/ktaxides/img/uploads/servicios/\",\"imgU\":\"http://www.web.ktaxi.com.ec/ktaxides/img/uploads/clientes/\",\"m\":\"\",\"sImgU\":\"http://www.web.ktaxi.com.ec/ktaxides/img/uploads/clientes/putImage.php\",\"v\":\"2.1.23\"},\"en\":2,\"m\":\"¡Tu APP necesita ser actualizada!\"}";

    public static void setIpServer(boolean isAmbiente, String ipUsar) {
        if (isAmbiente) {
            IP_SERVICIOS_SOCKET = ipUsar;
        } else {
            IP_SERVICIOS_SOCKET = ipUsar;
        }
    }

    public static final String SP_REGISTRAR_PUSH = "registrar_push";
    public static final String SP_DATA_REGISTRAR_PUSH = "is_push";

    public static final String STR_AVANZADO = "krcloja$$";


    /**REgistro de clases en servicio
     *
     */

    public static int TIEMPORASTREO = 17000;
}
