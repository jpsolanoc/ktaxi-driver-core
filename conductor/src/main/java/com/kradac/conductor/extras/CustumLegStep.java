package com.kradac.conductor.extras;


import com.mapbox.services.api.directions.v5.models.LegStep;

/**
 * Created by John on 29/07/2016.
 */
public class CustumLegStep {
    private LegStep legStep;
    private boolean isPadasa;


    public LegStep getLegStep() {
        return legStep;
    }

    public void setLegStep(LegStep legStep) {
        this.legStep = legStep;
    }

    public boolean isPadasa() {
        return isPadasa;
    }

    public void setPadasa(boolean padasa) {
        isPadasa = padasa;
    }
}
