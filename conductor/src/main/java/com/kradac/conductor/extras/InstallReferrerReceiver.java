package com.kradac.conductor.extras;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class InstallReferrerReceiver extends BroadcastReceiver {
    public static final String NAME_PREFERENCE_CODIGO_REFERIDO = "codigo_referrer";

    @Override
    public void onReceive(Context context, Intent intent) {
        String referrer = intent.getStringExtra("referrer");
        if (referrer != null) {
            if (!referrer.isEmpty() && !referrer.contains("utm_source")) {
                SharedPreferences.Editor mPrefs = context.getSharedPreferences(NAME_PREFERENCE_CODIGO_REFERIDO, Context.MODE_PRIVATE).edit();
                mPrefs.putString("codigo", referrer);
                mPrefs.apply();
            }
        }
    }

    public static String getCodigoReferido(Context context) {
        String codigo = "";
        SharedPreferences mPrefs = context.getSharedPreferences(NAME_PREFERENCE_CODIGO_REFERIDO, Context.MODE_PRIVATE);
        if (mPrefs.contains("codigo")) {
            codigo = mPrefs.getString("codigo", "");
        }
        return codigo;
    }

    public static void clearCodigoReferido(Context context) {
        SharedPreferences.Editor mPrefs = context.getSharedPreferences(NAME_PREFERENCE_CODIGO_REFERIDO, Context.MODE_PRIVATE).edit();
        mPrefs.clear();
        mPrefs.apply();
    }
}