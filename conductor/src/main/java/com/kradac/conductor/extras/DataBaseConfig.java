package com.kradac.conductor.extras;

import com.activeandroid.Configuration;
import com.activeandroid.content.ContentProvider;
import com.kradac.conductor.modelo.CorazonMalvado;
import com.kradac.conductor.mapagestion.SerialesMapBox;
import com.kradac.conductor.modelo.CalendarHistorial;
import com.kradac.conductor.modelo.DatosEnvioTaximetro;
import com.kradac.conductor.modelo.ItemHistorialCompras;
import com.kradac.conductor.modelo.ItemHistorialSolicitud;
import com.kradac.conductor.modelo.PosicionNueva;
import com.kradac.conductor.modelo.VideosPropaganda;

public class DataBaseConfig extends ContentProvider {

    @Override
    protected Configuration getConfiguration() {
        Configuration.Builder builder = new Configuration.Builder(getContext());
        switch (VariablesGlobales.NUM_ID_APLICATIVO) {
            case 2: //Azutaxi
                builder.setDatabaseName("azutaxi_conductor_data_v36b");
                builder.setDatabaseVersion(13);
                break;
            case 5: //Fedotaxi
                builder.setDatabaseName("fedotaxi_conductor_data_v4b");
                builder.setDatabaseVersion(23);
                break;
            case 9: //MiTaxi
                builder.setDatabaseName("mitaxi_conductor_data_v3b");
                builder.setDatabaseVersion(13);
                break;
            case 13: //PinosChiclayo
                builder.setDatabaseName("Chiclayo_conductor_dasta_v4b");
                builder.setDatabaseVersion(3);
                break;
            case 14: //PinosSeguro
                builder.setDatabaseName("pinosseguro_conductor_data_v3b");
                builder.setDatabaseVersion(3);
                break;
            default: //Ktaxi Conductor
                builder.setDatabaseName("ktaxi_conductor_data_v38b");
                builder.setDatabaseVersion(15);
                break;
        }
        builder.addModelClass(ItemHistorialSolicitud.class);
        builder.addModelClass(CorazonMalvado.class);
        builder.addModelClass(ItemHistorialCompras.class);
        builder.addModelClass(CalendarHistorial.class);
        builder.addModelClass(SerialesMapBox.class);
        builder.addModelClass(PosicionNueva.class);
        builder.addModelClass(VideosPropaganda.class);
        builder.addModelClass(DatosEnvioTaximetro.class);
        return builder.create();
    }
}