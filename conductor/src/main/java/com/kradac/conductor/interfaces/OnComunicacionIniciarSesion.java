package com.kradac.conductor.interfaces;

import com.google.android.gms.common.api.Status;
import com.kradac.conductor.modelo.ConfiguracionDesdeServidor;
import com.kradac.conductor.modelo.MensajeGenerico;

/**
 * Created by John on 29/11/2016.
 */

public interface OnComunicacionIniciarSesion {



    void iniciarSesionServidor (String dato);

    void respuestaRegistrarImei(String respuesta);

    void onGpsDisableNewApi(Status status);

    void onGpsEnable();

    void cambioGpsNew(Status status);

    void cambioGps();

    void configuracionServidorNew(boolean isEstado);

    void respuestaPin(boolean isRespuesta,String ipRespuesta);

    void respuestaHelp(String json);

    void clickItemLlamar();

    void otraApp(String mensaje,String paquete,boolean isDesinstalar);

}
