package com.kradac.conductor.interfaces;


import com.kradac.conductor.modelo.Resultado;

public interface OnComunicacionPais {

    void respuestaPais(Resultado resultado);

}
