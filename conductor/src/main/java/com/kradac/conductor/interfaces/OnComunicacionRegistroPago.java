package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.EntidadFinanciera;

/**
 * Created by Ing.John Patricio Solano Cabrera on 25/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public interface OnComunicacionRegistroPago {
    void respuestaEntidadFinanciera(EntidadFinanciera entidadFinanciera);

    void respuestaRegistrarpago(String dato);

    void respuestaEnviarImagen(String dato, String nombreImagen);

    void respuestaRegistrarTicket(String dato);
}
