package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.Cuenta;

import java.util.ArrayList;
/**
 * Created by John on 04/01/2017.
 */
public interface OnComunicacionCuentas {

     void responseCuentas(Cuenta cuentas);

}
