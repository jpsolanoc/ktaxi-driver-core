package com.kradac.conductor.interfaces;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.modelo.ComponentePublicitario;
import com.kradac.conductor.modelo.CompraServiciosCategorias;
import com.kradac.conductor.modelo.ConfiguracionDesdeServidor;
import com.kradac.conductor.modelo.ConfiguracionServidor;
import com.kradac.conductor.modelo.ConsultarMovimientos;
import com.kradac.conductor.modelo.ConsultarSaldoTarjetaCredito;
import com.kradac.conductor.modelo.Cuenta;
import com.kradac.conductor.modelo.DatosLogin;
import com.kradac.conductor.modelo.DatosNotificacion;
import com.kradac.conductor.modelo.DatosTaximetro;
import com.kradac.conductor.modelo.DatosTaximetroServicioActivo;
import com.kradac.conductor.modelo.DatosTrafico;
import com.kradac.conductor.modelo.DineroElectronico;
import com.kradac.conductor.modelo.EntidadFinanciera;
import com.kradac.conductor.modelo.Historial;
import com.kradac.conductor.modelo.ItemHistorialCompras;
import com.kradac.conductor.modelo.ItemHistorialSolicitud;
import com.kradac.conductor.modelo.MensajeGenerico;
import com.kradac.conductor.modelo.PagosConductor;
import com.kradac.conductor.modelo.ResponseApi;
import com.kradac.conductor.modelo.ResponseApiComponente;
import com.kradac.conductor.modelo.ResponseApiCorto;
import com.kradac.conductor.modelo.ResponseDatosConductor;
import com.kradac.conductor.modelo.ResponseGenerarToken;
import com.kradac.conductor.modelo.ResponseListarCodigosReferido;
import com.kradac.conductor.modelo.ResponseListarTarjetaCredito;
import com.kradac.conductor.modelo.ResponseMapboxToken;
import com.kradac.conductor.modelo.ResponseValorar;
import com.kradac.conductor.modelo.Respuesta;
import com.kradac.conductor.modelo.RespuestaComentarios;
import com.kradac.conductor.modelo.RespuestaConsultarCodigoReferido;
import com.kradac.conductor.modelo.RespuestaContactenos;
import com.kradac.conductor.modelo.RespuestaSaldoPaquetes;
import com.kradac.conductor.modelo.Resultado;
import com.kradac.conductor.modelo.SaldoKtaxi;
import com.kradac.conductor.modelo.Sugerencia;
import com.kradac.conductor.modelo.TransaccionesSaldoKtaxi;
import com.kradac.conductor.modelo.ValoracionConductor;
import com.kradac.conductor.presentador.RespuestaComponente;
import com.kradac.wigets.modelo.RespuestaPublicidad;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by John on 28/06/2016.
 */
public class ApiKtaxi {
    //verifica si existes una notificacion de inicio
    public interface Notificacion {
        @GET("boletines_usuarios/leer-boletin/{idUsuario}/{idCiudad}")
        Call<DatosNotificacion> verificarMensaje(@Path("idUsuario") int idUsuario, @Path("idCiudad") int idCiudad);

        @FormUrlEncoded
        @POST("boletines_usuarios")
        Call<List<DatosNotificacion>> obtenerMensajesCliente(@Field("idUsuario") int idUsuario,
                                                             @Field("desde") int desde,
                                                             @Field("cuantos") int hasta);

        @GET("mensajes-clientes/{idUsuario}")
        Call<DatosNotificacion> obtenerNumeroNotificaciones(@Path("idUsuario") int idUsuario);

        @FormUrlEncoded
        @PUT("boletines_usuarios")
        Call<ResponseApi> marcarLeido(@Field("idUsuario") int idUsuario, @Field("idBoletin") int idBoletin);

        @GET("/observaciones/obtener/{idUsuario}")
        Call<RespuestaComentarios> getObservaciones(@Path("idUsuario") int idUsuario);
    }

    public interface Trafico {
        @GET("informacion/obtener-trafico/{lt}/{lg}")
        Call<DatosTrafico> obtenerTrafico(
                @Path("lt") double lt,
                @Path("lg") double lg);
    }


    public interface OnValoracionConductor {
        @GET("ktaxi-driver/valoracion/usuario/promedio/" + "{" + VariablesGlobales.ID_USUARIO + "}")
        Call<ValoracionConductor> obtenerValoracionConductor(@Path(VariablesGlobales.ID_USUARIO) int idUsuario);
    }

    public interface OnCerrarSesion {
        @FormUrlEncoded
        @POST("ktaxi-driver/cerrar-session-driver")
        Call<DatosLogin.CerrarSesion> cerrarSesion(@Field(VariablesGlobales.ID_USUARIO) int idUsuario,
                                                   @Field(VariablesGlobales.ID_APLICATIVO) int idAplicativo);
    }

    public interface OnCambioClave {
        @FormUrlEncoded
        @POST("ktaxi-driver/cambiar-clave-driver/")
        Call<DatosLogin.CambioClave> cambioClave(@Field(VariablesGlobales.ID_USUARIO) int idUsuario,
                                                 @Field("claveAnterior") String claveAnterior,
                                                 @Field("claveNueva") String claveNueva,
                                                 @Field(VariablesGlobales.ID_APLICATIVO) int idAplicativo);
    }

    public interface OnConfiguracionServer {
        //http://www.gestion.kradac.com:8080/
        @FormUrlEncoded
        @POST("gestionkradacrest/webresources/com.kradac.gestionkradac.rest.entities.ktaxiconfigs/ktaxi")
        Call<ConfiguracionDesdeServidor> configuracionServidor(@Field("esProduccion") int numero);

        //                      https://testktaxi.kradac.com/dns/cnf/1/1/1/2.5.8
        @GET("dns/cnf/{idAplicativo}/{idPlataforma}/{app}/{version}")
        Call<ConfiguracionServidor> getConfiguracionesServer(
                @Path("idAplicativo") int idAplicativo,
                @Path("idPlataforma") int idPlataforma,
                @Path("app") int app,
                @Path("version") String version);

    }

    public interface OnDineroElectronico {
        @FormUrlEncoded
        @POST("DineroServicios/webresources/com.kradac.dinero/cobro")
        Call<DineroElectronico> dineroElectronico(@Field("monto") double monto,
                                                  @Field("cedula") String cedula,
                                                  @Field("idCarrera") int idSolicitud,
                                                  @Field("idTaxista") int idTaxista,
                                                  @Field("numero") String numero,
                                                  @Field(VariablesGlobales.ID_APLICATIVO) int idAplicativo);


    }


    public interface OnBoletin {
        @GET("boletines_usuarios/leer-boletin/{idUsuario}/{idCiudad}")
        Call<DatosNotificacion> obtenerBoletin(
                @Path("idUsuario") int idUsuario,
                @Path("idCiudad") int idCiudad);

        @FormUrlEncoded
        @PUT("boletines_usuarios/responder")
        Call<ResponseBody> responder_boletin(
                @Field("idUsuario") int idUsuario,
                @Field("idBoletin") int idBoletin,
                @Field("idBoletinPregunta") int idBoletinPregunta,
                @Field("respuesta") String respuesta);
    }

    public interface OnEnviarMetaData {
        @FormUrlEncoded
        @POST("ktaxi-driver/meta/")
        Call<ResponseBody> enviarMetaData(@Field("idPlataformaKtaxi") int idPlataformaKtaxi,
                                          @Field("versionKtaxi") String versionKtaxi,
                                          @Field("versionSo") String versionSo,
                                          @Field("marca") String marca,
                                          @Field("modelo") String modelo,
                                          @Field("idUsuario") int idUsuario);

    }


    public interface OnEnviarSugerencia {
        @FormUrlEncoded
        @POST("feedback/enviar-sugerencia/")
        Call<Sugerencia> enviarSugerencia(@Field("idUsuario") int idUsuario,
                                          @Field(VariablesGlobales.ID_APLICATIVO) int idAplicativo,
                                          @Field("sugerencia") String sugerencia);

    }

    public interface OnRecuperarContrasenia {
        @FormUrlEncoded
        @POST("ktaxi-driver/recuperar-contrasenia-driver/")
        Call<MensajeGenerico> recuperarContrasenia(@Field("usuario") String usuario,
                                                   @Field(VariablesGlobales.ID_APLICATIVO) int idAplicativo);
    }

    public interface OnEnviarContactenos {
        @FormUrlEncoded
        @POST("feedback/enviar-contactenos/")
        Call<MensajeGenerico> enviarContactenos(@Field("tipo") int tipo,
                                                @Field("idClienteUsuario") int idClienteUsuario,
                                                @Field("persona") String persona,
                                                @Field("correo") String correo,
                                                @Field("telefono") String telefono,
                                                @Field("motivo") String motivo,
                                                @Field("mensaje") String mensaje,
                                                @Field(VariablesGlobales.ID_APLICATIVO) int idAplicativo,
                                                @Field("idPlataformaKtaxi") int idPlataformaKtaxi);
    }


    public interface OnValidarImeiConductor {
        @FormUrlEncoded
        @POST("imei-conductor/registrar")
        Call<ResponseBody> validarImei(@Field("idUsuario") int idUsuario,
                                       @Field("idDispositivo") String idDispositivo,
                                       @Field("marca") String marca,
                                       @Field("modelo") String modelo,
                                       @Field("versionSo") String numero,
                                       @Field("versionAplicativo") String versionAplicativo,
                                       @Field("ltGps") double latitud,
                                       @Field("lgGps") double longitud);
    }

    public interface OnCoordenadas {
        @GET("route/v1/driving/{longitudIncial},{latitudInicial};{longitudFinal},{latitudFinal}?overview=false&steps=true")
        Call<ResponseBody> getDireccion(
                @Path("longitudIncial") double longitudIncial,
                @Path("latitudInicial") double latitudInicial,
                @Path("longitudFinal") double longitudFinal,
                @Path("latitudFinal") double latitudFinal);
    }

    public interface OnHistorial {
        @GET("ktaxi-driver/historial/cuantas/{idUsuario}")
        Call<ResponseBody> historialCuantos(
                @Path("idUsuario") int idUsuario);

        @GET("/ktaxi-driver/historial/cuantas-pedidos/{idUsuario}")
        Call<ResponseBody> historialCuantosCompras(
                @Path("idUsuario") int idUsuario);


        @GET("ktaxi-driver/historial/cuantas-ultimas-16-horas/{idUsuario}")
        Call<Historial.HistorialUltimas> historialUltimas(
                @Path("idUsuario") int idUsuario);

        @FormUrlEncoded
        @POST("ktaxi-driver/historial")
        Call<ArrayList<Historial>> historialObtener(@Field("idUsuario") int idUsuario,
                                                    @Field("desde") int desde,
                                                    @Field("cuantos") int cuantos);


    }


    public interface ComponentesPublicitarios {

        @GET("componente/obtener-componentes/{idAplicativo}/{idCiudad}")
        Call<ComponentePublicitario> obtenerComponentesPublicitarioCiudad(
                @Path("idAplicativo") int idAplicativo,
                @Path("idCiudad") int idCiudad
        );

        @GET("componente/obtener-informacion-componente/{idAplicativo}/{idCiudad}/{idCliente}/{tipo}/{desde}/{cuantos}")
        Call<RespuestaComponente> obtenerComponentes(
                @Path("idAplicativo") int idAplicativo,
                @Path("idCiudad") int idCiudad,
                @Path("idCliente") int idCliente,
                @Path("tipo") int tipo,
                @Path("desde") int desde,
                @Path("cuantos") int cuantos
        );


        @FormUrlEncoded
        @POST("componente/marcar-componente-leido")
        Call<ResponseApiComponente> marcarComponenteComoVisto(
                @Field("idCliente") int idCliente,
                @Field("tipo") int tipo,
                @Field("tipoLectura") int tipoLectura,
                @Field("listComponentes") String listComponentes
        );
    }

    public interface ComponentesContables {
        @FormUrlEncoded
        @POST("/cartera-conductor/obtenerSaldosCuentas")
        Call<Cuenta> historialObtener(@Field("idVehiculo") int idVehiculo,
                                      @Field("idUsuario") int idUsuario,
                                      @Field("anio") int anio,
                                      @Field("mes") int mes);

        //Consultar Saldo ktaxi  disponible
        @FormUrlEncoded
        @POST("/u/saldo/consultar/")
        Call<SaldoKtaxi.ItemSaldoKtaxi> consultarSaldo(@Field("idVehiculo") int idVehiculo,
                                                       @Field("idUsuario") int idUsuario,
                                                       @Field("idCiudad") int idCiudad);

        //Consultar Saldo Tarjeta Credito
        @FormUrlEncoded
        @POST("/u/credito/consultar/")
        Call<ConsultarSaldoTarjetaCredito> credito_consultarSaldo(@Field("idUsuario") int idUsuario,
                                                                  @Field("token") String token,
                                                                  @Field("key") String key,
                                                                  @Field("timeStanD") String timeStanD,
                                                                  @Field("idVehiculo") int idVehiculo,
                                                                  @Field("idCiudad") int idCiudad);


        //Consultar Saldo ktaxi  disponible
        @FormUrlEncoded
        @POST("/u/saldo/consultar-categorias/")
        Call<CompraServiciosCategorias> consultar_categorias(@Field("idVehiculo") int idVehiculo,
                                                             @Field("idUsuario") int idUsuario,
                                                             @Field("idCiudad") int idCiudad);

        //Consultar Saldo ktaxi  disponible
        @FormUrlEncoded
        @POST("/u/saldo/consultar-servicios/")
        Call<SaldoKtaxi.CompraServicios> consultar_servicios(@Field("idCategoria") int idCategoria,
                                                             @Field("idUsuario") int idUsuario,
                                                             @Field("idVehiculo") int idVehiculo,
                                                             @Field("idCiudad") int idCiudad);

        @FormUrlEncoded
        @POST("/u/saldo/consultar-transacciones/")
        Call<TransaccionesSaldoKtaxi> consultar_transacciones(@Field("anio") int anio,
                                                              @Field("mes") int mes,
                                                              @Field("idVehiculo") int idVehiculo,
                                                              @Field("idUsuario") int idUsuario,
                                                              @Field("idCiudad") int idCiudad);


        @FormUrlEncoded
        @POST("/u/credito/consultar-movimientos/")
        Call<ConsultarMovimientos> consultar_movimientos(@Field("idUsuario") int idUsuario,
                                                         @Field("token") String token,
                                                         @Field("key") String key,
                                                         @Field("timeStanD") String timeStanD,
                                                         @Field("idVehiculo") int idVehiculo,
                                                         @Field("idCiudad") int idCiudad,
                                                         @Field("anio") int anio,
                                                         @Field("mes") int mes);

    }


    //obtener historial de solicitudes
    public interface HistoialSolicitudesCliente {


        @GET("/ktaxi-driver/historial/anio-mes/{idUsuario}/{anio}/{mes}/{tipo}")
        Call<ResponseBody> cuantosPorAnioMes(
                @Path("idUsuario") int idUsuario,
                @Path("anio") int anio,
                @Path("mes") int mes,
                @Path("tipo") int tipo
        );

        @FormUrlEncoded
        @POST("ktaxi-driver/historial/anio-mes")
        Call<ArrayList<ItemHistorialSolicitud>> obtenerPorAnioMes(
                @Field("idUsuario") int idUsuario,
                @Field("anio") int anio,
                @Field("mes") int mes,
                @Field("tipo") int tipo,
                @Field("desde") int desde,
                @Field("cuantos") int hasta
        );


        @FormUrlEncoded
        @POST("/ktaxi-driver/historial/anio-mes-pedidos")
        Call<ArrayList<ItemHistorialCompras>> obtenerPorAnioMesCompras(
                @Field("idUsuario") int idUsuario,
                @Field("anio") int anio,
                @Field("mes") int mes,
                @Field("tipo") int tipo,
                @Field("desde") int desde,
                @Field("cuantos") int hastaMa
        );


        @GET("/ktaxi-driver/historial/anio-mes-pedidos/{idUsuario}/{anio}/{mes}/{tipo}")
        Call<ResponseBody> obtenerCuantosHistorialCompras(
                @Path("idUsuario") int idUsuario,
                @Path("anio") int anio,
                @Path("mes") int mes,
                @Path("tipo") int tipo);

    }

    public interface DatosConductor {
        @GET("historial/imagen/{idUsuario}/{idVehiculo}/{tipo}/{idAplicativo}")
        Call<ResponseDatosConductor> obtenerDatosConductor(
                @Path("idUsuario") int idUsuario,
                @Path("idVehiculo") int idVehiculo,
                @Path("tipo") int tipo,
                @Path("idAplicativo") int idAplicativo);
    }

    public interface ValorarSolicitud {
        @FormUrlEncoded
        @PUT("ktaxi-driver/historial/calificar/")
        Call<ResponseValorar> valorarSolicitud(
                @Field("idSolicitud") int idSolicitud,
                @Field("valoracion") int valoracion,
                @Field("observacion") String observacion
        );

        @FormUrlEncoded
        @PUT("ktaxi-driver/historial/calificar-pedidos/")
        Call<ResponseValorar> valorarCompra(
                @Field("idPedido") int idSolicitud,
                @Field("observacion") String observacion,
                @Field("valoracion") int valoracion);
    }

    public interface ReportarPirata {
        @FormUrlEncoded
        @POST("reporte-piratas/reportar")
        Call<ResponseBody> reportarPirata(@Field("idUsuario") int idUsuario,
                                          @Field("idVehiculo") int idVehiculo,
                                          @Field("placa") String placa,
                                          @Field("color") String color,
                                          @Field("denuncia") String denuncia,
                                          @Field("latitud") double latitud,
                                          @Field("longitud") double longitud);

        @Multipart
        @POST("reporte-piratas/alamcenar-imagen")
        Call<ResponseBody> postImage(@Part MultipartBody.Part image, @Part("imagen") RequestBody imagen);

    }

    public interface ConfiguracionIncial {
        @GET("/")
        Call<ResponseBody> getConfiguracionIncial();

        @FormUrlEncoded
        @POST("/respaldo/")
        Call<ResponseBody> getHelp(@Field("tipo") String tipo,
                                   @Field("version") String version,
                                   @Field("plataforma") String plataforma);
        //Hay que cambiar a post agregando un nuevo parametro por que se necesita esta configuración por aplicativo.
        @FormUrlEncoded
        @PUT("/API/taximetro/consultar-tarifas/")
        Call<DatosTaximetro> getConfiguracionTaximetro(@Field("idCiudad") int idCiudad,
                                                       @Field("idAplicativo") int idAplicativo);

        @FormUrlEncoded
        @POST("/API/taximetro/consultar-tarifas/")
        Call<DatosTaximetroServicioActivo> getConfiguracionTaximetroServicoActivo(@Field("idCiudad") int idCiudad,
                                                                                  @Field("idAplicativo") int idAplicativo,
                                                                                  @Field("idServicioActivo") int idServicioActivo);
    }

    public interface OnTaximetroDatos {
        @FormUrlEncoded
        @POST("/API/taximetro/iniciar-taximetro/")
        Call<ResponseBody> iniciarTaximetro(
                @Field("tipo") int tipo,
                @Field("idSolicitud") int idSolicitud,
                @Field("latitud") double latitud,
                @Field("longitud") double longitud,
                @Field("idUsuario") int idUsuario,
                @Field("idVehiculo") int idVehiculo);

        @FormUrlEncoded
        @PUT("/API/taximetro/finalizar/")
        Call<ResponseBody> finalizarTaximetro(
                @Field("idTaximetro") int idTaximetro,
                @Field("idSolicitud") int idSolicitud,
                @Field("latitudFin") double latitudFin,
                @Field("longitudFin") double longitudFin,
                @Field("costoTaximetro") double costoTaximetro,
                @Field("costoCobro") double costroCobro,
                @Field("horaInicio") String horaInicio,
                @Field("horaFin") String horaFin,
                @Field("tarifaArranque") String tarifaArranque,
                @Field("tiempoTotal") String tiempoTotal,
                @Field("tiempoEspera") String tiempoEspera,
                @Field("valorTiempo") String valorTiempo,
                @Field("distanciaRecorrida") String distanciaRecorrida,
                @Field("valorDistancia") String valorDistancia
        );
    }

    public interface OnPagosConductor {
        @FormUrlEncoded
        @POST("/c/pagos/listar-deudas/")
        Call<PagosConductor> listar_deudas(
                @Field("idAplicativo") int idAplicativo,
                @Field("idCiudad") int idCiudad,
                @Field("idUsuario") int idUsuario);


        @FormUrlEncoded
        @POST("/c/pagos/listar-entidades/")
        Call<EntidadFinanciera
                > listar_entidades(
                @Field("idAplicativo") int idAplicativo,
                @Field("idCiudad") int idCiudad,
                @Field("idUsuario") int idUsuario);

        @FormUrlEncoded
        @POST("/c/pagos/registrar-pago/")
        Call<ResponseBody> registrar_pago(
                @Field("idAplicativo") int idAplicativo,
                @Field("idCiudad") int idCiudad,
                @Field("numero") String numero,
                @Field("idUsuario") int idUsuario,
                @Field("idEntidad") int idEntidad,
                @Field("descuento") double descuento,
                @Field("impuestos") double impuesto,
                @Field("monto") double monto,
                @Field("lIdDeuda") String lIdDeuda,
                @Field("lMeses") String lMeses,
                @Field("lMora") String lMora,
                @Field("lMoraImpuestos") String lMoraImpuestos,
                @Field("lDescuento") String lDescuento,
                @Field("lPago") String lPago);


        @FormUrlEncoded
        @POST("/c/pagos/registrar-tikect/")
        Call<ResponseBody> registrar_tikect(
                @Field("idEntidadRegistro") int idEntidadRegistro,
                @Field("numero") String numero,
                @Field("nombre") String idUsuario,
                @Field("monto") double idDeuda,
                @Field("placa") String idEntidad,
                @Field("nombreUsuario") String descuento);

        @Multipart
        @POST("/c/pagos/subir-tikect/")
        Call<ResponseBody> subir_tikect(@Part MultipartBody.Part image, @Part("img") RequestBody imagen);

    }

    public interface OnContactenos {

        @FormUrlEncoded
        @POST("/feedback/enviar-contactenos")
        Call<RespuestaContactenos> enviar_contactenos(
                @Field("tipo") int tipo,  //0 Contactos desde APP cliente. 1 Contactos desde APP conductor. 2 Contactos de sistema desde APP cliente. 3 Contacto cuando conductor califica. 4 Contacto cuando cliente califica. 5 Denuncias desde APP cliente. 6 Contacto en solicitud desde cliente. 7 Contacto en solicitud desde conductor. 8 Contactos de sistema desde APP conductor.
                @Field("idAplicativo") int idAplicativo,
                @Field("idCliente") int idCliente,
                @Field("idUsuario") int idUsuario,
                @Field("idClienteUsuario") int idClienteUsuario,
                @Field("idPlataformaKtaxi") int idPlataformaKtaxi,
                @Field("idSolicitud") int idSolicitud,
                @Field("persona") String persona,
                @Field("correo") String correo,
                @Field("telefono") String telefono,
                @Field("motivo") String motivo,
                @Field("mensaje") String mensaje,
                @Field("latitud") double latitud,
                @Field("longitud") double longitud,
                @Field("versionApp") String versionApp,
                @Field("versionSo") String versionSo,
                @Field("marca") String marca,
                @Field("modelo") String modelo,
                @Field("tipoRed") int tipoRed,
                @Field("usandoGps") int usandoGps,
                @Field("tipoConexion") int tipoConexion,
                @Field("operadora") String operadora,
                @Field("imei") String imei,
                @Field("APP") int APP,
                @Field("paisSeleccionado") String paisSeleccionado,
                @Field("idCiudad") int idCiudad);

        @FormUrlEncoded
        @POST("/feedback/enviar-contactenos")
        Call<RespuestaContactenos> enviar_contactenos_pre_registro(
                @Field("tipo") int tipo,  //0 Contactos desde APP cliente. 1 Contactos desde APP conductor. 2 Contactos de sistema desde APP cliente. 3 Contacto cuando conductor califica. 4 Contacto cuando cliente califica. 5 Denuncias desde APP cliente. 6 Contacto en solicitud desde cliente. 7 Contacto en solicitud desde conductor. 8 Contactos de sistema desde APP conductor.
                @Field("idAplicativo") int idAplicativo,
                @Field("idCliente") int idCliente,
                @Field("idUsuario") int idUsuario,
                @Field("idClienteUsuario") int idClienteUsuario,
                @Field("idPlataformaKtaxi") int idPlataformaKtaxi,
                @Field("idSolicitud") int idSolicitud,
                @Field("persona") String persona,
                @Field("correo") String correo,
                @Field("telefono") String telefono,
                @Field("motivo") String motivo,
                @Field("mensaje") String mensaje,
                @Field("latitud") double latitud,
                @Field("longitud") double longitud,
                @Field("versionApp") String versionApp,
                @Field("versionSo") String versionSo,
                @Field("marca") String marca,
                @Field("modelo") String modelo,
                @Field("tipoRed") int tipoRed,
                @Field("usandoGps") int usandoGps,
                @Field("tipoConexion") int tipoConexion,
                @Field("operadora") String operadora,
                @Field("imei") String imei,
                @Field("APP") int APP,
                @Field("paisSeleccionado") String paisSeleccionado,
                @Field("codigoPaisSeleccionado") String codigoPaisSeleccionado,
                @Field("idCiudad") int idCiudad,
                @Field("pais") String pais,
                @Field("ciudad") String ciudad,
                @Field("idReferidoCodigo") int idReferido);
    }

    public interface OnInteracionSistema {
        @FormUrlEncoded
        @POST("u/pre-registro/consultar-paises/")
        Call<Resultado> obtenerPais(
                @Field("idAplicativo") int idAplicativo);
    }

    public interface OnPaquetesObtener {
        @FormUrlEncoded
        @POST("/c/paquetes/obtener")
        Call<RespuestaSaldoPaquetes> cPaquetesObtener(@Field("idUsuario") int idUsuario,
                                                      @Field("token") String token,
                                                      @Field("key") String key,
                                                      @Field("timeStanD") String timeStanD);
    }

    public interface ICodigoReferido {

        @FormUrlEncoded
        @POST("referidos/consultar-codigo")
        Call<RespuestaConsultarCodigoReferido> consultar(
                @Field("codigo") String codigo,
                @Field("idAplicativo") int idAplicativo
        );

        @FormUrlEncoded
        @POST("referidos/listar-codigos")
        Call<ResponseListarCodigosReferido> listarCodigos(
                @Field("tipo") int tipo,
                @Field("idUsuario") int idCliente,
                @Field("idAplicativo") int idAplicativo
        );

        @FormUrlEncoded
        @POST("referidos/registrar")
        Call<Respuesta> registrar(
                @Field("idCliente") int idCliente,
                @Field("idCodigo") int idCodigo,
                @Field("idAplicativo") int idAplicativo
        );
    }

    public interface IMapbox {
        @GET("tokens/v2")
        Call<ResponseMapboxToken> checkToken(@Query("access_token") String access_token);

        @GET("v4/mapbox.streets-basic/17/41775/72601.png")
        Call<ResponseBody> checkTokenV2(@Query("access_token") String access_token);
    }

    public interface OnRegistrarTokenPush {
        @FormUrlEncoded
        @POST("/x/push/usuario/registrar-token/")
        Call<ResponseBody> registrar_token(
                @Field("idUsuario") int idUsuario,
                @Field("imei") String imei,
                @Field("token") String token,
                @Field("key") String key,
                @Field("timeStanD") String timeStanD,
                @Field("tokenFirebase") String tokenFirebase,
                @Field("latitud") double latitud,
                @Field("longitud") double longitud,
                @Field("versionApp") String versionApp,
                @Field("versionSo") String versionSo,
                @Field("marca") String marca,
                @Field("modeolo") String modeolo,
                @Field("tipoRed") int tipoRed,
                @Field("usandoGps") int usandoGps,
                @Field("tipoConexion") int tieneConexion,
                @Field("operadora") String operadora);

        @FormUrlEncoded
        @POST("/x/push/usuario/registrar-token-anonimo/")
        Call<ResponseBody> registrar_token_anonimo(
                @Field("randon") int randon,
                @Field("imei") String imei,
                @Field("token") String token,
                @Field("key") String key,
                @Field("timeStanD") String timeStanD,
                @Field("tokenFirebase") String tokenFirebase,
                @Field("latitud") double latitud,
                @Field("longitud") double longitud,
                @Field("versionApp") String versionApp,
                @Field("versionSo") String versionSo,
                @Field("marca") String marca,
                @Field("modeolo") String modeolo,
                @Field("tipoRed") int tipoRed,
                @Field("usandoGps") int usandoGps,
                @Field("tipoConexion") int tieneConexion,
                @Field("operadora") String operadora);


        @FormUrlEncoded
        @POST("/x/push/usuario/activar-desactivar/")
        Call<ResponseBody> activar_desactivar_push(
                @Field("idUsuario") int idUsuario,
                @Field("idDispositivo") String idDispositivo,
                @Field("idEmpresa") int idEmpresa,
                @Field("idCiudad") int idCiudad,
                @Field("tipo") int tipo); //1 Para activar y 0 para desactivar
    }


    public interface OnComponentesPublicitarios {
        @FormUrlEncoded
        @POST("/x/banner/consultar-banner-conductor")
        Call<RespuestaPublicidad> getPublicidad(@Field("idAplicativo") int idAplicativo,
                                                @Field("idCiudad") int idCiudad,
                                                @Field("estado") int estado,
                                                @Field("idSolicitud") int idSolicitud,
                                                @Field("idVehiculo") int idVehiculo,
                                                @Field("idUsuario") int idUsuario);

        @GET
        Call<ResponseBody> downloadFileWithDynamicUrlSync(@Url String fileUrl);
    }

    public interface ICard {
        @FormUrlEncoded
        @POST("x/describ/pedir-token")
        Call<ResponseGenerarToken> solicitarToken(
                @Field("idUsuario") long idCliente,
                @Field("imei") String imei,
                @Field("correo") String correo, //base64
                @Field("celular") String celular, //base64
                @Field("nombres") String nombres,//base64
                @Field("apellidos") String apellidos,//base64
                @Field("token") String token,
                @Field("key") String key,
                @Field("timeStanD") String timeStanD,
                @Field("idAplicativo") int idAplicativo
        );

        @FormUrlEncoded
        @POST("x/describ/registrar-car")
        Call<ResponseApiCorto> registrarCard(
                @Field("idCliente") long idCliente,
                @Field("imei") String imei,
                @Field("con") String con,
                @Field("verificationCode") String verificationCode,
                @Field("number") String number,
                @Field("type") int type,
                @Field("expirationMonth") String expirationMonth,
                @Field("expirationYear") String expirationYear,
                @Field("email") String email,
                @Field("phoneNumber") String phoneNumber,
                @Field("token") String token,
                @Field("key") String key,
                @Field("timeStanD") String timeStanD,
                @Field("idAplicativo") int idAplicativo
        );

        @FormUrlEncoded
        @POST("x/describ/listar-car")
        Call<ResponseListarTarjetaCredito> listarCard(
                @Field("idCliente") long idCliente,
                @Field("imei") String imei,
                @Field("con") String con,
                @Field("token") String token,
                @Field("key") String key,
                @Field("timeStanD") String timeStanD,
                @Field("idAplicativo") int idAplicativo
        );


        @FormUrlEncoded
        @HTTP(method = "DELETE", path = "x/describ/eliminar-car", hasBody = true)
        Call<ResponseApiCorto> eliminarTarjeta(
                @Field("idCliente") long idCliente,
                @Field("imei") String imei,
                @Field("con") String con,
                @Field("numberCar") String numberCar,
                @Field("conCar") String conCar,
                @Field("token") String token,
                @Field("key") String key,
                @Field("timeStanD") String timeStanD,
                @Field("idAplicativo") int idAplicativo
        );
    }
}