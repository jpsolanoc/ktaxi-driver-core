package com.kradac.conductor.interfaces;

import android.location.Location;

/**
 * Created by Ing.John Patricio Solano Cabrera on 4/1/2018.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public interface OnComunicacionSaldoKtaxiPagerActity {

    void onLocation(Location location);
}
