package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.MensajeGenerico;

/**
 * Created by John on 29/11/2016.
 */

public interface OnComunicacionRecuperarContrasenia {

    void confirmacionCambioContrasenia(MensajeGenerico generico);

}
