package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.ItemHistorialCompras;
import com.kradac.conductor.modelo.ItemHistorialSolicitud;

import java.util.ArrayList;

/**
 * Created by John on 02/12/2016.
 */

public interface OnComunicacionHistorial {


    void respuestaCuantosPorAnioMes(int numero, int anio, int mes);

    void respuestaObtenerPorAnioMes(ArrayList<ItemHistorialSolicitud> historiales, int anio, int mes);

    void totalHistoricoSolicitud(int total);


    /**
     * Compras
     */

    void respuestaCuantosPorAnioMesCompras(int numero, int anio, int mes);

    void respuestaObtenerPorAnioMesCompras(ArrayList<ItemHistorialCompras> historiales, int anio, int mes);

    void totalHistoricoSolicitudCompras(int total);

}
