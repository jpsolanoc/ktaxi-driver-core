package com.kradac.conductor.interfaces;

import android.location.Location;

import com.kradac.conductor.modelo.SaldoKtaxi;

/**
 * Created by Ing.John Patricio Solano Cabrera on 3/1/2018.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public interface OnComunicacionCompraServiciosMapa extends OnComunicacionCompraServicios {
    void lanzarDialogoCompra();
    void aunNoEnLugar();
}
