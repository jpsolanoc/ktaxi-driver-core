package com.kradac.conductor.interfaces;


import android.location.Location;

import com.kradac.conductor.modelo.ChatMessage;
import com.kradac.conductor.modelo.DatosLogin;
import com.kradac.conductor.modelo.DatosNotificacion;
import com.kradac.conductor.modelo.MensajeDeuda;
import com.kradac.conductor.modelo.PositionTrafico;
import com.kradac.conductor.modelo.RespuestaPosibleSolicitudes;
import com.kradac.conductor.modelo.Solicitud;
import com.kradac.conductor.modelo.ValoracionConductor;
import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public interface OnComunicacionMapa extends OnComunicacionSolicitud{

    void clienteAbordo(String mensaje, Solicitud solicitudSelect);
    void nuevaSolicitud(ArrayList<Solicitud> listaSolicitudes);
    void clienteFinCarrera(String mensaje, String calificacion, Solicitud solicitud);
    void mensajesUsuario(ArrayList<ChatMessage> chatHistory);
    void cambioInterenet(boolean estado);
    void finCarreraGenerico(final String mensaje, boolean presentar);
    void mensajeDesconeccion(String mensaje);
    void mostrarTiempo(String tiempo);
    void cambiarAbordo();
    void nofificarBateriaBaja(int bateria);
    void cobrarDineroElectronico(JSONObject object);
    void cambiarEstadoLibre();
    void cambiarEstadoComprando();
    void cambiarEstadoComprado();
    void cobroAceptado(boolean isEstado, String mensaje, int estado);
    void cambiarEstadoComprandoUsuario();
    void cambiarEstadoCompradoUsuario();
    void borraCarreraAlCancelar();
    void posiblesSolicitudes(RespuestaPosibleSolicitudes respuestaPosibleSolicitudes);
    void boletinKtaxi(String boletin, String asunto);
    void onLocation(Location location);
    void cambiarOcupado();
    void borrarRutaEnDesuso();
    void onResponse(List<PositionTrafico> positionList);
    void calificacionCliente(ValoracionConductor valoracionConductor);
    void errorCalificacionCliente(String valor);
    void cerrarSesion(DatosLogin.CerrarSesion cerrarSesion);
    void errorCerrarSesion(String valor);
    void error(String error);
    void cambioClave(DatosLogin.CambioClave cambioClave);
    void errorCambioClave(String error);
    void presentarDialogoCobro(String texto);
    void reenvioDineroElectronico(String usuario, String pin);
    void boletinNuevo(DatosNotificacion datosNotificacion);
    void respuestaDineroElectronico(String mensaje, int estado);
    void cambioIconoPosibleSolicitude(int numEstado, RespuestaPosibleSolicitudes respuestaPosibleSolicitudes);
    void getDineroSaldoDineroElectronico(String valor, String usuario, String pin);
    void configuracionServidorNew(boolean isEstado);
    void createVideoDialogo(String v);
    void respuestaResponderBoletin(String dato);
    void respuestaCobroTarjetCredito(int tipoError, int estado, String mensaje);
    void solicitudSeleccionadaMarket(Solicitud solicitud);
    void intentarGraficar();
    void solicitudCallCenter(Solicitud solicitud);
    void actualizarRastreoPanico(LatLng latLng);
    void dejarRastrearPanico();
    void encenderPantalla();
    void apagarPantalla();
    void respuestaValidacion(String data);

}
