package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.ChatMessage;
import com.kradac.conductor.modelo.Solicitud;

import java.util.ArrayList;

/**
 * Created by John on 08/04/2016.
 */
public interface OnComunicacionSocketMensajeria {

    void mensajesUsuario(ArrayList<ChatMessage> chatHistory);

    void clienteAceptoTiempo(boolean isAceptado, Solicitud solSelected, String razon);

    void clienteAbordo(String mensaje, Solicitud solicitudSelect);

    void clienteFinCarrera(String mensaje, String calificacion);

    void clienteCancelo();
}
