package com.kradac.conductor.interfaces;

import android.location.Location;

import com.kradac.conductor.modelo.SaldoKtaxi;

/**
 * Created by Ing.John Patricio Solano Cabrera on 3/1/2018.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public interface OnComunicacionCompraServicios {
    void lanzarDialogoCompra(int posicion);
    void aunNoEnLugar(int posicion);
    void onLocation(Location location);
    void onRespuestaCompra(String pin,long cronometro);
    void onRespuestaCompraServicio(String respuesta);
    void respuestaServidorServicios(SaldoKtaxi.CompraServicios compraServicios);
    void repsuestaListaSaldoKtaxi(SaldoKtaxi.ItemSaldoKtaxi itemSaldoKtaxi);
}
