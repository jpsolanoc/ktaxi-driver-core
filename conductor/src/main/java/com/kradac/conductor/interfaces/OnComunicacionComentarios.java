package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.RespuestaComentarios;

/**
 * Created by Ing.John Patricio Solano Cabrera on 12/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public interface OnComunicacionComentarios {

    void RespuestaObtenerComentarios(RespuestaComentarios respuestaComentarios);

}
