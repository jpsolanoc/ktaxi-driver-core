package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.ChatMessage;
import com.kradac.conductor.service.ServicioSockets;

import java.util.ArrayList;

/**
 * Created by John on 07/11/2016.
 */

public interface OnHandleEnviarAudio {

    void onGrabarEnviarAudio(String nameAudio,int fragment);
    ServicioSockets onServicioSocket();
    ArrayList<ChatMessage> getChatCallCenter();
    ArrayList<ChatMessage> getChatBroadCastEmpresa();
    ArrayList<ChatMessage> getChatBroadCast();
}
