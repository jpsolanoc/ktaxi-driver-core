package com.kradac.conductor.interfaces;

/**
 * Created by John on 09/01/2017.
 */

public interface OnComuniacionTaximetro {

    void notificarRastreo(int editarCostoC, int tipo, String tiempoEspera, String valorTiempo, String velocidadtxt,
                          String distanciaRec, String valorPrincipal, String valDistancia,
                          String cronometro, String horaInicioTxt, String tarArr, String totalCarrea);

}
