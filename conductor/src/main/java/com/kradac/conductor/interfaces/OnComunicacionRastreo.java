package com.kradac.conductor.interfaces;

import android.location.Location;

/**
 * Created by John on 07/10/2016.
 */

public interface OnComunicacionRastreo {
    void rastreolocation(Location l);
    void alertaRastreo();
}
