package com.kradac.conductor.interfaces;

/**
 * Created by Ing.John Patricio Solano Cabrera on 29/6/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public interface OnComunicacionServicio {

    void respuestaPin(boolean isRespuesta,String ipRespuesta);
    void configuracionServidorNew(boolean isEstado);
    void respuestaHelp(String json);

}
