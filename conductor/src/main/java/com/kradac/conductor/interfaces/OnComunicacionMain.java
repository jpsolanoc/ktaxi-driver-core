package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.ConfiguracionDesdeServidor;
import com.kradac.conductor.modelo.ConfiguracionServidor;

/**
 * Created by John on 29/11/2016.
 */

public interface OnComunicacionMain {
    void configuracionServidorNew(boolean isRespuesta);
    void respuestaPin(boolean isRespuesta,String ipRespuesta);
    void respuestaHelp(String json);

}
