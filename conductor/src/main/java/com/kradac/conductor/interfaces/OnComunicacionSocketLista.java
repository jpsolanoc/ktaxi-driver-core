package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.Solicitud;

import java.util.ArrayList;

/**
 * Created by John on 06/04/2016.
 */
public interface OnComunicacionSocketLista extends OnComunicacionSolicitud{

    void nuevasolicitud(ArrayList<Solicitud> listaSolicitudes, boolean isPantalla);

}
