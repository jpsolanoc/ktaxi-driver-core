package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.TransaccionesSaldoKtaxi;

/**
 * Created by Ing.John Patricio Solano Cabrera on 8/1/2018.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public interface OnComunicacionTransaccionesSaldoKtaxi {
    void respuestaServidorTransacciones(TransaccionesSaldoKtaxi transaccionesSaldoKtaxi);
}
