package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.RespuestaContactenos;

/**
 * Created by John on 30/11/2016.
 */

public interface OnComunicacionContactenos {

    void respuestaServidor(RespuestaContactenos generico);

}
