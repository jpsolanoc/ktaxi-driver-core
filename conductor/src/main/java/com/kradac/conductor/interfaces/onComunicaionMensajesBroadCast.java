package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.ChatMessage;

import java.util.ArrayList;

/**
 * Created by John on 07/11/2016.
 */

public interface onComunicaionMensajesBroadCast {

    void mensajesCallCenter(ArrayList<ChatMessage> chatHistory);
    void mensajesBroadCastEmpresa(ArrayList<ChatMessage> chatHistory);
    void mensajesBroadCast(ArrayList<ChatMessage> chatHistory);

}
