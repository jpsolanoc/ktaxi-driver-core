package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.DatosNotificacion;

import java.util.List;

/**
 * Created by fabricio on 29/04/16.
 */
public class NotificacionesContract {

    public interface View {
        void showProgress();

        void showRequestError(String error);

        void mensajesClienteSuccess();

        void repsuestaResponderMensaje(String dato);

        void sinMensajesNuevos();

        void cargarMensajesNuevos();

        void mensajeDescargados(List<DatosNotificacion> listaMesajes);

    }

    public interface UserActionListener {
        void consularMensajesCliente(int idCliente, int desde, int cuantos);

        void obtenerNumeroNotiticaciones(int idCliente);

        void marcarLeido(int idCliente, int idMensaje);

        void responderBoletin(int idUsuario,
                              int idBoletin,
                              int idBoletinPregunta,
                              String respuesta);
    }

}