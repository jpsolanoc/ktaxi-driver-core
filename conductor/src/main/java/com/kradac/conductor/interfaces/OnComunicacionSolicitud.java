package com.kradac.conductor.interfaces;

import com.google.android.gms.common.api.Status;
import com.kradac.conductor.modelo.MensajeDeuda;
import com.kradac.conductor.modelo.Solicitud;

public interface OnComunicacionSolicitud {

    void tiempoEnviado(int tiempo, Solicitud solicitudSeleccionada);
    void clienteAceptoTiempo(boolean isAceptado, Solicitud solSelected, String razon);
    void mensajeAlertaCancelar(String mensaje);
    void solicitudAtendida(String mensaje);
    void alertasDialogos(int idSolicitud);
    void cambiarEstado(boolean conectado);
    void configuracionServidorHelp(String mensaje);
    void notificarRastreo(int editarCostoC,int tipo,String tiempoEspera, String valorTiempo, String velocidadtxt,
                          String distanciaRec, String valorPrincipal, String valDistancia,
                          String cronometro, String horaInicioTxt, String tarArr, String totalCarrea);
    void otraApp(String mensaje, String paquete, boolean isDesinstalar, boolean isCambioOcupado);
    void envioCobro(String dato, int estado);
    void solicitudCallCenter(Solicitud solicitud);
    void corazonMalvadoCambiarOcupado();
    void corazonMalvadoDialogo(boolean isBooleanFinish);
    void corazonMalvadoDialogoError(boolean isLanzarError);
    void clienteCanceloSolicitud(Solicitud solicitudSeleccionada,String razonCancelacion,boolean isCalificar);
    void clienteCanceloPedido(Solicitud solicitudSeleccionada,String razonCancelacion,boolean isCalificar);
    void solicitudAtendidaACK(MensajeDeuda mensajeDeuda);
    void probarGPSInicio();
}
