package com.kradac.conductor.interfaces;

import com.kradac.conductor.modelo.Solicitud;

/**
 * Created by Ing.John Patricio Solano Cabrera on 6/3/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public interface OnComunicacionListaAdapter {

    void masTiempo(Solicitud solicitud);

    void enviarTiempo(int tiempo,Solicitud solicitud);
}
