package com.kradac.conductor.interfaces;

public interface OnComunicacionAgregarTarjeta {

    void respuestaCobroTarjetCredito(final int tipoError, final int estado, final String mensaje);
    void envioCobro(String dato, int estado);


}
