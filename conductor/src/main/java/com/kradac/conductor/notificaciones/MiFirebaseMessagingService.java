package com.kradac.conductor.notificaciones;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class MiFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MiFirebaseMessagingService.class.getSimpleName();
    private NotificationUtils notificationUtils;

    /**
     * Sirve para gestionar cuando el aplicativo esta en primer plano.
     * @param remoteMessage
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        if (remoteMessage == null)
//            return;
//        if (remoteMessage.getNotification() != null) {
//            Log.e(TAG, "onMessageReceived: " + remoteMessage.getData().get("s"));
//            new SolicitudTemporal().guardarSolicitud(this, remoteMessage.getData().get("s"), true);
//            handleNotification(remoteMessage.getNotification().getBody());
//        }
    }

    private void handleNotification(String message) {
//        new NotificationUtils(this).showNotificationMessage("Nueva Solicitud", "Barrio los geraneos", new Intent(this, MapaBaseActivity.class));
    }



//    private void handleDataMessage(JSONObject json) {
//        try {
//            JSONObject data = json.getJSONObject("data");
//            Log.e("mensaje2", data.toString());
//            boolean isBackground = false;
//            String titulo = data.getString("title");
//            String mensaje = data.getString("body");
//            String imagen = data.getString("img");
//            int tipo = data.getInt("tipo");
//
//            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
//                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
//                pushNotification.putExtra("body", mensaje);
//                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//                notificationUtils.playNotificationSound();
//            } else {
//                Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
//                resultIntent.putExtra("body", mensaje);
//
//                if (TextUtils.isEmpty(imagen)) {
//                    showNotificationMessage(getApplicationContext(), titulo, mensaje, resultIntent);
//                } else {
//                    showNotificationMessageWithBigImage(getApplicationContext(), titulo, mensaje, resultIntent, imagen);
//                }
//            }
//
//
//        } catch (JSONException e) {
//            Log.e(TAG, "Json Exception: " + e.getMessage());
//        } catch (Exception e) {
//            Log.e(TAG, "Exception: " + e.getMessage());
//        }
//    }


    /**
     * Showing notification with text only
     */
//    private void showNotificationMessage(Context context, String titulo, String mensaje, Intent intent) {
//        notificationUtils = new NotificationUtils(context);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        notificationUtils.showNotificationMessage(titulo, mensaje, intent);
//    }

    /**
     * Showing notification with text and image
     */
//    private void showNotificationMessageWithBigImage(Context context, String title, String message, Intent intent, String imageUrl) {
//        notificationUtils = new NotificationUtils(context);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        notificationUtils.showNotificationMessage(title, message, intent, imageUrl);
//    }

}
