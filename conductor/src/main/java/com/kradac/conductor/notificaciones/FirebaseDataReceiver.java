package com.kradac.conductor.notificaciones;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.kradac.conductor.R;
import com.kradac.conductor.vista.MapaBaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class FirebaseDataReceiver extends WakefulBroadcastReceiver {

    private NotificationUtils notificationUtils;

    private static final String TAG = FirebaseDataReceiver.class.getName();

    public void onReceive(Context context, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        Intent notificationIntent = new Intent(context, MapaBaseActivity.class);
        Log.e(TAG, "onReceive: " + NotificationUtils.isAppIsInBackground(context));
        if (NotificationUtils.isAppIsInBackground(context)) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String datosSolicitud = bundle.getString("s");
                if (datosSolicitud != null) {
                    new SolicitudTemporal().guardarSolicitud(context, datosSolicitud, true);
                    String barrioCliente = "";
                    try {
                        JSONObject jsonObject = new JSONObject(datosSolicitud);
                        JSONObject object = jsonObject.getJSONObject("datosSolicitud");
                        barrioCliente = object.getString("barrioCliente");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    notificationUtils.showNotificationMessage(context.getString(R.string.app_name),
                            "Barrio: " + barrioCliente, notificationIntent);
                }
            }
        }
    }
}
