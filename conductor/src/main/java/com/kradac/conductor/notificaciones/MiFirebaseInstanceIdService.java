package com.kradac.conductor.notificaciones;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.presentador.RegistrarTokenPush;


public class MiFirebaseInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = MiFirebaseInstanceIdService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "onTokenRefresh: " + refreshedToken);
        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
        SharedPreferences spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        if (spLogin.getBoolean("logeado", false)) {
            new RegistrarTokenPush(this).registrarToken(spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), token,true);
        } else {
            new RegistrarTokenPush(this).registrarTokenAnonimo(token);
        }

    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.apply();
    }
}
