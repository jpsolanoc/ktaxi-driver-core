package com.kradac.conductor.notificaciones;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SolicitudTemporal {

    public void guardarSolicitud(Context context, String solicitud, boolean isEstado) {
        Log.e("SolicitudTemporal", "guardarSolicitud:  ENTRO A GUARDAR");
        SharedPreferences spDataSolciitud = context.getSharedPreferences("Solicitudtemporal", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = spDataSolciitud.edit();
        if (isEstado) {
            editor.putString("solicitud", solicitud);
        } else {
            editor.putString("solicitud", null);
        }
        editor.apply();
    }

    public String getSolicitud(Context context) {
        SharedPreferences spDataSolciitud = context.getSharedPreferences("Solicitudtemporal", Context.MODE_PRIVATE);
        return spDataSolciitud.getString("solicitud", null);
    }
}
