package com.kradac.conductor.mapagestion;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.util.Log;

import com.kradac.conductor.R;
import com.kradac.conductor.extras.ExtraLog;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.OnComunicacionMapa;
import com.kradac.conductor.modelo.Destino;
import com.kradac.conductor.modelo.RespuestaConfiguracion;
import com.kradac.conductor.modelo.RespuestaPosibleSolicitudes;
import com.kradac.conductor.modelo.Solicitud;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.Polyline;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.services.Constants;
import com.mapbox.services.api.ServicesException;
import com.mapbox.services.api.directions.v5.DirectionsCriteria;
import com.mapbox.services.api.directions.v5.MapboxDirections;
import com.mapbox.services.api.directions.v5.models.DirectionsResponse;
import com.mapbox.services.api.directions.v5.models.DirectionsRoute;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DellKradac on 23/02/2018.
 */

public class MapViewConfigMapBox {
    private static final String TAG = MapViewConfigMapBox.class.getName();
    private Marker markeConductor, markerCliente, markerEncomienda, markerPosiblesSolicitudes, markerPanico, markerDestino;
    private ArrayList<Marker> markeSolicitudes;
    private Context context;
    private OnComunicacionMapa onComunicacionMapa;
    private Polyline lineSolicitud, lineEncomienda, lineDestino;
    private Utilidades utilidades;

    public MapViewConfigMapBox(Context context) {
        this.context = context;
        markeSolicitudes = new ArrayList<>();
        utilidades = new Utilidades();

    }

    public void setValoresIniciales(int idCiudad, MapboxMap mapboxMap) {
        Utilidades.LatLngCiudad lngCiudad = new Utilidades().getDefinirLocalizacionInicial(idCiudad);
        ExtraLog.Log(TAG, "setValoresIniciales: " + lngCiudad.getLatitud() + "LL" + lngCiudad.getLongitud());
        LatLng latLng = new LatLng(lngCiudad.getLatitud(), lngCiudad.getLongitud());
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                .target(latLng)
                .zoom(12)
                .tilt(0)
                .build()), 1500);
    }

    public void addMarketTaxista(final LatLng latLng, String nombreTaxista, final MapboxMap mapBox, float rotate) {
        if (mapBox != null) {
            if (markeConductor == null && latLng != null) {
                Icon icon = IconFactory.getInstance(context).fromResource(R.mipmap.ic_taxy_map_te);
                MarkerOptions mapketOption = new MarkerOptions()
                        .position(latLng)
                        .title("Conductor")
                        .setIcon(icon)
                        .snippet(nombreTaxista);
                markeConductor = mapBox.addMarker(mapketOption);
            } else {
                try {
                    if (null != markeConductor && latLng != null) {
                        markeConductor.setPosition(latLng);
                    }
                } catch (ClassCastException e) {
                    ExtraLog.Log(TAG, "addMarketTaxista: " + e.getMessage());
                }
            }
        }
    }

    public void addMarketSolicitudes(MapboxMap mapBox, ArrayList<Solicitud> solicitudes) {
        try {
            if (mapBox != null && markeSolicitudes != null) {
                for (final Solicitud s : solicitudes) {
                    Icon icon = IconFactory.getInstance(context).fromResource(R.mipmap.ic_solicitar);
                    if (s.isPedido()) {
                        if (s.getT() == 1) {
                            icon = IconFactory.getInstance(context).fromResource(R.mipmap.ic_compras_taxi_osm);
                        } else {
                            icon = IconFactory.getInstance(context).fromResource(R.mipmap.icon_enconmiendas);
                        }
                    }
                    MarkerOptions mapketOption = new MarkerOptions()
                            .position(new LatLng(s.getLatitud(), s.getLongitud()))
                            .setIcon(icon)
                            .title("")
                            .snippet("s".concat(String.valueOf(solicitudes.indexOf(s))));
                    Marker mark = mapBox.addMarker(mapketOption);
                    markeSolicitudes.add(mark);
                }
            }
        } catch (ConcurrentModificationException e) {
            Log.e(TAG, "addMarketSolicitudes: " + e.getMessage());
        }
    }

    public void eliminarMarcadores(MapboxMap mapBox) {
        if (mapBox != null && markeSolicitudes != null && markeSolicitudes.size() > 0) {
            for (Marker marker : markeSolicitudes) {
                mapBox.removeMarker(marker);
            }
            markeSolicitudes.clear();
        }
    }

    /**
     * Agregar el mardacor del cliente en el mapa ademas se encarga del trazado de la ruta dependiendo del tipo de solicitud o encomienda o pedido
     * Este metodo es generico agrega y elimina los marcadores del mapa
     *
     * @param solicitud
     * @param location
     * @param mapboxMap
     * @param isEstado
     */
    public void marketCliente(Solicitud solicitud, Location location, MapboxMap mapboxMap, boolean isEstado, String key) {
        if (isEstado) {
            if (mapboxMap != null) {
                if (location != null) {
                    if (markerCliente == null) {
                        Icon icon;
                        MarkerOptions mapketOption = new MarkerOptions();
                        mapketOption.setPosition(new LatLng(solicitud.getLatitud(), solicitud.getLongitud()));
                        markerCliente = mapboxMap.addMarker(mapketOption);
                        markerCliente.setTitle("Cliente");
                        markerCliente.setSnippet(solicitud.getNombresCliente());
                        if (solicitud.isPedido()) {
                            if (solicitud.getT() == 1) {
                                icon = IconFactory.getInstance(context).fromResource(R.mipmap.icon_pedido_atendiendo);
                                markerCliente.setIcon(icon);
                                //Trazar Ruta cuenado es pedido
                                Position origin = Position.fromCoordinates(location.getLongitude(), location.getLatitude());
                                Position destination = Position.fromCoordinates(solicitud.getLongitud(), solicitud.getLatitud());
                                getRoute(origin, destination, mapboxMap, null, key, 1);
                                ///
                            } else {
                                icon = IconFactory.getInstance(context).fromResource(R.mipmap.ic_puntero_encomienda_fin);
                                markerCliente.setTitle("Entrega encomienda");
                                //Trazar Ruta cuenado es encomienda
                                Position origin = Position.fromCoordinates(location.getLongitude(), location.getLatitude());
                                Position destination = Position.fromCoordinates(solicitud.getLgE(), solicitud.getLtE());
                                getRoute(origin, destination, mapboxMap, null, key, 1);
                                ///
                                if (markerEncomienda == null) {
                                    Icon iconEncomienda = IconFactory.getInstance(context).fromResource(R.mipmap.ic_puntero_encomienda_inicio);
                                    MarkerOptions markerOptions = new MarkerOptions();
                                    markerOptions.setPosition(new LatLng(solicitud.getLtE(), solicitud.getLgE()));
                                    markerEncomienda = new Marker(markerOptions);
                                    markerEncomienda = mapboxMap.addMarker(markerOptions);
                                    markerEncomienda.setTitle("Recoje encomienda");
                                    markerEncomienda.setSnippet(solicitud.getbE());
                                    markerEncomienda.setIcon(iconEncomienda);
                                    Position originEn = Position.fromCoordinates(solicitud.getLgE(), solicitud.getLtE());
                                    Position destinationEn = Position.fromCoordinates(solicitud.getLongitud(), solicitud.getLatitud());
                                    getRoute(originEn, destinationEn, mapboxMap, solicitud, key, 1);
                                }
                            }
                        } else {
                            if (RespuestaConfiguracion.isActivarIconoEstadoGpsSolicitud(context)) {
                                icon = IconFactory.getInstance(context).fromResource(utilidades.iconEstadoGpsEnSolicitudMarket(solicitud.getConP()));
                            } else {
                                icon = IconFactory.getInstance(context).fromResource(R.mipmap.ic_user_map_osm);
                            }
                            markerCliente.setIcon(icon);
                            Position origin = Position.fromCoordinates(location.getLongitude(), location.getLatitude());
                            Position destination = Position.fromCoordinates(solicitud.getLongitud(), solicitud.getLatitud());
                            getRoute(origin, destination, mapboxMap, null, key, 1);
                            if (solicitud.getlD() != null && solicitud.getlD().size() > 0) {
                                marketDestino(solicitud, mapboxMap, true, key);
                            }
                        }
                    }
                } else {
                    if (onComunicacionMapa != null) {
                        onComunicacionMapa.intentarGraficar();
                    }
                }
            }
        } else {
            if (mapboxMap != null && markerCliente != null) {
                markerCliente.remove();
                trazarRutaSolicitud(mapboxMap, null, false);
                markerCliente = null;
                if (markerEncomienda != null) {
                    markerEncomienda.remove();
                    markerEncomienda = null;
                    trazarRutaEncomienda(mapboxMap, null, false);
                }
                marketDestino(null, mapboxMap, false, null);
            }
        }
    }

    public void marketDestino(Solicitud solicitud, MapboxMap mapboxMap, boolean isEstado, String key) {
        if (isEstado) {
            for (Destino destino : solicitud.getlD()) {
                Icon icon = IconFactory.getInstance(context).fromResource(R.mipmap.ic_user_map_osm);
                MarkerOptions mapketOption = new MarkerOptions();
                mapketOption.position(new LatLng(destino.getLtD(), destino.getLgD()));
                markerDestino = mapboxMap.addMarker(mapketOption);
                markerDestino.setTitle("Destino");
                markerDestino.setIcon(icon);
                markerDestino.setSnippet(solicitud.getNombresCliente());
                Position origin = Position.fromCoordinates(solicitud.getLongitud(), solicitud.getLatitud());
                Position destination = Position.fromCoordinates(destino.getLgD(), destino.getLtD());
                getRoute(origin, destination, mapboxMap, null, key, 2);
            }
        } else {
            if (mapboxMap != null && markerDestino != null) {
                markerDestino.remove();
                trazarRutaDestino(mapboxMap, null, false);
                markerDestino = null;
            }
        }
    }

    public synchronized void getRoute(Position origin, Position destination, final MapboxMap mapaOSM, final Solicitud solicitud, String key, int tipoRuta) {
        if (solicitud == null) {
            if (lineSolicitud != null) {
                return;
            }
        } else {
            if (lineEncomienda != null) {
                return;
            }
        }
        MapboxDirections client = null;
        try {
            client = new MapboxDirections.Builder()
                    .setOrigin(origin)
                    .setDestination(destination)
                    .setProfile(DirectionsCriteria.PROFILE_DRIVING)
                    .setSteps(true)
                    .setAccessToken(key)
                    .build();
        } catch (ServicesException e) {
            return;
        }
        client.enqueueCall(new Callback<DirectionsResponse>() {
                               @Override
                               public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                                   if (response.code() == 200) {
                                       if (response.body().getRoutes() != null) {
                                           if (response.body().getRoutes().size() > 0) {
                                               DirectionsRoute currentRoute = response.body().getRoutes().get(0);
                                               switch (tipoRuta) {
                                                   case 1:
                                                       if (solicitud != null) {
                                                           trazarRutaEncomienda(mapaOSM, currentRoute, true);
                                                       } else {
                                                           trazarRutaSolicitud(mapaOSM, currentRoute, true);
                                                       }
                                                       break;
                                                   case 2:
                                                       trazarRutaDestino(mapaOSM, currentRoute, true);
                                                       break;
                                               }
                                           }
                                       }
                                   } else {
                                       ExtraLog.Log(TAG, "onResponse: " + response.code());
                                   }
                               }

                               @Override
                               public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                                   ExtraLog.Log(TAG, "onFailure: " + t.getMessage());
                               }
                           }

        );
    }


    public void trazarRutaDestino(MapboxMap mapBox, DirectionsRoute currentRoute, boolean isEstado) {
        if (isEstado) {
            PolylineOptions polylineOptions = new PolylineOptions();
            if (currentRoute != null && mapBox != null && lineDestino == null) {
                LineString lineString = LineString.fromPolyline(currentRoute.getGeometry(), Constants.PRECISION_6);
                List<Position> coordinates = lineString.getCoordinates();
                for (int i = 0; i < coordinates.size(); i++) {
                    polylineOptions.add(new LatLng(coordinates.get(i).getLatitude(), coordinates.get(i).getLongitude()));
                }
                polylineOptions.color(Color.parseColor("#000000"));
                polylineOptions.width(4);
                lineDestino = mapBox.addPolyline(polylineOptions);
            }
        } else {
            if (mapBox != null && lineDestino != null) {
                lineDestino.remove();
                lineDestino = null;
            }
        }
    }


    public void trazarRutaSolicitud(MapboxMap mapBox, DirectionsRoute currentRoute, boolean isEstado) {
        ExtraLog.Log(TAG, "trazarRutaSolicitud: " + isEstado);
        if (isEstado) {
            PolylineOptions polylineOptions = new PolylineOptions();
            if (currentRoute != null && mapBox != null && lineSolicitud == null) {
                LineString lineString = LineString.fromPolyline(currentRoute.getGeometry(), Constants.PRECISION_6);
                List<Position> coordinates = lineString.getCoordinates();
                for (int i = 0; i < coordinates.size(); i++) {
                    polylineOptions.add(new LatLng(coordinates.get(i).getLatitude(), coordinates.get(i).getLongitude()));
                }
                polylineOptions.color(Color.parseColor("#3bb2d0"));
                polylineOptions.width(4);
                lineSolicitud = mapBox.addPolyline(polylineOptions);
            }
        } else {
            if (mapBox != null && lineSolicitud != null) {
                lineSolicitud.remove();
                lineSolicitud = null;
            }
        }
    }

    public void borrarTrazadoRuta() {
        if (lineSolicitud != null) {
            lineSolicitud.remove();
            lineSolicitud = null;
        }
    }


    public void trazarRutaEncomienda(MapboxMap mapaOSM, DirectionsRoute currentRoute, boolean isEstado) {
        ExtraLog.Log(TAG, "trazarRutaEncomienda: " + isEstado);
        if (isEstado) {
            PolylineOptions polylineOptions = new PolylineOptions();
            if (currentRoute != null && mapaOSM != null && lineEncomienda == null) {
                LineString lineString = LineString.fromPolyline(currentRoute.getGeometry(), Constants.PRECISION_6);
                List<Position> coordinates = lineString.getCoordinates();
                for (int i = 0; i < coordinates.size(); i++) {
                    polylineOptions.add(new LatLng(coordinates.get(i).getLatitude(), coordinates.get(i).getLongitude()));
                }
                polylineOptions.width(4);
                polylineOptions.color(Color.parseColor("#04B404"));
                lineEncomienda = mapaOSM.addPolyline(polylineOptions);
            }
        } else {
            if (mapaOSM != null && lineEncomienda != null) {
                lineEncomienda.remove();
                lineEncomienda = null;
            }
        }
    }


    public void marketPosiblesSolcitudes(RespuestaPosibleSolicitudes rPSolicitudes
            , String mensaje, MapboxMap mapboxMap, int iconRecurso, boolean isEstado) {
        if (isEstado) {
            if (mapboxMap != null) {
                if (markerPosiblesSolicitudes == null) {
                    Icon icon = IconFactory.getInstance(context).fromResource(iconRecurso);
                    MarkerOptions mapketOption = new MarkerOptions()
                            .position(new LatLng(rPSolicitudes.getLt(), rPSolicitudes.getLg()))
                            .setIcon(icon)
                            .title("Posibles solicitudes.")
                            .snippet(mensaje);
                    markerPosiblesSolicitudes = mapboxMap.addMarker(mapketOption);
                } else {
                    Icon icon = IconFactory.getInstance(context).fromResource(iconRecurso);
                    markerPosiblesSolicitudes.setPosition(new LatLng(rPSolicitudes.getLt(), rPSolicitudes.getLg()));
                    markerPosiblesSolicitudes.setSnippet(mensaje);
                    markerPosiblesSolicitudes.setIcon(icon);
                }
            }
        } else {
            if (mapboxMap != null && markerPosiblesSolicitudes != null) {
                markerPosiblesSolicitudes.remove();
                markerPosiblesSolicitudes = null;
            }
        }
    }

    public void marketPanico(LatLng latLng, String mensaje, final MapboxMap mapaOSM, boolean isEstado) {
        if (isEstado) {
            if (mapaOSM != null) {
                if (markerPanico == null) {
                    Icon icon = IconFactory.getInstance(context).fromResource(R.mipmap.ic_launcher_panico);
                    MarkerOptions mapketOption = new MarkerOptions()
                            .position(new LatLng(latLng.getLatitude(), latLng.getLongitude()))
                            .setIcon(icon)
                            .title("Panico.")
                            .snippet(mensaje);
                    markerPanico = mapaOSM.addMarker(mapketOption);
                } else {
                    markerPanico.setPosition(latLng);
                }
            }
        } else {
            if (mapaOSM != null && markerPanico != null) {
                markerPanico.remove();
                markerPanico = null;
            }
        }
    }

}
