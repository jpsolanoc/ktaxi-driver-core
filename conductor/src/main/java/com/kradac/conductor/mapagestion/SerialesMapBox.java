package com.kradac.conductor.mapagestion;

import android.content.Context;
import android.content.SharedPreferences;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.kradac.conductor.extras.VariablesGlobales;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DellKradac on 16/03/2018.
 */

@Table(name = "SerialesMapBox", id = "_id")
public class SerialesMapBox extends Model {

    private static final String TAG = SerialesMapBox.class.getName();
    @Column(name = "keyMapa")
    private String keyMapa;
    @Column(name = "isEstadoKeyMapa")
    private boolean isEstadoKeyMapa;


    public String getKeyMapa() {
        return keyMapa;
    }

    private ArrayList<SerialesMapBox> serialesMapBoxes;

    public SerialesMapBox() {
    }

    public SerialesMapBox(String keyMapa, boolean isEstadoKeyMapa) {
        this.keyMapa = keyMapa;
        this.isEstadoKeyMapa = isEstadoKeyMapa;
    }


    public void cargarDatos() {
        if (this.getAll().isEmpty()) {
            switch (VariablesGlobales.NUM_ID_APLICATIVO) {
                case 2: //Azutaxi
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTAxZWMiLCJhIjoiY2pocnFjNGlvMTk3ajMwbXB2NGVtNGJvZiJ9.DYZDMGLWzbXDP50zDQ_RPw");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTAyZWMiLCJhIjoiY2pocnFmYW1pMTA0ajM2c2EwOWZzYnQxNSJ9.9kDM-wE25I-m1BXbqKqc0A");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTAzZWMiLCJhIjoiY2pocnFndDU3MWJoNDM3cnVwaHZxbjNjaSJ9.DWO3R1y044nFiG_rVY-OPA");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTA0ZWMiLCJhIjoiY2pocnF2bWh1MDRsNzM2bzFwdnIxZTlyZiJ9.JHnGCt3HFp5adMGh_EJrGw");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTA1ZWMiLCJhIjoiY2pocnJveG92MWo2YTM3cHZ3YmJnNG8zcSJ9.-KYl5RYb6_i-b6ez2G9y2Q");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTA2ZWMiLCJhIjoiY2pocnJxdDBwMTltdzMwbXBsZ25pYnIyYSJ9.3ePOu4L68qwIY20OKB01cA");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTA3ZWMiLCJhIjoiY2pocnJ4Yjd1MTBsejM2c2FkNDltYmkxcCJ9.KdJy5W7NwiMZQ0aJJFQcRw");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTA4ZWMiLCJhIjoiY2pocnJ6YWp4MTJreDMwbWp5dWplOTZrcyJ9.xU8CBEiZErDk4bOONDmCjw");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTA5ZWMiLCJhIjoiY2pocnMxc3c3NHNpdzM3bmcyZjY2czRlZiJ9.Nrb7lOTUns3GtPr5S8qG0A");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTEwZWMiLCJhIjoiY2pocnMzZ2h4MTlxbjMwbXAzNzFoaTlxZSJ9.1EF6DhJ-xuvxaEZ6UHilmA");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTExZWMiLCJhIjoiY2pocnRpeXF0NTdqaDMwbXdhZGxiZWp2OSJ9.bLB1z0aSwumMlRgdvzENEQ");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTEyZWMiLCJhIjoiY2podDZwdWU4MGFpbzNwbHNxZjQxMHI4bCJ9.Nxw2qhWcl8242M0OnTX3gA");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTEzZWMiLCJhIjoiY2podDcyMzZxMGFxbDN2c2RkZjVoOHNkNSJ9.ZXnM5Jc_i9KlnKg-m3kzLg");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTE0ZWMiLCJhIjoiY2podDc0ZTV4MGFyYjN4cG1zZ2R6NWxhMCJ9.uGYm0wUxEofdCQEQJ-4jFw");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTE1ZWMiLCJhIjoiY2podDdhMTR1MGFweTNxcG02eDd6N2g2aiJ9.XJVXg0L3strW9aHJtOTy0w");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTE2ZWMiLCJhIjoiY2podDdxajJ0MGF3YTNwbDY1b3U5Zm9zdCJ9.jDsG5pOKC0ZDt2irtrZr1A");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTE3ZWMiLCJhIjoiY2podGFrcnlmMGJwYjNwcnNmcTQwZ3JnaiJ9.uB8fyosT9BPVVRaiq3CGkw");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTE4ZWMiLCJhIjoiY2podW1qcmk4MHB2MDN2cGRmcmo1bzMzZyJ9.Cjjx2QqpyRbYrWcQzI8-0w");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTE5ZWMiLCJhIjoiY2podW1sY3czMHA3ZzN2bjVvY3pkaWM0ZCJ9.zzwGyWMk0YMPgZtaPilaDQ");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTIwZWMiLCJhIjoiY2podW1tdG1iMHEycjNrbDZqdWNlZG5ncyJ9.wqnO25pOotKye_b6uh_C2g");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTIxZWMiLCJhIjoiY2podW1waGZtMHEwMTNxcDBnMHFzN3Y1YSJ9.7Nz5JPibOsezvG7jI4rRkQ");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTIyZWMiLCJhIjoiY2podW1yMXBoMHEzbDN2cnZqNTkyeDZtcyJ9.Tym2XnDn3sCw9illeqbe0Q");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTIzZWMiLCJhIjoiY2podW1zZ3pkMHEzbzN2bDZqcW9mOXgxaCJ9.qkUTaBH_i8uIuLwBut-Tig");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTI0ZWMiLCJhIjoiY2podW10dHk4MHE4bjNxcGQzZWdlZTBvYyJ9.QsFv5E5VlwwXeyMWymxLeA");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTI1ZWMiLCJhIjoiY2podW15NGFoMHE4YjN3cHE1MmNtMDF1NiJ9.aS5nlapU5dtQZIXHFe-56w");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTI3ZWMiLCJhIjoiY2podW40ZG1rMHE2aTNrbjQycThuc3ZpZSJ9.lIvrusSRmu224VnzgWjwsg");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTI4ZWMiLCJhIjoiY2podW41dXliMHFieTN2dGM5YjczenJ0YiJ9.9TsGbEnsSj8UgB_jm6sqag");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTI5ZWMiLCJhIjoiY2podW43b3pwMHE5NDN3bzQ4MXN0OWZ2eiJ9.GwFTKHzvnan8OBzX7ex0jQ");
                    agregarKey("sk.eyJ1IjoiYXp1dGF4aTMwZWMiLCJhIjoiY2podW5hc2JmMGMzMzNycGlmZGlmdmJsZyJ9.fleWiq0wyyevb1xGmlOxFg");
                    break;
                case 5: //Fedotaxi
                    agregarKey("sk.eyJ1IjoiZmVkb3RheGl1bm8iLCJhIjoiY2ptaTcyenA3MDF3YjN3cXNjZzJhNmNvdSJ9.SmI5wj9FzkbXr-ubM0g64w");
                    agregarKey("sk.eyJ1IjoiZmVkb3RheGlkb3MiLCJhIjoiY2ptaTc0cXZnMDF4NDNzbndibWhmdDJrOSJ9.YGmHB083Jy3cqhhZKfXj4g");
                    agregarKey("sk.eyJ1IjoiZmVkb3RheGl0cmVzIiwiYSI6ImNqbWk3NmMwaDAxem4zcm12NjVpMTEyd2wifQ.bBt_-KNbfA6Fdb6mKgvtSA");
                    agregarKey("sk.eyJ1IjoiZmVkb3RheGljdWF0cm8iLCJhIjoiY2ptaTc3dHZuMDF5YTNrbW9reGhscTl6YSJ9.cczWBR99AgT5bVL1X88FAw");
                    agregarKey("sk.eyJ1IjoidGVzdHVubyIsImEiOiJjam80d2NtZXMwNGZuM3ZxbDlnc3Rpb2FqIn0.EayaH596cOMWbG75p9PnuQ");
                    agregarKey("sk.eyJ1IjoiZmVkb3RheGlzZWlzIiwiYSI6ImNqbWk4NW1odzAyOXozcHJwdHU4MWJ0eWgifQ.kpPNdAb83qy01-gmyZl1cw");
                    agregarKey("sk.eyJ1IjoidGVzdGRvcyIsImEiOiJjam80d25nc3cwMTNmM290YzU0djVwOThoIn0.blqsfi0_m-1-UtQK9KvYGQ");
                    agregarKey("sk.eyJ1IjoidGVzdHRyZXNkZSIsImEiOiJjam80d3QyenYwMXY3M2ttcW96ZG54aHFuIn0.Tn4BFApYinwLhvYhNbPmmQ");
                    break;

                case 9: //Mitaxi
                    agregarKey("sk.eyJ1IjoibWl0YXhpdW5vIiwiYSI6ImNqbXFqbW00ajFxczMza21qam91NHUzaHMifQ.BkOBgp0wa6X2vGVCnU0RLg");
                    agregarKey("sk.eyJ1IjoibWl0YXhpZG9zIiwiYSI6ImNqbXFqb3R5eTFyMnUzcXA1OWp1NHc1dDQifQ.9ywSQ-nhET121z9Te9EN3A");
                    agregarKey("sk.eyJ1IjoibWl0YXhpdHJlcyIsImEiOiJjam1xanFxMWsxYmdtM2txY2RqYjllYXRlIn0.3jgQ4nldfESfEP-3wiiP3Q");
                    agregarKey("sk.eyJ1IjoibWl0YXhpY3VhdHJvIiwiYSI6ImNqbXFqd3N2ajFxdWgza3BkYjRxajZyMDgifQ.DVAhFHk9QBM0T2NCC_hnuQ");
                    agregarKey("sk.eyJ1IjoibWl0YXhpY2luY28iLCJhIjoiY2ptcWp5ajFmMXF2NjNybGI4NGViZTB1MiJ9.Tk-pe6WABDyfiaOUiatP5Q");

                    break;
                case 13: //PinosChiclayo
                    agregarKey("sk.eyJ1IjoicGlub3NjaGljbGF5b3VubyIsImEiOiJjam1pOWFvMngwMmx1M2txenoxNWFiZnhoIn0.x8DTybsXtKYxAWIGMd_jMA");
                    agregarKey("sk.eyJ1IjoicGlub3NjaGljbGF5b2RvcyIsImEiOiJjam1pOWQ4bHQwMmxpM2ttbzI4cmpjYWZlIn0.AY0APdGtuBmOnlTCvLWtZA");
                    agregarKey("sk.eyJ1IjoicGlub3NjaGljbGF5b3RyZXMiLCJhIjoiY2ptaTllaTB3MDJscjNxcWx2dmF3dDRzeCJ9.0V3fRMuc93qc53JFCuckIw");
                    agregarKey("sk.eyJ1IjoicGlub3NjaGljbGF5b2N1YXRybyIsImEiOiJjam1pOWZxaWwwMmxvM3Frd280NHVtbW41In0.2FmB-g7oYELgS3RlEKke6A");
                    break;
                case 14: //PinosSeguro
                    agregarKey("sk.eyJ1IjoicGlub3NlZ3Vyb3VubyIsImEiOiJjam1pOWkwbHEwMWx4M2tsZjN6eXhnc3FxIn0.dVQHodMJnCxEWWJbYlMyVw");
                    agregarKey("sk.eyJ1IjoicGlub3NzZWd1cm9kb3MiLCJhIjoiY2ptaTlqZjkxMDJwaDN3bTRwN3liZndmdSJ9.cndACcsGr15mCnV047MsnA");
                    agregarKey("sk.eyJ1IjoicGlub3NzZWd1cm90cmVzIiwiYSI6ImNqbWk5a2psYjAwcTkzcXBsdGs4bzc5Y3cifQ.GEHjjDCbPm9BNcIf4KQ1RA");
                    agregarKey("sk.eyJ1IjoicGlub3NzZWd1cm9jdWF0cm8iLCJhIjoiY2ptaTlsdTQ4MDJwcDNwb2M3em92d2E2aSJ9.x1eLhljsPPvMGIVKToJHgQ");
                    break;
                default: //Ktaxi
                    agregarKey("sk.eyJ1Ijoia3RheGkwMWVjIiwiYSI6ImNqaHFmNmJ6ZDRkem0zY213aTVyempjZTIifQ.WNx0A20VCfURSzIXnqzvDA");//Quitar el uno para salir
                    agregarKey("sk.eyJ1Ijoia3RheGkwMmVjIiwiYSI6ImNqaHFmYjg3OTB3aWUzZG11Y3VuNmFla2cifQ.cmzeAaOaJvEBisCqQeVbCQ");
                    agregarKey("sk.eyJ1Ijoia3RheGkwM2VjIiwiYSI6ImNqaHFoMHludDJkNm4zMHVpbHgxZWFsaXkifQ.1_2jur5-6PkqULUG8_OY6w");
                    agregarKey("sk.eyJ1Ijoia3RheGkwNGVjIiwiYSI6ImNqaHFoNDYwaTAxeTIzOHFqMnk1Ym13MjUifQ.vdfx65PpqINPs5bzWWQXYw");
                    agregarKey("sk.eyJ1Ijoia3RheGkwNWVjIiwiYSI6ImNqaHFoZDNnczB4NWgzZG11cjNlY2tjZjYifQ.RY2Z8dKT9D90-zj2XEtGOw");
                    agregarKey("sk.eyJ1Ijoia3RheGkwNmVjIiwiYSI6ImNqaHFoaDhqazE2ZGwzZHB2Z3JsODhnZGkifQ.3Yd2p1ThuLUNqZwA37QHOg");
                    agregarKey("sk.eyJ1Ijoia3RheGkwN2VjIiwiYSI6ImNqaHFodXNhcjFuZDIzYXJ5NmI4bWZmanoifQ.ctPKfFpD0tvinl_Kxp9wUQ");
                    agregarKey("sk.eyJ1Ijoia3RheGkwOGVjIiwiYSI6ImNqaHFoeXVkMzE0YWIzMHB2ZXc4azY2YWgifQ.kMVaj6xTQY9Ceabme_0x2g");
                    agregarKey("sk.eyJ1Ijoia3RheGkwOWVjIiwiYSI6ImNqaHFpMTlkMTB5ZGIzN21vMXpjeXZjOTEifQ.ZxfC_zrRO4u0xHjzBFw_8w");
                    agregarKey("sk.eyJ1Ijoia3RheGkxMGVjIiwiYSI6ImNqaHFpNXhpdzBsZHgzMHNhOHIxOXBlcjQifQ.hZkHoBBPaCcKisVylFjxlQ");
                    agregarKey("sk.eyJ1Ijoia3RheGkxMWVjIiwiYSI6ImNqaHFpN3BkMDBtZmIzNm1qdGcwZnU3djEifQ.4ThGUkt4qx4lv1zWb0FWOw");
                    agregarKey("sk.eyJ1Ijoia3RheGkxMmVjIiwiYSI6ImNqaHFpYmYwbTRvYnEzMG5naWRsZzVwY3cifQ.B3jVmb30T0Z2aIbvMbUEXw");
                    agregarKey("sk.eyJ1Ijoia3RheGkxM2VjIiwiYSI6ImNqaHFpZ3NxcTBsaW0zOXNhem1pMzYyNXQifQ.BXvneynd1Cf_tWZTA5V2Ng");
                    agregarKey("sk.eyJ1Ijoia3RheGkxNGVjIiwiYSI6ImNqaHFpbHlrMzJldDQzNm8xY3A3enFwaGMifQ.keIeP4izdcvnxU3rtXr5Wg");
                    agregarKey("sk.eyJ1Ijoia3RheGkxNmVjIiwiYSI6ImNqaHFpc2hhcDB2eHkzMG80NXh6cjQyc3EifQ.fmPTWYNTL0XBeFlG9TAptA");
                    agregarKey("sk.eyJ1Ijoia3RheGkxN2VjIiwiYSI6ImNqaHFpdWVoeDAyZ3gzOHFqenVhNWRocHMifQ.4CcQqz2yeFHOfRFvf8vA8g");
                    agregarKey("sk.eyJ1Ijoia3RheGkxOGVjIiwiYSI6ImNqaHFpejNtdjRpNzgzZG13emtpN2RpcWgifQ.4wJM8zHk5Xjc6rAlUFw1hA");
                    agregarKey("sk.eyJ1Ijoia3RheGkxOWVjIiwiYSI6ImNqaHFqMXR6dzAyaXIzYXJtY25yemxrNnkifQ.Q0PIMsD4xIghkTAq_pq1DQ");
                    agregarKey("sk.eyJ1Ijoia3RheGkyMGVjIiwiYSI6ImNqaHFqNmUwdTBsb3kzMHNhb2J3ZXBoOGwifQ.98hnRvJXDbH1VMcbIitp3w");
                    agregarKey("sk.eyJ1Ijoia3RheGkyMWVjIiwiYSI6ImNqaHFqODFydDB5cXUzN21vNDF2eWxic3MifQ.j4TX4Ru24g4Nx0s5WwDecg");
                    agregarKey("sk.eyJ1Ijoia3RheGkyMmVjIiwiYSI6ImNqaHFqZ3dkajRuYWwzZG4zbnNkZ3p3NzkifQ.Gvd4QiGM_J9LnnOkaT8hYA");
                    agregarKey("sk.eyJ1Ijoia3RheGkyM2VjIiwiYSI6ImNqaHFqajc4ZzB2cDkzZG1veXJ0MjdoN2IifQ.2CvJjeaeA-MvbcRYxHc33Q");
                    agregarKey("sk.eyJ1Ijoia3RheGkyNGVjIiwiYSI6ImNqaHFqa3VnNjA5a3ozZGtrbnB4a3U5dzkifQ.R7oEKjqnH-S9to4QOlyShA");
                    agregarKey("sk.eyJ1Ijoia3RheGkyNWVjIiwiYSI6ImNqaHFqbjkxYzB6ZW8zN3F6aXpoaHJpZGYifQ.y8BW8JVawzNLonRtnZoQhA");
                    agregarKey("sk.eyJ1Ijoia3RheGkyNmVjIiwiYSI6ImNqaHFqb3MzeTAzMTEzMHBwZWgyZTdwbXAifQ.F2VZh9DmwGzavq0tFRZffA");
                    agregarKey("sk.eyJ1Ijoia3RheGkyN2VjIiwiYSI6ImNqaHFqc3B2ZDRxb2szNm5nMDcyMjBtNWoifQ.ZyLOTh2lJXOaZoh5VuXiHw");
                    agregarKey("sk.eyJ1Ijoia3RheGkyOGVjIiwiYSI6ImNqaHFqdmRncTRzN2czY203aTU3ZWxjejkifQ.I4zz0dsD0v6G_H9We4EFHw");
                    agregarKey("sk.eyJ1Ijoia3RheGkyOWVjIiwiYSI6ImNqaHFrNm5lcjB4Y2UzMG11cGRzbHFkZGUifQ.fhavSpu_vVaAwyLEgtxrHQ");
                    break;
            }


        }
    }

    public void agregarKey(String key) {
        new SerialesMapBox(key, true).save();
    }

    public List<SerialesMapBox> getAll() {
        try {
            return new Select().from(SerialesMapBox.class).execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void deleteAll() {
        new Delete().from(SerialesMapBox.class).execute();
    }

    public static void delete(String keyMapa) {
        try {
            new Delete().from(SerialesMapBox.class).where("keyMapa= ?", keyMapa).execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    public void guardarUltimoUsado(int id, Context context) {
        SharedPreferences spKeyMapa = context.getSharedPreferences("keyMapBox", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = spKeyMapa.edit();
        editor.putInt("ultimoUsado", id);
        editor.apply();
    }

    public SerialesMapBox getKeyMapaOb(Context context) {
        serialesMapBoxes = (ArrayList<SerialesMapBox>) getAll();
        //Default Ktaxi
        SerialesMapBox key = new SerialesMapBox("sk.eyJ1Ijoia3RheGl0cmVpbnRhIiwiYSI6ImNqbjFycHY5bDIyNWIza3BsbG5leTN6NzUifQ.cnYH11m9zyJI0dMrlpvXtw", true);
        SharedPreferences spKeyMapa = context.getSharedPreferences("keyMapBox", Context.MODE_PRIVATE);
        int idusado = spKeyMapa.getInt("ultimoUsado", 0);
        if (idusado < serialesMapBoxes.size()) {
            key = serialesMapBoxes.get(idusado);
            idusado = idusado + 1;
            guardarUltimoUsado(idusado, context);
        } else {
            idusado = 0;
            switch (VariablesGlobales.NUM_ID_APLICATIVO) {
                case 2: //Azutaxi
                    key = new SerialesMapBox("sk.eyJ1IjoiYXp1dGF4aTI2ZWMiLCJhIjoiY2podW16aWw2MHE5MTNrcDB0bnl3cHFlaSJ9.zXjRUyb9GOtNrmKxtDyavA", true);
                    break;
                case 5: //Fedotaxi
                    key = new SerialesMapBox("sk.eyJ1IjoiZmVkb3RheGljaW5jbyIsImEiOiJjam1pNzk0MnEwMjEwM3dxemcyN240ODhvIn0.X9NiUC4pykPdTEAwE6MtIg", true);
                    break;
                case 9: //Mi taxi
                    key = new SerialesMapBox("sk.eyJ1IjoibWl0YXhpdHJlcyIsImEiOiJjam1xanFxMWsxYmdtM2txY2RqYjllYXRlIn0.3jgQ4nldfESfEP-3wiiP3Q", true);
                    break;
                case 13: //PinosChiclayo
                    key = new SerialesMapBox("sk.eyJ1IjoicGlub3NjaGljbGF5b3RyZXMiLCJhIjoiY2ptaTllaTB3MDJscjNxcWx2dmF3dDRzeCJ9.0V3fRMuc93qc53JFCuckIw", true);
                    break;
                case 14: //PinosSeguro
                    key = new SerialesMapBox("sk.eyJ1IjoicGlub3NzZWd1cm90cmVzIiwiYSI6ImNqbWk5a2psYjAwcTkzcXBsdGs4bzc5Y3cifQ.GEHjjDCbPm9BNcIf4KQ1RA", true);
                    break;
            }
            idusado = idusado + 1;
            guardarUltimoUsado(idusado, context);
        }
        return key;
    }


    @Override
    public String toString() {
        return "SerialesMapBox{" +
                "keyMapa='" + keyMapa + '\'' +
                ", isEstadoKeyMapa=" + isEstadoKeyMapa +
                ", serialesMapBoxes=" + serialesMapBoxes +
                '}';
    }
}
