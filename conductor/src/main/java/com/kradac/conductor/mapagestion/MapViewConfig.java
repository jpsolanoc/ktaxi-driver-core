package com.kradac.conductor.mapagestion;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.kradac.conductor.R;
import com.kradac.conductor.extras.LatLng;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.OnComunicacionMapa;
import com.kradac.conductor.modelo.Destino;
import com.kradac.conductor.modelo.RespuestaPosibleSolicitudes;
import com.kradac.conductor.modelo.Solicitud;
import com.mapbox.services.Constants;
import com.mapbox.services.api.ServicesException;
import com.mapbox.services.api.directions.v5.DirectionsCriteria;
import com.mapbox.services.api.directions.v5.MapboxDirections;
import com.mapbox.services.api.directions.v5.models.DirectionsResponse;
import com.mapbox.services.api.directions.v5.models.DirectionsRoute;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DellKradac on 23/02/2018.
 */

public class MapViewConfig {
    private static final String TAG = MapViewConfig.class.getName();
    private GpsMyLocationProvider gpsMyLocationProvider;
    private Marker markeConductor, markerCliente, markerEncomienda, markerPosiblesSolicitudes, markerPanico, markerDestino;
    private ArrayList<Marker> markeSolicitudes;
    private Context context;
    private OnComunicacionMapa onComunicacionMapa;
    private Polyline lineSolicitud, lineEncomienda, lineDestino;
    String tokenUsar;

    public MapView configure(Context context, int idCiudad, MapView mapView, OnComunicacionMapa onComunicacionMapa) {
        this.context = context;
        this.onComunicacionMapa = onComunicacionMapa;
        mapView.setBuiltInZoomControls(false);
        mapView.setMultiTouchControls(true);
        mapView.setMinZoomLevel(7.0);
        mapView.setMaxZoomLevel(20.0);
        mapView.getOverlayManager().getTilesOverlay().setLoadingBackgroundColor(Color.TRANSPARENT);
        mapView.getOverlayManager().getTilesOverlay().setLoadingLineColor(Color.TRANSPARENT);
        RotationGestureOverlay mRotationGestureOverlay = new RotationGestureOverlay(mapView);
        mRotationGestureOverlay.setEnabled(true);
        mapView.getOverlays().add(mRotationGestureOverlay);


        mapView.setTilesScaledToDpi(true);
        mapView.setTilesScaleFactor(0.4f);

        Utilidades.LatLngCiudad lngCiudad = new Utilidades().getDefinirLocalizacionInicial(idCiudad);
        GeoPoint gPt = new GeoPoint(lngCiudad.getLatitud(), lngCiudad.getLongitud());
        mapView.getController().setCenter(gPt);
        mapView.getController().setZoom(17.0);
        mapView.getController().animateTo(gPt);

//        MapboxRequest mapboxRequest = new MapboxRequest();
//        mapboxRequest.verificarToken(token, msbTokenListener);
        markeSolicitudes = new ArrayList<>();
        return mapView;
    }


    public void addMarketTaxista(final LatLng latLng, String nombreTaxista, final MapView mapaOSM, float rotate) {
        if (mapaOSM != null) {
            if (markeConductor == null) {
                markeConductor = new Marker(mapaOSM);
                markeConductor.setPosition(new GeoPoint(latLng.getLatitude(), latLng.getLongitude()));
                markeConductor.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
                markeConductor.setTitle("Conductor");
                markeConductor.setSnippet(nombreTaxista);
                markeConductor.setFlat(false);
                markeConductor.setIcon(context.getResources().getDrawable(R.mipmap.ic_taxy_map_te));
                markeConductor.setDraggable(false);
                mapaOSM.getOverlays().add(markeConductor);
                mapaOSM.invalidate();
            } else {
                final Handler handler = new Handler();
                final long start = SystemClock.uptimeMillis();
                final GeoPoint startGeoPoint = new GeoPoint(markeConductor.getPosition().getLatitude(), markeConductor.getPosition().getLongitude());
                final long duration = 2000;
                final Interpolator interpolator = new LinearInterpolator();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        long elapsed = SystemClock.uptimeMillis() - start;
                        float t = interpolator.getInterpolation((float) elapsed / duration);
                        double lng = t * latLng.getLongitude() + (1 - t) * startGeoPoint.getLongitude();
                        double lat = t * latLng.getLatitude() + (1 - t) * startGeoPoint.getLatitude();
                        if (markeConductor != null) {
                            if (markeConductor.isInfoWindowShown()) {
                                markeConductor.closeInfoWindow();
                                markeConductor.showInfoWindow();
                            }
                        }
                        markeConductor.setPosition(new GeoPoint(lat, lng));
                        mapaOSM.invalidate();
                        if (t < 1.0) {
                            handler.postDelayed(this, 16);
                        }
                    }
                });
            }
        }
    }

    public void addMarketSolicitudes(MapView mapaOSM, ArrayList<Solicitud> solicitudes) {
        try {
            if (mapaOSM != null && markeSolicitudes != null) {
                for (final Solicitud s : solicitudes) {
                    Marker markeCliente = new Marker(mapaOSM);
                    markeCliente.setPosition(new GeoPoint(s.getLatitud(), s.getLongitud()));
                    markeCliente.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
                    markeCliente.setSnippet("s".concat(String.valueOf(solicitudes.indexOf(s))));
                    if (s.isPedido()) {
                        if (s.getT() == 1) {
                            markeCliente.setIcon(context.getResources().getDrawable(R.mipmap.ic_compras_taxi_osm));
                        } else {
                            markeCliente.setIcon(context.getResources().getDrawable(R.mipmap.icon_enconmiendas));
                        }

                    } else {
                        markeCliente.setIcon(context.getResources().getDrawable(R.mipmap.ic_solicitar));
                    }
                    markeCliente.setDraggable(false);
                    markeCliente.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker, MapView mapView) {
                            String position = "";
                            if (!marker.getSnippet().isEmpty()) {
                                position = marker.getSnippet().substring(0, 1);
                            }
                            if ((position.equalsIgnoreCase("s"))) {
                                onComunicacionMapa.solicitudSeleccionadaMarket(s);
                            } else {
                                marker.showInfoWindow();
                            }
                            return false;
                        }
                    });
                    mapaOSM.getOverlays().add(markeCliente);
                    mapaOSM.invalidate();
                    markeSolicitudes.add(markeCliente);
                }
            }
        } catch (ConcurrentModificationException e) {
            Log.e(TAG, "addMarketSolicitudes: " + e.getMessage());
        }

    }

    public void eliminarMarcadores(MapView mapaOSM) {
        if (mapaOSM != null && markeSolicitudes != null && markeSolicitudes.size() > 0) {
            mapaOSM.getOverlays().removeAll(markeSolicitudes);
            mapaOSM.invalidate();
            markeSolicitudes.clear();
        }
    }

    /**
     * Agregar el mardacor del cliente en el mapa ademas se encarga del trazado de la ruta dependiendo del tipo de solicitud o encomienda o pedido
     * Este metodo es generico agrega y elimina los marcadores del mapa
     *
     * @param solicitud
     * @param location
     * @param mapaOSM
     * @param isEstado
     */
    public void marketCliente(Solicitud solicitud, Location location, MapView mapaOSM, boolean isEstado, String tokenUsar) {
        if (isEstado) {
            if (mapaOSM != null) {
                if (location != null) {
                    if (markerCliente == null) {
                        markerCliente = new Marker(mapaOSM);
                        markerCliente.setPosition(new GeoPoint(solicitud.getLatitud(), solicitud.getLongitud()));
                        markerCliente.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                        markerCliente.setTitle("Cliente");
                        markerCliente.setFlat(false);
                        markerCliente.setSnippet(solicitud.getNombresCliente());
                        if (solicitud.isPedido()) {
                            if (solicitud.getT() == 1) {
                                markerCliente.setIcon(context.getResources().getDrawable(R.mipmap.icon_pedido_atendiendo));
                                //Trazar Ruta cuenado es pedido
                                Position origin = Position.fromCoordinates(location.getLongitude(), location.getLatitude());
                                Position destination = Position.fromCoordinates(solicitud.getLongitud(), solicitud.getLatitud());
                                getRoute(origin, destination, mapaOSM, null, tokenUsar, 1);
                                ///
                            } else {
                                markerCliente.setTitle("Entrega encomienda");
                                markerCliente.setIcon(context.getResources().getDrawable(R.mipmap.ic_puntero_encomienda_fin));
                                //Trazar Ruta cuenado es encomienda
                                Position origin = Position.fromCoordinates(location.getLongitude(), location.getLatitude());
                                Position destination = Position.fromCoordinates(solicitud.getLgE(), solicitud.getLtE());
                                getRoute(origin, destination, mapaOSM, null, tokenUsar, 1);
                                ///
                                if (markerEncomienda == null) {
                                    markerEncomienda = new Marker(mapaOSM);
                                    markerEncomienda.setPosition(new GeoPoint(solicitud.getLtE(), solicitud.getLgE()));
                                    markerEncomienda.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                                    markerEncomienda.setTitle("Recoje encomienda");
                                    markerEncomienda.setFlat(false);
                                    markerEncomienda.setSnippet(solicitud.getbE());
                                    markerEncomienda.setIcon(context.getResources().getDrawable(R.mipmap.ic_puntero_encomienda_inicio));
                                    markerEncomienda.setDraggable(false);
                                    mapaOSM.getOverlays().add(markerEncomienda);
                                    mapaOSM.invalidate();
                                    Position originEn = Position.fromCoordinates(solicitud.getLgE(), solicitud.getLtE());
                                    Position destinationEn = Position.fromCoordinates(solicitud.getLongitud(), solicitud.getLatitud());
                                    getRoute(originEn, destinationEn, mapaOSM, solicitud, tokenUsar, 1);
                                }
                            }
                        } else {
                            markerCliente.setIcon(context.getResources().getDrawable(R.mipmap.ic_user_map_osm));
                            //Trazar Ruta cuenado es solicitud
                            Position origin = Position.fromCoordinates(location.getLongitude(), location.getLatitude());
                            Position destination = Position.fromCoordinates(solicitud.getLongitud(), solicitud.getLatitud());
                            getRoute(origin, destination, mapaOSM, null, tokenUsar, 1);
                            if (solicitud.getlD() != null && solicitud.getlD().size() > 0) {
                                marketDestino(solicitud, mapaOSM, true, tokenUsar);
                            }
                            ///
                        }
                        markerCliente.setDraggable(false);
                        mapaOSM.getOverlays().add(markerCliente);
                        mapaOSM.invalidate();
                    }
                } else {
                    onComunicacionMapa.intentarGraficar();
                }
            }
        } else {
            if (mapaOSM != null && markerCliente != null) {
                mapaOSM.getOverlays().remove(markerCliente);
                trazarRutaSolicitud(mapaOSM, null, false);
                mapaOSM.invalidate();
                markerCliente = null;
                if (markerEncomienda != null) {
                    mapaOSM.getOverlays().remove(markerEncomienda);
                    mapaOSM.invalidate();
                    markerEncomienda = null;
                    trazarRutaEncomienda(mapaOSM, null, false);
                }
                marketDestino(null, mapaOSM, false, null);
            }
        }
    }

    public void borrarMarcadorCliente(MapView mapaOSM) {
        if (markerCliente != null) {
            markerCliente.remove(mapaOSM);
        }
    }

    public void marketDestino(Solicitud solicitud, MapView mapaOSM, boolean isEstado, String key) {
        if (isEstado) {
            for (Destino destino : solicitud.getlD()) {
                markerDestino = new Marker(mapaOSM);
                markerDestino.setPosition(new GeoPoint(destino.getLtD(), destino.getLgD()));
                markerDestino.setAnchor(org.osmdroid.views.overlay.Marker.ANCHOR_CENTER, org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM);
                markerDestino.setTitle("Destino");
                markerDestino.setSnippet(solicitud.getNombresCliente());
                markerDestino.setFlat(false);
                markerDestino.setIcon(context.getResources().getDrawable(R.mipmap.ic_user_map_osm));
                markerDestino.setDraggable(false);
                mapaOSM.getOverlays().add(markerDestino);
                mapaOSM.invalidate();
                Position origin = Position.fromCoordinates(solicitud.getLongitud(), solicitud.getLatitud());
                Position destination = Position.fromCoordinates(destino.getLgD(), destino.getLtD());
                getRoute(origin, destination, mapaOSM, null, key, 2);
            }
        } else {
            if (mapaOSM != null && markerDestino != null) {
                mapaOSM.getOverlays().remove(markerDestino);
                trazarRutaSolicitud(mapaOSM, null, false);
                mapaOSM.invalidate();
            }
        }
    }

    public synchronized void getRoute(Position origin, Position destination, final MapView mapaOSM, final Solicitud solicitud, String tokenUsar, int tipoRuta) {
        if (solicitud == null) {
            if (lineSolicitud != null) {
                return;
            }
        } else {
            if (lineEncomienda != null) {
                return;
            }
        }
        MapboxDirections client = null;
        try {
            client = new MapboxDirections.Builder()
                    .setOrigin(origin)
                    .setDestination(destination)
                    .setProfile(DirectionsCriteria.PROFILE_DRIVING)
                    .setSteps(true)
                    .setAccessToken(tokenUsar)
                    .build();
        } catch (ServicesException e) {
            return;
        }
        client.enqueueCall(new Callback<DirectionsResponse>() {
                               @Override
                               public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                                   if (response.code() == 200) {
                                       if (response.body().getRoutes() != null) {
                                           if (response.body().getRoutes().size() > 0) {
                                               DirectionsRoute currentRoute = response.body().getRoutes().get(0);
                                               switch (tipoRuta) {
                                                   case 1:
                                                       if (solicitud != null) {
                                                           trazarRutaEncomienda(mapaOSM, currentRoute, true);
                                                       } else {
                                                           trazarRutaSolicitud(mapaOSM, currentRoute, true);
                                                       }
                                                       break;
                                                   case 2:
                                                       trazarRutaDestino(mapaOSM, currentRoute, true);
                                                       break;
                                               }
                                           }
                                       }
                                   } else {
                                       Log.e(TAG, "onResponse: " + response.code());
                                   }
                               }

                               @Override
                               public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                                   Log.e(TAG, "onFailure: ", t);
                               }
                           }

        );
    }

    public void trazarRutaDestino(MapView mapaOSM, DirectionsRoute currentRoute, boolean isEstado) {
        if (isEstado) {
            List<GeoPoint> geoPoints = new ArrayList<>();
            if (currentRoute != null && mapaOSM != null && lineDestino == null) {
                LineString lineString = LineString.fromPolyline(currentRoute.getGeometry(), Constants.PRECISION_6);
                List<Position> coordinates = lineString.getCoordinates();
                for (int i = 0; i < coordinates.size(); i++) {
                    geoPoints.add(new GeoPoint(coordinates.get(i).getLatitude(), coordinates.get(i).getLongitude()));
                }
                lineDestino = new Polyline();
                lineDestino.setPoints(geoPoints);
                lineDestino.setColor(Color.parseColor("#000000"));
                mapaOSM.getOverlayManager().add(lineDestino);
            }
        } else {
            if (mapaOSM != null && lineDestino != null) {
                mapaOSM.getOverlayManager().remove(lineDestino);
                mapaOSM.invalidate();
                lineDestino = null;
            }
        }
    }

    public void trazarRutaSolicitud(MapView mapaOSM, DirectionsRoute currentRoute, boolean isEstado) {
        if (isEstado) {
            List<GeoPoint> geoPoints = new ArrayList<>();
            if (currentRoute != null && mapaOSM != null && lineSolicitud == null) {
                LineString lineString = LineString.fromPolyline(currentRoute.getGeometry(), Constants.PRECISION_6);
                List<Position> coordinates = lineString.getCoordinates();
                for (int i = 0; i < coordinates.size(); i++) {
                    geoPoints.add(new GeoPoint(coordinates.get(i).getLatitude(), coordinates.get(i).getLongitude()));
                }
                lineSolicitud = new Polyline();
                lineSolicitud.setPoints(geoPoints);
                lineSolicitud.setColor(Color.parseColor("#3887be"));
                mapaOSM.getOverlayManager().add(lineSolicitud);
            }
        } else {
            if (mapaOSM != null && lineSolicitud != null) {
                mapaOSM.getOverlayManager().remove(lineSolicitud);
                mapaOSM.invalidate();
                lineSolicitud = null;
            }
        }
    }

    public void trazarRutaEncomienda(MapView mapaOSM, DirectionsRoute currentRoute, boolean isEstado) {
        Log.e(TAG, "trazarRutaEncomienda OSM: " + isEstado);
        if (isEstado) {
            List<GeoPoint> geoPoints = new ArrayList<>();
            if (currentRoute != null && mapaOSM != null && lineEncomienda == null) {
                LineString lineString = LineString.fromPolyline(currentRoute.getGeometry(), Constants.PRECISION_6);
                List<Position> coordinates = lineString.getCoordinates();
                for (int i = 0; i < coordinates.size(); i++) {
                    geoPoints.add(new GeoPoint(coordinates.get(i).getLatitude(), coordinates.get(i).getLongitude()));
                }
                lineEncomienda = new Polyline();
                lineEncomienda.setPoints(geoPoints);
                lineEncomienda.setColor(Color.parseColor("#04B404"));
                mapaOSM.getOverlayManager().add(lineEncomienda);
            }
        } else {
            if (mapaOSM != null && lineEncomienda != null) {
                mapaOSM.getOverlayManager().remove(lineEncomienda);
                mapaOSM.invalidate();
                lineEncomienda = null;
            }
        }
    }


    public void marketPosiblesSolcitudes(RespuestaPosibleSolicitudes rPSolicitudes
            , String mensaje, MapView mapaOSM, Drawable icon, boolean isEstado) {
        if (isEstado) {
            if (mapaOSM != null) {
                if (markerPosiblesSolicitudes == null) {
                    markerPosiblesSolicitudes = new Marker(mapaOSM);
                    markerPosiblesSolicitudes.setPosition(new GeoPoint(rPSolicitudes.getLt(), rPSolicitudes.getLg()));
                    markerPosiblesSolicitudes.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                    markerPosiblesSolicitudes.setTitle("Posibles solicitudes.");
                    markerPosiblesSolicitudes.setSnippet(mensaje);
                    markerPosiblesSolicitudes.setFlat(false);
                    markerPosiblesSolicitudes.setIcon(icon);
                    markerPosiblesSolicitudes.setDraggable(false);
                    mapaOSM.getOverlays().add(markerPosiblesSolicitudes);
                    mapaOSM.invalidate();
                } else {
                    markerPosiblesSolicitudes.setPosition(new GeoPoint(rPSolicitudes.getLt(), rPSolicitudes.getLg()));
                    markerPosiblesSolicitudes.setSnippet(mensaje);
                    markerPosiblesSolicitudes.setIcon(icon);
                    mapaOSM.invalidate();
                }
            }
        } else {
            if (mapaOSM != null && markerPosiblesSolicitudes != null) {
                mapaOSM.getOverlays().remove(markerPosiblesSolicitudes);
                mapaOSM.invalidate();
                markerPosiblesSolicitudes = null;
            }
        }
    }


    public void marketPanico(LatLng latLng, String mensaje, final MapView mapaOSM, boolean isEstado) {
        if (isEstado) {
            if (mapaOSM != null) {
                if (markerPanico == null) {
                    markerPanico = new Marker(mapaOSM);
                    markerPanico.setPosition(new GeoPoint(latLng.getLatitude(), latLng.getLongitude()));
                    markerPanico.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                    markerPanico.setTitle("Panico.");
                    markerPanico.setSnippet(mensaje);
                    markerPanico.setFlat(false);
                    markerPanico.setIcon(context.getResources().getDrawable(R.mipmap.ic_launcher_panico));
                    markerPanico.setDraggable(false);
                    mapaOSM.getOverlays().add(markerPanico);
                    mapaOSM.invalidate();
                } else {
                    markerPanico.setPosition(new GeoPoint(latLng.getLatitude(), latLng.getLongitude()));
                    mapaOSM.invalidate();
                }
            }
        } else {
            if (mapaOSM != null && markerPanico != null) {
                mapaOSM.getOverlays().remove(markerPanico);
                mapaOSM.invalidate();
                markerPanico = null;
            }
        }
    }

}
