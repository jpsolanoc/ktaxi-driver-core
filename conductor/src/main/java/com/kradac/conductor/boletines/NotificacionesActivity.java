package com.kradac.conductor.boletines;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;

public class NotificacionesActivity extends FragmentContainerActivity {

    @Override
    protected Fragment createFragment() {
        return NotificacionesFragment.newInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        ActiveAndroid.initialize(this);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Notificaciones");
        }
    }
}