package com.kradac.conductor.boletines;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(view);
        bindViews(view);
        initView(view, savedInstanceState);
    }

    public void initView(View view, Bundle savedInstanceState) {
    }

    @Override
    public void onDestroyView() {
        unbindViews();
        super.onDestroyView();
    }

    private void bindViews(View rootView) {
        ButterKnife.bind(this, rootView);
    }

    private void unbindViews() {
    }

    protected abstract int getFragmentLayout();

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        //   if (conexionRedReceiver.isInitialStickyBroadcast()) {

        //    }
    }
}