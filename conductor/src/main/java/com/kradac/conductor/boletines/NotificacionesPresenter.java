package com.kradac.conductor.boletines;

import android.util.Log;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.NotificacionesContract;
import com.kradac.conductor.modelo.DatosNotificacion;
import com.kradac.conductor.modelo.ResponseApi;
import com.kradac.conductor.presentador.Presenter;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by fabricio on 29/04/16.
 */
public class NotificacionesPresenter extends Presenter implements NotificacionesContract.UserActionListener {

    private static final String TAG = NotificacionesPresenter.class.getName();
    private NotificacionesContract.View mView;

    public NotificacionesPresenter(NotificacionesContract.View view) {
        mView = view;
    }

    @Override
    public void consularMensajesCliente(int idCliente, int desde, int cuantos) {
        mView.showProgress();
        Log.e("Notificacion", "listaDatosNotificacion: Se llamo");
        ApiKtaxi.Notificacion service = retrofit.create(ApiKtaxi.Notificacion.class);
        Call<List<DatosNotificacion>> call = service.obtenerMensajesCliente(idCliente, desde, cuantos);

        call.enqueue(new Callback<List<DatosNotificacion>>() {

            @Override
            public void onResponse(Call<List<DatosNotificacion>> call, Response<List<DatosNotificacion>> response) {
                List<DatosNotificacion> listaDatosNotificacion = response.body();
                if (listaDatosNotificacion != null) {
                    Log.e("listaDatosNotificacion", listaDatosNotificacion.size() + "");

//                    )  DatosNotificacion.deleteAll();
//                    ActiveAndroid.beginTransaction();
                    for (DatosNotificacion datosNotificacion : listaDatosNotificacion) {
                        Log.e("listaDatosNotificacion", datosNotificacion.toString());
//                        datosNotificacion.save();
                    }
//                    ActiveAndroid.endTransaction();
                    mView.mensajesClienteSuccess();
                    mView.mensajeDescargados(listaDatosNotificacion);

//                    ActiveAndroid.beginTransaction();
//                    try {
//                        for(DatosNotificacion datosNotificacion:listaDatosNotificacion){
//                            datosNotificacion.save();
//                        }
//                        ActiveAndroid.setTransactionSuccessful();
//                    }
//                    finally {
//                        ActiveAndroid.endTransaction();
//                    }

                } else {
                    mView.showRequestError("No se pueden descargar los boletines");
                }
            }

            @Override
            public void onFailure(Call<List<DatosNotificacion>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void responderBoletin(int idUsuario,
                                 int idBoletin,
                                 int idBoletinPregunta,
                                 String respuesta) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnBoletin service = retrofit.create(ApiKtaxi.OnBoletin.class);
        Call<ResponseBody> call = service.responder_boletin(idUsuario,idBoletin,idBoletinPregunta,respuesta);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code()== 200){
                    try {
                        mView.repsuestaResponderMensaje(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                        mView.repsuestaResponderMensaje(null);
                    }
                }else{
                    mView.repsuestaResponderMensaje(null);
                    Log.e(TAG, "onResponse: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onResponse: " + t);
                mView.repsuestaResponderMensaje(null);
            }
        });
    }

    @Override
    public void obtenerNumeroNotiticaciones(int idCliente) {
        ApiKtaxi.Notificacion service = retrofit.create(ApiKtaxi.Notificacion.class);
        Call<DatosNotificacion> call = service.obtenerNumeroNotificaciones(idCliente);
        call.enqueue(new Callback<DatosNotificacion>() {

            @Override
            public void onResponse(Call<DatosNotificacion> call, Response<DatosNotificacion> response) {
                final DatosNotificacion datosNotificacion = response.body();
                if (datosNotificacion != null) {
//                    nRegistrosServer = datosNotificacion.getRegistros();
//                    Log.e("nRegistrosServer","server"+nRegistrosServer+" local"+DatosNotificacion.count());
//
//                    if (DatosNotificacion.count() == 0 && nRegistrosServer == 0) {
                    mView.sinMensajesNuevos();
                    mView.cargarMensajesNuevos();
//                    }
//                    if (DatosNotificacion.count() < nRegistrosServer) {
//                        mView.cargarMensajesNuevos();
//                    }
                }
            }

            @Override
            public void onFailure(Call<DatosNotificacion> call, Throwable t) {
                mView.showRequestError("Intente nuevamente");
                t.printStackTrace();
            }
        });
    }

    @Override
    public void marcarLeido(int idCliente, int idMensaje) {
        ApiKtaxi.Notificacion service = retrofit.create(ApiKtaxi.Notificacion.class);
        Call<ResponseApi> call = service.marcarLeido(idCliente, idMensaje);
        call.enqueue(new Callback<ResponseApi>() {
            @Override
            public void onResponse(Call<ResponseApi> call, Response<ResponseApi> response) {
                ResponseApi responseApi = response.body();
                if (responseApi != null) {
                    Log.e("responseApi", responseApi.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseApi> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}