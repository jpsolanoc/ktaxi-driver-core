package com.kradac.conductor.boletines;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.ActionMode;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.NotificacionesContract;
import com.kradac.conductor.modelo.DatosNotificacion;
import com.kradac.conductor.vista.DetalleBoletin;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.OnClick;


public class NotificacionesFragment extends BaseFragment implements NotificacionRecyclerAdapter.OnItemClick, NotificacionesContract.View, RecyclerView.OnItemTouchListener, View.OnClickListener, ActionMode.Callback {


    private static final String TAG = NotificacionesFragment.class.getName();
    LinearLayout lyActualizar;
    TextView tvMensajeNoConexion;
    Button button;


    private NotificacionesContract.UserActionListener mPresenterListener;
    private SwipyRefreshLayout swipeContainer;
    private RecyclerView recyclerView;
    private GestureDetectorCompat gestureDetector;
    private ActionMode actionMode;
    private NotificacionRecyclerAdapter adapter;
    private ProgressDialog progress;
    private AlertDialog dialogNotificacion;
    private ImageView imv_boletin;
    private SharedPreferences datosUsuario;
    private Dialog dialogRespuestaPreguntas;
    public static NotificacionesFragment newInstance() {
        return new NotificacionesFragment();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_items_notificaciones_boletines;
    }

    @Override
    public void initView(View view, Bundle savedInstanceState) {
        super.initView(view, savedInstanceState);
        datosUsuario = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);

        swipeContainer = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipeContainer.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                swipeContainer.setRefreshing(false);
                obtenerNumeroNotificaciones(datosUsuario.getInt("idUsuario", 0));
            }
        });
        lyActualizar = (LinearLayout) view.findViewById(R.id.lyActualizar);
        button = (Button) view.findViewById(R.id.btnActualizar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarMensajesNuevos();
            }
        });
        swipeContainer.setDirection(SwipyRefreshLayoutDirection.BOTTOM);

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


    }

    private void obtenerNumeroNotificaciones(int idUsuario) {
        mPresenterListener.obtenerNumeroNotiticaciones(idUsuario);
    }

    @OnClick(R2.id.btnActualizar)
    void actualizarNotificaciones() {
        progress.show();
        obtenerNumeroNotificaciones(datosUsuario.getInt("idUsuario", 0));
    }

    @Override
    public void onResume() {
        super.onResume();
        cargarMensajesNuevos();
        cargarNotificacionesLocal();
        obtenerNumeroNotificaciones(datosUsuario.getInt("idUsuario", 0));

    }

    private void cargarNotificacionesLocal() {
        adapter = new NotificacionRecyclerAdapter(null, this);
        recyclerView.setAdapter(adapter);
        if (adapter != null) {
//            if (DatosNotificacion.count() == 0) {
//                lyActualizar.setVisibility(View.VISIBLE);
//            } else {
//                lyActualizar.setVisibility(View.GONE);
//            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mPresenterListener == null) {
            mPresenterListener = new NotificacionesPresenter(this);
        }
//        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST);
//        recyclerView.addItemDecoration(itemDecoration);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.addOnItemTouchListener(this);
        gestureDetector = new GestureDetectorCompat(getActivity(), new RecyclerViewDemoOnGestureListener());
        List<DatosNotificacion> listAlerta = new ArrayList<>();
        DatosNotificacion alerta = new DatosNotificacion();
        alerta.setBoletin("Actualiza la aplicacion ktaxi para disfrutar del mejor servicio de taxi.");

        listAlerta.add(alerta);
        listAlerta.add(alerta);

        adapter = new NotificacionRecyclerAdapter(null, this);
        recyclerView.setAdapter(adapter);

        progress = new ProgressDialog(getActivity());
        progress.setMessage("Cargando notificaciones...");
        progress.setProgressStyle(android.R.style.Widget_DeviceDefault_Light_ProgressBar_Horizontal);


        progress.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
//        inflater.inflate(R.menu.menu_recyclerview, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.menu_delete:
//                List<Integer> selectedItemPositions = adapter.getSelectedItems();
//                for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
//                    // adapter.deleteItem(selectedItemPositions.get(i));
//                }
//                actionMode.finish();
//                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        this.actionMode = null;
        adapter.clearSelections();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().overridePendingTransition(R.anim.zoom_back_out, R.anim.zoom_back_in);
    }

    @Override
    public void onClick(View view) {
        if (view == null)
            return;
        if (view.getId() == R.id.container_list_item) {
            int idx = recyclerView.getChildPosition(view);
            if (actionMode != null) {
                myToggleSelection(idx);
            }
        }
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        gestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }

    @Override
    public void showProgress() {
        progress.show();
    }

    @Override
    public void showRequestError(String error) {
        progress.dismiss();
        Snackbar.make(recyclerView, "No hay datos que descargar", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void mensajesClienteSuccess() {
        try {
            cargarNotificacionesLocal();
            if (progress != null) {
                if (progress.isShowing()) {
                    progress.dismiss();
                }
            }
            Snackbar.make(recyclerView, "Descarga correcta", Snackbar.LENGTH_LONG).show();
        } catch (IllegalArgumentException e) {
        }


    }

    @Override
    public void sinMensajesNuevos() {
        if (progress != null) {
            progress.dismiss();
        }
        cargarNotificacionesLocal();
        Snackbar.make(recyclerView, "No hay datos que descargar", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void cargarMensajesNuevos() {
        if (getActivity() != null) {
            mPresenterListener.consularMensajesCliente(datosUsuario.getInt("idUsuario", 0), 0, 10);
        }

    }

    @Override
    public void mensajeDescargados(List<DatosNotificacion> listaMesajes) {
        adapter = new NotificacionRecyclerAdapter(listaMesajes, this);
        recyclerView.setAdapter(adapter);
        if (listaMesajes.size() == 0) {
            lyActualizar.setVisibility(View.VISIBLE);
        } else {
            lyActualizar.setVisibility(View.GONE);
        }

    }

    @Override
    public void OnClickItem(int position) {
        DatosNotificacion datosNotificacion = adapter.getItem(position);
        mostrarMensaje(datosNotificacion);
    }

    private void myToggleSelection(int idx) {
        adapter.toggleSelection(idx);
//        String title = getString(, adapter.getSelectedItemCount());
//        actionMode.setTitle(title);
    }

    private void mostrarMensaje(DatosNotificacion datosNotificacion) {
        SharedPreferences datosUsuario = getActivity().getSharedPreferences("login", Context.MODE_PRIVATE);
        mPresenterListener.marcarLeido(datosUsuario.getInt("idUsuario", 0), datosNotificacion.getIdBoletin());
        if (datosNotificacion.getTipo() == 1) {
            Intent intent = new Intent(getActivity(), DetalleBoletin.class);
            intent.putExtra("datosNotificacion", datosNotificacion);
            startActivity(intent);
        } else {
            dialogRespuestaPreguntas =createPreguntasDialogo(datosNotificacion);
            dialogRespuestaPreguntas.show();
        }
    }


    private DatosNotificacion.LP preguntaSeleccionada;

    /**
     * Crea un diálogo con personalizado
     *
     * @return Diálogo
     */
    public AlertDialog createPreguntasDialogo(final DatosNotificacion datosNotificacion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.boletin_respuesta, null);
        TextView tvAsunto = v.findViewById(R.id.tv_asunto);
        TextView tvBoletin = v.findViewById(R.id.tv_boletin);
        TextView tvVerDetalle = v.findViewById(R.id.tv_ver_detalle);
        LinearLayout lyCheckPreguntas = v.findViewById(R.id.ly_check_preguntas);
        final EditText etRepuestaPersonalizada = v.findViewById(R.id.et_repuesta_personalizada);
        if (datosNotificacion.getTipo() == 2) {
            etRepuestaPersonalizada.setVisibility(View.GONE);
        }
        Button btnEnviarRespuesta = v.findViewById(R.id.btn_enviar_respuesta);
        tvAsunto.setText(datosNotificacion.getAsunto());
        tvBoletin.setText(datosNotificacion.getBoletin());
        deshabilitarCopyPage(etRepuestaPersonalizada);
        final List<CheckBox> checkTodos = new ArrayList<>();
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int idSelecionado = v.getId();
                Log.e("CLICK", "onClick: " + datosNotificacion.getLP().get((idSelecionado - 1000)).toString());
                preguntaSeleccionada = datosNotificacion.getLP().get((idSelecionado - 1000));
                for (CheckBox checkBox : checkTodos) {
                    if (checkBox.getId() == idSelecionado) {
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                }
            }
        };
        int chId = 1000;
        if (datosNotificacion.getLP() != null) {
            for (DatosNotificacion.LP lp : datosNotificacion.getLP()) {
                CheckBox opcion = new CheckBox(getActivity());
                opcion.setId(chId++);
                opcion.setOnClickListener(clickListener);
                opcion.setText(lp.getPregunta());
                opcion.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
                lyCheckPreguntas.addView(opcion);
                checkTodos.add(opcion);
            }
        } else {
            lyCheckPreguntas.setVisibility(View.GONE);
        }

        btnEnviarRespuesta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "onClick: " + datosNotificacion.toString() );
                int preguntaSelec = -1;
                String pregunta = "-1";
                switch (datosNotificacion.getTipo()) {
                    case 2:
                        if (preguntaSeleccionada != null) {
                            mPresenterListener.responderBoletin(datosUsuario.getInt(VariablesGlobales.ID_USUARIO, 0), datosNotificacion.getIdBoletin(), preguntaSeleccionada.getId(), "-1");
                        } else {
                            Toast.makeText(getActivity(), "Por favor seleccione una respuesta a la pregunta para continuar.", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case 3:
                        if (preguntaSeleccionada != null) {
                            preguntaSelec = preguntaSeleccionada.getId();
                        }
                        if (!etRepuestaPersonalizada.getText().toString().isEmpty()) {
                            pregunta = etRepuestaPersonalizada.getText().toString();
                        }
                        mPresenterListener.responderBoletin(
                                datosUsuario.getInt(VariablesGlobales.ID_USUARIO, 0),
                                datosNotificacion.getIdBoletin(),
                                preguntaSelec, pregunta);
                        break;
                    case 4:
                        if (preguntaSeleccionada != null) {
                            preguntaSelec = preguntaSeleccionada.getId();
                        }
                        if (!etRepuestaPersonalizada.getText().toString().isEmpty()) {
                            pregunta = etRepuestaPersonalizada.getText().toString();
                        }
                        if (datosNotificacion.getLP() != null && datosNotificacion.getLP().size() > 0) {
                            if (preguntaSelec == -1) {
                                Toast.makeText(getActivity(), "Seleccione una respuesta de la lista.", Toast.LENGTH_LONG).show();
                                return;
                            }
                            if (pregunta.equals("-1")) {
                                etRepuestaPersonalizada.requestFocus();
                                Toast.makeText(getActivity(), "Escriba una respuesta a la pregunta.", Toast.LENGTH_LONG).show();
                                return;
                            }
                            mPresenterListener.responderBoletin(
                                    datosUsuario.getInt(VariablesGlobales.ID_USUARIO, 0),
                                    datosNotificacion.getIdBoletin(),
                                    preguntaSelec, pregunta);
                        } else {
                            if (pregunta.equals("-1")) {
                                etRepuestaPersonalizada.requestFocus();
                                Toast.makeText(getActivity(), "Escriba una respuesta a la pregunta.", Toast.LENGTH_LONG).show();
                                return;
                            }
                            mPresenterListener.responderBoletin(
                                    datosUsuario.getInt(VariablesGlobales.ID_USUARIO, 0),
                                    datosNotificacion.getIdBoletin(),
                                    preguntaSelec, pregunta);
                        }

                        break;
                }
            }
        });
        if (datosNotificacion.getUrl() != null&&!datosNotificacion.getUrl().isEmpty()) {
            subrayarTextoConAcciones(datosNotificacion.getVinculo(), tvVerDetalle);
            tvVerDetalle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Uri uri = Uri.parse(datosNotificacion.getUrl());
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Log.e(TAG, "onClick Vinculo: ", e);
                    }
                }
            });
        } else {
            tvVerDetalle.setVisibility(View.GONE);
        }
        builder.setCancelable(false);
        builder.setView(v);
        return builder.create();
    }
    public void deshabilitarCopyPage(EditText textField){
        textField.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode actionMode, MenuItem item) {
                return false;
            }

            public void onDestroyActionMode(ActionMode actionMode) {
            }
        });

        textField.setLongClickable(false);
        textField.setTextIsSelectable(false);
    }

    public void subrayarTextoConAcciones(String text, TextView tvHide) {
        SpannableString spanString = new SpannableString(text);
        spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
        tvHide.setText(spanString);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private class RecyclerViewDemoOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            onClick(view);
            return super.onSingleTapConfirmed(e);
        }

        public void onLongPress(MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            if (actionMode != null) {
                return;
            }
            actionMode = getActivity().startActionMode(NotificacionesFragment.this);
            int idx = recyclerView.getChildPosition(view);
            myToggleSelection(idx);
            super.onLongPress(e);
        }
    }

    @Override
    public void repsuestaResponderMensaje(String dato) {
        if (dialogRespuestaPreguntas!=null){
            if (dialogRespuestaPreguntas.isShowing()){
                dialogRespuestaPreguntas.dismiss();
            }
        }
        if (dato != null) {
            try {
                JSONObject object = new JSONObject(dato);
                if (object.getInt("en") == 1) {
                    cargarNotificacionesLocal();
                    obtenerNumeroNotificaciones(datosUsuario.getInt("idUsuario", 0));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}