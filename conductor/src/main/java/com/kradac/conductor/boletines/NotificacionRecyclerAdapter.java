package com.kradac.conductor.boletines;

import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.modelo.DatosNotificacion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabricio on 01/04/16.
 */
public class NotificacionRecyclerAdapter extends RecyclerView.Adapter<NotificacionRecyclerAdapter.DataObjectHolder> {
    OnItemClick onItemClick;
    private List<DatosNotificacion> mDataset;
    private SparseBooleanArray selectedItems;

    public NotificacionRecyclerAdapter(List<DatosNotificacion> myDataset, OnItemClick onItemClick) {
        mDataset = myDataset;
        selectedItems = new SparseBooleanArray();
        this.onItemClick = onItemClick;
    }

    public DatosNotificacion getItem(int position) {
        return mDataset.get(position);
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_notificacion_boletines, parent, false);
        return new DataObjectHolder(view, onItemClick);
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        DatosNotificacion DatosNotificacion = mDataset.get(position);
        holder.tvAsunto.setText(DatosNotificacion.getAsunto());
        holder.tvMensaje.setText(DatosNotificacion.getBoletin());
        holder.itemView.setActivated(selectedItems.get(position, false));
    }

    public void addItem(DatosNotificacion dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    @Override
    public int getItemCount() {
        if (mDataset == null) {
            return 0;
        }
        return mDataset.size();
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items =
                new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public interface OnItemClick {
        void OnClickItem(int position);
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvMensaje;
        TextView tvAsunto;
        LinearLayout linearLayout;
        OnItemClick onItemClick;

        public DataObjectHolder(View itemView, OnItemClick onItemClick) {
            super(itemView);
            this.onItemClick = onItemClick;
            tvMensaje = (TextView) itemView.findViewById(R.id.txtNumero);
            tvAsunto = (TextView) itemView.findViewById(R.id.txtTitulo);
            itemView.setOnClickListener(this);
            if (itemView.isActivated())
                itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.negro));
            else
                itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.blanco));

        }

        @Override
        public void onClick(View v) {
            this.onItemClick.OnClickItem(getPosition());
        }
    }

}