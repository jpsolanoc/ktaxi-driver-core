package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.fragment.TransaccionesSaldoKtaxiFragment;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionTransaccionesSaldoKtaxi;
import com.kradac.conductor.modelo.SaldoKtaxi;
import com.kradac.conductor.modelo.TransaccionesSaldoKtaxi;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ing.John Patricio Solano Cabrera on 8/1/2018.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class TransaccionesSaldoKtaxiPresentador extends  Presenter {

    private static final String TAG = TransaccionesSaldoKtaxiPresentador.class.getName();
    OnComunicacionTransaccionesSaldoKtaxi onComunicacionTransaccionesSaldoKtaxi;

    public TransaccionesSaldoKtaxiPresentador(OnComunicacionTransaccionesSaldoKtaxi onComunicacionTransaccionesSaldoKtaxi) {
        this.onComunicacionTransaccionesSaldoKtaxi = onComunicacionTransaccionesSaldoKtaxi;
    }

    public void consultar_transacciones(int anio,int mes,int idVehiculo,int idUsuario,int idCiudad){
        ApiKtaxi.ComponentesContables service = retrofit.create(ApiKtaxi.ComponentesContables.class);
        Call<TransaccionesSaldoKtaxi> call = service.consultar_transacciones(anio,mes,idVehiculo,idUsuario,idCiudad);
        call.enqueue(new Callback<TransaccionesSaldoKtaxi>() {
            @Override
            public void onResponse(Call<TransaccionesSaldoKtaxi> call, Response<TransaccionesSaldoKtaxi> response) {
                if (response.code()==200){
                    onComunicacionTransaccionesSaldoKtaxi.respuestaServidorTransacciones(response.body());
                }else{
                    Log.e(TAG, "onResponse: " +response.code() );
                    onComunicacionTransaccionesSaldoKtaxi.respuestaServidorTransacciones(null);
                }
            }
            @Override
            public void onFailure(Call<TransaccionesSaldoKtaxi> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t );
                onComunicacionTransaccionesSaldoKtaxi.respuestaServidorTransacciones(null);
            }
        });
    }
}
