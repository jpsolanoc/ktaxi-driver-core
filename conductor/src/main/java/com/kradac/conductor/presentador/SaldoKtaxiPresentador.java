package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionCompraServicios;
import com.kradac.conductor.interfaces.OnComunicacionCompraServiciosMapa;
import com.kradac.conductor.interfaces.OnComunicacionSaldoKtaxi;
import com.kradac.conductor.modelo.ComponentePublicitario;
import com.kradac.conductor.modelo.SaldoKtaxi;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ing.John Patricio Solano Cabrera on 8/1/2018.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class SaldoKtaxiPresentador extends Presenter {

    private static final String TAG = SaldoKtaxiPresentador.class.getName();
    OnComunicacionSaldoKtaxi onComunicacionSaldoKtaxi;
    OnComunicacionCompraServicios onComunicacionCompraServicios;
    OnComunicacionCompraServiciosMapa onComunicacionCompraServiciosMapa;

    public SaldoKtaxiPresentador(OnComunicacionSaldoKtaxi onComunicacionSaldoKtaxi, OnComunicacionCompraServicios onComunicacionCompraServicios,OnComunicacionCompraServiciosMapa  onComunicacionCompraServiciosMapa) {
        this.onComunicacionSaldoKtaxi = onComunicacionSaldoKtaxi;
        this.onComunicacionCompraServicios = onComunicacionCompraServicios;
        this.onComunicacionCompraServiciosMapa = onComunicacionCompraServiciosMapa;
    }


    public void saldoConsultar(int idVehiculo,int idUsuario,int idCiudad){
        ApiKtaxi.ComponentesContables service = retrofit.create(ApiKtaxi.ComponentesContables.class);
        Call<SaldoKtaxi.ItemSaldoKtaxi> call = service.consultarSaldo(idVehiculo,idUsuario,idCiudad);
        call.enqueue(new Callback<SaldoKtaxi.ItemSaldoKtaxi>() {
            @Override
            public void onResponse(Call<SaldoKtaxi.ItemSaldoKtaxi> call, Response<SaldoKtaxi.ItemSaldoKtaxi> response) {
                if (response.code()==200){
                    if (onComunicacionSaldoKtaxi!=null){
                        onComunicacionSaldoKtaxi.repsuestaListaSaldoKtaxi(response.body());
                    }else if(onComunicacionCompraServicios!=null){
                        onComunicacionCompraServicios.repsuestaListaSaldoKtaxi(response.body());
                    }else if(onComunicacionCompraServiciosMapa!=null){
                        onComunicacionCompraServiciosMapa.repsuestaListaSaldoKtaxi(response.body());
                    }
                }else{
                    Log.e(TAG, "onResponse: " +response.code() );
                    onComunicacionSaldoKtaxi.repsuestaListaSaldoKtaxi(null);
                }
            }
            @Override
            public void onFailure(Call<SaldoKtaxi.ItemSaldoKtaxi> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t );
                onComunicacionSaldoKtaxi.repsuestaListaSaldoKtaxi(null);
            }
        });
    }
}
