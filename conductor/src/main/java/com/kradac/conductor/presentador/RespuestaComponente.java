package com.kradac.conductor.presentador;

import com.kradac.conductor.modelo.ComponenteBase;

import java.io.Serializable;
import java.util.List;
/**
 * Created by John on 02/01/2017.
 */

public class RespuestaComponente implements Serializable {
    private int en;
    private String m;
    private List<ComponenteBase> lIC;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public List<ComponenteBase> getlIC() {
        return lIC;
    }

    public void setlIC(List<ComponenteBase> lIC) {
        this.lIC = lIC;
    }

    @Override
    public String toString() {
        return "RespuestaComponente{" +
                "en=" + en +
                ", m='" + m + '\'' +
                ", lIC=" + lIC +
                '}';
    }
}
