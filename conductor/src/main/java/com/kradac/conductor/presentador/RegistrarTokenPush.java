package com.kradac.conductor.presentador;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v13.app.ActivityCompat;
import android.util.Log;

import com.kradac.conductor.extras.MetodosValidacion;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;

import java.io.IOException;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DellKradac on 02/03/2018.
 */

public class RegistrarTokenPush extends Presenter {
    private Utilidades utilidades;
    private Context context;
    private static final String TAG = RegistrarTokenPush.class.getName();

    public RegistrarTokenPush(Context context) {
        this.utilidades = new Utilidades();
        this.context = context;
    }

    public void registrarToken(int idUsuario, final String tokenFirebase, final boolean isRefresh) {
        String timeStanD = utilidades.getTimeStanD();
        String token = utilidades.SHA256(timeStanD + MetodosValidacion.MD5(utilidades.getImei(context)));
        double latitud = 0.0;
        double longitud = 0.0;
        if (localizacionActual() != null) {
            latitud = localizacionActual().getLatitude();
            longitud = localizacionActual().getLongitude();
        }

        ApiKtaxi.OnRegistrarTokenPush service = retrofit.create(ApiKtaxi.OnRegistrarTokenPush.class);
        Call<ResponseBody> call = service.registrar_token(idUsuario, utilidades.getImei(context),
                token, MetodosValidacion.MD5(timeStanD + token + timeStanD), timeStanD, tokenFirebase, latitud, longitud,
                utilidades.obtenerVersion(context), utilidades.obtenerVersionSo(), utilidades.getDeviceName(), utilidades.getDeviceName(),
                utilidades.getTipoRed(context), utilidades.statusGPS(context), utilidades.getConnectivityStatus(context), utilidades.getNameOperator(context));
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        Log.e(TAG, "onResponse registrarToken: " + response.body().string());
                        if (!isRefresh) {
                            SharedPreferences spFirebase = context.getSharedPreferences(VariablesGlobales.ESTADO_FIREBASE, Context.MODE_PRIVATE);
                            spFirebase.edit().putBoolean(VariablesGlobales.KEY_FIREBASE_ENVIADO, true).apply();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e(TAG, "onResponse registrarToken: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure registrarToken: ", t);
            }
        });
    }

    public void registrarTokenAnonimo(final String tokenFirebase) {
        String timeStanD = utilidades.getTimeStanD();
        String token = utilidades.SHA256(timeStanD + MetodosValidacion.MD5(utilidades.getImei(context)));
        double latitud = 0.0;
        double longitud = 0.0;
        if (localizacionActual() != null) {
            latitud = localizacionActual().getLatitude();
            longitud = localizacionActual().getLongitude();
        }
        Random randomGenerator = new Random();
        int randomInt = randomGenerator.nextInt(100);
        ApiKtaxi.OnRegistrarTokenPush service = retrofit.create(ApiKtaxi.OnRegistrarTokenPush.class);

        Call<ResponseBody> call = service.registrar_token_anonimo(randomInt, utilidades.getImei(context),
                token, MetodosValidacion.MD5(timeStanD + token + timeStanD), timeStanD, tokenFirebase, latitud, longitud,
                utilidades.obtenerVersion(context), utilidades.obtenerVersionSo(), utilidades.getDeviceName(), utilidades.getDeviceName(),
                utilidades.getTipoRed(context), utilidades.statusGPS(context), utilidades.getConnectivityStatus(context), utilidades.getNameOperator(context));
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        Log.e(TAG, "onResponse registrarTokenAnonimo: " + response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e(TAG, "onResponse registrarTokenAnonimo: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure registrarTokenAnonimo: ", t);
            }
        });
    }


    public Location localizacionActual() {
        Location retorno = null;
        LocationManager mlocManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        } else {
            retorno = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (retorno != null) {
                return retorno;
            } else {
                return mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
        }
    }

}
