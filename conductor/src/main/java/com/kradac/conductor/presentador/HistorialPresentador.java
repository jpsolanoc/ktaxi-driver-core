package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionHistorial;
import com.kradac.conductor.interfaces.OnComunicacionHistorialCompras;
import com.kradac.conductor.modelo.Historial;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by John on 02/12/2016.
 */

public class HistorialPresentador extends Presenter {
    private static final String TAG = HistorialPresentador.class.getName();
    private OnComunicacionHistorial onComunicacionHistorial;
    private OnComunicacionHistorialCompras onComunicacionHistorialCompras;

    public HistorialPresentador(OnComunicacionHistorial onComunicacionHistorial) {
        this.onComunicacionHistorial = onComunicacionHistorial;
    }

    public HistorialPresentador(OnComunicacionHistorialCompras onComunicacionHistorialCompras) {
        this.onComunicacionHistorialCompras = onComunicacionHistorialCompras;
    }

//    public void obtenerCuantos(int idUsuario) {
//        cambioIpPresenter(VariablesPublicidad.IP_SERVICIOS_SOCKET);
//        ApiKtaxi.OnHistorial service = retrofit.create(ApiKtaxi.OnHistorial.class);
//        Call<Historial> call = service.historialCuantos(idUsuario);
//        call.enqueue(new Callback<Historial>() {
//            @Override
//            public void onResponse(Call<Historial> call, Response<Historial> response) {
//                Historial historial = response.body();
////                onComunicacionHistorial.totalHistoricoSolicitud(historial.getRegistros());
//                Log.e(TAG, "onResponse: " + historial.getRegistros());
//            }
//
//            @Override
//            public void onFailure(Call<Historial> call, Throwable t) {
////                onComunicacionHistorial.totalHistoricoSolicitud(0);
//                Log.e(TAG, "onFailure: " + t);
//            }
//        });
//    }
//
//    public void obtenerCuantosCompras(int idUsuario) {
//        cambioIpPresenter(VariablesPublicidad.IP_SERVICIOS_SOCKET);
//        ApiKtaxi.OnHistorial service = retrofit.create(ApiKtaxi.OnHistorial.class);
//        Call<Historial> call = service.historialCuantosCompras(idUsuario);
//        call.enqueue(new Callback<Historial>() {
//            @Override
//            public void onResponse(Call<Historial> call, Response<Historial> response) {
//                Historial historial = response.body();
//                onComunicacionHistorialCompras.totalHistoricoCompras(historial.getRegistros());
//                Log.e(TAG, "onResponse: Compras" + historial.getRegistros());
//            }
//
//            @Override
//            public void onFailure(Call<Historial> call, Throwable t) {
//                onComunicacionHistorialCompras.totalHistoricoCompras(0);
//                Log.e(TAG, "onFailure: " + t);
//            }
//        });
//    }



    public void obtenerUltimosdi(int idUsuario) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnHistorial service = retrofit.create(ApiKtaxi.OnHistorial.class);
        Call<Historial.HistorialUltimas> call = service.historialUltimas(idUsuario);
        call.enqueue(new Callback<Historial.HistorialUltimas>() {

            @Override
            public void onResponse(Call<Historial.HistorialUltimas> call, Response<Historial.HistorialUltimas> response) {
                Historial.HistorialUltimas historialUltimas = response.body();
//                onComunicacionHistorial.ultimasHistorico(historialUltimas.getRegistros());
            }

            @Override
            public void onFailure(Call<Historial.HistorialUltimas> call, Throwable t) {

            }
        });
    }


    public void obtenerHistorial(int idUsuario, int desde, int cuantos) {
        Log.e(TAG, "obtenerHistorial: " + idUsuario + "   " + desde + "   " + cuantos);
        ApiKtaxi.OnHistorial service = retrofit.create(ApiKtaxi.OnHistorial.class);
        Call<ArrayList<Historial>> call = service.historialObtener(idUsuario, desde, cuantos);
        call.enqueue(new Callback<ArrayList<Historial>>() {
            @Override
            public void onResponse(Call<ArrayList<Historial>> call, Response<ArrayList<Historial>> response) {
                ArrayList<Historial> historiales = response.body();
                Log.e(TAG, "onResponse: " + historiales);
//                onComunicacionHistorial.listaHistorico(historiales);
            }

            @Override
            public void onFailure(Call<ArrayList<Historial>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t);
            }
        });
    }

}
