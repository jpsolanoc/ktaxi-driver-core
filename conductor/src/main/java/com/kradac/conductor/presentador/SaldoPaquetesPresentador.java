package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.modelo.RespuestaSaldoPaquetes;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaldoPaquetesPresentador extends Presenter {

    private static final String TAG = SaldoPaquetesPresentador.class.getName();

    public interface OnComunicacionSaldoPaquetes {
        void respuestaCPaquetesObtener(RespuestaSaldoPaquetes respuestaSaldoPaquetes);
    }

    public void cPaquetesObtener(int idUsuario, String token, String key, String timeStanD, final OnComunicacionSaldoPaquetes onComunicacionSaldoPaquetes) {
        ApiKtaxi.OnPaquetesObtener service = retrofit.create(ApiKtaxi.OnPaquetesObtener.class);
        Call<RespuestaSaldoPaquetes> call = service.cPaquetesObtener(idUsuario, token, key, timeStanD);
        call.enqueue(new Callback<RespuestaSaldoPaquetes>() {
            @Override
            public void onResponse(Call<RespuestaSaldoPaquetes> call, Response<RespuestaSaldoPaquetes> response) {
                if (response.code() == HttpsURLConnection.HTTP_OK) {
                    onComunicacionSaldoPaquetes.respuestaCPaquetesObtener(response.body());
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionSaldoPaquetes.respuestaCPaquetesObtener(null);
                }
            }

            @Override
            public void onFailure(Call<RespuestaSaldoPaquetes> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionSaldoPaquetes.respuestaCPaquetesObtener(null);
            }
        });
    }

}
