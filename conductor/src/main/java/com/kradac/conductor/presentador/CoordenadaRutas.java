package com.kradac.conductor.presentador;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DellKradac on 28/01/2018.
 */

public class CoordenadaRutas extends Presenter{

        public void getDirecciones(double latitudInicial,double longitudInicial,double latitudFinal,double longitudFinal) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_DIRECCIONES);
        ApiKtaxi.OnCoordenadas service = retrofit.create(ApiKtaxi.OnCoordenadas.class);
        Call<ResponseBody> call = service.getDireccion(longitudInicial,latitudInicial,longitudFinal,latitudFinal);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

}
