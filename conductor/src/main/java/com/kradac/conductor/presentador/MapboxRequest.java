package com.kradac.conductor.presentador;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.modelo.ResponseMapboxToken;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DellKradac on 23/02/2018.
 */

public class MapboxRequest extends Presenter {

    public void verificarToken(String token, final MBTokenListener mbTokenListener) {
        cambioIpPresenter("https://api.mapbox.com/");
        ApiKtaxi.IMapbox service = retrofit.create(ApiKtaxi.IMapbox.class);
        Call<ResponseMapboxToken> call = service.checkToken(token);
        call.enqueue(new Callback<ResponseMapboxToken>() {

            @Override
            public void onResponse(Call<ResponseMapboxToken> call, Response<ResponseMapboxToken> response) {
                ResponseMapboxToken mapboxToken = response.body();
                if (mapboxToken != null) {
                    mbTokenListener.status(mapboxToken.isTokenValid());
                } else {
                    mbTokenListener.status(true);
                }
            }

            @Override
            public void onFailure(Call<ResponseMapboxToken> call, Throwable t) {
                mbTokenListener.status(true);
            }
        });
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
    }

    public void verificarTokenV2(String token, final MBTokenListener mbTokenListener) {
        cambioIpPresenter("http://api.tiles.mapbox.com/");
        ApiKtaxi.IMapbox service = retrofit.create(ApiKtaxi.IMapbox.class);
        Call<ResponseBody> call = service.checkTokenV2(token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    mbTokenListener.status(true);
                } else {
                    mbTokenListener.status(false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mbTokenListener.status(true);
            }
        });
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
    }

    public interface MBTokenListener {
        void status(boolean status);
    }

}
