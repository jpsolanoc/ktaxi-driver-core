package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionCompraServicios;
import com.kradac.conductor.modelo.SaldoKtaxi;
import com.kradac.conductor.vista.DetalleCompraServicioActivity;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ing.John Patricio Solano Cabrera on 8/1/2018.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class DetalleCompraServicioPresentador extends Presenter {


    private static final String TAG = DetalleCompraServicioActivity.class.getName();
    OnComunicacionCompraServicios onComunicacionCompraServicios;

    public DetalleCompraServicioPresentador(OnComunicacionCompraServicios onComunicacionCompraServicios) {
        this.onComunicacionCompraServicios = onComunicacionCompraServicios;
    }

    public void consultar_servicios(int idCategoria,int idVehiculo,int idUsuario,int idCiudad){
        ApiKtaxi.ComponentesContables service = retrofit.create(ApiKtaxi.ComponentesContables.class);
        Call<SaldoKtaxi.CompraServicios> call = service.consultar_servicios(idCategoria,idVehiculo,idUsuario,idCiudad);
        call.enqueue(new Callback<SaldoKtaxi.CompraServicios>() {
            @Override
            public void onResponse(Call<SaldoKtaxi.CompraServicios> call, Response<SaldoKtaxi.CompraServicios> response) {
                if (response.code()==200){
                    onComunicacionCompraServicios.respuestaServidorServicios(response.body());
                }else{
                    Log.e(TAG, "onResponse: " +response.code() );
                    onComunicacionCompraServicios.respuestaServidorServicios(null);
                }

            }

            @Override
            public void onFailure(Call<SaldoKtaxi.CompraServicios> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t );
                onComunicacionCompraServicios.respuestaServidorServicios(null);
            }
        });
    }
}
