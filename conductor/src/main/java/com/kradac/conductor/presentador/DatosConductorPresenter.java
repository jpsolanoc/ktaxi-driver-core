package com.kradac.conductor.presentador;


import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.modelo.ResponseDatosConductor;
import com.kradac.conductor.modelo.ResponseValorar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by dev on 06/01/17.
 */

public class DatosConductorPresenter extends Presenter {

    public interface DatosTaxiListener {
        void onResponse(ResponseDatosConductor responseTaxi);
        void onError(String error);
    }
    
    public void obtenerDatosTaxi(int idUsuario,int idVehiculo,int tipo,int idAplicativo,final DatosTaxiListener datosTaxiListener){
        ApiKtaxi.DatosConductor service = retrofit.create(ApiKtaxi.DatosConductor.class);
        Call<ResponseDatosConductor> call = service.obtenerDatosConductor(idUsuario,idVehiculo,tipo,idAplicativo);
        call.enqueue(new Callback<ResponseDatosConductor>() {

            @Override
            public void onResponse(Call<ResponseDatosConductor> call, Response<ResponseDatosConductor> response) {
                ResponseDatosConductor responseDatosConductor = response.body();
                if (responseDatosConductor != null) {
                    datosTaxiListener.onResponse(responseDatosConductor);
                }else{
                    datosTaxiListener.onError("No se logró obtener datos");
                }

            }

            @Override
            public void onFailure(Call<ResponseDatosConductor> call, Throwable t) {
                t.printStackTrace();
                datosTaxiListener.onError("No se logró obtener datos");
            }
        });
    }

    public interface ValorarConductorListener{
        void onResponse(ResponseValorar responseValorar);
        void onError(String error);
        void onException();
    }

    public interface ValorarConductorCompraListener{
        void onResponse(ResponseValorar responseValorar);
        void onError(String error);
        void onException();
    }

    public void valorarConductor(int idSolicitud, int valoracion, String observacion, final ValorarConductorListener valorarConductorListener){
        ApiKtaxi.ValorarSolicitud service = retrofit.create(ApiKtaxi.ValorarSolicitud.class);
        Call<ResponseValorar> call = service.valorarSolicitud(idSolicitud,valoracion,observacion);
        call.enqueue(new Callback<ResponseValorar>() {
            @Override
            public void onResponse(Call<ResponseValorar> call, Response<ResponseValorar> response) {
                ResponseValorar responseValorar = response.body();
                if (responseValorar != null) {
                    if(responseValorar.getError()==0){
                        valorarConductorListener.onResponse(responseValorar);
                    }else{
                        valorarConductorListener.onException();
                    }
                }else{
                    valorarConductorListener.onException();
                }
            }

            @Override
            public void onFailure(Call<ResponseValorar> call, Throwable t) {
                t.printStackTrace();
                valorarConductorListener.onException();
            }
        });
    }

    public void valorarConductorCompra(int idSolicitud, int valoracion, String observacion, final ValorarConductorCompraListener valorarConductorListener){
        ApiKtaxi.ValorarSolicitud service = retrofit.create(ApiKtaxi.ValorarSolicitud.class);
        Call<ResponseValorar> call = service.valorarCompra(idSolicitud,observacion,valoracion);
        call.enqueue(new Callback<ResponseValorar>() {
            @Override
            public void onResponse(Call<ResponseValorar> call, Response<ResponseValorar> response) {
                ResponseValorar responseValorar = response.body();
                if (responseValorar != null) {
                    if(responseValorar.getError()==0){
                        valorarConductorListener.onResponse(responseValorar);
                    }else{
                        valorarConductorListener.onException();
                    }
                }else{
                    valorarConductorListener.onException();
                }
            }

            @Override
            public void onFailure(Call<ResponseValorar> call, Throwable t) {
                t.printStackTrace();
                valorarConductorListener.onException();
            }
        });
    }
}
