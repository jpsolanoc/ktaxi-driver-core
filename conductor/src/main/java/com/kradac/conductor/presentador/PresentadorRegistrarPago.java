package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionRegistroPago;
import com.kradac.conductor.modelo.EntidadFinanciera;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ing.John Patricio Solano Cabrera on 25/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class PresentadorRegistrarPago extends Presenter {
    private static final String TAG = PresentadorRegistrarPago.class.getName();
    OnComunicacionRegistroPago onComunicacionRegistroPago;

    public PresentadorRegistrarPago(OnComunicacionRegistroPago onComunicacionRegistroPago) {
        this.onComunicacionRegistroPago = onComunicacionRegistroPago;
    }

    public void getListarEntidades(int idAplicativo, int idCiudad, int idUsuario) {
        ApiKtaxi.OnPagosConductor service = retrofit.create(ApiKtaxi.OnPagosConductor.class);
        Call<EntidadFinanciera> call = service.listar_entidades(idAplicativo, idCiudad, idUsuario);
        call.enqueue(new Callback<EntidadFinanciera>() {

            @Override
            public void onResponse(Call<EntidadFinanciera> call, Response<EntidadFinanciera> response) {
                if (response.code() == 200) {
                    onComunicacionRegistroPago.respuestaEntidadFinanciera(response.body());
                } else {
                    onComunicacionRegistroPago.respuestaEntidadFinanciera(null);
                    Log.e(TAG, "onResponse: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<EntidadFinanciera> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionRegistroPago.respuestaEntidadFinanciera(null);
            }
        });
    }

    public void registrar_pago(int idAplicativo,
                               int idCiudad,
                               String numero,
                               int idUsuario,
                               int idEntidad,
                               double descuento,
                               double impuesto,
                               double monto,
                               String lIdDeuda,
                               String lMeses,
                               String lMora,
                               String lMoraImpuestos,
                               String lDescuento,
                               String lPago) {

        Log.e(TAG, "registrar_pago: " + idAplicativo + ":_: " + idCiudad + ":_: " + numero + ":_: " + idUsuario + ":_: " + idEntidad + ":_: " + descuento + ":_: " + impuesto + ":_: " + monto + ":_: " + lIdDeuda + ":_: " + lMeses + ":_: " + lMora + ":_: " + lMoraImpuestos + ":_: " + lDescuento + ":_: " + lPago);
        ApiKtaxi.OnPagosConductor service = retrofit.create(ApiKtaxi.OnPagosConductor.class);
        Call<ResponseBody> call = service.registrar_pago(idAplicativo, idCiudad, numero, idUsuario, idEntidad, descuento, impuesto, monto, lIdDeuda, lMeses, lMora, lMoraImpuestos, lDescuento, lPago);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String dato = response.body().string();
                        Log.e(TAG, "onResponse: " + dato);
                        onComunicacionRegistroPago.respuestaRegistrarpago(dato);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "onResponse: " + response.code());
                        onComunicacionRegistroPago.respuestaRegistrarpago(null);
                    }
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionRegistroPago.respuestaRegistrarpago(null);

                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onComunicacionRegistroPago.respuestaRegistrarpago(null);
            }
        });
    }

    public void registrar_tikect(int idEntidadRegistro,
                                 String numero,
                                 String idUsuario,
                                 double idDeuda,
                                 String idEntidad,
                                 String descuento) {

        Log.e(TAG, "registrar_tikect: " + idEntidadRegistro + " :: " + numero + " :: " + idUsuario + " :: " + idDeuda + " :: " + idEntidad + " :: " + descuento);
        ApiKtaxi.OnPagosConductor service = retrofit.create(ApiKtaxi.OnPagosConductor.class);
        Call<ResponseBody> call = service.registrar_tikect(idEntidadRegistro, numero, idUsuario, idDeuda, idEntidad, descuento);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String dato = response.body().string();
                        Log.e(TAG, "onResponse: " + dato);
                        onComunicacionRegistroPago.respuestaRegistrarTicket(dato);
                    } catch (IOException e) {
                        e.printStackTrace();
                        onComunicacionRegistroPago.respuestaRegistrarTicket(null);
                    }
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionRegistroPago.respuestaRegistrarTicket(null);
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onComunicacionRegistroPago.respuestaRegistrarTicket(null);
            }
        });
    }

    public void uploadImagen(String fileUri, final String nameArchivo) {
        File file = new File(fileUri);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("img", nameArchivo, reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), nameArchivo);
        ApiKtaxi.OnPagosConductor service = retrofit.create(ApiKtaxi.OnPagosConductor.class);
        final Call<okhttp3.ResponseBody> req = service.subir_tikect(body, name);
        req.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String dato = response.body().string();
                        Log.e(TAG, "onResponse: " + dato);
                        onComunicacionRegistroPago.respuestaEnviarImagen(dato, nameArchivo);
                    } catch (IOException e) {
                        e.printStackTrace();
                        onComunicacionRegistroPago.respuestaEnviarImagen(null, nameArchivo);
                    }
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionRegistroPago.respuestaEnviarImagen(null, nameArchivo);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionRegistroPago.respuestaEnviarImagen(null, nameArchivo);
            }
        });
    }

}
