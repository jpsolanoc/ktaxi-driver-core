package com.kradac.conductor.presentador;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.kradac.conductor.extras.ExtraLog;
import com.kradac.conductor.extras.MetodosValidacion;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionMapa;
import com.kradac.conductor.modelo.DatosLogin;
import com.kradac.conductor.modelo.DatosNotificacion;
import com.kradac.conductor.modelo.DatosTaximetro;
import com.kradac.conductor.modelo.DatosTaximetroServicioActivo;
import com.kradac.conductor.modelo.DatosTrafico;
import com.kradac.conductor.modelo.PositionTrafico;
import com.kradac.conductor.modelo.ValoracionConductor;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by John on 12/10/2016.
 */

public class MapaPresenter extends Presenter {

    private static final String TAG = MapaPresenter.class.getName();
    private OnComunicacionMapa MapaListener;
    private Context context;

    public MapaPresenter(OnComunicacionMapa MapaListener, Context context) {
        this.MapaListener = MapaListener;
        this.context = context;
    }


    public void obtenerTrafico(PositionTrafico position) {
        ApiKtaxi.Trafico service = retrofit.create(ApiKtaxi.Trafico.class);
        Call<DatosTrafico> call = service.obtenerTrafico(position.getLt(), position.getLg());
        call.enqueue(new Callback<DatosTrafico>() {
            @Override
            public void onResponse(Call<DatosTrafico> call, Response<DatosTrafico> response) {
                DatosTrafico responseApi = response.body();
                if (responseApi != null) {
                    if (responseApi.getEn() == 1) {
                        MapaListener.onResponse(responseApi.getT());
                    } else {
                        if (responseApi.getEn() == -1) {
                            MapaListener.error(responseApi.getM());
                        }
                    }
                } else {
                    MapaListener.error("error al obtener trafico.");
                }

            }

            @Override
            public void onFailure(Call<DatosTrafico> call, Throwable t) {
                MapaListener.error("error al obtener trafico.");
            }
        });
    }

    public void obtenerValoracionConductor(int idUsuario) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnValoracionConductor service = retrofit.create(ApiKtaxi.OnValoracionConductor.class);
        Call<ValoracionConductor> call = service.obtenerValoracionConductor(idUsuario);
        call.enqueue(new Callback<ValoracionConductor>() {
            @Override
            public void onResponse(Call<ValoracionConductor> call, Response<ValoracionConductor> response) {
                ValoracionConductor valoracionConductor = response.body();
                MapaListener.calificacionCliente(valoracionConductor);
            }

            @Override
            public void onFailure(Call<ValoracionConductor> call, Throwable t) {
                MapaListener.errorCalificacionCliente("No se pudo cargar la valoración");
            }
        });
    }


    public void obtenerCerrarSesion(int idUsuario, final boolean isDeconectar) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnCerrarSesion service = retrofit.create(ApiKtaxi.OnCerrarSesion.class);
        Call<DatosLogin.CerrarSesion> call = service.cerrarSesion(idUsuario, VariablesGlobales.NUM_ID_APLICATIVO);
        call.enqueue(new Callback<DatosLogin.CerrarSesion>() {
            @Override
            public void onResponse(Call<DatosLogin.CerrarSesion> call, Response<DatosLogin.CerrarSesion> response) {
                DatosLogin.CerrarSesion cerrarSesion = response.body();
                if (cerrarSesion != null) {
                    if (!isDeconectar) {
                        MapaListener.cerrarSesion(cerrarSesion);
                    }
                    ExtraLog.Log(TAG, "onResponse: " + cerrarSesion.toString());
                }
            }

            @Override
            public void onFailure(Call<DatosLogin.CerrarSesion> call, Throwable t) {
                MapaListener.errorCerrarSesion("No se puede cerrar Sesion");
                ExtraLog.Log(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private boolean isPushActivo() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(VariablesGlobales.SP_REGISTRAR_PUSH, context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(VariablesGlobales.SP_DATA_REGISTRAR_PUSH, false);
    }

    public void activarDesactivarPush(int idUsuario, String idDispositivo,
                                      int idEmpresa, int idCiudad, int tipo, boolean evaluar) {

        if (evaluar && !isPushActivo()) return;
        ApiKtaxi.OnRegistrarTokenPush service = retrofit.create(ApiKtaxi.OnRegistrarTokenPush.class);
        Call<ResponseBody> call = service.activar_desactivar_push(idUsuario, idDispositivo, idEmpresa, idCiudad, tipo);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == HttpsURLConnection.HTTP_OK) {
                    try {
                        ExtraLog.Log(TAG, "onResponse: " + response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    public void obtenerCambioClave(int idUsuario, final String claveAnterior, String claveNueva) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        MetodosValidacion metodosValidacion = new MetodosValidacion();
        String passAnterior = metodosValidacion.MD5(claveAnterior);
        String passNueva = metodosValidacion.MD5(claveNueva);
        ApiKtaxi.OnCambioClave service = retrofit.create(ApiKtaxi.OnCambioClave.class);
        Call<DatosLogin.CambioClave> call = service.cambioClave(idUsuario, passAnterior, passNueva, VariablesGlobales.NUM_ID_APLICATIVO);
        call.enqueue(new Callback<DatosLogin.CambioClave>() {
            @Override
            public void onResponse(Call<DatosLogin.CambioClave> call, Response<DatosLogin.CambioClave> response) {
                DatosLogin.CambioClave cambioClave = response.body();
                MapaListener.cambioClave(cambioClave);
            }

            @Override
            public void onFailure(Call<DatosLogin.CambioClave> call, Throwable t) {
                MapaListener.errorCambioClave("No se puede cambiar la clave, intente nuevamente.");
            }
        });
    }


    public void obtenerDineroElectronico(double monto, String cedula, int idSolicitud, int idUsuario, String numero) {
//        String ip = "http://" + VariablesPublicidad.IP_CONFIG_DINERO_ELETRONICO + "/";
//        cambioIpPresenter(ip);
//        ApiKtaxi.OnDineroElectronico service = retrofit.create(ApiKtaxi.OnDineroElectronico.class);
//        Call<DineroElectronico> call = service.dineroElectronico(monto,cedula,idSolicitud,idUsuario,numero,VariablesPublicidad.NUM_ID_APLICATIVO);
//        call.enqueue(new Callback<DineroElectronico>() {
//            @Override
//            public void onResponse(Call<DineroElectronico> call, Response<DineroElectronico> response) {
//                DineroElectronico dineroElectronico = response.body();
//                MapaListener.dineroElectronico(dineroElectronico);
//                cambioIpPresenter(VariablesPublicidad.IP_SERVICIOS_SOCKET);
//            }
//
//            @Override
//            public void onFailure(Call<DineroElectronico> call, Throwable t) {
//                MapaListener.dineroElectronico(null);
//                cambioIpPresenter(VariablesPublicidad.IP_SERVICIOS_SOCKET);
//            }
//        });
    }


    public void obtenerBoletin(int idUsuario, int idCiudad) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnBoletin service = retrofit.create(ApiKtaxi.OnBoletin.class);
        Call<DatosNotificacion> call = service.obtenerBoletin(idUsuario, idCiudad);
        call.enqueue(new Callback<DatosNotificacion>() {
            @Override
            public void onResponse(Call<DatosNotificacion> call, Response<DatosNotificacion> response) {
                if (response.code() == 200) {
                    MapaListener.boletinNuevo(response.body());
                } else {
                    MapaListener.boletinNuevo(null);
                }
            }

            @Override
            public void onFailure(Call<DatosNotificacion> call, Throwable t) {
                MapaListener.boletinNuevo(null);
                ExtraLog.Log(TAG, "onResponse: " + t);
            }
        });
    }

    public void responderBoletin(int idUsuario,
                                 int idBoletin,
                                 int idBoletinPregunta,
                                 String respuesta) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnBoletin service = retrofit.create(ApiKtaxi.OnBoletin.class);
        Call<ResponseBody> call = service.responder_boletin(idUsuario, idBoletin, idBoletinPregunta, respuesta);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        MapaListener.respuestaResponderBoletin(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    MapaListener.respuestaResponderBoletin(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                MapaListener.respuestaResponderBoletin(null);
                ExtraLog.Log(TAG, "onResponse: " + t);
            }
        });
    }

    public void getConfiguracionTaximetro(int idCiudad) {
        ApiKtaxi.ConfiguracionIncial service = retrofit.create(ApiKtaxi.ConfiguracionIncial.class);
        Call<DatosTaximetro> call = service.getConfiguracionTaximetro(idCiudad, VariablesGlobales.NUM_ID_APLICATIVO);
        call.enqueue(new Callback<DatosTaximetro>() {
            @Override
            public void onResponse(Call<DatosTaximetro> call, Response<DatosTaximetro> response) {
                if (response.code() == 200) {
                    DatosTaximetro datosTaximetro = response.body();
                    Log.e("pruebaTaxi", datosTaximetro.toString());
                    if (datosTaximetro != null) {
                        if (datosTaximetro.getEn() == 1) {
                            datosTaximetro.setTaximetro(true);
                            DatosTaximetro.guardarDatosTaximetro(context, datosTaximetro.serializeDatosTaximetro());
                        } else if (datosTaximetro.getEn() == 1) {
                            DatosTaximetro.guardarDatosTaximetro(context, null);
                        } else {
                            DatosTaximetro.guardarDatosTaximetro(context, null);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<DatosTaximetro> call, Throwable t) {
                DatosTaximetro.guardarDatosTaximetro(context, null);
            }
        });
    }


    public void getConfiguracionTaximetroServicioActivo(int idCiudad, int idServicioActivo) {
        ApiKtaxi.ConfiguracionIncial service = retrofit.create(ApiKtaxi.ConfiguracionIncial.class);
        Call<DatosTaximetroServicioActivo> call = service.getConfiguracionTaximetroServicoActivo(idCiudad, VariablesGlobales.NUM_ID_APLICATIVO, idServicioActivo);
        call.enqueue(new Callback<DatosTaximetroServicioActivo>() {
            @Override
            public void onResponse(Call<DatosTaximetroServicioActivo> call, Response<DatosTaximetroServicioActivo> response) {
                if (response.code() == 200) {
                    DatosTaximetroServicioActivo datosTaximetroServicioActivo = response.body();
                    Log.e("pruebaTaxis", datosTaximetroServicioActivo.toString());
                    if (datosTaximetroServicioActivo != null) {
                        if (datosTaximetroServicioActivo.getEn() == 1) {
                            datosTaximetroServicioActivo.setTaximetro(true);
                            DatosTaximetroServicioActivo.guardarDatosTaximetro(context, datosTaximetroServicioActivo.serializeDatosTaximetro());
                        } else if (datosTaximetroServicioActivo.getEn() == 1) {
                            DatosTaximetroServicioActivo.guardarDatosTaximetro(context, null);
                        } else {
                            DatosTaximetroServicioActivo.guardarDatosTaximetro(context, null);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<DatosTaximetroServicioActivo> call, Throwable t) {
                DatosTaximetroServicioActivo.guardarDatosTaximetro(context, null);
            }
        });
    }

    public void iniciarTaximetro(
            int tipo,
            int idSolicitud,
            double latitud,
            double longitud,
            int idUsuario,
            int idVehiculo) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnTaximetroDatos service = retrofit.create(ApiKtaxi.OnTaximetroDatos.class);
        Call<ResponseBody> call = service.iniciarTaximetro(tipo, idSolicitud, latitud, longitud, idUsuario, idVehiculo);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.code() == 200) {
                        String data = response.body().string();
                        if (data != null) {
                            JSONObject object = new JSONObject(data);
                            if (object.has("en")) {
                                if (object.getInt("en") == 1) {
                                    SharedPreferences spEstadobtn = context.getSharedPreferences(VariablesGlobales.CONFIG_ESTADO_BTN, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = spEstadobtn.edit();
                                    editor.putInt(VariablesGlobales.PARAMETRO_TAXIMETRO_TEMPORAL, object.getInt("idTaximetro"));
                                    editor.apply();
                                }
                            }
                        }
                    }

                } catch (IOException | JSONException | NullPointerException e) {
                    ExtraLog.Log(TAG, "onResponse: " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ExtraLog.Log(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    public void finalizarTaximetro(
            int idTaximetro,
            int idSolicitud,
            double latitudFin,
            double longitudFin,
            double costoTaximetro,
            double costroCobro,
            String horaInicio,
            String horaFin,
            String tarifaArranque,
            String tiempoTotal,
            String tiempoEspera,
            String valorTiempo,
            String distanciaRecorrida,
            String valorDistancia) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnTaximetroDatos service = retrofit.create(ApiKtaxi.OnTaximetroDatos.class);
        Call<ResponseBody> call = service.finalizarTaximetro(idTaximetro, idSolicitud, latitudFin, longitudFin, costoTaximetro, costroCobro, horaInicio,
                horaFin, tarifaArranque, tiempoTotal, tiempoEspera, valorTiempo, distanciaRecorrida, valorDistancia);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

}




