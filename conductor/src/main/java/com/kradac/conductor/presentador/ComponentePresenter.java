package com.kradac.conductor.presentador;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.modelo.ComponentePublicitario;
import com.kradac.conductor.modelo.ResponseApiComponente;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by John on 02/01/2017.
 */

public class ComponentePresenter extends Presenter {

    public interface ComponentePublicitarioListener{
        void error(String error);
        void response(ComponentePublicitario componentePublicitario);
    }
    public void obtenerComponetesPublicitariosCiudad(int idCiudad, final ComponentePublicitarioListener componentePublicitarioListener){
        ApiKtaxi.ComponentesPublicitarios service = retrofit.create(ApiKtaxi.ComponentesPublicitarios.class);
        Call<ComponentePublicitario> call = service.obtenerComponentesPublicitarioCiudad( VariablesGlobales.NUM_ID_APLICATIVO,idCiudad);
        call.enqueue(new Callback<ComponentePublicitario>() {

            @Override
            public void onResponse(Call<ComponentePublicitario> call, Response<ComponentePublicitario> response) {
                ComponentePublicitario componentePublicitario = response.body();
                if(componentePublicitario != null) {
                    componentePublicitarioListener.response(componentePublicitario);
                } else {
                    componentePublicitarioListener.error("no se logró obtener los eventos correctamente.");
                }
            }

            @Override
            public void onFailure(Call<ComponentePublicitario> call, Throwable t) {
                componentePublicitarioListener.error("no se logró obtener los eventos correctamente.");
            }
        });
    }


    public interface ObtenerComponentesListener{
        void error(String error);
        void response(RespuestaComponente respuestaComponente);
    }

    public void obtenerComponentes(int idCliente, int idCiudad,int tipo,int desde,int cuantos, final ObtenerComponentesListener obtenerComponentesListener){
        ApiKtaxi.ComponentesPublicitarios service = retrofit.create(ApiKtaxi.ComponentesPublicitarios.class);
        Call<RespuestaComponente> call = service.obtenerComponentes(VariablesGlobales.NUM_ID_APLICATIVO,idCiudad,idCliente,tipo,desde,cuantos);
        call.enqueue(new Callback<RespuestaComponente>() {

            @Override
            public void onResponse(Call<RespuestaComponente> call, Response<RespuestaComponente> response) {
                RespuestaComponente respuestaComponente = response.body();
                if(respuestaComponente  != null) {
                    obtenerComponentesListener.response(respuestaComponente);
                } else {
                    obtenerComponentesListener.error("no se logró obtener los eventos correctamente.");
                }
            }

            @Override
            public void onFailure(Call<RespuestaComponente> call, Throwable t) {
                obtenerComponentesListener.error("no se logró obtener los eventos correctamente.");
            }
        });
    }


    public interface ComponenteListener{
        void error(String error);
        void response(ResponseApiComponente responseApi);
    }

    public void marcarComponenteComoLeido(int idCliente,int tipo, int tipoLectura,String listaComponentes, final ComponenteListener eventoListener){
        ApiKtaxi.ComponentesPublicitarios service = retrofit.create(ApiKtaxi.ComponentesPublicitarios.class);
        Call<ResponseApiComponente> call = service.marcarComponenteComoVisto(idCliente,tipo,tipoLectura,listaComponentes);
        call.enqueue(new Callback<ResponseApiComponente>() {

            @Override
            public void onResponse(Call<ResponseApiComponente> call, Response<ResponseApiComponente> response) {
                ResponseApiComponente responseApi = response.body();
                if(responseApi != null) {
                    eventoListener.response(responseApi);
                } else {
                    eventoListener.error("no se logró obtener los eventos correctamente.");
                }
            }

            @Override
            public void onFailure(Call<ResponseApiComponente> call, Throwable t) {
                eventoListener.error("no se logró obtener los eventos correctamente.");
            }
        });
    }

}
