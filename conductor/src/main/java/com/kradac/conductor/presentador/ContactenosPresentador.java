package com.kradac.conductor.presentador;

import android.content.Context;
import android.util.Log;

import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionContactenos;
import com.kradac.conductor.modelo.RespuestaContactenos;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by John on 30/11/2016.
 */

public class ContactenosPresentador extends Presenter {

    private OnComunicacionContactenos contactenosListenes;
    private Utilidades utilidades;
    private Context context;
    private final String TAG = ContactenosPresentador.class.getName();

    public ContactenosPresentador(OnComunicacionContactenos contactenosListenes, Context context) {
        this.contactenosListenes = contactenosListenes;
        utilidades = new Utilidades();
        this.context = context;
    }


    public void enviarContactenosV2(int tipo, String nombre, String correo, String telefono, String motivo, String mensaje,
                                    double latitud, double longitud, String codigoPais, int idCiudad) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnContactenos service = retrofit.create(ApiKtaxi.OnContactenos.class);
        Call<RespuestaContactenos> call = service.enviar_contactenos(tipo, VariablesGlobales.NUM_ID_APLICATIVO,
                0, 0, 0, VariablesGlobales.ID_PLATAFORMA, 0, nombre, correo,
                telefono, motivo, mensaje, latitud, longitud, utilidades.obtenerVersion(context),
                utilidades.obtenerVersionSo(), utilidades.getDeviceName(), utilidades.obtenerModelo(),
                utilidades.getTipoRed(context), utilidades.statusGPS(context), utilidades.getConnectivityStatus(context),
                utilidades.getNameOperator(context), utilidades.getImei(context), 2, codigoPais, idCiudad);
        call.enqueue(new Callback<RespuestaContactenos>() {
            @Override
            public void onResponse(Call<RespuestaContactenos> call, Response<RespuestaContactenos> response) {
                if (response.code() == HttpsURLConnection.HTTP_OK) {
                    contactenosListenes.respuestaServidor(response.body());
                } else {
                    contactenosListenes.respuestaServidor(null);
                }
            }

            @Override
            public void onFailure(Call<RespuestaContactenos> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                contactenosListenes.respuestaServidor(null);
            }
        });
    }

    public void enviarContactenosV2Pre(int tipo, String nombre, String correo, String telefono, String motivo, String mensaje,
                                       double latitud, double longitud, String codigoPais, String codigoPaisSeleccionado, int idCiudad, String pais, String ciudad, int idReferido) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnContactenos service = retrofit.create(ApiKtaxi.OnContactenos.class);
        Call<RespuestaContactenos> call = service.enviar_contactenos_pre_registro(tipo, VariablesGlobales.NUM_ID_APLICATIVO,
                0, 0, 0, VariablesGlobales.ID_PLATAFORMA, 0, nombre, correo,
                telefono, motivo, mensaje, latitud, longitud, utilidades.obtenerVersion(context),
                utilidades.obtenerVersionSo(), utilidades.getDeviceName(), utilidades.obtenerModelo(),
                utilidades.getTipoRed(context), utilidades.statusGPS(context), utilidades.getConnectivityStatus(context),
                utilidades.getNameOperator(context), utilidades.getImei(context), 2, codigoPais, codigoPaisSeleccionado, idCiudad, pais, ciudad,idReferido);
        call.enqueue(new Callback<RespuestaContactenos>() {
            @Override
            public void onResponse(Call<RespuestaContactenos> call, Response<RespuestaContactenos> response) {
                if (response.code() == HttpsURLConnection.HTTP_OK) {
                    contactenosListenes.respuestaServidor(response.body());
                } else {
                    contactenosListenes.respuestaServidor(null);
                }
            }

            @Override
            public void onFailure(Call<RespuestaContactenos> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                contactenosListenes.respuestaServidor(null);
            }
        });
    }


}
