package com.kradac.conductor.presentador;

import android.content.Context;
import android.util.Log;

import com.kradac.conductor.extras.ExtraLog;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionIniciarSesion;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by John on 29/11/2016.
 */

public class IniciarSesionPresentador extends Presenter {

    private static final String TAG = IniciarSesionPresentador.class.getName();
    private OnComunicacionIniciarSesion iniciarSesionListener;
    private Utilidades utilidades;

    public IniciarSesionPresentador(OnComunicacionIniciarSesion iniciarSesionListener) {
        this.iniciarSesionListener = iniciarSesionListener;
        utilidades = new Utilidades();
    }

    public void enviarMetaDataServer(Context context, int idUsuario) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnEnviarMetaData service = retrofit.create(ApiKtaxi.OnEnviarMetaData.class);
        Call<ResponseBody> call = service.enviarMetaData(1, utilidades.obtenerVersion(context), utilidades.obtenerVersionSo(),
                utilidades.getDeviceName(), utilidades.modeloDispositivo(), idUsuario);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    ExtraLog.Log(TAG, "onResponse: " + response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

//    public void obtenerConfiguracionServer(int numero) {
//        cambioIpPresenter("http://174.36.22.14:8080/");
//        final ApiKtaxi.OnConfiguracionServer service = retrofit.create(ApiKtaxi.OnConfiguracionServer.class);
//        Call<ConfiguracionDesdeServidor> call = service.configuracionServidor(numero);
//        call.enqueue(new Callback<ConfiguracionDesdeServidor>() {
//            @Override
//            public void onResponse(Call<ConfiguracionDesdeServidor> call, Response<ConfiguracionDesdeServidor> response) {
//                ConfiguracionDesdeServidor servidor = response.body();
//                iniciarSesionListener.configuracionServidor(servidor);
//                cambioIpPresenter(VariablesPublicidad.IP_SERVICIOS_SOCKET);
//            }
//
//            @Override
//            public void onFailure(Call<ConfiguracionDesdeServidor> call, Throwable t) {
//                cambioIpPresenter(VariablesPublicidad.IP_SERVICIOS_SOCKET);
//                iniciarSesionListener.configuracionServidor(null);
//            }
//        });
//    }





    public void enviarValidarImei(Context context, int idUsuario, double latitud, double longitud) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ExtraLog.Log(TAG, "enviarValidarImei: " + VariablesGlobales.IP_SERVICIOS_SOCKET );
        ApiKtaxi.OnValidarImeiConductor service = retrofit.create(ApiKtaxi.OnValidarImeiConductor.class);
        Call<ResponseBody> call = service.validarImei(idUsuario, utilidades.getImei(context),
                utilidades.getDeviceName(), utilidades.modeloDispositivo(), utilidades.obtenerVersionSo(), utilidades.obtenerVersion(context), latitud, longitud);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.code()==200){
                    try {
                        String data = response.body().string();
                        iniciarSesionListener.respuestaRegistrarImei(data);
                    } catch (IOException e) {
                        e.printStackTrace();
                        iniciarSesionListener.respuestaRegistrarImei(null);
                    }
                }else{
                    ExtraLog.Log(TAG, "onResponse Code: " + response.code() );
                    iniciarSesionListener.respuestaRegistrarImei(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                iniciarSesionListener.respuestaRegistrarImei(null);
            }
        });
    }


}


