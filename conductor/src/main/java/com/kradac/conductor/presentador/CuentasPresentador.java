package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionCuentas;
import com.kradac.conductor.modelo.Cuenta;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * Created by John on 04/01/2017.
 */

public class CuentasPresentador extends Presenter {
    private static final String TAG = CuentasPresentador.class.getName();
    OnComunicacionCuentas comunicacionCuentas;

    public CuentasPresentador(OnComunicacionCuentas comunicacionCuentas) {
        this.comunicacionCuentas = comunicacionCuentas;
    }

    public void obtenerCuentas(int idVehiculo,int idUsuario,int anio,int mes){
        Log.e(TAG, "obtenerCuentas: ");
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.ComponentesContables service = retrofit.create(ApiKtaxi.ComponentesContables.class);
        Call<Cuenta> call = service.historialObtener(idVehiculo,idUsuario,anio,mes);
        call.enqueue(new Callback<Cuenta>() {
            @Override
            public void onResponse(Call<Cuenta> call, Response<Cuenta> response) {
                if (response.code()== HttpsURLConnection.HTTP_OK){
                    comunicacionCuentas.responseCuentas(response.body());
                }else{
                    comunicacionCuentas.responseCuentas(null);
                }
            }

            @Override
            public void onFailure(Call<Cuenta> call, Throwable t) {
                comunicacionCuentas.responseCuentas(null);
            }
        });
    }

}
