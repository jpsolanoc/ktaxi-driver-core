package com.kradac.conductor.presentador;

import android.content.Context;
import android.util.Log;

import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.wigets.modelo.RespuestaPublicidad;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PublicidadPresenter extends Presenter {

    private static final String TAG = PublicidadPresenter.class.getName();

    public interface OnComunicacionComponentePublicitario {
        void respuesta(RespuestaPublicidad respuestaPublicidad);
    }

    public void getPublicidad(int idAplicativo, int idCiudad, int estado, int idSolicitud, int idVehiculo,
                              int idUsuario, final OnComunicacionComponentePublicitario onComponentesPublicitarios, final Context context) {
        Log.e(TAG, "getPublicidad: " + idAplicativo + "  . : ." + idCiudad + "  . : ." + estado + "  . : ." + idSolicitud + "  . : ." + idVehiculo
                + "  . : ." + idUsuario);
        ApiKtaxi.OnComponentesPublicitarios service = retrofit.create(ApiKtaxi.OnComponentesPublicitarios.class);
        Call<RespuestaPublicidad> call = service.getPublicidad(idAplicativo, idCiudad, estado, idSolicitud, idVehiculo, idUsuario);
        call.enqueue(new Callback<RespuestaPublicidad>() {
            @Override
            public void onResponse(Call<RespuestaPublicidad> call, Response<RespuestaPublicidad> response) {
                if (response.code() == 200) {
                    RespuestaPublicidad respuestaPublicidad = response.body();
                    String dataPublicidad = RespuestaPublicidad.jsonRespuestaPublicidad(respuestaPublicidad);
                    RespuestaPublicidad.guardarRespuestaPublicidad(dataPublicidad, context);
                    onComponentesPublicitarios.respuesta(respuestaPublicidad);
                } else {
                    RespuestaPublicidad.guardarRespuestaPublicidad("", context);
                    onComponentesPublicitarios.respuesta(null);
                }
            }

            @Override
            public void onFailure(Call<RespuestaPublicidad> call, Throwable t) {
                RespuestaPublicidad.guardarRespuestaPublicidad("", context);
                onComponentesPublicitarios.respuesta(null);
                Log.e(TAG, "onFailure: ", t);

            }
        });
    }

}
