package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionComentarios;
import com.kradac.conductor.modelo.RespuestaComentarios;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ing.John Patricio Solano Cabrera on 12/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class PresentadorComentarios extends Presenter {

    private static final String TAG = PresentadorComentarios.class.getName();
    OnComunicacionComentarios onComunicacionComentarios;

    public PresentadorComentarios(OnComunicacionComentarios onComunicacionComentarios) {
        this.onComunicacionComentarios = onComunicacionComentarios;
    }

    public void consularMensajesCliente(int idUsuario) {
        ApiKtaxi.Notificacion service = retrofit.create(ApiKtaxi.Notificacion.class);
        Call<RespuestaComentarios> call = service.getObservaciones(idUsuario);
        call.enqueue(new Callback<RespuestaComentarios>() {
            @Override
            public void onResponse(Call<RespuestaComentarios> call, Response<RespuestaComentarios> response) {
                if (response.code() == 200) {
                    onComunicacionComentarios.RespuestaObtenerComentarios(response.body());
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionComentarios.RespuestaObtenerComentarios(null);
                }
            }

            @Override
            public void onFailure(Call<RespuestaComentarios> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionComentarios.RespuestaObtenerComentarios(null);
            }
        });
    }
}
