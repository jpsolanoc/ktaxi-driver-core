package com.kradac.conductor.presentador;


import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.modelo.ResponseApiCorto;
import com.kradac.conductor.modelo.ResponseGenerarToken;
import com.kradac.conductor.modelo.ResponseListarTarjetaCredito;


import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreditCardRequest extends Presenter {


    public void solicitarToken(
            int idCliente,
            String imei,
            String correo,
            String celular,
            String nombres,
            String apellidos,
            String token,
            String key,
            String timeStanD,
            final CreditCardListener creditCardListener
    ) {

        ApiKtaxi.ICard service = retrofit.create(ApiKtaxi.ICard.class);
        Call<ResponseGenerarToken> call = service.solicitarToken(idCliente, imei, correo, celular, nombres, apellidos, token, key, timeStanD, VariablesGlobales.NUM_ID_APLICATIVO);

        call.enqueue(new Callback<ResponseGenerarToken>() {

            @Override
            public void onResponse(Call<ResponseGenerarToken> call, Response<ResponseGenerarToken> response) {
                ResponseGenerarToken responseApi = response.body();
                if (responseApi != null) {
                    if (responseApi.getEn() == 1) {
                        creditCardListener.onResponse(responseApi);
                    } else {
                        creditCardListener.error(responseApi.getM());
                    }
                } else {
                    creditCardListener.error("Lo sentimos ha ocurrido un problema, intenta mas tarde.");
                }
            }

            @Override
            public void onFailure(Call<ResponseGenerarToken> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void registrarCard(
            int idCliente,
            String imei,
            String con,
            String verificationCode,
            String number,
            int type,
            String expirationMonth,
            String expirationYear,
            String email,
            String phoneNumber,
            String token,
            String key,
            String timeStanD
            , final CreditCardRegisterListener creditCardRegisterListener
    ) {


        ApiKtaxi.ICard service = retrofit.create(ApiKtaxi.ICard.class);
        Call<ResponseApiCorto> call = service.registrarCard(idCliente, imei, con, verificationCode, number, type, expirationMonth
                , expirationYear, email, phoneNumber, token, key, timeStanD, VariablesGlobales.NUM_ID_APLICATIVO);

        call.enqueue(new Callback<ResponseApiCorto>() {

            @Override
            public void onResponse(Call<ResponseApiCorto> call, Response<ResponseApiCorto> response) {
                ResponseApiCorto responseApi = response.body();

                if (responseApi != null) {
                    if (responseApi.getEn() == HttpURLConnection.HTTP_OK) {
                        creditCardRegisterListener.onResponse(responseApi);
                    } else {
                        creditCardRegisterListener.error(responseApi.getM());
                    }
                } else {
                    creditCardRegisterListener.error("Lo sentimos ha ocurrido un problema, intenta mas tarde");
                }
            }

            @Override
            public void onFailure(Call<ResponseApiCorto> call, Throwable t) {
                creditCardRegisterListener.error("Lo sentimos ha ocurrido un problema, intenta mas tarde");
            }
        });
    }


    public void listarCard(
            int idCliente,
            String imei,
            String con,
            String token,
            String key,
            String timeStanD
            , final ListCardListener listCardListener
    ) {


        ApiKtaxi.ICard service = retrofit.create(ApiKtaxi.ICard.class);
        Call<ResponseListarTarjetaCredito> call = service.listarCard(idCliente, imei, con, token, key, timeStanD, VariablesGlobales.NUM_ID_APLICATIVO);

        call.enqueue(new Callback<ResponseListarTarjetaCredito>() {

            @Override
            public void onResponse(Call<ResponseListarTarjetaCredito> call, Response<ResponseListarTarjetaCredito> response) {
                ResponseListarTarjetaCredito responseApi = response.body();
                if (responseApi != null) {
                    if (responseApi.getEn() == HttpURLConnection.HTTP_OK) {
                        listCardListener.onResponse(responseApi);
                    } else {
                        listCardListener.error(responseApi.getM());
                    }
                } else {
                    listCardListener.error("Lo sentimos ha ocurrido un problema, intenta mas tarde");
                }
            }

            @Override
            public void onFailure(Call<ResponseListarTarjetaCredito> call, Throwable t) {
                listCardListener.error("Lo sentimos ha ocurrido un problema, intenta mas tarde..");
            }
        });
    }

    public void eliminarTarjeta(
            int idCliente,
            String imei,
            String con,
            String numberCar,
            String conCar,
            String token,
            String key,
            String timeStanD
            , final EliminarCardListener eliminarCardListener
    ) {


        ApiKtaxi.ICard service = retrofit.create(ApiKtaxi.ICard.class);
        Call<ResponseApiCorto> call = service.eliminarTarjeta(idCliente, imei, con, numberCar, conCar, token, key, timeStanD, VariablesGlobales.NUM_ID_APLICATIVO);

        call.enqueue(new Callback<ResponseApiCorto>() {

            @Override
            public void onResponse(Call<ResponseApiCorto> call, Response<ResponseApiCorto> response) {

                ResponseApiCorto responseApi = response.body();
                if (responseApi != null) {
                    if (responseApi.getEn() == HttpURLConnection.HTTP_OK) {
                        eliminarCardListener.onResponse(responseApi);
                    } else {
                        eliminarCardListener.error(responseApi.getM());
                    }
                } else {
                    eliminarCardListener.error("Lo sentimos ha ocurrido un problema, intenta mas tarde");
                }
            }

            @Override
            public void onFailure(Call<ResponseApiCorto> call, Throwable t) {
                eliminarCardListener.error("Lo sentimos ha ocurrido un problema, intenta mas tarde..");
            }
        });
    }


    public interface CreditCardListener {
        void onResponse(ResponseGenerarToken responseApi);

        void error(String error);
    }

    public interface CreditCardRegisterListener {
        void onResponse(ResponseApiCorto responseApi);

        void error(String error);
    }

    public interface ListCardListener {
        void onResponse(ResponseListarTarjetaCredito responseApi);

        void error(String error);
    }

    public interface EliminarCardListener {
        void onResponse(ResponseApiCorto responseApi);

        void error(String error);
    }
}
