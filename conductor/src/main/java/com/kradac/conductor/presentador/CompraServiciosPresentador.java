package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionCompraServicio;
import com.kradac.conductor.modelo.CompraServiciosCategorias;
import com.kradac.conductor.modelo.SaldoKtaxi;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ing.John Patricio Solano Cabrera on 8/1/2018.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class CompraServiciosPresentador extends Presenter {

    private static final String TAG = CompraServiciosPresentador.class.getName();
    OnComunicacionCompraServicio onComunicacionCompraServicio;

    public CompraServiciosPresentador(OnComunicacionCompraServicio onComunicacionCompraServicio) {
        this.onComunicacionCompraServicio = onComunicacionCompraServicio;
    }

    public void consultar_categorias(int idVehiculo,int idUsuario,int idCiudad){
        ApiKtaxi.ComponentesContables service = retrofit.create(ApiKtaxi.ComponentesContables.class);
        Call<CompraServiciosCategorias> call = service.consultar_categorias(idVehiculo,idUsuario,idCiudad);
        call.enqueue(new Callback<CompraServiciosCategorias>() {
            @Override
            public void onResponse(Call<CompraServiciosCategorias> call, Response<CompraServiciosCategorias> response) {
                if (response.code()==200){
                    onComunicacionCompraServicio.respuestaServidorCategorias(response.body());
                }else{
                    Log.e(TAG, "onResponse: " +response.code() );
                    onComunicacionCompraServicio.respuestaServidorCategorias(null);
                }
            }
            @Override
            public void onFailure(Call<CompraServiciosCategorias> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t );
                onComunicacionCompraServicio.respuestaServidorCategorias(null);
            }
        });
    }
}
