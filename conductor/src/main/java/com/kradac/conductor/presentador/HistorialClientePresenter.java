package com.kradac.conductor.presentador;


import com.kradac.conductor.modelo.ItemHistorialCompras;
import com.kradac.conductor.modelo.ItemHistorialSolicitud;

import java.util.List;

/**
 * Created by fabricio on 29/04/16.
 */
public class HistorialClientePresenter extends Presenter {

    private static final String TAG = HistorialClientePresenter.class.getName();

    public interface HistorialListener {
        void historialDescargados(List<ItemHistorialSolicitud> listaMensajes);
        void showRequestError(String error);
    }

    public interface HistorialListenerCompras {
        void historialDescargados(List<ItemHistorialCompras> listaMensajes);
        void showRequestError(String error);
    }

    public interface OnResponseNumeroHistorial {
        void numeroSolicitudesRealizadas(int numeroHistorial);
        void showRequestError(String error);
    }

    public interface OnResponseNumeroHistorialCompras {
        void numeroComprasRealizadas(int numeroHistorial);
        void showRequestError(String error);
    }

//    public void consularHistorialCliente(final int idUsuario,int anio,int mes,int tipo, int desde, int cuantos, final  HistorialListener historialListener) {
//        Log.e(TAG, "consularHistorialCliente: " + idUsuario +":"+ anio +":"+ mes +":"+ tipo +":" + desde +":"+ cuantos +":" );
//        ApiKtaxi.HistoialSolicitudesCliente service = retrofit.create(ApiKtaxi.HistoialSolicitudesCliente.class);
//        Call<List<ResponseBody> call = service.obtenerSolicitudes(idUsuario, anio, mes, tipo, desde, cuantos);
//        call.enqueue(new Callback<List<ItemHistorialSolicitud>>() {
//
//            @Override
//            public void onResponse(Call<List<ItemHistorialSolicitud>> call, Response<List<ItemHistorialSolicitud>> response) {
//                Log.e(TAG, "onResponse: " + response.body());
//                List<ItemHistorialSolicitud> listaDatosNotificacion = response.body();
//                if (listaDatosNotificacion != null) {
//                    historialListener.historialDescargados(listaDatosNotificacion);
//                }else{
//                    historialListener.showRequestError("No se logró obtener datos");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<ItemHistorialSolicitud>> call, Throwable t) {
//                t.printStackTrace();
//                Log.e(TAG, "onFailure:"+ t );
//                historialListener.showRequestError("No se logró obtener datos");
//            }
//        });
//    }

//    public void consularHistorialCliente(final int idUsuario,int anio,int mes,int tipo, int desde, int cuantos, final  HistorialListener historialListener) {
//        Log.e(TAG, "consularHistorialCliente: " + idUsuario +":"+ anio +":"+ mes +":"+ tipo +":" + desde +":"+ cuantos +":" );
//        ApiKtaxi.HistoialSolicitudesCliente service = retrofit.create(ApiKtaxi.HistoialSolicitudesCliente.class);
//        Call<ResponseBody> call = service.obtenerSolicitudes(idUsuario, anio, mes, tipo, desde, cuantos);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    Log.e(TAG, "onResponse: " + response.body().string());
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//            }
//        });
//    }


//    public void consularHistorialClienteCompras(final int idUsuario,int anio,int mes,int tipo, int desde, int cuantos, final  HistorialListenerCompras historialListenerCompras) {
//        Log.e(TAG, "consularHistorialClienteCompras: "+idUsuario+":"+anio+":"+mes+":"+tipo+":"+desde+":"+cuantos );
//        ApiKtaxi.HistoialSolicitudesCliente service = retrofit.create(ApiKtaxi.HistoialSolicitudesCliente.class);
//        Call<List<ItemHistorialCompras>> call = service.obtenerCompras(idUsuario,anio,mes,tipo, desde, cuantos);
//        call.enqueue(new Callback<List<ItemHistorialCompras>>() {
//
//            @Override
//            public void onResponse(Call<List<ItemHistorialCompras>> call, Response<List<ItemHistorialCompras>> response) {
//                Log.e(TAG, "onResponse: " + response.body());
//
//                List<ItemHistorialCompras> listaDatosNotificacion = response.body();
//                if (listaDatosNotificacion != null) {
//                    historialListenerCompras.historialDescargados(listaDatosNotificacion);
//                }else{
//                    historialListenerCompras.showRequestError("No se logró obtener datos");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<ItemHistorialCompras>> call, Throwable t) {
//                Log.e(TAG, "onFailure: ", t );
//                historialListenerCompras.showRequestError("No se logró obtener datos");
//            }
//        });
//    }


//    public void obtenerNumeroSolicitudes(final int idUsuario,int mes,int anio,int tipo,final OnResponseNumeroHistorial onResponseNumeroHistorial) {
//        ApiKtaxi.HistoialSolicitudesCliente service = retrofit.create(ApiKtaxi.HistoialSolicitudesCliente.class);
//        Log.e("IP", "obtenerNumeroSolicitudes: " + VariablesPublicidad.IP_SERVICIOS_SOCKET + "ktaxi-driver/historial/anio-mes/"+idUsuario+"/"+anio+"/"+mes+"/"+tipo);
//        Call<ResponseHistorial> call = service.obtenerCuantosHistorial(idUsuario,anio,mes,tipo);
//        call.enqueue(new Callback<ResponseHistorial>() {
//
//            @Override
//            public void onResponse(Call<ResponseHistorial> call, Response<ResponseHistorial> response) {
//                final ResponseHistorial datosNotificacion = response.body();
//                int nRegistrosServer;
//                if (datosNotificacion != null ) {
//                    nRegistrosServer = datosNotificacion.getRegistros();
//                    Log.e(TAG, "onResponse NumSolicitudes: " + nRegistrosServer );
//                    onResponseNumeroHistorial.numeroSolicitudesRealizadas(nRegistrosServer);
//                }else{
//                    Log.e(TAG, "onResponse: NULL" );
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseHistorial> call, Throwable t) {
//                if (onResponseNumeroHistorial != null) {
//                    onResponseNumeroHistorial.showRequestError("Error al obtener historial");
//                }
//                t.printStackTrace();
//                Log.e(TAG, "onFailure: ",t );
//            }
//        });
//    }

//    public void obtenerNumeroCompras(final int idUsuario,int mes,int anio,int tipo,final OnResponseNumeroHistorialCompras onResponseNumeroHistorial) {
//        ApiKtaxi.HistoialSolicitudesCliente service = retrofit.create(ApiKtaxi.HistoialSolicitudesCliente.class);
//        Call<ResponseHistorial> call = service.obtenerCuantosHistorialCompras(idUsuario,anio,mes,tipo);
//        call.enqueue(new Callback<ResponseHistorial>() {
//
//            @Override
//            public void onResponse(Call<ResponseHistorial> call, Response<ResponseHistorial> response) {
//                final ResponseHistorial datosNotificacion = response.body();
//                int nRegistrosServer;
//                if (datosNotificacion != null ) {
//                    nRegistrosServer = datosNotificacion.getRegistros();
//                    Log.e(TAG, "onResponse COMPRA: " + nRegistrosServer );
//                    onResponseNumeroHistorial.numeroComprasRealizadas(nRegistrosServer);
//                }else{
//                    Log.e(TAG, "onResponse COMPRA: NULL" );
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseHistorial> call, Throwable t) {
//                if (onResponseNumeroHistorial != null) {
//                    onResponseNumeroHistorial.showRequestError("Error al obtener historial");
//                }
//                t.printStackTrace();
//                Log.e(TAG, "onFailure COMPRA: ",t );
//            }
//        });
//    }


}
