package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionHistorial;
import com.kradac.conductor.modelo.ItemHistorialCompras;
import com.kradac.conductor.modelo.ItemHistorialSolicitud;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ing.John Patricio Solano Cabrera on 1/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class HistorialModificado extends Presenter {


    private static final String TAG = HistorialModificado.class.getName();
    private OnComunicacionHistorial onComunicacionHistorial;
    Call<ResponseBody> call;

    public HistorialModificado(OnComunicacionHistorial onComunicacionHistorial) {
        this.onComunicacionHistorial = onComunicacionHistorial;
    }

    public void cuantosPorAnioMes(int idUsuario, final int anio, final int mes, int tipo) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.HistoialSolicitudesCliente service = retrofit.create(ApiKtaxi.HistoialSolicitudesCliente.class);
        Call<ResponseBody> call = service.cuantosPorAnioMes(idUsuario, anio, mes, tipo);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject object = new JSONObject(response.body().string());
                        if (object.has("registros")) {
                            onComunicacionHistorial.respuestaCuantosPorAnioMes(object.getInt("registros"), anio, mes);
                        } else {
                            onComunicacionHistorial.respuestaCuantosPorAnioMes(-1, anio, mes);
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        onComunicacionHistorial.respuestaCuantosPorAnioMes(-1, anio, mes);
                    }
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionHistorial.respuestaCuantosPorAnioMes(-1, anio, mes);
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionHistorial.respuestaCuantosPorAnioMes(-1, anio, mes);
            }
        });
    }

    public void obtenerPorAnioMes(int idUsuario, final int anio, final int mes, int tipo, int desde, int cuantos) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.HistoialSolicitudesCliente service = retrofit.create(ApiKtaxi.HistoialSolicitudesCliente.class);
        Call<ArrayList<ItemHistorialSolicitud>> call = service.obtenerPorAnioMes(idUsuario, anio, mes, tipo, desde, cuantos);
        call.enqueue(new Callback<ArrayList<ItemHistorialSolicitud>>() {
            @Override
            public void onResponse(Call<ArrayList<ItemHistorialSolicitud>> call, Response<ArrayList<ItemHistorialSolicitud>> response) {
                if (response.code() == 200) {
                    ArrayList<ItemHistorialSolicitud> itemHistorial = response.body();
                    onComunicacionHistorial.respuestaObtenerPorAnioMes(itemHistorial, anio, mes);
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionHistorial.respuestaObtenerPorAnioMes(null, 0, 0);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ItemHistorialSolicitud>> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionHistorial.respuestaObtenerPorAnioMes(null, 0, 0);
            }
        });
    }

    public void obtenerCuantosTotal(int idUsuario) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnHistorial service = retrofit.create(ApiKtaxi.OnHistorial.class);
        Call<ResponseBody> call = service.historialCuantos(idUsuario);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject object = new JSONObject(response.body().string());
                        if (object.has("registros")) {
                            onComunicacionHistorial.totalHistoricoSolicitud(object.getInt("registros"));
                        } else {
                            onComunicacionHistorial.totalHistoricoSolicitud(-1);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        onComunicacionHistorial.totalHistoricoSolicitud(-1);
                    }
                } else {
                    onComunicacionHistorial.totalHistoricoSolicitud(-1);
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onComunicacionHistorial.totalHistoricoSolicitud(-1);
            }
        });
    }


    /**
     * COMPRAS HISTORIAL
     */


    public void cuantosPorAnioMesCompras(int idUsuario, final int anio, final int mes, int tipo) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.HistoialSolicitudesCliente service = retrofit.create(ApiKtaxi.HistoialSolicitudesCliente.class);
        Call<ResponseBody> call = service.obtenerCuantosHistorialCompras(idUsuario, anio, mes, tipo);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject object = new JSONObject(response.body().string());
                        if (object.has("registros")) {
                            onComunicacionHistorial.respuestaCuantosPorAnioMesCompras(object.getInt("registros"), anio, mes);
                        } else {
                            onComunicacionHistorial.respuestaCuantosPorAnioMesCompras(-1, anio, mes);
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        onComunicacionHistorial.respuestaCuantosPorAnioMesCompras(-1, anio, mes);
                    }
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionHistorial.respuestaCuantosPorAnioMesCompras(-1, anio, mes);
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionHistorial.respuestaCuantosPorAnioMes(-1, anio, mes);
            }
        });
    }

    public void obtenerPorAnioMesCompras(int idUsuario, final int anio, final int mes, int tipo, int desde, int cuantos) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.HistoialSolicitudesCliente service = retrofit.create(ApiKtaxi.HistoialSolicitudesCliente.class);
        Call<ArrayList<ItemHistorialCompras>> call = service.obtenerPorAnioMesCompras(idUsuario, anio, mes, tipo, desde, cuantos);
        call.enqueue(new Callback<ArrayList<ItemHistorialCompras>>() {
            @Override
            public void onResponse(Call<ArrayList<ItemHistorialCompras>> call, Response<ArrayList<ItemHistorialCompras>> response) {
                if (response.code() == 200) {
                    ArrayList<ItemHistorialCompras> itemHistorial = response.body();
                    onComunicacionHistorial.respuestaObtenerPorAnioMesCompras(itemHistorial, anio, mes);
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionHistorial.respuestaObtenerPorAnioMesCompras(null, 0, 0);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ItemHistorialCompras>> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionHistorial.respuestaObtenerPorAnioMesCompras(null, 0, 0);
            }
        });
    }

    public void obtenerCuantosTotalCompras(int idUsuario) {
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnHistorial service = retrofit.create(ApiKtaxi.OnHistorial.class);
        Call<ResponseBody> call = service.historialCuantosCompras(idUsuario);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject object = new JSONObject(response.body().string());
                        if (object.has("registros")) {
                            onComunicacionHistorial.totalHistoricoSolicitudCompras(object.getInt("registros"));
                        } else {
                            onComunicacionHistorial.totalHistoricoSolicitudCompras(-1);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        onComunicacionHistorial.totalHistoricoSolicitudCompras(-1);
                    }
                } else {
                    onComunicacionHistorial.totalHistoricoSolicitudCompras(-1);
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onComunicacionHistorial.totalHistoricoSolicitud(-1);
            }
        });
    }


}
