package com.kradac.conductor.presentador;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionIniciarSesion;
import com.kradac.conductor.interfaces.OnComunicacionMain;
import com.kradac.conductor.interfaces.OnComunicacionMapa;
import com.kradac.conductor.interfaces.OnComunicacionServicio;
import com.kradac.conductor.modelo.ConfiguracionServidor;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by John on 29/11/2016.
 */

public class MainPresentador extends Presenter {

    private static final String TAG = MainPresentador.class.getName();
    private OnComunicacionMain mainListener;
    private OnComunicacionIniciarSesion iniciarSesion;
    private OnComunicacionServicio comunicacionServicio;
    private OnComunicacionMapa comunicacionMapa;
    private SharedPreferences spConfigServerNew;
    private Context context;

    public MainPresentador(OnComunicacionMain mainListener, OnComunicacionIniciarSesion iniciarSesion, OnComunicacionMapa comunicacionMapa, Context context, OnComunicacionServicio comunicacionServicio) {
        this.mainListener = mainListener;
        this.iniciarSesion = iniciarSesion;
        this.comunicacionMapa = comunicacionMapa;
        this.comunicacionServicio = comunicacionServicio;
        this.context = context;
        spConfigServerNew = context.getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
    }

    public void obtenerConfiguracionServerNew(final String ip, int idAplicativo, int idPlataforma, int app, String version) {
        if (ip != null) {
            cambioIpPresenter(ip);
            VariablesGlobales.IP_SERVICIOS_SOCKET = ip;
        }
        final ApiKtaxi.OnConfiguracionServer service = retrofit.create(ApiKtaxi.OnConfiguracionServer.class);
        Call<ConfiguracionServidor> call = service.getConfiguracionesServer(idAplicativo, idPlataforma, app, version);
        call.enqueue(new Callback<ConfiguracionServidor>() {
            @Override
            public void onResponse(Call<ConfiguracionServidor> call, Response<ConfiguracionServidor> response) {
                if (response.code() == 200) {
                    ConfiguracionServidor configuracionServer = response.body();
                    configuracionServer.setIpUsada(ip);
                    SharedPreferences.Editor editor = spConfigServerNew.edit();
                    editor.putString(VariablesGlobales.CONFIG_IP_NEW, configuracionServer.serializeStringConfiguracionServer());
                    editor.apply();
                    if (mainListener != null) {
                        mainListener.configuracionServidorNew(true);
                    } else if (iniciarSesion != null) {
                        iniciarSesion.configuracionServidorNew(true);
                    } else if (comunicacionMapa != null) {
                        comunicacionMapa.configuracionServidorNew(true);
                    } else if (comunicacionServicio != null) {
                        comunicacionServicio.configuracionServidorNew(true);
                    }
                    ConfiguracionServidor.createConfiguracionServidor(spConfigServerNew.getString(VariablesGlobales.CONFIG_IP_NEW, VariablesGlobales.CONFIGURACION_INICIAL));
                } else {
                    if (mainListener != null) {
                        mainListener.configuracionServidorNew(false);
                    } else if (iniciarSesion != null) {
                        iniciarSesion.configuracionServidorNew(false);
                    } else if (comunicacionMapa != null) {
                        comunicacionMapa.configuracionServidorNew(false);
                    } else if (comunicacionServicio != null) {
                        comunicacionServicio.configuracionServidorNew(false);
                    }
                    Log.e(TAG, "onResponse: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ConfiguracionServidor> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                if (mainListener != null) {
                    mainListener.configuracionServidorNew(false);
                } else if (iniciarSesion != null) {
                    iniciarSesion.configuracionServidorNew(false);
                } else if (comunicacionMapa != null) {
                    comunicacionMapa.configuracionServidorNew(false);
                } else if (comunicacionServicio != null) {
                    comunicacionServicio.configuracionServidorNew(false);
                }
            }
        });
    }

    public void getConfiuracionInicial(final String ip) {
        if (ip != null) {
            cambioIpPresenter(ip);
            VariablesGlobales.IP_SERVICIOS_SOCKET = ip;
        }
        final ApiKtaxi.ConfiguracionIncial service = retrofit.create(ApiKtaxi.ConfiguracionIncial.class);
        Call<ResponseBody> call = service.getConfiguracionIncial();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        JSONObject object = new JSONObject(response.body().string());
                        if (object.has("EC")) {
                            if (mainListener != null) {
                                mainListener.respuestaPin(true, ip);
                            } else if (iniciarSesion != null) {
                                iniciarSesion.respuestaPin(true, ip);
                            } else if (comunicacionServicio != null) {
                                comunicacionServicio.respuestaPin(true, ip);
                            }
                        } else {
                            if (mainListener != null) {
                                mainListener.respuestaPin(false, null);
                            } else if (iniciarSesion != null) {
                                iniciarSesion.respuestaPin(false, null);
                            } else if (comunicacionServicio != null) {
                                comunicacionServicio.respuestaPin(true, ip);
                            }
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        if (mainListener != null) {
                            mainListener.respuestaPin(false, null); //Cambiar a false
                        } else if (iniciarSesion != null) {
                            iniciarSesion.respuestaPin(false, null); //Cambiar a false
                        } else if (comunicacionServicio != null) {
                            comunicacionServicio.respuestaPin(false, null); //Cambiar a false
                        }
                    }
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    if (mainListener != null) {
                        mainListener.respuestaPin(false, null);
                    } else if (iniciarSesion != null) {
                        iniciarSesion.respuestaPin(false, null);
                    } else if (comunicacionServicio != null) {
                        comunicacionServicio.respuestaPin(false, null);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                if (mainListener != null) {
                    mainListener.respuestaPin(false, null);
                } else if (iniciarSesion != null) {
                    iniciarSesion.respuestaPin(false, null);
                } else if (comunicacionServicio != null) {
                    comunicacionServicio.respuestaPin(false, null);
                }
            }


        });
    }


    public void getHelp() {
        VariablesGlobales.IP_SERVICIOS_SOCKET = VariablesGlobales.URL_SERVIDOR_HELP;
        cambioIpPresenter(VariablesGlobales.URL_SERVIDOR_HELP);
        final ApiKtaxi.ConfiguracionIncial service = retrofit.create(ApiKtaxi.ConfiguracionIncial.class);
        Call<ResponseBody> call = service.getHelp("1", new Utilidades().obtenerVersion(context), "1");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    try {
                        String data = response.body().string();
                        if (mainListener != null) {
                            mainListener.respuestaHelp(data);
                        } else if (iniciarSesion != null) {
                            iniciarSesion.respuestaHelp(data);
                        } else if (comunicacionServicio != null) {
                            comunicacionServicio.respuestaHelp(data);
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        if (mainListener != null) {
                            mainListener.respuestaHelp(null);
                        } else if (iniciarSesion != null) {
                            iniciarSesion.respuestaHelp(null);
                        } else if (comunicacionServicio != null) {
                            comunicacionServicio.respuestaHelp(null);
                        }
                    }
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    if (mainListener != null) {
                        mainListener.respuestaHelp(null);
                    } else if (iniciarSesion != null) {
                        iniciarSesion.respuestaHelp(null);
                    } else if (comunicacionServicio != null) {
                        comunicacionServicio.respuestaHelp(null);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                if (mainListener != null) {
                    mainListener.respuestaHelp(null);
                } else if (iniciarSesion != null) {
                    iniciarSesion.respuestaHelp(null);
                } else if (comunicacionServicio != null) {
                    comunicacionServicio.respuestaHelp(null);
                }
            }
        });
    }


}
