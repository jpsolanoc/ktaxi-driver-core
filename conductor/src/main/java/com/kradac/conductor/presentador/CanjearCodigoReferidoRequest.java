package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.modelo.ResponseListarCodigosReferido;
import com.kradac.conductor.modelo.Respuesta;
import com.kradac.conductor.modelo.RespuestaConsultarCodigoReferido;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CanjearCodigoReferidoRequest extends Presenter {

    private static final String TAG = CanjearCodigoReferidoRequest.class.getName();
    public final int TIPO_CLIENTE = 2;


    public void consultarCodigo(String codigo, final CodigoReferidoConsultaListerner validarListerner) {
        ApiKtaxi.ICodigoReferido service = retrofit.create(ApiKtaxi.ICodigoReferido.class);
        Call<RespuestaConsultarCodigoReferido> call = service.consultar(codigo, VariablesGlobales.NUM_ID_APLICATIVO);
        call.enqueue(new Callback<RespuestaConsultarCodigoReferido>() {

            @Override
            public void onResponse(Call<RespuestaConsultarCodigoReferido> call, Response<RespuestaConsultarCodigoReferido> response) {
                RespuestaConsultarCodigoReferido respuesta = response.body();
                if (respuesta != null) {
                    validarListerner.onSuccess(respuesta);
                } else {
                    validarListerner.onException();
                }
            }

            @Override
            public void onFailure(Call<RespuestaConsultarCodigoReferido> call, Throwable t) {
                validarListerner.onException();
            }
        });
    }

    public void registrarCodigo(int idCliente, int idCodigo, final CodigoReferidoListerner validarListerner) {
        ApiKtaxi.ICodigoReferido service = retrofit.create(ApiKtaxi.ICodigoReferido.class);
        Call<Respuesta> call = service.registrar(idCliente, idCodigo, VariablesGlobales.NUM_ID_APLICATIVO);
        call.enqueue(new Callback<Respuesta>() {

            @Override
            public void onResponse(Call<Respuesta> call, Response<Respuesta> response) {
                Respuesta respuesta = response.body();
                if (respuesta != null) {
                    validarListerner.onSuccess(respuesta);
                } else {
                    validarListerner.onException();
                }
            }

            @Override
            public void onFailure(Call<Respuesta> call, Throwable t) {
                validarListerner.onException();
            }
        });
    }

    public void listarCodigos(int idUsuario, final ListarCodigoReferidoListerner listarCodigoReferidoListerner) {
        ApiKtaxi.ICodigoReferido service = retrofit.create(ApiKtaxi.ICodigoReferido.class);
        Call<ResponseListarCodigosReferido> call = service.listarCodigos(TIPO_CLIENTE, idUsuario, VariablesGlobales.NUM_ID_APLICATIVO);
        call.enqueue(new Callback<ResponseListarCodigosReferido>() {

            @Override
            public void onResponse(Call<ResponseListarCodigosReferido> call, Response<ResponseListarCodigosReferido> response) {
                ResponseListarCodigosReferido respuesta = response.body();
                if (respuesta != null) {
                    Log.e(TAG, "onResponse: ResponseListarCodigosReferido" + respuesta.toString());
                    listarCodigoReferidoListerner.onSuccess(respuesta);
                } else {
                    listarCodigoReferidoListerner.onException();
                }
            }

            @Override
            public void onFailure(Call<ResponseListarCodigosReferido> call, Throwable t) {
                listarCodigoReferidoListerner.onException();
            }
        });
    }

    public interface CodigoReferidoListerner {
        void onSuccess(Respuesta respuesta);

        void onException();
    }

    public interface CodigoReferidoConsultaListerner {
        void onSuccess(RespuestaConsultarCodigoReferido respuesta);

        void onException();
    }

    public interface ListarCodigoReferidoListerner {
        void onSuccess(ResponseListarCodigosReferido respuesta);

        void onException();
    }



}
