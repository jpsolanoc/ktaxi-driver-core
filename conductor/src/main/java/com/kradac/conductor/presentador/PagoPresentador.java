package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionPago;
import com.kradac.conductor.modelo.PagosConductor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ing.John Patricio Solano Cabrera on 25/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class PagoPresentador extends Presenter {

    private static final String TAG = PagoPresentador.class.getName();
    OnComunicacionPago onComunicacionPago;

    public PagoPresentador(OnComunicacionPago onComunicacionPago) {
        this.onComunicacionPago = onComunicacionPago;
    }

    public void getListarDeudas(int idAplicativo, int idCiudad, int idUsuario) {
        ApiKtaxi.OnPagosConductor service = retrofit.create(ApiKtaxi.OnPagosConductor.class);
        Call<PagosConductor> call = service.listar_deudas(idAplicativo, idCiudad, idUsuario);
        call.enqueue(new Callback<PagosConductor>() {

            @Override
            public void onResponse(Call<PagosConductor> call, Response<PagosConductor> response) {
                if (response.code() == 200) {
                    onComunicacionPago.repsuestaListarDeudas(response.body());
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionPago.repsuestaListarDeudas(null);
                }
            }

            @Override
            public void onFailure(Call<PagosConductor> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionPago.repsuestaListarDeudas(null);
            }
        });
    }
}
