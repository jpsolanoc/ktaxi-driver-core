package com.kradac.conductor.presentador;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionRecuperarContrasenia;
import com.kradac.conductor.modelo.MensajeGenerico;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by John on 29/11/2016.
 */

public class RecuperarContraseniaPresentador extends Presenter {

    private OnComunicacionRecuperarContrasenia recuperarContraseniaListener;

    public RecuperarContraseniaPresentador(OnComunicacionRecuperarContrasenia recuperarContraseniaListener) {
        this.recuperarContraseniaListener = recuperarContraseniaListener;
    }


    public void recuperarContraseniaUsuario(String usuario){
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnRecuperarContrasenia service = retrofit.create(ApiKtaxi.OnRecuperarContrasenia.class);
        Call<MensajeGenerico> call = service.recuperarContrasenia(usuario,VariablesGlobales.NUM_ID_APLICATIVO);
        call.enqueue(new Callback<MensajeGenerico>() {

            @Override
            public void onResponse(Call<MensajeGenerico> call, Response<MensajeGenerico> response) {
                MensajeGenerico mensajeGenerico = response.body();
                recuperarContraseniaListener.confirmacionCambioContrasenia(mensajeGenerico);
            }

            @Override
            public void onFailure(Call<MensajeGenerico> call, Throwable t) {
                recuperarContraseniaListener.confirmacionCambioContrasenia(null);
            }
        });
    }

}
