package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionPais;
import com.kradac.conductor.modelo.Resultado;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PresenterPais extends Presenter {

    private static final String TAG = PresenterPais.class.getName();
    private OnComunicacionPais onComunicacionPais;

    public PresenterPais(OnComunicacionPais onComunicacionPais) {
        this.onComunicacionPais = onComunicacionPais;
    }

    public void obtenerPais(int idAplicativo) {
        ApiKtaxi.OnInteracionSistema service = retrofit.create(ApiKtaxi.OnInteracionSistema.class);
        Call<Resultado> call = service.obtenerPais(idAplicativo);
        call.enqueue(new Callback<Resultado>() {
            @Override
            public void onResponse(Call<Resultado> call, Response<Resultado> response) {
                if (response.code() == 200) {
                    Resultado resultado = response.body();
                    onComunicacionPais.respuestaPais(resultado);
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionPais.respuestaPais(null);
                }
            }

            @Override
            public void onFailure(Call<Resultado> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionPais.respuestaPais(null);
            }
        });
    }
}

