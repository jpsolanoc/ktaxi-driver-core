package com.kradac.conductor.presentador;

import android.content.Context;
import android.util.Log;

import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.onComunicacionSugerencia;
import com.kradac.conductor.modelo.RespuestaContactenos;
import com.kradac.conductor.modelo.Sugerencia;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by John on 29/11/2016.
 */

public class SugerenciaPresentador extends Presenter {

    private static final String TAG = SugerenciaPresentador.class.getName();
    private onComunicacionSugerencia comunicacionSugerenciaListener;
    private Utilidades utilidades;
    private Context context;

    public SugerenciaPresentador(onComunicacionSugerencia comunicacionSugerenciaListener, Context context) {
        this.comunicacionSugerenciaListener = comunicacionSugerenciaListener;
        utilidades = new Utilidades();
        this.context = context;
    }



    public void enviarContactenosV2(int tipo,int idUsuario,int idSolicitud,String nombre,String correo,String telefono,String motivo,String mensaje,
                                    double latitud,double longitud,String codigoPais){
        cambioIpPresenter(VariablesGlobales.IP_SERVICIOS_SOCKET);
        ApiKtaxi.OnContactenos service = retrofit.create(ApiKtaxi.OnContactenos.class);
        Call<RespuestaContactenos> call = service.enviar_contactenos(tipo,VariablesGlobales.NUM_ID_APLICATIVO,
                0,idUsuario,idUsuario,VariablesGlobales.ID_PLATAFORMA,idSolicitud,nombre,correo,
                telefono,motivo,mensaje,latitud,longitud,utilidades.obtenerVersion(context),
                utilidades.obtenerVersionSo(),utilidades.getDeviceName(),utilidades.obtenerModelo(),
                utilidades.getTipoRed(context),utilidades.statusGPS(context),utilidades.getConnectivityStatus(context),
                utilidades.getNameOperator(context),utilidades.getImei(context),2,codigoPais,0);
        call.enqueue(new Callback<RespuestaContactenos>() {
            @Override
            public void onResponse(Call<RespuestaContactenos> call, Response<RespuestaContactenos> response) {
                if (response.code()== HttpsURLConnection.HTTP_OK){
                    comunicacionSugerenciaListener.respuestaSugerecia(response.body());
                }else {
                    comunicacionSugerenciaListener.respuestaSugerecia(null);
                }
            }

            @Override
            public void onFailure(Call<RespuestaContactenos> call, Throwable t) {
                Log.e(TAG, "onFailure: ",t );
                comunicacionSugerenciaListener.respuestaSugerecia(null);
            }
        });
    }

}
