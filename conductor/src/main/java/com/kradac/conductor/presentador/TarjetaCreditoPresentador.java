package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.modelo.ConsultarSaldoTarjetaCredito;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by DellKradac on 17/02/2018.
 */

public class TarjetaCreditoPresentador extends Presenter {

    private static final String TAG = TarjetaCreditoPresentador.class.getName();
    OnComunicacionTarjetaCredito onComunicacionSaldoKtaxi;

    public TarjetaCreditoPresentador(OnComunicacionTarjetaCredito onComunicacionSaldoKtaxi) {
        this.onComunicacionSaldoKtaxi = onComunicacionSaldoKtaxi;
    }

    public interface OnComunicacionTarjetaCredito {
        void respuestaSaldoTarjetaCredito(ConsultarSaldoTarjetaCredito consultarSaldoTarjetaCredito);
    }

    public void creditoConsultarSaldo(int idUsuario,
                                      String token,
                                      String key,
                                      String timeStanD,
                                      int idVehiculo,
                                      int idCiudad) {
        ApiKtaxi.ComponentesContables service = retrofit.create(ApiKtaxi.ComponentesContables.class);
        Call<ConsultarSaldoTarjetaCredito> call = service.credito_consultarSaldo(idUsuario, token, key, timeStanD, idVehiculo, idCiudad);
        call.enqueue(new Callback<ConsultarSaldoTarjetaCredito>() {
            @Override
            public void onResponse(Call<ConsultarSaldoTarjetaCredito> call, Response<ConsultarSaldoTarjetaCredito> response) {
                if (response.code() == 200) {
                    onComunicacionSaldoKtaxi.respuestaSaldoTarjetaCredito(response.body());
                } else {
                    onComunicacionSaldoKtaxi.respuestaSaldoTarjetaCredito(null);
                }
            }

            @Override
            public void onFailure(Call<ConsultarSaldoTarjetaCredito> call, Throwable t) {
                onComunicacionSaldoKtaxi.respuestaSaldoTarjetaCredito(null);
            }
        });
    }
}
