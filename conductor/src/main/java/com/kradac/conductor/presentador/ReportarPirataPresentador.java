package com.kradac.conductor.presentador;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.kradac.conductor.extras.TratarImagenesGiro;
import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.interfaces.OnComunicacionReportarPirata;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dell on 14/2/2017.
 */

public class ReportarPirataPresentador  extends Presenter{

    private static final String TAG = ReportarPirataPresentador.class.getName();
    private OnComunicacionReportarPirata reportarPirata;
    private Context context;

    public ReportarPirataPresentador(OnComunicacionReportarPirata reportarPirata, Context context) {
        this.reportarPirata = reportarPirata;
        this.context = context;
    }


    public void getReportarPirata(int idUsuario,int idVehiculo,String placa,String color,String denuncia,double latitud,double longitud){
        ApiKtaxi.ReportarPirata service = retrofit.create(ApiKtaxi.ReportarPirata.class);
        Call<ResponseBody> call = service.reportarPirata(idUsuario,idVehiculo,placa,color,denuncia,latitud,longitud);
        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code()==200){
                try {
                    reportarPirata.repsuestaDenuncia(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }}else{
                    Log.e(TAG, "onResponse: "+ response.code() );
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: ",t );
            }
        });
    }


    public void uploadFile(String fileUri,String nameArchivo) {
        Log.e(TAG, "uploadFile: " + fileUri);
        Log.e(TAG, "uploadFile NAME ARCHIVO: " + nameArchivo);
        File file = new File(fileUri);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("imagen", nameArchivo, reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "upload_test");
        ApiKtaxi.ReportarPirata service = retrofit.create(ApiKtaxi.ReportarPirata.class);
        final Call<okhttp3.ResponseBody> req = service.postImage(body, name);
        req.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code()==200){
                    try {
                        reportarPirata.respuestaFoto(response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: ",t );
            }
        });
    }


    public void bitmapExtrae(String fileUri, String nombreArchivo) {
        try {
            persistImage(new TratarImagenesGiro().handleSamplingAndRotationBitmap(context, Uri.parse("file://" + fileUri)), nombreArchivo);
        } catch (IOException e) {
            Log.e(TAG, "bitmapExtrae: ", e);
        }
    }

    private File createImageFile() throws IOException {
        String imageFileName = "Ktaxi-";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );
        return image;
    }


    private void persistImage(Bitmap bitmap, String nombreArchivo) {
        File imageFile = null;
        try {
            imageFile = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.getMessage();
        }
        assert imageFile != null;
        uploadFile(imageFile.getAbsolutePath(), nombreArchivo);
    }
}
