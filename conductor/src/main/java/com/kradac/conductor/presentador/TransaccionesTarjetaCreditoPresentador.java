package com.kradac.conductor.presentador;

import android.util.Log;

import com.kradac.conductor.interfaces.ApiKtaxi;
import com.kradac.conductor.modelo.ConsultarMovimientos;
import com.kradac.conductor.modelo.TransaccionesSaldoKtaxi;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

/**
 * Created by DellKradac on 17/02/2018.
 */

public class TransaccionesTarjetaCreditoPresentador extends Presenter {

    OnComunicacionMovimientos onComunicacionMovimientos;

    public TransaccionesTarjetaCreditoPresentador(OnComunicacionMovimientos onComunicacionMovimientos) {
        this.onComunicacionMovimientos = onComunicacionMovimientos;
    }

    private static final String TAG = TransaccionesTarjetaCreditoPresentador.class.getName();

    public interface OnComunicacionMovimientos {
        void respuestaConsultarMovimientos(ConsultarMovimientos consultarMovimientos);
    }

    public void consultarMovimientos(int idUsuario,
                                     String token,
                                     String key,
                                     String timeStanD,
                                     int idVehiculo,
                                     int idCiudad,
                                     int anio,
                                     int mes) {
        ApiKtaxi.ComponentesContables service = retrofit.create(ApiKtaxi.ComponentesContables.class);
        Call<ConsultarMovimientos> call = service.consultar_movimientos(idUsuario, token, key, timeStanD, idVehiculo, idCiudad, anio, mes);
        call.enqueue(new Callback<ConsultarMovimientos>() {
            @Override
            public void onResponse(Call<ConsultarMovimientos> call, Response<ConsultarMovimientos> response) {
                if (response.code() == 200) {
                    onComunicacionMovimientos.respuestaConsultarMovimientos(response.body());
                } else {
                    Log.e(TAG, "onResponse: " + response.code());
                    onComunicacionMovimientos.respuestaConsultarMovimientos(null);
                }
            }

            @Override
            public void onFailure(Call<ConsultarMovimientos> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionMovimientos.respuestaConsultarMovimientos(null);
            }
        });
    }
}
