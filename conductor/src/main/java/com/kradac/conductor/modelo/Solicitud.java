package com.kradac.conductor.modelo;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import butterknife.internal.ListenerClass;

public class Solicitud implements Comparator<Solicitud>, Parcelable {

    private int idUsuario;
    private String callePrincipal;
    private String calleSecundaria;
    private String barrio;
    private String referencia;
    private String pedido;
    private String destino;
    private double latitud;
    private double longitud;
    private Double latitudDestino;
    private Double longitudDestino;
    private int estadoChat;
    private int idEmpresa;
    private int idSolicitud;
    private int idPedido;
    private int estadoSolicitud;
    private int estadoPedido;
    private int eventualidadCancelacion;
    private int idVehiculo;
    private int favorito;
    private String nombresCliente;
    private String telefonoCliente;
    private int tiempo;
    private double distancia;
    private int username;
    private double precioPedido;
    private double costoCarrera;
    private String celular;
    private String lugarCompra;
    private ArrayList<Integer> tiempos;
    private ArrayList<Double> precios;
    private int abordadoPor;
    private CancelarSolicitud datosCancelarSolicitud;
    private boolean isQuiosco;
    private boolean isBotonAbordo;
    private int tipoFormaPago;
    private double propina;
    private double saldo;
    private int pin;
    private int asignadaDesdeSolicitud;
    private boolean isEnvioBotonAbordo;
    //Nuevos parametros
    private int t;
    private double ltE;
    private double lgE;
    private String bE;
    private ArrayList<Double> lp;
    private double au;
    private double m;
    private int estadoComprandoSemaforo;
    private int estadoCompradoSemaforo;
    private int razonCancelada;
    private String mensajeSolicitudCancelada;
    private String mensajePedidoCancelado;
    private String color;
    private String servicio;
    private double precioVoucher;
    private boolean isCalificar;
    private int idServicioActivo;

    public int getIdServicioActivo() {
        return idServicioActivo;
    }

    public void setIdServicioActivo(int idServicioActivo) {
        this.idServicioActivo = idServicioActivo;
    }

    public String direccionSolicitudCompleta() {
        String direccion = "";
        if (!calleSecundaria.isEmpty() && calleSecundaria != null &&
                !calleSecundaria.trim().equalsIgnoreCase("NONE")) {
            direccion = barrio.concat(" ,CP: ").concat(callePrincipal).concat(" ,CS: ").concat(calleSecundaria);
        } else {
            direccion = barrio.concat(" ,CP: ").concat(callePrincipal);
        }
        return direccion;
    }

    private List<Destino> lD;

    public List<Destino> getlD() {
        return lD;
    }

    public void setlD(List<Destino> lD) {
        this.lD = lD;
    }

    public boolean isCalificar() {
        return isCalificar;
    }

    public void setCalificar(boolean calificar) {
        isCalificar = calificar;
    }

    public double getPrecioVoucher() {
        return precioVoucher;
    }

    public void setPrecioVoucher(double precioVoucher) {
        this.precioVoucher = precioVoucher;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    private int conP = 4;

    public int getConP() {
        return conP;
    }

    public void setConP(int conP) {
        this.conP = conP;
    }

    private boolean isSolicitudCC;

    public boolean isSolicitudCC() {
        return isSolicitudCC;
    }

    public void setSolicitudCC(boolean solicitudCC) {
        isSolicitudCC = solicitudCC;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMensajeSolicitudCancelada() {
        return mensajeSolicitudCancelada;
    }

    public void setMensajeSolicitudCancelada(String mensajeSolicitudCancelada) {
        this.mensajeSolicitudCancelada = mensajeSolicitudCancelada;
    }

    public String getMensajePedidoCancelado() {
        return mensajePedidoCancelado;
    }

    public void setMensajePedidoCancelado(String mensajePedidoCancelado) {
        this.mensajePedidoCancelado = mensajePedidoCancelado;
    }

    public int getRazonCancelada() {
        return razonCancelada;
    }

    public void setRazonCancelada(int razonCancelada) {
        this.razonCancelada = razonCancelada;
    }

    public int getEstadoComprandoSemaforo() {
        return estadoComprandoSemaforo;
    }

    public void setEstadoComprandoSemaforo(int estadoComprandoSemaforo) {
        this.estadoComprandoSemaforo = estadoComprandoSemaforo;
    }

    public int getEstadoCompradoSemaforo() {
        return estadoCompradoSemaforo;
    }

    public void setEstadoCompradoSemaforo(int estadoCompradoSemaforo) {
        this.estadoCompradoSemaforo = estadoCompradoSemaforo;
    }

    public String getbE() {
        return bE;
    }

    public boolean isPedido() {
        if (idPedido != 0) {
            return true;
        }
        return false;
    }

    public Solicitud() {
        tiempos = new ArrayList<>();
        datosCancelarSolicitud = new CancelarSolicitud();
    }

    public static Solicitud createSolicitud(String solicitudJson) {
        Gson obGson = new Gson();
        return obGson.fromJson(solicitudJson, Solicitud.class);
    }

    public int getEstadoChat() {
        return estadoChat;
    }

    public void setEstadoChat(int estadoChat) {
        this.estadoChat = estadoChat;
    }

    public int getAbordadoPor() {
        return abordadoPor;
    }

    public void setAbordadoPor(int abordadoPor) {
        this.abordadoPor = abordadoPor;
    }

    public ArrayList<Integer> getTiempos() {
        return tiempos;
    }

    public void setTiempos(ArrayList<Integer> tiempos) {
        this.tiempos = tiempos;
    }

    public int getUsername() {
        return username;
    }

    public void setUsername(int username) {
        this.username = username;
    }

    public int getFavorito() {
        return favorito;
    }

    public void setFavorito(int favorito) {
        this.favorito = favorito;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombresCliente() {
        return nombresCliente;
    }

    public void setNombresCliente(String nombresCliente) {
        this.nombresCliente = nombresCliente;
    }

    public String getCallePrincipal() {
        return callePrincipal;
    }

    public void setCallePrincipal(String callePrincipal) {
        this.callePrincipal = callePrincipal;
    }

    public String getCalleSecundaria() {
        return calleSecundaria;
    }

    public void setCalleSecundaria(String calleSecundaria) {
        this.calleSecundaria = calleSecundaria;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public int getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(int idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public int getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(int estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public int getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(int idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public int getEventualidadCancelacion() {
        return eventualidadCancelacion;
    }

    public void setEventualidadCancelacion(int eventualidadCancelacion) {
        this.eventualidadCancelacion = eventualidadCancelacion;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    public double getLtE() {
        return ltE;
    }

    public void setLtE(double ltE) {
        this.ltE = ltE;
    }

    public double getLgE() {
        return lgE;
    }

    public void setLgE(double lgE) {
        this.lgE = lgE;
    }

    public void setbE(String bE) {
        this.bE = bE;
    }


    public ArrayList<Double> getLp() {
        return lp;
    }

    public void setLp(ArrayList<Double> lp) {
        this.lp = lp;
    }

    public double getAu() {
        return au;
    }

    public void setAu(double au) {
        this.au = au;
    }

    public double getM() {
        return m;
    }

    public void setM(double m) {
        this.m = m;
    }

    public int getAsignadaDesdeSolicitud() {
        return asignadaDesdeSolicitud;
    }

    public void setAsignadaDesdeSolicitud(int asignadaDesdeSolicitud) {
        this.asignadaDesdeSolicitud = asignadaDesdeSolicitud;
    }

    @Override
    public int compare(Solicitud lhs, Solicitud rhs) {
        int estado = 0;
        if (lhs.getIdSolicitud() == rhs.getIdSolicitud()) {
            estado = 1;
        } else {
            estado = 0;
        }
        return estado;
    }


    public boolean isEnvioBotonAbordo() {
        return isEnvioBotonAbordo;
    }

    public void setEnvioBotonAbordo(boolean envioBotonAbordo) {
        isEnvioBotonAbordo = envioBotonAbordo;
    }

    public boolean compararID(int lhs, int rhs) {
        return lhs == rhs;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public ArrayList<Double> getPrecios() {
        return precios;
    }

    public void setPrecios(ArrayList<Double> precios) {
        this.precios = precios;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public String getPedido() {
        return pedido;
    }

    public void setPedido(String pedido) {
        this.pedido = pedido;
    }

    public double getPropina() {
        return propina;
    }

    public void setPropina(double propina) {
        this.propina = propina;
    }

    public double getCostoCarrera() {
        return costoCarrera;
    }

    public void setCostoCarrera(double costoCarrera) {
        this.costoCarrera = costoCarrera;
    }

    public double getPrecioPedido() {
        return precioPedido;
    }

    public void setPrecioPedido(int precioPedido) {
        this.precioPedido = precioPedido;
    }

    public void setPrecioPedido(double precioPedido) {
        this.precioPedido = precioPedido;
    }

    public String getLugarCompra() {
        return lugarCompra;
    }

    public void setLugarCompra(String lugarCompra) {
        this.lugarCompra = lugarCompra;
    }

    public String serializeSolicitud() {
        Gson solicitudGson = new Gson();
        return solicitudGson.toJson(this);
    }

    public CancelarSolicitud getDatosCancelarSolicitud() {
        return datosCancelarSolicitud;
    }

    public void setDatosCancelarSolicitud(CancelarSolicitud datosCancelarSolicitud) {
        this.datosCancelarSolicitud = datosCancelarSolicitud;
    }

    public boolean isQuiosco() {
        return isQuiosco;
    }

    public void setQuiosco(boolean quiosco) {
        isQuiosco = quiosco;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Double getLatitudDestino() {
        return latitudDestino;
    }

    public void setLatitudDestino(Double latitudDestino) {
        this.latitudDestino = latitudDestino;
    }

    public Double getLongitudDestino() {
        return longitudDestino;
    }

    public void setLongitudDestino(Double longitudDestino) {
        this.longitudDestino = longitudDestino;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Solicitud)) return false;

        Solicitud solicitud = (Solicitud) o;

        if (idSolicitud != solicitud.idSolicitud) return false;
        return idPedido == solicitud.idPedido;

    }


    public int getEstadoPedido() {
        return estadoPedido;
    }

    public void setEstadoPedido(int estadoPedido) {
        this.estadoPedido = estadoPedido;
    }

    public boolean isBotonAbordo() {
        return isBotonAbordo;
    }

    public void setBotonAbordo(boolean botonAbordo) {
        isBotonAbordo = botonAbordo;
    }

    @Override
    public int hashCode() {
        return idSolicitud;
    }

    public int getTipoFormaPago() {
        return tipoFormaPago;
    }

    public void setTipoFormaPago(int tipoFormaPago) {
        this.tipoFormaPago = tipoFormaPago;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idUsuario);
        dest.writeString(this.callePrincipal);
        dest.writeString(this.calleSecundaria);
        dest.writeString(this.barrio);
        dest.writeString(this.referencia);
        dest.writeString(this.pedido);
        dest.writeString(this.destino);
        dest.writeDouble(this.latitud);
        dest.writeDouble(this.longitud);
        dest.writeValue(this.latitudDestino);
        dest.writeValue(this.longitudDestino);
        dest.writeInt(this.estadoChat);
        dest.writeInt(this.idEmpresa);
        dest.writeInt(this.idSolicitud);
        dest.writeInt(this.idPedido);
        dest.writeInt(this.estadoSolicitud);
        dest.writeInt(this.estadoPedido);
        dest.writeInt(this.eventualidadCancelacion);
        dest.writeInt(this.idVehiculo);
        dest.writeInt(this.favorito);
        dest.writeString(this.nombresCliente);
        dest.writeString(this.telefonoCliente);
        dest.writeInt(this.tiempo);
        dest.writeDouble(this.distancia);
        dest.writeInt(this.username);
        dest.writeDouble(this.precioPedido);
        dest.writeDouble(this.costoCarrera);
        dest.writeString(this.celular);
        dest.writeString(this.lugarCompra);
        dest.writeList(this.tiempos);
        dest.writeList(this.precios);
        dest.writeInt(this.abordadoPor);
        dest.writeSerializable(this.datosCancelarSolicitud);
        dest.writeByte(this.isQuiosco ? (byte) 1 : (byte) 0);
        dest.writeByte(this.isBotonAbordo ? (byte) 1 : (byte) 0);
        dest.writeInt(this.tipoFormaPago);
        dest.writeDouble(this.propina);
        dest.writeDouble(this.saldo);
        dest.writeInt(this.pin);
        dest.writeInt(this.asignadaDesdeSolicitud);
        dest.writeByte(this.isEnvioBotonAbordo ? (byte) 1 : (byte) 0);
        dest.writeInt(this.t);
        dest.writeDouble(this.ltE);
        dest.writeDouble(this.lgE);
        dest.writeString(this.bE);
        dest.writeList(this.lp);
        dest.writeDouble(this.au);
        dest.writeDouble(this.m);
        dest.writeInt(this.estadoComprandoSemaforo);
        dest.writeInt(this.estadoCompradoSemaforo);
        dest.writeInt(this.razonCancelada);
        dest.writeString(this.mensajeSolicitudCancelada);
        dest.writeString(this.mensajePedidoCancelado);
        dest.writeString(this.color);
        dest.writeString(this.servicio);
        dest.writeDouble(this.precioVoucher);
        dest.writeByte(this.isCalificar ? (byte) 1 : (byte) 0);
        dest.writeInt(this.idServicioActivo);
        dest.writeTypedList(this.lD);
        dest.writeInt(this.conP);
        dest.writeByte(this.isSolicitudCC ? (byte) 1 : (byte) 0);
    }

    protected Solicitud(Parcel in) {
        this.idUsuario = in.readInt();
        this.callePrincipal = in.readString();
        this.calleSecundaria = in.readString();
        this.barrio = in.readString();
        this.referencia = in.readString();
        this.pedido = in.readString();
        this.destino = in.readString();
        this.latitud = in.readDouble();
        this.longitud = in.readDouble();
        this.latitudDestino = (Double) in.readValue(Double.class.getClassLoader());
        this.longitudDestino = (Double) in.readValue(Double.class.getClassLoader());
        this.estadoChat = in.readInt();
        this.idEmpresa = in.readInt();
        this.idSolicitud = in.readInt();
        this.idPedido = in.readInt();
        this.estadoSolicitud = in.readInt();
        this.estadoPedido = in.readInt();
        this.eventualidadCancelacion = in.readInt();
        this.idVehiculo = in.readInt();
        this.favorito = in.readInt();
        this.nombresCliente = in.readString();
        this.telefonoCliente = in.readString();
        this.tiempo = in.readInt();
        this.distancia = in.readDouble();
        this.username = in.readInt();
        this.precioPedido = in.readDouble();
        this.costoCarrera = in.readDouble();
        this.celular = in.readString();
        this.lugarCompra = in.readString();
        this.tiempos = new ArrayList<Integer>();
        in.readList(this.tiempos, Integer.class.getClassLoader());
        this.precios = new ArrayList<Double>();
        in.readList(this.precios, Double.class.getClassLoader());
        this.abordadoPor = in.readInt();
        this.datosCancelarSolicitud = (CancelarSolicitud) in.readSerializable();
        this.isQuiosco = in.readByte() != 0;
        this.isBotonAbordo = in.readByte() != 0;
        this.tipoFormaPago = in.readInt();
        this.propina = in.readDouble();
        this.saldo = in.readDouble();
        this.pin = in.readInt();
        this.asignadaDesdeSolicitud = in.readInt();
        this.isEnvioBotonAbordo = in.readByte() != 0;
        this.t = in.readInt();
        this.ltE = in.readDouble();
        this.lgE = in.readDouble();
        this.bE = in.readString();
        this.lp = new ArrayList<Double>();
        in.readList(this.lp, Double.class.getClassLoader());
        this.au = in.readDouble();
        this.m = in.readDouble();
        this.estadoComprandoSemaforo = in.readInt();
        this.estadoCompradoSemaforo = in.readInt();
        this.razonCancelada = in.readInt();
        this.mensajeSolicitudCancelada = in.readString();
        this.mensajePedidoCancelado = in.readString();
        this.color = in.readString();
        this.servicio = in.readString();
        this.precioVoucher = in.readDouble();
        this.isCalificar = in.readByte() != 0;
        this.idServicioActivo = in.readInt();
        this.lD = in.createTypedArrayList(Destino.CREATOR);
        this.conP = in.readInt();
        this.isSolicitudCC = in.readByte() != 0;
    }

    public static final Creator<Solicitud> CREATOR = new Creator<Solicitud>() {
        @Override
        public Solicitud createFromParcel(Parcel source) {
            return new Solicitud(source);
        }

        @Override
        public Solicitud[] newArray(int size) {
            return new Solicitud[size];
        }
    };
}
