package com.kradac.conductor.modelo;

import java.util.ArrayList;

public class ResponseListarTarjetaCredito extends ResponseApiCorto {
    private ArrayList<TarjetaCredito> lT;

    public ArrayList<TarjetaCredito> getlT() {
        return lT;
    }

    public void setlT(ArrayList<TarjetaCredito> lT) {
        this.lT = lT;
    }

    @Override
    public String toString() {
        return "ResponseListarTarjetaCredito{" +
                "lT=" + lT +
                '}';
    }
}
