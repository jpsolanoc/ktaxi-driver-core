package com.kradac.conductor.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

/**
 * Created by DellKradac on 26/02/2018.
 */

public class RespuestaPosibleSolicitudes implements Parcelable {


    private int iS;
    private int idAplicativo;
    private int numeroUnidad;
    private String empresa;
    private String usuario;
    private int t3;
    private int t2;
    private int t1;
    private String m;
    private int iPS;
    private double lg;
    private double lt;
    private int t;
    private int i;
    private int tp;
    private int iV;

    public int getiS() {
        return iS;
    }

    public void setiS(int iS) {
        this.iS = iS;
    }

    public int getIdAplicativo() {
        return idAplicativo;
    }

    public void setIdAplicativo(int idAplicativo) {
        this.idAplicativo = idAplicativo;
    }

    public int getNumeroUnidad() {
        return numeroUnidad;
    }

    public void setNumeroUnidad(int numeroUnidad) {
        this.numeroUnidad = numeroUnidad;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getT3() {
        return t3;
    }

    public void setT3(int t3) {
        this.t3 = t3;
    }

    public int getT2() {
        return t2;
    }

    public void setT2(int t2) {
        this.t2 = t2;
    }

    public int getT1() {
        return t1;
    }

    public void setT1(int t1) {
        this.t1 = t1;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public int getiPS() {
        return iPS;
    }

    public void setiPS(int iPS) {
        this.iPS = iPS;
    }

    public double getLg() {
        return lg;
    }

    public void setLg(double lg) {
        this.lg = lg;
    }

    public double getLt() {
        return lt;
    }

    public void setLt(double lt) {
        this.lt = lt;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public int getTp() {
        return tp;
    }

    public void setTp(int tp) {
        this.tp = tp;
    }

    public int getiV() {
        return iV;
    }

    public void setiV(int iV) {
        this.iV = iV;
    }

    public String serializePosibleSolicitud() {
        Gson posibleSolicitud = new Gson();
        return posibleSolicitud.toJson(this);
    }


    public static RespuestaPosibleSolicitudes createPosibleSolicitudes(String posibleSolcitud) {
        Gson obGson = new Gson();
        return obGson.fromJson(posibleSolcitud, RespuestaPosibleSolicitudes.class);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.iS);
        dest.writeInt(this.idAplicativo);
        dest.writeInt(this.numeroUnidad);
        dest.writeString(this.empresa);
        dest.writeString(this.usuario);
        dest.writeInt(this.t3);
        dest.writeInt(this.t2);
        dest.writeInt(this.t1);
        dest.writeString(this.m);
        dest.writeInt(this.iPS);
        dest.writeDouble(this.lg);
        dest.writeDouble(this.lt);
        dest.writeInt(this.t);
        dest.writeInt(this.i);
        dest.writeInt(this.tp);
        dest.writeInt(this.iV);
    }

    public RespuestaPosibleSolicitudes() {
    }

    protected RespuestaPosibleSolicitudes(Parcel in) {
        this.iS = in.readInt();
        this.idAplicativo = in.readInt();
        this.numeroUnidad = in.readInt();
        this.empresa = in.readString();
        this.usuario = in.readString();
        this.t3 = in.readInt();
        this.t2 = in.readInt();
        this.t1 = in.readInt();
        this.m = in.readString();
        this.iPS = in.readInt();
        this.lg = in.readDouble();
        this.lt = in.readDouble();
        this.t = in.readInt();
        this.i = in.readInt();
        this.tp = in.readInt();
        this.iV = in.readInt();
    }

    public static final Creator<RespuestaPosibleSolicitudes> CREATOR = new Creator<RespuestaPosibleSolicitudes>() {
        @Override
        public RespuestaPosibleSolicitudes createFromParcel(Parcel source) {
            return new RespuestaPosibleSolicitudes(source);
        }

        @Override
        public RespuestaPosibleSolicitudes[] newArray(int size) {
            return new RespuestaPosibleSolicitudes[size];
        }
    };
}
