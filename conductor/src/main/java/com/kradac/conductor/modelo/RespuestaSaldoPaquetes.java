package com.kradac.conductor.modelo;

import java.util.List;

public class RespuestaSaldoPaquetes {

    private List<LPt> lPt;
    private int en;

    public List<LPt> getLPt() {
        return lPt;
    }

    public void setLPt(List<LPt> lPt) {
        this.lPt = lPt;
    }

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public static class LPt {
        private List<LP> lP;
        private String descripcion;
        private String paqueteTipo;
        private int idPaqueteTipo;

        public List<LP> getLP() {
            return lP;
        }

        public void setLP(List<LP> lP) {
            this.lP = lP;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getPaqueteTipo() {
            return paqueteTipo;
        }

        public void setPaqueteTipo(String paqueteTipo) {
            this.paqueteTipo = paqueteTipo;
        }

        public int getIdPaqueteTipo() {
            return idPaqueteTipo;
        }

        public void setIdPaqueteTipo(int idPaqueteTipo) {
            this.idPaqueteTipo = idPaqueteTipo;
        }
    }

    public static class LP {
        private double creditoPromocion;
        private int caduca;
        private double credito;
        private double creditoTotal;
        private double relacion;
        private double relacionPorcentaje;
        private double referenciaD;
        private double referenciaP;
        private String desde;
        private String hasta;
        private String descripcion;

        public double getCreditoPromocion() {
            return creditoPromocion;
        }

        public void setCreditoPromocion(double creditoPromocion) {
            this.creditoPromocion = creditoPromocion;
        }

        public int getCaduca() {
            return caduca;
        }

        public void setCaduca(int caduca) {
            this.caduca = caduca;
        }

        public double getCredito() {
            return credito;
        }

        public void setCredito(double credito) {
            this.credito = credito;
        }

        public double getCreditoTotal() {
            return creditoTotal;
        }

        public void setCreditoTotal(double creditoTotal) {
            this.creditoTotal = creditoTotal;
        }

        public double getRelacion() {
            return relacion;
        }

        public void setRelacion(double relacion) {
            this.relacion = relacion;
        }

        public double getRelacionPorcentaje() {
            return relacionPorcentaje;
        }

        public void setRelacionPorcentaje(double relacionPorcentaje) {
            this.relacionPorcentaje = relacionPorcentaje;
        }

        public double getReferenciaD() {
            return referenciaD;
        }

        public void setReferenciaD(double referenciaD) {
            this.referenciaD = referenciaD;
        }

        public void setReferenciaD(int referenciaD) {
            this.referenciaD = referenciaD;
        }

        public double getReferenciaP() {
            return referenciaP;
        }

        public void setReferenciaP(double referenciaP) {
            this.referenciaP = referenciaP;
        }

        public String getDesde() {
            return desde;
        }

        public void setDesde(String desde) {
            this.desde = desde;
        }

        public String getHasta() {
            return hasta;
        }

        public void setHasta(String hasta) {
            this.hasta = hasta;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        @Override
        public String toString() {
            return "LP{" +
                    "creditoPromocion=" + creditoPromocion +
                    ", caduca=" + caduca +
                    ", credito=" + credito +
                    ", creditoTotal=" + creditoTotal +
                    ", relacion=" + relacion +
                    ", relacionPorcentaje=" + relacionPorcentaje +
                    ", referenciaD=" + referenciaD +
                    ", referenciaP=" + referenciaP +
                    ", desde='" + desde + '\'' +
                    ", hasta='" + hasta + '\'' +
                    ", descripcion='" + descripcion + '\'' +
                    '}';
        }
    }
}
