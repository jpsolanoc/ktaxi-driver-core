package com.kradac.conductor.modelo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "DatosEnvioTaximetro", id = "_id")
public class DatosEnvioTaximetro extends Model {

    @Column(name = "horaInicio")
    private String horaInicio;

    @Column(name = "horaFin")
    private String horaFin;

    @Column(name = "tarifaArranque")
    private String tarifaArranque;

    @Column(name = "tiempoTotal")
    private String tiempoTotal;

    @Column(name = "tiempoEspera")
    private String tiempoEspera;

    @Column(name = "valorTiempo")
    private String valorTiempo;

    @Column(name = "distanciaRecorrida")
    private String distanciaRecorrida;

    @Column(name = "valorDistancia")
    private String valorDistancia;

    @Column(name = "totalCarrera")
    private String totalCarrera;

    @Column(name = "editarCostoC")
    private int editarCostoC;

    public DatosEnvioTaximetro() {
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }

    public String getTarifaArranque() {
        return tarifaArranque;
    }

    public void setTarifaArranque(String tarifaArranque) {
        this.tarifaArranque = tarifaArranque;
    }

    public String getTiempoTotal() {
        return tiempoTotal;
    }

    public void setTiempoTotal(String tiempoTotal) {
        this.tiempoTotal = tiempoTotal;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getTiempoEspera() {
        return tiempoEspera;
    }

    public void setTiempoEspera(String tiempoEspera) {
        this.tiempoEspera = tiempoEspera;
    }

    public String getValorTiempo() {
        return valorTiempo;
    }

    public void setValorTiempo(String valorTiempo) {
        this.valorTiempo = valorTiempo;
    }

    public String getDistanciaRecorrida() {
        return distanciaRecorrida;
    }

    public void setDistanciaRecorrida(String distanciaRecorrida) {
        this.distanciaRecorrida = distanciaRecorrida;
    }

    public String getValorDistancia() {
        return valorDistancia;
    }

    public void setValorDistancia(String valorDistancia) {
        this.valorDistancia = valorDistancia;
    }

    public String getTotalCarrera() {
        return totalCarrera;
    }

    public void setTotalCarrera(String totalCarrera) {
        this.totalCarrera = totalCarrera;
    }

    public static List<DatosEnvioTaximetro> getAll() {
        return new Select()
                .from(DatosEnvioTaximetro.class)
                .execute();
    }


    public static void deleteAll() {
        new Delete().from(DatosEnvioTaximetro.class).execute();
    }

    public int getEditarCostoC() {
        return editarCostoC;
    }

    public void setEditarCostoC(int editarCostoC) {
        this.editarCostoC = editarCostoC;
    }

    @Override
    public String toString() {
        return "DatosEnvioTaximetro{" +
                "horaInicio='" + horaInicio + '\'' +
                ", horaFin='" + horaFin + '\'' +
                ", tarifaArranque='" + tarifaArranque + '\'' +
                ", tiempoTotal='" + tiempoTotal + '\'' +
                ", tiempoEspera='" + tiempoEspera + '\'' +
                ", valorTiempo='" + valorTiempo + '\'' +
                ", distanciaRecorrida='" + distanciaRecorrida + '\'' +
                ", valorDistancia='" + valorDistancia + '\'' +
                ", totalCarrera='" + totalCarrera + '\'' +
                ", editarCostoC=" + editarCostoC +
                '}';
    }
}
