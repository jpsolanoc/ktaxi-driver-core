package com.kradac.conductor.modelo;

/**
 * Created by work on 05/12/16.
 */

public class ResponseApiCorto {
    private int en;
    private int error;
    private String m;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ResponseApiCorto{" +
                "en=" + en +
                ", error=" + error +
                ", m='" + m + '\'' +
                '}';
    }
}
