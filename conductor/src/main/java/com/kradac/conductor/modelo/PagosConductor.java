package com.kradac.conductor.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ing.John Patricio Solano Cabrera on 25/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class PagosConductor implements Parcelable {

    private int en;
    private List<LD> lD;
    private String m;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public List<LD> getLD() {
        return lD;
    }

    public void setLD(List<LD> lD) {
        this.lD = lD;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public static class LD implements Parcelable {
        private int idDeudaEstado;
        private String deudaEstado;
        private int correctas;
        private int idDeuda;
        private double deuda;
        private double impuestos;
        private double monto;
        private double pago;
        private int anio;
        private int mes;
        private String descripccion;
        private int meses;
        private double mora;
        private double moraImpuestos;
        private double descuento;


        public double getDeudaConIva() {
            return ((monto * impuestos) / 100);
        }

        public double getObtenerPagoDescuento() {
            return ((deuda * descuento) / 100);
        }

        public int getIdDeudaEstado() {
            return idDeudaEstado;
        }

        public void setIdDeudaEstado(int idDeudaEstado) {
            this.idDeudaEstado = idDeudaEstado;
        }

        public String getDeudaEstado() {
            return deudaEstado;
        }

        public void setDeudaEstado(String deudaEstado) {
            this.deudaEstado = deudaEstado;
        }

        public int getCorrectas() {
            return correctas;
        }

        public void setCorrectas(int correctas) {
            this.correctas = correctas;
        }

        public int getIdDeuda() {
            return idDeuda;
        }

        public void setIdDeuda(int idDeuda) {
            this.idDeuda = idDeuda;
        }

        public double getDeuda() {
            return deuda;
        }

        public void setDeuda(double deuda) {
            this.deuda = deuda;
        }

        public double getImpuestos() {
            return impuestos;
        }

        public void setImpuestos(double impuestos) {
            this.impuestos = impuestos;
        }

        public double getMonto() {
            return monto;
        }

        public void setMonto(double monto) {
            this.monto = monto;
        }

        public void setMonto(int monto) {
            this.monto = monto;
        }

        public int getAnio() {
            return anio;
        }

        public void setAnio(int anio) {
            this.anio = anio;
        }

        public int getMes() {
            return mes;
        }

        public void setMes(int mes) {
            this.mes = mes;
        }

        public String getDescripccion() {
            return descripccion;
        }

        public void setDescripccion(String descripccion) {
            this.descripccion = descripccion;
        }

        public int getMeses() {
            return meses;
        }

        public void setMeses(int meses) {
            this.meses = meses;
        }

        public double getMoraImpuestos() {
            return moraImpuestos;
        }

        public void setMoraImpuestos(double moraImpuestos) {
            this.moraImpuestos = moraImpuestos;
        }

        public double getDescuento() {
            return descuento;
        }

        public void setDescuento(double descuento) {
            this.descuento = descuento;
        }

        public double getMora() {
            return mora;
        }

        public void setMora(double mora) {
            this.mora = mora;
        }

        public double getPago() {
            return pago;
        }

        public void setPago(double pago) {
            this.pago = pago;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.idDeudaEstado);
            dest.writeString(this.deudaEstado);
            dest.writeInt(this.correctas);
            dest.writeInt(this.idDeuda);
            dest.writeDouble(this.deuda);
            dest.writeDouble(this.impuestos);
            dest.writeDouble(this.monto);
            dest.writeInt(this.anio);
            dest.writeInt(this.mes);
            dest.writeString(this.descripccion);
            dest.writeInt(this.meses);
            dest.writeDouble(this.mora);
            dest.writeDouble(this.moraImpuestos);
            dest.writeDouble(this.descuento);
        }

        public LD() {
        }

        protected LD(Parcel in) {
            this.idDeudaEstado = in.readInt();
            this.deudaEstado = in.readString();
            this.correctas = in.readInt();
            this.idDeuda = in.readInt();
            this.deuda = in.readDouble();
            this.impuestos = in.readDouble();
            this.monto = in.readDouble();
            this.anio = in.readInt();
            this.mes = in.readInt();
            this.descripccion = in.readString();
            this.meses = in.readInt();
            this.mora = in.readDouble();
            this.moraImpuestos = in.readDouble();
            this.descuento = in.readDouble();
        }

        public static final Creator<LD> CREATOR = new Creator<LD>() {
            @Override
            public LD createFromParcel(Parcel source) {
                return new LD(source);
            }

            @Override
            public LD[] newArray(int size) {
                return new LD[size];
            }
        };
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.en);
        dest.writeList(this.lD);
        dest.writeString(this.m);
    }

    public PagosConductor() {
    }

    protected PagosConductor(Parcel in) {
        this.en = in.readInt();
        this.lD = new ArrayList<LD>();
        in.readList(this.lD, LD.class.getClassLoader());
        this.m = in.readString();
    }

    public static final Creator<PagosConductor> CREATOR = new Creator<PagosConductor>() {
        @Override
        public PagosConductor createFromParcel(Parcel source) {
            return new PagosConductor(source);
        }

        @Override
        public PagosConductor[] newArray(int size) {
            return new PagosConductor[size];
        }
    };

    @Override
    public String toString() {
        return "PagosConductor{" +
                "en=" + en +
                ", lD=" + lD +
                ", m='" + m + '\'' +
                '}';
    }
}
