package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by Marjorie on 26/06/2015.
 */
public class Eventualidad implements Serializable {

    int id;
    String descripcion;

    public Eventualidad(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Eventualidad() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
