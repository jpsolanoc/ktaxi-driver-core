package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by fabricio on 05/05/16.
 */
public class ResponseApi implements Serializable {
    private int estado;
    private int error;
    private String mensaje;

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "ResponseApi{" +
                "estado=" + estado +
                ", error=" + error +
                ", mensaje='" + mensaje + '\'' +
                '}';
    }
}