package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by John on 19/05/2016.
 */
public class CancelarSolicitud implements Serializable {

    private String placa;
    private String registroMunicipal;
    private int numeroUnidad;
    private String empresa;
    private boolean isCancelar;

    public CancelarSolicitud() {
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setRegistroMunicipal(String registroMunicipal) {
        this.registroMunicipal = registroMunicipal;
    }

    public void setNumeroUnidad(int numeroUnidad) {
        this.numeroUnidad = numeroUnidad;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public boolean isCancelar() {
        return isCancelar;
    }

    public void setCancelar(boolean cancelar) {
        isCancelar = cancelar;
    }
}
