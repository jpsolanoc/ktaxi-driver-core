package com.kradac.conductor.modelo;


import java.util.ArrayList;
import java.util.List;

public class Pais {

    private int idPais;
    private String pais;
    private List<Ciudad> lCiudades;

    public Pais(int idPais, String pais) {
        this.idPais = idPais;
        this.pais = pais;
    }

    public int getIdPais() {
        return idPais;
    }

    public void setIdPais(int idPais) {
        this.idPais = idPais;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public List<Ciudad> getlCiudades() {
        return lCiudades;
    }

    public void setlCiudades(List<Ciudad> lCiudades) {
        this.lCiudades = lCiudades;
    }

    public List<Ciudad> getlCiudad() {
        List<Ciudad> ciudad=new ArrayList<>();
        for(Ciudad c:lCiudades){
            c.getCiudad();
            ciudad.add(c);
        }
        return lCiudades;
    }

    @Override
    public String toString() {
        return "Pais{" +
                "idPais=" + idPais +
                ", pais='" + pais + '\'' +
                ", lCiudades=" + lCiudades +
                '}';
    }
}
