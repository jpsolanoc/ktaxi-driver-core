package com.kradac.conductor.modelo;

import java.util.List;

/**
 * Created by John on 04/01/2017.
 */

public class Cuenta {


    private int en;
    private List<C> c;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public List<C> getC() {
        return c;
    }

    public void setC(List<C> c) {
        this.c = c;
    }

    public static class C {
        private double dinero;
        private double saldo;
        private String e;
        private double propina;
        private int tipo;
        private String cuenta;
        private String cartera;
        private int idCuenta;

        public double getDinero() {
            return dinero;
        }

        public void setDinero(double dinero) {
            this.dinero = dinero;
        }

        public double getSaldo() {
            return saldo;
        }

        public void setSaldo(double saldo) {
            this.saldo = saldo;
        }

        public String getE() {
            return e;
        }

        public void setE(String e) {
            this.e = e;
        }

        public double getPropina() {
            return propina;
        }

        public void setPropina(double propina) {
            this.propina = propina;
        }

        public int getTipo() {
            return tipo;
        }

        public void setTipo(int tipo) {
            this.tipo = tipo;
        }

        public String getCuenta() {
            return cuenta;
        }

        public void setCuenta(String cuenta) {
            this.cuenta = cuenta;
        }

        public String getCartera() {
            return cartera;
        }

        public void setCartera(String cartera) {
            this.cartera = cartera;
        }

        public int getIdCuenta() {
            return idCuenta;
        }

        public void setIdCuenta(int idCuenta) {
            this.idCuenta = idCuenta;
        }
    }
}
