package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by John on 02/01/2017.
 */

public class Componente implements Serializable {
    private int t;
    private String nb;
    private String d;

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    public String getNb() {
        return nb;
    }

    public void setNb(String nb) {
        this.nb = nb;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    @Override
    public String toString() {
        return "Componente{" +
                "t=" + t +
                ", nb='" + nb + '\'' +
                ", d='" + d + '\'' +
                '}';
    }
}
