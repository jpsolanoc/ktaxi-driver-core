package com.kradac.conductor.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class DatosNotificacion implements Parcelable {


    private int estado ;
    private int idBoletin;
    private int leido;
    private int tipo;
    private boolean isPublicidad;
    private int T;
    private String linkPublicidad;
    private String asunto;
    private String boletin;
    private String url;
    private String vinculo;
    private String imgBoletin;
    private List<LP> lP;
    private String imgPublicidad;
    private String tituloPublicidad;

    public String getTituloPublicidad() {
        return tituloPublicidad;
    }

    public void setTituloPublicidad(String tituloPublicidad) {
        this.tituloPublicidad = tituloPublicidad;
    }

    public String getImgPublicidad() {
        return imgPublicidad;
    }

    public void setImgPublicidad(String imgPublicidad) {
        this.imgPublicidad = imgPublicidad;
    }

    public int getIdBoletin() {
        return idBoletin;
    }

    public void setIdBoletin(int idBoletin) {
        this.idBoletin = idBoletin;
    }

    public int getLeido() {
        return leido;
    }

    public void setLeido(int leido) {
        this.leido = leido;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getBoletin() {
        return boletin;
    }

    public void setBoletin(String boletin) {
        this.boletin = boletin;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVinculo() {
        return vinculo;
    }

    public void setVinculo(String vinculo) {
        this.vinculo = vinculo;
    }

    public String getImgBoletin() {
        return imgBoletin;
    }

    public void setImgBoletin(String imgBoletin) {
        this.imgBoletin = imgBoletin;
    }

    public int getT() {
        return T;
    }

    public void setT(int t) {
        T = t;
    }

    public List<LP> getLP() {
        return lP;
    }

    public void setLP(List<LP> lP) {
        this.lP = lP;
    }

    public static class LP implements Parcelable {
        private int id;
        private String pregunta;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPregunta() {
            return pregunta;
        }

        public void setPregunta(String pregunta) {
            this.pregunta = pregunta;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.id);
            dest.writeString(this.pregunta);
        }

        public LP() {
        }

        protected LP(Parcel in) {
            this.id = in.readInt();
            this.pregunta = in.readString();
        }

        public static final Creator<LP> CREATOR = new Creator<LP>() {
            @Override
            public LP createFromParcel(Parcel source) {
                return new LP(source);
            }

            @Override
            public LP[] newArray(int size) {
                return new LP[size];
            }
        };

        @Override
        public String toString() {
            return "LP{" +
                    "id=" + id +
                    ", pregunta='" + pregunta + '\'' +
                    '}';
        }
    }

    public boolean isPublicidad() {
        return isPublicidad;
    }

    public void setPublicidad(boolean publicidad) {
        isPublicidad = publicidad;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public DatosNotificacion() {
    }


    public String getLinkPublicidad() {
        return linkPublicidad;
    }

    public void setLinkPublicidad(String linkPublicidad) {
        this.linkPublicidad = linkPublicidad;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.estado);
        dest.writeInt(this.idBoletin);
        dest.writeInt(this.leido);
        dest.writeInt(this.tipo);
        dest.writeByte(this.isPublicidad ? (byte) 1 : (byte) 0);
        dest.writeInt(this.T);
        dest.writeString(this.linkPublicidad);
        dest.writeString(this.asunto);
        dest.writeString(this.boletin);
        dest.writeString(this.url);
        dest.writeString(this.vinculo);
        dest.writeString(this.imgBoletin);
        dest.writeTypedList(this.lP);
    }

    protected DatosNotificacion(Parcel in) {
        this.estado = in.readInt();
        this.idBoletin = in.readInt();
        this.leido = in.readInt();
        this.tipo = in.readInt();
        this.isPublicidad = in.readByte() != 0;
        this.T = in.readInt();
        this.linkPublicidad = in.readString();
        this.asunto = in.readString();
        this.boletin = in.readString();
        this.url = in.readString();
        this.vinculo = in.readString();
        this.imgBoletin = in.readString();
        this.lP = in.createTypedArrayList(LP.CREATOR);
    }

    public static final Creator<DatosNotificacion> CREATOR = new Creator<DatosNotificacion>() {
        @Override
        public DatosNotificacion createFromParcel(Parcel source) {
            return new DatosNotificacion(source);
        }

        @Override
        public DatosNotificacion[] newArray(int size) {
            return new DatosNotificacion[size];
        }
    };

    @Override
    public String toString() {
        return "DatosNotificacion{" +
                "estado=" + estado +
                ", idBoletin=" + idBoletin +
                ", leido=" + leido +
                ", tipo=" + tipo +
                ", isPublicidad=" + isPublicidad +
                ", T=" + T +
                ", linkPublicidad='" + linkPublicidad + '\'' +
                ", asunto='" + asunto + '\'' +
                ", boletin='" + boletin + '\'' +
                ", url='" + url + '\'' +
                ", vinculo='" + vinculo + '\'' +
                ", imgBoletin='" + imgBoletin + '\'' +
                ", lP=" + lP +
                ", imgPublicidad='" + imgPublicidad + '\'' +
                '}';
    }
}