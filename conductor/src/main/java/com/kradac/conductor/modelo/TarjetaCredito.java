package com.kradac.conductor.modelo;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

public class TarjetaCredito implements Parcelable {

    /*
    *
1 Visa
2 American Express
3 MasterCard
4 Diners Club
5 Bankcard
6 China UnionPay
7 Discover Card
8 InterPayment
9 JCB
10 Laser
11 Maestro
12 Dankort
13 Solo
14 Switch

    * */
    public static final  int TIPO_VISA=1;
    public static final  int TIPO_AMEX=2;
    public static final  int TIPO_MASTERCARD=3;
    public static final  int TIPO_DINERS_CLUB=4;
    public static final  int TIPO_DISCOVER=15;
    public static final  int TIPO_DEFAULT=0;

    private String number;
    private String con;
    private int type;
    private String expirationMonth;
    private String expirationYear;



    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNumberBase64() {
        return number;
    }

    public String getNumber() {
        byte[] data = Base64.decode(number, Base64.DEFAULT);
        String text = new String(data);
        return text;
    }

    public String getNumberMask() {
        byte[] data = Base64.decode(number, Base64.DEFAULT);
        String text = new String(data);
        if(text.length()>4){
            text=text.substring(text.length()-4,text.length());
        }
        return "**** **** "+text;
    }


    public void setNumber(String number) {
        this.number = number;
    }

    public String getCon() {
        return con;
    }

    public void setCon(String con) {
        this.con = con;
    }

    public String getExpirationMonth() {
        byte[] data = Base64.decode(expirationMonth, Base64.DEFAULT);
        String text = new String(data);
        return text;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getExpirationYear() {
        byte[] data = Base64.decode(expirationYear, Base64.DEFAULT);
        String text = new String(data);
        return text;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }


    public TarjetaCredito() {
    }

    public static int getTipoTarjeta(String numero){
        int tipo =TIPO_DEFAULT;
        int codigo = Integer.parseInt(numero.substring(0, 4));
        int codigoMastercar = Integer.parseInt(numero.substring(0, 3));

        if (codigoMastercar >= 510 && codigoMastercar <= 559) {
            //Mastercard
            tipo = TIPO_MASTERCARD;
        } else if (codigo >= 4000 && codigo <= 4999) {
            //Visa
            tipo = TIPO_VISA;
        } else if (codigo >= 3400 && codigo <= 3799) {
            //American Express
            tipo = TIPO_AMEX;
        } else if (codigo >= 3000 && codigo <= 3059) {
            //Diners Club
            tipo = TIPO_DINERS_CLUB;
        } else if (codigo == 6011) {
            //Discover
            tipo = TIPO_DISCOVER;
        }

        return tipo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.number);
        dest.writeString(this.con);
        dest.writeInt(this.type);
        dest.writeString(this.expirationMonth);
        dest.writeString(this.expirationYear);
    }

    protected TarjetaCredito(Parcel in) {
        this.number = in.readString();
        this.con = in.readString();
        this.type = in.readInt();
        this.expirationMonth = in.readString();
        this.expirationYear = in.readString();
    }

    public static final Creator<TarjetaCredito> CREATOR = new Creator<TarjetaCredito>() {
        @Override
        public TarjetaCredito createFromParcel(Parcel source) {
            return new TarjetaCredito(source);
        }

        @Override
        public TarjetaCredito[] newArray(int size) {
            return new TarjetaCredito[size];
        }
    };
}
