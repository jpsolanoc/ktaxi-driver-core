package com.kradac.conductor.modelo;

import java.util.List;

/**
 * Created by Ing.John Patricio Solano Cabrera on 8/1/2018.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class TransaccionesSaldoKtaxi {


    private int en;
    private List<LT> lT;
    private String m;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public List<LT> getLT() {
        return lT;
    }

    public void setLT(List<LT> lT) {
        this.lT = lT;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public static class LT {
        private double saldo;
        private String fecha_registro;
        private int iE;
        private String estado;
        private int iT;
        private String tipo;
        private int iR;
        private String razon;

        public double getSaldo() {
            return saldo;
        }

        public void setSaldo(double saldo) {
            this.saldo = saldo;
        }

        public String getFecha_registro() {
            return fecha_registro;
        }

        public void setFecha_registro(String fecha_registro) {
            this.fecha_registro = fecha_registro;
        }

        public int getIE() {
            return iE;
        }

        public void setIE(int iE) {
            this.iE = iE;
        }

        public String getEstado() {
            return estado;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }

        public int getIT() {
            return iT;
        }

        public void setIT(int iT) {
            this.iT = iT;
        }

        public String getTipo() {
            return tipo;
        }

        public void setTipo(String tipo) {
            this.tipo = tipo;
        }

        public int getIR() {
            return iR;
        }

        public void setIR(int iR) {
            this.iR = iR;
        }

        public String getRazon() {
            return razon;
        }

        public void setRazon(String razon) {
            this.razon = razon;
        }
    }
}
