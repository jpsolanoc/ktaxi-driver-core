package com.kradac.conductor.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Ing.John Patricio Solano Cabrera on 3/1/2018.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class SaldoKtaxi {

    public static class ItemSaldoKtaxi{

        private int en;
        private List<LS> lS;
        private String m;

        public int getEn() {
            return en;
        }

        public void setEn(int en) {
            this.en = en;
        }

        public List<LS> getLS() {
            return lS;
        }

        public void setLS(List<LS> lS) {
            this.lS = lS;
        }

        public String getM() {
            return m;
        }

        public void setM(String m) {
            this.m = m;
        }

        public static class LS {
            private String razon;
            private double saldo;

            public String getRazon() {
                return razon;
            }

            public void setRazon(String razon) {
                this.razon = razon;
            }

            public double getSaldo() {
                return saldo;
            }

            public void setSaldo(double saldo) {
                this.saldo = saldo;
            }
        }
    }

    public static class CompraServicios implements Parcelable {


        private int en;
        private List<LS> lS;
        private String m;

        public int getEn() {
            return en;
        }

        public void setEn(int en) {
            this.en = en;
        }

        public List<LS> getLS() {
            return lS;
        }

        public void setLS(List<LS> lS) {
            this.lS = lS;
        }

        public String getM() {
            return m;
        }

        public void setM(String m) {
            this.m = m;
        }

        public static class LS implements Parcelable {
            private int idEmpresa;
            private String empresa;
            private double latitud;
            private double longitud;
            private boolean isLugar;
            private double distancia;

            public int getIdEmpresa() {
                return idEmpresa;
            }

            public void setIdEmpresa(int idEmpresa) {
                this.idEmpresa = idEmpresa;
            }

            public String getEmpresa() {
                return empresa;
            }

            public void setEmpresa(String empresa) {
                this.empresa = empresa;
            }

            public double getLatitud() {
                return latitud;
            }

            public void setLatitud(double latitud) {
                this.latitud = latitud;
            }

            public double getLongitud() {
                return longitud;
            }

            public void setLongitud(double longitud) {
                this.longitud = longitud;
            }

            public boolean isLugar() {
                return isLugar;
            }

            public void setLugar(boolean lugar) {
                isLugar = lugar;
            }

            public double getDistancia() {
                return distancia;
            }

            public void setDistancia(double distancia) {
                this.distancia = distancia;
            }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(this.idEmpresa);
                dest.writeString(this.empresa);
                dest.writeDouble(this.latitud);
                dest.writeDouble(this.longitud);
            }

            public LS() {
            }

            protected LS(Parcel in) {
                this.idEmpresa = in.readInt();
                this.empresa = in.readString();
                this.latitud = in.readDouble();
                this.longitud = in.readDouble();
            }

            public static final Parcelable.Creator<LS> CREATOR = new Parcelable.Creator<LS>() {
                @Override
                public LS createFromParcel(Parcel source) {
                    return new LS(source);
                }

                @Override
                public LS[] newArray(int size) {
                    return new LS[size];
                }
            };
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.en);
            dest.writeTypedList(this.lS);
            dest.writeString(this.m);
        }

        public CompraServicios() {
        }

        protected CompraServicios(Parcel in) {
            this.en = in.readInt();
            this.lS = in.createTypedArrayList(LS.CREATOR);
            this.m = in.readString();
        }

        public static final Parcelable.Creator<CompraServicios> CREATOR = new Parcelable.Creator<CompraServicios>() {
            @Override
            public CompraServicios createFromParcel(Parcel source) {
                return new CompraServicios(source);
            }

            @Override
            public CompraServicios[] newArray(int size) {
                return new CompraServicios[size];
            }
        };
    }


}
