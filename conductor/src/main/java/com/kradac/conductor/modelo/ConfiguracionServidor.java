package com.kradac.conductor.modelo;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ing.John Patricio Solano Cabrera on 10/3/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class ConfiguracionServidor {


    @SerializedName("en")
    private int en;
    @SerializedName("dns")
    private Dns dns;
    @SerializedName("m")
    private String m;

    private String IpUsada;

    public String getIpUsada() {
        return IpUsada;
    }

    public void setIpUsada(String ipUsada) {
        IpUsada = ipUsada;
    }

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public Dns getDns() {
        return dns;
    }

    public void setDns(Dns dns) {
        this.dns = dns;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public static class Dns {
        @SerializedName("dnsN")
        private String dnsN;
        @SerializedName("en")
        private int en;
        @SerializedName("v")
        private String v;
        @SerializedName("m")
        private String m;
        @SerializedName("imgA")
        private String imgA;
        @SerializedName("imgU")
        private String imgU;
        @SerializedName("imgC")
        private String imgC;
        @SerializedName("dnsE")
        private String dnsE;
        @SerializedName("dnsD")
        private String dnsD;
        @SerializedName("sImgU")
        private String sImgU;
        @SerializedName("imgPub")
        private String imgPub;
        @SerializedName("imgSer")
        private String imgSer;

        public String getDnsN() {
            return dnsN;
        }

        public void setDnsN(String dnsN) {
            this.dnsN = dnsN;
        }

        public int getEn() {
            return en;
        }

        public void setEn(int en) {
            this.en = en;
        }

        public String getV() {
            return v;
        }

        public void setV(String v) {
            this.v = v;
        }

        public String getM() {
            return m;
        }

        public void setM(String m) {
            this.m = m;
        }

        public String getImgA() {
            return imgA;
        }

        public void setImgA(String imgA) {
            this.imgA = imgA;
        }

        public String getImgU() {
            return imgU;
        }

        public void setImgU(String imgU) {
            this.imgU = imgU;
        }

        public String getImgC() {
            return imgC;
        }

        public void setImgC(String imgC) {
            this.imgC = imgC;
        }

        public String getDnsE() {
            return dnsE;
        }

        public void setDnsE(String dnsE) {
            this.dnsE = dnsE;
        }

        public String getDnsD() {
            return dnsD;
        }

        public void setDnsD(String dnsD) {
            this.dnsD = dnsD;
        }

        public String getsImgU() {
            return sImgU;
        }

        public void setsImgU(String sImgU) {
            this.sImgU = sImgU;
        }

        public String getImgPub() {
            return imgPub;
        }

        public void setImgPub(String imgPub) {
            this.imgPub = imgPub;
        }

        public String getImgSer() {
            return imgSer;
        }

        public void setImgSer(String imgSer) {
            this.imgSer = imgSer;
        }

        @Override
        public String toString() {
            return "Dns{" +
                    "dnsN='" + dnsN + '\'' +
                    ", en=" + en +
                    ", v='" + v + '\'' +
                    ", m='" + m + '\'' +
                    ", imgA='" + imgA + '\'' +
                    ", imgU='" + imgU + '\'' +
                    ", imgC='" + imgC + '\'' +
                    ", dnsE='" + dnsE + '\'' +
                    ", dnsD='" + dnsD + '\'' +
                    ", sImgU='" + sImgU + '\'' +
                    ", imgPub='" + imgPub + '\'' +
                    ", imgSer='" + imgSer + '\'' +
                    '}';
        }
    }

    public static ConfiguracionServidor createConfiguracionServidor(String configuracionServidorJson) {
        Gson obGson = new Gson();
        return obGson.fromJson(configuracionServidorJson, ConfiguracionServidor.class);
    }

    public String serializeStringConfiguracionServer() {
        Gson configuracionServerGson = new Gson();
        return configuracionServerGson.toJson(this);
    }

    @Override
    public String toString() {
        return "ConfiguracionServidor{" +
                "en=" + en +
                ", dns=" + dns +
                ", m='" + m + '\'' +
                ", IpUsada='" + IpUsada + '\'' +
                '}';
    }
}
