package com.kradac.conductor.modelo;


import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by fabricio on 28/03/16.
 */
@Table(name = "CalendarHistorial", id = "_id")
public class CalendarHistorial extends Model {
    @Column(name = "registros")
    private int registros;
    @Column(name = "idCliente")
    private int idCliente;
    @Column(name = "mes")
    private int mes;
    @Column(name = "anio")
    private int anio;

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getRegistros() {
        return registros;
    }

    public void setRegistros(int registros) {
        this.registros = registros;
    }

    public static boolean verificarDescargaMesAnio(int mes,int anio,int idCliente) {
        try {
            CalendarHistorial calendarHistorial= new Select()
                    .from(CalendarHistorial.class)
                    .where("idCliente= ?", idCliente)
                    .where("anio= ?", anio)
                    .where("mes= ?", mes)
                    .executeSingle();
            if(calendarHistorial!=null){
                Log.e("verificarhMesAnio",calendarHistorial.toString());
                return true;
            }else{
                return false;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static List<CalendarHistorial> getAll(int idCliente) {
        try {
            return new Select()
                    .from(CalendarHistorial.class)
                    .where("idCliente= ?", idCliente)
                    .orderBy("id DESC")
                    .execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int count(int idCliente) {
        try {
            return new Select().from(CalendarHistorial.class).where("idCliente= ?", idCliente).count();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static CalendarHistorial get(int id) {
        try {
            List<CalendarHistorial> lista = new Select().from(CalendarHistorial.class).where("id=" + id).execute();
            if (lista.size() > 0) {
                return lista.get(0);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void deleteAll() {
        try{
            new Delete().from(CalendarHistorial.class).execute();
        }catch (SecurityException e){

        }

    }

    @Override
    public String toString() {
        return "CalendarHistorial{" +
                "registros=" + registros +
                ", idCliente=" + idCliente +
                ", mes=" + mes +
                ", anio='" + anio + '\'' +
                '}';
    }
}
