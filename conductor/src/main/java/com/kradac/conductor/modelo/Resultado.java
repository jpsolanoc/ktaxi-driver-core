package com.kradac.conductor.modelo;

import java.util.List;

public class Resultado {

    private int en;
    private List<Pais> lPaises;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public List<Pais> getlPaises() {
        return lPaises;
    }

    public void setlPaises(List<Pais> lPaises) {
        this.lPaises = lPaises;
    }

    @Override
    public String toString() {
        return "Resultado{" +
                "en=" + en +
                ", lPaises=" + lPaises +
                '}';
    }
}
