package com.kradac.conductor.modelo;

/**
 * Created by John on 18/11/2016.
 */

public class Boletin {


    private int estado;
    private String asunto;
    private String boletin;
    private boolean isPublicidad;
    private String linkPublicidad;

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }


    public boolean isPublicidad() {
        return isPublicidad;
    }

    public void setPublicidad(boolean publicidad) {
        isPublicidad = publicidad;
    }

    public String getLinkPublicidad() {
        return linkPublicidad;
    }

    public void setLinkPublicidad(String linkPublicidad) {
        this.linkPublicidad = linkPublicidad;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getBoletin() {
        return boletin;
    }

    public void setBoletin(String boletin) {
        this.boletin = boletin;
    }

    @Override
    public String toString() {
        return "Boletin{" +
                "estado=" + estado +
                ", asunto=" + asunto +
                ", boletin=" + boletin +
                ", isPublicidad=" + isPublicidad +
                ", linkPublicidad='" + linkPublicidad + '\'' +
                '}';
    }

}
