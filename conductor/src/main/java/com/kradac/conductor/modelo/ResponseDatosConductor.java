package com.kradac.conductor.modelo;

/**
 * Created by dev on 06/01/17.
 */

public class ResponseDatosConductor {
    private int en;
    private DatosTaxiAtendio dT;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public DatosTaxiAtendio getdT() {
        return dT;
    }

    public void setdT(DatosTaxiAtendio dT) {
        this.dT = dT;
    }

    @Override
    public String toString() {
        return "ResponseDatosConductor{" +
                "en=" + en +
                ", dT=" + dT +
                '}';
    }

    public class DatosTaxiAtendio{
        private String img;
        private String nB;
        private String t;

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getnB() {
            return nB;
        }

        public void setnB(String nB) {
            this.nB = nB;
        }

        public String getT() {
            return t;
        }

        public void setT(String t) {
            this.t = t;
        }

        @Override
        public String toString() {
            return "DatosTaxiAtendio{" +
                    "img='" + img + '\'' +
                    ", nB='" + nB + '\'' +
                    ", t='" + t + '\'' +
                    '}';
        }
    }
}
