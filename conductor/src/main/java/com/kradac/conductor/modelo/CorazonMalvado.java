package com.kradac.conductor.modelo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by DellKradac on 01/02/2018.
 */
@Table(name = "CorazonMalvado", id = "_id")
public class CorazonMalvado extends Model {

    private static final String TAG = CorazonMalvado.class.getName();
    @Column(name = "idUsuario")
    private int idUsuario;
    @Column(name = "paquete")
    private String paquete;
    @Column(name = "isEnvio")
    private boolean isEnvio;
    @Column(name = "isInstalada")
    private boolean isInstalada;
    @Column(name = "estadoInstalada")
    private int estadoInstalada;
    @Column(name = "idAplicativo")
    private int idAplicativo;
    /**
     * 0 Nada
     * 1 cambiar ocupado
     * 2 mostrar dialogo
     * 3 mostrar dialogo y cierre el aplicativo
     */
    @Column(name = "tipoAccion")
    private int tipoAccion;

    public int getTipoAccion() {
        return tipoAccion;
    }

    public void setTipoAccion(int tipoAccion) {
        this.tipoAccion = tipoAccion;
    }

    public String getPaquete() {
        return paquete;
    }

    public void setPaquete(String paquete) {
        this.paquete = paquete;
    }

    public boolean isEnvio() {
        return isEnvio;
    }

    public void setEnvio(boolean envio) {
        isEnvio = envio;
    }

    public int getEstadoInstalada() {
        return estadoInstalada;
    }

    public void setEstadoInstalada(int estadoInstalada) {
        this.estadoInstalada = estadoInstalada;
    }

    public int getIdAplicativo() {
        return idAplicativo;
    }

    public void setIdAplicativo(int idAplicativo) {
        this.idAplicativo = idAplicativo;
    }

    public boolean isInstalada() {
        return isInstalada;
    }

    public void setInstalada(boolean instalada) {
        isInstalada = instalada;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }


    public static List<CorazonMalvado> getAll() {
        return new Select()
                .from(CorazonMalvado.class)
                .execute();
    }

    public static CorazonMalvado isDentroLista(String paquete) {
        return new Select()
                .from(CorazonMalvado.class)
                .where("paquete = ?", paquete)
                .executeSingle();
    }

    public static void borrarTodos() {
        if (getAll().size() > 0) {
            for (CorazonMalvado data : getAll()) {
                data.delete();
            }
        }
    }

    public static void borrarYaNoEsta(List<String> paquetes) {
        for (CorazonMalvado c : getAll()) {
            boolean isEsta = false;
            for (int i = 0; i < paquetes.size(); i++) {
                if (c.getPaquete().equals(paquetes.get(i))) {
                    isEsta = true;
                }
            }
            if (!isEsta) {
                c.delete();
            }
        }
    }


    @Override
    public String toString() {
        return "CorazonMalvado{" +
                "idUsuario=" + idUsuario +
                ", paquete='" + paquete + '\'' +
                ", isEnvio=" + isEnvio +
                ", isInstalada=" + isInstalada +
                ", estadoInstalada=" + estadoInstalada +
                ", idAplicativo=" + idAplicativo +
                ", tipoAccion=" + tipoAccion +
                '}';
    }
}
