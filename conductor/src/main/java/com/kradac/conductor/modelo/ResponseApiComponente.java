package com.kradac.conductor.modelo;

/**
 * Created by John on 02/01/2017.
 */

public class ResponseApiComponente {
    private  int en;
    private String m;
    private String error;
    private int t;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "ResponseApiComponente{" +
                "en=" + en +
                ", m='" + m + '\'' +
                ", error='" + error + '\'' +
                ", t=" + t +
                '}';
    }
}
