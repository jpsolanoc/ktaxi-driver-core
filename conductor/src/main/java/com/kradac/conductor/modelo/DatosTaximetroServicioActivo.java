package com.kradac.conductor.modelo;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.kradac.conductor.extras.VariablesGlobales;

/**
 * Created by Ing.John Patricio Solano Cabrera on 6/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class DatosTaximetroServicioActivo {


    private int en;

    private R r;

    private boolean isTaximetro;

    public boolean isTaximetro() {
        return isTaximetro;
    }

    public void setTaximetro(boolean taximetro) {
        isTaximetro = taximetro;
    }

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public R getR() {
        return r;
    }

    public void setR(R r) {
        this.r = r;
    }

    public static class R {
        private int visible;  // 1 Visible cualquier otro numero invisible.
        //km_rerorrido_diurno
        private double kmD;
        //km_rerorrido_nocturno
        private double kmN;
        //minuto_espera_diurno
        private double minD;
        //minuto_espera_nocturno
        private double minN;
        //arranque_diurno
        private double aD;
        //arranque_nocturno
        private double aN;
        // carrera_minima_diurno
        private double cD;
        //carrera_minima_nocturno
        private double cN;
        //hora_diurno
        private String hD;
        //hora_diurno
        private String hN;

        private double b = 8; //banderazo

        private double vV = 0.01;

        private int tipo;

        private int editarCostoC;


        public double getB() {
            return b;
        }

        public void setB(double b) {
            this.b = b;
        }

        public double getvV() {
            return vV;
        }

        public void setvV(double vV) {
            this.vV = vV;
        }

        public int getVisible() {
            return visible;
        }

        public void setVisible(int visible) {
            this.visible = visible;
        }

        public double getKmD() {
            return kmD;
        }

        public void setKmD(double kmD) {
            this.kmD = kmD;
        }

        public double getKmN() {
            return kmN;
        }

        public void setKmN(double kmN) {
            this.kmN = kmN;
        }

        public double getMinD() {
            return minD;
        }

        public void setMinD(double minD) {
            this.minD = minD;
        }

        public double getMinN() {
            return minN;
        }

        public void setMinN(double minN) {
            this.minN = minN;
        }

        public double getaD() {
            return aD;
        }

        public void setaD(double aD) {
            this.aD = aD;
        }

        public double getaN() {
            return aN;
        }

        public void setaN(double aN) {
            this.aN = aN;
        }

        public double getcD() {
            return cD;
        }

        public void setcD(double cD) {
            this.cD = cD;
        }

        public double getcN() {
            return cN;
        }

        public void setcN(double cN) {
            this.cN = cN;
        }

        public String gethD() {
            return hD;
        }

        public void sethD(String hD) {
            this.hD = hD;
        }

        public String gethN() {
            return hN;
        }

        public void sethN(String hN) {
            this.hN = hN;
        }

        public int getTipo() {
            return tipo;
        }

        public void setTipo(int tipo) {
            this.tipo = tipo;
        }

        public int getEditarCostoC() {
            return editarCostoC;
        }

        public void setEditarCostoC(int editarCostoC) {
            this.editarCostoC = editarCostoC;
        }


    }

    public static DatosTaximetroServicioActivo createControlador(String datosTaximetro) {
        Gson obGson = new Gson();
        return obGson.fromJson(datosTaximetro, DatosTaximetroServicioActivo.class);
    }

    public String serializeDatosTaximetro() {
        Gson DatosTaximetroGson = new Gson();
        return DatosTaximetroGson.toJson(this);
    }

    public static void guardarDatosTaximetro(Context context, String datosTaximetro) {
        SharedPreferences spTaximetro = context.getSharedPreferences(VariablesGlobales.SP_TAXIMETRO_SERVICIO_ACTIVO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = spTaximetro.edit();
        editor.putString(VariablesGlobales.SP_TAXIMETRO_DATOS_SERVICIO_ACTIVO, datosTaximetro);
        editor.apply();
    }

    public static DatosTaximetroServicioActivo obtenerDatosTaximetro(Context context) {
        SharedPreferences spTaximetro = context.getSharedPreferences(VariablesGlobales.SP_TAXIMETRO_SERVICIO_ACTIVO, Context.MODE_PRIVATE);
        final String controladorS = spTaximetro.getString(VariablesGlobales.SP_TAXIMETRO_DATOS_SERVICIO_ACTIVO, null);
        if (controladorS != null && !controladorS.isEmpty()) {
            return DatosTaximetroServicioActivo.createControlador(controladorS);
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return "DatosTaximetroServicioActivo{" +
                "en=" + en +
                ", r=" + r +
                ", isTaximetro=" + isTaximetro +
                '}';
    }
}
