package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by John on 12/10/2016.
 */

public class PositionTrafico implements Serializable {
    private double lt;
    private double lg;

    public double getLt() {
        return lt;
    }

    public void setLt(double lt) {
        this.lt = lt;
    }

    public double getLg() {
        return lg;
    }

    public void setLg(double lg) {
        this.lg = lg;
    }

    @Override
    public String toString() {
        return "Position{" +
                "lt=" + lt +
                ", lg=" + lg +
                '}';
    }
}
