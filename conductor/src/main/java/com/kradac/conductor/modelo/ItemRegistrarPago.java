package com.kradac.conductor.modelo;

/**
 * Created by Ing.John Patricio Solano Cabrera on 22/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class ItemRegistrarPago {


    private String pagareDe;
    private String monto;

    public ItemRegistrarPago(String pagareDe, String monto) {
        this.pagareDe = pagareDe;
        this.monto = monto;
    }

    public String getPagareDe() {
        return pagareDe;
    }

    public void setPagareDe(String pagareDe) {
        this.pagareDe = pagareDe;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }
}
