package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by Technovibe on 17-04-2015.
 */
public class ChatMessage implements Serializable {
    private long id;
    private boolean isMe;
    private int tipo;
    private int tipoDesde;
    private String nameArchivo;
    private String message;
    private String dateTime;

    public boolean isMe() {
        return isMe;
    }

    public void setMe(boolean isMe) {
        this.isMe = isMe;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getNameArchivo() {
        return nameArchivo;
    }

    public void setNameArchivo(String nameArchivo) {
        this.nameArchivo = nameArchivo;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean getIsme() {
        return isMe;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return dateTime;
    }

    public void setDate(String dateTime) {
        this.dateTime = dateTime;
    }

    public int getTipoDesde() {
        return tipoDesde;
    }

    public void setTipoDesde(int tipoDesde) {
        this.tipoDesde = tipoDesde;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ChatMessage) {
            ChatMessage tmpChatMessage = (ChatMessage) obj;
            return super.equals(tmpChatMessage) && this.id == tmpChatMessage.id && this.message == tmpChatMessage.message;
        } else {
            return false;
        }
    }
}
