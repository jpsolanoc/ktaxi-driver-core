package com.kradac.conductor.modelo;

import java.util.List;

/**
 * Created by DellKradac on 17/02/2018.
 */

public class ConsultarSaldoTarjetaCredito {


    private List<LS> lS;
    private int en;
    private String m;

    public List<LS> getLS() {
        return lS;
    }

    public void setLS(List<LS> lS) {
        this.lS = lS;
    }

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public static class LS {
        private double credito;
        private String razon;

        public double getCredito() {
            return credito;
        }

        public void setCredito(double credito) {
            this.credito = credito;
        }

        public String getRazon() {
            return razon;
        }

        public void setRazon(String razon) {
            this.razon = razon;
        }
    }
}
