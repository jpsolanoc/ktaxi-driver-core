package com.kradac.conductor.modelo;


public class Respuesta {

    private int en;
    private String m;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    @Override
    public String toString() {
        return "Respuesta{" +
                "en=" + en +
                ", m='" + m + '\'' +
                '}';
    }
}
