package com.kradac.conductor.modelo;

import java.io.Serializable;
import java.util.List;
/**
 * Created by John on 12/10/2016.
 */

public class DatosTrafico implements Serializable {
    private int en;
    private String m;
    private List<PositionTrafico>t;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public List<PositionTrafico> getT() {
        return t;
    }

    public void setT(List<PositionTrafico> t) {
        this.t = t;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    @Override
    public String toString() {
        return "DatosTrafico{" +
                "en=" + en +
                ", m='" + m + '\'' +
                ", t=" + t +
                '}';
    }
}