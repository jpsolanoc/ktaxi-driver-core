package com.kradac.conductor.modelo;

import java.util.List;

/**
 * Created by Ing.John Patricio Solano Cabrera on 25/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class EntidadFinanciera {


    private int en;
    private String m;
    private List<LT> lT;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public List<LT> getLT() {
        return lT;
    }

    public void setLT(List<LT> lT) {
        this.lT = lT;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }


    public static class LT {
        private int idEntidad;
        private String nombre;
        private String descripcion;
        private String cuenta;
        private String label;

        public int getIdEntidad() {
            return idEntidad;
        }

        public void setIdEntidad(int idEntidad) {
            this.idEntidad = idEntidad;
        }

        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }

        public String getDescripcion() {
            return descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.descripcion = descripcion;
        }

        public String getCuenta() {
            return cuenta;
        }

        public void setCuenta(String cuenta) {
            this.cuenta = cuenta;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        @Override
        public String toString() {
            return "LT{" +
                    "idEntidad=" + idEntidad +
                    ", nombre='" + nombre + '\'' +
                    ", descripcion='" + descripcion + '\'' +
                    ", cuenta='" + cuenta + '\'' +
                    ", label='" + label + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "EntidadFinanciera{" +
                "en=" + en +
                ", m='" + m + '\'' +
                ", lT=" + lT +
                '}';
    }
}
