package com.kradac.conductor.modelo;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.kradac.conductor.extras.VariablesGlobales;

import java.util.List;

public class RespuestaConfiguracion {


    private List<String> t;
    private List<Cnf> cnf;
    private String mensaje;
    private int estado;

    public List<String> getT() {
        return t;
    }

    public void setT(List<String> t) {
        this.t = t;
    }

    public List<Cnf> getCnf() {
        return cnf;
    }

    public void setCnf(List<Cnf> cnf) {
        this.cnf = cnf;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public static class Cnf {
        private int h;
        private String vd;
        private String nb;
        private int id;

        public int getH() {
            return h;
        }

        public void setH(int h) {
            this.h = h;
        }

        public String getVd() {
            return vd;
        }

        public void setVd(String vd) {
            this.vd = vd;
        }

        public String getNb() {
            return nb;
        }

        public void setNb(String nb) {
            this.nb = nb;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }


    public static RespuestaConfiguracion createDatosSolicitud(String respuestaConfiguracion) {
        Gson obGson = new Gson();
        return obGson.fromJson(respuestaConfiguracion, RespuestaConfiguracion.class);
    }

    public static String textActivarIconoEstadoGpsSolicitud(Context context) {
        SharedPreferences spParametrosConfiguracion = context.getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        RespuestaConfiguracion respuestaConfiguracion = createDatosSolicitud(spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION_GLOBAL, ""));
        if (respuestaConfiguracion != null) {
            for (Cnf configuracion : respuestaConfiguracion.getCnf()) {
                if (configuracion.getId() == 36) {
                    if (configuracion.getH() == 1) {
                        return configuracion.getVd();
                    }
                }
            }
        }
        return "Se encuentra en el lugar donde se realizo la solicitud.(Opcional)";
    }

    public static boolean isActivarIconoEstadoGpsSolicitud(Context context) {
        SharedPreferences spParametrosConfiguracion = context.getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        RespuestaConfiguracion respuestaConfiguracion = createDatosSolicitud(spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION_GLOBAL, ""));
        if (respuestaConfiguracion != null) {
            for (Cnf configuracion : respuestaConfiguracion.getCnf()) {
                if (configuracion.getId() == 36) {
                    if (configuracion.getH() == 1) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static int numeroDecimales(Context context) {
        SharedPreferences spParametrosConfiguracion = context.getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        RespuestaConfiguracion respuestaConfiguracion = createDatosSolicitud(spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION_GLOBAL, ""));
        if (respuestaConfiguracion != null) {
            for (Cnf configuracion : respuestaConfiguracion.getCnf()) {
                if (configuracion.getId() == 37) {
                    if (configuracion.getH() == 1) {
                        return Integer.parseInt(configuracion.vd);
                    }
                }
            }
        }
        return 2;
    }

    public static boolean viewReportePirata(Context context) {
        SharedPreferences spParametrosConfiguracion = context.getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        RespuestaConfiguracion respuestaConfiguracion = createDatosSolicitud(spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION_GLOBAL, ""));
        if (respuestaConfiguracion != null) {
            for (Cnf configuracion : respuestaConfiguracion.getCnf()) {
                if (configuracion.getId() == 38) {
                    if (configuracion.getH() == 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }


}
