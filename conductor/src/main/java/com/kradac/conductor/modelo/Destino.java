package com.kradac.conductor.modelo;

import android.os.Parcel;
import android.os.Parcelable;

public class Destino implements Parcelable {


    private String desT;//Tiempo
    private Double desC;//Costo
    private Integer desDis;//Distancia
    private String desCs;
    private String desCp;
    private String desRef;
    private String desBar; //destino barrio
    private double ltD;
    private double lgD;

    public double getLtD() {
        return ltD;
    }

    public void setLtD(double ltD) {
        this.ltD = ltD;
    }

    public double getLgD() {
        return lgD;
    }

    public void setLgD(double lgD) {
        this.lgD = lgD;
    }

    public String getDesT() {
        return desT;
    }

    public void setDesT(String desT) {
        this.desT = desT;
    }

    public Double getDesC() {
        return desC;
    }

    public void setDesC(Double desC) {
        this.desC = desC;
    }

    public Integer getDesDis() {
        return desDis;
    }

    public void setDesDis(Integer desDis) {
        this.desDis = desDis;
    }

    public String getDesCs() {
        return desCs;
    }

    public void setDesCs(String desCs) {
        this.desCs = desCs;
    }

    public String getDesCp() {
        return desCp;
    }

    public void setDesCp(String desCp) {
        this.desCp = desCp;
    }

    public String getDesRef() {
        return desRef;
    }

    public void setDesRef(String desRef) {
        this.desRef = desRef;
    }

    public String getDesBar() {
        return desBar;
    }

    public void setDesBar(String desBar) {
        this.desBar = desBar;
    }

    @Override
    public String toString() {
        return "Destino{" +
                "desT='" + desT + '\'' +
                ", desC=" + desC +
                ", desDis=" + desDis +
                ", desCs='" + desCs + '\'' +
                ", desCp='" + desCp + '\'' +
                ", desRef='" + desRef + '\'' +
                ", desBar='" + desBar + '\'' +
                ", ltD=" + ltD +
                ", lgD=" + lgD +
                '}';
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.desT);
        dest.writeValue(this.desC);
        dest.writeValue(this.desDis);
        dest.writeString(this.desCs);
        dest.writeString(this.desCp);
        dest.writeString(this.desRef);
        dest.writeString(this.desBar);
        dest.writeDouble(this.ltD);
        dest.writeDouble(this.lgD);
    }

    public Destino() {
    }

    protected Destino(Parcel in) {
        this.desT = in.readString();
        this.desC = (Double) in.readValue(Double.class.getClassLoader());
        this.desDis = (Integer) in.readValue(Integer.class.getClassLoader());
        this.desCs = in.readString();
        this.desCp = in.readString();
        this.desRef = in.readString();
        this.desBar = in.readString();
        this.ltD = in.readDouble();
        this.lgD = in.readDouble();
    }

    public static final Parcelable.Creator<Destino> CREATOR = new Parcelable.Creator<Destino>() {
        @Override
        public Destino createFromParcel(Parcel source) {
            return new Destino(source);
        }

        @Override
        public Destino[] newArray(int size) {
            return new Destino[size];
        }
    };
}
