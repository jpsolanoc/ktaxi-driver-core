package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by John on 02/01/2017.
 */

public class ComponenteBase implements Serializable {
    private int idC;
    private int en;
    private String t;
    private String dC;
    private String dL;
    private double lt;
    private double lg;
    private String img;

    public int getIdC() {
        return idC;
    }

    public void setIdC(int idC) {
        this.idC = idC;
    }

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getdC() {
        return dC;
    }

    public void setdC(String dC) {
        this.dC = dC;
    }

    public String getdL() {
        return dL;
    }

    public void setdL(String dL) {
        this.dL = dL;
    }

    public double getLt() {
        return lt;
    }

    public void setLt(double lt) {
        this.lt = lt;
    }

    public double getLg() {
        return lg;
    }

    public void setLg(double lg) {
        this.lg = lg;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    @Override
    public String toString() {
        return "ComponenteBase{" +
                "idC=" + idC +
                ", en=" + en +
                ", t='" + t + '\'' +
                ", dC='" + dC + '\'' +
                ", dL='" + dL + '\'' +
                ", lt=" + lt +
                ", lg=" + lg +
                ", img='" + img + '\'' +
                '}';
    }
}