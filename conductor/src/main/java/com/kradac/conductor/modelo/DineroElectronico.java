package com.kradac.conductor.modelo;

/**
 * Created by John on 18/11/2016.
 */

public class DineroElectronico {

    private String resultText;

    public String getResultText() {
        return resultText;
    }

    public void setResultText(String resultText) {
        this.resultText = resultText;
    }

    @Override
    public String toString() {
        return "DineroElectronico{" +
                "resultText='" + resultText + '\'' +
                '}';
    }
}
