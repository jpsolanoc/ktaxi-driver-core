package com.kradac.conductor.modelo;

import java.util.List;

/**
 * Created by DellKradac on 17/02/2018.
 */

public class ConsultarMovimientos {


    private List<LT> lT;
    private int en;
    private String m;

    public List<LT> getLT() {
        return lT;
    }

    public void setLT(List<LT> lT) {
        this.lT = lT;
    }

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public static class LT {
        private String razon;
        private int iR;
        private String tipo;
        private int iT;
        private String estado;
        private int iE;
        private String fecha_registro;
        private double credito;
        private int reverso;

        public String getRazon() {
            return razon;
        }

        public void setRazon(String razon) {
            this.razon = razon;
        }

        public int getIR() {
            return iR;
        }

        public void setIR(int iR) {
            this.iR = iR;
        }

        public String getTipo() {
            return tipo;
        }

        public void setTipo(String tipo) {
            this.tipo = tipo;
        }

        public int getIT() {
            return iT;
        }

        public void setIT(int iT) {
            this.iT = iT;
        }

        public String getEstado() {
            return estado;
        }

        public void setEstado(String estado) {
            this.estado = estado;
        }

        public int getIE() {
            return iE;
        }

        public void setIE(int iE) {
            this.iE = iE;
        }

        public String getFecha_registro() {
            return fecha_registro;
        }

        public void setFecha_registro(String fecha_registro) {
            this.fecha_registro = fecha_registro;
        }

        public double getCredito() {
            return credito;
        }

        public void setCredito(double credito) {
            this.credito = credito;
        }

        public int getReverso() {
            return reverso;
        }

        public void setReverso(int reverso) {
            this.reverso = reverso;
        }
    }
}
