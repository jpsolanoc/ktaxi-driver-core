package com.kradac.conductor.modelo;

/**
 * Created by DellKradac on 23/02/2018.
 */

public class ResponseMapboxToken {

    private String code;

    public boolean isTokenValid() {
        if (code == null) return false;
        return code.equals("TokenValid");
    }
}
