package com.kradac.conductor.modelo;

/**
 * Created by John on 29/11/2016.
 */

public class MensajeGenerico {

    private int estado;
    private String mensaje;

    private int en;
    private String m;

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    @Override
    public String toString() {
        return "MensajeGenerico{" +
                "estado=" + estado +
                ", mensaje='" + mensaje + '\'' +
                ", en=" + en +
                ", m='" + m + '\'' +
                '}';
    }
}
