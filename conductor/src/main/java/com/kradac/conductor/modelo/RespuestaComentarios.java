package com.kradac.conductor.modelo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ing.John Patricio Solano Cabrera on 12/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class RespuestaComentarios {


    @SerializedName("en")
    private int en;
    @SerializedName("m")
    private String m;
    @SerializedName("ob")
    private List<Ob> ob;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public List<Ob> getOb() {
        return ob;
    }

    public void setOb(List<Ob> ob) {
        this.ob = ob;
    }

    public static class Ob {
        @SerializedName("v")
        private int v;
        @SerializedName("oB")
        private String oB;

        public int getV() {
            return v;
        }

        public void setV(int v) {
            this.v = v;
        }

        public String getoB() {
            return oB;
        }

        public void setoB(String oB) {
            this.oB = oB;
        }

        @Override
        public String toString() {
            return "Ob{" +
                    "v=" + v +
                    ", oB='" + oB + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "RespuestaComentarios{" +
                "en=" + en +
                ", m='" + m + '\'' +
                ", ob=" + ob +
                '}';
    }
}
