package com.kradac.conductor.modelo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.util.SQLiteUtils;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ing.John Patricio Solano Cabrera on 26/7/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */
@Table(name = "VideosPropaganda", id = "_id")
public class VideosPropaganda extends Model implements Serializable {

    @Column(name = "nombreVideo", unique = true)
    private String nombreVideo;

    public String getNombreVideo() {
        return nombreVideo;
    }

    public void setNombreVideo(String nombreVideo) {
        this.nombreVideo = nombreVideo;
    }

    public static List<VideosPropaganda> listaVideos() {
        try {
            return new Select()
                    .from(VideosPropaganda.class)
                    .execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isVideo(String nombre) {
        try {
            for (VideosPropaganda v : listaVideos()) {
                if (v.getNombreVideo().equals(nombre)) {
                    return true;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    public static void deleteVideo(String name){
        try {
            List<VideosPropaganda> video = new Select()
                    .from(VideosPropaganda.class)
                    .where("nombreVideo = ?", name)
                    .execute();
            if (!video.isEmpty()){
                video.get(0).delete();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void removerVideos(List<VideosPropaganda> videosRemover) {
        try {
            for (VideosPropaganda p : videosRemover) {
                p.delete();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

}
