package com.kradac.conductor.modelo;

import java.io.Serializable;

public class Sugerencia implements Serializable {
    private int id_sugerencia;
    private int id_usuario;
    private int pregunta1;
    private int pregunta2;
    private int pregunta3;
    private String sugerencia;
    private String usuario;
    private String correo;
    private String celular;
    private int estado;
    private String mensaje;

    public Sugerencia(int id_sugerencia, int id_usuario, int pregunta1, int pregunta2, int pregunta3, String sugerencia, String usuario, String correo, String celular) {
        this.id_sugerencia = id_sugerencia;
        this.id_usuario = id_usuario;
        this.pregunta1 = pregunta1;
        this.pregunta2 = pregunta2;
        this.pregunta3 = pregunta3;
        this.sugerencia = sugerencia;
        this.usuario = usuario;
        this.correo = correo;
        this.celular = celular;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public String getSugerencia() {
        return sugerencia;
    }

    public void setSugerencia(String sugerencia) {
        this.sugerencia = sugerencia;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Sugerencia{" +
                "id_sugerencia=" + id_sugerencia +
                ", id_usuario=" + id_usuario +
                ", pregunta1=" + pregunta1 +
                ", pregunta2=" + pregunta2 +
                ", pregunta3=" + pregunta3 +
                ", sugerencia='" + sugerencia + '\'' +
                ", usuario='" + usuario + '\'' +
                ", correo='" + correo + '\'' +
                ", celular='" + celular + '\'' +
                ", estado=" + estado +
                ", mensaje='" + mensaje + '\'' +
                '}';
    }
}
