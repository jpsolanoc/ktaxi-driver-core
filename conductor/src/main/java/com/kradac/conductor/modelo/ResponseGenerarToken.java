package com.kradac.conductor.modelo;

public class ResponseGenerarToken extends ResponseApiCorto {
    private String con;
    private int op;
    private int sum;
    private int mul;
    private int div;

    public String getCon() {
        return con;
    }

    public void setCon(String con) {
        this.con = con;
    }

    public int getOp() {
        return op;
    }

    public void setOp(int op) {
        this.op = op;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getMul() {
        return mul;
    }

    public void setMul(int mul) {
        this.mul = mul;
    }

    public int getDiv() {
        return div;
    }

    public void setDiv(int div) {
        this.div = div;
    }

    @Override
    public String toString() {
        return "ResponseGenerarToken{" +
                ", con='" + con + '\'' +
                ", op=" + op +
                ", sum=" + sum +
                ", mul=" + mul +
                ", div=" + div +
                '}';
    }
}
