package com.kradac.conductor.modelo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import com.activeandroid.query.Delete;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ing.John Patricio Solano Cabrera on 25/4/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */
@Table(name = "PosicionNueva", id = "_id")
public class PosicionNueva extends Model implements Serializable {
    @Column(name = "idVehiculo")
    private int idVehiculo;
    @Column(name = "fecha",unique = true)
    private String fecha; //2012/02/12
    @Column(name = "idEquipo")
    private int idEquipo;
    @Column(name = "latitud")
    private double latitud; //5 decimales
    @Column(name = "longitud")
    private double longitud; // 5 decilamles
    @Column(name = "altitud")
    private int altitud; //entero
    @Column(name = "velocidad")
    private int velocidad; //entero
    @Column(name = "acury")
    private int acury; // tabla de estados.
    @Column(name = "direccion")
    private int direccion; // maximo de 32767
    @Column(name = "bateria")
    private int bateria; //maximo 127
    @Column(name = "conexion")
    private int conexion; // 0 deconocido 1 wifi  2 datos 3  USSD
    @Column(name = "gps")
    private int gps; // bit uno o cero
    @Column(name = "estado")
    private int estado; // int
    @Column(name = "temperatura")
    private int temperatura; // maximo 127
    @Column(name = "consumo")
    private double consumo; // maximo 2 decimales MB
    @Column(name = "tipoRed")
    private double tipoRed; // maximo 2 decimales MB



    public PosicionNueva() {
    }

    public int getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(int idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getAltitud() {
        return altitud;
    }

    public void setAltitud(int altitud) {
        this.altitud = altitud;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }

    public int getAcury() {
        return acury;
    }

    public void setAcury(int acury) {
        this.acury = acury;
    }

    public int getDireccion() {
        return direccion;
    }

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }

    public int getBateria() {
        return bateria;
    }

    public void setBateria(int bateria) {
        this.bateria = bateria;
    }

    public int getConexion() {
        return conexion;
    }

    public void setConexion(int conexion) {
        this.conexion = conexion;
    }

    public int getGps() {
        return gps;
    }

    public void setGps(int gps) {
        this.gps = gps;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(int temperatura) {
        this.temperatura = temperatura;
    }

    public double getConsumo() {
        return consumo;
    }

    public void setConsumo(double consumo) {
        this.consumo = consumo;
    }


    public static List<PosicionNueva> listaPosiciones(){
        try {
            return new Select()
                    .from(PosicionNueva.class)
                    .orderBy("fecha DESC")
                    .limit(50)
                    .execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void removerPosiciones(List<PosicionNueva> posionesRemover){
        try {
            for (PosicionNueva p:posionesRemover) {
                p.delete();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public double getTipoRed() {
        return tipoRed;
    }

    public void setTipoRed(double tipoRed) {
        this.tipoRed = tipoRed;
    }

    @Override
    public String toString() {
        return "PosicionNueva{" +
                "idVehiculo=" + idVehiculo +
                ", fecha='" + fecha + '\'' +
                ", idEquipo=" + idEquipo +
                ", latitud=" + latitud +
                ", longitud=" + longitud +
                ", altitud=" + altitud +
                ", velocidad=" + velocidad +
                ", acury=" + acury +
                ", direccion=" + direccion +
                ", bateria=" + bateria +
                ", conexion=" + conexion +
                ", gps=" + gps +
                ", estado=" + estado +
                ", temperatura=" + temperatura +
                ", consumo=" + consumo +
                ", tipoRed=" + tipoRed +
                '}';
    }


}

