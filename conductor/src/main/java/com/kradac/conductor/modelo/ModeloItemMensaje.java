package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by John on 10/08/2016.
 */
public class ModeloItemMensaje implements Serializable {

    private String mensaje;


    public ModeloItemMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
