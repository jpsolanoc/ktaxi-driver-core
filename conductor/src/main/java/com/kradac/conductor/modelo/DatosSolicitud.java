package com.kradac.conductor.modelo;

import com.google.gson.Gson;

public class DatosSolicitud {


    private String fecha;
    private double latitud;
    private double longitud;
    private int d;
    private int estado;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }


    public static DatosSolicitud datosSolicitud(String posibleSolcitud) {
        Gson obGson = new Gson();
        return obGson.fromJson(posibleSolcitud, DatosSolicitud.class);
    }

    @Override
    public String toString() {
        return "DatosSolicitud{" +
                "fecha='" + fecha + '\'' +
                ", latitud=" + latitud +
                ", longitud=" + longitud +
                ", d=" + d +
                ", estado=" + estado +
                '}';
    }
}
