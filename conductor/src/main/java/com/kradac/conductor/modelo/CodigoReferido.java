package com.kradac.conductor.modelo;

/**
 * Created by DellKradac on 19/02/2018.
 */

public class CodigoReferido {

    private String codigo;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "Codigo{" +
                "codigo='" + codigo + '\'' +
                '}';
    }
}
