package com.kradac.conductor.modelo;

/**
 * Created by Ing.John Patricio Solano Cabrera on 21/9/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class Pago {

    private int idPago;
    private String titulo;
    private String anio;
    private String mes;
    private int carrerasCorrectas;
    private int carrerasAtendidas;
    private int carrerasErroneas;

    private double deuda;
    private int iva;
    private double total;

    private boolean isRegistrarPago;

    private String estadoDeuda;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getIdPago() {
        return idPago;
    }

    public void setIdPago(int idPago) {
        this.idPago = idPago;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public int getCarrerasCorrectas() {
        return carrerasCorrectas;
    }

    public void setCarrerasCorrectas(int carrerasCorrectas) {
        this.carrerasCorrectas = carrerasCorrectas;
    }

    public int getCarrerasAtendidas() {
        return carrerasAtendidas;
    }

    public void setCarrerasAtendidas(int carrerasAtendidas) {
        this.carrerasAtendidas = carrerasAtendidas;
    }

    public int getCarrerasErroneas() {
        return carrerasErroneas;
    }

    public void setCarrerasErroneas(int carrerasErroneas) {
        this.carrerasErroneas = carrerasErroneas;
    }

    public double getDeuda() {
        return deuda;
    }

    public void setDeuda(double deuda) {
        this.deuda = deuda;
    }

    public int getIva() {
        return iva;
    }

    public void setIva(int iva) {
        this.iva = iva;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public boolean isRegistrarPago() {
        return isRegistrarPago;
    }

    public void setRegistrarPago(boolean registrarPago) {
        isRegistrarPago = registrarPago;
    }

    public String getEstadoDeuda() {
        return estadoDeuda;
    }

    public void setEstadoDeuda(String estadoDeuda) {
        this.estadoDeuda = estadoDeuda;
    }

    public Pago(int idPago, String titulo, String anio, String mes, int carrerasCorrectas, int carrerasAtendidas, int carrerasErroneas, double deuda, int iva, double total, boolean isRegistrarPago, String estadoDeuda) {
        this.idPago = idPago;
        this.titulo = titulo;
        this.anio = anio;
        this.mes = mes;
        this.carrerasCorrectas = carrerasCorrectas;
        this.carrerasAtendidas = carrerasAtendidas;
        this.carrerasErroneas = carrerasErroneas;
        this.deuda = deuda;
        this.iva = iva;
        this.total = total;
        this.isRegistrarPago = isRegistrarPago;
        this.estadoDeuda = estadoDeuda;
    }

    @Override
    public String toString() {
        return "Pago{" +
                "idPago=" + idPago +
                ", titulo='" + titulo + '\'' +
                ", anio='" + anio + '\'' +
                ", mes='" + mes + '\'' +
                ", carrerasCorrectas=" + carrerasCorrectas +
                ", carrerasAtendidas=" + carrerasAtendidas +
                ", carrerasErroneas=" + carrerasErroneas +
                ", deuda=" + deuda +
                ", iva=" + iva +
                ", total=" + total +
                ", isRegistrarPago=" + isRegistrarPago +
                ", estadoDeuda='" + estadoDeuda + '\'' +
                '}';
    }

}
