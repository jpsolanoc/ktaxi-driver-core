package com.kradac.conductor.modelo;


import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fabricio on 28/03/16.
 */
@Table(name = "ItemHistorialSolicitud", id = "_id")
public class ItemHistorialSolicitud extends Model implements Parcelable {

    @Column(name = "anio")
    private int anio;
    @Column(name = "mes")
    private int mes;
    @Column(name = "idCliente")
    private int idCliente;
    @Column(name = "t")
    private int t;//tipo
    @Column(name = "p")
    private String p;//pin
    @Column(name = "id", unique = true)
    private int id;//id solicitud{
    @Column(name = "e")
    private int e;//estado
    @Column(name = "cP")
    private String cP;//calle principal
    @Column(name = "cS")
    private String cS;//calle secundaria
    @Column(name = "b")
    private String b;//barrio
    @Column(name = "r")
    private String r;//refrencia
    @Column(name = "d")
    private int d;//dia
    @Column(name = "h")
    private String h;//hora es un time ---------------
    @Column(name = "lt")
    private double lt;
    @Column(name = "lg")
    private double lg;
    @Column(name = "v")
    private int v;//unidad de vehciulo
    @Column(name = "pl")
    private String pl;//placa
    @Column(name = "rM")
    private String rM; //regis
    @Column(name = "nbE")
    private String nbE;//nombre empresa
    @Column(name = "ltC")
    private double ltC;// punto final puede llegar 0
    @Column(name = "lgC")
    private double lgC;// punto final puede llegar 0
    @Column(name = "cF")
    private double cF;//cobro foma pago
    @Column(name = "vImg")
    private String vImg;// imagen
    @Column(name = "aT")
    private int aT;// 0 callcenter y 1 driver.
    @Column(name = "idU")
    private int idU;//id usuario atendio
    @Column(name = "idV")
    private int idV;//id vehiculo
    @Column(name = "ob")
    private String ob;//observacio
    @Column(name = "vl")
    private int vl; //valoracion

    //estos datos se setean en detalle
    @Column(name = "rol")
    private String rol; //rol del que atendio
    @Column(name = "nombreConductor")
    private String nombreConductor; //rol del que atendio
    @Column(name = "imgConductor")
    private String imgConductor; //rol del que atendio
    //String cImg;  para imagen consumir otro rest. pendiente


    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getNombreConductor() {
        return nombreConductor;
    }

    public void setNombreConductor(String nombreConductor) {
        this.nombreConductor = nombreConductor;
    }

    public String getImgConductor() {
        return imgConductor;
    }

    public void setImgConductor(String imgConductor) {
        this.imgConductor = imgConductor;
    }


    public int getIdUsuarioAtendio() {
        return idU;
    }

    public void setIdU(int idU) {
        this.idU = idU;
    }

    public int getIdVehiculo() {
        return idV;
    }

    public void setIdV(int idV) {
        this.idV = idV;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getTipo() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    public String getPin() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public int getIdSolicitud() {
        return id;
    }

    public void setIdSolicitud(int id) {
        this.id = id;
    }

    public int getEstado() {
        return e;
    }

    public void setE(int e) {
        this.e = e;
    }

    public String getCallePrincipal() {
        return cP;
    }

    public void setcP(String cP) {
        this.cP = cP;
    }

    public String getCalleSecundaria() {
        return cS;
    }

    public void setcS(String cS) {
        this.cS = cS;
    }

    public String getBarrioCliente() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getReferenciaCliente() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public int getDia() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public String getHora() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }

    public double getLatitud() {
        return lt;
    }

    public void setLt(double lt) {
        this.lt = lt;
    }

    public double getLongitud() {
        return lg;
    }

    public void setLg(double lg) {
        this.lg = lg;
    }

    public int getVehiculo() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public String getPlaca() {
        return pl;
    }

    public void setPl(String pl) {
        this.pl = pl;
    }

    public String getRegistroMunicipal() {
        return rM;
    }

    public void setrM(String rM) {
        this.rM = rM;
    }

    public String getEmpresa() {
        return nbE;
    }

    public void setNbE(String nbE) {
        this.nbE = nbE;
    }

    public double getLtC() {
        return ltC;
    }

    public void setLtC(double ltC) {
        this.ltC = ltC;
    }

    public double getLgC() {
        return lgC;
    }

    public void setLgC(double lgC) {
        this.lgC = lgC;
    }

    public double getcF() {
        return cF;
    }

    public void setcF(double cF) {
        this.cF = cF;
    }

    public String getvImg() {
        return vImg;
    }

    public void setvImg(String vImg) {
        this.vImg = vImg;
    }

    public int getAt() {
        return aT;
    }

    public void setAt(int at) {
        this.aT = at;
    }

    public int getValoracion() {
        return vl;
    }

    public void setVl(int vl) {
        this.vl = vl;
    }

    public String getObservacion() {
        return ob;
    }

    public void setOb(String ob) {
        this.ob = ob;
    }

    public String getFecha() {
        String mesString = mes < 10 ? "0" + mes : mes + "";
        String diaString = d < 10 ? "0" + d : d + "";
        return anio + "-" + mesString + "-" + diaString;
    }

    public static List<ItemHistorialSolicitud> getAll(int idCliente) {
        List<ItemHistorialSolicitud> list = new ArrayList<>();
        try {
            list = new Select()
                    .from(ItemHistorialSolicitud.class)
                    .where("idCliente= ?", idCliente)
                    .orderBy("id DESC")
                    .execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static ItemHistorialSolicitud get(int id) {
        try {
            return new Select()
                    .from(ItemHistorialSolicitud.class)
                    .where("id= ?", id)
                    .executeSingle();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static List<ItemHistorialSolicitud> getAllMesAnio(int mes, int anio, int idCliente) {
        try {
            return new Select()
                    .from(ItemHistorialSolicitud.class)
                    .where("idCliente= ?", idCliente)
                    .where("anio= ?", anio)
                    .where("mes= ?", mes)
                    .orderBy("id DESC")
                    .execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void deleteAllMesAnio(int mes, int anio, int idCliente) {
        try {
            new Delete().from(ItemHistorialSolicitud.class).where("idCliente= ?", idCliente)
                    .where("anio= ?", anio)
                    .where("mes= ?", mes)
                    .execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static List<ItemHistorialSolicitud> getAllDia(int dia, int mes, int anio, int idCliente) {
        try {
            return new Select()
                    .from(ItemHistorialSolicitud.class)
                    .where("idCliente= ?", idCliente)
                    .where("d= ?", dia)
                    .where("mes= ?", mes)
                    .where("anio= ?", anio)
                    .orderBy("id DESC")
                    .execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int count(int idCliente) {
        try {
            return new Select().from(ItemHistorialSolicitud.class).where("idCliente= ?", idCliente).count();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void updat1e(ItemHistorialSolicitud itemHistorialSolicitud) {
        new Update(ItemHistorialSolicitud.class)
                .set("ob =" + itemHistorialSolicitud.getObservacion())
                .where("id = ?", itemHistorialSolicitud.getId())
                .execute();
    }

    public static void deleteAll() {
        new Delete().from(ItemHistorialSolicitud.class).execute();
    }


    public String toString() {
        return "id=" + id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.anio);
        dest.writeInt(this.mes);
        dest.writeInt(this.idCliente);
        dest.writeInt(this.t);
        dest.writeString(this.p);
        dest.writeInt(this.id);
        dest.writeInt(this.e);
        dest.writeString(this.cP);
        dest.writeString(this.cS);
        dest.writeString(this.b);
        dest.writeString(this.r);
        dest.writeInt(this.d);
        dest.writeString(this.h);
        dest.writeDouble(this.lt);
        dest.writeDouble(this.lg);
        dest.writeInt(this.v);
        dest.writeString(this.pl);
        dest.writeString(this.rM);
        dest.writeString(this.nbE);
        dest.writeDouble(this.ltC);
        dest.writeDouble(this.lgC);
        dest.writeDouble(this.cF);
        dest.writeString(this.vImg);
        dest.writeInt(this.aT);
        dest.writeInt(this.idU);
        dest.writeInt(this.idV);
        dest.writeString(this.ob);
        dest.writeInt(this.vl);
        dest.writeString(this.rol);
        dest.writeString(this.nombreConductor);
        dest.writeString(this.imgConductor);
    }

    public ItemHistorialSolicitud() {
    }

    protected ItemHistorialSolicitud(Parcel in) {
        this.anio = in.readInt();
        this.mes = in.readInt();
        this.idCliente = in.readInt();
        this.t = in.readInt();
        this.p = in.readString();
        this.id = in.readInt();
        this.e = in.readInt();
        this.cP = in.readString();
        this.cS = in.readString();
        this.b = in.readString();
        this.r = in.readString();
        this.d = in.readInt();
        this.h = in.readString();
        this.lt = in.readDouble();
        this.lg = in.readDouble();
        this.v = in.readInt();
        this.pl = in.readString();
        this.rM = in.readString();
        this.nbE = in.readString();
        this.ltC = in.readDouble();
        this.lgC = in.readDouble();
        this.cF = in.readDouble();
        this.vImg = in.readString();
        this.aT = in.readInt();
        this.idU = in.readInt();
        this.idV = in.readInt();
        this.ob = in.readString();
        this.vl = in.readInt();
        this.rol = in.readString();
        this.nombreConductor = in.readString();
        this.imgConductor = in.readString();
    }

    public static final Parcelable.Creator<ItemHistorialSolicitud> CREATOR = new Parcelable.Creator<ItemHistorialSolicitud>() {
        @Override
        public ItemHistorialSolicitud createFromParcel(Parcel source) {
            return new ItemHistorialSolicitud(source);
        }

        @Override
        public ItemHistorialSolicitud[] newArray(int size) {
            return new ItemHistorialSolicitud[size];
        }
    };
}
