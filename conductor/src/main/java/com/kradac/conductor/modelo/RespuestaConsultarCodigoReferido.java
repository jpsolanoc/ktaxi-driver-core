package com.kradac.conductor.modelo;

/**
 * Created by fabricio on 2/5/18.
 */

public class RespuestaConsultarCodigoReferido extends Respuesta {
    private int idCodigo;

    public int getIdCodigo() {
        return idCodigo;
    }

    public void setIdCodigo(int idCodigo) {
        this.idCodigo = idCodigo;
    }

    @Override
    public String toString() {
        return "RespuestaConsultarCodigoReferido{" +
                "idCodigo=" + idCodigo +
                "} " + super.toString();
    }
}
