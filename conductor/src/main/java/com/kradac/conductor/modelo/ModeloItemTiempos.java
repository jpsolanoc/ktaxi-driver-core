package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by John on 10/08/2016.
 */
public class ModeloItemTiempos implements Serializable {

    private String tiempo;

    public ModeloItemTiempos(String tiempo) {
        this.tiempo = tiempo;
    }

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }
}
