package com.kradac.conductor.modelo;

import java.io.Serializable;

public class Insignia implements Serializable {
    private String insignia;
    private String mensajeInsignia;
    private String nombre;
    private int numero;

    public String getInsignia() {
        return insignia;
    }

    public void setInsignia(String insignia) {
        this.insignia = insignia;
    }

    public String getMensajeInsignia() {
        return mensajeInsignia;
    }

    public void setMensajeInsignia(String mensajeInsignia) {
        this.mensajeInsignia = mensajeInsignia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Insignia{" +
                "insignia='" + insignia + '\'' +
                ", mensajeInsignia='" + mensajeInsignia + '\'' +
                ", nombre='" + nombre + '\'' +
                ", numero=" + numero +
                '}';
    }
}