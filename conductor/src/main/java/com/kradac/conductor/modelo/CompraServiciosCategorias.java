package com.kradac.conductor.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ing.John Patricio Solano Cabrera on 5/1/2018.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class CompraServiciosCategorias implements Parcelable {
    private int en;
    private List<LC> lC;
    private String m;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public List<LC> getLC() {
        return lC;
    }

    public void setLC(List<LC> lC) {
        this.lC = lC;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public static class LC implements Parcelable {
        private int idCategoria;
        private String categoria;
        private String detalle;
        private int total;

        public int getIdCategoria() {
            return idCategoria;
        }

        public void setIdCategoria(int idCategoria) {
            this.idCategoria = idCategoria;
        }

        public String getCategoria() {
            return categoria;
        }

        public void setCategoria(String categoria) {
            this.categoria = categoria;
        }

        public String getDetalle() {
            return detalle;
        }

        public void setDetalle(String detalle) {
            this.detalle = detalle;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.idCategoria);
            dest.writeString(this.categoria);
            dest.writeString(this.detalle);
            dest.writeInt(this.total);
        }

        public LC() {
        }

        protected LC(Parcel in) {
            this.idCategoria = in.readInt();
            this.categoria = in.readString();
            this.detalle = in.readString();
            this.total = in.readInt();
        }

        public static final Creator<LC> CREATOR = new Creator<LC>() {
            @Override
            public LC createFromParcel(Parcel source) {
                return new LC(source);
            }

            @Override
            public LC[] newArray(int size) {
                return new LC[size];
            }
        };

        @Override
        public String toString() {
            return "LC{" +
                    "idCategoria=" + idCategoria +
                    ", categoria='" + categoria + '\'' +
                    ", detalle='" + detalle + '\'' +
                    ", total=" + total +
                    '}';
        }
    }




//
//    private static CompraServiciosCategorias [] productosActuales = {
//            new CompraServiciosCategorias(
//                    "Gasolina",
//                    R.mipmap.ic_gasolina),
//            new CompraServiciosCategorias(
//                    "Comida",
//                    R.mipmap.ic_comida),
//            new CompraServiciosCategorias(
//                    "Repuestos",
//                    R.mipmap.ic_repuestos)
//
//    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.en);
        dest.writeList(this.lC);
    }

    public CompraServiciosCategorias() {
    }

    protected CompraServiciosCategorias(Parcel in) {
        this.en = in.readInt();
        this.lC = new ArrayList<LC>();
        in.readList(this.lC, LC.class.getClassLoader());
    }

    public static final Parcelable.Creator<CompraServiciosCategorias> CREATOR = new Parcelable.Creator<CompraServiciosCategorias>() {
        @Override
        public CompraServiciosCategorias createFromParcel(Parcel source) {
            return new CompraServiciosCategorias(source);
        }

        @Override
        public CompraServiciosCategorias[] newArray(int size) {
            return new CompraServiciosCategorias[size];
        }
    };
}
