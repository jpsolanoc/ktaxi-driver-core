package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by John on 15/03/2016.
 */
public class DatosLogin implements Serializable {

    private String usuario;
    private String contrasenia;
    private int idUsuario;
    private int idVehiculo;
    private int idEquipo;
    private String nombres;
    private String apellidos;
    private String correo;
    private String celular;
    private String cedula;
    private String empresa;
    private String placaVehiculo;
    private String regMunVehiculo;
    private String marcaVehiculo;
    private String modeloVehiculo;
    private int idCiudad;
    private String ciudad;
    private int idEmpresa;
    private int anioVehiculo;
    private int unidadVehiculo;
    private String imagenConductor;
    private boolean logeado;
    private int tipoVehiculo;

    public DatosLogin(String usuario, String contrasenia, int idUsuario, int idVehiculo, int idEquipo, String nombres, String apellidos, String correo, String celular, String cedula, String empresa, String placaVehiculo, String regMunVehiculo, String marcaVehiculo, String modeloVehiculo, int idCiudad,String ciudad, int idEmpresa, int anioVehiculo, int unidadVehiculo, String imagenConductor, boolean logeado) {
        this.usuario = usuario;
        this.contrasenia = contrasenia;
        this.idUsuario = idUsuario;
        this.idVehiculo = idVehiculo;
        this.idEquipo = idEquipo;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
        this.celular = celular;
        this.cedula = cedula;
        this.empresa = empresa;
        this.placaVehiculo = placaVehiculo;
        this.regMunVehiculo = regMunVehiculo;
        this.marcaVehiculo = marcaVehiculo;
        this.modeloVehiculo = modeloVehiculo;
        this.idCiudad = idCiudad;
        this.ciudad = ciudad;
        this.idEmpresa = idEmpresa;
        this.anioVehiculo = anioVehiculo;
        this.unidadVehiculo = unidadVehiculo;
        this.imagenConductor = imagenConductor;
        this.logeado = logeado;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public void setIdVehiculo(int idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdVehiculo() {
        return idVehiculo;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getPlacaVehiculo() {
        return placaVehiculo;
    }

    public void setPlacaVehiculo(String placaVehiculo) {
        this.placaVehiculo = placaVehiculo;
    }

    public String getRegMunVehiculo() {
        return regMunVehiculo;
    }

    public void setRegMunVehiculo(String regMunVehiculo) {
        this.regMunVehiculo = regMunVehiculo;
    }

    public String getMarcaVehiculo() {
        return marcaVehiculo;
    }

    public void setMarcaVehiculo(String marcaVehiculo) {
        this.marcaVehiculo = marcaVehiculo;
    }

    public String getModeloVehiculo() {
        return modeloVehiculo;
    }

    public void setModeloVehiculo(String modeloVehiculo) {
        this.modeloVehiculo = modeloVehiculo;
    }

    public int getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(int idCiudad) {
        this.idCiudad = idCiudad;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public int getAnioVehiculo() {
        return anioVehiculo;
    }

    public void setAnioVehiculo(int anioVehiculo) {
        this.anioVehiculo = anioVehiculo;
    }

    public int getUnidadVehiculo() {
        return unidadVehiculo;
    }

    public void setUnidadVehiculo(int unidadVehiculo) {
        this.unidadVehiculo = unidadVehiculo;
    }

    public String getImagenConductor() {
        return imagenConductor;
    }

    public void setImagenConductor(String imagenConductor) {
        this.imagenConductor = imagenConductor;
    }

    public boolean isLogeado() {
        return logeado;
    }

    public void setLogeado(boolean logeado) {
        this.logeado = logeado;
    }


    public int getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(int tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    @Override
    public String toString() {
        return "DatosLogin{" +
                "usuario='" + usuario + '\'' +
                ", contrasenia='" + contrasenia + '\'' +
                ", idUsuario=" + idUsuario +
                ", idVehiculo=" + idVehiculo +
                ", idEquipo=" + idEquipo +
                ", nombres='" + nombres + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", correo='" + correo + '\'' +
                ", celular='" + celular + '\'' +
                ", cedula='" + cedula + '\'' +
                ", empresa='" + empresa + '\'' +
                ", placaVehiculo='" + placaVehiculo + '\'' +
                ", regMunVehiculo='" + regMunVehiculo + '\'' +
                ", marcaVehiculo='" + marcaVehiculo + '\'' +
                ", modeloVehiculo='" + modeloVehiculo + '\'' +
                ", idCiudad=" + idCiudad +
                ", idEmpresa=" + idEmpresa +
                ", anioVehiculo=" + anioVehiculo +
                ", unidadVehiculo=" + unidadVehiculo +
                ", imagenConductor='" + imagenConductor + '\'' +
                ", logeado=" + logeado +
                ", tipoVehiculo=" + tipoVehiculo +
                '}';
    }

    public class CerrarSesion{
        private int estado;
        private String mensaje;

        public int getEstado() {
            return estado;
        }

        public void setEstado(int estado) {
            this.estado = estado;
        }

        public String getMensaje() {
            return mensaje;
        }

        public void setMensaje(String mensaje) {
            this.mensaje = mensaje;
        }

        @Override
        public String toString() {
            return "CerrarSesion{" +
                    "estado=" + estado +
                    ", mensaje='" + mensaje + '\'' +
                    '}';
        }
    }

    public class CambioClave{
        private int estado;
        private String mensaje;

        public int getEstado() {
            return estado;
        }

        public void setEstado(int estado) {
            this.estado = estado;
        }

        public String getMensaje() {
            return mensaje;
        }

        public void setMensaje(String mensaje) {
            this.mensaje = mensaje;
        }

        @Override
        public String toString() {
            return "CambioClave{" +
                    "estado=" + estado +
                    ", mensaje='" + mensaje + '\'' +
                    '}';
        }
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}

