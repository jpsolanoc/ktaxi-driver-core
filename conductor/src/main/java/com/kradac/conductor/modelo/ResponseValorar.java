package com.kradac.conductor.modelo;

/**
 * Created by dev on 06/01/17.
 */

public class ResponseValorar {
    private int en;
    private int error;
    private String m;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    @Override
    public String toString() {
        return "ResponseValorar{" +
                "en=" + en +
                ", error=" + error +
                ", m='" + m + '\'' +
                '}';
    }




}
