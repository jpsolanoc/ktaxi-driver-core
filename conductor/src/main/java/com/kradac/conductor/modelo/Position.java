package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by John on 22/12/2015.
 */
public class Position implements Serializable {

    private int id;
    private int idUsuario;
    private int idEquipo;
    private String fechaHoraMovil;
    private double latitud;
    private double longitud;
    private double altitud;
    private double velocidad;
    private double direccion;
    private double confiabilidadGps;
    private int nivelBateria;
    private String operador;
    private int tipoConexion;
    private String tipoRed;
    private int usandoGps;
    private String fechaHoraConexion;
    private int estadoBoton;
    private int idVehiculo;
    private int temperaturaBateria;

    public Position(int idUsuario, int idEquipo, String fechaHoraMovil, double latitud, double longitud, double altitud, double direccion, double velocidad, double confiabilidadGps, int nivelBateria, String operador, int tipoConexion, String tipoRed, int usandoGps, String fechaHoraConexion, int estadoBoton, int idVehiculo) {
        this.idUsuario = idUsuario;
        this.idEquipo = idEquipo;
        this.fechaHoraMovil = fechaHoraMovil;
        this.latitud = latitud;
        this.longitud = longitud;
        this.altitud = altitud;
        this.direccion = direccion;
        this.velocidad = velocidad;
        this.confiabilidadGps = confiabilidadGps;
        this.nivelBateria = nivelBateria;
        this.operador = operador;
        this.tipoConexion = tipoConexion;
        this.tipoRed = tipoRed;
        this.usandoGps = usandoGps;
        this.fechaHoraConexion = fechaHoraConexion;
        this.estadoBoton = estadoBoton;
        this.idVehiculo = idVehiculo;
    }

    public Position() {
    }


    public int getEstadoBoton() {
        return estadoBoton;
    }

    public void setEstadoBoton(int estadoBoton) {
        this.estadoBoton = estadoBoton;
    }

    public double getDireccion() {
        return direccion;
    }

    public void setDireccion(double direccion) {
        this.direccion = direccion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getFechaHoraMovil() {
        return fechaHoraMovil;
    }

    public void setFechaHoraMovil(String fechaHoraMovil) {
        this.fechaHoraMovil = fechaHoraMovil;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getAltitud() {
        return altitud;
    }

    public void setAltitud(double altitud) {
        this.altitud = altitud;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public double getConfiabilidadGps() {
        return confiabilidadGps;
    }

    public void setConfiabilidadGps(double confiabilidadGps) {
        this.confiabilidadGps = confiabilidadGps;
    }

    public int getNivelBateria() {
        return nivelBateria;
    }

    public void setNivelBateria(int nivelBateria) {
        this.nivelBateria = nivelBateria;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public int getTipoConexion() {
        return tipoConexion;
    }

    public void setTipoConexion(int tipoConexion) {
        this.tipoConexion = tipoConexion;
    }

    public String getTipoRed() {
        return tipoRed;
    }

    public void setTipoRed(String tipoRed) {
        this.tipoRed = tipoRed;
    }

    public int getUsandoGps() {
        return usandoGps;
    }

    public void setUsandoGps(int usandoGps) {
        this.usandoGps = usandoGps;
    }

    public String getFechaHoraConexion() {
        return fechaHoraConexion;
    }

    public void setFechaHoraConexion(String fechaHoraConexion) {
        this.fechaHoraConexion = fechaHoraConexion;
    }

    public int getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(int idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public int getTemperaturaBateria() {
        return temperaturaBateria;
    }

    public void setTemperaturaBateria(int temperaturaBateria) {
        this.temperaturaBateria = temperaturaBateria;
    }

    @Override
    public String toString() {
        return "Position{" +
                "id=" + id +
                ", idUsuario=" + idUsuario +
                ", idEquipo=" + idEquipo +
                ", fechaHoraMovil='" + fechaHoraMovil + '\'' +
                ", latitud=" + latitud +
                ", longitud=" + longitud +
                ", altitud=" + altitud +
                ", velocidad=" + velocidad +
                ", direccion=" + direccion +
                ", confiabilidadGps=" + confiabilidadGps +
                ", nivelBateria=" + nivelBateria +
                ", operador='" + operador + '\'' +
                ", tipoConexion=" + tipoConexion +
                ", tipoRed='" + tipoRed + '\'' +
                ", usandoGps=" + usandoGps +
                ", fechaHoraConexion='" + fechaHoraConexion + '\'' +
                ", estadoBoton=" + estadoBoton +
                ", idVehiculo=" + idVehiculo +
                ", temperaturaBateria=" + temperaturaBateria +
                '}';
    }
}
