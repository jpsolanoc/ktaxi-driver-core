package com.kradac.conductor.modelo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

/**
 * Created by Ing.John Patricio Solano Cabrera on 5/10/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class MensajeDeuda implements Parcelable {

    private L l;

    public L getL() {
        return l;
    }

    public void setL(L l) {
        this.l = l;
    }

    public static class P implements Parcelable {
        private int nP;
        private int nA;
        private int nF;
        private int rP;
        private int rA;
        private int rF;
        private int ts;
        private String ms;

        public int getNP() {
            return nP;
        }

        public void setNP(int nP) {
            this.nP = nP;
        }

        public int getNA() {
            return nA;
        }

        public void setNA(int nA) {
            this.nA = nA;
        }

        public int getNF() {
            return nF;
        }

        public void setNF(int nF) {
            this.nF = nF;
        }

        public int getRP() {
            return rP;
        }

        public void setRP(int rP) {
            this.rP = rP;
        }

        public int getRA() {
            return rA;
        }

        public void setRA(int rA) {
            this.rA = rA;
        }

        public int getRF() {
            return rF;
        }

        public void setRF(int rF) {
            this.rF = rF;
        }

        public int getTs() {
            return ts;
        }

        public void setTs(int ts) {
            this.ts = ts;
        }

        public String getMs() {
            return ms;
        }

        public void setMs(String ms) {
            this.ms = ms;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.nP);
            dest.writeInt(this.nA);
            dest.writeInt(this.nF);
            dest.writeInt(this.rP);
            dest.writeInt(this.rA);
            dest.writeInt(this.rF);
            dest.writeInt(this.ts);
            dest.writeString(this.ms);
        }

        public P() {
        }

        protected P(Parcel in) {
            this.nP = in.readInt();
            this.nA = in.readInt();
            this.nF = in.readInt();
            this.rP = in.readInt();
            this.rA = in.readInt();
            this.rF = in.readInt();
            this.ts = in.readInt();
            this.ms = in.readString();
        }

        public static final Creator<P> CREATOR = new Creator<P>() {
            @Override
            public P createFromParcel(Parcel source) {
                return new P(source);
            }

            @Override
            public P[] newArray(int size) {
                return new P[size];
            }
        };

        @Override
        public String toString() {
            return "P{" +
                    "nP=" + nP +
                    ", nA=" + nA +
                    ", nF=" + nF +
                    ", rP=" + rP +
                    ", rA=" + rA +
                    ", rF=" + rF +
                    ", ts=" + ts +
                    ", ms='" + ms + '\'' +
                    '}';
        }
    }

    public static class L implements Parcelable {
        private int t;
        private P p;

        public int getT() {
            return t;
        }

        public void setT(int t) {
            this.t = t;
        }

        public P getP() {
            return p;
        }

        public void setP(P p) {
            this.p = p;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.t);
            dest.writeParcelable(this.p, flags);
        }

        public L() {
        }

        protected L(Parcel in) {
            this.t = in.readInt();
            this.p = in.readParcelable(P.class.getClassLoader());
        }

        public static final Creator<L> CREATOR = new Creator<L>() {
            @Override
            public L createFromParcel(Parcel source) {
                return new L(source);
            }

            @Override
            public L[] newArray(int size) {
                return new L[size];
            }
        };

        @Override
        public String toString() {
            return "L{" +
                    "t=" + t +
                    ", p=" + p +
                    '}';
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.l, flags);
    }

    public MensajeDeuda() {
    }

    protected MensajeDeuda(Parcel in) {
        this.l = in.readParcelable(L.class.getClassLoader());
    }

    public static final Parcelable.Creator<MensajeDeuda> CREATOR = new Parcelable.Creator<MensajeDeuda>() {
        @Override
        public MensajeDeuda createFromParcel(Parcel source) {
            return new MensajeDeuda(source);
        }

        @Override
        public MensajeDeuda[] newArray(int size) {
            return new MensajeDeuda[size];
        }
    };

    @Override
    public String toString() {
        return "MensajeDeuda{" +
                "l=" + l +
                '}';
    }

    public static MensajeDeuda createMensajeDeuda(String mensajeDeuda) {
        Gson obGson = new Gson();
        return obGson.fromJson(mensajeDeuda, MensajeDeuda.class);
    }
}
