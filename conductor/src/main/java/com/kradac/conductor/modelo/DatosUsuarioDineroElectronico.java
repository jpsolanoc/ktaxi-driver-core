package com.kradac.conductor.modelo;

/**
 * Created by Ing.John Patricio Solano Cabrera on 23/2/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public class DatosUsuarioDineroElectronico {

    private String usuario;
    private String pin;
    private boolean estado;

    public DatosUsuarioDineroElectronico() {
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "DatosUsuarioDineroElectronico{" +
                "usuario='" + usuario + '\'' +
                ", pin='" + pin + '\'' +
                ", estado=" + estado +
                '}';
    }
}
