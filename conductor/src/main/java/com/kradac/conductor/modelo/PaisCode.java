package com.kradac.conductor.modelo;

public class PaisCode {
    public static final String CODIGO_ECUADOR="EC";
    public static final String CODIGO_COLOMBIA="CO";
    public static final String CODIGO_BOLIVIA="BO";
    public static final String CODIGO_PERU="PE";
    public static final String CODIGO_ARGENTINA="AR";
    public static final String CODIGO_MEXICO="MX";

}
