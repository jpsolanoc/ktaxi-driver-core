package com.kradac.conductor.modelo;

import java.util.List;

/**
 * Created by fabricio on 2/5/18.
 */

public class ResponseListarCodigosReferido extends Respuesta {


    private List<Insignia> lI;
    private List<CodigoReferido> lC;
    private String label;
    private String mensaje;
    private String titulo;

    public List<Insignia> getlI() {
        return lI;
    }

    public void setlI(List<Insignia> lI) {
        this.lI = lI;
    }

    public List<CodigoReferido> getlC() {
        return lC;
    }

    public void setlC(List<CodigoReferido> lC) {
        this.lC = lC;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Override
    public String toString() {
        return "ResponseListarCodigosReferido{" +
                "lI=" + lI +
                ", lC=" + lC +
                ", label='" + label + '\'' +
                ", mensaje='" + mensaje + '\'' +
                ", titulo='" + titulo + '\'' +
                '}';
    }
}