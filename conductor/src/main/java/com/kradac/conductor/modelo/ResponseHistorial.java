package com.kradac.conductor.modelo;

/**
 * Created by dev on 04/01/17.
 */

public class ResponseHistorial {
    private int registros;

    public int getRegistros() {
        return registros;
    }

    public void setRegistros(int registros) {
        this.registros = registros;
    }

    @Override
    public String toString() {
        return "ResponseHistorial{" +
                "registros=" + registros +
                '}';
    }
}
