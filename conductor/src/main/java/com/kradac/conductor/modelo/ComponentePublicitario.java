package com.kradac.conductor.modelo;

import java.io.Serializable;
import java.util.List;
/**
 * Created by John on 02/01/2017.
 */

public class ComponentePublicitario implements Serializable {
    private int en;
    private List<Componente>lC;

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public List<Componente> getlC() {
        return lC;
    }

    public void setlC(List<Componente> lC) {
        this.lC = lC;
    }

    @Override
    public String toString() {
        return "ComponentePublicitarioActivity{" +
                "en=" + en +
                ", lC=" + lC +
                '}';
    }
}

