package com.kradac.conductor.modelo;

/**
 * Created by John on 02/12/2016.
 */

public class Historial {


    private int registros;
    private String estado;
    private String callePrincipal;
    private String calleSecundaria;
    private String barrioCliente;
    private String referenciaCliente;
    private String fecha;
    private double latitud;
    private double longitud;
    private String nombres;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCallePrincipal() {
        return callePrincipal;
    }

    public void setCallePrincipal(String callePrincipal) {
        this.callePrincipal = callePrincipal;
    }

    public String getCalleSecundaria() {
        return calleSecundaria;
    }

    public void setCalleSecundaria(String calleSecundaria) {
        this.calleSecundaria = calleSecundaria;
    }

    public String getBarrioCliente() {
        return barrioCliente;
    }

    public void setBarrioCliente(String barrioCliente) {
        this.barrioCliente = barrioCliente;
    }

    public String getReferenciaCliente() {
        return referenciaCliente;
    }

    public void setReferenciaCliente(String referenciaCliente) {
        this.referenciaCliente = referenciaCliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public int getRegistros() {
        return registros;
    }

    public void setRegistros(int registros) {
        this.registros = registros;
    }

    @Override
    public String toString() {
        return "Historial{" +
                "registros=" + registros +
                ", estado='" + estado + '\'' +
                ", callePrincipal='" + callePrincipal + '\'' +
                ", calleSecundaria='" + calleSecundaria + '\'' +
                ", barrioCliente='" + barrioCliente + '\'' +
                ", referenciaCliente='" + referenciaCliente + '\'' +
                ", fecha='" + fecha + '\'' +
                ", latitud=" + latitud +
                ", longitud=" + longitud +
                ", nombres='" + nombres + '\'' +
                '}';
    }

    public class HistorialUltimas{
        private int registros;

        public int getRegistros() {
            return registros;
        }

        public void setRegistros(int registros) {
            this.registros = registros;
        }

        @Override
        public String toString() {
            return "HistorialUltimas{" +
                    "registros=" + registros +
                    '}';
        }
    }
}
