package com.kradac.conductor.modelo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Dell on 16/2/2017.
 */
@Table(name = "ItemHistorialCompras", id = "_id")
public class ItemHistorialCompras extends Model implements Serializable {

    @Column(name = "anio")
    private int anio;
    @Column(name = "mes")
    private int mes;
    @Column(name = "idCliente")
    private int idCliente;
    @Column(name = "ped")
    public String ped;
    @Column(name = "pres")
    public double pres;
    @Column(name = "ob")
    public String ob;
    @Column(name = "vl")
    public String vl;
    @Column(name = "t")
    public int t;
    @Column(name = "id",unique = true)
    public int id;
    @Column(name = "e")
    public int e;
    @Column(name = "cP")
    public String cP;
    @Column(name = "cS")
    public String cS;
    @Column(name = "b")
    public String b;
    @Column(name = "r")
    public String r;
    @Column(name = "d")
    public int d;
    @Column(name = "h")
    public String h;
    @Column(name = "v")
    public int v;
    @Column(name = "pl")
    public String pl;
    @Column(name = "rM")
    public String rM;
    @Column(name = "nbE")
    public String nbE;


    //estos datos se setean en detalle
    @Column(name = "rol")
    private String rol; //rol del que atendio
    @Column(name = "nombreConductor")
    private String nombreConductor; //rol del que atendio
    @Column(name = "imgConductor")
    private String imgConductor; //rol del que atendio
    //String cImg;  para imagen consumir otro rest. pendiente


    public String getPed() {
        return ped;
    }

    public void setPed(String ped) {
        this.ped = ped;
    }

    public double getPres() {
        return pres;
    }

    public void setPres(double pres) {
        this.pres = pres;
    }

    public void setPres(int pres) {
        this.pres = pres;
    }

    public String getOb() {
        return ob;
    }

    public void setOb(String ob) {
        this.ob = ob;
    }

    public String getVl() {
        return vl;
    }

    public void setVl(String vl) {
        this.vl = vl;
    }

    public int getIdCompra() {
        return id;
    }

    public int getT() {
        return t;
    }

    public void setT(int t) {
        this.t = t;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getE() {
        return e;
    }

    public void setE(int e) {
        this.e = e;
    }

    public String getcP() {
        return cP;
    }

    public void setcP(String cP) {
        this.cP = cP;
    }

    public String getcS() {
        return cS;
    }

    public void setcS(String cS) {
        this.cS = cS;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public String getH() {
        return h;
    }

    public void setH(String h) {
        this.h = h;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public String getPl() {
        return pl;
    }

    public void setPl(String pl) {
        this.pl = pl;
    }

    public String getrM() {
        return rM;
    }

    public void setrM(String rM) {
        this.rM = rM;
    }

    public String getNbE() {
        return nbE;
    }

    public void setNbE(String nbE) {
        this.nbE = nbE;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public int getIdPedido() {
        return id;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }


    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getNombreConductor() {
        return nombreConductor;
    }

    public void setNombreConductor(String nombreConductor) {
        this.nombreConductor = nombreConductor;
    }

    public String getImgConductor() {
        return imgConductor;
    }

    public void setImgConductor(String imgConductor) {
        this.imgConductor = imgConductor;
    }

    public String getFecha(){
        String mesString=mes<10?"0"+mes:mes+"";
        String diaString=d<10?"0"+d:d+"";
        return anio+"-"+mesString+"-"+diaString;
    }

    public static ItemHistorialCompras get(int id) {
        try {
            return new Select()
                    .from(ItemHistorialCompras.class)
                    .where("id= ?", id)
                    .executeSingle();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static List<ItemHistorialCompras> getAllMesAnio(int mes, int anio, int idCliente) {
        try {
            return new Select()
                    .from(ItemHistorialCompras.class)
                    .where("idCliente= ?", idCliente)
                    .where("anio= ?", anio)
                    .where("mes= ?", mes)
                    .orderBy("id DESC")
                    .execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static List<ItemHistorialCompras> getAllDia(int dia, int mes, int anio, int idCliente) {
        try {
            return new Select()
                    .from(ItemHistorialCompras.class)
                    .where("idCliente= ?", idCliente)
                    .where("d= ?", dia)
                    .where("mes= ?",mes)
                    .where("anio= ?",anio)
                    .orderBy("id DESC")
                    .execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static int count(int idCliente) {
        try {
            return new Select().from(ItemHistorialCompras.class).where("idCliente= ?", idCliente).count();
        } catch (NullPointerException e) {
            e.printStackTrace();
    }
    return 0;
}

    public static void updat1e(ItemHistorialCompras itemHistorialSolicitud){
        new Update(ItemHistorialCompras.class)
                .set("ob ="+ itemHistorialSolicitud.getOb())
                .where("id = ?", itemHistorialSolicitud.getId())
                .execute();
    }

    public static void deleteAllMesAnio(int mes, int anio, int idCliente) {
        try {
            new Delete().from(ItemHistorialSolicitud.class).where("idCliente= ?", idCliente)
                    .where("anio= ?", anio)
                    .where("mes= ?", mes)
                    .execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public static void deleteAll() {
        new Delete().from(ItemHistorialCompras.class).execute();
    }

    @Override
    public String toString() {
        return "ItemHistorialCompras{" +
                "anio=" + anio +
                ", mes=" + mes +
                ", idCliente=" + idCliente +
                ", ped='" + ped + '\'' +
                ", pres=" + pres +
                ", ob='" + ob + '\'' +
                ", vl=" + vl +
                ", t=" + t +
                ", id=" + id +
                ", e=" + e +
                ", cP='" + cP + '\'' +
                ", cS='" + cS + '\'' +
                ", b='" + b + '\'' +
                ", r='" + r + '\'' +
                ", d=" + d +
                ", h='" + h + '\'' +
                ", v=" + v +
                ", pl='" + pl + '\'' +
                ", rM='" + rM + '\'' +
                ", nbE='" + nbE + '\'' +
                ", rol='" + rol + '\'' +
                ", nombreConductor='" + nombreConductor + '\'' +
                ", imgConductor='" + imgConductor + '\'' +
                '}';
    }
}
