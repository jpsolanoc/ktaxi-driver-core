package com.kradac.conductor.modelo;

import java.io.Serializable;

/**
 * Created by John on 17/11/2016.
 */

public class ValoracionConductor implements Serializable{
    private int estado;
    private int valoracion;

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getValoracion() {
        return valoracion;
    }

    public void setValoracion(int valoracion) {
        this.valoracion = valoracion;
    }

    @Override
    public String toString() {
        return "ValoracionConductor{" +
                "estado=" + estado +
                ", valoracion=" + valoracion +
                '}';
    }
}
