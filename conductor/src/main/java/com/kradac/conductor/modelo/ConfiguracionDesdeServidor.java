package com.kradac.conductor.modelo;

/**
 * Created by John on 18/11/2016.
 */

public class ConfiguracionDesdeServidor {

    private String ktaxi;
    private String imgTaxista;
    private String imgCliente;
    private String imgUploadCliente;
    private String version;
    private String sockets;
    private String ktaxidriverVersion;
    private boolean esOsm;
    private String direccion;
    private String azutaxiVersion;

    public String getKtaxi() {
        return ktaxi;
    }

    public void setKtaxi(String ktaxi) {
        this.ktaxi = ktaxi;
    }


    public String getImgTaxista() {
        return imgTaxista;
    }

    public void setImgTaxista(String imgTaxista) {
        this.imgTaxista = imgTaxista;
    }


    public String getImgCliente() {
        return imgCliente;
    }

    public void setImgCliente(String imgCliente) {
        this.imgCliente = imgCliente;
    }

    public String getImgUploadCliente() {
        return imgUploadCliente;
    }

    public void setImgUploadCliente(String imgUploadCliente) {
        this.imgUploadCliente = imgUploadCliente;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSockets() {
        return sockets;
    }

    public void setSockets(String sockets) {
        this.sockets = sockets;
    }


    public String getKtaxidriverVersion() {
        return ktaxidriverVersion;
    }

    public void setKtaxidriverVersion(String ktaxidriverVersion) {
        this.ktaxidriverVersion = ktaxidriverVersion;
    }

    public boolean isEsOsm() {
        return esOsm;
    }

    public void setEsOsm(boolean esOsm) {
        this.esOsm = esOsm;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getAzutaxiVersion() {
        return azutaxiVersion;
    }

    public void setAzutaxiVersion(String azutaxiVersion) {
        this.azutaxiVersion = azutaxiVersion;
    }

    @Override
    public String toString() {
        return "ConfiguracionDesdeServidor{" +
                "ktaxi='" + ktaxi + '\'' +
                ", imgTaxista='" + imgTaxista + '\'' +
                ", imgCliente='" + imgCliente + '\'' +
                ", imgUploadCliente='" + imgUploadCliente + '\'' +
                ", version='" + version + '\'' +
                ", sockets='" + sockets + '\'' +
                ", ktaxidriverVersion='" + ktaxidriverVersion + '\'' +
                ", esOsm=" + esOsm +
                ", direccion='" + direccion + '\'' +
                ", azutaxiVersion='" + azutaxiVersion + '\'' +
                '}';
    }
}
