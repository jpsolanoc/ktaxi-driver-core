package com.kradac.conductor.modelo;

/**
 * Created by John on 07/11/2016.
 */

public class chatBroadCast {

    public interface OnMensajesCallCenter{
        void cargarMensajes();
    }
    public interface OnMensajesBroadCastEmpresa{
        void cargarMensajes();
    }
    public interface OnMensajesBroadCast{
        void cargarMensajes();
    }

}
