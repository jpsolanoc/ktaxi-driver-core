package com.kradac.conductor.vista;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.fragment.CompraServicios;
import com.kradac.conductor.fragment.SaldoKtaxi;
import com.kradac.conductor.fragment.TransaccionesSaldoKtaxiFragment;

public class SaldoKtaxiPager extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    private SaldoKtaxi fragmentSaldoKtaxi;
    private CompraServicios fragmentCompraServicio;
    private TransaccionesSaldoKtaxiFragment transaccionesSaldoKtaxiFragment;

    public SaldoKtaxiPager(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                fragmentSaldoKtaxi = new SaldoKtaxi();
                fragmentCompraServicio = null;
                transaccionesSaldoKtaxiFragment = null;
                return fragmentSaldoKtaxi;
            case 1:
                fragmentSaldoKtaxi = null;
                transaccionesSaldoKtaxiFragment = null;
                fragmentCompraServicio = new CompraServicios();
                return fragmentCompraServicio;
            case 2:
                fragmentSaldoKtaxi = null;
                transaccionesSaldoKtaxiFragment = new TransaccionesSaldoKtaxiFragment();
                return transaccionesSaldoKtaxiFragment;
            default:
                return null;
        }
    }


    public SaldoKtaxi getFragmentSaldoKtaxi() {
        return fragmentSaldoKtaxi;
    }


    public CompraServicios getFragmentCompraServicio() {
        return fragmentCompraServicio;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
