package com.kradac.conductor.vista;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.CuentaAdapter;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionCuentas;
import com.kradac.conductor.modelo.Cuenta;
import com.kradac.conductor.presentador.CuentasPresentador;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CuentasActivity extends AppCompatActivity implements OnComunicacionCuentas {

    @BindView(R2.id.btn_anterior)
    ImageButton btnAnterior;
    @BindView(R2.id.tv_mes_anio)
    TextView tvMesAnio;
    @BindView(R2.id.btn_siguiente)
    ImageButton btnSiguiente;
    @BindView(R2.id.tv_total_cuenta)
    TextView tvTotalCuenta;
    @BindView(R2.id.appbar)
    Toolbar appbar;

    private CuentasPresentador cuentasPresentador;
    private Utilidades utilidades;
    public String titulo;
    private CuentaAdapter adapter;
    private double totalCuenta;
    private int anio, mes;
    private String moneda;


//    GridViewWithHeaderAndFooter grid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuentas);
        ButterKnife.bind(this);
        setSupportActionBar(appbar);
        moneda = definirMoneda();
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle("Cuentas");
            ab.setDisplayHomeAsUpEnabled(true);
        }
        utilidades = new Utilidades();
        cuentasPresentador = new CuentasPresentador(this);
        anio = utilidades.getAnio();
        mes = utilidades.getMes();
        getDataServer(anio, mes + 1);
        titulo = utilidades.getMesString(utilidades.getMes());
        RecyclerView recycler = (RecyclerView) findViewById(R.id.reciclador);
        recycler.setHasFixedSize(true);
        RecyclerView.LayoutManager lManager = new LinearLayoutManager(this);
        recycler.setLayoutManager(lManager);
        adapter = new CuentaAdapter(new ArrayList<Cuenta.C>(), this, moneda);
        recycler.setAdapter(adapter);
        tvMesAnio.setText(utilidades.getMesString(mes).toUpperCase());
    }

    public void getDataServer(int anio, int mes) {
        SharedPreferences spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        cuentasPresentador.obtenerCuentas(spLogin.getInt("idVehiculo", 0), spLogin.getInt("idUsuario", 0), anio, mes);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cuentas, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: // ID del boton
                finish();
                startActivity(new Intent(this, MapaBaseActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void responseCuentas(Cuenta cuentas) {
        if (cuentas != null && cuentas.getC().size() > 0) {
            totalCuenta = 0;
            adapter.updateItem(cuentas.getC());
            for (Cuenta.C c : cuentas.getC()) {
                totalCuenta = totalCuenta + c.getDinero();
            }
            String data = utilidades.dosDecimales(CuentasActivity.this, totalCuenta) + " " + moneda;
            tvTotalCuenta.setText(data);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("No se puede obtener la información, intente nuevamente más tarde.")
                    .setCancelable(false)
                    .setTitle("Alerta")
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();
                        }
                    }).show();
        }

    }

    @OnClick({R2.id.btn_anterior, R2.id.btn_siguiente})
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btn_anterior) {
            if (mes == 0) {
                mes = 11;
                anio = anio - 1;
            } else {
                mes--;
            }
            if (utilidades.getAnio() == anio) {
                tvMesAnio.setText(utilidades.getMesString(mes).toUpperCase());
            } else {
                tvMesAnio.setText(anio + " " + utilidades.getMesString(mes).toUpperCase());
            }
            getDataServer(anio, mes + 1);

        } else if (i == R.id.btn_siguiente) {
            if (mes == 11) {
                mes = 0;
                anio = anio + 1;
            } else {
                mes++;
            }
            if (utilidades.getAnio() == anio) {
                tvMesAnio.setText(utilidades.getMesString(mes).toUpperCase());
            } else {
                tvMesAnio.setText(anio + " " + utilidades.getMesString(mes).toUpperCase());
            }
            getDataServer(anio, mes + 1);

        }
    }

    private SharedPreferences spParametrosConfiguracion;

    public String definirMoneda() {
        spParametrosConfiguracion = getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.getInt("id") == 12) {
                        if (objetoConfig.getInt("h") == 1) {
                            return objetoConfig.getString("nb");
                        }
                    }
                }
            } catch (JSONException e) {
                Log.e("CUENTAS A", "definirMoneda: ", e);
            }
        }
        return "Usd";
    }


}
