package com.kradac.conductor.vista;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.adaptadoreslista.ChatAdapter;
import com.kradac.conductor.adaptadoreslista.ListAdapterMensajes;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.OnComunicacionSocketMensajeria;
import com.kradac.conductor.modelo.ChatMessage;
import com.kradac.conductor.modelo.ModeloItemMensaje;
import com.kradac.conductor.modelo.Solicitud;

import java.io.File;
import java.util.ArrayList;

public class ChatActivity extends BaseConexionService implements OnComunicacionSocketMensajeria {

    public static final String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";

    public static boolean estadoChat = false;
    String nameAudio;
    private EditText messageET;
    private ListView messagesContainer;
    private ImageButton sendBtn, btnAudio;
    private ChatAdapter adapter;
    private boolean isPlay = false;
    private MediaRecorder mediaRecorder;
    private boolean isAudio = true;
    private static final int MY_PERMISSIONS_REQUEST_AUDIO = 3;
    private AlertDialog dialogMensajes;
    private ActionBar ab;
    private Utilidades utilidades;

    @Override
    public void conexionCompleta() {
        super.conexionCompleta();
        registrarAcitvityServer(this, "ChatActivity");
        servicioSocket.obtenerMensajes();
        evaluarEstadoChat(servicioSocket.solicitudSeleccionada.getEstadoChat());
        if (servicioSocket.solicitudSeleccionada.getIdPedido() != 0) {
            ab.setSubtitle(String.valueOf(servicioSocket.solicitudSeleccionada.getIdPedido()));
        } else {
            ab.setSubtitle(String.valueOf(servicioSocket.solicitudSeleccionada.getIdSolicitud()));
        }
    }


    private void evaluarEstadoChat(int valor) {
        switch (valor) {
            case 0:
                break;
            case 1:
                isAudio = false;
                sendBtn.setVisibility(View.VISIBLE);
                btnAudio.setVisibility(View.GONE);
                break;
            case 2:
                isAudio = true;
                sendBtn.setVisibility(View.VISIBLE);
                btnAudio.setVisibility(View.VISIBLE);
                break;
            default:
                isAudio = false;
                btnAudio.setVisibility(View.GONE);
                break;
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            servicioSocket.isMensajeNuevo = false;
            estadoChat = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registrarAcitvityServer(this, "ChatActivity");
        estadoChat = true;
        utilidades.hideFloatingView(this);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Window win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
        setContentView(R.layout.activity_chat);
        Toolbar toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);

        directorio();
        ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
        btnAudio = findViewById(R.id.btn_audio);
        utilidades = new Utilidades();
        btnAudio.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_UP:
                    try {
                        if (mediaRecorder != null) {
                            if (isPlay) {
                                mediaRecorder.stop();
                            }
                            servicioSocket.enviarAudio(nameAudio);
                            isPlay = false;
                            mediaRecorder = null;
                        }
                    } catch (RuntimeException e) {
                    }
                    break;
            }
            return false;
        });

        btnAudio.setOnLongClickListener(view -> {
            if (ActivityCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ChatActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_AUDIO);
                return true;
            } else {
                nameAudio = servicioSocket.nameAudio();
                recordAudio(path + nameAudio);
            }
            return false;
        });


        initControls();
    }

    public void recordAudio(String fileName) {
        mediaRecorder = new MediaRecorder();
        ContentValues values = new ContentValues(3);
        values.put(MediaStore.MediaColumns.TITLE, fileName);
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        mediaRecorder.setAudioSamplingRate(16000);
        mediaRecorder.setAudioEncodingBitRate(256);
        mediaRecorder.setOutputFile(fileName);
        try {
            mediaRecorder.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            mediaRecorder.start();
            isPlay = true;
        } catch (IllegalStateException ignored) {
        }
    }


    private void initControls() {
        adapter = new ChatAdapter(ChatActivity.this, new ArrayList<>());
        messagesContainer = findViewById(R.id.messagesContainer);
        messageET = findViewById(R.id.messageEdit);
        messagesContainer.setAdapter(adapter);
        sendBtn = findViewById(R.id.chatSendButton);
        sendBtn.setVisibility(View.GONE);
        sendBtn.setOnClickListener(v -> {
            String messageText = messageET.getText().toString();
            if (TextUtils.isEmpty(messageText)) {
                return;
            }
            if (messageET.getText().toString().trim().equals("")) {
                return;
            }
            servicioSocket.enviarChat(messageET.getText().toString().trim());
            messageET.setText("");

        });

        messageET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (isAudio) {
                    if (!messageET.getText().toString().isEmpty()) {
                        sendBtn.setVisibility(View.VISIBLE);
                        btnAudio.setVisibility(View.GONE);
                    } else {
                        sendBtn.setVisibility(View.GONE);
                        btnAudio.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }


    private void scroll() {
        messagesContainer.setSelection(messagesContainer.getCount() - 1);
    }


    @Override
    public void clienteAbordo(final String mensaje, Solicitud solicitudSelect) {
        runOnUiThread(() -> {
            finish();
            Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();

        });
    }

    @Override
    public void clienteFinCarrera(final String mensaje, String calificacion) {
        runOnUiThread(() -> {
            finish();
            Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_LONG).show();
        });
    }


    @Override
    public void mensajesUsuario(final ArrayList<ChatMessage> chat) {
        runOnUiThread(() -> {
            adapter.removeAll();
            adapter.notifyDataSetChanged();
            adapter.add(chat);
            adapter.notifyDataSetChanged();
            scroll();
        });
    }

    @Override
    public void clienteAceptoTiempo(boolean isAceptado, Solicitud solSelected, final String razon) {
        runOnUiThread(this::finish);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.menu_mensajes) {
            dialogoMensajes();
            return true;
        } else if (i == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_AUDIO: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new Utilidades().customToast(this, getString(R.string.msj_ahora_puedes_enviar_audios));
                } else {
                    new Utilidades().customToast(this, getString(R.string.msj_no_puede_enviar_audios));
                }
                break;
            }
        }
    }

    public void dialogoMensajes() {
        if (utilidades.isVerificarDialogo(dialogMensajes)) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View viewInformacion = inflater.inflate(R.layout.item_seleccionar_mensaje, null, false);
            ListView listView = viewInformacion.findViewById(R.id.listView);
            final ArrayList<ModeloItemMensaje> itemTiemposList = new ArrayList<>();
            if (mBound && servicioSocket.solicitudSeleccionada.isPedido()) {
                itemTiemposList.add(new ModeloItemMensaje(getString(R.string.str_pedido_preparacion)));
            }
            itemTiemposList.add(new ModeloItemMensaje(getString(R.string.str_estoy_camino)));
            itemTiemposList.add(new ModeloItemMensaje(getResources().getString(R.string.mensaje_chat_tres)));
            itemTiemposList.add(new ModeloItemMensaje(getString(R.string.str_espere_por_favor)));
            itemTiemposList.add(new ModeloItemMensaje(getString(R.string.str_mas_tiempo)));
            listView.setOnItemClickListener((parent, view, position, id) -> {
                if (mBound) {
                    servicioSocket.enviarChat(itemTiemposList.get(position).getMensaje());
                    dialogMensajes.dismiss();
                }
            });
            ListAdapterMensajes miAdaptador = new ListAdapterMensajes(this, R.layout.item_seleccionar_mensaje, itemTiemposList);
            listView.setAdapter(miAdaptador);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(viewInformacion);
            builder.setCancelable(true);
            dialogMensajes = builder.show();
        }

    }

    @Override
    public void clienteCancelo() {
        finish();
    }


    public String directorio() {
        String intStorageDirectory = getFilesDir().toString();
        File file = new File(intStorageDirectory, "ktaxiDriver/Audio");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }
}
