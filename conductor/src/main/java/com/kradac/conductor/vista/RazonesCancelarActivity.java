package com.kradac.conductor.vista;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RazonesCancelarActivity extends AppCompatActivity {

    @BindView(R2.id.appbar)
    Toolbar appbar;
    @BindView(R2.id.my_recycler_datos)
    RecyclerView myRecyclerDatos;
    @BindView(R2.id.tv_comentario)
    EditText tvComentario;
    @BindView(R2.id.btn_regresar)
    Button btnRegresar;
    @BindView(R2.id.btn_cancelar_solicitud)
    Button btnCancelarSolicitud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_razones_cancelar);
        ButterKnife.bind(this);
        setSupportActionBar(appbar);
        appbar.setTitle("Razón de cancelado");
    }

    @OnClick({R2.id.btn_regresar, R2.id.btn_cancelar_solicitud})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.btn_regresar) {
        } else if (i == R.id.btn_cancelar_solicitud) {
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }
}
