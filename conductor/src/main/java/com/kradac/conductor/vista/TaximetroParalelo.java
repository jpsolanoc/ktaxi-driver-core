package com.kradac.conductor.vista;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaximetroParalelo extends AppCompatActivity {
    @BindView(R2.id.tv_taxi)
    TextView tvTaxi;
    @BindView(R2.id.tv_taxi_valor)
    TextView tvTaxiValor;
    @BindView(R2.id.tv_taxi_valor_dos)
    TextView tvTaxiValorDos;
    @BindView(R2.id.tv_taxi_valor_tres)
    TextView tvTaxiValorTres;
    private StringBuilder recDataString = new StringBuilder();
    private Handler bluetoothIn;
    private static final int REQUEST_ENABLE_BT = 2;
    private BluetoothAdapter btAdapter = null;
    private ConnectedThread mConnectedThread;
    private BluetoothSocket btSocket = null;
    private Utilidades utilidades;
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taximetro_paralelo);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle("Taximetro");
            ab.setDisplayHomeAsUpEnabled(true);
        }
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        checkBTState();
        utilidades = new Utilidades();
        Intent intent2 = getIntent();
        String address = intent2.getStringExtra(DeviceListSensorActivity.EXTRA_DEVICE_ADDRESS);
        if (address != null && btAdapter != null) {
            BluetoothDevice device = btAdapter.getRemoteDevice(address);
            try {
                btSocket = createBluetoothSocket(device);
                if (!btSocket.isConnected()) {
                    btSocket.connect();
                }
            } catch (IOException e) {
                AlertDialog.Builder dialogo = new AlertDialog.Builder(this);
                dialogo.setTitle("ALERTA");
                dialogo.setMessage("Vincule el dispositivo nuevamente.");
                dialogo.setCancelable(false);
                dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                        finish();
                    }
                });
                dialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        dialogo1.dismiss();
                        utilidades.customToastCorto(TaximetroParalelo.this, "No se podrá usar el detector del asiento.");
                    }
                });
                dialogo.show();
                try {
                    btSocket.close();
                } catch (IOException ignored) {
                }
            }
            mConnectedThread = new ConnectedThread(btSocket);
            mConnectedThread.start();
        } else {
            Intent intent = new Intent(this, DeviceListSensorActivity.class);
            intent.putExtra("clase", 1);
            startActivity(intent);
            finish();
        }


        manejadorTrama();
    }


    private void checkBTState() {
        if (btAdapter == null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }


    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
        //creates secure outgoing connecetion with BT device using UUID
    }

    /**
     * #tax;tarifa;distancia;tiempo$
     * //#tax;0.45;0.21;2$
     */
    public void manejadorTrama() {
        bluetoothIn = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    String readMessage = (String) msg.obj;
                    recDataString.append(readMessage);
                    int endOfLineIndex = recDataString.indexOf("$");                    // determine the end-of-line
                    if (endOfLineIndex > 0) {
                        String dataInPrint = recDataString.substring(0, endOfLineIndex);    // extract string
                        final ArrayList<String> data = utilidades.extraerTramaTaximetro(dataInPrint);
                        try {
                            if (data != null && data.size() >= 4) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        tvTaxi.setText(data.get(0));
                                        tvTaxiValor.setText(String.valueOf(utilidades.round(Double.parseDouble(data.get(1)), 2)));
                                        tvTaxiValorDos.setText(data.get(2));
                                        tvTaxiValorTres.setText(data.get(3));

                                    }
                                });
                            }
                        } catch (IndexOutOfBoundsException | NullPointerException | NumberFormatException ignored) {
                        }
                        recDataString.delete(0, recDataString.length());                    //clear all string data
                    }
                }
            }
        };
    }


    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        private boolean isRun = true;

        public boolean isRun() {
            return isRun;
        }

        public void setRun(boolean run) {
            isRun = run;
        }

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException ingnore) {
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }


        public void run() {
            byte[] buffer = new byte[256];
            int bytes;
            while (isRun) {
                try {
                    bytes = mmInStream.read(buffer);            //read bytes from input buffer
                    String readMessage = new String(buffer, 0, bytes);
                    bluetoothIn.obtainMessage(0, bytes, 0, readMessage).sendToTarget();
                    try {
                        Thread.sleep(800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    break;
                }
            }
        }

        //tax;0.5399999141;14
        public void cancel() {
            isRun = false;
        }

        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
            } catch (IOException e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), "La Conexión fallo", Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_taximetro_extra, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.menu_enlazar) {
            Intent intent = new Intent(this, DeviceListSensorActivity.class);
            intent.putExtra("clase", 1);
            startActivity(intent);
            finish();
            return true;
        } else if (i == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        if (mConnectedThread != null) {
            mConnectedThread.setRun(false);
        }
        try {
            if (btSocket != null) {
                btSocket.close();
            }
        } catch (IOException ignored) {
        }
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }
}
