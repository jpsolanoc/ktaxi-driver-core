package com.kradac.conductor.vista;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.modelo.ItemHistorialSolicitud;
import com.kradac.conductor.modelo.ResponseDatosConductor;
import com.kradac.conductor.modelo.ResponseValorar;
import com.kradac.conductor.presentador.DatosConductorPresenter;
import com.nostra13.universalimageloader.cache.memory.impl.FIFOLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetalleSolicitudActivity extends AppCompatActivity {

    private static final String TAG = DetalleSolicitudActivity.class.getName();
    public static String urlImagenVehiculos = "http://ktaxiweb.kradac.com/ktaxi/img/uploads/vehiculos/";
    // public static String urlImagenPerfilTaxista = "http://ktaxiweb.kradac.com/ktaxi/img/uploads/people/";
    @BindView(R2.id.llDatosConductor)
    LinearLayout llDatosConductor;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.btn_contactar)
    Button btnContactar;


    private TextView tvRol;
    private RatingBar rbCalificacion;
    private int[] valoracion = {0, 5, 4, 3, 2, 1};
    private ImageView ivTaxi;
    private ImageButton ivEditarValoracion;
    private TextView tvEditarValoracion;

    private TextView tvEmpresa;
    private TextView tvPlaca;
    private TextView tvRegistro;
    private TextView tvBarrio;
    private TextView tvCalles;
    private TextView tvFecha;


    private ItemHistorialSolicitud itemHistorialSolicitud;
    private View.OnClickListener onClickListenerEditarValoracion = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View view = inflater.inflate(R.layout.dialog_editar_valoracion, null, false);
            final RatingBar rbEditarValoracion = (RatingBar) view.findViewById(R.id.rbEditarValoracion);
            final EditText etEditarValoracion = (EditText) view.findViewById(R.id.etEditarObservacion);
            String observacion = itemHistorialSolicitud.getObservacion();
            if (observacion != null) {
                etEditarValoracion.setText(observacion.trim());
            }
            rbEditarValoracion.setRating(valoracion[itemHistorialSolicitud.getValoracion()]);
            AlertDialog.Builder localBuilder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                localBuilder = new AlertDialog.Builder(DetalleSolicitudActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                localBuilder = new AlertDialog.Builder(DetalleSolicitudActivity.this);
            }
            localBuilder.setView(view);
            localBuilder.setCancelable(true);
            localBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    valorarConductor(itemHistorialSolicitud.getIdSolicitud(), valoracion[(int) rbEditarValoracion.getRating()], etEditarValoracion.getText().toString().trim());
                }
            });

            localBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            localBuilder.show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle("Detalle Solicitud");
        }
        initView();
        initImageLoader();
        itemHistorialSolicitud = getIntent().getExtras().getParcelable("itemHistorialSolicitud");
        itemHistorialSolicitud = ItemHistorialSolicitud.get(itemHistorialSolicitud.getIdSolicitud());
        cargarInformacionHistorial(itemHistorialSolicitud);
        cargarDatosConductor(itemHistorialSolicitud);
    }

    private void initView() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ivTaxi = (ImageView) findViewById(R.id.ivTaxi);

        tvRol = (TextView) findViewById(R.id.tvRol);
        rbCalificacion = (RatingBar) findViewById(R.id.rbCalificacion);
        ivEditarValoracion = (ImageButton) findViewById(R.id.ivEditarValoracion);
        tvEditarValoracion = (TextView) findViewById(R.id.tvEditarValoracion);
        ivEditarValoracion.setOnClickListener(onClickListenerEditarValoracion);
        tvEditarValoracion.setOnClickListener(onClickListenerEditarValoracion);

        tvEmpresa = (TextView) findViewById(R.id.tvEmpresa);
        tvPlaca = (TextView) findViewById(R.id.tvPlaca);
        tvRegistro = (TextView) findViewById(R.id.tvRegistro);
        tvBarrio = (TextView) findViewById(R.id.tvBarrio);
        tvCalles = (TextView) findViewById(R.id.tvCalles);
        tvFecha = (TextView) findViewById(R.id.tvFecha);

    }

    private void cargarInformacionHistorial(ItemHistorialSolicitud itemHistorialSolicitud) {
        tvEmpresa.setText(itemHistorialSolicitud.getEmpresa());
        tvPlaca.setText(itemHistorialSolicitud.getPlaca());
        tvRegistro.setText(itemHistorialSolicitud.getRegistroMunicipal());
        tvBarrio.setText(itemHistorialSolicitud.getBarrioCliente());
        tvCalles.setText(itemHistorialSolicitud.getCallePrincipal() + " y " + itemHistorialSolicitud.getCalleSecundaria());
        tvFecha.setText(itemHistorialSolicitud.getFecha());
        rbCalificacion.setRating(valoracion[itemHistorialSolicitud.getValoracion()]);
        cargarFoto(urlImagenVehiculos + itemHistorialSolicitud.getvImg(), ivTaxi);

        if (itemHistorialSolicitud.getNombreConductor() != null) {
            if (!itemHistorialSolicitud.getNombreConductor().trim().equals("")) {
                llDatosConductor.setVisibility(View.VISIBLE);
                tvRol.setText(itemHistorialSolicitud.getRol() + ":");
            }
        }
    }

    private void cargarDatosConductor(final ItemHistorialSolicitud itemHistorialSolicitud) {
        DatosConductorPresenter datosConductorPresenter = new DatosConductorPresenter();

        datosConductorPresenter.obtenerDatosTaxi(itemHistorialSolicitud.getIdUsuarioAtendio(), itemHistorialSolicitud.getIdVehiculo(), itemHistorialSolicitud.getAt(), 1, new DatosConductorPresenter.DatosTaxiListener() {
            @Override
            public void onResponse(ResponseDatosConductor responseTaxi) {
                if (responseTaxi.getdT() != null) {

                    ResponseDatosConductor.DatosTaxiAtendio datosConductor = responseTaxi.getdT();
                    if (datosConductor != null) {
                        itemHistorialSolicitud.setRol(datosConductor.getT());
                        itemHistorialSolicitud.setNombreConductor(datosConductor.getnB());
                        itemHistorialSolicitud.setImgConductor(datosConductor.getImg());
                        itemHistorialSolicitud.save();
                        tvRol.setText(datosConductor.getT() + ":");
                        llDatosConductor.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void cargarFoto(String url, final ImageView imageView) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
        if (url != null && !url.equalsIgnoreCase("null") && !url.equalsIgnoreCase("")) {
            imageLoader.displayImage(url, imageView, options, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    super.onLoadingComplete(imageUri, view, loadedImage);
                    imageView.setImageBitmap(loadedImage);
                }
            });
        }
    }

    private void initImageLoader() {
        int memoryCacheSize;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            int memClass = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE))
                    .getMemoryClass();
            memoryCacheSize = (memClass / 8) * 1024 * 1024;
        } else {
            memoryCacheSize = 2 * 1024 * 1024;
        }

        final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .memoryCacheSize(memoryCacheSize)
                .memoryCache(
                        new FIFOLimitedMemoryCache(memoryCacheSize - 1000000))
                .denyCacheImageMultipleSizesInMemory()
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();

        ImageLoader.getInstance().init(config);
    }

    private void valorarConductor(int idSolicitud, final int valoracionNueva, final String observacion) {
        DatosConductorPresenter datosConductorPresenter = new DatosConductorPresenter();
        datosConductorPresenter.valorarConductor(idSolicitud, valoracionNueva, observacion, new DatosConductorPresenter.ValorarConductorListener() {
            @Override
            public void onResponse(ResponseValorar responseValorar) {
                itemHistorialSolicitud.setVl(valoracionNueva);
                itemHistorialSolicitud.setOb(observacion);
                itemHistorialSolicitud.save();
                rbCalificacion.setRating(valoracion[valoracionNueva]);
                Toast.makeText(getApplicationContext(), responseValorar.getM(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(String error) {
                // Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onException() {
                Toast.makeText(getApplicationContext(), "Por favor intente más tarde", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R2.id.btn_contactar)
    public void onViewClicked() {
        Log.e(TAG, "onViewClicked: ENTRO BTN CONTACTAR"  );
        Intent intent = new Intent(this,SugerenciasActivity.class);
        intent.putExtra("isDesdeSolicitud",true);
        intent.putExtra("solicitudSeleccionada",itemHistorialSolicitud);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }
}
