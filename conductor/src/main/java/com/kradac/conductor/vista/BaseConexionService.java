package com.kradac.conductor.vista;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.service.ServicioSockets;

public abstract class BaseConexionService extends BaseActivity {

    public ServicioSockets servicioSocket;
    public boolean mBound;
    public Utilidades utilidades;
    public SharedPreferences spParametrosConfiguracion, spLogin, spConfiguracionAvanzada,
            spConfiguracionApp, spEstadobtn,spSolicitud,solicitudSeleccionada;


    public ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ServicioSockets.LocalBinder binder = (ServicioSockets.LocalBinder) service;
            servicioSocket = binder.getService();
            mBound = true;
            cambiarEstado(servicioSocket.isServerOnline());
            conexionCompleta();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };


    public void cambiarEstado(boolean isEstado) {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        utilidades = new Utilidades();
        initSharePreferences();
        Intent intent = new Intent(this, ServicioSockets.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    public void initSharePreferences() {
        spParametrosConfiguracion = getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        spConfiguracionApp = getSharedPreferences(VariablesGlobales.CONFIGURACION_APP, Context.MODE_PRIVATE);
        spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        spConfiguracionAvanzada = getSharedPreferences(VariablesGlobales.CONFIG_IP, Context.MODE_PRIVATE);
        spEstadobtn = getSharedPreferences(VariablesGlobales.CONFIG_ESTADO_BTN, Context.MODE_PRIVATE);
        spSolicitud = getSharedPreferences("estado_solicitud_mapa", Context.MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, ServicioSockets.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, ServicioSockets.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mConnection != null) {
            unbindService(mConnection);
        }
    }

    public void registrarAcitvityServer(Activity activity, String nombre) {
        if (mBound) {
            servicioSocket.registerCliente(activity, nombre);
        }
    }

    public void conexionCompleta() {

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
