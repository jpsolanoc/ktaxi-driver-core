package com.kradac.conductor.vista;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.ThemeUIManager;
import com.facebook.accountkit.ui.UIManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.hbb20.CountryCodePicker;
import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.custom.SearchableSpinnerModificado;
import com.kradac.conductor.extras.CustomSpinner;
import com.kradac.conductor.extras.InstallReferrerReceiver;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionContactenos;
import com.kradac.conductor.interfaces.OnComunicacionPais;
import com.kradac.conductor.modelo.Ciudad;
import com.kradac.conductor.modelo.Pais;
import com.kradac.conductor.modelo.RespuestaConsultarCodigoReferido;
import com.kradac.conductor.modelo.RespuestaContactenos;
import com.kradac.conductor.modelo.Resultado;
import com.kradac.conductor.presentador.CanjearCodigoReferidoRequest;
import com.kradac.conductor.presentador.ContactenosPresentador;
import com.kradac.conductor.presentador.PresenterPais;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PreRegistroBaseActivity extends BaseActivity implements OnComunicacionPais,
        AdapterView.OnItemSelectedListener, OnComunicacionContactenos {

    private static final String TAG = PreRegistroBaseActivity.class.getName();
    private static final int REQUEST_CODE = 10;
    @BindView(R2.id.et_nombre)
    EditText etNombre;
    @BindView(R2.id.et_apellidos)
    EditText etApellidos;
    @BindView(R2.id.enviar_pre_registro)
    Button enviarPreRegistro;
    @BindView(R2.id.etMotivo)
    EditText etMotivo;
    @BindView(R2.id.et_celular)
    EditText etCelular;
    @BindView(R2.id.et_correo)
    EditText etCorreo;
    @BindView(R2.id.et_pais)
    EditText etPais;
    @BindView(R2.id.ly_pais)
    LinearLayout lyPais;
    @BindView(R2.id.et_ciudad)
    EditText etCiudad;
    @BindView(R2.id.ly_ciudad)
    LinearLayout lyCiudad;
    @BindView(R2.id.ly_spiner_ciudad)
    LinearLayout lySpinerCiudad;
    @BindView(R2.id.ccp)
    CountryCodePicker ccp;
    @BindView(R2.id.et_referido)
    EditText etReferido;
    @BindView(R2.id.ly_referido)
    LinearLayout lyReferido;
    @BindView(R2.id.spPais)
    SearchableSpinnerModificado spPais;
    @BindView(R2.id.spCiudad)
    SearchableSpinnerModificado spCiudad;
    @BindView(R2.id.spCorreo)
    CustomSpinner spCorreo;
    @BindView(R2.id.btnHabCorreo)
    Button btnHabCorreo;
    @BindView(R2.id.appbar)
    Toolbar appbar;
    @BindView(R2.id.ch_pregunta)
    CheckBox chPregunta;

    private String nombres;
    private Location locationEs;

    @OnClick(R2.id.enviar_pre_registro)
    public void onClick() {
        if (etNombre.getText().toString().trim().isEmpty()) {
            etNombre.requestFocus();
            mensajeError("Ingrese el nombre.");
            return;
        }
        if (etNombre.getText().toString().trim().length() < 3) {
            etNombre.requestFocus();
            mensajeError("El campo nombre debe tener un mínimo de 3 letras.");
            return;
        }
        if (etApellidos.getText().toString().trim().isEmpty()) {
            etApellidos.requestFocus();
            mensajeError("Ingrese el apellido.");
            return;
        }
        if (etApellidos.getText().toString().trim().length() < 3) {
            etApellidos.requestFocus();
            mensajeError("El campo apellido debe tener un mínimo de 3 letras.");
            return;
        }
        nombres = etNombre.getText().toString() + " " + etApellidos.getText().toString();
        if (etCelular.getText().toString().trim().isEmpty()) {
            etCelular.requestFocus();
            mensajeError("Ingrese el celular.");
            return;
        }
        if (etCorreo.getText().toString().trim().isEmpty()) {
            etCorreo.requestFocus();
            mensajeError("Ingrese el correo.");
            return;
        }

        if (isCiudad && etCiudad.getText().toString().trim().length() <= 4) {
            etCiudad.requestFocus();
            mensajeError("Ingrese una ciudad válida.");
            return;
        }

        if (etMotivo.getText().toString().trim().isEmpty()) {
            etMotivo.requestFocus();
            mensajeError("Ingrese el motivo.");
            return;
        }
        if (etMotivo.getText().toString().trim().length() < 10) {
            etMotivo.requestFocus();
            mensajeError("Ingrese un motivo mínimo de quince caracteres.");
            return;
        }

        if (isPais && etPais.getText().toString().trim().length() <= 4) {
            etPais.requestFocus();
            mensajeError("Ingrese un pais válido.");
            return;
        }

        if (!chPregunta.isChecked()) {
            mensajeError("Por favor es necesario contestar a la pregunta realizada.");
            return;
        }


        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            locationEs = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationEs == null) {
                locationEs = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
        }
        if (etReferido.getText().toString().isEmpty()) {
            if (locationEs != null) {
                contactenosPresentador.enviarContactenosV2Pre(10,
                        nombres,
                        isOtro ? etCorreo.getText().toString() : etCorreo.getText().toString().concat(spCorreo.getSelectedItem().toString()),
                        etCelular.getText().toString(),
                        "Pre-registro",
                        motivoEnvio(),
                        locationEs.getLatitude(),
                        locationEs.getLongitude(),
                        ccp.getSelectedCountryNameCode(),
                        String.valueOf("+" + ccp.getSelectedCountryCodeAsInt()),
                        isCiudad ? -1 : ciudades.get(spCiudad.getSelectedItemPosition()).getIdCiudad(),
                        isPais ? etPais.getText().toString() : "",
                        isCiudad ? etCiudad.getText().toString() : String.valueOf(spCiudad.getSelectedItem()) != null ? String.valueOf(spCiudad.getSelectedItem()) : "",
                        idReferido);
                mostrarEspera("Enviando información, espere por favor.");


            } else {
                contactenosPresentador.enviarContactenosV2Pre(10, nombres, isOtro ? etCorreo.getText().toString() : etCorreo.getText().toString().concat(spCorreo.getSelectedItem().toString()),
                        etCelular.getText().toString(), "Pre-registro", motivoEnvio(),
                        0, 0, ccp.getSelectedCountryNameCode(),
                        String.valueOf("+" + ccp.getSelectedCountryCodeAsInt()), isCiudad ? -1 : ciudades.get(spCiudad.getSelectedItemPosition()).getIdCiudad()
                        , isPais ? etPais.getText().toString() : "",
                        isCiudad ? etCiudad.getText().toString() : String.valueOf(spCiudad.getSelectedItem()) != null ? String.valueOf(spCiudad.getSelectedItem()) : "", idReferido);
                mostrarEspera("Enviando información, espere por favor.");

            }
        } else {
            enviarPreRegistro.setEnabled(false);
            verificarCodigoReferido(etReferido.getText().toString());
            mostrarEspera("Enviando información, espere por favor.");
        }


    }

    public void verificarCodigoReferido(String codigoReferido) {
        CanjearCodigoReferidoRequest canjearCodigoReferidoRequest = new CanjearCodigoReferidoRequest();
        canjearCodigoReferidoRequest.consultarCodigo(codigoReferido, new CanjearCodigoReferidoRequest.CodigoReferidoConsultaListerner() {
            @Override
            public void onSuccess(RespuestaConsultarCodigoReferido respuesta) {
                ocultarEspera();
                enviarPreRegistro.setEnabled(true);
                if (respuesta.getEn() == 1) {
                    idReferido = respuesta.getIdCodigo();
                    if (locationEs != null) {
                        contactenosPresentador.enviarContactenosV2Pre(10, nombres, isOtro ? etCorreo.getText().toString() : etCorreo.getText().toString().concat(spCorreo.getSelectedItem().toString()),
                                etCelular.getText().toString(), "Pre-registro", motivoEnvio(),
                                locationEs.getLatitude(), locationEs.getLongitude(), ccp.getSelectedCountryNameCode(),
                                String.valueOf("+" + ccp.getSelectedCountryCodeAsInt()), isCiudad ? -1 : ciudades.get(spCiudad.getSelectedItemPosition()).getIdCiudad(),
                                isPais ? etPais.getText().toString() : "",
                                isCiudad ? etCiudad.getText().toString() : String.valueOf(spCiudad.getSelectedItem()) != null ? String.valueOf(spCiudad.getSelectedItem()) : "", idReferido);
                        mostrarEspera("Enviando información, espere por favor.");

                    } else {
                        contactenosPresentador.enviarContactenosV2Pre(10, nombres, isOtro ? etCorreo.getText().toString() : etCorreo.getText().toString().concat(spCorreo.getSelectedItem().toString()),
                                etCelular.getText().toString(), "Pre-registro", motivoEnvio(),
                                0, 0, ccp.getSelectedCountryNameCode(),
                                String.valueOf("+" + ccp.getSelectedCountryCodeAsInt()), isCiudad ? -1 : ciudades.get(spCiudad.getSelectedItemPosition()).getIdCiudad()
                                , isPais ? etPais.getText().toString() : "",
                                isCiudad ? etCiudad.getText().toString() : String.valueOf(spCiudad.getSelectedItem()) != null ? String.valueOf(spCiudad.getSelectedItem()) : "", idReferido);
                        mostrarEspera("Enviando información, espere por favor.");

                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(PreRegistroBaseActivity.this);
                    builder.setMessage(respuesta.getM())
                            .setTitle("Alerta")
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    etReferido.setText("");
                                    etReferido.requestFocus();
                                }
                            });
                    if (!isFinishing()) {
                        builder.show();
                    }
                }
            }

            @Override
            public void onException() {

            }
        });
    }

    public String motivoEnvio() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(etMotivo.getText().toString().concat(" | "));
        stringBuffer.append(isPais ? "Pais: ".concat(etPais.getText().toString() + " | ") : "");
        stringBuffer.append(isCiudad ? "Ciudad:".concat(etCiudad.getText().toString() + " | ") : "");
        stringBuffer.append(etReferido.getText().toString().isEmpty() ? "" : "Referido:".concat(etReferido.getText().toString()));
        return stringBuffer.toString();
    }

    public int idReferido = 0;

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private interface OnCompleteListener {
        void onComplete();
    }

    public void mensajeError(String mensaje) {
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }


    @SuppressLint("UseSparseArrays")
    private final Map<Integer, OnCompleteListener> permissionsListeners = new HashMap<>();
    private static final int FRAMEWORK_REQUEST_CODE = 1;
    private PresenterPais presenterPais;

    protected int selectedThemeId, auxEmail, auxPhone;
    private int nextPermissionsRequestCode = 4000;
    private String seleccionPais, seleccionCiudad;
    private List<String> listaPais, listaCiudad;
    private List<Pais> listaPaises;
    private ContactenosPresentador contactenosPresentador;
    private List<Ciudad> ciudades;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_registro);
        ButterKnife.bind(this);

        setSupportActionBar(appbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle("Pre Registro");
        }

        ccp.setDefaultCountryUsingNameCode("CO");
        ccp.setCountryForNameCode("CO");
        ccp.setAutoDetectedCountry(true);
        ccp.setCountryPreference("EC,CO,PE,BO,AR");
        ccp.setCountryAutoDetectionPref(CountryCodePicker.AutoDetectionPref.SIM_LOCALE_NETWORK);
        ccp.setNumberAutoFormattingEnabled(false);
        ccp.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
            @Override
            public void onValidityChanged(boolean isValidNumber) {
                Log.e("", "onValidityChanged: " + isValidNumber);
            }
        });

        spPais.setTitle("Seleccione el País");
        spPais.setPositiveButton("Cancelar");
        spCiudad.setTitle("Seleccione la ciudad");
        spCiudad.setPositiveButton("Cancelar");

        contactenosPresentador = new ContactenosPresentador(this, this);

        presenterPais = new PresenterPais(this);
        presenterPais.obtenerPais(1);

        selectedThemeId = R.style.AppLoginTheme_Blue;

        spPais.setOnItemSelectedListener(this);

        spPais.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent,
                                               View v, int position, long id) {
                        if (position > listaPaises.size() - 1) {
                            lyPais.setVisibility(View.VISIBLE);
                            lyCiudad.setVisibility(View.VISIBLE);
                            lySpinerCiudad.setVisibility(View.GONE);
                            isPais = true;
                            isCiudad = true;
                            return;
                        }
                        ciudades = listaPaises.get(position).getlCiudades();
                        isPais = false;
                        isCiudad = false;
                        etPais.setText("");
                        etCiudad.setText("");
                        lyPais.setVisibility(View.GONE);
                        lyCiudad.setVisibility(View.GONE);
                        lySpinerCiudad.setVisibility(View.VISIBLE);
                        listaCiudad = new ArrayList<>();
                        for (Ciudad c : ciudades) {
                            listaCiudad.add(c.getCiudad());
                        }
                        listaCiudad.add("Otra");
                        spCiudad.setAdapter(new ArrayAdapter<>(getApplicationContext(),
                                R.layout.item_spinner, listaCiudad));

                    }

                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

        spCiudad.setOnItemSelectedListener(this);
        spCiudad.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                        seleccionCiudad = String.valueOf(parent.getItemAtPosition(position));
                        if (position > ciudades.size() - 1) {
                            isCiudad = true;
                            lyCiudad.setVisibility(View.VISIBLE);
                            return;
                        }
                        etCiudad.setText("");
                        isCiudad = false;
                        lyCiudad.setVisibility(View.GONE);
                    }

                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

        if (InstallReferrerReceiver.getCodigoReferido(this) != null &&
                !InstallReferrerReceiver.getCodigoReferido(this).isEmpty()) {
            etReferido.setText(InstallReferrerReceiver.getCodigoReferido(this));
            etReferido.setEnabled(false);
        }
        cargarSpiner();

        etCorreo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isContieneArroba(s.toString()) && !isOtro) {
                    String dato = etCorreo.getText().toString();
                    etCorreo.setText(dato.replace("@", ""));
                    spCorreo.performClick();
                    Toast.makeText(PreRegistroBaseActivity.this, "Seleccione una extención para su correo.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private boolean isOtro;

    public void cargarSpiner() {
        String[] opcionesCorreo = {"@gmail.com", "@hotmail.com", "@outlook.es", "@outlook.com", "@yahoo.com", "@yahoo.es", "@live.com", "OTRO"};
        ArrayAdapter adapterCorreo = new ArrayAdapter(this, android.R.layout.simple_list_item_1, opcionesCorreo);
        spCorreo.setAdapter(adapterCorreo);
        spCorreo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String seleccionCorreo = String.valueOf(parent.getItemAtPosition(position));
                if (seleccionCorreo.equals("OTRO")) {
                    spCorreo.setVisibility(View.GONE);
                    btnHabCorreo.setVisibility(View.VISIBLE);
                    isOtro = true;
                } else {
                    isOtro = false;
                    String respuesta = recortaString(etCorreo.getText().toString());
                    etCorreo.setText(respuesta);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public boolean isContieneArroba(String dato) {
        for (int n = 0; n < dato.length(); n++) {
            char c = dato.charAt(n);
            if (c == '@') {
                return true;
            }
        }
        return false;
    }

    public String recortaString(String dato) {
        String resultado = "";
        for (int n = 0; n < dato.length(); n++) {
            char c = dato.charAt(n);
            if (c == '@') {
                break;
            } else {
                resultado = resultado.concat(String.valueOf(c));
            }
        }
        return resultado;
    }

    @OnClick(R2.id.btnHabCorreo)
    public void onViewClicked() {
        spCorreo.setVisibility(View.VISIBLE);
        btnHabCorreo.setVisibility(View.GONE);
        spCorreo.performClick();
        spCorreo.setSelection(0);
    }

    public String getUserEmail() {
        AccountManager manager = AccountManager.get(this);
        Account[] accounts = manager.getAccountsByType("com.google");
        List<String> possibleEmails = new LinkedList<>();
        for (Account account : accounts) {
            possibleEmails.add(account.name);
        }
        if (!possibleEmails.isEmpty() && possibleEmails.get(0) != null) {
            return possibleEmails.get(0);
        }
        return "";
    }


    public boolean isPais, isCiudad;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // ID del boton
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void respuestaPais(Resultado resultado) {
        if (resultado != null) {
            if (resultado.getEn() == 1) {
                listaPaises = resultado.getlPaises();
                listaPais = new ArrayList<>();
                for (Pais p : resultado.getlPaises()) {
                    listaPais.add(p.getPais());
                }
                listaPais.add("Otro");
                spPais.setAdapter(new ArrayAdapter<>(getApplicationContext(),
                        R.layout.item_spinner, listaPais));
            }
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    //Validacion de correo y celular

    public void onLoginEmail() {
        onLogin(LoginType.EMAIL);
    }

    public void onLoginPhone() {
        onLogin(LoginType.PHONE);
    }


    private AccountKitActivity.ResponseType getResponseType() {
        return AccountKitActivity.ResponseType.TOKEN;
    }

    public AccountKitConfiguration.AccountKitConfigurationBuilder createAccountKitConfiguration(
            final LoginType loginType) {
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder
                = new AccountKitConfiguration.AccountKitConfigurationBuilder(
                loginType,
                getResponseType());

        final UIManager uiManager;
        uiManager = new ThemeUIManager(selectedThemeId);
        configurationBuilder.setUIManager(uiManager);

        return configurationBuilder;
    }

    private void onLogin(final LoginType loginType) {
        final Intent intent = new Intent(this, AccountKitActivity.class);

        final AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder
                = createAccountKitConfiguration(loginType);
        final AccountKitConfiguration configuration = configurationBuilder.build();

        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configuration);
        OnCompleteListener completeListener = new OnCompleteListener() {
            @Override
            public void onComplete() {
                startActivityForResult(intent, FRAMEWORK_REQUEST_CODE);
            }

        };

        switch (loginType) {
            case EMAIL:
                if (!isGooglePlayServicesAvailable()) {
                    final OnCompleteListener getAccountsCompleteListener = completeListener;
                    completeListener = new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            requestPermissions(
                                    Manifest.permission.GET_ACCOUNTS,
                                    R.string.permissions_get_accounts_title,
                                    R.string.permissions_get_accounts_message,
                                    getAccountsCompleteListener);
                        }
                    };
                }
                break;
            case PHONE:
                if (configuration.isReceiveSMSEnabled() && !canReadSmsWithoutPermission()) {
                    final OnCompleteListener receiveSMSCompleteListener = completeListener;
                    completeListener = new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            requestPermissions(
                                    Manifest.permission.RECEIVE_SMS,
                                    R.string.permissions_receive_sms_title,
                                    R.string.permissions_receive_sms_message,
                                    receiveSMSCompleteListener);
                        }
                    };
                }
                if (configuration.isReadPhoneStateEnabled() && !isGooglePlayServicesAvailable()) {
                    final OnCompleteListener readPhoneStateCompleteListener = completeListener;
                    completeListener = new OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            requestPermissions(
                                    Manifest.permission.READ_PHONE_STATE,
                                    R.string.permissions_read_phone_state_title,
                                    R.string.permissions_read_phone_state_message,
                                    readPhoneStateCompleteListener);
                        }
                    };
                }
                break;
        }
        completeListener.onComplete();
    }

    private boolean isGooglePlayServicesAvailable() {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int googlePlayServicesAvailable = apiAvailability.isGooglePlayServicesAvailable(this);
        return googlePlayServicesAvailable == ConnectionResult.SUCCESS;
    }

    private boolean canReadSmsWithoutPermission() {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int googlePlayServicesAvailable = apiAvailability.isGooglePlayServicesAvailable(this);
        return googlePlayServicesAvailable == ConnectionResult.SUCCESS;
    }

    private void requestPermissions(
            final String permission,
            final int rationaleTitleResourceId,
            final int rationaleMessageResourceId,
            final OnCompleteListener listener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        checkRequestPermissions(
                permission,
                rationaleTitleResourceId,
                rationaleMessageResourceId,
                listener);
    }

    @TargetApi(23)
    private void checkRequestPermissions(
            final String permission,
            final int rationaleTitleResourceId,
            final int rationaleMessageResourceId,
            final OnCompleteListener listener) {
        if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        final int requestCode = nextPermissionsRequestCode++;
        permissionsListeners.put(requestCode, listener);

        if (shouldShowRequestPermissionRationale(permission)) {
            new AlertDialog.Builder(this)
                    .setTitle(rationaleTitleResourceId)
                    .setMessage(rationaleMessageResourceId)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            requestPermissions(new String[]{permission}, requestCode);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            permissionsListeners.remove(requestCode);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            requestPermissions(new String[]{permission}, requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode,
                                           final @NonNull String permissions[],
                                           final @NonNull int[] grantResults) {
        final OnCompleteListener permissionsListener = permissionsListeners.remove(requestCode);
        if (permissionsListener != null
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permissionsListener.onComplete();
        }
        switch (requestCode) {
            case REQUEST_CODE:
                mensajeInicial();
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "onCreate: " + getUserEmail());
                    etCorreo.setText(getUserEmail());
                } else {
                    Toast.makeText(this, "Permisos denegados.", Toast.LENGTH_SHORT).show();
                }
        }

    }


    @Override
    public void respuestaServidor(RespuestaContactenos generico) {
        ocultarEspera();
        if (generico != null) {
            if (generico.getEstado() == 1) {
                Log.e(TAG, "respuestaServidor: CORRECTO");
                reiniciarCampos();
                Toast.makeText(this, "Registro satisfactorio. Nos pondremos en contacto con usted.", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                mensajeGenericoNoSeEnvio();
            }
        } else {
            mensajeGenericoNoSeEnvio();
        }
    }

    public void mensajeGenericoNoSeEnvio() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("No se pudo realizar la acción, intente nuevamente.")
                .setTitle("Alerta")
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        builder.show();
    }

    public Dialog dialogMensajeInicial;

    public void mensajeInicial() {
        if (dialogMensajeInicial != null) {
            if (dialogMensajeInicial.isShowing())
                return;
        }
        if (getDefinirMostrar()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Estás intentando registrarte en el aplicativo conductor. ¿Eres cliente o conductor?")
                    .setTitle("ATENCIÓN")
                    .setCancelable(false)
                    .setPositiveButton("Cliente", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.kradac.ktaxi")));
                            definirMostrar(false);
                            finish();
                        }
                    })
                    .setNegativeButton("Conductor", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            definirMostrar(false);
                        }
                    });
            builder.show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        guardarDatos();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
        cargarDatos();
        if (etCorreo.getText().toString().isEmpty()) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]
                        {Manifest.permission.GET_ACCOUNTS}, REQUEST_CODE);
                return;
            } else {
                etCorreo.setText(getUserEmail().split("@")[0]);
            }
        }
        mensajeInicial();
    }

    public void definirMostrar(boolean isMostrar) {
        SharedPreferences spPrecioPedido = getSharedPreferences(VariablesGlobales.SP_PRE_REGISTRO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = spPrecioPedido.edit();
        editor.putBoolean("isMostrar", isMostrar);
        editor.apply();
    }

    public boolean getDefinirMostrar() {
        SharedPreferences spPrecioPedido = getSharedPreferences(VariablesGlobales.SP_PRE_REGISTRO, Context.MODE_PRIVATE);
        return spPrecioPedido.getBoolean("isMostrar", true);
    }

    public void guardarDatos() {
        SharedPreferences spDatosPreRegistro = getSharedPreferences(getString(R.string.preferencia_preregistro_datos), MODE_PRIVATE);
        SharedPreferences.Editor editor = spDatosPreRegistro.edit();
        editor.putString("nombre", etNombre.getText().toString());
        editor.putString("apellidos", etApellidos.getText().toString());
        editor.putString("celular", etCelular.getText().toString());
        editor.putString("correo", etCorreo.getText().toString());
        editor.putString("pais", etPais.getText().toString());
        editor.putString("ciudad", etCiudad.getText().toString());
        editor.putString("comentario", etMotivo.getText().toString());
        editor.putString("codigo_referido", etReferido.getText().toString());
        editor.apply();
    }

    public void reiniciarCampos() {
        etNombre.setText("");
        etApellidos.setText("");
        etCelular.setText("");
        etCorreo.setText("");
        etPais.setText("");
        etCiudad.setText("");
        etMotivo.setText("");
        etReferido.setText("");
        guardarDatos();
    }


    public void cargarDatos() {
        SharedPreferences spDatosPreRegistro = getSharedPreferences(getString(R.string.preferencia_preregistro_datos), MODE_PRIVATE);
        etNombre.setText(spDatosPreRegistro.getString("nombre", ""));
        etApellidos.setText(spDatosPreRegistro.getString("apellidos", ""));
        etCelular.setText(spDatosPreRegistro.getString("celular", ""));
        etCorreo.setText(spDatosPreRegistro.getString("correo", ""));
        etPais.setText(spDatosPreRegistro.getString("pais", ""));
        etCiudad.setText(spDatosPreRegistro.getString("ciudad", ""));
        etMotivo.setText(spDatosPreRegistro.getString("comentario", ""));
        etReferido.setText(spDatosPreRegistro.getString("codigo_referido", ""));
    }

}

