package com.kradac.conductor.vista;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.modelo.ComponenteBase;
import com.kradac.conductor.modelo.ResponseApiComponente;
import com.kradac.conductor.presentador.ComponentePresenter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetalleComponenteActivity extends AppCompatActivity {

    @BindView(R2.id.txtTitulo)
    TextView txtTitulo;
    @BindView(R2.id.txtDescripcion)
    TextView txtDescripcion;
    @BindView(R2.id.img)
    ImageView img;
    @BindView(R2.id.btnAceptar)
    Button btnAceptar;
    ComponenteBase componenteBase;
    @BindView(R2.id.marker)
    ImageView marker;
    @BindView(R2.id.txtVerMapa)
    TextView txtVerMapa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_componente);
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.title_detalle_evento);
        }

        Intent intent = getIntent();
        if(intent != null) {
            componenteBase = (ComponenteBase) intent.getSerializableExtra("componenteBase");
            leerComponente(componenteBase, intent.getIntExtra("tipo", 0));
            txtTitulo.setText(componenteBase.getT());
            txtDescripcion.setText(componenteBase.getdL());
            double latitud = componenteBase.getLt();
            double longitud = componenteBase.getLg();
            if(latitud == 0.0 && longitud == 0.0) {
                marker.setVisibility(View.GONE);
                txtVerMapa.setVisibility(View.GONE);
            }

            try {
                DisplayImageOptions options = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.RGB_565).cacheOnDisk(true).build();
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.init(ImageLoaderConfiguration.createDefault(this));
                imageLoader.displayImage(componenteBase.getImg(), img, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        super.onLoadingComplete(imageUri, view, loadedImage);
                        if(loadedImage != null && img != null) {
                            img.setImageBitmap(loadedImage);
                        }
                    }
                });
            } catch (NullPointerException e) {

            }
        }

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view != null) {
                    view.startAnimation(AnimationUtils.loadAnimation(DetalleComponenteActivity.this, R.anim.clic_anim));
                }
                finish();
            }
        });

    }

    private void leerComponente(ComponenteBase componenteBase, int tipo) {
        JSONArray e = new JSONArray();
        e.put(componenteBase.getIdC());

        SharedPreferences spobtener;
        spobtener = getSharedPreferences("login", Context.MODE_PRIVATE);
        ComponentePresenter componentePresenter = new ComponentePresenter();
        componentePresenter.marcarComponenteComoLeido(spobtener.getInt("idUsuario", 0), tipo, 2, e.toString(), new ComponentePresenter.ComponenteListener() {
            @Override
            public void error(String s) {
                Log.e("marcar componente", s);
            }

            @Override
            public void response(ResponseApiComponente responseApi) {

            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch(keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R2.id.txtVerMapa, R2.id.marker})
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.txtVerMapa || i == R.id.marker) {
            if (componenteBase != null) {
                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + componenteBase.getLt() + "," + componenteBase.getLg());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                } else {
                    Toast.makeText(getApplicationContext(), "Aplicación Google Maps no disponible en su dispositivo.", Toast.LENGTH_LONG).show();
                }
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }
}
