package com.kradac.conductor.vista;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v13.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.CustomSpinner;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.OnComunicacionContactenos;
import com.kradac.conductor.modelo.RespuestaContactenos;
import com.kradac.conductor.presentador.ContactenosPresentador;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactenosActivity extends AppCompatActivity implements View.OnClickListener, OnComunicacionContactenos {
    private static final String TAG = ContactenosActivity.class.getName();
    @BindView(R2.id.ccp)
    CountryCodePicker ccp;
    @BindView(R2.id.spCorreo)
    CustomSpinner spCorreo;
    @BindView(R2.id.btnHabCorreo)
    Button btnHabCorreo;
    private EditText etvNombreContacto, etCorreoContacto, etTelefonoContacto, etMotivoMail, etMensajeMail;
    private SharedPreferences spobtener;
    private ProgressDialog pDialog;
    private Utilidades utilidades;
    private ContactenosPresentador contactenosPresentador;

    //Inicio del oncreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactenos);
        ButterKnife.bind(this);
        utilidades = new Utilidades();
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);
        assert toolbar != null;
        toolbar.setTitle("Contáctenos");
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
        contactenosPresentador = new ContactenosPresentador(this, this);
        etvNombreContacto = findViewById(R.id.etNombreContacto);
        etCorreoContacto = findViewById(R.id.etCorreoContacto);
        etTelefonoContacto = findViewById(R.id.etTelefonoContacto);
        etMotivoMail = findViewById(R.id.etMotivoMail);
        etMensajeMail =  findViewById(R.id.etMensajeMail);
        Button btnEnviar = findViewById(R.id.btnEnviarMail);
        assert btnEnviar != null;
        btnEnviar.setOnClickListener(this);
        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(10);
        etTelefonoContacto.setFilters(filterArray);
        spobtener = getApplicationContext().getSharedPreferences("login", Context.MODE_PRIVATE);
        ccp.setDefaultCountryUsingNameCode("CO");
        ccp.setCountryForNameCode("CO");
        ccp.setAutoDetectedCountry(true);
        ccp.changeDefaultLanguage(CountryCodePicker.Language.SPANISH);
        ccp.setCountryPreference("EC,CO,PE,BO,AR");
        ccp.registerCarrierNumberEditText(etTelefonoContacto);
        ccp.setCountryAutoDetectionPref(CountryCodePicker.AutoDetectionPref.SIM_LOCALE_NETWORK);
        ccp.setNumberAutoFormattingEnabled(false);
        ccp.setPhoneNumberValidityChangeListener(new CountryCodePicker.PhoneNumberValidityChangeListener() {
            @Override
            public void onValidityChanged(boolean isValidNumber) {
                Log.e("", "onValidityChanged: " + isValidNumber);
            }
        });
        cargarSpiner();
        etCorreoContacto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (isContieneArroba(s.toString()) && !isOtro) {
                    String dato = etCorreoContacto.getText().toString();
                    etCorreoContacto.setText(dato.replace("@", ""));
                    spCorreo.performClick();
                    Toast.makeText(ContactenosActivity.this, "Seleccione una extención para su correo.", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public boolean isContieneArroba(String dato) {
        for (int n = 0; n < dato.length(); n++) {
            char c = dato.charAt(n);
            if (c == '@') {
                return true;
            }
        }
        return false;
    }

    private boolean isOtro;

    public void cargarSpiner() {
        String[] opcionesCorreo = {"@gmail.com", "@hotmail.com", "@outlook.es", "@outlook.com", "@yahoo.com", "@yahoo.es", "@live.com", "OTRO"};
        ArrayAdapter adapterCorreo = new ArrayAdapter(this, android.R.layout.simple_list_item_1, opcionesCorreo);
        spCorreo.setAdapter(adapterCorreo);
        spCorreo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String seleccionCorreo = String.valueOf(parent.getItemAtPosition(position));
                if (seleccionCorreo.equals("OTRO")) {
                    spCorreo.setVisibility(View.GONE);
                    btnHabCorreo.setVisibility(View.VISIBLE);
                    isOtro = true;
                } else {
                    isOtro = false;
                    String respuesta = recortaString(etCorreoContacto.getText().toString());
                    etCorreoContacto.setText(respuesta);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public String recortaString(String dato) {
        String resultado = "";
        for (int n = 0; n < dato.length(); n++) {
            char c = dato.charAt(n);
            if (c == '@') {
                break;
            } else {
                resultado = resultado.concat(String.valueOf(c));
            }
        }
        return resultado;
    }


    @Override
    public void onClick(View v) {

        int i = v.getId();
        if (i == R.id.btnEnviarMail) {
            if ((etvNombreContacto.getText().toString().isEmpty())) {
                etvNombreContacto.setError("Por favor, ingrese su nombre.");
                etvNombreContacto.requestFocus();
                return;
            }
            if ((etvNombreContacto.getText().toString().trim().length() < 3)) {
                etvNombreContacto.setError("El nombre ingresado no es válido.");
                etvNombreContacto.requestFocus();
                return;
            }
            if (etCorreoContacto.getText().toString().isEmpty()) {
                etCorreoContacto.setError("Por favor, ingrese su correo.");
                etCorreoContacto.requestFocus();
                return;
            }
            if (etCorreoContacto.getText().toString().trim().length() < 3) {
                etCorreoContacto.setError("Por favor, ingrese un correo válido.");
                etCorreoContacto.requestFocus();
                return;
            }
            if (isOtro) {
                if (!validarEmail(etCorreoContacto.getText().toString())) {
                    etCorreoContacto.setError("Por favor, ingrese un correo válido.");
                    etCorreoContacto.requestFocus();
                    return;
                }
            }
            if (!ccp.isValidFullNumber()) {
                etTelefonoContacto.setError("El número de teléfono no es válido");
                etTelefonoContacto.requestFocus();
                return;
            }

            if (etMotivoMail.getText().toString().isEmpty()) {
                etMotivoMail.setError("Ingrese datos en el campo motivo para continuar.");
                etMotivoMail.requestFocus();
                return;
            }
            if (etMotivoMail.getText().toString().length() < 4) {
                etMotivoMail.setError("Escriba un motivo de mínimo 4 caracteres.");
                etMotivoMail.requestFocus();
                return;
            }
            if (isSoloNumeros(etMotivoMail.getText().toString())) {
                etMotivoMail.setError("El motivo debe contener letras.");
                etMotivoMail.requestFocus();
                return;
            }
            String sugerencia = etMensajeMail
                    .getText().toString().trim();
            if (sugerencia.isEmpty()) {
                etMensajeMail.setError("Ingrese datos en el campo mensaje para continuar.");
                etMensajeMail.requestFocus();
                return;
            }
            if (sugerencia.length() < 6) {
                etMensajeMail.setError("Escriba un mensaje de mínimo 6 caracteres.");
                etMensajeMail.requestFocus();
                return;
            }
            if (isSoloNumeros(sugerencia)) {
                etMensajeMail.setError("El mensaje debe contener letras.");
                etMensajeMail.requestFocus();
                return;
            }

            Location location = localizacionActual();
            contactenosPresentador.enviarContactenosV2(1, etvNombreContacto.getText().toString().trim(),
                    (isOtro) ? etCorreoContacto.getText().toString().trim() : etCorreoContacto.getText().toString().trim().concat(spCorreo.getSelectedItem().toString()),
                    etTelefonoContacto.getText().toString().trim(),
                    etMotivoMail.getText().toString().trim(),
                    etMensajeMail.getText().toString().trim(), location.getLatitude(), location.getLongitude(),
                    ccp.getSelectedCountryNameCode(), 0);
            mostrarDialogoEspera();

        }
    }

    public boolean isSoloNumeros(String str) {
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public Location localizacionActual() {
        Location retorno = null;
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        } else {
            retorno = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (retorno != null) {
                return retorno;
            } else {
                return mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
        }
    }

    public void limpiarCampos() {
        etvNombreContacto.setText(" ");
        etCorreoContacto.setText(" ");
        etTelefonoContacto.setText(" ");
        etMotivoMail.setText(" ");
        etMensajeMail.setText(" ");
    }


    public void cargarCampos() {
        if (spobtener.getInt("idUsuario", 0) != 0) {
            etvNombreContacto.setText(spobtener.getString("cartera", "0") + " " + spobtener.getString("apellidos", "0"));
            etCorreoContacto.setText(spobtener.getString("correo", "0"));
            etTelefonoContacto.setText(spobtener.getString("celular", "0"));
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        cargarCampos();
        new Utilidades().hideFloatingView(this);
    }


    public void mostrarDialogoEspera() {
        pDialog = new ProgressDialog(ContactenosActivity.this);
        pDialog.setMessage("Enviando contáctenos.");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                AlertDialog.Builder localBuilder = new AlertDialog.Builder(ContactenosActivity.this);
                localBuilder.setIcon(R.mipmap.ic_launcher);
                localBuilder.setTitle("ALERTA");
                localBuilder.setCancelable(false);
                localBuilder
                        .setMessage("¿Esta seguro de cancelar el envío?");
                localBuilder.setPositiveButton("Si",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int paramAnonymous2Int) {
                                dialog.dismiss();
                            }
                        });
                localBuilder.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface paramAnonymous2DialogInterface,
                                    int paramAnonymous2Int) {
                                paramAnonymous2DialogInterface.dismiss();
                                mostrarDialogoEspera();
                            }
                        });
            }
        });
        pDialog.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void respuestaServidor(RespuestaContactenos generico) {
        if (generico != null) {
            if (!utilidades.isVerificarDialogo(pDialog)) {
                pDialog.dismiss();
            }
            if (generico.getEstado() == 1) {
                utilidades.customToast(ContactenosActivity.this, generico.getMensaje());
                limpiarCampos();
                finish();
            } else {
                utilidades.customToast(this, "No se puede enviar el contactenos intente nuevamente.");
            }
        } else {
            utilidades.customToast(this, "No se puede enviar el contactenos intente nuevamente.");
        }
    }


    @OnClick(R2.id.btnHabCorreo)
    public void onViewClicked() {
        spCorreo.setVisibility(View.VISIBLE);
        btnHabCorreo.setVisibility(View.GONE);
        spCorreo.performClick();
        spCorreo.setSelection(0);
    }


}
