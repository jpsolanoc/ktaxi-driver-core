package com.kradac.conductor.vista;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.provider.CallLog;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.GridAdapterSeleccionarTiempo;
import com.kradac.conductor.boletines.BadgeDrawable;
import com.kradac.conductor.boletines.NotificacionesActivity;
import com.kradac.conductor.dialog.DialogInformacionUsuario;
import com.kradac.conductor.dialog.DialogosGenericos;
import com.kradac.conductor.extras.ExtraLog;
import com.kradac.conductor.extras.LatLng;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionMapa;
import com.kradac.conductor.mapagestion.MapViewConfig;
import com.kradac.conductor.mapagestion.MapViewConfigMapBox;
import com.kradac.conductor.mapagestion.MapViewGoogle;
import com.kradac.conductor.mapagestion.SerialesMapBox;
import com.kradac.conductor.modelo.ChatMessage;
import com.kradac.conductor.modelo.ConfiguracionServidor;
import com.kradac.conductor.modelo.DatosEnvioTaximetro;
import com.kradac.conductor.modelo.DatosLogin;
import com.kradac.conductor.modelo.DatosNotificacion;
import com.kradac.conductor.modelo.DatosTaximetro;
import com.kradac.conductor.modelo.DatosTaximetroServicioActivo;
import com.kradac.conductor.modelo.Destino;
import com.kradac.conductor.modelo.MensajeDeuda;
import com.kradac.conductor.modelo.ModeloItemTiempos;
import com.kradac.conductor.modelo.PositionTrafico;
import com.kradac.conductor.modelo.RespuestaConfiguracion;
import com.kradac.conductor.modelo.RespuestaPosibleSolicitudes;
import com.kradac.conductor.modelo.Solicitud;
import com.kradac.conductor.modelo.ValoracionConductor;
import com.kradac.conductor.notificaciones.Config;
import com.kradac.conductor.presentador.MainPresentador;
import com.kradac.conductor.presentador.MapaPresenter;
import com.kradac.conductor.presentador.MapboxRequest;
import com.kradac.conductor.presentador.PublicidadPresenter;
import com.kradac.conductor.presentador.RegistrarTokenPush;
import com.kradac.conductor.service.ServicioSockets;
import com.kradac.conductor.service.TaximetroService;
import com.kradac.wigets.LayoutPublicitario;
import com.kradac.wigets.extra.VariablesPublicidad;
import com.kradac.wigets.modelo.RespuestaPublicidad;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.Style;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.srain.cube.views.GridViewWithHeaderAndFooter;

public class MapaBaseActivity extends BaseConexionService implements View.OnClickListener,
        SensorEventListener, OnComunicacionMapa {
    private static final int VOICE_RECOGNITION_REQUEST_CODE = 1001;
    private static final int REQUEST_ENABLE_BT = 2;
    public static final int MY_PERMISSIONS_REQUEST_CALL = 3;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 4;
    private static final int MY_PERMISSIONS_REQUEST_AUDIO = 5;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 6;
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final String TAG = MapaBaseActivity.class.getName();
    public static boolean isEstadoActividad = false;
    public String recursoFeria;
    public Dialog pDialogo;
    @BindView(R2.id.imb_zoom_mas)
    ImageButton imbZoomMas;
    @BindView(R2.id.imb_zoom_menos)
    ImageButton imbZoomMenos;
    @BindView(R2.id.btnGpsDisable)
    ImageButton btnGpsDisable;
    @BindView(R2.id.tv_placa)
    TextView tvPlaca;
    @BindView(R2.id.tv_sub_total)
    TextView tvSubTotal;
    @BindView(R2.id.btn_mostrar_taximetro)
    Button btnMostrarTaximetro;
    @BindView(R2.id.tv_hora_inicio)
    TextView tvHoraInicio;
    @BindView(R2.id.tv_hora_fin)
    TextView tvHoraFin;
    @BindView(R2.id.tv_distancia_recorrida)
    TextView tvDistanciaRecorrida;
    @BindView(R2.id.tv_velocidad)
    TextView tvVelocidad;
    @BindView(R2.id.tv_tiempo)
    TextView tvTiempo;
    @BindView(R2.id.tv_cosoto_arranque)
    TextView tvCosotoArranque;
    @BindView(R2.id.tv_valor_distancia)
    TextView tvValorDistancia;
    @BindView(R2.id.tv_tiempo_espera)
    TextView tvTiempoEspera;
    @BindView(R2.id.tv_valor_tiempo)
    TextView tvValorTiempo;
    @BindView(R2.id.ly_taximetro)
    LinearLayout lyTaximetro;
    @BindView(R2.id.img_btn_feria)
    ImageView imgBtnFeria;
    @BindView(R2.id.mapbox)
    com.mapbox.mapboxsdk.maps.MapView mapViewBox;
    boolean banderaSalida, banderaCerrarSecion, isOpenMenuLateral = false,
            banderaClienteAsignado, isBotonConexion = false, isDialogoCancelar = false;
    View dos, btnFlotante, tres, btnReportarPirata, inclTaximetro;
    ConfiguracionServidor configuracionServidor;
    Dialog dialogFinCarrera;
    @BindView(R2.id.btn_dejar_rastrear)
    Button btnDejarRastrear;
    @BindView(R2.id.img_btn_rastreo_panico)
    ImageButton imgBtnRastreoPanico;
    @BindView(R2.id.ly_publicidad)
    LayoutPublicitario lyPublicidad;
    @BindView(R2.id.progress_bar_cliente_abordo)
    ProgressBar progressBarClienteAbordo;
    @BindView(R2.id.map_touch_layer)
    FrameLayout mapTouchLayer;
    @BindView(R2.id.row_tiempo_espera)
    TableRow rowTiempoEspera;
    private ImageButton btnInternetOn, btnInternetOff, btnGpsOn, btnGpsOff, btnNumSolicitudes,
            ibtnInformacionSolicitud, iBtnPosicion, imageButtonMensajes,
            btnWaze, imgBtnComprando, imgBtnComprado;
    private Button btnLlamarCliente, btnAviso, btnClienteAbordo, btnEstadoCarrera, btnOffLine;
    private int contadorZoom = 6, countTest, getTiempo, numeroPasajeros = 0;
    private TextView numeroSolicitudes, tvCronometroTiempo;
    private ArrayList<Solicitud> listaSolicitudes;
    private Solicitud solicitudSeleccionada;
    private Dialog dialogoInformacionSolicitud, dialogoCambioClave,
            dialogoSalirSistema, mostrarDialogoCarrera, dialogoEspera,
            alerDialogoCancelar, dialogoAbordo, dialogoCompras, dialogCobro,
            dialogRespuestaPreguntas;
    private AlertDialog dialogoTiempo;
    private Activity activity = this;
    private AlertDialog alertDialogEventualidades, alDineroElectronico;
    private ProgressDialog pDialog, progressDialog;
    private Location locacionDefault;
    private DrawerLayout drawerLayout;
    private Handler bluetoothIn;
    private Bitmap imagenGlobal;
    private double costo = 0, propina = 0, saldoKtaxi = 0;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder recDataString = new StringBuilder();
    private MapView mapaOSM;
    private MenuItem itemNotificacion;
    private RatingBar ratingBar;
    private MapaPresenter mapaPresenter;
    private ImageButton rlDesactivado;
    private Window win;
    private ConnectedThread mConnectedThread;
    private CircularImageView imageViewTaxista;
    private DatosTaximetro datosTaximetro;
    private MapViewConfig mapViewConfig;
    private MapViewConfigMapBox mapViewConfigMapBox;
    private MapViewGoogle mapViewGoogle;
    private boolean isActivarSaldoKtaxi, isActivarSaldoKtaxiProximanete,
            isActivarCreditoTarjetaCredito, isActivarTarjetaCreditoProximanete,
            isActivarReferido, isActivarRefediroProximante;
    private String nombreReferido;
    private Dialog dialogMensajeTarjetaCredito;
    private MapboxMap mapBox;
    private boolean isRastreo = false;
    private Drawable drawableReferido;
    private Menu menuNav;
    private Dialog dialogCarreraCallCenter;
    private AlertDialog dialogPanico;
    private AlertDialog dialogPosible;
    private int countDescargaImagen = 0;
    private boolean isActualizarNoPrioritaria = false;
    private String tituloPublicidad;
    private DatosNotificacion.LP preguntaSeleccionada;
    private Dialog dialogoSaldoKtaxi;
    private Dialog dialogoMensajeCobro;
    private String totalCarrera;
    private AlertDialog dialogoInf;
    private double valorMaximo = 10;
    private double valorMaximoKtaxi = 0;
    private String totalCarreraCajaTexto = "0";
    private String totalCarreraTemporal;
    private Dialog dialogoVoucherPin;
    private Dialog dialogoDineroElectronico;
    private boolean isVisibleTarjetaCredito = true;
    private boolean isObligatorioCostoCarrera, isObligatorioNumeroPasajeros;
    private boolean isConsulta, isRun;
    private int countConsulta;
    private Dialog dialogOtraApp;
    private boolean isDialogoEspera;

    private int valorEditarCostoC;


    @Override
    public void conexionCompleta() {
        registrarAcitvityServer(this, "MapaBaseActivity");
        servicioSocket.probarIniciarConexion();
        estadoServicio();
        servicioSocket.getEstadoPosibleSolicitud();
        servicioSocket.ejecutarCorazonMalvado();
        if (spEstadobtn.getBoolean(VariablesGlobales.ESTADO_BTN, true)) {
            Log.e("estado47", "estado47");
            //cambiarEstadoLibre();
        } else {
            if (servicioSocket.isTaximetroRun) {
                datosTaximetro = DatosTaximetro.obtenerDatosTaximetro(MapaBaseActivity.this);
                if (datosTaximetro != null && datosTaximetro.getR().getVisible() == 1) {
                    inclTaximetro.setVisibility(View.VISIBLE);
                    lyTaximetro.setVisibility(View.GONE);
                } else {
                    inclTaximetro.setVisibility(View.GONE);
                    lyTaximetro.setVisibility(View.GONE);
                }
            } else {
                inclTaximetro.setVisibility(View.GONE);
            }
            Log.e("estado2", "estado2");
            cambiarEstadoOcupado();
        }
        if (servicioSocket.isMensajeDeudaAbordo()) {
            comprobarMostrarDialogoAbordo();
        }
        if (servicioSocket.isMensajeDeudaFinCarrera()) {
            comprobarMostrarDialogoFinalizar();
        }
        mapaPresenter.activarDesactivarPush(spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), utilidades.getImei(MapaBaseActivity.this), spLogin.getInt("idEmpresa", 0), spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0), 0, true);
    }

    public void manejadorAsiento() {
        bluetoothIn = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    String readMessage = (String) msg.obj;
                    recDataString.append(readMessage);
                    int endOfLineIndex = recDataString.indexOf("$");                    // determine the end-of-line
                    if (endOfLineIndex > 0) {
                        String dataInPrint = recDataString.substring(0, endOfLineIndex);    // extract string
                        ExtraLog.Log(TAG, "handleMessage: " + dataInPrint);
                        if (!dataInPrint.subSequence(1, dataInPrint.length()).equals("0000")) {
                            Log.e("estado3", "estado3");
                            cambiarEstadoOcupado();
                        }
                        recDataString.delete(0, recDataString.length());                    //clear all string data
                    }
                }
            }
        };
    }

    private SerialesMapBox tokenUsar = null;

    public void cambiaValor(Mapbox nuevoValor) {
        try {
            Field field = Mapbox.class.getDeclaredField("INSTANCE");
            field.setAccessible(true);
            field.set(null, nuevoValor);
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }


    public void iniciarMapBox(final Bundle bundle) {
        cambiaValor(null);
        mapViewConfigMapBox = new MapViewConfigMapBox(this);
        tokenUsar = new SerialesMapBox();
        tokenUsar = tokenUsar.getKeyMapaOb(this);
        Mapbox.getInstance(MapaBaseActivity.this, tokenUsar.getKeyMapa());
        mapViewBox.onCreate(bundle);
        new MapboxRequest().verificarTokenV2(tokenUsar.getKeyMapa(), status -> {
            asinTackMapbox();
            if (!status) {
                SerialesMapBox.delete(tokenUsar.getKeyMapa());
                new Handler().postDelayed(MapaBaseActivity.this::recreate, 2000);
            }
        });
        onTouchMapBOx();
    }

    public void asinTackMapbox() {
        mapViewBox.getMapAsync(mapboxMap -> {
            mapBox = mapboxMap;
            mapViewConfigMapBox.setValoresIniciales(spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0), mapBox);
            cambioMapa();
            mapBox.setOnMarkerClickListener(marker -> {
                String position = marker.getSnippet().substring(0, 1);
                switch (position) {
                    case "s":
                        if (listaSolicitudes.size() > 0) {
                            if (!marker.getSnippet().isEmpty()) {
                                int numSolicitud = Integer.parseInt(marker.getSnippet().substring(1, marker.getSnippet().length()));
                                solicitudSeleccionada = listaSolicitudes.get(numSolicitud);
                                seleccionarTiempo();
                            }
                        }
                        break;
                    default:
                        marker.showInfoWindow(mapBox, mapViewBox);
                        break;
                }
                return true;
            });
        });
    }


    public void onTouchMapBOx() {
        mapViewBox.setOnTouchListener((v, event) -> {
            int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN: {
                }
                case MotionEvent.ACTION_UP: {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal, getBaseContext().getTheme()));
                    } else {
                        iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal));
                    }
                    contadorZoom = 1;
                    guardarEstadoRastreo(0);
                    isRastreo = false;
                    modoGpsRastreo(isRastreo, locacionDefault);
                }
            }
            return false;
        });
    }

    public void onTouchGoogle() {
        mapTouchLayer.setOnTouchListener((v, event) -> {
            int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN: {
                }
                case MotionEvent.ACTION_UP: {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal, getBaseContext().getTheme()));
                    } else {
                        iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal));
                    }
                    contadorZoom = 1;
                    guardarEstadoRastreo(0);
                    isRastreo = false;
                    modoGpsRastreo(isRastreo, locacionDefault);
                }
            }
            return false; // Pass on the touch to the map or shadow layer.
        });
    }

    public static String convertJSONString(String data) {
        data = data.replace("\\", "");
        return data;
    }

    public void cambioMapa() {
        if (mapFragment != null && mapaOSM != null && mapViewBox != null) {
            switch (spConfiguracionApp.getInt(VariablesGlobales.TIPO_MAPA, new ConfiguracionActivity().getUltimoUsado(this))) {
                case 1://GOOGLE
                    if (mapFragment != null && mapFragment.getView() != null)
                        mapFragment.getView().setVisibility(View.VISIBLE);
                    if (mapaOSM != null)
                        mapaOSM.setVisibility(View.GONE);
                    if (mapViewBox != null)
                        mapViewBox.setVisibility(View.GONE);
                    if (mMap != null) {
                        switch (spConfiguracionApp.getInt(VariablesGlobales.CONFIGURACION_ESTILO_MAPA, 1)) {
                            case 1:
                                styleMapa(R.raw.style_normal);
                                break;
                            case 2:
                                styleMapa(R.raw.style_dark);
                                break;
                            case 3:
                                if (utilidades.compararHorasFin(null, null)) {
                                    styleMapa(R.raw.style_normal);
                                } else {
                                    styleMapa(R.raw.style_dark);
                                }
                                break;
                        }
                    }
                    break;
                case 2://MAPBOX
                    if (mapFragment != null && mapFragment.getView() != null)
                        mapFragment.getView().setVisibility(View.GONE);
                    if (mapViewBox != null)
                        mapViewBox.setVisibility(View.VISIBLE);
                    if (mapaOSM != null)
                        mapaOSM.setVisibility(View.GONE);
                    if (mapBox != null) {
                        switch (spConfiguracionApp.getInt(VariablesGlobales.CONFIGURACION_ESTILO_MAPA, 1)) {
                            case 1:
                                mapBox.setStyleUrl(Style.MAPBOX_STREETS);
                                break;
                            case 2:
                                mapBox.setStyleUrl(Style.DARK);
                                break;
                            case 3:
                                if (utilidades.compararHorasFin(null, null)) {
                                    mapBox.setStyleUrl(Style.MAPBOX_STREETS);
                                } else {
                                    mapBox.setStyleUrl(Style.DARK);
                                }
                                break;
                        }
                    }
                    break;
                case 3: //OSM
                    if (mapaOSM != null)
                        mapaOSM.setVisibility(View.VISIBLE);
                    if (mapViewBox != null)
                        mapViewBox.setVisibility(View.GONE);
                    if (mapFragment != null && mapFragment.getView() != null)
                        mapFragment.getView().setVisibility(View.GONE);
                    break;
            }
        }
    }

    public void styleMapa(int tipoMapa) {
        try {
            boolean success = mMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, tipoMapa));
            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
    }

    public void cargarOpenStreetMap() {
        mapViewConfig = new MapViewConfig();
        mapaOSM = findViewById(R.id.openmapview);
        mapaOSM = mapViewConfig.configure(this, spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0), mapaOSM, this);
        mapaOSM.setOnTouchListener((v, event) -> {
            int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN: {
                }
                case MotionEvent.ACTION_UP: {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal, getBaseContext().getTheme()));
                    } else {
                        iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal));
                    }
                    contadorZoom = 1;
                    guardarEstadoRastreo(0);
                    isRastreo = false;
                    modoGpsRastreo(isRastreo, locacionDefault);
                }
            }
            return false;
        });
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
        return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
    }

    public void determinarModoIncio() {
        if (spConfiguracionAvanzada.getBoolean("modoAvanzado", false)) {
            tres.setVisibility(View.VISIBLE);
            btnFlotante.setVisibility(View.VISIBLE);
        } else {
            tres.setVisibility(View.GONE);
            btnFlotante.setVisibility(View.VISIBLE);
        }
        tres.setVisibility(View.VISIBLE);
        btnFlotante.setVisibility(View.VISIBLE);

    }

    public JSONObject configuracionCobro(String dataConfig) {
        JSONObject objetoConfigRespuesta = null;
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        if (objetoConfig.getInt("id") == 13) {
                            objetoConfigRespuesta = objetoConfig;
                        }
                    }
                }
            } catch (JSONException e) {
                e.getMessage();
            }
        }
        return objetoConfigRespuesta;
    }

    public void presentarDialogoFinCarrera() {
        String dataConfig = servicioSocket.getSpParametrosConfiguracion().getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (configuracionCobro(dataConfig) != null) {
            JSONObject objetoConfig = configuracionCobro(dataConfig);
            if (objetoConfig.has("id")) {
                try {
                    if (objetoConfig.getInt("id") == 13) {
                        if (objetoConfig.getInt("h") == 1) {
                            presentarValorCobro();
                        } else {
                            if (servicioSocket.getIdPedidoSeleccionado() != 0) {
                                presentar_info_fin_carrera();
                                Log.e("estado26", "estado26");
                                cambiarEstadoLibre();
                            } else if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                                presentar_info_fin_carrera();
                                Log.e("estado27", "estado27");
                                cambiarEstadoLibre();
                            } else {
                                Log.e("estado28", "estado28");
                                cambiarEstadoLibre();
                            }
                            Log.e("estado29", "estado29");
                            cambiarEstadoLibre();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            presentarValorCobro();
        }
    }

    public void modoGpsRastreo(boolean isRastreo, Location location) {
        agregarMarcadorBoxTaxista(location);
        if (location != null) {
            if (isRastreo) {
                if (location.hasBearing()) {
                    if (mapaOSM != null) {
                        mapaOSM.getController().setZoom(18.60);
                        mapaOSM.getController().animateTo(new GeoPoint(location));
                        mapaOSM.setMapOrientation((location.getBearing() * -1));
                    }

                    ////////////////////// MapBOX //////////////////////
                    if (mapBox != null) {
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        mapBox.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                .target(latLng)
                                .zoom(16)
                                .bearing(location.getBearing())
                                .tilt(0)
                                .build()), 200);
                    }

                    if (mMap != null) {
                        com.google.android.gms.maps.model.LatLng latLng =
                                new com.google.android.gms.maps.model.LatLng(location.getLatitude(), location.getLongitude());
                        mMap.animateCamera(com.google.android.gms.maps.CameraUpdateFactory.newCameraPosition(
                                new com.google.android.gms.maps.model.CameraPosition.Builder()
                                        .target(latLng)
                                        .zoom(16)
                                        .bearing(location.getBearing())
                                        .tilt(0)
                                        .build()));
                    }

                } else {
                    if (mapaOSM != null) {
                        mapaOSM.getController().setZoom(18.60);
                        mapaOSM.getController().animateTo(new GeoPoint(location));
                    }
                    ////////////////////// MapBOX //////////////////////
                    if (mapBox != null) {
                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        mapBox.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                .target(latLng)
                                .zoom(16)
                                .tilt(0)
                                .build()), 200);
                    }
                    if (mMap != null) {
                        com.google.android.gms.maps.model.LatLng latLng =
                                new com.google.android.gms.maps.model.LatLng(location.getLatitude(), location.getLongitude());
                        mMap.animateCamera(com.google.android.gms.maps.CameraUpdateFactory.newCameraPosition(
                                new com.google.android.gms.maps.model.CameraPosition.Builder()
                                        .target(latLng)
                                        .zoom(16)
                                        .tilt(0)
                                        .build()));
                    }
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (solicitudSeleccionada != null) {
                        Intent i = new Intent(Intent.ACTION_CALL,
                                Uri.parse("tel:" + solicitudSeleccionada.getCelular()));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        } else {
                            MapaBaseActivity.this.startActivity(i);
                            servicioSocket.estadoBotonDelTaxi(7);
                            servicioSocket.enviarEventoConductor(servicioSocket.getIdSolicitudSeleccionada(), 2, locacionDefault);
                        }
                    } else {
                        utilidades.customToast(this, "Ahora puede llamar.");
                    }
                } else {
                    Snackbar.make(drawerLayout, "Al no activar el permiso, no se podrá llamar al cliente.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_AUDIO: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    abrirChatBroadcast();
                } else {
                    Snackbar.make(drawerLayout, "Al no activar el permiso, no podrá enviar audios.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal, getBaseContext().getTheme()));
                    } else {
                        iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal));
                    }
                    isRastreo = false;
                    modoGpsRastreo(isRastreo, locacionDefault);
                    contadorZoom = 1;
                    guardarEstadoRastreo(0);
                } else {
                    Snackbar.make(drawerLayout, "Al no activar el permiso, no podrá ver su ubicación.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(MapaBaseActivity.this, FormularioDenunciaActivity.class));
                } else {
                    utilidades.customToast(this, "Si no activa el permiso de la cámara no puede realizar la denuncia.");
                }
            }
        }
    }

    public void afirmarPosiblesSoliciudes() {
        if (servicioSocket != null && locacionDefault != null) {
            dialogosGenericos = new DialogosGenericos();
            dialogosGenericos.afirmarPosiblesSoliciudes(this, servicioSocket, locacionDefault);
        }
    }

    public boolean registrarPago() {
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 14:
                                if (objetoConfig.getInt("h") == 1) {
                                    return true;
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.getMessage();
            }
        }
        return false;
    }


    public boolean mostrarMiSaldo() {
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 34:
                                if (objetoConfig.getInt("h") == 1)
                                    return true;
                                else
                                    return false;
                        }
                    }
                }
            } catch (JSONException e) {
                e.getMessage();
            }
        }
        return false;
    }

    public void estadoServicio() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mBound) {
                    comprobarEstadoEntrar();
                    servicioSocket.getEstadoPosibleSolicitud();
                    servicioSocket.activarConexion();
                    servicioSocket.activarEscuchas();
                    servicioSocket.notificarCambioEnServidor();
                    servicioSocket.estadoPago();
                    servicioSocket.cancelNotification(MapaBaseActivity.this, 4);
                    if (servicioSocket.solicitudSeleccionada != null) {
                        solicitudSeleccionada = servicioSocket.solicitudSeleccionada;
                        if (solicitudSeleccionada.isPedido()) {//Se comprueba el estado del pedido
                            servicioSocket.estadoPedido();
                            setInfoCarrera(solicitudSeleccionada);
                        } else { //Se comprueba el estado de la solicitud
                            servicioSocket.estadoSolicitud();
                            servicioSocket.getEstadoHora();
                            setInfoCarrera(solicitudSeleccionada);
                        }
                    } else {
                        setInfoCarrera(null);
                        servicioSocket.retomarCarreras();
                    }
                }
            }
        });
    }

    public void setInfoCarrera(Solicitud infoCarrera) {
        String data = spLogin.getString(VariablesGlobales.PLACA_VEHICULO, "") + "\n" + spLogin.getString(VariablesGlobales.CIUDAD, "");
        if (infoCarrera != null) {
            if (infoCarrera.isPedido()) {
                data = spLogin.getString(VariablesGlobales.PLACA_VEHICULO, "") + "\n" + spLogin.getString(VariablesGlobales.CIUDAD, "") + "\n" + solicitudSeleccionada.getIdPedido();
            } else {
                data = spLogin.getString(VariablesGlobales.PLACA_VEHICULO, "") + "\n" + spLogin.getString(VariablesGlobales.CIUDAD, "") + "\n" + solicitudSeleccionada.getIdSolicitud();
            }
        }
        tvPlaca.setText(data);

    }

    public void comprobarEstadoEntrar() {
        if (spEstadobtn.getBoolean(VariablesGlobales.ESTADO_BTN, true)) {
            Log.e("estado30", "estado30");
            cambiarEstadoLibre();
        } else {
            Log.e("estado4", "estado4");
            cambiarEstadoOcupado();
        }
    }

    private long touchStartTime = 0;
    private long lastTouchTime = 0;

    @Override
    public void apagarPantalla() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                win.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, 1);
            }
        });
    }

    //Encende
    @Override
    public void encenderPantalla() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, 4 * 60 * 60 * 1000);
                win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        });
    }

    public void cargarBtnClienteAbordoAcciones() {
        btnClienteAbordo.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    progressBarClienteAbordo.setVisibility(View.VISIBLE);
                    v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.clic_anim_grance));
                    progressBarClienteAbordo.setProgress(0);
                    touchStartTime = System.currentTimeMillis();
                    return true;
                case MotionEvent.ACTION_UP:
                    progressBarClienteAbordo.setVisibility(View.GONE);
                    v.clearAnimation();
                    return true;
                case MotionEvent.ACTION_CANCEL:
                    progressBarClienteAbordo.setVisibility(View.GONE);
                    v.clearAnimation();
                    return true;
                case MotionEvent.ACTION_MOVE:
                    lastTouchTime = System.currentTimeMillis();
                    long total = lastTouchTime - touchStartTime;
                    int totalMenos = 800 - (int) total;
                    progressBarClienteAbordo.setProgress(totalMenos);
                    if (total >= 800) {
                        if (RespuestaConfiguracion.isActivarIconoEstadoGpsSolicitud(MapaBaseActivity.this)) {
                            if (!solicitudSeleccionada.isPedido() && solicitudSeleccionada.getConP() <= 3) {
                                mostrarValidarUbicacion(solicitudSeleccionada.getIdSolicitud(), RespuestaConfiguracion.textActivarIconoEstadoGpsSolicitud(MapaBaseActivity.this));
                            } else {
                                dialogoAbordo();
                            }
                        } else {
                            dialogoAbordo();
                        }
                        progressBarClienteAbordo.setVisibility(View.GONE);
                    }
                    return true;
            }
            return true;
        });
    }

    public void cargarEventoPanicoConTiempo() {
        tres.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        v.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        touchStartTime = System.currentTimeMillis();
                        return true;
                    case MotionEvent.ACTION_UP:
                        v.getBackground().clearColorFilter();
                        v.invalidate();
                        return true;
                    case MotionEvent.ACTION_CANCEL:
                        v.getBackground().clearColorFilter();
                        v.invalidate();
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        lastTouchTime = System.currentTimeMillis();
                        long total = lastTouchTime - touchStartTime;
                        if (total >= 500) {
                            if (locacionDefault != null) {
                                servicioSocket.enviarEvento(locacionDefault, 1, 0, 0);
                            }
                            v.getBackground().clearColorFilter();
                            v.invalidate();
                        }
                        return true;
                }
                return false;
            }
        });
    }

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;

    public void cargarMapaGoogle() {
        mapViewGoogle = new MapViewGoogle(this);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mapViewGoogle.setValoresIniciales(spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0), mMap);
                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        String position = marker.getSnippet().substring(0, 1);
                        switch (position) {
                            case "s":
                                if (listaSolicitudes.size() > 0) {
                                    if (!marker.getSnippet().isEmpty()) {
                                        int numSolicitud = Integer.parseInt(marker.getSnippet().substring(1, marker.getSnippet().length()));
                                        solicitudSeleccionada = listaSolicitudes.get(numSolicitud);
                                        seleccionarTiempo();
                                    }
                                }
                                break;
                            default:
                                marker.showInfoWindow();
                                break;
                        }
                        return false;
                    }
                });
                cambioMapa();
            }
        });
        onTouchGoogle();
    }


    private TextView tvDireccionCliente;
    private LinearLayout lyEnCarrera;

    public void cargarDireccionCliente() {
        lyEnCarrera = findViewById(R.id.ly_en_carrera);
        tvDireccionCliente = findViewById(R.id.tv_direccion);
        ImageView imgBtnGpsCliente = findViewById(R.id.img_btn_gps_cliente);
        imgBtnGpsCliente.setOnClickListener(v ->
                ubicarCliente());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_mapa);
        directorio();
        datosTaximetro = DatosTaximetro.obtenerDatosTaximetro(this);
        cancelNotification(this, Config.NOTIFICATION_ID);
        mapaPresenter = new MapaPresenter(this, this);
        win = getWindow();
        ButterKnife.bind(this);
        cargarMapaGoogle();
        btnDejarRastrear.setVisibility(View.GONE);
        imgBtnRastreoPanico.setVisibility(View.GONE);
        iniciarMapBox(savedInstanceState);
        cargarToolBar();
        if (savedInstanceState != null) {
            if (savedInstanceState.getParcelable("localizacion") != null) {
                locacionDefault = savedInstanceState.getParcelable("localizacion");
            }
        }
        btnGpsDisable.setVisibility(View.GONE);
        drawerLayout = findViewById(R.id.drawer_layout);
        rlDesactivado = findViewById(R.id.rl_desactivado);
        rlDesactivado.setOnClickListener(this);
        rlDesactivado.setOnLongClickListener(v -> {
            if (rlDesactivado.getVisibility() == View.VISIBLE) {
                rlDesactivado.setVisibility(View.GONE);
            }
            return true;
        });
        btnFlotante = findViewById(R.id.include_btn_extra);
        dos = findViewById(R.id.accion_posibles_solicitudes);
        tres = findViewById(R.id.accion_panico);

        btnReportarPirata = findViewById(R.id.reportar_pirata);
        btnReportarPirata.setOnClickListener(this);
        if (!RespuestaConfiguracion.viewReportePirata(this)) {
            btnReportarPirata.setVisibility(View.GONE);
        }
        cargarDireccionCliente();
        solicitudSeleccionada = Solicitud.createSolicitud(spSolicitud.getString("key_objeto_solicitud", null));
        dos.setOnClickListener(this);
        tres.setOnClickListener(this);
        cargarEventoPanicoConTiempo();
        cargarOpenStreetMap();
        btnWaze = findViewById(R.id.button);
        btnWaze.setVisibility(View.GONE);
        btnEstadoCarrera = findViewById(R.id.btnEstado);
        inclTaximetro = findViewById(R.id.incl_taximetro);
        btnInternetOn = findViewById(R.id.btnInternetOn);
        btnInternetOff = findViewById(R.id.btnInternetOff);
        btnGpsOn = findViewById(R.id.btnGpsOn);
        btnGpsOff = findViewById(R.id.btnGpsOff);
        numeroSolicitudes = findViewById(R.id.numSolicitudes);
        numeroSolicitudes.setOnClickListener(this);
        listaSolicitudes = new ArrayList<>();
        tvCronometroTiempo = findViewById(R.id.tvCronometroTiempo);
        lyEnCarrera.setVisibility(View.GONE);
        btnOffLine = findViewById(R.id.btn_off_line);
        imageButtonMensajes = findViewById(R.id.image_button_mensajes);
        imageButtonMensajes.setVisibility(View.GONE);
        imgBtnComprando = findViewById(R.id.im_btn_comprando);
        imgBtnComprando.setOnClickListener(this);
        imgBtnComprado = findViewById(R.id.img_btn_comprado);
        imgBtnComprado.setOnClickListener(this);
        imgBtnComprado.setVisibility(View.GONE);
        imgBtnComprando.setVisibility(View.GONE);
        imageButtonMensajes.setOnClickListener(this);
        btnOffLine.setOnClickListener(this);
        btnEstadoCarrera.setTag(0);
        btnEstadoCarrera.setOnClickListener(this);
        btnNumSolicitudes = findViewById(R.id.btnNumSolicitudes);
        btnNumSolicitudes.setOnClickListener(this);
        ibtnInformacionSolicitud = findViewById(R.id.ibtnInformacionSolicitud);
        ibtnInformacionSolicitud.setOnClickListener(this);
        ibtnInformacionSolicitud.setVisibility(View.GONE);
        banderaSalida = false;
        banderaClienteAsignado = false;
        btnLlamarCliente = findViewById(R.id.btnLlamarCliente);
        btnLlamarCliente.setEnabled(false);
        btnLlamarCliente.setOnClickListener(this);
        btnAviso = findViewById(R.id.btnAviso);
        btnAviso.setEnabled(false);
        btnAviso.setOnClickListener(this);
        SensorManager mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        if (mSensorManager != null) {
            mSensorManager.registerListener(this,
                    mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                    SensorManager.SENSOR_DELAY_NORMAL);
        }
        iBtnPosicion = findViewById(R.id.imgBtnLocation);
        iBtnPosicion.setOnClickListener(this);
        btnClienteAbordo = findViewById(R.id.btnClienteaBordo);
        btnClienteAbordo.setEnabled(false);
        btnClienteAbordo.setOnClickListener(this);
        determinarModoIncio();
        if (spConfiguracionAvanzada.getBoolean("modoAvanzadoBluetooth", false) &&
                spConfiguracionAvanzada.getBoolean("modoAvanzadoSensor", false)) {
            btAdapter = BluetoothAdapter.getDefaultAdapter();
            checkBTState();
            Intent intent2 = getIntent();
            String address = intent2.getStringExtra(DeviceListSensorActivity.EXTRA_DEVICE_ADDRESS);
            if (address != null && btAdapter != null) {
                BluetoothDevice device = btAdapter.getRemoteDevice(address);
                try {
                    btSocket = createBluetoothSocket(device);
                    btSocket.connect();
                } catch (IOException e) {
                    Builder dialogo = new Builder(MapaBaseActivity.this);
                    dialogo.setTitle(R.string.str_alerta_may);
                    dialogo.setMessage(R.string.msj_vincule_dispositivo);
                    dialogo.setCancelable(false);
                    dialogo.setPositiveButton(R.string.str_aceptar, (dialogo1, id) -> {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                        finish();
                    });
                    dialogo.setNegativeButton(R.string.str_cancelar, (dialogo1, id) -> {
                        dialogo1.dismiss();
                        utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.msj_no_se_puede_usar_sensor_asientos));
                    });
                    dialogo.show();
                    try {
                        btSocket.close();
                    } catch (IOException e2) {
                        //insert code to deal with this
                    }
                }
                mConnectedThread = new ConnectedThread(btSocket);
                mConnectedThread.start();
            } else {
                startActivity(new Intent(this, DeviceListSensorActivity.class));
                finish();
            }


        }
        //Bluethooo asientos
        manejadorAsiento();
        setInfoCarrera(null);
        mapaPresenter.obtenerBoletin(spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0));
        mapaPresenter.obtenerValoracionConductor(spLogin.getInt(VariablesGlobales.ID_USUARIO, 0));
        mapaPresenter.getConfiguracionTaximetro(spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0));
        new MainPresentador(null, null, MapaBaseActivity.this, MapaBaseActivity.this, null).obtenerConfiguracionServerNew(null, VariablesGlobales.NUM_ID_APLICATIVO, VariablesGlobales.ID_PLATAFORMA, VariablesGlobales.APP, utilidades.obtenerVersion(MapaBaseActivity.this));
        envioKeyFirebasePush();
        definirModoRastreoInicio();
        cargarBtnClienteAbordoAcciones();
        configuracionMapaInicial();

    }

    public void envioKeyFirebasePush() {
        if (spLogin.getBoolean(VariablesGlobales.LOGEADO, false)) {
            SharedPreferences spFirebase = getSharedPreferences(VariablesGlobales.ESTADO_FIREBASE, Context.MODE_PRIVATE);
            boolean isEnviado = spFirebase.getBoolean(VariablesGlobales.KEY_FIREBASE_ENVIADO, false);
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            if (refreshedToken != null) {
                if (!isEnviado) {
                    new RegistrarTokenPush(this).registrarToken(spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), refreshedToken, false);
                }
            }
        }
    }

    @OnClick({R2.id.imb_zoom_mas, R2.id.imb_zoom_menos, R2.id.img_btn_feria, R2.id.btnGpsOn,
            R2.id.btnGpsDisable, R2.id.btn_dejar_rastrear, R2.id.img_btn_rastreo_panico})
    public void onClick(View v) {
        if (v != null) {
            v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.clic_anim));
        }
        Intent intent = new Intent(MapaBaseActivity.this, ListaSolicitudesActivity.class);
        intent.putExtra("tamanio", listaSolicitudes.size());
        assert v != null;
        int i1 = v.getId();
        if (i1 == R.id.img_btn_rastreo_panico) {
            if (respuestaPosibleSolicitudes != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal, getBaseContext().getTheme()));
                } else {
                    iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal));
                }
                contadorZoom = 1;
                guardarEstadoRastreo(0);
                isRastreo = false;
                modoGpsRastreo(isRastreo, locacionDefault);

                if (mapaOSM != null) {
                    mapaOSM.getController().setZoom(18.60);
                    mapaOSM.getController().animateTo(new GeoPoint(respuestaPosibleSolicitudes.getLt(), respuestaPosibleSolicitudes.getLg()));
                }
                ////////////////////// MapBOX //////////////////////
                if (mapBox != null) {
                    LatLng latLng = new LatLng(respuestaPosibleSolicitudes.getLt(), respuestaPosibleSolicitudes.getLg());
                    mapBox.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                            .target(latLng)
                            .zoom(16)
                            .tilt(0)
                            .build()), 200);
                }
            }

        } else if (i1 == R.id.btn_dejar_rastrear) {
            Builder builderRastreo = new Builder(MapaBaseActivity.this);
            builderRastreo.setMessage(R.string.msj_dejar_rastrear)
                    .setTitle(R.string.str_alerta_min)
                    .setPositiveButton(R.string.str_si, (dialog, id) -> {
                        dialog.cancel();
                        if (mBound) {
                            servicioSocket.dejarRastrearPanico();
                            servicioSocket.prenderEscuchaPosibleSolicitud();
                        }
                        mapViewConfig.marketPanico(null, null, mapaOSM, false);
                        mapViewConfigMapBox.marketPanico(null, null, mapBox, false);
                        btnDejarRastrear.setVisibility(View.GONE);
                        imgBtnRastreoPanico.setVisibility(View.GONE);
                        respuestaPosibleSolicitudes = null;
                    })
                    .setNegativeButton(R.string.str_no, (dialog, id) -> dialog.cancel());
            builderRastreo.show();

        } else if (i1 == R.id.btnNumSolicitudes) {
            if (servicioSocket.isEstadoBoton) {
                startActivity(intent);
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
            }

        } else if (i1 == R.id.numSolicitudes) {
            if (mBound) {
                if (servicioSocket.isEstadoBoton) {
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                }
            }

        } else if (i1 == R.id.btnGpsOn) {
            if (locacionDefault != null) {
                Builder dialogo = new Builder(MapaBaseActivity.this);
                dialogo.setTitle(R.string.informacion);
                dialogo.setMessage(getString(R.string.str_nivel_confianza_gps) + utilidades.round(locacionDefault.getAccuracy(), 2));
                dialogo.setCancelable(false);
                dialogo.setPositiveButton(R.string.str_ok, (dialogo1, id) -> dialogo1.dismiss());
                if (!isFinishing()) {
                    dialogo.show();
                }
            }


        } else if (i1 == R.id.btnGpsDisable) {
            if (locacionDefault != null) {
                Builder dialogoRojo = new Builder(MapaBaseActivity.this);
                dialogoRojo.setTitle(R.string.str_alerta_min);
                dialogoRojo.setMessage(getString(R.string.str_nivel_confianza_gps) + utilidades.round(locacionDefault.getAccuracy(), 2) + getString(R.string.str_muy_alto));
                dialogoRojo.setCancelable(false);
                //Nuevo
                dialogoRojo.setNegativeButton(R.string.str_reestablecer, (dialog, which) -> {
                    LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                    locationManager.sendExtraCommand(LocationManager.GPS_PROVIDER, "delete_aiding_data", null);
                    Bundle bundle = new Bundle();
                    locationManager.sendExtraCommand("gps", "force_xtra_injection", bundle);
                    locationManager.sendExtraCommand("gps", "force_time_injection", bundle);
                    dialog.dismiss();
                    Toast.makeText(MapaBaseActivity.this, "Variables Gps restablecidas.", Toast.LENGTH_LONG).show();
                });
                //aca
                dialogoRojo.setPositiveButton(R.string.aceptar, (dialogo1, id) -> dialogo1.dismiss());
                if (!isFinishing()) {
                    dialogoRojo.show();
                }
            }

        } else if (i1 == R.id.im_btn_comprando) {
            if (mBound) {
                if (solicitudSeleccionada != null) {
                    if (solicitudSeleccionada.getIdPedido() != 0) {
                        servicioSocket.enviarComprandoPedido();
                    } else {
                        utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.sin_pedido));
                    }
                } else {
                    utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.sin_carrera));
                }
            }

        } else if (i1 == R.id.img_btn_comprado) {
            if (mBound) {
                if (solicitudSeleccionada != null) {
                    if (solicitudSeleccionada.isPedido()) {
                        if (solicitudSeleccionada.getT() != 2) {
                            if (dialogPrecioPedido != null && dialogPrecioPedido.isShowing()) {
                                return;
                            }
                            dialogPrecioPedido = createDialogoPrecioPedido();
                            dialogPrecioPedido.show();
                        } else {
                            servicioSocket.enviarPedidoComprado(0);
                        }
                    } else {
                        utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.sin_pedido));
                    }
                } else {
                    utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.sin_carrera));
                }
            }

        } else if (i1 == R.id.image_button_mensajes) {
            if (servicioSocket.enCarrera()) {
                runOnUiThread(() -> {
                    imageButtonMensajes.setImageDrawable(setBadgeCount(0, getResources().getDrawable(R.drawable.ic_message)));
                    servicioSocket.isMensajeNuevo = false;
                });
                startActivity(new Intent(MapaBaseActivity.this, ChatActivity.class));
            } else {
                utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.sin_carrera));
            }

        } else if (i1 == R.id.btn_off_line) {
            isBotonConexion = true;
            v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.clic_anim));
            utilidades.customToast(MapaBaseActivity.this, getString(R.string.desconectado_servidor));
            new MainPresentador(null, null, MapaBaseActivity.this, MapaBaseActivity.this, null).obtenerConfiguracionServerNew(null, VariablesGlobales.NUM_ID_APLICATIVO, VariablesGlobales.ID_PLATAFORMA, VariablesGlobales.APP, utilidades.obtenerVersion(MapaBaseActivity.this));

        } else if (i1 == R.id.btnEstado) {
            int val = (int) btnEstadoCarrera.getTag();
            Log.e(TAG, "onClick: " + val);
            switch (val) {
                case 0:
                    lyPublicidad.hide();
                    if (!utilidades.isVertical(MapaBaseActivity.this)) {
                        peticionDePublicidad(VariablesPublicidad.OCUPADO_SIN_CARRERA, 0);
                    }
                    Log.e("estado5", "estado5");
                    cambiarEstadoOcupado();
                    if (!spConfiguracionApp.getBoolean(VariablesGlobales.CONFIGURACION_TAXIMETRO, true)) {
                        utilidades.customToast(MapaBaseActivity.this, getString(R.string.str_active_modulo_taximetro));
                    } else {
                        if (!servicioSocket.isTaximetroRun) {
                            activarTaximetro();
                            lyTaximetro.setVisibility(View.GONE);
                        } else {
                            drawerLayout.closeDrawers();
                        }
                    }
                    break;
                case 1:
                    if (servicioSocket.isCambiarLibre) {
                        if (servicioSocket.enCarrera()) {
                            presentarEventualidades();
                        } else {
                            if (servicioSocket.isTaximetroRun) {
                                Builder builder = new Builder(MapaBaseActivity.this);
                                if (solicitudSeleccionada != null) {
                                    if (solicitudSeleccionada.getTipoFormaPago() == 4) {
                                        builder.setIcon(R.mipmap.ic_saldo_ktaxi);
                                        builder.setCancelable(true);
                                        builder.setTitle(R.string.informacion);
                                    } else if (solicitudSeleccionada.getTipoFormaPago() == VariablesGlobales.TARJETA_CREDITO) {
                                        builder.setMessage(R.string.msj_terminar_carrera_cobrar);
                                        builder.setCancelable(true);
                                        builder.setTitle(R.string.informacion);
                                    } else {
                                        builder.setMessage(R.string.msj_terminar_carrera_cobrar);
                                        builder.setCancelable(true);
                                        builder.setTitle(R.string.informacion);
                                    }
                                } else {
                                    builder.setMessage(R.string.msj_terminar_carrera_cobrar);
                                    builder.setCancelable(true);
                                    builder.setTitle(R.string.informacion);
                                }
                                builder.setPositiveButton(R.string.str_si, (dialog, id) -> {
                                    dialog.cancel();
                                    servicioSocket.finalizarTaximetro();
                                    stopService(new Intent(MapaBaseActivity.this, TaximetroService.class));
                                    inclTaximetro.setVisibility(View.GONE);
                                    presentarDialogoFinCarrera();
                                    guardarDatosTaximetro(); // Aqui es donde se guarda los datos del táximetro.
                                    tvHoraInicio.setText("");
                                    tvDistanciaRecorrida.setText("");
                                    tvVelocidad.setText("");
                                    tvTiempo.setText("");
                                    tvCosotoArranque.setText("");
                                    tvValorDistancia.setText("");
                                    tvTiempoEspera.setText("");
                                    tvValorTiempo.setText("");
                                    tvSubTotal.setText("");
                                    if (!utilidades.isVertical(MapaBaseActivity.this)) {
                                        peticionDePublicidad(VariablesPublicidad.LIBRE_SIN_CARRERA, 0);
                                    }
                                });
                                builder.setNegativeButton(R.string.str_no, (dialog, id) -> dialog.cancel());
                                if (!isFinishing()) {
                                    builder.show();
                                }
                            } else {
                                presentarDialogoFinCarrera();
                                if (!utilidades.isVertical(MapaBaseActivity.this)) {
                                    peticionDePublicidad(VariablesPublicidad.LIBRE_SIN_CARRERA, 0);
                                }
                            }
                        }
                    } else {
                        Toast.makeText(MapaBaseActivity.this, R.string.msj_no_puede_cambiar_libre, Toast.LENGTH_LONG).show();
                    }
                    break;
            }

        } else if (i1 == R.id.ibtnInformacionSolicitud) {
            informacionSolicitud();

        } else if (i1 == R.id.btnLlamarCliente) {
            if (mBound) {
                Intent i = new Intent(Intent.ACTION_CALL,
                        Uri.parse("tel:" + solicitudSeleccionada.getCelular()));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL);
                    return;
                } else {
                    startActivity(i);
                    servicioSocket.estadoBotonDelTaxi(7);
                    servicioSocket.enviarEventoConductor(servicioSocket.solicitudSeleccionada.getIdSolicitud(), 2, locacionDefault);
                }
            }

        } else if (i1 == R.id.btnAviso) {
            if (solicitudSeleccionada != null) {
                if (solicitudSeleccionada.isPedido()) {
                    if (solicitudSeleccionada.getT() == 1) {
                        servicioSocket.enviarChat(getString(R.string.msj_me_encuentro_lugar_compra));
                    } else {
                        servicioSocket.enviarChat(getString(R.string.msj_encomienda_lugar));
                    }
                } else {
                    servicioSocket.enviarMensaje(getString(R.string.mensaje_para_usuario));
                }
            }

        } else if (i1 == R.id.imgBtnLocation) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
                return;
            } else {
                if (locacionDefault != null) {
                    switch (contadorZoom) {
                        case 0:
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal, getBaseContext().getTheme()));
                            } else {
                                iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal));
                            }
                            isRastreo = false;
                            modoGpsRastreo(isRastreo, locacionDefault);
                            guardarEstadoRastreo(0);
                            contadorZoom = 1;
                            break;
                        case 1:
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo, getTheme()));
                            } else {
                                iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo));
                            }
                            contadorZoom = 0;
                            guardarEstadoRastreo(1);
                            isRastreo = true;
                            modoGpsRastreo(isRastreo, locacionDefault);
                            break;
                    }
                }
            }

//            case R.id.btnClienteaBordo:
//                if (RespuestaConfiguracion.isActivarIconoEstadoGpsSolicitud(this)) {
//                    if (!solicitudSeleccionada.isPedido() && solicitudSeleccionada.getConP() <= 3) {
//                        mostrarValidarUbicacion(solicitudSeleccionada.getIdSolicitud(), RespuestaConfiguracion.textActivarIconoEstadoGpsSolicitud(this));
//                    } else {
//                        dialogoAbordo();
//                    }
//                } else {
//                    dialogoAbordo();
//                }
//                break;
        } else if (i1 == R.id.accion_posibles_solicitudes) {
            afirmarPosiblesSoliciudes();

        } else if (i1 == R.id.reportar_pirata) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                return;
            } else {
                startActivity(new Intent(MapaBaseActivity.this, FormularioDenunciaActivity.class));
            }

        } else if (i1 == R.id.accion_panico) {//                if (locacionDefault != null) {
//                    servicioSocket.enviarEvento(locacionDefault, 1, 0, 0);
//                }

        } else if (i1 == R.id.img_btn_feria) {
            Intent intentF = new Intent(MapaBaseActivity.this, EventoFeriaLojaActivity.class);
            intentF.putExtra("ip_feria", recursoFeria);
            intentF.putExtra("titulo", tituloPublicidad);
            startActivity(intentF);

        } else if (i1 == R.id.rl_desactivado) {
            if (!spConfiguracionApp.getBoolean(VariablesGlobales.ASISTENTE_VOZ, true)) {
                utilidades.mostrarMensajeCorto(this, "Debes bajar la velocidad para hacer uso del aplicativo");
            }

        } else {
        }
    }

    private void guardarDatosTaximetro() {
        DatosEnvioTaximetro datosEnvioTaximetro = new DatosEnvioTaximetro();
        datosEnvioTaximetro.setHoraInicio(tvHoraInicio.getText().toString());
        datosEnvioTaximetro.setHoraFin(utilidades.obtenerHora());
        datosEnvioTaximetro.setDistanciaRecorrida(tvDistanciaRecorrida.getText().toString());
        datosEnvioTaximetro.setTiempoTotal(tvTiempo.getText().toString());
        datosEnvioTaximetro.setTarifaArranque(tvCosotoArranque.getText().toString());
        datosEnvioTaximetro.setValorDistancia(tvValorDistancia.getText().toString());
        datosEnvioTaximetro.setTiempoEspera(tvTiempoEspera.getText().toString());
        datosEnvioTaximetro.setValorTiempo(tvValorTiempo.getText().toString());
        datosEnvioTaximetro.setTotalCarrera(totalCarrera);
        datosEnvioTaximetro.setEditarCostoC(valorEditarCostoC);

        datosEnvioTaximetro.save();
    }

    public void mostrarSaldoKtaxi() {
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 27://Para activar la vista de Saldo Ktaxi
                                if (objetoConfig.getInt("h") == 1) { //Habilitado mostrar
                                    isActivarSaldoKtaxi = true;
                                }
                                if (objetoConfig.getString("vd").equals("1")) { //Proximamente
                                    isActivarSaldoKtaxiProximanete = true;
                                }
                                break;
                            case 30://Para activar La vista de tarjeta de credito
                                if (objetoConfig.getInt("h") == 1) { //Habilitado mostrar
                                    isActivarCreditoTarjetaCredito = true;
                                }
                                if (objetoConfig.getString("vd").equals("1")) { //Proximamente
                                    isActivarTarjetaCreditoProximanete = true;
                                }
                                break;
                            case 29://Para activar La vista de tarjeta de credito
                                if (objetoConfig.getInt("h") == 1) { //Habilitado mostrar
                                    isActivarReferido = true;
                                }
                                if (objetoConfig.getString("vd").equals("1")) { //Proximamente
                                    isActivarRefediroProximante = true;
                                }
                                if (objetoConfig.has("nb")) {
                                    nombreReferido = objetoConfig.getString("nb");
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.getMessage();
            }
        }
    }


    public int configuracionMapaInicial() {
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 40://Para configurar el incio del mapa
                                if (objetoConfig.getInt("h") == 1) { //Habilitado
                                    final int modificado = Integer.parseInt(objetoConfig.getString("vd"));
                                    if (modificado != new ConfiguracionActivity().getUltimoUsado(this)) {
                                        Builder builder = new Builder(this);
                                        builder.setMessage("El mapa para su correcto funcionamiento debe ser cambiado a otro ¿Desea realizar el cambio?")
                                                .setTitle(R.string.str_alerta_min)
                                                .setPositiveButton(R.string.str_si, new OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                        spConfiguracionApp.edit().putInt(VariablesGlobales.TIPO_MAPA, modificado).apply();
                                                        new ConfiguracionActivity().setUltimoUsado(modificado, MapaBaseActivity.this);
                                                        cambioMapa();
                                                    }
                                                })
                                                .setNegativeButton(R.string.str_no, new OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                        builder.show();
                                    }

                                    return modificado;
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.getMessage();
                return 1;
            }
        }
        return 1;
    }

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_menu);
            ab.setDisplayHomeAsUpEnabled(true);
            if (!utilidades.isDesarrolloPorduccion(this)) {
                ab.setBackgroundDrawable(new ColorDrawable(utilidades.getColorWrapper(this, R.color.desarrollo)));
            }
        }
    }

    public boolean getEstadoFuncionalidad() {
        SharedPreferences spFuncionalidades = getSharedPreferences(VariablesGlobales.FUNCIONALIDADES, MODE_PRIVATE);
        return spFuncionalidades.getBoolean(VariablesGlobales.FUNCIONALIDADES_REFERIDO, false);
    }

    public void setEstadoFuncionalidad(boolean isReferido) {
        SharedPreferences spFuncionalidades = getSharedPreferences(VariablesGlobales.FUNCIONALIDADES, MODE_PRIVATE);
        SharedPreferences.Editor editor = spFuncionalidades.edit();
        editor.putBoolean(VariablesGlobales.FUNCIONALIDADES_REFERIDO, isReferido);
        editor.apply();
    }

    public void cargarToolBar() {
        mostrarSaldoKtaxi();
        setToolbar();
        NavigationView navigationView = findViewById(R.id.nav_view);
        menuNav = navigationView.getMenu();
        //Modificar para poner en produccion......
        menuNav.findItem(R.id.nav_taximetro_local).setVisible(
                spConfiguracionAvanzada.getBoolean("modoAvanzadoBluetooth", false) &&
                        spConfiguracionAvanzada.getBoolean("modoAvanzadoImpresion", false));
        if (datosTaximetro != null) {
            menuNav.findItem(R.id.fun_taximetro).setVisible(false);
        }
        if (registrarPago()) {
            menuNav.findItem(R.id.nav_pago).setVisible(true);
        } else {
            menuNav.findItem(R.id.nav_pago).setVisible(false);
        }
        if (isActivarSaldoKtaxi) {
            menuNav.findItem(R.id.nav_saldo_ktaxi).setVisible(true);
        } else {
            menuNav.findItem(R.id.nav_saldo_ktaxi).setVisible(false);
        }
        if (isActivarCreditoTarjetaCredito) {
            menuNav.findItem(R.id.nav_tarjeta_credito).setVisible(true);
        } else {
            menuNav.findItem(R.id.nav_tarjeta_credito).setVisible(false);
        }
        if (mostrarMiSaldo()) {
            menuNav.findItem(R.id.nav_paquetes).setVisible(true);
        } else {
            menuNav.findItem(R.id.nav_paquetes).setVisible(false);
        }
        if (isActivarReferido && VariablesGlobales.NUM_ID_APLICATIVO != 14) {//Si queremos que aparesca en todos los dispositivos le quitamos la comparativa con NUM_ID_APLICATIVO
            menuNav.findItem(R.id.nav_codigo_referido).setVisible(true);
            menuNav.findItem(R.id.nav_codigo_referido).setTitle(nombreReferido);
            if (!getEstadoFuncionalidad()) {
                drawableReferido = menuNav.findItem(R.id.nav_codigo_referido).getIcon();
                if (drawableReferido != null) {
                    Drawable nuevoReferido = utilidades.setBadgeCount("Ver", drawableReferido);
                    if (nuevoReferido != null) {
//                        menuNav.findItem(R.id.nav_codigo_referido).setIcon(nuevoReferido); Pendiente no cambia
                    }
                }
            }
        } else {
            menuNav.findItem(R.id.nav_codigo_referido).setVisible(false);
        }
        if (isActivarRefediroProximante) {
            menuNav.findItem(R.id.nav_codigo_referido).setTitle(nombreReferido);
        }
        drawerLayout = findViewById(R.id.drawer_layout);
        View header = navigationView.getHeaderView(0);
        imageViewTaxista = header.findViewById(R.id.perfil_default);
        TextView textView = header.findViewById(R.id.username);
        TextView dondeEstoy = header.findViewById(R.id.donde_estoy);
        if (utilidades.isDesarrolloPorduccion(this)) {
            dondeEstoy.setVisibility(View.GONE);
        } else {
            dondeEstoy.setVisibility(View.VISIBLE);
        }
        ratingBar = header.findViewById(R.id.rating_promedio);

        FloatingActionButton btnVerCalificacion = header.findViewById(R.id.btn_calificaciones);
        btnVerCalificacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapaBaseActivity.this, ComentariosActivity.class);
                String nombreUsuario = spLogin.getString("nombres", " ") + " " + spLogin.getString("apellidos", " ");
                intent.putExtra("usuario", nombreUsuario);
                intent.putExtra("imagen", imagenGlobal);
                startActivity(intent);
            }
        });

        try {
            textView.setText(spLogin.getString("nombres", " ") + " " + spLogin.getString("apellidos", " "));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        setupDrawerContent(navigationView);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                isOpenMenuLateral = true;
                if (imagenGlobal != null && imageViewTaxista != null) {
                    imageViewTaxista.setImageBitmap(imagenGlobal);
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                isOpenMenuLateral = false;
            }
        };
        drawerLayout.addDrawerListener(drawerToggle);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        int i = menuItem.getItemId();
                        if (i == R.id.nav_pago) {
                            if (!spLogin.getString("cedula", "").equals("")) {
                                cambiarOcupado();
                                Intent intentPago = new Intent(MapaBaseActivity.this, PagosActivity.class);
                                startActivity(intentPago);
                            } else {
                                Builder builder = new Builder(MapaBaseActivity.this);
                                builder.setMessage("Para registrar su pago usted debe cerrar sesión e iniciar nuevamente.")
                                        .setTitle(R.string.str_alerta_min)
                                        .setPositiveButton(R.string.aceptar, new OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                if (!isFinishing()) {
                                    builder.show();
                                }
                            }

                        } else if (i == R.id.nav_miPerfil) {
                            presentarInformacionCliente();

                        } else if (i == R.id.nav_paquetes) {
                            startActivity(new Intent(MapaBaseActivity.this, SaldoPaquetesActivity.class));

                        } else if (i == R.id.nav_cambiarclave) {
                            presentarCambioCable();

                        } else if (i == R.id.nav_cerrarsesion) {
                            confirmarSalida(true);
                            banderaCerrarSecion = true;

                        } else if (i == R.id.nav_taximetro_local) {
                            Log.e("estado6", "estado6");
                            cambiarEstadoOcupado();
                            servicioSocket.iniciarTaximetro();
                            startActivity(new Intent(MapaBaseActivity.this, TaximetroActivity.class));

                        } else if (i == R.id.nav_taximetro) {
                            if (!spConfiguracionApp.getBoolean(VariablesGlobales.CONFIGURACION_TAXIMETRO, true)) {
                                drawerLayout.closeDrawers();
                                utilidades.customToast(MapaBaseActivity.this, "Active el modulo de taximetro emergente en configuración de la app.");
                            } else {
                                if (!spEstadobtn.getBoolean(VariablesGlobales.ESTADO_BTN, true)) {
                                    if (!servicioSocket.isTaximetroRun) {
                                        drawerLayout.closeDrawers();
                                        activarTaximetro();
                                    } else {
                                        drawerLayout.closeDrawers();
                                        utilidades.mostrarMensajeCorto(MapaBaseActivity.this, "El taximetro esta ejecutandose en este momento.");
                                    }
                                } else {
                                    drawerLayout.closeDrawers();
                                    utilidades.mostrarMensajeCorto(MapaBaseActivity.this, "Debe estar en ocupado para poder usar el taximetro.");
                                }
                            }
                        } else if (i == R.id.nav_menu_cuentas) {
                            startActivity(new Intent(MapaBaseActivity.this, CuentasActivity.class));

                        } else if (i == R.id.nav_acerca_de) {
                            new DialogosGenericos().mostrarAcercaDe(MapaBaseActivity.this);

                        } else if (i == R.id.nav_saldo_ktaxi) {
                            if (isActivarSaldoKtaxiProximanete) {
                                Builder builder = new Builder(MapaBaseActivity.this);
                                builder.setMessage("Este servicio estará activo próximamente para tu ciudad.")
                                        .setTitle(R.string.informacion)
                                        .setPositiveButton(R.string.aceptar, new OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                if (!isFinishing()) {
                                    builder.show();
                                }
                            } else {
                                Log.e("estado7", "estado7");
                                cambiarEstadoOcupado();
                                startActivity(new Intent(MapaBaseActivity.this, SaldoKtaxiPagerActity.class));
                            }

                        } else if (i == R.id.nav_tarjeta_credito) {
                            if (isActivarTarjetaCreditoProximanete) {
                                Builder builder = new Builder(MapaBaseActivity.this);
                                builder.setMessage("Este servicio estará activo próximamente para tu ciudad.")
                                        .setTitle(R.string.informacion)
                                        .setPositiveButton(R.string.aceptar, new OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                if (!isFinishing()) {
                                    builder.show();
                                }
                            } else {
                                Log.e("estado8", "estado8");
                                cambiarEstadoOcupado();
                                startActivity(new Intent(MapaBaseActivity.this, TarjetaCreditoPagerActivity.class));
                            }

                        } else if (i == R.id.nav_codigo_referido) {
                            if (!getEstadoFuncionalidad() && drawableReferido != null) {
                                menuNav.findItem(R.id.nav_codigo_referido).setIcon(drawableReferido);
                                setEstadoFuncionalidad(true);
                            }
                            if (isActivarRefediroProximante) {
                                Builder builder = new Builder(MapaBaseActivity.this);
                                builder.setMessage("Este servicio estará activo próximamente para tu ciudad.")
                                        .setTitle(R.string.informacion)
                                        .setPositiveButton(R.string.aceptar, new OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                if (!isFinishing()) {
                                    builder.show();
                                }
                            } else {
                                Log.e("estado9", "estado9");
                                cambiarEstadoOcupado();
                                startActivity(new Intent(MapaBaseActivity.this, CodigoReferidoCompartirBaseActivity.class));
                            }

                        } else if (i == R.id.nav_menu_contactenos) {
                            dialogoContactoSoporte();

                        } else if (i == R.id.nav_menu_sugerencia) {
                            startActivity(new Intent(MapaBaseActivity.this, SugerenciasActivity.class));

                        } else if (i == R.id.nav_carrerasdespachadas) {
                            Intent intentCarreras = new Intent(MapaBaseActivity.this, HistorialActivity.class);
                            startActivity(intentCarreras);

                        } else if (i == R.id.nav_compras_despachados) {
                            Intent intentCompras = new Intent(MapaBaseActivity.this, HistorialComprasActivity.class);
                            startActivity(intentCompras);

                        } else if (i == R.id.nav_personalizacion) {
                            startActivity(new Intent(MapaBaseActivity.this, ConfiguracionActivity.class));

                        } else if (i == R.id.nav_recargar_electronica) {
                            Log.e("estado10", "estado10");
                            cambiarEstadoOcupado();
                            if (btnEstadoCarrera.getTag().toString().equals("1")) {
                                startActivity(new Intent(MapaBaseActivity.this, RecargasActivity.class));
                                utilidades.mostrarMensajeCorto(MapaBaseActivity.this, "Su estado esta en ocupado.");
                            } else {
                                utilidades.mostrarMensajeCorto(MapaBaseActivity.this, "Debe estar en ocupado para realizar esta acción.");
                            }


                        } else if (i == R.id.nav_salir) {
                            if (solicitudSeleccionada == null) {
                                confirmarSalida(false);
                            } else {
                                utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.en_carrera));
                            }

                        } else if (i == R.id.nav_evaluar) {
                            Builder dialogo = new Builder(MapaBaseActivity.this);
                            dialogo.setMessage(getResources().getString(R.string.calificar_app));
                            dialogo.setCancelable(true);
                            dialogo.setPositiveButton("EVALUAR AHORA", new OnClickListener() {
                                public void onClick(DialogInterface dialogo1, int id) {
                                    utilidades.calificarApp(MapaBaseActivity.this);
                                    dialogo1.dismiss();
                                }
                            });
                            dialogo.setNegativeButton("CANCELAR", new OnClickListener() {
                                public void onClick(DialogInterface dialogo1, int id) {
                                    dialogo1.dismiss();
                                }
                            });
                            if (!isFinishing()) {
                                dialogo.show();
                            }

                        } else if (i == R.id.nav_menu_reglamento) {
                            Intent intent = new Intent(MapaBaseActivity.this, PreRegistroBaseActivity.class);
                            String pag = "http://173.192.13.14:8080/reglamentos/" + spLogin.getInt("id_ciudad", 1) + ".html";
                            intent.putExtra("pagina", pag);
                            startActivity(intent);

                        } else {
                        }
                        return true;
                    }
                }
        );
    }


    public void probarGPSInicio() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mEnableGps();
        }
        if (!utilidades.comprobarGPS(this)) {
            btnGpsOn.setVisibility(View.GONE);
            btnGpsOff.setVisibility(View.VISIBLE);
            btnGpsDisable.setVisibility(View.GONE);
        } else {
            btnGpsOn.setVisibility(View.VISIBLE);
            btnGpsOff.setVisibility(View.GONE);
            btnGpsDisable.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registrarAcitvityServer(this, "MapaBaseActivity");
        mapViewBox.onResume();
        cambioMapa();
        preguntarEstadobateria();
        if (mapaOSM != null) {
            mapaOSM.onResume();
        }
        isEstadoActividad = true;
        probarGPSInicio();
        if (utilidades.estaConectado(this)) {
            cambioInterenet(false);
        } else {
            cambioInterenet(true);
        }
        //comprobarEstadoEntrar();
        if (solicitudSeleccionada != null)
            deleteNumber(solicitudSeleccionada.getCelular()); // solicitud selecionada
        btnFlotante.setVisibility(View.VISIBLE);
        configuracionServidorNew(true);
        utilidades.hideFloatingView(this);
    }

    private void enviarPrimerTramaMapa(int tipo, int estado) {
        if (mBound) {
            servicioSocket.enviarEvento(tipo, estado);
            servicioSocket.apagarEscuchaPosibleSolicitud();
            spConfiguracionApp.edit().putBoolean("enviotipoMapa", false).apply();
        }
    }

    private void checkBTState() {
        if (btAdapter == null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapViewBox.onPause();
        if (mapaOSM != null) {
            mapaOSM.onPause();
        }
        if (dialogoTiempo != null) {
            if (dialogoTiempo.isShowing()) {
                if (mBound) {
                    solicitudSeleccionada = null;
                    servicioSocket.borrarCarreraSeleccionada();
                    dialogoTiempo.dismiss();
                    dialogoTiempo = null;
                }
            }
        }
        if (dialogoInformacionSolicitud != null && dialogoInformacionSolicitud.isShowing())
            dialogoInformacionSolicitud.dismiss();
        if (alerDialogoCancelar != null && alerDialogoCancelar.isShowing())
            alerDialogoCancelar.dismiss();
        utilidades.showFloatingView(this, true);
    }

    @Override
    public void onStop() {
        super.onStop();
        mapViewBox.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapViewBox.onLowMemory();
        mapaOSM.onDetach();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        synchronized (this) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_ORIENTATION:
                    break;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onStart() {
        super.onStart();
        mapViewBox.onStart();

    }

    public void dialogoContactoSoporte() {
        dialogosGenericos = new DialogosGenericos();
        dialogosGenericos.dialogoContactoSoporte(this, utilidades);
    }

    @Override
    public void tiempoEnviado(final int tiempo, final Solicitud solicitudSelec) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                solicitudSeleccionada = solicitudSelec;
                if (pDialog != null) {
                    if (!pDialog.isShowing()) {
                        mostrarDialogoEspera(getString(R.string.esperando_confirmacion));
                    }
                }
                if (pDialog == null) {
                    mostrarDialogoEspera(getString(R.string.esperando_confirmacion));
                }

            }
        });
    }

    @Override
    public void solicitudAtendida(final String mensaje) {
        neutralizarDialogos();
        solicitudSeleccionada = null;
        servicioSocket.solicitudSeleccionada = null;
        servicioSocket.estadoPedido = 0;
        servicioSocket.estadoSolicitud = 0;
        if (!mensaje.equalsIgnoreCase("")) {
            dialogoCancelar(mensaje, false);
        }
    }

    @Override
    public void nuevaSolicitud(final ArrayList<Solicitud> listaSolicitudesEntrantes) {
        borrarCarrera();
        listaSolicitudes = listaSolicitudesEntrantes;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                reiniciarConfiguracionTaximetroTemporal();
                eliminarMarcadoresSolicitudesBoxYOpen();
                if (!utilidades.isVerificarDialogo(dialogoTiempo)) {
                    dialogoTiempo.dismiss();
                }
                if (listaSolicitudes != null && listaSolicitudes.size() > 0) {
                    numeroSolicitudes.setText(String.valueOf(listaSolicitudes.size()));
                    agregarMarketSolicitudes();
                } else {
                    numeroSolicitudes.setText("0");
                    if (dialogoTiempo != null) {
                        if (dialogoTiempo.isShowing()) {
                            dialogoTiempo.dismiss();
                        }
                    }
                    if (dialogoCompras != null) {
                        if (dialogoCompras.isShowing()) {
                            dialogoCompras.dismiss();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void solicitudCallCenter(final Solicitud solicitud) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (solicitud.getIdSolicitud() == 0) {
                    if (dialogCarreraCallCenter != null) {
                        if (dialogCarreraCallCenter.isShowing()) {
                            return;
                        }
                    }
                    solicitudSeleccionada = solicitud;
                    ibtnInformacionSolicitud.setVisibility(View.VISIBLE);
                    servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdSolicitud(), 1);
                    btnClienteAbordo.setEnabled(true);
                    servicioSocket.solicitudSeleccionada.setBotonAbordo(true);
                    solicitudSeleccionada.setSolicitudCC(true);
                    btnClienteAbordo.setEnabled(false);
                    btnLlamarCliente.setEnabled(false);
                    Log.e("estado11", "estado11");
                    cambiarEstadoOcupado();
                    numeroSolicitudes.setText("0");
                    servicioSocket.estadoSolicitud = 5;
                    Builder dialogo = new Builder(MapaBaseActivity.this);
                    dialogo.setTitle(R.string.informacion);
                    dialogo.setMessage("Le han asignado una carrera desde el call center.");
                    dialogo.setCancelable(false);
                    dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            informacionSolicitud();
                            servicioSocket.solicitudSeleccionada.setEstadoSolicitud(5);
                            servicioSocket.guardarEstadoSolicitud();
                            servicioSocket.enviarValidacionCallCenter(1);
                            if (dialogCarreraCallCenter != null) {
                                if (dialogCarreraCallCenter.isShowing()) {
                                    dialogCarreraCallCenter.dismiss();
                                }
                            }
                        }
                    });
                    dialogo.setNegativeButton("Cancelar", new OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            if (solicitudSeleccionada != null) {
                                servicioSocket.enviarValidacionCallCenter(0);
                                servicioSocket.cancelarSolicitud(9, 7, solicitudSeleccionada, false, 0, null);
                                btnEstadoCarrera.setTag(1);
                                borrarMarketCliente();
                                deshabilitarComponentesCarrera();
                                btnClienteAbordo.setEnabled(false);
                                lyEnCarrera.setVisibility(View.GONE);
                                servicioSocket.borrarCarreraSeleccionada();
                                imageButtonMensajes.setVisibility(View.GONE);
                                btnWaze.setVisibility(View.GONE);
                                servicioSocket.isEstadoAbordo = false;
                                servicioSocket.isMensajeAbordo = false;
                                if (dialogCarreraCallCenter != null) {
                                    if (dialogCarreraCallCenter.isShowing()) {
                                        dialogCarreraCallCenter.dismiss();
                                    }
                                }
                                solicitudSeleccionada = null;
                            }
                            Log.e("estado31", "estado31");
                            cambiarEstadoLibre();
                        }
                    });
                    dialogCarreraCallCenter = dialogo.create();
                    dialogCarreraCallCenter.show();
                } else if (solicitud.getIdSolicitud() < 0) {
                    solicitudSeleccionada = solicitud;
                    solicitudSeleccionada.setSolicitudCC(true);
                    solicitudSeleccionada.setIdSolicitud(-1 * solicitudSeleccionada.getIdSolicitud());
                    servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdSolicitud(), 1);
                    servicioSocket.solicitudSeleccionada.setEstadoSolicitud(5);
                    servicioSocket.guardarEstadoSolicitud();
                    btnWaze.setVisibility(View.VISIBLE);
                    if (pDialog != null) {
                        pDialog.dismiss();
                    }
                    Log.e("estado12", "estado12");
                    cambiarEstadoOcupado();
                    habilitarComponentesCarrera();
                    informacionSolicitud();
                    servicioSocket.estadoSolicitud = 5;
                    btnClienteAbordo.setEnabled(true);
                    servicioSocket.solicitudSeleccionada.setBotonAbordo(true);
                }
            }
        });
    }

    private void evaluarEstadoChat(int valor) {
        switch (valor) {
            case 0:
                imageButtonMensajes.setVisibility(View.GONE);
                break;
            default:
                imageButtonMensajes.setVisibility(View.VISIBLE);
                break;
        }
    }

    /**
     * Sirve para cambiar los iconos de abordo y pedido entregado del sistema
     *
     * @param isTipo true: solicitud
     *               false: pedidoCl
     */
    public void cambiarIconoPedidosSolicitud(boolean isTipo) {
        if (isTipo) {
            btnClienteAbordo.setBackgroundResource(R.drawable.btn_abordo);
        } else {
            if (solicitudSeleccionada.getT() == 1) {
                btnClienteAbordo.setBackgroundResource(R.drawable.btn_pedido);
            } else {
                btnClienteAbordo.setBackgroundResource(R.drawable.btn_encomienda);
            }
        }
    }

    @Override
    public void clienteAceptoTiempo(final boolean isAceptado, final Solicitud solSeleccionada,
                                    final String razon) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (solSeleccionada != null && solSeleccionada.getEstadoPedido() >= 3) {
                    solicitudSeleccionada = solSeleccionada;
                }
                if (solSeleccionada != null && solSeleccionada.getEstadoSolicitud() >= 5) {
                    solicitudSeleccionada = solSeleccionada;
                }
                if (solicitudSeleccionada != null) {
                    if (isAceptado) {
                        Log.e("estado13", "estado13");
                        cambiarEstadoOcupado();
                        neutralizarDialogos();
                        habilitarComponentesCarrera();
                        if (solicitudSeleccionada.isPedido()) {
                            imgBtnComprando.setVisibility(View.VISIBLE);
                            imgBtnComprado.setVisibility(View.VISIBLE);
                            cambiarIconoPedidosSolicitud(false);
                            imgBtnComprado.setEnabled(false);
                            if (solicitudSeleccionada.getT() == 1) {
                                imgBtnComprando.setImageResource(R.mipmap.ic_comprando_blanco);
                                imgBtnComprado.setImageResource(R.mipmap.ic_comprado_blanco);
                            } else {
                                imgBtnComprando.setImageResource(R.mipmap.ic_recojer_blanco);
                                imgBtnComprado.setImageResource(R.mipmap.ic_recogida_blanco);
                            }
                            btnAviso.setEnabled(false);
                        } else {
                            imgBtnComprando.setVisibility(View.GONE);
                            imgBtnComprado.setVisibility(View.GONE);
                            imgBtnComprado.setEnabled(false);
                        }
                        if (mBound && servicioSocket.estadoBoton <= 3) {
                            servicioSocket.estadoBotonDelTaxi(3);
                        }
                        agregarMarketCliente();
                        if (servicioSocket.solicitudSeleccionada == null) {
                            return;
                        }
                        if (servicioSocket.solicitudSeleccionada.isBotonAbordo()) {
                            btnClienteAbordo.setEnabled(true);
                        }
                        servicioSocket.apagarEscuchaPosibleSolicitud();
                        solicitudCallCenter(solicitudSeleccionada);
                        definirDireccionCliente(solicitudSeleccionada);
                        configurarTaximetroTemporal(solicitudSeleccionada);
                    }
                    solicitudSeleccionada = solSeleccionada;
                }
            }
        });
    }

    public void configurarTaximetroTemporal(Solicitud solicitudSeleccionada) {
        DatosTaximetroServicioActivo datosTaximetroServicioActivo = DatosTaximetroServicioActivo.obtenerDatosTaximetro(this);
        if (datosTaximetroServicioActivo == null && solicitudSeleccionada != null && solicitudSeleccionada.getIdServicioActivo() != 0 && solicitudSeleccionada.getIdServicioActivo() != -1) {
            mapaPresenter.getConfiguracionTaximetroServicioActivo(spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0), solicitudSeleccionada.getIdServicioActivo());
        }
    }

    public void reiniciarConfiguracionTaximetroTemporal() {
        DatosTaximetroServicioActivo.guardarDatosTaximetro(this, null);
    }


    public void definirDireccionCliente(Solicitud solicitudSeleccionada) {
        tvDireccionCliente.setText(solicitudSeleccionada.direccionSolicitudCompleta());
        if (utilidades.isVertical(this)) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(700, 0, 100, 0);
            lyEnCarrera.setLayoutParams(layoutParams);
        }
    }

    Handler handlerCancelarSolicitud;
    Runnable runnableCancelarSolicitud;
    private static final int TIEMPO_LIBERAR_CALIFICACION = 10000;

    @Override
    public void clienteCanceloSolicitud(final Solicitud solicitudSeleccionada,
                                        final String razonCancelacion, final boolean isCalificar) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                utilidades.customToastSolicitud(MapaBaseActivity.this, razonCancelacion, R.drawable.ic_error_toast, R.drawable.design_toast_personalizado);
                if (isCalificar) {
                    if (handlerCancelarSolicitud == null) {
                        handlerCancelarSolicitud = new Handler();
                        handlerCancelarSolicitud.postDelayed(runnableCancelarSolicitud = new Runnable() {
                            @Override
                            public void run() {
                                liberarAlCancelar();
                                if (dialogFinCarrera != null && dialogFinCarrera.isShowing())
                                    dialogFinCarrera.dismiss();
                                handlerCancelarSolicitud = null;
                            }
                        }, TIEMPO_LIBERAR_CALIFICACION);
                    }
                    presentar_info_fin_carrera();
                } else {
                    liberarAlCancelar();
                }
            }
        });
    }

    @Override
    public void clienteCanceloPedido(Solicitud solicitudSeleccionada, final String razonCancelacion, final boolean isCalificar) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                utilidades.customToastSolicitud(MapaBaseActivity.this, razonCancelacion, R.drawable.ic_error_toast, R.drawable.design_toast_personalizado);
                if (isCalificar) {
                    if (handlerCancelarSolicitud == null) {
                        handlerCancelarSolicitud = new Handler();
                        handlerCancelarSolicitud.postDelayed(runnableCancelarSolicitud = new Runnable() {
                            @Override
                            public void run() {
                                liberarAlCancelar();
                                if (dialogFinCarrera != null && dialogFinCarrera.isShowing())
                                    dialogFinCarrera.dismiss();
                                handlerCancelarSolicitud = null;
                            }
                        }, TIEMPO_LIBERAR_CALIFICACION);
                    }
                    presentar_info_fin_carrera();
                } else {
                    liberarAlCancelar();
                }
            }
        });
    }

    public void liberarAlCancelar() {
        Log.e("estado32", "estado32");
        cambiarEstadoLibre();
        cambiarIconoPedidosSolicitud(true);
        neutralizarDialogos();
        deshabilitarComponentesCarrera();
        borrarMarketCliente();
        imgBtnComprando.setVisibility(View.GONE);
        imgBtnComprado.setVisibility(View.GONE);
        btnClienteAbordo.setEnabled(false);
        if (mBound) {
            servicioSocket.setClienteAceptoTiempo(false);
            servicioSocket.isCronometroTiempo = false;
            servicioSocket.borrarPreferencia();
            servicioSocket.isEstadoAbordo = false;
            servicioSocket.isMensajeAbordo = false;
            servicioSocket.activaEnNuevaSolicitud();
            servicioSocket.setEnviarComprarPedido(false);
            servicioSocket.setEnviarllevandoPedido(false);
            servicioSocket.resetIdEnvioCalificacion();
            servicioSocket.borrarCarreraSeleccionada();
            servicioSocket.estadoSolicitud = 0;
            servicioSocket.estadoPedido = 0;
            servicioSocket.solicitudSeleccionada = null;
            servicioSocket.razonCancelada = -1;
        }
        procesoParaPresentarDialogo = null;
    }

    @Override
    public void clienteAbordo(final String mensaje, final Solicitud solicitudSelect) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                servicioSocket.estadoBotonDelTaxi(4);
                if (mBound && servicioSocket.solicitudSeleccionada != null) {
                    servicioSocket.solicitudSeleccionada.setBotonAbordo(true);
                }
                if (!servicioSocket.isMensajeAbordo) {
                    utilidades.customToastCorto(MapaBaseActivity.this, mensaje);
                    servicioSocket.isMensajeAbordo = true;
                }
                servicioSocket.reiniciarCronometro();
                btnClienteAbordo.setEnabled(true);
                Log.e("estado14", "estado14");
                cambiarEstadoOcupado();
                habilitarComponentesCarrera();
                neutralizarDialogos();
                solicitudSeleccionada = solicitudSelect;
                if (alertDialogEventualidades != null) {
                    if (alertDialogEventualidades.isShowing()) {
                        alertDialogEventualidades.dismiss();
                        alertDialogEventualidades = null;
                    }
                }
            }
        });
    }

    @Override
    public void cambiarOcupado() {
        Log.e("estado15", "estado15");
        runOnUiThread(() -> cambiarEstadoOcupado());
    }


    @Override
    public void clienteFinCarrera(final String mensaje, String calificacion,
                                  final Solicitud solicitud) {
        solicitudSeleccionada = solicitud;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                clienteAceptoTiempo(true, solicitud, null);
                if (solicitudSeleccionada != null) {
                    if (!isDialogoCancelar) {
                        isDialogoCancelar = true;
                        Builder dialogo = new Builder(MapaBaseActivity.this);
                        dialogo.setTitle(R.string.str_alerta_min);
                        if (solicitudSeleccionada.isPedido()) {
                            dialogo.setMessage("El cliente indica que el pedido ha sido entregado. ¿Es correcto?");
                        } else {
                            dialogo.setMessage("El cliente indica que la carrera ha sido completada. ¿Es correcto?");
                        }
                        dialogo.setCancelable(false);
                        dialogo.setPositiveButton(R.string.str_si, new OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                //borrarMarketCliente();
                                if (solicitudSeleccionada.isPedido()) {
                                    servicioSocket.enviarPedidoEntregado(solicitudSeleccionada);
                                    finCarreraGenerico(mensaje, false);
                                } else {
                                    servicioSocket.enviarAbordo(solicitudSeleccionada);
                                    finCarreraGenerico(mensaje, true);
                                    activarTaximetro();
                                    if (solicitudSeleccionada != null) {
                                        if (solicitudSeleccionada.getTipoFormaPago() == 4) {
                                            dialogoSaldoKtaxi = createSaldoKtaxiDialogo();
                                            if (!isFinishing()) {
                                                dialogoSaldoKtaxi.show();
                                            }
                                        }
                                    }
                                }
                                isDialogoCancelar = false;
                                dialogo1.dismiss();
                            }
                        });
                        dialogo.setNegativeButton(R.string.str_no, (dialogo1, id) -> {
                            dialogo1.dismiss();
                            isDialogoCancelar = false;
                            imageButtonMensajes.setVisibility(View.GONE);
                            lyEnCarrera.setVisibility(View.GONE);
                            btnClienteAbordo.setEnabled(true);
                        });
                        if (!isFinishing()) {
                            dialogo.show();
                        }

                    }
                }
            }
        });
    }

    @Override
    public void mensajesUsuario(ArrayList<ChatMessage> chatHistory) {
        if (solicitudSeleccionada != null) {
            if (servicioSocket.enCarrera()) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageButtonMensajes.setImageDrawable(setBadgeCount(1, imageButtonMensajes.getDrawable()));
                        MapaBaseActivity.this.startActivity(new Intent(MapaBaseActivity.this, ChatActivity.class));
                    }
                });
            } else {
                utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.sin_carrera));
            }
        }
    }

    @Override
    public void cambioInterenet(final boolean estado) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (estado) {
                    btnInternetOn.setVisibility(View.GONE);
                    btnInternetOff.setVisibility(View.VISIBLE);
                } else {
                    btnInternetOn.setVisibility(View.VISIBLE);
                    btnInternetOff.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void cambiarEstado(final boolean conectado) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!conectado) {
                    btnOffLine.setVisibility(View.VISIBLE);
                } else {
                    btnOffLine.setVisibility(View.GONE);
                    servicioSocket.consultarSolicitudesEntrantes();
                }
            }
        });
    }

    @Override
    public void alertasDialogos(int idSolicitud) {
        if (dialogoTiempo != null && solicitudSeleccionada != null) {
            if (solicitudSeleccionada.getIdSolicitud() == idSolicitud) {
                dialogoTiempo.dismiss();
                solicitudSeleccionada = null;
                dialogoTiempo = null;
            }
        }
    }

    @Override
    public void finCarreraGenerico(final String mensaje, boolean presentar) {
        servicioSocket.prenderEscuchaPosibleSolicitud();
        neutralizarDialogos();
        //borrarMarketCliente();
        if (solicitudSeleccionada != null) {
            if (solicitudSeleccionada.getIdPedido() != 0) {
                imgBtnComprando.setVisibility(View.GONE);
                imgBtnComprado.setVisibility(View.GONE);
                imgBtnComprando.setImageResource(R.mipmap.ic_comprando_blanco);
                imgBtnComprado.setImageResource(R.mipmap.ic_comprado_blanco);
                imgBtnComprando.setEnabled(true);
                imgBtnComprado.setEnabled(false);
            }
        }
        servicioSocket.reiniciarCronometro();
        cambiarIconoPedidosSolicitud(true);
        deshabilitarComponentesCarrera();
        btnClienteAbordo.setEnabled(false);
        servicioSocket.borrarCarreraSeleccionada();
        servicioSocket.isMensajeAbordo = false;
        utilidades.neutralizarDialogos(dialogoInformacionSolicitud);
        servicioSocket.estadoPedido = 0;
        servicioSocket.isEstadoAbordo = false;
        servicioSocket.borrarPreferencia();
        ibtnInformacionSolicitud.setVisibility(View.GONE);
        servicioSocket.setEnviarComprarPedido(false);
        servicioSocket.setEnviarllevandoPedido(false);
        isDialogoCancelar = false;
    }

    @Override
    public void mostrarTiempo(final String tiempo) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvCronometroTiempo.setText(tiempo);
            }
        });
    }

    @Override
    public void mensajeDesconeccion(final String mensaje) {
        mapaPresenter.obtenerCerrarSesion(spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), true);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                servicioSocket.apagaEscuchas();
                DatosTaximetro.guardarDatosTaximetro(MapaBaseActivity.this, null);
                borrarDatosDeSesion();
                servicioSocket.cerrarSesion();
                servicioSocket.stopForeground(true);
                Builder dialogo = new Builder(MapaBaseActivity.this);
                dialogo.setTitle(R.string.str_alerta_min);
                dialogo.setIcon(R.mipmap.ic_launcher);
                dialogo.setCancelable(false);
                dialogo.setMessage(mensaje);
                dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        dialogo1.dismiss();
                        finish();
                        startActivity(new Intent(MapaBaseActivity.this, IniciarSesionBaseActivity.class));
                    }
                }).show();
            }
        });


    }

    @Override
    public void cambiarAbordo() {
        if (locacionDefault != null) {
            if (locacionDefault.getLatitude() == 0 && locacionDefault.getLatitude() == 0 && locacionDefault.getAccuracy() < 200) {
                if (!btnClienteAbordo.isEnabled()) {
                    btnClienteAbordo.setEnabled(true);
                    servicioSocket.solicitudSeleccionada.setBotonAbordo(true);
                }
            }
        }

    }

    @Override
    public void nofificarBateriaBaja(final int bateria) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isFinishing()) {
                    Builder dialogo = new Builder(MapaBaseActivity.this);
                    dialogo.setTitle(R.string.str_alerta_min);
                    dialogo.setMessage("Su nivel de batería es " + bateria +
                            "%, para usar el aplicativo sin problemas " +
                            "conectese a una fuente de alimentación.");
                    dialogo.setCancelable(false);
                    dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                        public void onClick(DialogInterface dialogo1, int id) {
                            dialogo1.dismiss();
                        }
                    });
                    if (!isFinishing()) {
                        dialogo.show();
                    }

                }
            }
        });
    }

    @Override
    public void cobrarDineroElectronico(final JSONObject object) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    cobros(object.getString("celular"), object.getString("cedula"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void mensajeAlertaCancelar(final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialogoCancelar(mensaje, false);
                borrarMarketCliente();
                neutralizarDialogos();
                deshabilitarComponentesCarrera();
                utilidades.neutralizarDialogo(dialogoInformacionSolicitud);
                btnClienteAbordo.setEnabled(false);
                servicioSocket.borrarCarreraSeleccionada();
                imgBtnComprando.setVisibility(View.GONE);
                imgBtnComprado.setVisibility(View.GONE);
                servicioSocket.estadoSolicitud = 0;
                servicioSocket.estadoPedido = 0;
                servicioSocket.razonCancelada = -1;
                imgBtnComprando.setImageResource(R.mipmap.ic_comprando_blanco);
                imgBtnComprado.setImageResource(R.mipmap.ic_comprado_blanco);
                imgBtnComprando.setEnabled(true);
                servicioSocket.isEstadoAbordo = false;
                servicioSocket.isMensajeAbordo = false;
                cambiarIconoPedidosSolicitud(true);
                imgBtnComprado.setEnabled(false);
            }
        });
    }


    @Override
    public void cambiarEstadoComprando() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mBound) {
                    if (servicioSocket.solicitudSeleccionada != null) {
                        if (servicioSocket.solicitudSeleccionada.getT() == 1) {
                            imgBtnComprando.setEnabled(true);
                            imgBtnComprando.setImageResource(R.mipmap.ic_comprando_amarillo);
                        } else {
                            imgBtnComprando.setEnabled(true);
                            imgBtnComprando.setImageResource(R.mipmap.ic_recojer_rojo);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void cobroAceptado(final boolean isEstado, final String mensaje, final int estado) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (estado) {
                    case 1:
                        if (isEstado) {
                            if (progressDialog != null) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                    if (dialogCobro != null) {
                                        dialogCobro.dismiss();
                                    }
                                    Builder dialogo = new Builder(MapaBaseActivity.this);
                                    dialogo.setTitle(R.string.informacion);
                                    dialogo.setMessage("El pago se ha realizado con éxito");
                                    dialogo.setCancelable(false);
                                    dialogo.setPositiveButton("Ok", new OnClickListener() {
                                        public void onClick(DialogInterface dialogo1, int id) {
                                            dialogo1.dismiss();
                                            presentar_info_fin_carrera();
                                        }
                                    });
                                    dialogo.show();
                                }
                            }
                            //{¿
                        } else {
                            Builder dialogo = new Builder(MapaBaseActivity.this);
                            dialogo.setTitle(R.string.informacion);
                            dialogo.setMessage(mensaje);
                            dialogo.setCancelable(false);
                            dialogo.setPositiveButton("Salir", new OnClickListener() {
                                public void onClick(DialogInterface dialogo1, int id) {
                                    dialogo1.dismiss();
                                }
                            });
                            dialogo.show();

                            if (progressDialog != null) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                            }
                            if (dialogCobro != null) {
                                if (dialogCobro.isShowing()) {
                                    dialogCobro.dismiss();
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }

            }
        });
    }

    @Override
    public void cambiarEstadoComprado() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mBound) {
                    if (servicioSocket.solicitudSeleccionada != null) {
                        solicitudSeleccionada = servicioSocket.solicitudSeleccionada;
                    } else {
                        return;
                    }
                }
                if (solicitudSeleccionada.getT() == 1) {
                    imgBtnComprando.setImageResource(R.mipmap.ic_comprado_verde);
                    imgBtnComprado.setImageResource(R.mipmap.ic_comprado_amarillo);
                    btnClienteAbordo.setEnabled(true);
                } else {
                    imgBtnComprando.setImageResource(R.mipmap.ic_recojer_verde);
                    imgBtnComprado.setImageResource(R.mipmap.ic_recojer_rojo);
                    btnClienteAbordo.setEnabled(true);
                }
            }
        });
    }

    @Override
    public void cambiarEstadoComprandoUsuario() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mBound) {
                    if (servicioSocket.solicitudSeleccionada != null) {
                        solicitudSeleccionada = servicioSocket.solicitudSeleccionada;
                    } else {
                        return;
                    }
                }
                if (solicitudSeleccionada.getT() == 1) {
                    imgBtnComprando.setEnabled(false);
                    imgBtnComprando.setImageResource(R.mipmap.ic_comprado_verde);
                    servicioSocket.setEnviarComprarPedido(true);
                    imgBtnComprado.setEnabled(true);
                } else {
                    imgBtnComprando.setEnabled(false);
                    imgBtnComprando.setImageResource(R.mipmap.ic_recojer_verde);
                    servicioSocket.setEnviarComprarPedido(true);
                    imgBtnComprado.setEnabled(true);
                }
            }
        });
    }

    @Override
    public void cambiarEstadoCompradoUsuario() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mBound) {
                    if (servicioSocket.solicitudSeleccionada != null) {
                        solicitudSeleccionada = servicioSocket.solicitudSeleccionada;
                    } else {
                        return;
                    }
                }
                if (solicitudSeleccionada.getT() == 1) {
                    imgBtnComprando.setEnabled(false);
                    imgBtnComprado.setEnabled(false);
                    imgBtnComprando.setImageResource(R.mipmap.ic_comprado_verde);
                    imgBtnComprado.setImageResource(R.mipmap.ic_comprando_verde);
                    servicioSocket.setEnviarllevandoPedido(true);
                } else {
                    imgBtnComprando.setEnabled(false);
                    imgBtnComprado.setEnabled(false);
                    imgBtnComprando.setImageResource(R.mipmap.ic_recojer_verde);
                    imgBtnComprado.setImageResource(R.mipmap.ic_recogida_verde);
                    servicioSocket.setEnviarllevandoPedido(true);
                }
                btnAviso.setEnabled(true);

            }
        });
    }

    @Override
    public void respuestaDineroElectronico(final String mensaje, final int estado) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dialogCobro != null) {
                    if (dialogCobro.isShowing()) {
                        dialogCobro.dismiss();
                    }
                }
                switch (estado) {
                    case 1:
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                                Builder dialogo = new Builder(MapaBaseActivity.this);
                                dialogo.setTitle(R.string.informacion);
                                dialogo.setMessage(mensaje);
                                dialogo.setCancelable(false);
                                dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                                    public void onClick(DialogInterface dialogo1, int id) {
                                        dialogo1.dismiss();
                                        presentar_info_fin_carrera();
                                    }
                                });
                                dialogo.show();
                                servicioSocket.estadoCobro = 0;
                            }
                        }
                        break;
                    case 6:
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                                Builder dialogo = new Builder(MapaBaseActivity.this);
                                dialogo.setTitle("Informacion");
                                dialogo.setMessage(mensaje);
                                dialogo.setCancelable(false);
                                dialogo.setPositiveButton(R.string.str_si, new OnClickListener() {
                                    public void onClick(DialogInterface dialogo1, int id) {
                                        dialogo1.dismiss();
                                        presentar_info_fin_carrera();
                                    }
                                });
                                dialogo.setNegativeButton(R.string.str_no, new OnClickListener() {
                                    public void onClick(DialogInterface dialogo1, int id) {
                                        Builder dialogo = new Builder(MapaBaseActivity.this);
                                        dialogo.setTitle(R.string.str_alerta_min);
                                        dialogo.setMessage("Por favor cobre en efectivo");
                                        dialogo.setCancelable(false);
                                        dialogo.setPositiveButton("Ok", new OnClickListener() {
                                            public void onClick(DialogInterface dialogo1, int id) {
                                                dialogo1.dismiss();
                                                presentarValorCobro();
                                            }
                                        });
                                        dialogo.show();
                                    }
                                });
                                dialogo.show();
                                servicioSocket.estadoCobro = 0;
                            }
                        }
                        break;
                    case 7:
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                                Builder dialogo = new Builder(MapaBaseActivity.this);
                                dialogo.setTitle("Informacion");
                                dialogo.setMessage(mensaje);
                                dialogo.setCancelable(false);
                                dialogo.setPositiveButton("Si se débito", new OnClickListener() {
                                    public void onClick(DialogInterface dialogo1, int id) {
                                        if (mBound) {
                                            dialogo1.dismiss();
                                            presentar_info_fin_carrera();
                                            servicioSocket.verificarPago(costo);
                                        } else {
                                            utilidades.customToast(MapaBaseActivity.this, "Intente nuevamente.");
                                        }

                                    }
                                });
                                dialogo.setNegativeButton("No, cobrar en efectivo", new OnClickListener() {
                                    public void onClick(DialogInterface dialogo1, int id) {
                                        if (mBound) {
                                            dialogo1.dismiss();
                                            presentar_info_fin_carrera();
                                            servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajeros, solicitudSeleccionada);
                                            costo = 0;
                                            Log.e("estado33", "estado33");
                                            cambiarEstadoLibre();
                                        } else {
                                            utilidades.customToast(MapaBaseActivity.this, "Intente nuevamente.");
                                        }

                                    }
                                });
                                dialogo.show();
                                servicioSocket.estadoCobro = 0;
                            }
                        }
                        break;
                    default:
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                                Builder dialogo = new Builder(MapaBaseActivity.this);
                                dialogo.setTitle(R.string.informacion);
                                dialogo.setMessage(mensaje);
                                dialogo.setCancelable(false);
                                dialogo.setPositiveButton("Salir", new OnClickListener() {
                                    public void onClick(DialogInterface dialogo1, int id) {
                                        dialogo1.dismiss();
                                        presentarValorCobro();
                                    }
                                });
                                servicioSocket.estadoCobro = 0;
                                dialogo.show();
                            }
                        }
                        break;
                }
            }
        });
    }

    public void mensajePanico(final RespuestaPosibleSolicitudes respuestaPosibleSolicitudes) {
        if (dialogPanico != null) {
            if (dialogPanico.isShowing()) {
                return;
            }

        }
        Builder dialogo = new Builder(MapaBaseActivity.this);
        dialogo.setTitle("PANICO");
        dialogo.setMessage("Se reporta un pánico.\nUsuario: " + respuestaPosibleSolicitudes.getUsuario() + "\nEmpresa: " + respuestaPosibleSolicitudes.getEmpresa() + "\nNumero Unidad: " + respuestaPosibleSolicitudes.getNumeroUnidad() + "\nPor favor dirigirse al auxilio.");
        dialogo.setCancelable(false);
        dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                String mensaje = "Se reporta un pánico.\nUsuario: " + respuestaPosibleSolicitudes.getUsuario() + "\nEmpresa: " + respuestaPosibleSolicitudes.getEmpresa() + "\nNumero Unidad: " + respuestaPosibleSolicitudes.getNumeroUnidad() + "\nPor favor dirigirse al auxilio.";
                mapViewConfig.marketPanico(new LatLng(respuestaPosibleSolicitudes.getLt(), respuestaPosibleSolicitudes.getLg()), mensaje, mapaOSM, true);
                mapViewConfigMapBox.marketPanico(new LatLng(respuestaPosibleSolicitudes.getLt(), respuestaPosibleSolicitudes.getLg()), mensaje, mapBox, true);
                if (mBound) {
                    servicioSocket.actualizarRastreoPanico(respuestaPosibleSolicitudes.getI(), respuestaPosibleSolicitudes.getTp(), respuestaPosibleSolicitudes.getiV());
                    servicioSocket.apagarEscuchaPosibleSolicitud();
                }
                btnDejarRastrear.setVisibility(View.VISIBLE);
                imgBtnRastreoPanico.setVisibility(View.VISIBLE);
            }
        });
        dialogo.setNegativeButton("Ignorar", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialogPanico = dialogo.show();
    }

    @Override
    public void dejarRastrearPanico() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mapViewConfig.marketPanico(null, null, mapaOSM, false);
                mapViewConfigMapBox.marketPanico(null, null, mapBox, false);
                btnDejarRastrear.setVisibility(View.GONE);
                imgBtnRastreoPanico.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void actualizarRastreoPanico(final com.mapbox.mapboxsdk.geometry.LatLng latLng) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                respuestaPosibleSolicitudes.setLt(latLng.getLatitude());
                respuestaPosibleSolicitudes.setLg(latLng.getLongitude());
                mapViewConfig.marketPanico(new LatLng(latLng.getLatitude(), latLng.getLongitude()), "", mapaOSM, true);
                mapViewConfigMapBox.marketPanico(latLng, "", mapBox, true);
            }
        });
    }

    private RespuestaPosibleSolicitudes respuestaPosibleSolicitudes;

    @Override
    public void posiblesSolicitudes(
            final RespuestaPosibleSolicitudes respuestaPosibleSolicitudes) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                MapaBaseActivity.this.respuestaPosibleSolicitudes = respuestaPosibleSolicitudes;
                if (respuestaPosibleSolicitudes.getT() == 1) {
                    mensajePanico(respuestaPosibleSolicitudes);
                } else {
                    if (utilidades.isVerificarDialogo(dialogPosible)) {
                        Builder dialogo = new Builder(MapaBaseActivity.this);
                        dialogo.setTitle("Posible Solicitud");
                        dialogo.setMessage(respuestaPosibleSolicitudes.getM());
                        dialogo.setCancelable(false);
                        dialogo.setPositiveButton("Dirigirse", new OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                mapViewConfig.marketPosiblesSolcitudes(respuestaPosibleSolicitudes, "Se reporta posibles solicitudes recientemente.", mapaOSM,
                                        getResources().getDrawable(R.mipmap.ic_launcher_posibles_solicitudes), true);
                                mapViewConfigMapBox.marketPosiblesSolcitudes(respuestaPosibleSolicitudes, "Se reporta posibles solicitudes recientemente.", mapBox,
                                        R.mipmap.ic_launcher_posibles_solicitudes, true);
                                mapViewGoogle.marketPosiblesSolcitudes(respuestaPosibleSolicitudes, "Se reporta posibles solicitudes recientemente.", mMap,
                                        R.mipmap.ic_launcher_posibles_solicitudes, true);
                                if (mBound) {
                                    servicioSocket.apagarEscuchaPosibleSolicitud();
                                    servicioSocket.cronometroPosiblesSolicitudes(1, respuestaPosibleSolicitudes);
                                }

                            }
                        });
                        dialogo.setNegativeButton("Ignorar", new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        if (!isFinishing()) {
                            dialogPosible = dialogo.show();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void boletinKtaxi(final String boletin, final String asunto) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final Builder dialogo = new Builder(MapaBaseActivity.this);
                dialogo.setTitle(asunto);
                dialogo.setMessage(boletin);
                dialogo.setCancelable(false);
                dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        //codigo
                        if (itemNotificacion != null) {
                            itemNotificacion.setIcon(setBadgeCount(1, itemNotificacion.getIcon()));
                        }
                        dialogo1.dismiss();
                    }
                });
                dialogo.show();
                if (itemNotificacion != null) {
                    itemNotificacion.setIcon(setBadgeCount(1, itemNotificacion.getIcon()));
                }
            }
        });
    }

    @Override
    public void borraCarreraAlCancelar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mBound) {
                    borrarMarketCliente();
                    neutralizarDialogos();
                    deshabilitarComponentesCarrera();
                    solicitudSeleccionada = null;
                    utilidades.neutralizarDialogo(dialogoInformacionSolicitud);
                    btnClienteAbordo.setEnabled(false);
                    servicioSocket.borrarCarreraSeleccionada();
                    imgBtnComprando.setVisibility(View.GONE);
                    imgBtnComprado.setVisibility(View.GONE);
                    servicioSocket.estadoSolicitud = 0;
                    servicioSocket.estadoPedido = 0;
                    servicioSocket.solicitudSeleccionada = null;
                    servicioSocket.razonCancelada = -1;
                    imgBtnComprando.setImageResource(R.mipmap.ic_comprando_blanco);
                    imgBtnComprado.setImageResource(R.mipmap.ic_comprado_blanco);
                    imgBtnComprando.setEnabled(true);
                    servicioSocket.isEstadoAbordo = false;
                    servicioSocket.isMensajeAbordo = false;
                    cambiarIconoPedidosSolicitud(true);
                    imgBtnComprado.setEnabled(false);
                    Log.e("estado34", "estado34");
                    cambiarEstadoLibre();
                    servicioSocket.resetIdEnvioCalificacion();
                    borrarRutaEnDesuso();
                }

            }
        });
    }

    public void puntoReferencia() {
        Location locationEs;
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(MapaBaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapaBaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            assert mlocManager != null;
            locationEs = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationEs == null) {
                locationEs = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if (locationEs == null) {
                locationEs = mlocManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            }

            if (locationEs != null) {
                agregarMarcadorBoxTaxista(locationEs);
                locacionDefault = locationEs;
            }
        }


    }

    @Override
    public void borrarRutaEnDesuso() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                servicioSocket.borrarRuta(mapViewBox);
            }
        });
    }

    @Override
    public void onResponse(List<PositionTrafico> positionList) {

    }

    @Override
    public void calificacionCliente(ValoracionConductor valoracionConductor) {
        if (valoracionConductor != null) {
            if (valoracionConductor.getEstado() != 0) {
                switch (valoracionConductor.getEstado()) {
                    case 1:
                        Hashtable<Integer, Integer> contenedor = new Hashtable();
                        contenedor.put(0, 5);
                        contenedor.put(1, 5);
                        contenedor.put(2, 4);
                        contenedor.put(3, 3);
                        contenedor.put(4, 2);
                        contenedor.put(5, 1);
                        if (ratingBar != null) {
                            ratingBar.setRating(contenedor.get(valoracionConductor.getValoracion()));
                        }
                        break;
                }
            }
        }
    }

    @Override
    public void errorCalificacionCliente(String valor) {
    }

    @Override
    public void onLocation(final Location location) {
        if (location != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    locacionDefault = location;
                    modoGpsRastreo(isRastreo, locacionDefault);
                    if (location.getAccuracy() > 100.0f) {
                        btnGpsDisable.setVisibility(View.VISIBLE);
                        btnGpsOn.setVisibility(View.GONE);
                        btnGpsOff.setVisibility(View.GONE);
                    } else {
                        btnGpsOn.setVisibility(View.VISIBLE);
                        btnGpsOff.setVisibility(View.GONE);
                        btnGpsDisable.setVisibility(View.GONE);
                    }
                    if (mBound) {
                        if (servicioSocket.solicitudSeleccionada != null && !servicioSocket.solicitudSeleccionada.isBotonAbordo()) {
                            if (utilidades.calculationByDistance(new LatLng(location.getLatitude(), location.getLongitude()),
                                    new LatLng(servicioSocket.solicitudSeleccionada.getLatitud(),
                                            servicioSocket.solicitudSeleccionada.getLongitud())) <= 0.3) {
                                if (servicioSocket.solicitudSeleccionada.isPedido()) {
                                    if (servicioSocket.isEnviarllevandoPedido()) {
                                        btnClienteAbordo.setEnabled(true);
                                        servicioSocket.solicitudSeleccionada.setBotonAbordo(true);
                                        servicioSocket.guardarEstadoSolicitud();
                                        servicioSocket.enviarEventoConductor(servicioSocket.getIdSolicitudSeleccionada(), 1, locacionDefault);
                                    }
                                } else {
                                    btnClienteAbordo.setEnabled(true);
                                    servicioSocket.solicitudSeleccionada.setBotonAbordo(true);
                                    servicioSocket.guardarEstadoSolicitud();
                                    comprobarMostrarDialogoAbordo();
                                    servicioSocket.enviarEventoConductor(servicioSocket.getIdSolicitudSeleccionada(), 1, locacionDefault);
                                }
                            }
                        }

                    }
                }
            });
        } else {
            puntoReferencia();
        }
    }

    @Override
    public void errorCerrarSesion(String valor) {
    }

    @Override
    public void error(String error) {

    }

    @Override
    public void cambioClave(DatosLogin.CambioClave cambioClave) {
        if (pDialog != null) {
            pDialog.dismiss();
        }
        if (cambioClave.getEstado() == 1) {
            utilidades.customToastCorto(MapaBaseActivity.this, cambioClave.getMensaje());
            utilidades.neutralizarDialogo(dialogoCambioClave);
        } else {
            utilidades.customToastCorto(MapaBaseActivity.this, cambioClave.getMensaje());
        }
    }

    @Override
    public void errorCambioClave(String error) {
        if (pDialog != null) {
            pDialog.dismiss();
        }
        utilidades.customToastCorto(MapaBaseActivity.this, error);
    }

    @Override
    public void configuracionServidorNew(boolean isEstado) {
        if (imagenGlobal == null && countDescargaImagen < 3) {
            SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
            configuracionServidor = ConfiguracionServidor.createConfiguracionServidor(spConfigServerNew.getString(VariablesGlobales.CONFIG_IP_NEW, VariablesGlobales.CONFIGURACION_INICIAL));
            String url = configuracionServidor.getDns().getImgC() + spLogin.getString(getString(R.string.str_imagen_conductor), getString(R.string.str_sin_imagen_conductor));
            cargarImagen(imageViewTaxista, url, this);
            countDescargaImagen++;
            if (configuracionServidor != null) {
                estadoActualizacionApp(configuracionServidor);
            }
        }
    }

    public void estadoActualizacionApp(ConfiguracionServidor configuracionServidor) {
        if (configuracionServidor.getEn() == 2) {
            switch (configuracionServidor.getDns().getEn()) {
                case 1:
                    if (!isActualizarNoPrioritaria) {
                        Builder builderOp = new Builder(MapaBaseActivity.this);
                        builderOp.setCancelable(true);
                        builderOp.setIcon(R.mipmap.ic_launcher);
                        builderOp.setMessage(configuracionServidor.getDns().getM())
                                .setTitle(R.string.str_alerta_min)
                                .setPositiveButton("Actualizar", new OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        Log.e("estado16", "estado16");
                                        cambiarEstadoOcupado();
                                        utilidades.calificarApp(MapaBaseActivity.this);
                                    }
                                })
                                .setNegativeButton("Mas tarde", new OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                }).show();
                        isActualizarNoPrioritaria = true;
                    }
                    break;
                case 2:
                    if (solicitudSeleccionada == null) {
                        Log.e("estado17", "estado17");
                        cambiarEstadoOcupado();
                        Builder builder = new Builder(MapaBaseActivity.this);
                        builder.setCancelable(false);
                        builder.setIcon(R.mipmap.ic_launcher);
                        builder.setMessage(configuracionServidor.getDns().getM())
                                .setTitle(R.string.str_alerta_min)
                                .setPositiveButton("Actualizar", new OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        utilidades.calificarApp(MapaBaseActivity.this);
                                        servicioSocket.salirSistema();
                                        Intent intent = new Intent(MapaBaseActivity.this, ServicioSockets.class);
                                        if (mConnection != null) {
                                            unbindService(mConnection);
                                            mConnection = null;
                                        }
                                        stopService(intent);
                                        finish();
                                    }
                                }).show();
                    }
                    break;
            }
        }
    }

    @Override
    public void configuracionServidorHelp(final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Builder builder = new Builder(MapaBaseActivity.this);
                builder.setMessage(mensaje)
                        .setTitle(R.string.str_alerta_min)
                        .setPositiveButton("Actualizar", new OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                utilidades.calificarApp(MapaBaseActivity.this);
                            }
                        }).show();
            }
        });

    }

    private void cargarImagen(final ImageView imageView, String url, Context context) {
        DisplayImageOptions options = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.RGB_565).cacheOnDisk(true).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        imageLoader.displayImage(url, imageView, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                imagenGlobal = loadedImage;
                imageView.setImageBitmap(loadedImage);
            }
        });
    }

    public void descargarImagen(final ImageView imageView, String url, Context context) {
        DisplayImageOptions options = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.RGB_565).cacheOnDisk(false).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        imageLoader.displayImage(url, imageView, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                imgBtnFeria.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void boletinNuevo(final DatosNotificacion datosNotificacion) {
        if (datosNotificacion != null) {
            if (datosNotificacion.getEstado() == 1) {
                Log.e("estado18", "estado18");
                cambiarEstadoOcupado();
                if (datosNotificacion.getT() == 1) {
                    Intent intent = new Intent(this, DetalleBoletin.class);
                    intent.putExtra("datosNotificacion", datosNotificacion);
                    startActivity(intent);
                } else {
                    dialogRespuestaPreguntas = createPreguntasDialogo(datosNotificacion);
                    dialogRespuestaPreguntas.show();
                }
            } else if (datosNotificacion.getEstado() == 2) {
                if (datosNotificacion.isPublicidad()) {
                    tituloPublicidad = datosNotificacion.getTituloPublicidad();
                    recursoFeria = datosNotificacion.getLinkPublicidad();
                    descargarImagen(imgBtnFeria, datosNotificacion.getImgPublicidad(), this);
                }
            }
        }
    }

    @Override
    public void respuestaResponderBoletin(String dato) {
        if (dialogRespuestaPreguntas != null) {
            if (dialogRespuestaPreguntas.isShowing()) {
                dialogRespuestaPreguntas.dismiss();
            }
        }
    }

    /**
     * Crea un diálogo con personalizado
     *
     * @return Diálogo
     */
    public AlertDialog createPreguntasDialogo(final DatosNotificacion datosNotificacion) {
        Builder builder = new Builder(MapaBaseActivity.this);
        LayoutInflater inflater = MapaBaseActivity.this.getLayoutInflater();
        View v = inflater.inflate(R.layout.boletin_respuesta, null);
        TextView tvAsunto = v.findViewById(R.id.tv_asunto);
        TextView tvBoletin = v.findViewById(R.id.tv_boletin);
        TextView tvVerDetalle = v.findViewById(R.id.tv_ver_detalle);
        LinearLayout lyCheckPreguntas = v.findViewById(R.id.ly_check_preguntas);
        final EditText etRepuestaPersonalizada = v.findViewById(R.id.et_repuesta_personalizada);
        deshabilitarCopyPage(etRepuestaPersonalizada);
        if (datosNotificacion.getT() == 2) {
            etRepuestaPersonalizada.setVisibility(View.GONE);
        }

        Button btnEnviarRespuesta = v.findViewById(R.id.btn_enviar_respuesta);
        tvAsunto.setText(datosNotificacion.getAsunto());
        tvBoletin.setText(datosNotificacion.getBoletin());
        final List<CheckBox> checkTodos = new ArrayList<>();
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int idSelecionado = v.getId();
                preguntaSeleccionada = datosNotificacion.getLP().get((idSelecionado - 1000));
                for (CheckBox checkBox : checkTodos) {
                    if (checkBox.getId() == idSelecionado) {
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                }
            }
        };
        int chId = 1000;
        if (datosNotificacion.getLP() != null) {
            for (DatosNotificacion.LP lp : datosNotificacion.getLP()) {
                CheckBox opcion = new CheckBox(MapaBaseActivity.this);
                opcion.setId(chId++);
                opcion.setOnClickListener(clickListener);
                opcion.setText(lp.getPregunta());
                opcion.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                lyCheckPreguntas.addView(opcion);
                checkTodos.add(opcion);
            }
        } else {
            lyCheckPreguntas.setVisibility(View.GONE);
        }

        btnEnviarRespuesta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int preguntaSelec = -1;
                String pregunta = "-1";
                switch (datosNotificacion.getT()) {
                    case 2:
                        if (preguntaSeleccionada != null) {
                            mapaPresenter.responderBoletin(spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), datosNotificacion.getIdBoletin(), preguntaSeleccionada.getId(), "-1");
                        } else {
                            Toast.makeText(MapaBaseActivity.this, "Por favor seleccione una respuesta a la pregunta para continuar.", Toast.LENGTH_LONG).show();
                        }
                        break;
                    case 3:

                        if (preguntaSeleccionada != null) {
                            preguntaSelec = preguntaSeleccionada.getId();
                        }
                        if (!etRepuestaPersonalizada.getText().toString().isEmpty()) {
                            pregunta = etRepuestaPersonalizada.getText().toString();
                        }
                        mapaPresenter.responderBoletin(
                                spLogin.getInt(VariablesGlobales.ID_USUARIO, 0),
                                datosNotificacion.getIdBoletin(),
                                preguntaSelec, pregunta);
                        break;
                    case 4:
                        if (preguntaSeleccionada != null) {
                            preguntaSelec = preguntaSeleccionada.getId();
                        }
                        if (!etRepuestaPersonalizada.getText().toString().isEmpty()) {
                            pregunta = etRepuestaPersonalizada.getText().toString();
                        }
                        if (datosNotificacion.getLP() != null && datosNotificacion.getLP().size() > 0) {
                            if (preguntaSelec == -1) {
                                Toast.makeText(MapaBaseActivity.this, "Seleccione una respuesta de la lista.", Toast.LENGTH_LONG).show();
                                return;
                            }
                            if (pregunta.equals("-1")) {
                                etRepuestaPersonalizada.requestFocus();
                                Toast.makeText(MapaBaseActivity.this, "Escriba una respuesta a la pregunta.", Toast.LENGTH_LONG).show();
                                return;
                            }
                            mapaPresenter.responderBoletin(
                                    spLogin.getInt(VariablesGlobales.ID_USUARIO, 0),
                                    datosNotificacion.getIdBoletin(),
                                    preguntaSelec, pregunta);
                        } else {
                            if (pregunta.equals("-1")) {
                                etRepuestaPersonalizada.requestFocus();
                                Toast.makeText(MapaBaseActivity.this, "Escriba una respuesta a la pregunta.", Toast.LENGTH_LONG).show();
                                return;
                            }
                            mapaPresenter.responderBoletin(
                                    spLogin.getInt(VariablesGlobales.ID_USUARIO, 0),
                                    datosNotificacion.getIdBoletin(),
                                    preguntaSelec, pregunta);
                        }

                        break;
                }
            }
        });
        if (datosNotificacion.getUrl() != null && !datosNotificacion.getUrl().isEmpty()) {
            subrayarTextoConAcciones(datosNotificacion.getVinculo(), tvVerDetalle);
            tvVerDetalle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Uri uri = Uri.parse(datosNotificacion.getUrl());
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        e.getMessage();
                    }
                }
            });
        } else {
            tvVerDetalle.setVisibility(View.GONE);
        }
        builder.setCancelable(false);
        builder.setView(v);
        return builder.create();
    }

    public void deshabilitarCopyPage(EditText textField) {
        textField.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode actionMode, MenuItem item) {
                return false;
            }

            public void onDestroyActionMode(ActionMode actionMode) {
            }
        });

        textField.setLongClickable(false);
        textField.setTextIsSelectable(false);
    }

    public void subrayarTextoConAcciones(String text, TextView tvHide) {
        SpannableString spanString = new SpannableString(text);
        spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
        tvHide.setText(spanString);
    }

    @Override
    public void cambioIconoPosibleSolicitude(final int numEstado,
                                             final RespuestaPosibleSolicitudes respuestaPosibleSolicitudes) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (respuestaPosibleSolicitudes != null) {
                    switch (numEstado) {
                        case 2:
                            mapViewConfig.marketPosiblesSolcitudes(respuestaPosibleSolicitudes,
                                    formatearDataPosibleSolicitud(respuestaPosibleSolicitudes.getT2()),
                                    mapaOSM, getResources().getDrawable(R.mipmap.ic_launcher_posibles_solicitudes_dos), true);
                            mapViewConfigMapBox.marketPosiblesSolcitudes(respuestaPosibleSolicitudes,
                                    formatearDataPosibleSolicitud(respuestaPosibleSolicitudes.getT2()),
                                    mapBox, R.mipmap.ic_launcher_posibles_solicitudes_dos, true);
                            mapViewGoogle.marketPosiblesSolcitudes(respuestaPosibleSolicitudes,
                                    formatearDataPosibleSolicitud(respuestaPosibleSolicitudes.getT2()),
                                    mMap, R.mipmap.ic_launcher_posibles_solicitudes_dos, true);
                            break;
                        case 3:
                            mapViewConfig.marketPosiblesSolcitudes(respuestaPosibleSolicitudes,
                                    formatearDataPosibleSolicitud(respuestaPosibleSolicitudes.getT3()),
                                    mapaOSM, getResources().getDrawable(R.mipmap.ic_launcher_posibles_solicitudes_tres), true);
                            mapViewConfigMapBox.marketPosiblesSolcitudes(respuestaPosibleSolicitudes,
                                    formatearDataPosibleSolicitud(respuestaPosibleSolicitudes.getT3()),
                                    mapBox, R.mipmap.ic_launcher_posibles_solicitudes_tres, true);
                            mapViewGoogle.marketPosiblesSolcitudes(respuestaPosibleSolicitudes,
                                    formatearDataPosibleSolicitud(respuestaPosibleSolicitudes.getT3()),
                                    mMap, R.mipmap.ic_launcher_posibles_solicitudes_tres, true);
                            break;
                        case 4:
                            mapViewConfig.marketPosiblesSolcitudes(null, null,
                                    mapaOSM, null, false);
                            mapViewConfigMapBox.marketPosiblesSolcitudes(null, null,
                                    mapBox, 0, false);
                            mapViewGoogle.marketPosiblesSolcitudes(null, null,
                                    mMap, 0, false);
                            break;
                    }
                } else {
                    mapViewConfig.marketPosiblesSolcitudes(null, null,
                            mapaOSM, null, false);
                    mapViewConfigMapBox.marketPosiblesSolcitudes(null, null,
                            mapBox, 0, false);
                    mapViewGoogle.marketPosiblesSolcitudes(null, null,
                            mMap, 0, false);
                }
            }
        });
    }

    public String formatearDataPosibleSolicitud(int times) {
        int min = ((times / 60000));
        int seg = ((times % 60000) / 1000);
        String strMin;
        String strSeg;
        if (min < 10) {
            strMin = "0" + min;
        } else {
            strMin = min + "";
        }
        if (seg < 10) {
            strSeg = "0" + seg;
        } else {
            strSeg = seg + "";
        }
        return "Tiempo: " + strMin + ":" + strSeg;
    }

    /**
     * Cuando hat algun cambio en a pantalla permite quitar los dialogos mostrados
     */
    public void neutralizarDialogos() {
        banderaCerrarSecion = false;
        try {
            if (dialogoEspera != null) {
                if (dialogoEspera.isShowing()) {
                    dialogoEspera.dismiss();
                    dialogoEspera = null;
                }
            }
            if (dialogoTiempo != null) {
                if (dialogoTiempo.isShowing()) {
                    dialogoTiempo.dismiss();
                    dialogoTiempo = null;
                }
            }
            if (pDialog != null) {
                if (pDialog.isShowing()) {
                    pDialog.dismiss();
                    pDialog = null;
                }
            }
            if (alertDialogEventualidades != null) {
                if (alertDialogEventualidades.isShowing()) {
                    alertDialogEventualidades.dismiss();
                    alertDialogEventualidades = null;

                }
            }
            if (mostrarDialogoCarrera != null) {
                if (mostrarDialogoCarrera.isShowing()) {
                    mostrarDialogoCarrera.dismiss();
                    mostrarDialogoCarrera = null;
                }
            }
        } catch (IllegalArgumentException ingnore) {
        }
    }

    /**
     * Permite habilitar los componentes una vez que la carrera a iniciado
     */
    public void habilitarComponentesCarrera() {
        if (solicitudSeleccionada != null) {
            ibtnInformacionSolicitud.setVisibility(View.VISIBLE);
            lyEnCarrera.setVisibility(View.VISIBLE);
            btnLlamarCliente.setEnabled(true);
            if (!solicitudSeleccionada.isPedido()) {
                btnAviso.setEnabled(true);
            }
            evaluarEstadoChat(solicitudSeleccionada.getEstadoChat());
            btnWaze.setVisibility(View.VISIBLE);
            btnNumSolicitudes.setEnabled(false);
            numeroSolicitudes.setEnabled(false);
        }
    }

    /**
     * Permite desabilitar los componentes una vez que la carrera a finalizado
     */
    public void deshabilitarComponentesCarrera() {
        ibtnInformacionSolicitud.setVisibility(View.GONE);
        imageButtonMensajes.setVisibility(View.GONE);
        lyEnCarrera.setVisibility(View.GONE);
        btnWaze.setVisibility(View.GONE);
        btnLlamarCliente.setEnabled(false);
        btnAviso.setEnabled(false);
        btnNumSolicitudes.setEnabled(true);
        numeroSolicitudes.setEnabled(true);
    }

    /**
     * Cuando se cierra sesión permite borrar los datos guardados del usuario
     */
    public void borrarDatosDeSesion() {
        SharedPreferences.Editor localEditor = spLogin.edit();
        localEditor.putString("usuario", "");
        localEditor.putString("contrasenia", "");
        localEditor.putInt(VariablesGlobales.ID_USUARIO, 0);
        localEditor.putInt(VariablesGlobales.ID_VEHICULO, 0);
        localEditor.putInt("idEquipo", 0);
        localEditor.putString("nombres", "");
        localEditor.putString("apellidos", "");
        localEditor.putString("correo", "");
        localEditor.putString("celular", "");
        localEditor.putString("empresa", "");
        localEditor.putString(VariablesGlobales.PLACA_VEHICULO, "");
        localEditor.putString("regMunVehiculo", "");
        localEditor.putString("marcaVehiculo", "");
        localEditor.putString("modeloVehiculo", "");
        localEditor.putInt(VariablesGlobales.ID_CIUDAD, 0);
        localEditor.putInt("idEmpresa", 0);
        localEditor.putInt("anioVehiculo", 0);
        localEditor.putInt("unidadVehiculo", 0);
        localEditor.putString("imagenConductor", "");
        localEditor.putBoolean("logeado", false);
        localEditor.apply();

    }

    @Override
    public void cerrarSesion(DatosLogin.CerrarSesion cerrarSesion) {
        if (cerrarSesion != null) {
            if (cerrarSesion.getEstado() == 1) {
                servicioSocket.apagaEscuchas();
                DatosTaximetro.guardarDatosTaximetro(MapaBaseActivity.this, null);
                borrarDatosDeSesion();
                servicioSocket.cerrarSesion();
                servicioSocket.stopForeground(true);
                utilidades.customToast(this, cerrarSesion.getMensaje());
                finish();
                startActivity(new Intent(MapaBaseActivity.this, IniciarSesionBaseActivity.class));
            }
        } else {
            utilidades.mostrarMensajeCorto(MapaBaseActivity.this, getString(R.string.no_cierra_sesion));
        }
    }

    public AlertDialog createSaldoKtaxiDialogo() {
        Builder builder = new Builder(MapaBaseActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        @SuppressLint("InflateParams") View v = inflater.inflate(R.layout.dialogo_dinero_ktaxi, null);
        final Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        final TextView tvDetalle = v.findViewById(R.id.tv_ver_detalle);
        subrayarTextoConAcciones("VER TUTORIAL", tvDetalle);
        tvDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri uri = Uri.parse("http://bit.ly/SaldoKTaxi");
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch (ActivityNotFoundException ignore) {
                }
            }
        });
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogoSaldoKtaxi != null) {
                    if (dialogoSaldoKtaxi.isShowing()) {
                        dialogoSaldoKtaxi.dismiss();
                    }
                }
            }
        });

        btnAceptar.setEnabled(false);
        new Handler(Looper.myLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                btnAceptar.setEnabled(true);
                if (servicioSocket != null) {
                    servicioSocket.reproducirTextAudio.speak("Estimado conductor la carrera fue realizada con saldo k taxi, favor realizar el proceso de finalizado correctamente, para el cobro use siempre el valor que marca el taxímetro de su vehículo, la diferencia se deberá cobrar al cliente en efectivo, el costo del saldo ktaxi sera devuelto al momento de pagar la mensualidad.");
                }
            }
        }, 5000);
        builder.setCancelable(false);
        builder.setView(v);
        return builder.create();
    }

    public void comprobarMostrarDialogoAbordo() {
        if (servicioSocket.getMensajeDeuda() != null &&
                servicioSocket.getMensajeDeuda().getL() != null &&
                servicioSocket.getMensajeDeuda().getL().getP() != null) {
            if (servicioSocket.getMensajeDeuda().getL().getP().getRA() == 1) {
                servicioSocket.reproducirTextAudio.speak(servicioSocket.getMensajeDeuda().getL().getP().getMs());
            }
            switch (servicioSocket.getMensajeDeuda().getL().getP().getNA()) {
                case 2:
                    comprobarDialogoCobro();
                    servicioSocket.setMensajeDeudaAbordo(true);
                    dialogoMensajeCobro = createDialogoMensaje(servicioSocket.getMensajeDeuda().getL().getP().getMs(), true, false, dialogCobro, false);
                    dialogoMensajeCobro.show();
                    break;
                case 3:
                    comprobarDialogoCobro();
                    dialogoMensajeCobro = createDialogoMensaje(servicioSocket.getMensajeDeuda().getL().getP().getMs(), true, false, dialogCobro, false);
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (dialogoMensajeCobro != null) {
                                if (dialogoMensajeCobro.isShowing()) {
                                    try {
                                        dialogoMensajeCobro.dismiss();
                                    } catch (IllegalArgumentException e) {
                                        e.getMessage();
                                    }

                                }
                            }
                        }
                    }, servicioSocket.getMensajeDeuda().getL().getP().getTs() * 1000);
                    dialogoMensajeCobro.show();
                    break;
                default:
                    servicioSocket.setMensajeDeudaAbordo(null);
                    break;
            }

        }
    }

    public void comprobarDialogoCobro() {
        if (dialogoMensajeCobro != null) {
            if (dialogoMensajeCobro.isShowing()) return;
        }
    }

    /**
     * Muestra un dialogo para verificar que efectivamente el cliente se encuentra abordo
     */
    public void dialogoAbordo() {
        try {
            if (utilidades != null && utilidades.isVerificarDialogo(dialogoAbordo)) {
                if (solicitudSeleccionada != null) {
                    if (solicitudSeleccionada.getIdPedido() != 0) {
                        if (utilidades.calculationByDistance(new LatLng(locacionDefault.getLatitude(), locacionDefault.getLongitude()),
                                new LatLng(servicioSocket.solicitudSeleccionada.getLatitud(),
                                        servicioSocket.solicitudSeleccionada.getLongitud())) <= 0.1) {
                            servicioSocket.enviarPedidoEntregado(solicitudSeleccionada);
                            if (solicitudSeleccionada != null) {
                                if (solicitudSeleccionada.getIdPedido() != 0) {
                                    imgBtnComprando.setVisibility(View.GONE);
                                    imgBtnComprado.setVisibility(View.GONE);
                                    imgBtnComprando.setImageResource(R.mipmap.ic_comprando_blanco);
                                    imgBtnComprado.setImageResource(R.mipmap.ic_comprado_blanco);
                                    imgBtnComprando.setEnabled(true);
                                    imgBtnComprado.setEnabled(false);
                                    servicioSocket.setEnviarComprarPedido(false);
                                    servicioSocket.setEnviarllevandoPedido(false);
                                }
                            }
                            servicioSocket.setCobroEfectivo(1, "", solicitudSeleccionada);
                            servicioSocket.setCobroTarjetaCredito(1, "", solicitudSeleccionada);
                            cambiarIconoPedidosSolicitud(true);
                            btnClienteAbordo.setEnabled(false);

                            Log.e("valorSolicitud", "bien3");

                            mapViewGoogle.trazarRutaSolicitud(mMap, null, false);
                            mapViewConfigMapBox.borrarTrazadoRuta();
                            mapViewConfig.trazarRutaSolicitud(mapaOSM, null, false);

                            deshabilitarComponentesCarrera();
                            if (servicioSocket.isEstadoAbordo) {
                                servicioSocket.estadoBotonDelTaxi(6);
                            } else {
                                servicioSocket.estadoBotonDelTaxi(5);
                            }
                            servicioSocket.estadoSolicitud = 0;
                            servicioSocket.borrarCarreraSeleccionada();
                            lyEnCarrera.setVisibility(View.GONE);
                            imageButtonMensajes.setVisibility(View.GONE);
                            btnWaze.setVisibility(View.GONE);
                            servicioSocket.isEstadoAbordo = false;
                            servicioSocket.isMensajeAbordo = false;
                            servicioSocket.borrarPreferencia();
                        } else {
                            Builder alertDialog = new Builder(MapaBaseActivity.this);
                            alertDialog.setCancelable(true);
                            alertDialog.setIcon(R.mipmap.ic_launcher);
                            alertDialog.setTitle(getString(R.string.informacion));
                            alertDialog.setMessage(getString(R.string.cliente_abordo_pedido));
                            alertDialog.setNegativeButton("Sí",
                                    new OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            servicioSocket.setCobroEfectivo(1, "", solicitudSeleccionada);
                                            servicioSocket.setCobroTarjetaCredito(1, "", solicitudSeleccionada);
                                            servicioSocket.enviarPedidoEntregado(solicitudSeleccionada);
                                            if (solicitudSeleccionada != null) {
                                                if (solicitudSeleccionada.getIdPedido() != 0) {
                                                    imgBtnComprando.setVisibility(View.GONE);
                                                    imgBtnComprado.setVisibility(View.GONE);
                                                    imgBtnComprando.setImageResource(R.mipmap.ic_comprando_blanco);
                                                    imgBtnComprado.setImageResource(R.mipmap.ic_comprado_blanco);
                                                    imgBtnComprando.setEnabled(true);
                                                    imgBtnComprado.setEnabled(false);
                                                    servicioSocket.setEnviarComprarPedido(false);
                                                    servicioSocket.setEnviarllevandoPedido(false);

                                                }
                                            }
                                            cambiarIconoPedidosSolicitud(true);
                                            btnClienteAbordo.setEnabled(false);

                                            Log.e("valorSolicitud", "bien2");
                                            borrarMarketCliente();


                                            deshabilitarComponentesCarrera();
                                            if (servicioSocket.isEstadoAbordo) {
                                                servicioSocket.estadoBotonDelTaxi(6);
                                            } else {
                                                servicioSocket.estadoBotonDelTaxi(5);
                                            }
                                            servicioSocket.estadoSolicitud = 0;
                                            servicioSocket.borrarCarreraSeleccionada();
                                            lyEnCarrera.setVisibility(View.GONE);
                                            imageButtonMensajes.setVisibility(View.GONE);
                                            btnWaze.setVisibility(View.GONE);
                                            servicioSocket.isEstadoAbordo = false;
                                            servicioSocket.isMensajeAbordo = false;
                                            servicioSocket.borrarPreferencia();
                                        }
                                    });

                            alertDialog.setPositiveButton(R.string.str_no,
                                    new OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            btnClienteAbordo.setEnabled(true);
                                        }
                                    });
                            dialogoAbordo = alertDialog.show();
                        }
                    } else {
                        if (utilidades.calculationByDistance(new LatLng(locacionDefault.getLatitude(), locacionDefault.getLongitude()),
                                new LatLng(servicioSocket.solicitudSeleccionada.getLatitud(),
                                        servicioSocket.solicitudSeleccionada.getLongitud())) <= 0.2) {
                            servicioSocket.setCobroEfectivo(1, "", solicitudSeleccionada);
                            servicioSocket.setCobroTarjetaCredito(1, "", solicitudSeleccionada);
                            servicioSocket.enviarAbordo(solicitudSeleccionada);
                            btnClienteAbordo.setEnabled(false);

                            Log.e("valorSolicitud", "bien1");

                            mapViewGoogle.trazarRutaSolicitud(mMap, null, false);
                            mapViewConfigMapBox.borrarTrazadoRuta();
                            mapViewConfig.trazarRutaSolicitud(mapaOSM, null, false);


                            mapViewConfig.borrarMarcadorCliente(mapaOSM);
                            mapViewGoogle.borrarMarkerClienteGoogle();


                            deshabilitarComponentesCarrera();
                            if (servicioSocket.isEstadoAbordo) {
                                servicioSocket.estadoBotonDelTaxi(6);
                            } else {
                                servicioSocket.estadoBotonDelTaxi(5);
                            }
                            servicioSocket.estadoSolicitud = 0;

                            servicioSocket.borrarCarreraSeleccionada();
                            lyEnCarrera.setVisibility(View.GONE);
                            imageButtonMensajes.setVisibility(View.GONE);
                            btnWaze.setVisibility(View.GONE);
                            servicioSocket.borrarPreferencia();
                            servicioSocket.isEstadoAbordo = false;
                            activarTaximetro();
                            if (solicitudSeleccionada != null) {
                                if (solicitudSeleccionada.getTipoFormaPago() == 4) {
                                    dialogoSaldoKtaxi = createSaldoKtaxiDialogo();
                                    dialogoSaldoKtaxi.show();
                                }
                            }
                        } else {
                            final Builder alertDialog = new Builder(MapaBaseActivity.this);
                            alertDialog.setCancelable(true);
                            alertDialog.setIcon(R.mipmap.ic_launcher);
                            alertDialog.setTitle(getString(R.string.informacion));
                            alertDialog.setMessage(getString(R.string.cliente_abordo));
                            alertDialog.setNegativeButton("Sí",
                                    new OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            servicioSocket.setCobroEfectivo(1, "", solicitudSeleccionada);
                                            servicioSocket.setCobroTarjetaCredito(1, "", solicitudSeleccionada);
                                            servicioSocket.enviarAbordo(solicitudSeleccionada);
                                            btnClienteAbordo.setEnabled(false);

                                            mapViewConfig.trazarRutaSolicitud(mapaOSM, null, false);
                                            mapViewGoogle.trazarRutaSolicitud(mMap, null, false);


                                            mapViewConfig.borrarMarcadorCliente(mapaOSM);
                                            mapViewGoogle.borrarMarkerClienteGoogle();

                                            deshabilitarComponentesCarrera();
                                            if (servicioSocket.isEstadoAbordo) {
                                                servicioSocket.estadoBotonDelTaxi(6);
                                            } else {
                                                servicioSocket.estadoBotonDelTaxi(5);
                                            }
                                            servicioSocket.estadoSolicitud = 0;
                                            servicioSocket.borrarCarreraSeleccionada();
                                            lyEnCarrera.setVisibility(View.GONE);
                                            imageButtonMensajes.setVisibility(View.GONE);
                                            btnWaze.setVisibility(View.GONE);
                                            servicioSocket.borrarPreferencia();
                                            servicioSocket.isEstadoAbordo = false;
                                            activarTaximetro();
                                            if (solicitudSeleccionada != null) {
                                                if (solicitudSeleccionada.getTipoFormaPago() == 4) {
                                                    dialogoSaldoKtaxi = createSaldoKtaxiDialogo();
                                                    dialogoSaldoKtaxi.show();
                                                }
                                            }
                                            peticionDePublicidad(VariablesPublicidad.AL_PRECIONAR_BOTON_ABORDO, solicitudSeleccionada.getIdSolicitud());
                                        }
                                    });

                            alertDialog.setPositiveButton(R.string.str_no,
                                    new OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            btnClienteAbordo.setEnabled(true);
                                        }
                                    });
                            dialogoAbordo = alertDialog.show();
                        }
                    }
                } else {
                    btnClienteAbordo.setEnabled(false);
                    peticionDePublicidad(VariablesPublicidad.AL_PRECIONAR_BOTON_ABORDO, solicitudSeleccionada.getIdSolicitud());
                }

            }
        } catch (NullPointerException e) {
            e.getMessage();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void notificarRastreo(final int editarCostoC, final int tipo, final String tiempoEspera, final String valorTiempo,
                                 final String velocidadtxt,
                                 final String distanciaRec, final String valorPrincipal, final String valDistancia,
                                 final String cronometro, final String horaInicioTxt, final String costoArranque,
                                 final String totalCarrea) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (utilidades == null && datosTaximetro == null) return;

                Log.e("valorEditarCostoC",editarCostoC+"");

                valorEditarCostoC = editarCostoC;

                if (tipo == 1) {
                    rowTiempoEspera.setVisibility(View.VISIBLE);
                } else {
                    rowTiempoEspera.setVisibility(View.GONE);
                }
                MapaBaseActivity.this.totalCarrera = utilidades.dosDecimales(MapaBaseActivity.this, utilidades.valorDecimalTaximetro(Double.parseDouble(totalCarrea), datosTaximetro.getR().getvV()));
                tvSubTotal.setText(utilidades.dosDecimales(MapaBaseActivity.this, utilidades.valorDecimalTaximetro(Double.parseDouble(valorPrincipal), datosTaximetro.getR().getvV())));
                tvHoraInicio.setText(horaInicioTxt);
                tvDistanciaRecorrida.setText(distanciaRec);
                tvVelocidad.setText(velocidadtxt);
                tvTiempo.setText(cronometro);
                tvCosotoArranque.setText(costoArranque);
                tvValorDistancia.setText(valDistancia);
                tvTiempoEspera.setText(tiempoEspera);
                tvValorTiempo.setText(valorTiempo);


            }
        });
    }

    @OnClick(R2.id.btn_mostrar_taximetro)
    public void onViewClicked() {
        if (lyTaximetro.getVisibility() == View.VISIBLE) {
            lyTaximetro.setVisibility(View.GONE);
        } else {
            lyTaximetro.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Agrega el marcador del taxista en el mapa
     */
    public void agregarMarcadorBoxTaxista(Location location) {
        if (location != null) {
            String nombreTaxista = spLogin.getString("nombres", " ") + " " + spLogin.getString("apellidos", " ");
            if (mapViewConfig != null) {
                mapViewConfig.addMarketTaxista(new LatLng(location.getLatitude(), location.getLongitude()),
                        nombreTaxista, mapaOSM, location.getBearing());
            }
            if (mapViewConfigMapBox != null) {
                mapViewConfigMapBox.addMarketTaxista(new LatLng(location.getLatitude(), location.getLongitude()),
                        nombreTaxista, mapBox, location.getBearing());
            }
            if (mapViewGoogle != null) {
                mapViewGoogle.addMarketTaxista(new com.google.android.gms.maps.model.LatLng(location.getLatitude(), location.getLongitude()),
                        nombreTaxista, mMap, location.getBearing());
            }
        }
    }

    public void activarTaximetro() {
        datosTaximetro = DatosTaximetro.obtenerDatosTaximetro(this);
        crearSolicitudCobro();
        if (datosTaximetro != null) {
            if (datosTaximetro.isTaximetro()) {
                servicioSocket.iniciarTaximetro();
                totalCarrera = null;
                totalCarreraTemporal = null;
                totalCarreraCajaTexto = null;
                if (datosTaximetro.getR().getVisible() == 1) {
                    inclTaximetro.setVisibility(View.VISIBLE);
                }
                servicioSocket.isCambiarLibre = false;
                servicioSocket.controlCambioLibre();
                if (servicioSocket.getIdSolicitudSeleccionada() == 0) {
                    if (locacionDefault != null) {
                        mapaPresenter.iniciarTaximetro(2, 0, locacionDefault.getLatitude(), locacionDefault.getLongitude(), spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), spLogin.getInt(VariablesGlobales.ID_VEHICULO, 0));
                    } else {
                        mapaPresenter.iniciarTaximetro(2, 0, 0, 0, spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), spLogin.getInt(VariablesGlobales.ID_VEHICULO, 0));
                    }
                } else {
                    if (locacionDefault != null) {
                        mapaPresenter.iniciarTaximetro(1, servicioSocket.getIdSolicitudSeleccionada(), locacionDefault.getLatitude(), locacionDefault.getLongitude(), spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), spLogin.getInt(VariablesGlobales.ID_VEHICULO, 0));
                    } else {
                        mapaPresenter.iniciarTaximetro(1, servicioSocket.getIdSolicitudSeleccionada(), 0, 0, spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), spLogin.getInt(VariablesGlobales.ID_VEHICULO, 0));
                    }
                }
            }
        }
    }

    /**
     * Agrega los marcadores de usuarios que realizan peticion de carrera en el mapra
     */
    public void agregarMarketSolicitudes() {
        mapViewConfig.addMarketSolicitudes(mapaOSM, listaSolicitudes);
        mapViewConfigMapBox.addMarketSolicitudes(mapBox, listaSolicitudes);
        mapViewGoogle.addMarketSolicitudes(mMap, listaSolicitudes);
    }

    @Override
    public void solicitudSeleccionadaMarket(Solicitud solicitud) {
        solicitudSeleccionada = solicitud;
        if (dialogoTiempo != null) {
            if (dialogoTiempo.isShowing()) {
                return;
            }
        }
        seleccionarTiempo();
    }

    @Override
    public void intentarGraficar() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            agregarMarketCliente();
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * Agrega el marcador del usuario asingado a la carrera en el mapa
     */
    public void agregarMarketCliente() {
        if (solicitudSeleccionada != null) {
            if (mBound) {
                mapViewConfig.marketCliente(solicitudSeleccionada, servicioSocket.getLocationService(), mapaOSM, true, tokenUsar.getKeyMapa());
                mapViewConfigMapBox.marketCliente(solicitudSeleccionada, servicioSocket.getLocationService(), mapBox, true, tokenUsar.getKeyMapa());
                mapViewGoogle.marketCliente(solicitudSeleccionada, servicioSocket.getLocationService(), mMap, true, tokenUsar.getKeyMapa());
            } else {
                intentarGraficar();
            }

        }
    }

    public void borrarMarketCliente() {
        mapViewConfig.marketCliente(null, null, mapaOSM, false, tokenUsar.getKeyMapa());
        mapViewConfigMapBox.marketCliente(null, null, mapBox, false, tokenUsar.getKeyMapa());
        mapViewGoogle.marketCliente(null, null, mMap, false, tokenUsar.getKeyMapa());
    }


    private DialogInformacionUsuario bottomSheetFragment;
    private DialogosGenericos dialogosGenericos;

    /**
     * Presenta la información de la solicitud atendida en el momento que es tomada por el taxista
     */
    public void informacionSolicitud() {
        dialogosGenericos = new DialogosGenericos();
        if (dialogoInformacionSolicitud != null && dialogoInformacionSolicitud.isShowing())
            return;
        dialogoInformacionSolicitud = dialogosGenericos.mostrarInformaciosnUsuario(this, this, solicitudSeleccionada, servicioSocket.getSpParametrosConfiguracion().getString(VariablesGlobales.DATOS_CONFIGURACION, ""),
                new DialogosGenericos.OnComunicacionCliente() {
                    @Override
                    public void mostrarCliente() {
                        ubicarCliente();
                        if (dialogoInformacionSolicitud != null && dialogoInformacionSolicitud.isShowing())
                            dialogoInformacionSolicitud.dismiss();

                    }
                });
        dialogoInformacionSolicitud.show();
    }

    public void ubicarCliente() {
        if (locacionDefault != null && solicitudSeleccionada != null && solicitudSeleccionada.getLatitud() != 0 && solicitudSeleccionada.getLongitud() != 0) {
            agregarMarketCliente();
            if (mapaOSM != null) {
                GeoPoint gPt = new GeoPoint(solicitudSeleccionada.getLatitud(), solicitudSeleccionada.getLongitud());
                mapaOSM.getController().setZoom(18.5);
                mapaOSM.getController().animateTo(gPt);
            }
            if (mapBox != null) {
                LatLngBounds latLngBounds = new LatLngBounds.Builder()
                        .include(new com.mapbox.mapboxsdk.geometry.LatLng(locacionDefault.getLatitude(), locacionDefault.getLongitude())) // Northeast
                        .include(new com.mapbox.mapboxsdk.geometry.LatLng(solicitudSeleccionada.getLatitud(), solicitudSeleccionada.getLongitud())) // Southwest
                        .build();
                if (utilidades.isVertical(this)) {
                    mapBox.easeCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 50), 1000);
                } else {
                    mapBox.easeCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 250), 1000);
                }

            }
            if (mMap != null && mapFragment != null && mapFragment.getView().getVisibility() == View.VISIBLE) {

                com.google.android.gms.maps.model.LatLng latLngInicial =
                        new com.google.android.gms.maps.model.LatLng(locacionDefault.getLatitude(), locacionDefault.getLongitude());
                com.google.android.gms.maps.model.LatLng latLngFinal =
                        new com.google.android.gms.maps.model.LatLng(solicitudSeleccionada.getLatitud(), solicitudSeleccionada.getLongitud());

                com.google.android.gms.maps.model.LatLngBounds.Builder builder =
                        new com.google.android.gms.maps.model.LatLngBounds.Builder();

                builder.include(latLngInicial);
                builder.include(latLngFinal);

                com.google.android.gms.maps.model.LatLngBounds bounds = builder.build();

                if (utilidades.isVertical(this)) {
                    CameraUpdate cu = com.google.android.gms.maps.CameraUpdateFactory.newLatLngBounds(bounds, 30);
                    mMap.animateCamera(cu);
                } else {
                    CameraUpdate cu = com.google.android.gms.maps.CameraUpdateFactory.newLatLngBounds(bounds, 250);
                    mMap.animateCamera(cu);
                }
            }
            if (solicitudSeleccionada.isSolicitudCC()) {
                btnClienteAbordo.setEnabled(true);
            }

        } else {
            utilidades.mostrarMensajeCorto(MapaBaseActivity.this, getString(R.string.cliente_no_asignado));
        }
    }

    private static final int TIEMPO_BORRAR = 1000;

    /**
     * elimina los marcadores de usuarios que realizan peticion de carrera en el mapra
     */
    public void eliminarMarcadoresSolicitudesBoxYOpen() {
        mapViewConfig.eliminarMarcadores(mapaOSM);
        if (mapBox != null) {
            mapViewConfigMapBox.eliminarMarcadores(mapBox);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mapBox != null) {
                        eliminarMarcadoresSolicitudesBoxYOpen();
                    }
                }
            }, TIEMPO_BORRAR);
        }
        if (mMap != null) {
            mapViewGoogle.eliminarMarcadores(mMap);
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mMap != null) {
                        eliminarMarcadoresSolicitudesBoxYOpen();
                    }
                }
            }, TIEMPO_BORRAR);
        }

    }

    /**
     * Presenta la información del cliente asignado a la solicitud atendida.
     */
    public void presentarInformacionCliente() {
        if (dialogoInf != null) {
            if (dialogoInf.isShowing()) {
                return;
            }
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (spLogin.getBoolean("logeado", true)) {
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View viewInformacion = inflater.inflate(R.layout.dialog_perfil_conductor, null, false);
                    final ImageView imagenPerfil = viewInformacion.findViewById(R.id.imageView);
                    if (imagenGlobal != null) {
                        imagenPerfil.setImageBitmap(imagenGlobal);
                    }
                    final TextView clienteNombre = viewInformacion
                            .findViewById(R.id.tvNombre);
                    clienteNombre.setText(spLogin.getString("nombres", " "));
                    final TextView clienteApellido = viewInformacion
                            .findViewById(R.id.tvApellido);
                    clienteApellido.setText(spLogin.getString("apellidos", " "));
                    final TextView clienteCelular = viewInformacion
                            .findViewById(R.id.tvCelular);
                    clienteCelular.setText(spLogin.getString("celular", ""));
                    final TextView clienteCorreo = viewInformacion
                            .findViewById(R.id.tvCorreo);
                    clienteCorreo.setText(spLogin.getString("correo", ""));
                    final TextView clienteIdVehiculo = viewInformacion
                            .findViewById(R.id.tvEmpresa);
                    clienteIdVehiculo.setText(String.valueOf(spLogin.getString("empresa", "")));
                    final TextView placa = viewInformacion
                            .findViewById(R.id.tvPlaca);
                    placa.setText(String.valueOf(spLogin.getString(VariablesGlobales.PLACA_VEHICULO, "")));
                    final TextView resgistroMunicipal = viewInformacion
                            .findViewById(R.id.tvRegistroMun);
                    final TextView tituloResgistroMunicipal = viewInformacion
                            .findViewById(R.id.tv_titulo_reg_municipal);
                    String registroMunicipalText = String.valueOf(spLogin.getString("regMunVehiculo", ""));
                    if (registroMunicipalText != null && !registroMunicipalText.isEmpty()) {
                        resgistroMunicipal.setText(registroMunicipalText);
                    } else {
                        tituloResgistroMunicipal.setVisibility(View.GONE);
                        resgistroMunicipal.setVisibility(View.GONE);
                    }
                    final TextView marca = viewInformacion
                            .findViewById(R.id.tvMarca);
                    marca.setText(String.valueOf(spLogin.getString("marcaVehiculo", "")));
                    final TextView modelo = viewInformacion
                            .findViewById(R.id.tvModelo);
                    modelo.setText(String.valueOf(spLogin.getString("modeloVehiculo", "")));
                    final TextView anio = viewInformacion
                            .findViewById(R.id.tvAnio);
                    anio.setText(String.valueOf(spLogin.getInt("anioVehiculo", 0)));
                    final TextView numeroUnidad = viewInformacion
                            .findViewById(R.id.tvNumUnidad);
                    final Button btn_aceptar = viewInformacion.findViewById(R.id.btn_aceptar);
                    final Builder builder = new Builder(MapaBaseActivity.this);
                    builder.setView(viewInformacion);
                    numeroUnidad.setText(String.valueOf(spLogin.getInt("unidadVehiculo", 0)));
                    if (!isFinishing()) {
                        dialogoInf = builder.create();
                        dialogoInf.show();
                    }
                    final TextView tvCiudad = viewInformacion.findViewById(R.id.tv_ciudad);
                    final TextView tvCiudadTitulo = viewInformacion.findViewById(R.id.tv_ciudad_titulo);
                    if (spLogin.getString(VariablesGlobales.CIUDAD, "").isEmpty()) {
                        tvCiudad.setVisibility(View.GONE);
                        tvCiudadTitulo.setVisibility(View.GONE);
                    } else {
                        tvCiudad.setVisibility(View.VISIBLE);
                        tvCiudadTitulo.setVisibility(View.VISIBLE);
                        tvCiudad.setText(spLogin.getString(VariablesGlobales.CIUDAD, ""));
                    }
                    btn_aceptar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (dialogoInf != null) {
                                if (dialogoInf.isShowing()) {
                                    dialogoInf.dismiss();
                                }
                            }
                        }
                    });
                } else {
                    utilidades.mostrarMensajeCorto(MapaBaseActivity.this, getString(R.string.no_recupera_informacion));
                }
            }
        });


    }

    /**
     * Presenta el dialogo para el cambio de clave del usuario
     */
    public void presentarCambioCable() {
        if (utilidades.isVerificarDialogo(dialogoCambioClave)) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //Los elementos del layout del dialogo deben ser final para poder manipularlos.
            @SuppressLint("InflateParams") final View viewInformacion = inflater.inflate(R.layout.dialog_cambio_clave,
                    null, false);
            final EditText claveAnterior = viewInformacion
                    .findViewById(R.id.etClaveAnterior);
            final EditText claveNueva = viewInformacion
                    .findViewById(R.id.etClaveNueva);
            final Button btnAceptar = viewInformacion
                    .findViewById(R.id.btnAceptar);
            final Button btnCancelar = viewInformacion
                    .findViewById(R.id.btnCancelar);
            final TextInputLayout tilClaveAnterior = viewInformacion.findViewById(R.id.til_clave_anterior);
            final TextInputLayout tilClaveNueva = viewInformacion.findViewById(R.id.til_clave_nueva);
            claveNueva.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    tilClaveNueva.setError(null);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            claveAnterior.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    tilClaveAnterior.setError(null);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            Builder builder = new Builder(MapaBaseActivity.this);
            builder.setView(viewInformacion);
            builder.setCancelable(false);
            if (!isFinishing()) {
                dialogoCambioClave = builder.show();
            }
            btnCancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    utilidades.neutralizarDialogo(dialogoCambioClave);
                }
            });
            btnAceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String passAnterior = claveAnterior.getText().toString();
                    String passNueva = claveNueva.getText().toString();
                    if (passAnterior.trim().equals("")) {
                        claveAnterior.requestFocus();
                        tilClaveAnterior.setError(getString(R.string.campo_vacio));
                        return;
                    }
                    if (passNueva.trim().equals("")) {
                        claveNueva.requestFocus();
                        tilClaveNueva.setError(getString(R.string.campo_vacio));
                        return;
                    }
                    if (!utilidades.comprobarTamanioClave(passNueva.trim())) {
                        claveNueva.requestFocus();
                        tilClaveNueva.setError(getString(R.string.mensaje_contrasenia));
                        return;
                    }
                    if (passAnterior.equals(passNueva)) {
                        claveNueva.requestFocus();
                        utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.contrasenia_iguales));
                        return;
                    }
                    mostrarDialogoEspera(getString(R.string.mensaje_cambiando_clave));
                    mapaPresenter.obtenerCambioClave(spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), passAnterior, passNueva);
                }
            });
        }
    }

    /**
     * cobrar carrera
     * Presenta el dialogo para el cambio de clave del usuario
     */
    public void presentarValorCobro() {
        reiniciarConfiguracionTaximetroTemporal();
        if (utilidades.isVerificarDialogo(dialogCobro)) {
            if (mBound) { //Para cuando es cobro en efectivo con tarjeta de saldo ktaxi o normal
                switch (servicioSocket.getEstadoEfecitvo()) {
                    case 0:
                        servicioSocket.setCobroEfectivo(1, "", solicitudSeleccionada);
                        solicitudSeleccionada = servicioSocket.getSolicitudEfectivo();
                        break;
                    case 1:
                        solicitudSeleccionada = servicioSocket.getSolicitudEfectivo();
                        break;
                    case 2:
                        mostrarEspera(getString(R.string.msj_enviando_datos));
                        servicioSocket.cobroReloadEfectivo();
                        break;
                    case 3:
                        servicioSocket.ejecutarRespuestaEfectivoCobro();
                        return;
                    case 4:
                        presentar_info_fin_carrera();
                        return;
                }
            } else {
                return;
            }
            if (mBound) { //Para cuando es por tarjeta de credito
                if (servicioSocket.getSolicitudTarjetaCredito() != null &&
                        servicioSocket.getSolicitudTarjetaCredito().getTipoFormaPago() == VariablesGlobales.TARJETA_CREDITO) {
                    switch (servicioSocket.getEstadoCobroTarjetaCredito()) {
                        case 0:
                            servicioSocket.setCobroTarjetaCredito(1, "", solicitudSeleccionada);
                            solicitudSeleccionada = servicioSocket.getSolicitudTarjetaCredito();
                            break;
                        case 1:
                            solicitudSeleccionada = servicioSocket.getSolicitudTarjetaCredito();
                            break;
                        case 2:
                            mostrarEspera("Enviando datos.");
                            servicioSocket.cobroReloadTarjetaCredito();
                            return;
                        case 3:
                            servicioSocket.ejecutarRespuestaCobroTarjetaCredito();
                            return;
                        case 4:
                            presentar_info_fin_carrera();
                            return;
                    }
                }
            } else {
                return;
            }
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            assert inflater != null;
            final View viewInformacion = inflater.inflate(R.layout.dialog_cobro_forma_pago, null, false);
            final ImageView img_tipo = viewInformacion.findViewById(R.id.img_tipo_cobro);
            final ImageView img_salir = viewInformacion.findViewById(R.id.img_salir);

            final Button btnCobroRegistroTarjetaCredito = viewInformacion.findViewById(R.id.btn_registro_tarjeta_credito);
            final Button btnVaucher = viewInformacion.findViewById(R.id.btn_vaucher);
            final Button btnVaucherPin = viewInformacion.findViewById(R.id.btn_envio_pin);
            final Button btnVaucherPinCallCenter = viewInformacion.findViewById(R.id.btn_pin_callCenter);
            final Button btnTarjetaCredito = viewInformacion.findViewById(R.id.btn_tarjeta_credito);

            final Button btnClienteUido = viewInformacion.findViewById(R.id.btn_cliente_uido);
            final Button btnPayPhone = viewInformacion.findViewById(R.id.btn_pay_phone);
            final Button btnElectronico = viewInformacion.findViewById(R.id.btn_electronico);
            final Button btnEfectivo = viewInformacion.findViewById(R.id.btn_efectivo);
            final Button btnMasPasajero = viewInformacion.findViewById(R.id.btn_mas_pasajero);
            final Button btnMenosPasajero = viewInformacion.findViewById(R.id.btn_menos_pasajero);

            final EditText etCostoCarrera = viewInformacion.findViewById(R.id.et_costo_carrera);
            final EditText etSaldoKtaxi = viewInformacion.findViewById(R.id.et_saldo_ktaxi);
            final EditText etPropina = viewInformacion.findViewById(R.id.et_propina);
            final EditText etValorCobrar = viewInformacion.findViewById(R.id.et_valor_cobro);
            final TextView tvNumPasajeros = viewInformacion.findViewById(R.id.tv_num_pasajero);

            if (valorEditarCostoC == 0) {
                etCostoCarrera.setEnabled(false);
            } else {
                etCostoCarrera.setEnabled(true);
            }

            TableRow filaSaldoKtaxi = viewInformacion.findViewById(R.id.fila_saldo_ktaxi);
            LinearLayout lyPasajero = viewInformacion.findViewById(R.id.ly_pasajeros);
            LinearLayout lyVoucher = viewInformacion.findViewById(R.id.ly_voucher);
            definirPasajero(lyPasajero, tvNumPasajeros);
            if (mBound) {
                if (servicioSocket.solicitudSeleccionada != null && servicioSocket.solicitudSeleccionada.getTipoFormaPago() == 1) {
                    lyVoucher.setVisibility(View.VISIBLE);
                }
            }

            if (mBound) {
                if (servicioSocket.solicitudSeleccionada != null && servicioSocket.solicitudSeleccionada.getTipoFormaPago() == 5) {
                    btnVaucherPinCallCenter.setVisibility(View.VISIBLE);
                } else {
                    btnVaucherPinCallCenter.setVisibility(View.GONE);
                }
            }

            if (mBound) {
                if (solicitudSeleccionada != null && solicitudSeleccionada.getTipoFormaPago() == VariablesGlobales.TARJETA_CREDITO) {
                    if (isVisibleTarjetaCredito) {
                        btnTarjetaCredito.setVisibility(View.VISIBLE);
                    }
                } else {
                    btnTarjetaCredito.setVisibility(View.GONE);
                }
            }

            if (mBound) {
                if (solicitudSeleccionada != null && solicitudSeleccionada.getTipoFormaPago() == VariablesGlobales.TARJETA_CREDITO_NUEVO) {
                    btnTarjetaCredito.setVisibility(View.VISIBLE);
                } else {
                    btnTarjetaCredito.setVisibility(View.GONE);
                }
            }
            definirIconoFormaDePago(img_tipo);
            etValorCobrar.setEnabled(false);
            if (DatosEnvioTaximetro.getAll() != null && DatosEnvioTaximetro.getAll().size() > 0) {
                totalCarrera = DatosEnvioTaximetro.getAll().get(0).getTotalCarrera();


            }
            if (totalCarreraTemporal != null) {
                totalCarrera = totalCarreraTemporal;
            }
            if (totalCarrera != null) {
                totalCarreraTemporal = totalCarrera;
            }
            if (servicioSocket.getSolicitudEfectivo() != null) {
                solicitudSeleccionada = servicioSocket.getSolicitudEfectivo();
                etSaldoKtaxi.setText(utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getSaldo()));
                valorMaximoKtaxi = solicitudSeleccionada.getSaldo();
                etPropina.setText(utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getPropina()));
                if (totalCarrera != null && !totalCarrera.isEmpty() && solicitudSeleccionada.getIdPedido() == 0) {
                    etCostoCarrera.setText(totalCarrera);
                    if (Double.parseDouble(totalCarreraTemporal) <= solicitudSeleccionada.getSaldo()) {
                        etSaldoKtaxi.setText(utilidades.dosDecimales(MapaBaseActivity.this, Double.parseDouble(totalCarreraTemporal)));
                    }
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty()) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty()) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty()) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(MapaBaseActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                    Log.e("valorCobro1", etValorCobrar.getText().toString().trim() + "");
                } else {
                    if (solicitudSeleccionada != null && solicitudSeleccionada.getPrecioPedido() != 0) {
                        etCostoCarrera.setText(String.valueOf(solicitudSeleccionada.getPrecioPedido()));
                    } else {
                        etCostoCarrera.setText(String.valueOf(0));
                    }
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty()) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty()) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty()) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(MapaBaseActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                    Log.e("valorCobro2", etValorCobrar.getText().toString().trim() + "");
                }
            } else {
                filaSaldoKtaxi.setVisibility(View.GONE);
                if (totalCarrera != null && !totalCarrera.isEmpty()) {
                    etCostoCarrera.setText(totalCarrera);
                    etSaldoKtaxi.setText(utilidades.dosDecimales(MapaBaseActivity.this, 0.0));
                    etPropina.setText(utilidades.dosDecimales(MapaBaseActivity.this, 0.0));
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty() && !etCostoCarrera.getText().toString().equals(".")) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty() && !etSaldoKtaxi.getText().toString().equals(".")) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty() && !etPropina.getText().toString().equals(".")) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(MapaBaseActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                    Log.e("valorCobro3", etValorCobrar.getText().toString().trim() + "");
                } else {
                    etCostoCarrera.setText(utilidades.dosDecimales(MapaBaseActivity.this, 0.0));
                    etSaldoKtaxi.setText(utilidades.dosDecimales(MapaBaseActivity.this, 0.0));
                    etPropina.setText(utilidades.dosDecimales(MapaBaseActivity.this, 0.0));
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty() && !etCostoCarrera.getText().toString().equals(".")) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty() && !etSaldoKtaxi.getText().toString().equals(".")) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty() && !etPropina.getText().toString().equals(".")) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(MapaBaseActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                    Log.e("valorCobro4", etValorCobrar.getText().toString().trim() + "");
                }
            }


            etSaldoKtaxi.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty() && !etCostoCarrera.getText().toString().equals(".")) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty() && !etSaldoKtaxi.getText().toString().equals(".")) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                        if (saldoKtaxi > valorMaximoKtaxi) {
                            saldoKtaxi = valorMaximoKtaxi;
                            etSaldoKtaxi.setText(utilidades.dosDecimales(MapaBaseActivity.this, saldoKtaxi));
                            if (saldoKtaxi != 0.0) {
                                Toast.makeText(MapaBaseActivity.this, "No se puede definir un valor mayor al enviado por el cliente.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                    if (!etPropina.getText().toString().isEmpty() && !etPropina.getText().toString().equals(".")) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }

                    if (costoCarrera < saldoKtaxi) {
                        double dosDecimales = utilidades.formatDecimales(costoCarrera, 2);
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(MapaBaseActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                    Log.e("valorCobro5", etValorCobrar.getText().toString().trim() + "");
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            etPropina.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty() && !etCostoCarrera.getText().toString().equals(".")) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty() && !etSaldoKtaxi.getText().toString().equals(".")) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                        if (saldoKtaxi > valorMaximoKtaxi) {
                            saldoKtaxi = valorMaximoKtaxi;
                            etSaldoKtaxi.setText(utilidades.dosDecimales(MapaBaseActivity.this, saldoKtaxi));
                            Toast.makeText(MapaBaseActivity.this, "No se puede definir un valor mayor al enviado por el cliente.", Toast.LENGTH_LONG).show();
                        }
                    }
                    if (!etPropina.getText().toString().isEmpty() && !etPropina.getText().toString().equals(".")) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(MapaBaseActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                    Log.e("valorCobro6", etValorCobrar.getText().toString().trim() + "");
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            etCostoCarrera.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (etCostoCarrera.getText().toString().equals(".")) {
                        etCostoCarrera.setText("0");
                        return;
                    }
                    if (!etCostoCarrera.getText().toString().isEmpty()) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty()) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    if (solicitudSeleccionada != null && costoCarrera <= valorMaximo) {
                        etSaldoKtaxi.setText(utilidades.dosDecimales(MapaBaseActivity.this, costoCarrera));
                    } else {
                        if (solicitudSeleccionada != null) {
                            etSaldoKtaxi.setText(utilidades.dosDecimales(MapaBaseActivity.this, costoCarrera));
                        }
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty()) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                        if (saldoKtaxi > valorMaximoKtaxi) {
                            saldoKtaxi = valorMaximoKtaxi;
                            etSaldoKtaxi.setText(utilidades.dosDecimales(MapaBaseActivity.this, saldoKtaxi));
                            Toast.makeText(MapaBaseActivity.this, "No se puede definir un valor mayor al enviado por el cliente.", Toast.LENGTH_LONG).show();
                        }
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(MapaBaseActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                    Log.e("valorCobro7", etValorCobrar.getText().toString().trim() + "");
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            definirHindCobro(etCostoCarrera);
            definirHindCobro(etSaldoKtaxi);
            definirHindCobro(etPropina);
            definirHindCobro(etValorCobrar);

            btnMasPasajero.setOnClickListener(clickPasajero(tvNumPasajeros));
            btnMenosPasajero.setOnClickListener(clickPasajero(tvNumPasajeros));

            btnVaucher.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            btnVaucherPin.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));

            btnVaucherPinCallCenter.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));

            btnCobroRegistroTarjetaCredito.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialogCobro != null && dialogCobro.isShowing()) dialogCobro.dismiss();
                    Intent intent = new Intent(MapaBaseActivity.this, AgregarTarjeta.class);
                    intent.putExtra("costoCarrera", etCostoCarrera.getText().toString());
                    intent.putExtra("saldoKtaxi", etSaldoKtaxi.getText().toString());
                    intent.putExtra("propina", etPropina.getText().toString());
                    intent.putExtra("aCobrar", etValorCobrar.getText().toString());
                    intent.putExtra("solicitud", solicitudSeleccionada);
                    startActivityForResult(intent, AgregarTarjeta.AGREGAR_TARJETA);
                }
            });

            btnClienteUido.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            btnPayPhone.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            btnElectronico.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            btnEfectivo.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            btnTarjetaCredito.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            img_salir.setOnClickListener(definirFormaPago(null, null, null, null, null));

            definirFormaDePago(lyVoucher, btnElectronico, btnPayPhone, btnClienteUido, btnVaucherPinCallCenter);

            Builder builder = new Builder(MapaBaseActivity.this);
            builder.setView(viewInformacion);
            builder.setCancelable(false);
            if (!isFinishing()) {
                dialogCobro = builder.show();
            }
            etCostoCarrera.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    etCostoCarrera.setBackgroundResource(R.drawable.style_caja_texto);
                }
            });

            if (solicitudSeleccionada != null && solicitudSeleccionada.getPrecioVoucher() != 0) {
                etCostoCarrera.setText(utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getPrecioVoucher()));
                etCostoCarrera.setEnabled(false);
                servicioSocket.reproducirTextAudio.speak("Esta carrera es de Voucher fijo con valor de ".concat(
                        utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getPrecioVoucher())));
            }
        }
    }

    public double totalCarrera(double costoCarrera, double saldoKtaxi, double propina) {
        double total = costoCarrera - saldoKtaxi;
        if (total < 0) {
            return propina;
        } else {
            return total + propina;
        }
    }

    public void definirFormaDePago(LinearLayout lyVoucher, Button electronico, Button payPhone,
                                   Button voucherEmergente, Button btnVoucherCallCenter) {
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 18:
                                if (objetoConfig.getInt("h") == 1) {

                                    if (servicioSocket.solicitudSeleccionada != null && servicioSocket.solicitudSeleccionada.getTipoFormaPago() != 1) {
                                        lyVoucher.setVisibility(View.GONE);
                                    } else {
                                        if (servicioSocket.solicitudSeleccionada != null) {
                                            lyVoucher.setVisibility(View.VISIBLE);
                                            if (objetoConfig.getString("vd").equals("1")) {
                                                voucherEmergente.setVisibility(View.VISIBLE);
                                            } else {
                                                voucherEmergente.setVisibility(View.GONE);
                                            }
                                        } else {
                                            lyVoucher.setVisibility(View.GONE);
                                        }
                                    }

                                } else {
                                    if (mBound) {
                                        if (servicioSocket.solicitudSeleccionada != null && servicioSocket.solicitudSeleccionada.getTipoFormaPago() == 1) {
                                            lyVoucher.setVisibility(View.VISIBLE);
                                        } else {
                                            lyVoucher.setVisibility(View.GONE);
                                        }
                                    }


                                }
                                break;
                            case 19:
                                if (objetoConfig.getInt("h") == 1) {
                                    electronico.setVisibility(View.VISIBLE);
                                } else {
                                    electronico.setVisibility(View.GONE);
                                }
                                break;
                            case 20:
                                if (objetoConfig.getInt("h") == 1) {
                                    payPhone.setVisibility(View.VISIBLE);
                                } else {
                                    payPhone.setVisibility(View.GONE);

                                }
                                break;

                            case 21:

                                if (mBound) {
                                    if (servicioSocket.solicitudSeleccionada != null && servicioSocket.solicitudSeleccionada.getTipoFormaPago() == 5) {
                                        btnVoucherCallCenter.setVisibility(View.VISIBLE);
                                    } else {
                                        btnVoucherCallCenter.setVisibility(View.GONE);
                                    }
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.getMessage();
            }
            if (solicitudSeleccionada != null) {
                switch (solicitudSeleccionada.getTipoFormaPago()) {
                    case VariablesGlobales.VOUCHER:
                        lyVoucher.setVisibility(View.VISIBLE);
                        break;
                    case VariablesGlobales.DINERO_ELECTRONICO:
                        electronico.setVisibility(View.VISIBLE);
                        break;
                    case VariablesGlobales.PAYPHONE:
                        payPhone.setVisibility(View.VISIBLE);
                        break;
                    case VariablesGlobales.VOUCHER_CALL_CENTER:
                        btnVoucherCallCenter.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }
    }

    public boolean provarCampos(EditText editText) {
        if (editText.getText().toString().isEmpty()) {
            return false;
        }
        return !editText.getText().toString().equals(".");
    }

    public void definirIconoFormaDePago(ImageView img_tipo) {
        if (mBound) {
            switch (servicioSocket.getTipoFormaPago()) {
                case VariablesGlobales.VOUCHER:
                    img_tipo.setImageResource(R.mipmap.voucherapp);
                    break;
                case VariablesGlobales.DINERO_ELECTRONICO:
                    img_tipo.setImageResource(R.mipmap.ic_dinero_electronico);
                    break;
                case VariablesGlobales.PAYPHONE:
                    img_tipo.setImageResource(R.mipmap.ic_icon_payphone);
                    break;
                case VariablesGlobales.SALDO_KTAXI:
                    img_tipo.setImageResource(R.mipmap.ic_saldo_ktaxi);
                    break;
                case VariablesGlobales.VOUCHER_CALL_CENTER:
                    img_tipo.setImageResource(R.mipmap.vouchercall);
                    break;
                default:
                    img_tipo.setImageResource(R.mipmap.ic_efectivo);
                    break;
            }
        }
    }

    public View.OnClickListener clickPasajero(final TextView tvNumPasajero) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numero = Integer.parseInt(tvNumPasajero.getText().toString());
                int i = v.getId();
                if (i == R.id.btn_mas_pasajero) {
                    tvNumPasajero.setBackgroundColor(getResources().getColor(R.color.transparent));
                    if (numero == 6) {
                        Toast.makeText(MapaBaseActivity.this, "Este es el número máximo permitido", Toast.LENGTH_LONG).show();
                        return;
                    }
                    numero = numero + 1;
                    tvNumPasajero.setText(String.valueOf(numero));

                } else if (i == R.id.btn_menos_pasajero) {
                    tvNumPasajero.setBackgroundColor(getResources().getColor(R.color.transparent));
                    if (numero == 0) {
                        Toast.makeText(MapaBaseActivity.this, "Este es el número mínimo permitido", Toast.LENGTH_LONG).show();
                        return;
                    }
                    numero = numero - 1;
                    tvNumPasajero.setText(String.valueOf(numero));

                }
            }
        };
    }

    /**
     * Crea un diálogo con personalizado
     *
     * @return Diálogo
     */
    public AlertDialog createVoucherPinDialogo(final double costo, final double saldoKtaxi,
                                               final double propina, final int numeroPasajero) {
        Builder builder = new Builder(MapaBaseActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialogo_pin_voucher, null);
        final EditText etPinVaucher = v.findViewById(R.id.et_pin_carrera);
        //final TextView txtPin = v.findViewById(R.id.txt_pin);

        // txtPin.setText(String.valueOf(solicitudSeleccionada.getPin()));

        Button btnRegresar = v.findViewById(R.id.btn_regresar);
        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = v.getId();
                if (i == R.id.btn_regresar) {
                    if (dialogoVoucherPin != null && dialogoVoucherPin.isShowing()) {
                        dialogoVoucherPin.dismiss();
                    }

                } else if (i == R.id.btn_aceptar) {
                    cobroVaucherPin(costo, etPinVaucher.getText().toString(), saldoKtaxi, propina, numeroPasajero);
                    if (dialogoVoucherPin != null && dialogoVoucherPin.isShowing()) {
                        dialogoVoucherPin.dismiss();
                    }

                }
            }
        };
        btnRegresar.setOnClickListener(onClickListener);
        btnAceptar.setOnClickListener(onClickListener);
        builder.setView(v);
        return builder.create();
    }

    public AlertDialog createVoucherPinDialogoCallCenter(final double costo,
                                                         final double saldoKtaxi, final double propina, final int numeroPasajero) {

        Builder builder = new Builder(MapaBaseActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialogo_pin_voucher, null);
        final EditText etPinVaucher = v.findViewById(R.id.et_pin_carrera);
        final TextView txtPin = v.findViewById(R.id.txt_pin);

        txtPin.setText(String.valueOf(solicitudSeleccionada.getPin()));

        Button btnRegresar = v.findViewById(R.id.btn_regresar);

        Button btnAceptar = v.findViewById(R.id.btn_aceptar);


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = v.getId();
                if (i == R.id.btn_regresar) {
                    if (dialogoVoucherPin != null && dialogoVoucherPin.isShowing()) {
                        dialogoVoucherPin.dismiss();
                    }

                } else if (i == R.id.btn_aceptar) {
                    cobroVaucherCallCenter(costo, etPinVaucher.getText().toString(), saldoKtaxi, propina, numeroPasajero);
                    if (dialogoVoucherPin != null && dialogoVoucherPin.isShowing()) {
                        dialogoVoucherPin.dismiss();
                    }

                }
            }
        };
        btnRegresar.setOnClickListener(onClickListener);
        btnAceptar.setOnClickListener(onClickListener);
        builder.setView(v);
        return builder.create();
    }

    /**
     * Crea un diálogo con personalizado
     *
     * @return Diálogo
     */
    public AlertDialog createDineroElectronicoDialogo(final double costo,
                                                      final double saldoKtaxi, final double propina, final int numeroPasajero) {
        Builder builder = new Builder(MapaBaseActivity.this);
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialogo_dinero_electronico, null);
        final EditText etUsuario = v.findViewById(R.id.et_usuario_dinero_electronico);
        final EditText etPin = v.findViewById(R.id.et_pin_dinero_electronico);
        Button btnRegresar = v.findViewById(R.id.btn_regresar);
        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = v.getId();
                if (i == R.id.btn_regresar) {
                    if (dialogoDineroElectronico != null && dialogoDineroElectronico.isShowing()) {
                        dialogoDineroElectronico.dismiss();
                    }

                } else if (i == R.id.btn_aceptar) {
                    cobroDineroElectronico(costo, etUsuario.getText().toString(), etPin.getText().toString(), saldoKtaxi, propina, numeroPasajero);
                    if (dialogoDineroElectronico != null && dialogoDineroElectronico.isShowing()) {
                        dialogoDineroElectronico.dismiss();
                    }

                }
            }
        };
        btnRegresar.setOnClickListener(onClickListener);
        btnAceptar.setOnClickListener(onClickListener);
        builder.setView(v);
        return builder.create();
    }

    /***
     * OnClick de forma de pago aqui es donde se define el comporataiento de cada uno de los botones de forma de pago
     * @return
     */
    public View.OnClickListener definirFormaPago(final EditText costoCarrera,
                                                 final EditText costoFinal,
                                                 final EditText etSaldoKtaxi, final EditText EtPropina, final TextView TvNumeroPasajero) {

        return new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                if (costoCarrera != null) {
                    if (!provarCampos(costoCarrera)) {
                        Toast.makeText(MapaBaseActivity.this, "El valor ingresado no es correcto", Toast.LENGTH_LONG).show();
                        costoCarrera.setText("0.0");
                        costoCarrera.requestFocus();
                        return;
                    }
                }
                if (costoFinal != null) {
                    if (provarCampos(costoFinal)) {
                        costo = Double.parseDouble(costoFinal.getText().toString());
                    } else {
                        Toast.makeText(MapaBaseActivity.this, "El valor ingresado no es correcto", Toast.LENGTH_LONG).show();
                        costoFinal.setText("0.0");
                        costoFinal.requestFocus();
                        return;
                    }
                }
                if (etSaldoKtaxi != null) {
                    if (provarCampos(etSaldoKtaxi)) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    } else {
                        Toast.makeText(MapaBaseActivity.this, "El valor ingresado no es correcto", Toast.LENGTH_LONG).show();
                        etSaldoKtaxi.setText("0.00");
                        etSaldoKtaxi.requestFocus();
                        return;
                    }
                }
                if (EtPropina != null) {
                    if (provarCampos(EtPropina)) {
                        propina = Double.parseDouble(EtPropina.getText().toString());
                    } else {
                        Toast.makeText(MapaBaseActivity.this, "El valor ingresado no es correcto", Toast.LENGTH_LONG).show();
                        EtPropina.setText("0.0");
                        EtPropina.requestFocus();
                        return;
                    }
                }
                if (TvNumeroPasajero != null) {
                    numeroPasajeros = Integer.parseInt(TvNumeroPasajero.getText().toString());
                }
                int i = v.getId();
                if (i == R.id.btn_vaucher) {
                    cobroVaucher(costo, saldoKtaxi, propina, numeroPasajeros);

                } else if (i == R.id.btn_envio_pin) {
                    dialogoVoucherPin = createVoucherPinDialogo(costo, saldoKtaxi, propina, numeroPasajeros);
                    if (!isFinishing()) {
                        dialogoVoucherPin.show();
                    }

                } else if (i == R.id.btn_pin_callCenter) {
                    dialogoVoucherPin = createVoucherPinDialogoCallCenter(costo, saldoKtaxi, propina, numeroPasajeros);
                    if (!isFinishing()) {
                        dialogoVoucherPin.show();
                    }

                } else if (i == R.id.btn_cliente_uido) {
                    cobroVoucherClienteUido(costo, saldoKtaxi, propina, numeroPasajeros);

                } else if (i == R.id.btn_pay_phone) {
                } else if (i == R.id.btn_electronico) {
                    dialogoDineroElectronico = createDineroElectronicoDialogo(costo, saldoKtaxi, propina, numeroPasajeros);
                    if (!isFinishing()) {
                        dialogoDineroElectronico.show();
                    }

                } else if (i == R.id.btn_efectivo) {
                    cobroEfectivo(costo, saldoKtaxi, propina, numeroPasajeros, costoCarrera, TvNumeroPasajero);

                } else if (i == R.id.btn_tarjeta_credito) {
                    cobroTarjetaCredito(costo, saldoKtaxi, propina, numeroPasajeros, costoCarrera, TvNumeroPasajero);

                } else if (i == R.id.img_salir) {
                    if (dialogCobro != null && dialogCobro.isShowing()) {
                        dialogCobro.dismiss();
                    }

                }
            }
        };
    }

    public void cobroEfectivo(final double costo, final double saldoKtaxi,
                              final double propina, final int numeroPasajero, TextView costoCarrera, TextView
                                      numeroPasajeros) {
        if (isObligatorioCostoCarrera) {
            if (costoCarrera.getText().toString().isEmpty()) {
                Toast.makeText(this, "Defina un costo de la carrera.", Toast.LENGTH_LONG).show();
                costoCarrera.setBackgroundResource(R.drawable.style_caja_texto_error);
                return;
            }
            double dbCostoCarrera = Double.parseDouble(costoCarrera.getText().toString().equals(".") ? "0" : costoCarrera.getText().toString());
            if (dbCostoCarrera == 0) {
                Toast.makeText(this, "Defina un costo de la carrera diferente de 0.00", Toast.LENGTH_LONG).show();
                costoCarrera.setBackgroundResource(R.drawable.style_caja_texto_error);
                return;
            }
        }
        if (isObligatorioNumeroPasajeros) {
            if (numeroPasajero == 0) {
                Toast.makeText(this, "Defina un número de pasajeros para continuar.", Toast.LENGTH_LONG).show();
                numeroPasajeros.setBackgroundColor(getResources().getColor(R.color.rojo_bajo));
                return;
            }
        }
        if (solicitudSeleccionada != null && solicitudSeleccionada.getTipoFormaPago() != 0) {
            Builder dialogo = new Builder(MapaBaseActivity.this);
            dialogo.setTitle(R.string.informacion);
            if (solicitudSeleccionada.getTipoFormaPago() == 4) {
                double dbCostoCarrera = Double.parseDouble(costoCarrera.getText().toString().equals(".") ? "0" : costoCarrera.getText().toString());
                if (dbCostoCarrera == 0) {
                    dialogo.setIcon(R.mipmap.ic_saldo_ktaxi);
                    dialogo.setMessage("Defina un costo de la carrera diferente de 0.00, recuerde esta carrera fue realizada con saldo ktaxi y debe definir un valor.");
                    dialogo.setNegativeButton("Regresar", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    if (!isFinishing()) {
                        dialogo.show();
                    }

                    costoCarrera.setBackgroundResource(R.drawable.style_caja_texto_error);
                    costoCarrera.requestFocus();
                    return;
                }
                servicioSocket.reproducirTextAudio.speak("Esta solicitud se realizó con Saldo K taxi, por favor cobra la diferencia " + costo + "  en efectivo");
                dialogo.setIcon(R.mipmap.ic_saldo_ktaxi);
                dialogo.setMessage("Esta solicitud se realizó con Saldo Ktaxi, por favor cobra la diferencia " + utilidades.dosDecimales(MapaBaseActivity.this, costo) + "  en efectivo.");
                dialogo.setNegativeButton("Regresar", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

            } else {
                dialogo.setNegativeButton("Regresar", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialogo.setMessage("Esta solicitud se realizo con otra forma de pago, desea cobrar con efectivo");
            }
            dialogo.setCancelable(false).setPositiveButton("Cobrar", new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (costo > valorMaximo) {
                        DecimalFormat df = new DecimalFormat("####0.00");
                        final Builder dialogo = new Builder(MapaBaseActivity.this);
                        dialogo.setTitle(R.string.str_alerta_min);
                        dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
                        dialogo.setCancelable(false);
                        dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                dialogCobro.dismiss();
                                if (servicioSocket.getSolicitudEfectivo() != null && servicioSocket.getSolicitudEfectivo().getIdPedido() != 0) {
                                    mostrarEspera("Enviando datos.");
                                    servicioSocket.efectivoCobrarConDriverPedido(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                                } else if (servicioSocket.getSolicitudEfectivo() != null && servicioSocket.getSolicitudEfectivo().getIdSolicitud() != 0) {
                                    mostrarEspera("Enviando datos.");
                                    servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                                } else {
                                    mostrarEspera("Enviando datos.");
                                    servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                                }
                                finalizarTaximetro();
                                totalCarreraCajaTexto = String.valueOf(costo);
                                Log.e("valorCobro6", totalCarreraCajaTexto + "");
                            }
                        });
                        dialogo.setNegativeButton("Corregir", new OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                dialogo1.dismiss();
                            }
                        });
                        if (!isFinishing()) {
                            dialogo.show();
                        }
                    } else {
                        totalCarreraCajaTexto = String.valueOf(costo);
                        Log.e("valorCobro7", totalCarreraCajaTexto + "");

                        dialogCobro.dismiss();
                        if (servicioSocket.getSolicitudEfectivo() != null && servicioSocket.getSolicitudEfectivo().getIdPedido() != 0) {
                            servicioSocket.efectivoCobrarConDriverPedido(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                            mostrarEspera("Enviando datos.");
                        } else if (servicioSocket.getSolicitudEfectivo() != null && servicioSocket.getSolicitudEfectivo().getIdSolicitud() != 0) {
                            servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                            mostrarEspera("Enviando datos.");
                        } else {
                            servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                            mostrarEspera("Enviando datos.");
                        }
                        finalizarTaximetro();
                    }
                }
            });
            if (!isFinishing()) {
                dialogo.show();
            }
        } else {
            if (costo > valorMaximo) {
                DecimalFormat df = new DecimalFormat("####0.00");
                final Builder dialogo = new Builder(MapaBaseActivity.this);
                dialogo.setTitle(R.string.str_alerta_min);
                dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
                dialogo.setCancelable(false);
                dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        dialogCobro.dismiss();
                        totalCarreraCajaTexto = String.valueOf(costo);
                        Log.e("valorCobro7", totalCarreraCajaTexto + "");
                        if (servicioSocket.getSolicitudEfectivo() != null && servicioSocket.getSolicitudEfectivo().getIdPedido() != 0) {
                            servicioSocket.efectivoCobrarConDriverPedido(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                            mostrarEspera("Enviando datos.");
                        } else if (servicioSocket.getSolicitudEfectivo() != null && servicioSocket.getSolicitudEfectivo().getIdSolicitud() != 0) {
                            servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                            mostrarEspera("Enviando datos.");
                        } else {
                            servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                            mostrarEspera("Enviando datos.");
                        }
                        finalizarTaximetro();
                    }
                });
                dialogo.setNegativeButton("Corregir", new OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        dialogo1.dismiss();
                    }
                });
                if (!isFinishing()) {
                    dialogo.show();
                }
            } else {
                totalCarreraCajaTexto = String.valueOf(costo);
                Log.e("valorCobro8", totalCarreraCajaTexto + "");
                dialogCobro.dismiss();
                if (servicioSocket.getSolicitudEfectivo() != null && servicioSocket.getSolicitudEfectivo().getIdPedido() != 0) {
                    servicioSocket.efectivoCobrarConDriverPedido(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                    mostrarEspera("Enviando datos.");
                } else if (servicioSocket.getSolicitudEfectivo() != null && servicioSocket.getSolicitudEfectivo().getIdSolicitud() != 0) {
                    servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                    mostrarEspera("Enviando datos.");
                } else {
                    servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                    mostrarEspera("Enviando datos.");
                }
                finalizarTaximetro();
            }
        }
    }

    @Override
    public void respuestaCobroTarjetCredito(final int tipoError, final int estado,
                                            final String mensaje) {
        isVisibleTarjetaCredito = true;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarEspera();
                if (tipoError != 0) {
                    Log.e("estado19", "estado19");
                    cambiarEstadoOcupado();
                    switch (tipoError) {
                        case 1:
                            mensajeTarjetaCredito(mensaje, 1);
                            break;
                        case 2:
                            mensajeTarjetaCredito(mensaje, 1);
                            break;
                    }
                } else {
                    Log.e("estado20", "estado20");
                    cambiarEstadoOcupado();
                    switch (estado) {
                        case -1:
                            mensajeTarjetaCredito(mensaje, 3);
                            break;
                        case 1:
                            presentar_info_fin_carrera();
                            break;
                        case 2:
                            mensajeTarjetaCredito(mensaje, 2);
                            break;
                        case 3:
                            mensajeTarjetaCredito(mensaje);
                            break;
                    }
                }
            }
        });
    }

    public void mensajeTarjetaCredito(String mensaje) {
        if (dialogMensajeTarjetaCredito != null) {
            if (dialogMensajeTarjetaCredito.isShowing()) {
                return;
            }
        }
        Builder builder = new Builder(MapaBaseActivity.this);
        builder.setMessage(mensaje)
                .setTitle(R.string.str_alerta_min)
                .setPositiveButton(R.string.aceptar, new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        presentarValorCobro();
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Llamar", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        presentarValorCobro();
                        if (ActivityCompat.checkSelfPermission(MapaBaseActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(MapaBaseActivity.this, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL);
                        } else {
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:+593991503740")));
                        }
                        dialog.cancel();
                    }
                });
        dialogMensajeTarjetaCredito = builder.create();
        if (!isFinishing()) {
            dialogMensajeTarjetaCredito.show();
        }

    }

    public void mensajeTarjetaCredito(String mensaje, final int idTipo) {
        if (dialogMensajeTarjetaCredito != null) {
            if (dialogMensajeTarjetaCredito.isShowing()) {
                return;
            }
        }
        Builder builder = new Builder(MapaBaseActivity.this);
        builder.setMessage(mensaje)
                .setTitle(R.string.informacion)
                .setCancelable(false)
                .setPositiveButton(R.string.aceptar, new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        switch (idTipo) {
                            case 1:
                                isVisibleTarjetaCredito = false;
                                presentarValorCobro();
                                dialog.cancel();
                                break;
                            case 2:
                                presentar_info_fin_carrera();
                                dialog.cancel();
                                break;
                            case 3:
                                presentarValorCobro();
                                dialog.cancel();
                                break;
                        }

                    }
                });
        dialogMensajeTarjetaCredito = builder.create();
        if (!isFinishing()) {
            dialogMensajeTarjetaCredito.show();
        }
    }

    public void cobroTarjetaCredito(final double costo, final double saldoKtaxi,
                                    final double propina, final int numeroPasajero, TextView costoCarrera, TextView
                                            numeroPasajeros) {
        if (isObligatorioCostoCarrera) {
            if (costoCarrera.getText().toString().isEmpty()) {
                Toast.makeText(this, "Defina un costo de la carrera.", Toast.LENGTH_LONG).show();
                costoCarrera.setBackgroundResource(R.drawable.style_caja_texto_error);
                return;
            }
            double dbCostoCarrera = Double.parseDouble(costoCarrera.getText().toString().equals(".") ? "0" : costoCarrera.getText().toString());
            if (dbCostoCarrera == 0) {
                Toast.makeText(this, "Defina un costo de la carrera diferente de 0.00", Toast.LENGTH_LONG).show();
                costoCarrera.setBackgroundResource(R.drawable.style_caja_texto_error);
                return;
            }
        }
        if (isObligatorioNumeroPasajeros) {
            if (numeroPasajero == 0) {
                Toast.makeText(this, "Defina un número de pasajeros para continuar.", Toast.LENGTH_LONG).show();
                numeroPasajeros.setBackgroundColor(getResources().getColor(R.color.rojo_bajo));
                return;
            }
        }
        if (costo > valorMaximo) {
            DecimalFormat df = new DecimalFormat("####0.00");
            final Builder dialogo = new Builder(MapaBaseActivity.this);
            dialogo.setTitle(R.string.str_alerta_min);
            dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
            dialogo.setCancelable(false);
            dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    if (solicitudSeleccionada.getTipoFormaPago() == VariablesGlobales.TARJETA_CREDITO_NUEVO) {
                        totalCarreraCajaTexto = String.valueOf(costo);
                        Log.e("valorCobro9", totalCarreraCajaTexto + "");
                        dialogCobro.dismiss();
                        if (solicitudSeleccionada != null) {
                            mostrarEspera("Enviando datos.");
                            servicioSocket.cobroTarjetaCreditoNew(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                        }
                    } else {
                        servicioSocket.setCobroTarjetaCredito(2, "", solicitudSeleccionada);
                        dialogCobro.dismiss();
                        totalCarreraCajaTexto = String.valueOf(costo);
                        Log.e("valorCobro10", totalCarreraCajaTexto + "");
                        if (solicitudSeleccionada.getIdPedido() != 0) {
                            mostrarEspera("Enviando datos.");
                            servicioSocket.cobroTarjetaCredito(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                        } else if (solicitudSeleccionada.getIdSolicitud() != 0) {
                            mostrarEspera("Enviando datos.");
                            servicioSocket.cobroTarjetaCredito(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                        } else {
                            servicioSocket.cobroTarjetaCredito(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                            Log.e("estado35", "estado35");
                            cambiarEstadoLibre();
                        }
                    }

                }
            });
            dialogo.setNegativeButton("Corregir", new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    dialogo1.dismiss();
                }
            });
            if (!isFinishing()) {
                dialogo.show();
            }

        } else {
            if (solicitudSeleccionada.getTipoFormaPago() == VariablesGlobales.TARJETA_CREDITO_NUEVO) {
                totalCarreraCajaTexto = String.valueOf(costo);
                Log.e("valorCobro11", totalCarreraCajaTexto + "");
                dialogCobro.dismiss();
                if (solicitudSeleccionada != null) {
                    mostrarEspera("Enviando datos.");
                    servicioSocket.cobroTarjetaCreditoNew(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                }
            } else {
                totalCarreraCajaTexto = String.valueOf(costo);
                Log.e("valorCobro12", totalCarreraCajaTexto + "");
                dialogCobro.dismiss();
                if (solicitudSeleccionada.getIdPedido() != 0) {
                    servicioSocket.setCobroTarjetaCredito(2, "", solicitudSeleccionada);
                    mostrarEspera("Enviando datos.");
                    servicioSocket.cobroTarjetaCredito(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                } else if (solicitudSeleccionada.getIdSolicitud() != 0) {
                    mostrarEspera("Enviando datos.");
                    servicioSocket.cobroTarjetaCredito(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                } else {
                    servicioSocket.cobroTarjetaCredito(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                    Log.e("estado36", "estado36");
                    cambiarEstadoLibre();
                }
            }

        }
    }

    public void definirPasajero(LinearLayout lyPasajero, TextView tvNumeroPasajero) {
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);

                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 17:
                                if (objetoConfig.getInt("h") == 1) {
                                    String[] tes = sacarPart(objetoConfig.getString("vd"));
                                    if (tes.length == 3) {
                                        if (tes[0].equals("1")) {
                                            isObligatorioCostoCarrera = true;
                                        } else {
                                            isObligatorioCostoCarrera = false;
                                        }
                                        if (tes[1].equals("1")) {
                                            isObligatorioNumeroPasajeros = true;
                                        } else {
                                            isObligatorioNumeroPasajeros = false;
                                        }
                                        tvNumeroPasajero.setText(tes[2]);
                                    }
                                    lyPasajero.setVisibility(View.VISIBLE);
                                } else {
                                    lyPasajero.setVisibility(View.GONE);
                                }
                                break;
                        }
                    } else {
                        lyPasajero.setVisibility(View.GONE);
                    }
                }
            } catch (JSONException e) {
                e.getMessage();
                lyPasajero.setVisibility(View.GONE);
            }
        } else {
            lyPasajero.setVisibility(View.GONE);
        }
    }

    /**
     * Sacar paremetros de configuracion al pagar para s
     * 0 = No obligatorio
     * 1 = Es Obligatorio
     * Costo carrera obligatorio (0,1)
     * Numero de pasajeros obligatorio (0,1)
     * Inicio numero de pasajeros (0,1,...)
     * 1,1,1
     */
    public String[] sacarPart(String datos) {
        return datos.split(",");
    }

    public void cobroVaucher(final double costo, final double saldoKtaxi,
                             final double propina, final int numeroPasajero) {
        if (costo > valorMaximo) {
            DecimalFormat df = new DecimalFormat("####0.00");
            final Builder dialogo = new Builder(MapaBaseActivity.this);
            dialogo.setTitle(R.string.str_alerta_min);
            dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
            dialogo.setCancelable(false);
            dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                        servicioSocket.voucherCobraVaucherConConfirmacion(costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                        dialogCobro.dismiss();
                        progressDialog = new ProgressDialog(MapaBaseActivity.this);
                        progressDialog.setMessage("Esperando confirmación cliente");
                        progressDialog.setIndeterminate(false);
                        progressDialog.setCancelable(false);
                        if (!isFinishing()) {
                            progressDialog.show();
                        }

                        totalCarreraCajaTexto = String.valueOf(costo);
                        Log.e("valorCobro13", totalCarreraCajaTexto + "");
                    } else {
                        Toast.makeText(MapaBaseActivity.this, "Usted no puede realizar el cobro con voucher ya que la solicitud no fue realizada con esta forma de pago.", Toast.LENGTH_LONG).show();
                    }
                }
            });
            dialogo.setNegativeButton("Corregir", new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    dialogo1.dismiss();
                }
            });
            dialogo.show();
        } else {
            if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                servicioSocket.voucherCobraVaucherConConfirmacion(costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                dialogCobro.dismiss();
                progressDialog = new ProgressDialog(MapaBaseActivity.this);
                progressDialog.setMessage("Esperando confirmación cliente");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                if (!isFinishing()) {
                    progressDialog.show();
                }
                totalCarreraCajaTexto = String.valueOf(costo);
                Log.e("valorCobro14", totalCarreraCajaTexto + "");
            } else {
                Toast.makeText(MapaBaseActivity.this, "Usted no puede realizar el cobro con voucher ya que la solicitud no fue realizada con esta forma de pago.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void cobroVaucherPin(final double costo, final String pin, final double saldoKtaxi,
                                final double propina, final int numeroPasajero) {
        if (costo > valorMaximo) {
            DecimalFormat df = new DecimalFormat("####0.00");
            final Builder dialogo = new Builder(MapaBaseActivity.this);
            dialogo.setTitle(R.string.str_alerta_min);
            dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
            dialogo.setCancelable(false);
            dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                        servicioSocket.voucherConPin(Integer.parseInt(pin), costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                        dialogCobro.dismiss();
                        progressDialog = new ProgressDialog(MapaBaseActivity.this);
                        progressDialog.setMessage("Esperando confirmación");
                        progressDialog.setIndeterminate(false);
                        progressDialog.setCancelable(false);
                        if (!isFinishing()) {
                            progressDialog.show();
                        }
                        totalCarreraCajaTexto = String.valueOf(costo);
                        Log.e("valorCobro15", totalCarreraCajaTexto + "");
                    }
                }
            });
            dialogo.setNegativeButton("Corregir", new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    dialogo1.dismiss();
                }
            });
            if (!isFinishing()) {
                dialogo.show();
            }
        } else {
            if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                servicioSocket.voucherConPin(Integer.parseInt(pin), costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                dialogCobro.dismiss();
                progressDialog = new ProgressDialog(MapaBaseActivity.this);
                progressDialog.setMessage("Esperando confirmación cliente");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                if (!isFinishing()) {
                    progressDialog.show();
                }
                totalCarreraCajaTexto = String.valueOf(costo);
                Log.e("valorCobro16", totalCarreraCajaTexto + "");
            }
        }
    }

    public void cobroVaucherCallCenter(final double costo, final String pin,
                                       final double saldoKtaxi, final double propina, final int numeroPasajero) {
        if (costo > valorMaximo) {

            DecimalFormat df = new DecimalFormat("####0.00");
            final Builder dialogo = new Builder(MapaBaseActivity.this);
            dialogo.setTitle(R.string.str_alerta_min);
            dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
            dialogo.setCancelable(false);
            dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {

                    if (servicioSocket.getIdSolicitudSeleccionada() != 0) {

                        servicioSocket.voucherConPinCallCenter(Integer.parseInt(pin), costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                        dialogCobro.dismiss();
                        progressDialog = new ProgressDialog(MapaBaseActivity.this);
                        progressDialog.setMessage("Esperando confirmación");
                        progressDialog.setIndeterminate(false);
                        progressDialog.setCancelable(false);
                        if (!isFinishing()) {
                            progressDialog.show();
                        }
                        totalCarreraCajaTexto = String.valueOf(costo);
                        Log.e("valorCobro17", totalCarreraCajaTexto + "");
                    }
                }
            });
            dialogo.setNegativeButton("Corregir", new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    dialogo1.dismiss();
                }
            });
            if (!isFinishing()) {
                dialogo.show();
            }


        } else {
            if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                servicioSocket.voucherConPinCallCenter(Integer.parseInt(pin), costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                dialogCobro.dismiss();
                progressDialog = new ProgressDialog(MapaBaseActivity.this);
                progressDialog.setMessage("Esperando confirmación cliente");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                if (!isFinishing()) {
                    progressDialog.show();
                }
                totalCarreraCajaTexto = String.valueOf(costo);
                Log.e("valorCobro18", totalCarreraCajaTexto + "");
            }
        }
    }

    public void cobroVoucherClienteUido(final double costo, final double saldoKtaxi,
                                        final double propina, final int numeroPasajero) {
        if (costo > valorMaximo) {
            DecimalFormat df = new DecimalFormat("####0.00");
            final Builder dialogo = new Builder(MapaBaseActivity.this);
            dialogo.setTitle(R.string.str_alerta_min);
            dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
            dialogo.setCancelable(false);
            dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                        servicioSocket.voucherCobraVaucherClienteUido(costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                        dialogCobro.dismiss();
                        progressDialog = new ProgressDialog(MapaBaseActivity.this);
                        progressDialog.setMessage("Esperando confirmación");
                        progressDialog.setIndeterminate(false);
                        progressDialog.setCancelable(false);
                        if (!isFinishing()) {
                            progressDialog.show();
                        }
                        totalCarreraCajaTexto = String.valueOf(costo);
                        Log.e("valorCobro19", totalCarreraCajaTexto + "");
                    }
                }
            });
            dialogo.setNegativeButton("Corregir", new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    dialogo1.dismiss();
                }
            });
            dialogo.show();
        } else {
            if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                servicioSocket.voucherCobraVaucherClienteUido(costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                dialogCobro.dismiss();
                progressDialog = new ProgressDialog(MapaBaseActivity.this);
                progressDialog.setMessage("Esperando confirmación cliente");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                if (!isFinishing()) {
                    progressDialog.show();
                }
                totalCarreraCajaTexto = String.valueOf(costo);
                Log.e("valorCobro20", totalCarreraCajaTexto + "");
            }
        }
    }

    public void cobroDineroElectronico(final double costo, final String usuario,
                                       final String pin, final double saldoKtaxi, final double propina, final int numeroPasajero) {
        if (costo > valorMaximo) {
            DecimalFormat df = new DecimalFormat("####0.00");
            final Builder dialogo = new Builder(MapaBaseActivity.this);
            dialogo.setTitle(R.string.str_alerta_min);
            dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
            dialogo.setCancelable(false);
            dialogo.setPositiveButton(R.string.aceptar, new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                        servicioSocket.DineroElectronicoCobrar(costo, servicioSocket.getIdSolicitudSeleccionada(), false, usuario, pin, saldoKtaxi, propina, numeroPasajero);
                        dialogCobro.dismiss();
                        presentarDialogoCobro("Esperando confirmación");
                        totalCarreraCajaTexto = String.valueOf(costo);
                        Log.e("valorCobro21", totalCarreraCajaTexto + "");
                    }
                }
            });
            dialogo.setNegativeButton("Corregir", new OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    dialogo1.dismiss();
                }
            });
            if (!isFinishing()) {
                dialogo.show();
            }

        } else {
            if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                servicioSocket.DineroElectronicoCobrar(costo, servicioSocket.getIdSolicitudSeleccionada(), false, usuario, pin, saldoKtaxi, propina, numeroPasajero);
                dialogCobro.dismiss();
                presentarDialogoCobro("Esperando confirmación");
                totalCarreraCajaTexto = String.valueOf(costo);
                Log.e("valorCobro22", totalCarreraCajaTexto + "");
            }
        }
    }

    public void definirHindCobro(EditText etHind) {
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 12:
                                if (objetoConfig.getInt("h") == 1) {
                                    String[] data = objetoConfig.getString("vd").split(",");
                                    if (utilidades.compararHorasFin(data[2], data[3])) {
                                        etHind.setHint("Ejm: " + data[0] + " " + objetoConfig.getString("nb"));
                                        valorMaximo = Double.parseDouble(data[0]) * 10;
                                        return;
                                    } else {
                                        etHind.setHint("Ejm: " + data[1] + " " + objetoConfig.getString("nb"));
                                        valorMaximo = Double.parseDouble(data[1]) * 10;
                                        return;
                                    }
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                ExtraLog.Log(TAG, e.getMessage());
            }
        }
        if (utilidades.compararHorasFin(null, null)) {
            etHind.setHint("Ejm: 1.25 Usd");
        } else {
            etHind.setHint("Ejm: 1.40 Usd");
        }
    }

    @Override
    public void getDineroSaldoDineroElectronico(final String valor, final String usuario,
                                                final String pin) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dialogCobro != null) {
                    if (dialogCobro.isShowing()) {
                        dialogCobro.dismiss();
                    }
                }
                if (valor != null) {
                    try {
                        final JSONObject object = new JSONObject(valor);
                        if (object.has("e")) {
                            switch (object.getInt("e")) {
                                case 1:
                                    if (object.has("s")) {
                                        if (progressDialog != null && progressDialog.isShowing()) {
                                            servicioSocket.guardarSaldo(Float.parseFloat(object.getString("s")));
                                            progressDialog.dismiss();
                                            presentarDialogoCobro(object.getString("m") + object.getString("s"));
                                        }
                                    }
                                    break;
                                case -1:
                                    if (progressDialog != null) {
                                        if (progressDialog.isShowing()) {
                                            progressDialog.dismiss();
                                        }
                                    }
                                    if (object.has("m")) {
                                        Builder builder = new Builder(MapaBaseActivity.this);
                                        builder.setMessage(object.getString("m"))
                                                .setTitle(R.string.informacion)
                                                .setCancelable(false)
                                                .setPositiveButton(R.string.aceptar, new OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                        presentarValorCobro();
                                                        utilidades.customToast(MapaBaseActivity.this, "Cobre en efectivo");
                                                    }
                                                });
                                        if (!isFinishing()) {
                                            builder.show();
                                        }
                                    }
                                    break;
                            }
                        } else {
                            if (progressDialog != null) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                            }
                            if (object.has("m")) {
                                Builder builder = new Builder(MapaBaseActivity.this);
                                builder.setMessage(object.getString("m"))
                                        .setTitle(R.string.informacion)
                                        .setCancelable(false)
                                        .setPositiveButton(R.string.aceptar, new OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                presentarValorCobro();
                                                utilidades.customToast(MapaBaseActivity.this, "Cobre en efectivo");
                                            }
                                        });
                                if (!isFinishing()) {
                                    builder.show();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void reenvioDineroElectronico(String usuario, String pin) {
        servicioSocket.DineroElectronicoCobrar(costo, servicioSocket.getIdSolicitudSeleccionada(), true, usuario, pin, saldoKtaxi, propina, numeroPasajeros);
    }

    public void preguntarEstadobateria() {
        PowerManager powerManager = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (powerManager.isPowerSaveMode()) {
                Builder builder = new Builder(MapaBaseActivity.this);
                builder.setMessage("Usted se encuentra en modo ahorro de energía, si usa este modo " +
                        "puede producir que la aplicación no trabaje de forma eficiente.")
                        .setTitle(R.string.str_alerta_min)
                        .setPositiveButton("Cambiar", new OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                utilidades.abrirIntentBatery(MapaBaseActivity.this);
                            }
                        })
                        .setNegativeButton("Ignorar", new OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                if (!isFinishing()) {
                    builder.show();
                }
            }
        }
    }

    @Override
    public void presentarDialogoCobro(String texto) {
        if (dialogCobro != null) {
            if (dialogCobro.isShowing()) {
                dialogCobro.dismiss();
            }
        }

        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        }

        progressDialog = new ProgressDialog(MapaBaseActivity.this);
        if (texto != null) {
            progressDialog.setMessage(texto);
        } else {
            progressDialog.setMessage("ESPERE...");
        }
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        if (!isFinishing()) {
            progressDialog.show();
        }


    }

    public Handler procesoParaPresentarDialogo;

    public void presentar_info_fin_carrera() {
        if (dialogFinCarrera != null && dialogFinCarrera.isShowing()) return;
        if (servicioSocket.solicitudSeleccionada != null) {
            solicitudSeleccionada = servicioSocket.solicitudSeleccionada;
        }
        if (solicitudSeleccionada == null) {
            servicioSocket.setCobroTarjetaCredito(0, "", null);
            servicioSocket.setCobroEfectivo(0, "", null);
            servicioSocket.isEstadoAbordo = false;
            servicioSocket.isMensajeAbordo = false;
            servicioSocket.activaEnNuevaSolicitud();
            servicioSocket.setEnviarComprarPedido(false);
            servicioSocket.setEnviarllevandoPedido(false);
            servicioSocket.resetIdEnvioCalificacion();
            servicioSocket.borrarCarreraSeleccionada();
            servicioSocket.estadoSolicitud = 0;
            servicioSocket.estadoPedido = 0;
            btnClienteAbordo.setEnabled(false);
            servicioSocket.solicitudSeleccionada = null;
            deshabilitarComponentesCarrera();
            servicioSocket.razonCancelada = -1;
            Log.e("estado37", "estado37");
            cambiarEstadoLibre();
            return;
        }
        Log.e("estado21", "estado21");
        cambiarEstadoOcupado();
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        final View viewInformacion = inflater.inflate(R.layout.dialog_calificar_cliente, null, false);
        final Button btnAceptar = viewInformacion.findViewById(R.id.btnAceptar);
        final Builder builder = new Builder(MapaBaseActivity.this);
        final EditText et_comentario = viewInformacion.findViewById(R.id.et_comentario);
        final TextView tvTipoCalificacion = viewInformacion.findViewById(R.id.tv_tipo_calificacion);
        final RatingBar ratingBar = viewInformacion.findViewById(R.id.rating_bar);
        final int[] calificacion = {0};
        cambiarTextEnBaseCalificacion(0, tvTipoCalificacion);
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                pararHiloCancelar();
                try {
                    Hashtable<Integer, Integer> contenedor = new Hashtable();
                    contenedor.put(0, 5);
                    contenedor.put(1, 5);
                    contenedor.put(2, 4);
                    contenedor.put(3, 3);
                    contenedor.put(4, 2);
                    contenedor.put(5, 1);
                    contenedor.put(5, 1);
                    calificacion[0] = contenedor.get((int) rating);
                    cambiarTextEnBaseCalificacion((int) rating, tvTipoCalificacion);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    calificacion[0] = 0;
                }
            }
        });
        builder.setView(viewInformacion);
        builder.setCancelable(false);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pararHiloCancelar();
                dialogFinCarrera.dismiss();
                servicioSocket.setTipoFormaPago(0);
                if (servicioSocket.getIdPedidoSeleccionado() != 0) {
                    servicioSocket.calificaConductorPedido(calificacion[0], et_comentario.getText().toString(), solicitudSeleccionada.getIdPedido());
                } else {
                    servicioSocket.calificaConductorSolicitud(calificacion[0], et_comentario.getText().toString(), solicitudSeleccionada.getIdSolicitud());
                }
                costo = 0;
                Log.e("estado38", "estado38");
                cambiarEstadoLibre();
                btnClienteAbordo.setEnabled(false);
                totalCarrera = null;
                totalCarreraTemporal = null;
                deshabilitarComponentesCarrera();
                servicioSocket.activaEnNuevaSolicitud();
                comprobarMostrarDialogoFinalizar();
                solicitudSeleccionada = null;
                servicioSocket.solicitudSeleccionada = null;
                procesoParaPresentarDialogo = null;
                borrarCarrera();
            }
        });
        if (!isFinishing()) {
            dialogFinCarrera = builder.show();
        } else {
            if (procesoParaPresentarDialogo == null) {
                procesoParaPresentarDialogo = new Handler();
                procesoParaPresentarDialogo.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        ExtraLog.Log(TAG, "run: ENRTO AL HILO PARA ABROR NUEVAMENTE");
                        presentar_info_fin_carrera();
                    }
                }, 2000);
            }
        }
    }

    public void pararHiloCancelar() {
        if (handlerCancelarSolicitud != null && runnableCancelarSolicitud != null) {
            handlerCancelarSolicitud.removeCallbacks(runnableCancelarSolicitud);
            handlerCancelarSolicitud = null;
            runnableCancelarSolicitud = null;
        }
    }

    public void cambiarTextEnBaseCalificacion(int numero, TextView tvTitle) {
        switch (numero) {
            case 1:
                tvTitle.setText("-- Malo --");
                break;
            case 2:
                tvTitle.setText("-- Regular --");
                break;
            case 3:
                tvTitle.setText("-- Bueno --");
                break;
            case 4:
                tvTitle.setText("-- Muy bueno --");
                break;
            case 5:
                tvTitle.setText("-- Excelente --");
                break;
            default:
                tvTitle.setText("-- Sin calificar --");
        }
    }

    public void comprobarMostrarDialogoFinalizar() {
        if (servicioSocket.getMensajeDeuda() != null &&
                servicioSocket.getMensajeDeuda().getL() != null &&
                servicioSocket.getMensajeDeuda().getL().getP() != null) {
            if (servicioSocket.getMensajeDeuda().getL().getP().getRF() == 1) {
                servicioSocket.reproducirTextAudio.speak(servicioSocket.getMensajeDeuda().getL().getP().getMs());
            }
            switch (servicioSocket.getMensajeDeuda().getL().getP().getNF()) {
                case 2:
                    comprobarDialogoCobro();
                    servicioSocket.setMensajeDeudaFinCarrera(true);
                    dialogoMensajeCobro = createDialogoMensaje(servicioSocket.getMensajeDeuda().getL().getP().getMs(), true, false, dialogoMensajeCobro, true);
                    if (!isFinishing()) {
                        dialogoMensajeCobro.show();
                    }

                    break;
                case 3:
                    comprobarDialogoCobro();
                    dialogoMensajeCobro = createDialogoMensaje(servicioSocket.getMensajeDeuda().getL().getP().getMs(), true, false, dialogoMensajeCobro, true);
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (dialogoMensajeCobro != null) {
                                if (dialogoMensajeCobro.isShowing()) {
                                    try {
                                        servicioSocket.setMensajeDeudaAbordo(null);
                                        dialogoMensajeCobro.dismiss();
                                    } catch (IllegalArgumentException e) {
                                        e.getMessage();
                                    }

                                }
                            }
                        }
                    }, servicioSocket.getMensajeDeuda().getL().getP().getTs() * 1000);
                    if (!isFinishing()) {
                        dialogoMensajeCobro.show();
                    }

                    break;
                default:
                    servicioSocket.setMensajeDeudaAbordo(null);
                    break;
            }
        }
    }

    public void finalizarTaximetro() {
        int idTaximetro = spEstadobtn.getInt(VariablesGlobales.PARAMETRO_TAXIMETRO_TEMPORAL, 0);
        SharedPreferences.Editor editor = spEstadobtn.edit();
        DatosEnvioTaximetro datosEnvioTaximetro = null;
        if (DatosEnvioTaximetro.getAll() != null && DatosEnvioTaximetro.getAll().size() > 0) {
            datosEnvioTaximetro = DatosEnvioTaximetro.getAll().get(0);
            totalCarrera = datosEnvioTaximetro.getTotalCarrera();
            Log.e("valorCobro23", totalCarreraCajaTexto + "");
        }
        if (datosEnvioTaximetro != null && idTaximetro != 0) {
            if (mapaPresenter != null) {
                if (locacionDefault != null) {
                    mapaPresenter.finalizarTaximetro(
                            idTaximetro,
                            servicioSocket.getIdSolicitudSeleccionada(),
                            locacionDefault.getLatitude(),
                            locacionDefault.getLongitude(),
                            Double.parseDouble(datosEnvioTaximetro.getTotalCarrera() != null ? datosEnvioTaximetro.getTotalCarrera() : "0"),
                            Double.parseDouble(totalCarreraCajaTexto),
                            datosEnvioTaximetro.getHoraInicio(),
                            datosEnvioTaximetro.getHoraFin(),
                            datosEnvioTaximetro.getTarifaArranque(),
                            datosEnvioTaximetro.getTiempoTotal(),
                            datosEnvioTaximetro.getTiempoEspera(),
                            datosEnvioTaximetro.getValorTiempo(),
                            datosEnvioTaximetro.getDistanciaRecorrida(),
                            datosEnvioTaximetro.getValorDistancia());
                }
            }

            DatosEnvioTaximetro.deleteAll();
            editor.putInt(VariablesGlobales.PARAMETRO_TAXIMETRO_TEMPORAL, 0);
            editor.apply();
        }
    }

    /**
     * Cambia el estado a libre de la aplicación
     */
    @Override
    public void cambiarEstadoLibre() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SharedPreferences.Editor editor = spEstadobtn.edit();
                    editor.putBoolean(VariablesGlobales.ESTADO_BTN, true);
                    editor.apply();
                    clearPantallaEncendido();
                    btnEstadoCarrera.setTag(0);
                    numeroSolicitudes.setEnabled(true);
                    btnNumSolicitudes.setEnabled(true);
                    btnEstadoCarrera.setBackgroundResource(R.drawable.libre_loja);
                    isConsulta = false;
                    countConsulta = 0;
                    int idTaximetro = spEstadobtn.getInt(VariablesGlobales.PARAMETRO_TAXIMETRO_TEMPORAL, 0);
                    totalCarrera = null;
                    DatosEnvioTaximetro datosEnvioTaximetro = null;
                    if (DatosEnvioTaximetro.getAll() != null && DatosEnvioTaximetro.getAll().size() > 0) {
                        datosEnvioTaximetro = DatosEnvioTaximetro.getAll().get(0);
                        totalCarrera = datosEnvioTaximetro.getTotalCarrera();
                        Log.e("valorCobro24", totalCarrera + "");
                    }
                    if (mBound) {
                        if (totalCarrera != null && idTaximetro != 0) {
                            Log.e("valorCobro25", totalCarreraCajaTexto + "");
                            assert datosEnvioTaximetro != null;
                            mapaPresenter.finalizarTaximetro(
                                    idTaximetro,
                                    servicioSocket.getIdSolicitudSeleccionada(),
                                    locacionDefault.getLatitude(),
                                    locacionDefault.getLongitude(),

                                    Double.parseDouble(totalCarrera),
                                    Double.parseDouble(totalCarreraCajaTexto),
                                    datosEnvioTaximetro.getHoraInicio(),
                                    datosEnvioTaximetro.getHoraFin(),
                                    datosEnvioTaximetro.getTarifaArranque(),
                                    datosEnvioTaximetro.getTiempoTotal(),
                                    datosEnvioTaximetro.getTiempoEspera(),
                                    datosEnvioTaximetro.getValorTiempo(),
                                    datosEnvioTaximetro.getDistanciaRecorrida(),
                                    datosEnvioTaximetro.getValorDistancia());
                            DatosEnvioTaximetro.deleteAll();
                            editor.putInt(VariablesGlobales.PARAMETRO_TAXIMETRO_TEMPORAL, 0);
                            editor.apply();
                        }
                        servicioSocket.iscreateVideoDialogo = false;
                        servicioSocket.isCambioLibreConsultado = false;
                        servicioSocket.activaEnNuevaSolicitud();
                        servicioSocket.consultarSolicitudesEntrantes();
                        servicioSocket.retomarCarreras();
                        servicioSocket.estadoDelTaxi(1);
                        servicioSocket.estadoBotonDelTaxi(0);
                        servicioSocket.isEstadoAbordo = false;
                        servicioSocket.isEstadoBoton = true;
                    }
                }
            });
        } catch (NullPointerException e) {
            ExtraLog.Log(TAG, "cambiarEstadoLibre: " + e.getMessage());
        }

    }

    /**
     * Cambia ha estado ocupado la plicación
     */
    public void cambiarEstadoOcupado() {
        try {
            SharedPreferences.Editor editor = spEstadobtn.edit();
            editor.putBoolean(VariablesGlobales.ESTADO_BTN, false);
            editor.apply();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cambiarPantallaEncendido();
                    btnEstadoCarrera.setBackgroundResource(R.drawable.ocupado_loja);
                    btnEstadoCarrera.setTag(1);
                    eliminarMarcadoresSolicitudesBoxYOpen();
                    numeroSolicitudes.setText("0");
                    numeroSolicitudes.setEnabled(false);
                    btnNumSolicitudes.setEnabled(false);
                    numeroSolicitudes.setText("0");
                    if (mBound) {
                        servicioSocket.isEstadoBoton = false;
                        servicioSocket.desactivaEnNuevaSolicitud();
                        servicioSocket.estadoDelTaxi(6);
                        if (servicioSocket.estadoBoton <= 1) {
                            servicioSocket.estadoBotonDelTaxi(1);
                        }
                        cargarCambioTaximetro();
                    }
                }
            });
        } catch (NullPointerException e) {
            e.getMessage();
        }
    }

    public void cargarCambioTaximetro() {
        isConsulta = true;
        if (!servicioSocket.isCambioLibreConsultado && isConsulta && !isRun && !servicioSocket.iscreateVideoDialogo) {
            servicioSocket.isCambiarLibre = false;
            servicioSocket.controlCambioLibre();
            cosultarAlerta();
        }
    }

    public void cambiarPantallaEncendido() {
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
    }

    public void clearPantallaEncendido() {
        win.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void cosultarAlerta() {
        isRun = true;
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isConsulta) {
                    if (mBound) {
                        if (solicitudSeleccionada != null) {
                            if (!solicitudSeleccionada.isPedido()) { // CONSULTA DE ALERTAS ESTO SE PUSO POR QUE NO DA EL AUDIO CUANDO SE ESTA EN PEDIDOS.
                                servicioSocket.consultaMensaje(1);
                            }
                        } else {
                            servicioSocket.consultaMensaje(2);
                        }
                        isConsulta = false;
                        countConsulta = 0;
                    }
                }
                isRun = false;
            }
        }, 5000);
    }

    public void presentarEventualidades() {
        if (servicioSocket.solicitudSeleccionada != null) {
            if (servicioSocket.solicitudSeleccionada.getIdPedido() != 0) {
                if (servicioSocket.isEnviarComprarPedido()) {
                    startActivityForResult(new Intent(this, CancelarSolicitudActivity.class), 3);
                } else {
                    utilidades.mostrarMensajeCorto(this, "Usted está atendiendo un pedido aún, no puede cancelar");
                }
            } else {
                if (servicioSocket.solicitudSeleccionada.isQuiosco()) {
                    if (utilidades.calculationByDistance(new LatLng(locacionDefault.getLatitude(), locacionDefault.getLongitude()),
                            new LatLng(servicioSocket.solicitudSeleccionada.getLatitud(),
                                    servicioSocket.solicitudSeleccionada.getLongitud())) <= 0.2) {
                        Intent intent = new Intent(this, CancelarSolicitudActivity.class);
                        intent.putExtra("isQuiosko", true);
                        startActivityForResult(intent, 3);
                    } else {
                        if (!ServicioSockets.isBanderaRun) {
                            Intent intent = new Intent(this, CancelarSolicitudActivity.class);
                            intent.putExtra("isQuiosko", true);
                            startActivityForResult(intent, 3);
                        } else {
                            utilidades.customToast(this, "Usted no puede cancelar esta solicitud hasta que llegue a recojer al cliente, ya que es una solicitud de quiosko.");
                        }

                    }
                } else {
                    Intent intent = new Intent(this, CancelarSolicitudActivity.class);
                    intent.putExtra("isQuiosko", false);
                    startActivityForResult(intent, 3);
                }

            }
        }


    }

    public SupportMapFragment mapFragmentDestino;

    public void cargarDestino(View viewInformacion, Solicitud solicitud) {
//        mapFragmentDestino = ((SupportMapFragment) ((AppCompatActivity) activity).getSupportFragmentManager().findFragmentById(R.id.map_destino));
        LinearLayout lyDestino = viewInformacion.findViewById(R.id.ly_destino_ld);
        if (solicitud.getlD() != null && solicitud.getlD().size() > 0) {
            lyDestino.setVisibility(View.VISIBLE);
            Destino destino = solicitud.getlD().get(0);
            TextView tvDestinoBarrio = viewInformacion.findViewById(R.id.tv_destino_barrio);
            TextView tvDestinoCallePrincipal = viewInformacion.findViewById(R.id.tv_destino_calle_principal);
            TextView tvDestinoCalleSecundaria = viewInformacion.findViewById(R.id.tv_destino_barrio_calle_secundaria);
            TextView tvDestinoReferencia = viewInformacion.findViewById(R.id.tv_destino_refencia);
            TextView tvCostoDestino = viewInformacion.findViewById(R.id.tv_destino_costo);
            TextView tvTiempoDestino = viewInformacion.findViewById(R.id.tv_destino_tiempo);
            TextView tvDistanciaDestino = viewInformacion.findViewById(R.id.tv_destino_distancia);
            tvCostoDestino.setText(destino.getDesC().toString());
            tvTiempoDestino.setText(destino.getDesT());
            tvDistanciaDestino.setText(destino.getDesDis().toString().concat(" Km"));
            String barrioD = destino.getDesBar().isEmpty() ? "" : ("Barrio. ".concat(destino.getDesBar()));
            String cpD = destino.getDesCp().isEmpty() ? "" : (" CP. ".concat(destino.getDesCp()));
            String csD = destino.getDesCs().isEmpty() ? "" : (" CS. ".concat(destino.getDesCs()));
            String rfD = destino.getDesRef().isEmpty() ? "" : (" RF. ".concat(destino.getDesRef()));
            String destinoText = barrioD.concat(cpD).concat(csD).concat(rfD);
            tvDestinoBarrio.setText(destinoText);
            tvDestinoCallePrincipal.setText(destino.getDesCp());
            tvDestinoCalleSecundaria.setText(destino.getDesCs());
            tvDestinoReferencia.setText(destino.getDesRef());
//            if (mapFragmentDestino != null) {
//                mapFragmentDestino.getMapAsync(new OnMapReadyCallback() {
//                    @Override
//                    public void onMapReady(GoogleMap map) {
//                        com.google.android.gms.maps.model.LatLng locationDestino = new com.google.android.gms.maps.model.LatLng(destino.getLtD(), destino.getLgD());
//                        com.google.android.gms.maps.model.LatLng locationCliente = new com.google.android.gms.maps.model.LatLng(solicitud.getLatitud(), solicitud.getLongitud());
//                        agregarMarcador(map, locationDestino, false, solicitud.getBarrio());
//                        agregarMarcador(map, locationCliente, true, solicitud.getNombresCliente());
//                        centrarMarket(map, solicitud);
//                        map.getUiSettings().setCompassEnabled(false);
//                        map.getUiSettings().setZoomControlsEnabled(false);
//                        map.getUiSettings().setMapToolbarEnabled(false);
//
//                    }
//                });
//            } else {
//                Toast.makeText(this, "Error - Map Fragment was null!!", Toast.LENGTH_SHORT).show();
//            }


        } else {
            lyDestino.setVisibility(View.GONE);
        }
        viewInformacion.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
            }

            @Override
            public void onViewDetachedFromWindow(View v) {

            }
        });

    }

    public void agregarMarcador(GoogleMap map, com.google.android.gms.maps.model.LatLng locationCliente, boolean isCliente, String desBar) {
        MarkerOptions mapketOption = new MarkerOptions()
                .position(locationCliente)
                .title("Destino")
                .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(),
                        isCliente ? R.mipmap.pin_usuario_mini : R.mipmap.pin_destino_mini)))
                .snippet(isCliente ? "Barrio: ".concat(desBar) : "Destino: ".concat(desBar));
        map.addMarker(mapketOption);
    }


    public void centrarMarket(GoogleMap miMapDialog, Solicitud solicitudSeleccionada) {
        if (miMapDialog != null && solicitudSeleccionada.getlD() != null && solicitudSeleccionada.getlD().size() > 0) {
            Destino destino = solicitudSeleccionada.getlD().get(0);
            com.google.android.gms.maps.model.LatLng latLng = new com.google.android.gms.maps.model.LatLng(destino.getLtD(), destino.getLgD());
            miMapDialog.animateCamera(com.google.android.gms.maps.CameraUpdateFactory.newCameraPosition(
                    new com.google.android.gms.maps.model.CameraPosition.Builder()
                            .target(latLng)
                            .zoom(12)
                            .tilt(0)
                            .build()));
        }
    }

    public void seleccionarTiempo() {
        if (solicitudSeleccionada != null) {
            if (!solicitudSeleccionada.isPedido()) {
                if (utilidades.isVerificarDialogo(dialogoTiempo)) {
                    if (mBound) {
                        final ArrayList<ModeloItemTiempos> itemTiemposList = new ArrayList<>();
                        final int[] arrayData;
                        if (solicitudSeleccionada.getTiempos().size() == 0) {
                            arrayData = new int[4];
                            itemTiemposList.add(new ModeloItemTiempos("1 minuto"));
                            itemTiemposList.add(new ModeloItemTiempos("3 minutos"));
                            itemTiemposList.add(new ModeloItemTiempos("5 minutos"));
                            itemTiemposList.add(new ModeloItemTiempos("7 minutos"));
                            arrayData[0] = 1;
                            arrayData[1] = 3;
                            arrayData[2] = 5;
                            arrayData[3] = 7;
                        } else {
                            arrayData = new int[solicitudSeleccionada.getTiempos().size()];
                            for (int i = 0; i < solicitudSeleccionada.getTiempos().size(); i++) {
                                if (solicitudSeleccionada.getTiempos().get(i) == 1) {
                                    itemTiemposList.add(new ModeloItemTiempos(solicitudSeleccionada.getTiempos().get(i) + " minuto"));
                                } else {
                                    itemTiemposList.add(new ModeloItemTiempos(solicitudSeleccionada.getTiempos().get(i) + " minutos"));
                                }
                                arrayData[i] = solicitudSeleccionada.getTiempos().get(i);
                            }
                        }
                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        final View viewInformacion = inflater.inflate(R.layout.item_seleccionar_tiempos, null, false);
                        cargarDestino(viewInformacion, solicitudSeleccionada);
                        LinearLayout linearColor = viewInformacion.findViewById(R.id.linearColor);
                        TextView tvServicio = viewInformacion.findViewById(R.id.tv_servicio);
                        utilidades.definirNombreServicio(solicitudSeleccionada, tvServicio);
                        if (solicitudSeleccionada.getColor() != null) {
                            linearColor.setBackgroundColor(Color.parseColor(solicitudSeleccionada.getColor()));
                        } else {
                            linearColor.setBackgroundColor(Color.parseColor("#dddddd"));
                        }
                        ImageView imageEstadoGps = viewInformacion.findViewById(R.id.img_estado_gps);
                        if (RespuestaConfiguracion.isActivarIconoEstadoGpsSolicitud(this)) {
                            imageEstadoGps.setVisibility(View.VISIBLE);
                            imageEstadoGps.setImageResource(utilidades.iconEstadoGpsEnSolicitud(solicitudSeleccionada.getConP()));
                        } else {
                            imageEstadoGps.setVisibility(View.GONE);
                        }
                        ScrollView rlColor = viewInformacion.findViewById(R.id.srcv_fondo);
                        ImageView imageFormaPago = viewInformacion.findViewById(R.id.imv_icon);
                        imageFormaPago.setImageResource(utilidades.iconFormaPagoSolicitud(solicitudSeleccionada.getTipoFormaPago()));
                        ImageView tipoSolicitud = viewInformacion.findViewById(R.id.btnInfoMapa);
                        if (!solicitudSeleccionada.isPedido()) {
                            tipoSolicitud.setImageResource(utilidades.iconTipoSolitud(0));
                        } else {
                            tipoSolicitud.setImageResource(utilidades.iconTipoSolitud(solicitudSeleccionada.getT()));
                        }
                        rlColor.setBackgroundDrawable(utilidades.drawableFondoSolicitud(solicitudSeleccionada.isQuiosco(),
                                solicitudSeleccionada.getTipoFormaPago(), solicitudSeleccionada.isPedido(), solicitudSeleccionada.getT(), this));
                        ImageButton buttonCancelar = viewInformacion.findViewById(R.id.accion_cerrar);
                        buttonCancelar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                solicitudSeleccionada = null;
                                btnClienteAbordo.setEnabled(false);
                                servicioSocket.borrarCarreraSeleccionada();
                                servicioSocket.estadoBotonDelTaxi(0);
                                utilidades.neutralizarDialogo(dialogoTiempo);
                            }
                        });
                        TextView tvPropina = (TextView) viewInformacion.findViewById(R.id.tv_propina);
                        if (solicitudSeleccionada != null && solicitudSeleccionada.getPropina() != 0) {
                            tvPropina.setVisibility(View.VISIBLE);
                            tvPropina.setText("Propina: ".concat(String.valueOf(solicitudSeleccionada.getPropina())));
                        } else {
                            tvPropina.setVisibility(View.GONE);
                        }
                        TextView tvSaldoKtaxi = (TextView) viewInformacion.findViewById(R.id.tv_saldo_ktaxi);
                        tvSaldoKtaxi.setVisibility(View.GONE);
                        if (solicitudSeleccionada != null && solicitudSeleccionada.getSaldo() != 0) {
                            tvSaldoKtaxi.setVisibility(View.GONE);
                            tvSaldoKtaxi.setText("Saldo ktaxi: ".concat(String.valueOf(solicitudSeleccionada.getSaldo())));
                        } else {
                            tvSaldoKtaxi.setVisibility(View.GONE);
                        }
                        final EditText et_cobro_carrera = (EditText) viewInformacion.findViewById(R.id.tv_cobro_carrera);
                        et_cobro_carrera.setVisibility(View.GONE);
                        boolean isCobro = false;
                        final boolean isCobroFin = isCobro;
                        TextView tvBarrio = viewInformacion.findViewById(R.id.tv_barrio);
                        if (utilidades.ocularMostrarCampos(solicitudSeleccionada.getBarrio())) {
                            tvBarrio.setVisibility(View.VISIBLE);
                            tvBarrio.setText(solicitudSeleccionada.getBarrio());
                        } else {
                            tvBarrio.setVisibility(View.GONE);
                        }
                        TextView tvCallePrincipal = viewInformacion.findViewById(R.id.tv_calle_principal);
                        if (utilidades.ocularMostrarCampos(solicitudSeleccionada.getCallePrincipal())) {
                            tvCallePrincipal.setVisibility(View.VISIBLE);
                            tvCallePrincipal.setText(solicitudSeleccionada.getCallePrincipal());
                        } else {
                            tvCallePrincipal.setVisibility(View.GONE);
                        }
                        TextView tvCalleSecundaria = viewInformacion.findViewById(R.id.tv_calle_secundaria);
                        if (utilidades.ocularMostrarCampos(solicitudSeleccionada.getCalleSecundaria())) {
                            tvCalleSecundaria.setVisibility(View.VISIBLE);
                            tvCalleSecundaria.setText(solicitudSeleccionada.getCalleSecundaria());
                        } else {
                            tvCalleSecundaria.setVisibility(View.GONE);
                        }
                        ((TextView) viewInformacion.findViewById(R.id.tv_distancia_tiempo)).setText(utilidades.tiempoDistancia(solicitudSeleccionada));
                        final Button buttonIgnorar = viewInformacion.findViewById(R.id.btn_ignorar);
                        final GridViewWithHeaderAndFooter grid = viewInformacion.findViewById(R.id.grid_time);
                        grid.setAdapter(new GridAdapterSeleccionarTiempo(this, itemTiemposList));
                        final Button btnEnvioTiempo = viewInformacion.findViewById(R.id.btn_envio_tiempo);
                        getTiempo = arrayData[arrayData.length - 1];
                        getTiempo = getTiempo + 1;
                        btnEnvioTiempo.setText(getTiempo + " minutos");
                        viewInformacion.findViewById(R.id.img_btn_mas).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (getTiempo >= arrayData[arrayData.length - 1] + 30) {
                                    utilidades.mostrarMensajeCorto(MapaBaseActivity.this, "No puede exceder el límite máximo");
                                } else {
                                    getTiempo = getTiempo + 1;
                                    btnEnvioTiempo.setText(getTiempo + " minutos");
                                }

                            }
                        });
                        viewInformacion.findViewById(R.id.img_btn_menos).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (getTiempo <= arrayData[0]) {
                                    utilidades.mostrarMensajeCorto(MapaBaseActivity.this, "No puede exceder el límite mínimo");
                                } else {
                                    getTiempo = getTiempo - 1;
                                    btnEnvioTiempo.setText(getTiempo + " minutos");
                                }

                            }
                        });


                        btnEnvioTiempo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (solicitudSeleccionada != null) {
                                    servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdSolicitud(), 1);
                                    servicioSocket.enviarTiempo(getTiempo, 0);
                                    servicioSocket.estadoBotonDelTaxi(2);
                                    mostrarDialogoEspera(getString(R.string.esperando_confirmacion));

                                } else {
                                    solicitudSeleccionada = null;
                                    btnClienteAbordo.setEnabled(false);
                                    servicioSocket.borrarCarreraSeleccionada();
                                    utilidades.customToast(MapaBaseActivity.this, getString(R.string.solicitud_atendida));
                                    Log.e("estado39", "estado39");
                                    cambiarEstadoLibre();
                                }
                                utilidades.neutralizarDialogo(dialogoTiempo);

                            }
                        });
                        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                if (!isCobroFin) {
                                    if (solicitudSeleccionada != null) {
                                        servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdSolicitud(), 1);
                                        int selectedPosition = position;
                                        servicioSocket.enviarTiempo(arrayData[selectedPosition], 0);
                                        servicioSocket.estadoBotonDelTaxi(2);
                                        mostrarDialogoEspera(getString(R.string.esperando_confirmacion));

                                    } else {
                                        solicitudSeleccionada = null;
                                        btnClienteAbordo.setEnabled(false);
                                        servicioSocket.borrarCarreraSeleccionada();
                                        utilidades.customToast(MapaBaseActivity.this, getString(R.string.solicitud_atendida));
                                        Log.e("estado40", "estado40");
                                        cambiarEstadoLibre();
                                    }
                                    utilidades.neutralizarDialogo(dialogoTiempo);
                                } else {
                                    if (et_cobro_carrera.getText().length() >= 1) {
                                        double cobroEnviar;
                                        try {
                                            cobroEnviar = Double.parseDouble(et_cobro_carrera.getText().toString());
                                        } catch (NumberFormatException ex) {
                                            utilidades.customToastCorto(MapaBaseActivity.this, "El valor ingresado no es valido");
                                            return;
                                        }
                                        if (solicitudSeleccionada != null) {
                                            servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdSolicitud(), 1);
                                            int selectedPosition = position;
                                            servicioSocket.enviarTiempo(arrayData[selectedPosition], cobroEnviar);
                                            servicioSocket.estadoBotonDelTaxi(2);
                                            mostrarDialogoEspera(getString(R.string.esperando_confirmacion));

                                        } else {
                                            solicitudSeleccionada = null;
                                            btnClienteAbordo.setEnabled(false);
                                            servicioSocket.borrarCarreraSeleccionada();
                                            utilidades.customToast(MapaBaseActivity.this, getString(R.string.solicitud_atendida));
                                            Log.e("estado41", "estado41");
                                            cambiarEstadoLibre();
                                        }
                                        utilidades.neutralizarDialogo(dialogoTiempo);
                                    } else {
                                        utilidades.customToastCorto(MapaBaseActivity.this, "ingrese el valor de la carrera");
                                    }
                                }
                            }
                        });
                        final Builder builder = new Builder(MapaBaseActivity.this);
                        builder.setView(viewInformacion);
                        builder.setCancelable(false);
                        if (!isFinishing()) {
                            dialogoTiempo = builder.show();
                        }
                        buttonIgnorar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                servicioSocket.eliminarDeLaLista(solicitudSeleccionada);
                                solicitudSeleccionada = null;
                                btnClienteAbordo.setEnabled(false);
                                servicioSocket.borrarCarreraSeleccionada();
                                servicioSocket.estadoBotonDelTaxi(0);
                                utilidades.neutralizarDialogo(dialogoTiempo);

                            }
                        });
                    }
                }
            } else {
                switch (solicitudSeleccionada.getT()) {
                    case 1:
                        if (utilidades.isVerificarDialogo(dialogoCompras)) {
                            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View viewInformacion = inflater.inflate(R.layout.dialog_seleccionar_precio_pedido, null, false);
                            LinearLayout linearColor = viewInformacion.findViewById(R.id.linearColor);
                            if (solicitudSeleccionada.getColor() != null) {
                                linearColor.setBackgroundColor(Color.parseColor(solicitudSeleccionada.getColor()));
                            } else {
                                linearColor.setBackgroundColor(Color.parseColor("#dddddd"));
                            }
                            TextView tvServicio = viewInformacion.findViewById(R.id.tv_servicio);
                            utilidades.definirNombreServicio(solicitudSeleccionada, tvServicio);
                            final ScrollView scrollView = viewInformacion.findViewById(R.id.scrv_pedido);
                            scrollView.setBackgroundDrawable(utilidades.drawableFondoSolicitud(solicitudSeleccionada.isQuiosco(),
                                    solicitudSeleccionada.getTipoFormaPago(), solicitudSeleccionada.isPedido(), solicitudSeleccionada.getT(), this));
                            final TextView txtCliente = viewInformacion.findViewById(R.id.txt_nombre_cliente);
                            final TextView txtPedido = viewInformacion.findViewById(R.id.txt_pedido_com);
                            final TextView txtLugarCompra = viewInformacion.findViewById(R.id.tv_lugar_compra);
                            final TextView txtLugarEntrega = viewInformacion.findViewById(R.id.tv_lugar_entrega);
                            final Button btnAceptar = viewInformacion.findViewById(R.id.btn_aceptar_compra);
                            final Button btnCancelar = viewInformacion.findViewById(R.id.btn_cancelar_compra);
                            final RadioButton radioButton_uno = viewInformacion.findViewById(R.id.rb_precio_one);
                            radioButton_uno.setText(utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getLp().get(0)));
                            final RadioButton radioButton_dos = viewInformacion.findViewById(R.id.rb_precio_two);
                            radioButton_dos.setText(utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getLp().get(1)));
                            final RadioButton radioButton_tre = viewInformacion.findViewById(R.id.rb_precio_there);
                            radioButton_tre.setText(utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getLp().get(2)));
                            final RadioButton radioButton_cuatro = viewInformacion.findViewById(R.id.rb_precio_four);
                            final LinearLayout ly_precios_otro = viewInformacion.findViewById(R.id.ly_precio_otros);
                            final TextView tv_precios_otro = viewInformacion.findViewById(R.id.tv_precio_otro);
                            final ImageButton btn_precio_aumentar = viewInformacion.findViewById(R.id.btn_precio_aumentar);
                            final ImageButton btn_precio_disminuir = viewInformacion.findViewById(R.id.btn_precio_disminuir);
                            ly_precios_otro.setVisibility(View.GONE);
                            txtCliente.setText(solicitudSeleccionada.getNombresCliente());
                            txtPedido.setText(solicitudSeleccionada.getPedido());
                            txtLugarCompra.setText(solicitudSeleccionada.getLugarCompra());
                            txtLugarEntrega.setText(solicitudSeleccionada.getBarrio());
                            final double[] precio = new double[1];
                            precio[0] = 0;
                            radioButton_uno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(0);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_dos.setChecked(false);
                                        radioButton_tre.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_dos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(1);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_uno.setChecked(false);
                                        radioButton_tre.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_tre.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(2);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_uno.setChecked(false);
                                        radioButton_dos.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_cuatro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        ly_precios_otro.setVisibility(View.VISIBLE);
                                        tv_precios_otro.setText(utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getLp().get(2)));
                                        precio[0] = solicitudSeleccionada.getLp().get(2);
                                        radioButton_uno.setChecked(false);
                                        radioButton_dos.setChecked(false);
                                        radioButton_tre.setChecked(false);

                                    }
                                }
                            });

                            btn_precio_aumentar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    double total = Double.parseDouble(tv_precios_otro.getText().toString());
                                    if (total >= solicitudSeleccionada.getM()) {
                                        utilidades.customToastCorto(MapaBaseActivity.this, "No se puede aumentar mas la cantidad");
                                        btn_precio_aumentar.setEnabled(false);
                                        return;
                                    }
                                    btn_precio_disminuir.setEnabled(true);
                                    String retorno = utilidades.dosDecimales(MapaBaseActivity.this, (total + solicitudSeleccionada.getAu()));
                                    tv_precios_otro.setText(retorno);
                                    precio[0] = total + solicitudSeleccionada.getAu();


                                }
                            });

                            btn_precio_disminuir.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    double total = Double.parseDouble(tv_precios_otro.getText().toString());
                                    if (total <= solicitudSeleccionada.getLp().get(0)) {
                                        utilidades.customToastCorto(MapaBaseActivity.this, "No se puede reducir la cantidad");
                                        btn_precio_disminuir.setEnabled(false);
                                        return;
                                    }
                                    if (total > solicitudSeleccionada.getLp().get(0)) {
                                        btn_precio_aumentar.setEnabled(true);
                                        String retorno = utilidades.dosDecimales(MapaBaseActivity.this, (total - solicitudSeleccionada.getAu()));
                                        tv_precios_otro.setText(retorno);
                                        precio[0] = total - solicitudSeleccionada.getAu();
                                    }
                                }
                            });

                            Builder builder = new Builder(MapaBaseActivity.this);
                            builder.setView(viewInformacion);
                            if (!isFinishing()) {
                                dialogoCompras = builder.show();
                            }
                            btnCancelar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    utilidades.neutralizarDialogo(dialogoCompras);
                                    solicitudSeleccionada = null;
                                    servicioSocket.borrarCarreraSeleccionada();
                                    btnClienteAbordo.setEnabled(false);

                                }
                            });
                            btnAceptar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (mBound) {
                                        if (precio[0] != 0) {
                                            servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdPedido(), 2);
                                            servicioSocket.enviarAceptarPedido(precio[0]);
                                            Log.e("estado22", "estado22");
                                            cambiarEstadoOcupado();
                                            mostrarDialogoEspera(getString(R.string.esperando_confirmacion));
                                            utilidades.neutralizarDialogos(dialogoCompras);
                                            servicioSocket.estadoBotonDelTaxi(2);
                                        } else {
                                            utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.precio_antes_envio));
                                        }
                                    }
                                }
                            });

                        }
                        break;
                    case 2:
                        if (utilidades.isVerificarDialogo(dialogoCompras)) {
                            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View viewInformacion = inflater.inflate(R.layout.dialog_seleccionar_precio_encomienda, null, false);
                            LinearLayout linearColor = viewInformacion.findViewById(R.id.linearColor);
                            if (solicitudSeleccionada.getColor() != null) {
                                linearColor.setBackgroundColor(Color.parseColor(solicitudSeleccionada.getColor()));
                            } else {
                                linearColor.setBackgroundColor(Color.parseColor("#dddddd"));
                            }
                            TextView tvServicio = viewInformacion.findViewById(R.id.tv_servicio);
                            utilidades.definirNombreServicio(solicitudSeleccionada, tvServicio);
                            final ScrollView scrollView = viewInformacion.findViewById(R.id.scrv_pedido);
                            scrollView.setBackgroundDrawable(utilidades.drawableFondoSolicitud(solicitudSeleccionada.isQuiosco(),
                                    solicitudSeleccionada.getTipoFormaPago(), solicitudSeleccionada.isPedido(), solicitudSeleccionada.getT(), this));
                            final TextView tvDesde = viewInformacion.findViewById(R.id.tv_desde);
                            final TextView tvEntregar = viewInformacion.findViewById(R.id.tv_entregar);
                            final TextView tvCallePrincipal = viewInformacion.findViewById(R.id.tv_calle_principal);
                            final TextView tvCalleSecundaria = viewInformacion.findViewById(R.id.tv_calle_secundaria);
                            final TextView tvReferecnia = viewInformacion.findViewById(R.id.tv_referecnia);
                            final TextView tvEncomienda = viewInformacion.findViewById(R.id.tv_encomienda);
                            final Button btnAceptar = viewInformacion.findViewById(R.id.btn_aceptar_compra);
                            final Button btnCancelar = viewInformacion.findViewById(R.id.btn_cancelar_compra);
                            final RadioButton radioButton_uno = viewInformacion.findViewById(R.id.rb_precio_one);
                            radioButton_uno.setText(utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getLp().get(0)));
                            final RadioButton radioButton_dos = viewInformacion.findViewById(R.id.rb_precio_two);
                            radioButton_dos.setText(utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getLp().get(1)));
                            final RadioButton radioButton_tre = viewInformacion.findViewById(R.id.rb_precio_there);
                            radioButton_tre.setText(utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getLp().get(2)));
                            final RadioButton radioButton_cuatro = viewInformacion.findViewById(R.id.rb_precio_four);
                            final LinearLayout ly_precios_otro = viewInformacion.findViewById(R.id.ly_precio_otros);
                            final TextView tv_precios_otro = viewInformacion.findViewById(R.id.tv_precio_otro);
                            final Button btn_precio_aumentar = viewInformacion.findViewById(R.id.btn_precio_aumentar);
                            final Button btn_precio_disminuir = viewInformacion.findViewById(R.id.btn_precio_disminuir);
                            ly_precios_otro.setVisibility(View.GONE);
                            tvDesde.setText(solicitudSeleccionada.getbE());
                            tvEntregar.setText(solicitudSeleccionada.getBarrio());
                            tvCallePrincipal.setText(solicitudSeleccionada.getCallePrincipal());
                            tvCalleSecundaria.setText(solicitudSeleccionada.getCalleSecundaria());
                            tvReferecnia.setText(solicitudSeleccionada.getReferencia());
                            tvEncomienda.setText(solicitudSeleccionada.getPedido());
                            final double[] precio = new double[1];
                            precio[0] = 0;
                            radioButton_uno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(0);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_dos.setChecked(false);
                                        radioButton_tre.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_dos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(1);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_uno.setChecked(false);
                                        radioButton_tre.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_tre.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(2);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_uno.setChecked(false);
                                        radioButton_dos.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_cuatro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        ly_precios_otro.setVisibility(View.VISIBLE);
                                        tv_precios_otro.setText(utilidades.dosDecimales(MapaBaseActivity.this, solicitudSeleccionada.getLp().get(0)));
                                        precio[0] = solicitudSeleccionada.getLp().get(2);
                                        radioButton_uno.setChecked(false);
                                        radioButton_dos.setChecked(false);
                                        radioButton_tre.setChecked(false);

                                    }
                                }
                            });

                            btn_precio_aumentar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    double total = Double.parseDouble(tv_precios_otro.getText().toString());
                                    if (total >= solicitudSeleccionada.getM()) {
                                        utilidades.customToastCorto(MapaBaseActivity.this, "No se puede aumentar mas la cantidad");
                                        btn_precio_aumentar.setEnabled(false);
                                        return;
                                    }
                                    btn_precio_disminuir.setEnabled(true);
                                    String retorno = utilidades.dosDecimales(MapaBaseActivity.this, total + solicitudSeleccionada.getAu());
                                    tv_precios_otro.setText(retorno);
                                    precio[0] = total + solicitudSeleccionada.getAu();
                                }
                            });

                            btn_precio_disminuir.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    double total = Double.parseDouble(tv_precios_otro.getText().toString());
                                    if (total <= solicitudSeleccionada.getLp().get(0)) {
                                        utilidades.customToastCorto(MapaBaseActivity.this, "No se puede reducir la cantidad");
                                        btn_precio_disminuir.setEnabled(false);
                                        return;
                                    }
                                    if (total > solicitudSeleccionada.getLp().get(0)) {
                                        btn_precio_aumentar.setEnabled(true);
                                        String retorno = utilidades.dosDecimales(MapaBaseActivity.this, total - solicitudSeleccionada.getAu());
                                        tv_precios_otro.setText(retorno);
                                        precio[0] = total - solicitudSeleccionada.getAu();
                                    }
                                }
                            });

                            Builder builder = new Builder(MapaBaseActivity.this);
                            builder.setView(viewInformacion);
                            if (!isFinishing()) {
                                dialogoCompras = builder.show();
                            }
                            btnCancelar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    utilidades.neutralizarDialogo(dialogoCompras);
                                    solicitudSeleccionada = null;
                                    servicioSocket.borrarCarreraSeleccionada();
                                    btnClienteAbordo.setEnabled(false);

                                }
                            });
                            btnAceptar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (mBound) {
                                        if (precio[0] != 0) {
                                            servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdPedido(), 2);
                                            servicioSocket.enviarAceptarPedido(precio[0]);
                                            Log.e("estado23", "estado23");
                                            cambiarEstadoOcupado();
                                            mostrarDialogoEspera(getString(R.string.esperando_confirmacion));
                                            utilidades.neutralizarDialogos(dialogoCompras);
                                            servicioSocket.estadoBotonDelTaxi(2);
                                        } else {
                                            utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.precio_antes_envio));
                                        }
                                    }
                                }
                            });


                        }
                        break;
                }
            }


        } else {
            utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.solicitud_atendida));
            utilidades.neutralizarDialogo(dialogoTiempo);
            servicioSocket.borrarCarreraSeleccionada();
            servicioSocket.resetIdEnvioCalificacion();
            servicioSocket.estadoBotonDelTaxi(0);
            solicitudSeleccionada = null;
        }

    }

    public void mostrarDialogoEspera(String mensaje) {
        if (servicioSocket.solicitudSeleccionada != null) {
            pDialog = new ProgressDialog(activity);
            pDialog.setMessage(mensaje);
            pDialog.setIndeterminate(false);
            if (servicioSocket.solicitudSeleccionada.getIdPedido() != 0) {
                pDialog.setCancelable(false);
            } else {
                pDialog.setCancelable(true);
                pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        Builder localBuilder = new Builder(MapaBaseActivity.this);
                        localBuilder.setIcon(R.mipmap.ic_launcher);
                        localBuilder.setTitle(R.string.str_alerta_min);
                        localBuilder.setCancelable(false);
                        localBuilder
                                .setMessage(getString(R.string.cancelar_solicitud));
                        localBuilder.setPositiveButton(R.string.str_si,
                                new OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int paramAnonymous2Int) {
                                        if (servicioSocket.solicitudSeleccionada.getEstadoSolicitud() == 5) {
                                            servicioSocket.cancelarSolicitud(12, 7, solicitudSeleccionada, false, 0, null);
                                        } else {
                                            servicioSocket.cancelarSolicitud(10, 7, solicitudSeleccionada, false, 0, null);
                                        }
                                        //borrarMarketCliente();
                                        if (dialogoTiempo != null) {
                                            if (dialogoTiempo.isShowing()) {
                                                dialogoTiempo.dismiss();
                                                dialogoTiempo = null;
                                            }
                                        }
                                        if (pDialog != null) {
                                            if (pDialog.isShowing()) {
                                                pDialog.dismiss();
                                            }
                                        }
                                        btnWaze.setVisibility(View.GONE);
                                        imageButtonMensajes.setVisibility(View.GONE);
                                        deshabilitarComponentesCarrera();
                                        Log.e("estado42", "estado42");
                                        cambiarEstadoLibre();
                                        solicitudSeleccionada = null;
                                        btnClienteAbordo.setEnabled(false);
                                        lyEnCarrera.setVisibility(View.GONE);
                                        servicioSocket.borrarCarreraSeleccionada();
                                        servicioSocket.isCronometroTiempo = false;
                                        dialog.cancel();
                                        servicioSocket.estadoPedido = 0;
                                    }
                                });
                        localBuilder.setNegativeButton(R.string.str_no,
                                new OnClickListener() {
                                    public void onClick(
                                            DialogInterface paramAnonymous2DialogInterface,
                                            int paramAnonymous2Int) {
                                        paramAnonymous2DialogInterface.dismiss();
                                        mostrarDialogoEspera(getString(R.string.esperando_confirmacion));
                                    }
                                });
                        if (!isFinishing()) {
                            dialogoEspera = localBuilder.show();
                        }


                    }
                });
            }
            if (!isFinishing()) {
                pDialog.show();
            } else {
                mostrarTiempo(getString(R.string.esperando_confirmacion));
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mapa, menu);
        //Mostrar Carreras despachadas
        String dataConfig = "";
        menu.getItem(8).setVisible(false); //CAMBIAR A FALSE
        if (mBound) {
            dataConfig = servicioSocket.getSpParametrosConfiguracion().getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        }
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 10: //Configuracion de mensajeria con audios
                                if (objetoConfig.getInt("h") == 1) {
                                    menu.getItem(8).setVisible(true);
                                } else {
                                    menu.getItem(8).setVisible(false);
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (!spConfiguracionAvanzada.getBoolean("modoAvanzado", false)) {
            menu.getItem(3).setVisible(false);
        } else {
            menu.getItem(3).setVisible(true);
        }
        if (!spConfiguracionAvanzada.getBoolean("modoAvanzadoDineroElectronico", false)) {
            menu.getItem(6).setVisible(false);
        } else {
            menu.getItem(6).setVisible(true);
        }
        itemNotificacion = menu.findItem(R.id.menu_mensajes);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.miperfil) {
            presentarInformacionCliente();
            return true;
        } else if (i == R.id.cambiarclave) {
            presentarCambioCable();
            return true;
        } else if (i == R.id.cerrarsecion) {
            confirmarSalida(true);
            banderaCerrarSecion = solicitudSeleccionada == null;
            return true;
        } else if (i == R.id.historico_despacho) {
            startActivity(new Intent(this, HistorialActivity.class));
            return true;
        } else if (i == R.id.envio_sugerencia) {
            startActivity(new Intent(this, SugerenciasActivity.class));
            return true;
        } else if (i == R.id.dinero_electronico) {
            Toast.makeText(getBaseContext(), "DINERO ELECTRONICO", Toast.LENGTH_LONG).show();
            cobros("", "");
            return true;
        } else if (i == R.id.menu_mensajes) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    itemNotificacion.setIcon(setBadgeCount(0, itemNotificacion.getIcon()));
                }
            });

            startActivity(new Intent(MapaBaseActivity.this, NotificacionesActivity.class));
            overridePendingTransition(R.anim.zoom_back_in, R.anim.zoom_back_out);
            return true;
        } else if (i == R.id.salir) {
            if (mBound) {
                if (servicioSocket.solicitudSeleccionada == null) {
                    confirmarSalida(false);
                } else {
                    utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.en_carrera));
                }
            }
            return true;
        } else if (i == R.id.menu_mensaje_broadcast) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, MY_PERMISSIONS_REQUEST_AUDIO);
                return true;
            } else {
                abrirChatBroadcast();
            }

            return true;
        } else if (i == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    public Drawable setBadgeCount(int count, Drawable recurso) {
        Drawable reuse = recurso;
        BadgeDrawable badge;
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable();
        }
        badge.setCount(count);
        Drawable[] capas = new Drawable[2];
        capas[0] = reuse;
        capas[1] = badge;
        LayerDrawable capasDrawable = new LayerDrawable(capas);
        return capasDrawable;
    }

    public void confirmarSalida(boolean isCerrarSesion) {
        try {
            if (utilidades.isVerificarDialogo(dialogoSalirSistema)) {
                String mensaje;
                if (servicioSocket.solicitudSeleccionada != null) {
                    mensaje = getString(R.string.mensaje_salida_cancelar_solicitud);
                } else {
                    mensaje = getString(R.string.mensaje_desea_salir) + " Su estado será fuera de servicio";
                }
                Builder alertDialog = new Builder(MapaBaseActivity.this);
                alertDialog.setTitle(R.string.str_alerta_min);
                alertDialog.setCancelable(false);
                alertDialog.setIcon(R.mipmap.ic_launcher);
                alertDialog.setMessage(mensaje);
                if (!isCerrarSesion) {
                    alertDialog.setCancelable(true);
                    alertDialog.setNegativeButton("Minimizar",
                            new OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    utilidades.showFloatingView(MapaBaseActivity.this, true);
                                    dialog.dismiss();
                                    banderaSalida = false;
                                    banderaCerrarSecion = false;
                                    Intent startMain = new Intent(Intent.ACTION_MAIN);
                                    startMain.addCategory(Intent.CATEGORY_HOME);
                                    startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(startMain);


                                }
                            });

                    alertDialog.setNeutralButton("Cancelar", new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }

                alertDialog.setPositiveButton("Salir",
                        new OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                solicitudSeleccionada = servicioSocket.solicitudSeleccionada;
                                if (solicitudSeleccionada != null) {
                                    if (solicitudSeleccionada.getIdPedido() != 0) {
                                        presentarEventualidades();
                                    } else {
                                        utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.mensaje_confirmacion_salir));
                                        presentarEventualidades();
                                    }
                                } else if (banderaCerrarSecion) {
                                    mapaPresenter.obtenerCerrarSesion(spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), false);
                                } else if (!banderaSalida) {
                                    int tipoV = spLogin.getInt("tipoVehiculo", 0);
                                    if (tipoV == 5 || tipoV == 4) {
                                        if (dialogResgistroPush != null && dialogResgistroPush.isShowing())
                                            return;
                                        dialogRegistrarPush().show();
                                    } else {
                                        cerrarAplicativo();
                                    }

                                }
                            }
                        });
                alertDialog.setNeutralButton("Cancelar", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                if (!isFinishing()) {
                    dialogoSalirSistema = alertDialog.show();
                }

            }
        } catch (IllegalArgumentException | ClassCastException e) {
            ExtraLog.Log(TAG, "confirmarSalida: " + e);
        }
    }


    public void cerrarAplicativo() {
        servicioSocket.salirSistema();
        Intent intent = new Intent(MapaBaseActivity.this, ServicioSockets.class);
        if (mConnection != null) {
            unbindService(mConnection);
            mConnection = null;
        }
        stopService(intent);
        finish();

    }

    private Dialog dialogResgistroPush;

    public void registrarPush(boolean isActivar) {
        SharedPreferences sharedPreferences = getSharedPreferences(VariablesGlobales.SP_REGISTRAR_PUSH, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(VariablesGlobales.SP_DATA_REGISTRAR_PUSH, isActivar);
        editor.apply();
    }

    public Dialog dialogRegistrarPush() {
        final boolean[] isActivaPush = {spConfiguracionApp.getBoolean(VariablesGlobales.OPCION_PUSH, true)};
        Builder builder = new Builder(this);
        final ArrayList itemsSeleccionados = new ArrayList();
        CharSequence[] items = new CharSequence[1];
        items[0] = "Activar notificación.";
        builder.setTitle("¿Desea que se le notifique solicitudes Push?")
                .setIcon(R.mipmap.ic_launcher)
                .setMultiChoiceItems(items, isActivaPush, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        isActivaPush[0] = isChecked;
                        registrarPush(isChecked);
                    }
                }).setPositiveButton("Salir", new OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (utilidades.estaConectado(MapaBaseActivity.this)) {
                    if (isActivaPush[0]) {
                        dialog.cancel();
                        cerrarAplicativo();
                        mapaPresenter.activarDesactivarPush(spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), utilidades.getImei(MapaBaseActivity.this), spLogin.getInt("idEmpresa", 0), spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0), 1, false);
                    } else cerrarAplicativo();
                } else {
                    Toast.makeText(MapaBaseActivity.this, "Usted no tiene una conexión activa a internet.", Toast.LENGTH_SHORT).show();
                }
            }
        })
                .setNegativeButton("Cancelar", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        dialogResgistroPush = builder.create();
        return dialogResgistroPush;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (isOpenMenuLateral) {
                    drawerLayout.closeDrawers();
                } else {
                    confirmarSalida(false);
                    banderaSalida = false;
                }
                return false;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapViewBox.onDestroy();
        if (mConnectedThread != null) {
            mConnectedThread.setRun(false);
        }
        try {
            if (btSocket != null) {
                btSocket.close();
            }
        } catch (IOException ignored) {
        }
        if (mBound) {
            servicioSocket.cancelNotification(getApplicationContext(), 1);
            servicioSocket.cancelNotification(getApplicationContext(), 4);
        }
        if (!spLogin.getBoolean("logeado", true)) {
            finish();
            startActivity(new Intent(MapaBaseActivity.this, IniciarSesionBaseActivity.class));
        }
        setEstadoFuncionalidad(false); //STO SOLO PARA PROBAR BORRAR
        utilidades.hideFloatingView(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (intent.getExtras() != null) {
            boolean isCarrera = intent.getExtras().getBoolean("enSolicitud");
            if (isCarrera) {
                if (mBound) {
                    servicioSocket.cambioActividad();
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal, getBaseContext().getTheme()));
                } else {
                    iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal));
                }
                contadorZoom = 1;
                isRastreo = false;
                modoGpsRastreo(isRastreo, locacionDefault);
                final double lati = intent.getExtras().getDouble("latitud");
                final double longitu = intent.getExtras().getDouble("longitud");
                if (mapaOSM != null) {
                    GeoPoint gPt = new GeoPoint(lati, longitu);
                    mapaOSM.getController().setZoom(18.5);
                    mapaOSM.getController().animateTo(gPt);
                }

                if (mapBox != null) {
                    LatLng latLng = new LatLng(lati, longitu);
                    mapBox.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                            .target(latLng)
                            .zoom(16)
                            .tilt(0)
                            .build()), 200);
                }
                if (mapFragment != null && mapFragment.getView().getVisibility() == View.VISIBLE) {
                    if (mMap != null) {
                        com.google.android.gms.maps.model.LatLng latLng =
                                new com.google.android.gms.maps.model.LatLng(lati, longitu);
                        mMap.animateCamera(com.google.android.gms.maps.CameraUpdateFactory.newCameraPosition(
                                new com.google.android.gms.maps.model.CameraPosition.Builder()
                                        .target(latLng)
                                        .zoom(16)
                                        .tilt(0)
                                        .build()));
                    }
                }
            }
            if (intent.getExtras().getBoolean("isCorazonCerrar")) {
                cerrarAplicativo();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        mapViewBox.onSaveInstanceState(savedInstanceState);
        utilidades.neutralizarDialogos(alertDialogEventualidades);
        utilidades.neutralizarDialogos(dialogoTiempo);
        utilidades.neutralizarDialogos(dialogoAbordo);
        utilidades.neutralizarDialogos(dialogoCambioClave);
        utilidades.neutralizarDialogos(dialogoSalirSistema);
        banderaCerrarSecion = false;
        savedInstanceState.putInt("estadoGps", contadorZoom);
        savedInstanceState.putString("totalCarreraTemporal", totalCarreraTemporal);
        savedInstanceState.putParcelable("localizacion", locacionDefault);
        savedInstanceState.putParcelable("respuestaPosibleSolicitudes", respuestaPosibleSolicitudes);
        savedInstanceState.putParcelable("solicitudSeleccionada", solicitudSeleccionada);
        savedInstanceState.putBoolean("isEnableBtnAbordo", btnClienteAbordo.isEnabled());
        savedInstanceState.putBoolean("isConsulta", isConsulta);
        savedInstanceState.putBoolean("isRun", isRun);
        savedInstanceState.putInt("countConsulta", countConsulta);
        savedInstanceState.putInt("isVisibleTaximetro", inclTaximetro.getVisibility());
        if (dialogFinCarrera != null) {
            if (dialogFinCarrera.isShowing()) {
                savedInstanceState.putBoolean("isDialogFinCarrera", true);
            }
        }
        savedInstanceState.putBoolean("isDialogoEspera", isDialogoEspera);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        isDialogoEspera = savedInstanceState.getBoolean("isDialogoEspera");
        if (isDialogoEspera) {
            mostrarEspera("Enviando datos.");
        }
        isConsulta = savedInstanceState.getBoolean("isConsulta");
        isRun = savedInstanceState.getBoolean("isRun");
        countConsulta = savedInstanceState.getInt("countConsulta");
        solicitudSeleccionada = savedInstanceState.getParcelable("solicitudSeleccionada");
        respuestaPosibleSolicitudes = savedInstanceState.getParcelable("respuestaPosibleSolicitudes");
        if (respuestaPosibleSolicitudes != null) {
            if (respuestaPosibleSolicitudes.getT() == 1) {
                String mensaje = "Se reporta un pánico.\nUsuario: " + respuestaPosibleSolicitudes.getUsuario() + "\nEmpresa: " + respuestaPosibleSolicitudes.getEmpresa() + "\nNúmero Unidad: " + respuestaPosibleSolicitudes.getNumeroUnidad() + "\nPor favor dirigirse al auxilio.";
                mapViewConfig.marketPanico(new LatLng(respuestaPosibleSolicitudes.getLt(), respuestaPosibleSolicitudes.getLg()), mensaje, mapaOSM, true);
                mapViewConfigMapBox.marketPanico(new LatLng(respuestaPosibleSolicitudes.getLt(), respuestaPosibleSolicitudes.getLg()), mensaje, mapBox, true);
                mapViewGoogle.marketPanico(new com.google.android.gms.maps.model.LatLng(respuestaPosibleSolicitudes.getLt(), respuestaPosibleSolicitudes.getLg()), mensaje, mMap, true);
                btnDejarRastrear.setVisibility(View.VISIBLE);
                imgBtnRastreoPanico.setVisibility(View.VISIBLE);
            }
        }
        totalCarreraTemporal = savedInstanceState.getString("totalCarreraTemporal");
        btnClienteAbordo.setEnabled(savedInstanceState.getBoolean("isEnableBtnAbordo"));
        if (savedInstanceState.getBoolean("isDialogFinCarrera")) {
            if (mBound) {
                presentar_info_fin_carrera();
            }
        }
        if (savedInstanceState.getInt("isVisibleTaximetro") == View.VISIBLE) {
            inclTaximetro.setVisibility(View.VISIBLE);
        } else {
            inclTaximetro.setVisibility(View.GONE);
        }
    }

    public void abrirWaze(View v) {
        if (v != null) {
            v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.clic_anim));
        }
        if (solicitudSeleccionada != null) {
            String lat = String.valueOf(solicitudSeleccionada.getLatitud());
            String lon = String.valueOf(solicitudSeleccionada.getLongitud());
            try {
                String url = "waze://?ll=" + lat + "," + lon + "&navigate=yes";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
            } catch (ActivityNotFoundException ex) {
                Intent intent =
                        new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
                startActivity(intent);
            }
        }
    }

    public void prenderInternet(View v) {
        Intent i = new Intent(Settings.ACTION_WIFI_SETTINGS);
        startActivity(i);
    }

    public void prenderGps(View v) {
        Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(i);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 3:
                if (data != null) {
                    if (data.getStringExtra("m").equals("s")) {
                        borraCarreraAlCancelar();
                    }
                }
                break;
        }

        if (requestCode == AgregarTarjeta.AGREGAR_TARJETA) {
            if (resultCode == Activity.RESULT_OK) {
                int tipoError = data.getIntExtra("tipoError", 0);
                int estado = data.getIntExtra("estado", 0);
                String mensaje = data.getStringExtra("mensaje");
                respuestaCobroTarjetCredito(tipoError, estado, mensaje);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "No se realizo el cobro.", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String[] palabras = matches.get(0).split(" ");
            switch (palabras[0]) {
                case "ocupado":
                    Log.e("estado24", "estado24");
                    cambiarEstadoOcupado();
                    break;
                case "libre":
                    Log.e("estado43", "estado43");
                    cambiarEstadoLibre();
                    break;
                case "lista":
                    if (servicioSocket.isEstadoBoton) {
                        Intent intent = new Intent(MapaBaseActivity.this, ListaSolicitudesActivity.class);
                        intent.putExtra("tamanio", listaSolicitudes.size());
                        startActivity(intent);
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    }
                    break;
                case "información":
                    if (solicitudSeleccionada != null) {
                        informacionSolicitud();
                    }
                    break;
                case "llamar":
                    if (solicitudSeleccionada != null) {
                        Intent i = new Intent(Intent.ACTION_CALL,
                                Uri.parse("tel:" + solicitudSeleccionada.getCelular()));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL);
                            return;
                        } else {
                            MapaBaseActivity.this.startActivity(i);
                            servicioSocket.estadoBotonDelTaxi(7);
                            servicioSocket.enviarEventoConductor(servicioSocket.getIdSolicitudSeleccionada(), 2, locacionDefault);
                        }
                    }
                    break;
                case "avisó":
                    if (solicitudSeleccionada != null) {
                        servicioSocket.enviarMensaje(getString(R.string.mensaje_para_usuario));
                    }
                    break;
            }
        }
    }

    public void dialogoCancelar(final String mensaje, final boolean isFinCarrera) {
        try {
            if (alerDialogoCancelar != null) {
                if (alerDialogoCancelar.isShowing()) {
                    return;
                }
            }
            new Handler().postDelayed(() -> runOnUiThread(() -> {
                if (alerDialogoCancelar != null && alerDialogoCancelar.isShowing())
                    return;
                Builder alertDialogBuilder = new Builder(MapaBaseActivity.this);
                alertDialogBuilder.setCancelable(false);
                alertDialogBuilder.setMessage(mensaje)
                        .setTitle(R.string.informacion)
                        .setCancelable(false)
                        .setPositiveButton(R.string.aceptar, new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                if (isFinCarrera) {
                                    presentar_info_fin_carrera();
                                    return;
                                }
                                Log.e("estado44", "estado44");
                                cambiarEstadoLibre();
                            }
                        });
                alerDialogoCancelar = alertDialogBuilder.create();
                if (!isFinishing())
                    alerDialogoCancelar.show();
            }), 1000);
        } catch (RuntimeException e) {
            Log.e("estado45", "estado45");
            cambiarEstadoLibre();
        }

    }

    public void cobros(String telefono, String cedula) {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_cobro_electronico, null);
        Builder alertDialogBuilder = new Builder(MapaBaseActivity.this);
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder.setCancelable(false);
        final EditText etTelefono = promptsView.findViewById(R.id.txt_telefono_cobro);
        final EditText etCedula = promptsView.findViewById(R.id.et_cedula);
        final EditText etMonto = promptsView.findViewById(R.id.et_monto);
        final Button btnAceptar = promptsView.findViewById(R.id.btn_aceptar);
        final Button btnCancelar = promptsView.findViewById(R.id.btn_cancel);
        if (solicitudSeleccionada != null) {
            if (telefono.equals(solicitudSeleccionada.getCelular())) {
                etTelefono.setText(solicitudSeleccionada.getCelular());
            } else {
                etTelefono.setText(telefono);
            }
        }
        etCedula.setText(cedula);
        btnAceptar.setOnClickListener(v -> {
            if (!etTelefono.getText().toString().equals("")) {
                if (!etCedula.getText().equals("")) {
                    if (!etMonto.getText().toString().trim().equals("")) {
                        if (solicitudSeleccionada != null) {
                            mapaPresenter.obtenerDineroElectronico(Double.parseDouble(etMonto.getText().toString()), etCedula.getText().toString(),
                                    solicitudSeleccionada.getIdSolicitud(), spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), etTelefono.getText().toString());
                        } else {
                            mapaPresenter.obtenerDineroElectronico(Double.parseDouble(etMonto.getText().toString()), etCedula.getText().toString(),
                                    0, spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), etTelefono.getText().toString());
                        }
                        esperaDineroElectronico();


                        alDineroElectronico.dismiss();
                    } else {
                        utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.monto));
                        etMonto.requestFocus();
                    }
                } else {
                    utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.cedula));
                    etCedula.requestFocus();
                }
            } else {
                utilidades.customToastCorto(MapaBaseActivity.this, getString(R.string.telefono));
                etTelefono.requestFocus();
            }
        });
        btnCancelar.setOnClickListener(v -> alDineroElectronico.dismiss());
        alDineroElectronico = alertDialogBuilder.create();
        if (!isFinishing()) {
            alDineroElectronico.show();
        }

    }

    public void esperaDineroElectronico() {
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage(getString(R.string.dinero_confirmacion_cliente));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        if (!isFinishing()) {
            pDialog.show();
        }


    }

    private void deleteNumber(String phoneNumber) {
        new Handler().post(() -> {
            try {
                String strNumberOne[] = {phoneNumber};
                if (ActivityCompat.checkSelfPermission(MapaBaseActivity.this, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
                    return;
                } else {
                    Cursor cursor = MapaBaseActivity.this.getContentResolver().query(
                            CallLog.Calls.CONTENT_URI, null,
                            CallLog.Calls.NUMBER + " = ? ", strNumberOne, "");
                    boolean bol = cursor.moveToFirst();
                    if (bol) {
                        do {
                            int idOfRowToDelete = cursor.getInt(cursor
                                    .getColumnIndex(CallLog.Calls._ID));
                            MapaBaseActivity.this.getContentResolver().delete(
                                    CallLog.Calls.CONTENT_URI,
                                    CallLog.Calls._ID + "= ? ",
                                    new String[]{String.valueOf(idOfRowToDelete)});
                        } while (cursor.moveToNext());
                    }
                }

            } catch (Exception ex) {
                ExtraLog.Log(TAG, "run: " + ex.getMessage());
            }
        });

    }


    public void abrirGoogleMaps(View v) {
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, null);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    public void obtenerTrafico(View v) {
        if (countTest == 0) {
            PositionTrafico position = new PositionTrafico();
            if (locacionDefault != null) {
                position.setLt(locacionDefault.getLatitude());
                position.setLg(locacionDefault.getLongitude());
                mapaPresenter.obtenerTrafico(position);
            }
            countTest = 1;
        } else {
            countTest = 0;
        }

    }

    public void abrirChatBroadcast() {
        Intent intent = new Intent(this, ChatBroadCast.class);
        startActivity(intent);
    }

    @Override
    public void createVideoDialogo(final String video) {
        runOnUiThread(() -> {
            if (video != null) {
                Intent intent = new Intent(MapaBaseActivity.this, VideoPublicidadActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void solicitudAtendidaACK(final MensajeDeuda mensajeDeuda) {
        runOnUiThread(() -> {
            utilidades.neutralizarDialogo(pDialog);
            utilidades.neutralizarDialogo(dialogoEspera);
            utilidades.neutralizarDialogo(dialogoTiempo);
            dialogoEspera = createDialogoMensaje(mensajeDeuda.getL().getP().getMs(), false, true, null, false);
            dialogoEspera.setCancelable(false);
            if (!isFinishing()) {
                dialogoEspera.show();
            }

        });
    }

    public AlertDialog createDialogoMensaje(String mensaje, boolean isMostraBoton,
                                            boolean isMostrarTextoEspera, final Dialog dialog, final boolean finCarrera) {

        Builder builder = new Builder(MapaBaseActivity.this);

        LayoutInflater inflater = getLayoutInflater();

        View v = inflater.inflate(R.layout.deudas_pendientes, null);

        TextView tvMensaje = v.findViewById(R.id.tv_mensaje);

        TextView tvMensajeEspera = v.findViewById(R.id.tv_mensaje_espera);
        if (isMostrarTextoEspera) {
            tvMensajeEspera.setVisibility(View.VISIBLE);
        } else {
            tvMensajeEspera.setVisibility(View.GONE);
        }
        ImageView imageTaxista = v.findViewById(R.id.img_taxista);
        if (imagenGlobal != null) {
            imageTaxista.setImageBitmap(imagenGlobal);
        }
        ProgressBar progressBarEspera = v.findViewById(R.id.progress_espera);

        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        if (isMostraBoton) {
            progressBarEspera.setVisibility(View.GONE);
            btnAceptar.setVisibility(View.VISIBLE);
        } else {
            btnAceptar.setVisibility(View.GONE);
        }
        btnAceptar.setOnClickListener(v1 -> {
            dialogoMensajeCobro.dismiss();
            servicioSocket.setMensajeDeudaAbordo(false);
            servicioSocket.setMensajeDeudaFinCarrera(false);
            if (finCarrera) {
                servicioSocket.setMensajeDeudaAbordo(null);
            }
        });

        tvMensaje.setText(mensaje);

        builder.setView(v);

        return builder.create();
    }

    @Override
    public void envioCobro(final String dato, final int estado) {
        ocultarEspera();
        if (dialogFinCarrera != null && dialogFinCarrera.isShowing()) {
            dialogFinCarrera.dismiss();
        }
        runOnUiThread(() -> {
            Builder builder = new Builder(MapaBaseActivity.this);
            switch (estado) {
                case 1:
                    if (solicitudSeleccionada != null) {
                        servicioSocket.setCobroEfectivo(4, "", solicitudSeleccionada);
                        presentar_info_fin_carrera();
                    } else {
                        servicioSocket.setCobroEfectivo(0, "", null);
                        Log.e("estado46", "estado46");
                        cambiarEstadoLibre();
                    }
                    return;
                case -1:
                    cambiarOcupado();
                    builder.setMessage(dato)
                            .setTitle(R.string.str_alerta_min)
                            .setPositiveButton(R.string.aceptar, (dialog, id) -> {
                                dialog.cancel();
                                presentarValorCobro();
                            });
                    if (!isFinishing()) {
                        builder.show();
                    }
                    return;
                case -2:
                    servicioSocket.setCobroEfectivo(0, "", solicitudSeleccionada);
                    servicioSocket.setCobroTarjetaCredito(0, "", solicitudSeleccionada);
                    cambiarOcupado();
                    builder.setMessage(dato)
                            .setTitle(R.string.str_alerta_min)
                            .setPositiveButton(R.string.aceptar, (dialog, id) -> {
                                dialog.cancel();
                                presentarValorCobro();
                            });
                    if (!isFinishing()) {
                        builder.show();
                    }
                    return;
            }
        });
    }

    private void crearSolicitudCobro() {
        if (solicitudSeleccionada != null) {
            SharedPreferences.Editor editor = spSolicitud.edit();
            String jSonSolicitud = solicitudSeleccionada.serializeSolicitud();
            editor.putString("key_objeto_solicitud", jSonSolicitud);
            editor.apply();
        }
    }

    public void borrarCarrera() {
        SharedPreferences.Editor editor = spSolicitud.edit();
        editor.putString("key_objeto_solicitud", "");
        editor.apply();
    }

    @Override
    public void otraApp(final String mensaje, final String paquete,
                        final boolean isDesinstalar, final boolean isCambioOcupado) {
        runOnUiThread(() -> {
            if (dialogOtraApp != null) {
                if (dialogOtraApp.isShowing()) {
                    return;
                }
            }
            if (isCambioOcupado) {
                Log.e("estado1", "estado1");
                cambiarEstadoOcupado();
            }
            Builder builder = new Builder(MapaBaseActivity.this);
            builder.setMessage(mensaje)
                    .setTitle(R.string.str_alerta_min)
                    .setPositiveButton(R.string.aceptar, (dialog, id) -> dialog.cancel());
            if (isDesinstalar) {
                builder.setNegativeButton(R.string.str_desinstalar, (dialog, id) -> {
                    dialog.cancel();
                    utilidades.startInstalledAppDetailsActivity(MapaBaseActivity.this, paquete);
                });
                dialogOtraApp = builder.create();
                if (!isFinishing()) {
                    dialogOtraApp.show();
                }

            } else {
                if (utilidades.verificarEjecucionApp(paquete, MapaBaseActivity.this)) {
                    builder.setNegativeButton(R.string.str_forzar_cierre, (dialog, id) -> {
                        dialog.cancel();
                        utilidades.startInstalledAppDetailsActivity(MapaBaseActivity.this, paquete);
                    });
                    dialogOtraApp = builder.create();
                    if (!isFinishing()) {
                        dialogOtraApp.show();
                    }
                }
            }
        });
    }

    /**
     * Muestra un dialogo de espera de cualquier activiadad de donde sea llamado
     *
     * @param titulo
     */
    public void mostrarEspera(String titulo) {
        pDialogo = new ProgressDialog(this);
        pDialogo.setCancelable(false);
        pDialogo.setTitle(titulo);
        if (!isFinishing()) {
            pDialogo.show();
        }
        isDialogoEspera = true;
    }

    /**
     * Oculta el dialogo del sistema.
     */
    public void ocultarEspera() {
        runOnUiThread(() -> new Handler(Looper.myLooper()).postDelayed(() -> {
            try {
                if (pDialogo != null) {
                    if (pDialogo.isShowing()) {
                        pDialogo.dismiss();
                        isDialogoEspera = false;
                    }
                }
            } catch (IllegalArgumentException e) {
                e.getMessage();
            }

        }, 100));
    }

    public void guardarEstadoRastreo(int contadorZoom) {
        SharedPreferences spModoRastreo = getSharedPreferences(VariablesGlobales.MODO_RASTREO, MODE_PRIVATE);
        SharedPreferences.Editor editor = spModoRastreo.edit();
        editor.putInt(VariablesGlobales.IS_MODO_CONTADOR, contadorZoom);
        editor.apply();
    }

    public void definirModoRastreoInicio() {
        SharedPreferences spModoRastreo = getSharedPreferences(VariablesGlobales.MODO_RASTREO, MODE_PRIVATE);
        contadorZoom = spModoRastreo.getInt(VariablesGlobales.IS_MODO_CONTADOR, 0);
        switch (contadorZoom) {
            case 0:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal, getBaseContext().getTheme()));
                } else {
                    iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal));
                }
                isRastreo = false;
                modoGpsRastreo(isRastreo, locacionDefault);
                contadorZoom = 1;
                break;
            case 1:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo, getTheme()));
                } else {
                    iBtnPosicion.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo));
                }
                contadorZoom = 0;
                isRastreo = true;
                modoGpsRastreo(isRastreo, locacionDefault);
                break;
        }
    }


    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private boolean isRun = true;


        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException ignored) {
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public boolean isRun() {
            return isRun;
        }

        public void setRun(boolean run) {
            isRun = run;
        }

        public void run() {
            byte[] buffer = new byte[256];
            int bytes;
            while (isRun) {
                try {
                    bytes = mmInStream.read(buffer);            //read bytes from input buffer
                    String readMessage = new String(buffer, 0, bytes);
                    bluetoothIn.obtainMessage(0, bytes, 0, readMessage).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }

        public void cancel() {
            isRun = false;
        }

        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
            } catch (IOException e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), R.string.msj_la_conexion_fallo, Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }

    private Dialog dialogPrecioPedido;

    public AlertDialog createDialogoPrecioPedido() {
        Builder builder = new Builder(MapaBaseActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_precio_pedido, null);
        final EditText etCostoCarrera = v.findViewById(R.id.et_costo_carrera);
        Button btnDefinirPrecio = v.findViewById(R.id.btn_definir_precio);
        btnDefinirPrecio.setOnClickListener(v1 -> {
            if (mBound) {
                String costo = etCostoCarrera.getText().toString();
                if (costo.trim().isEmpty()) {
                    Toast.makeText(MapaBaseActivity.this, R.string.msj_ingrese_valor_continuar, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (costo.trim().equals(".")) {
                    Toast.makeText(MapaBaseActivity.this, R.string.msj_ingrese_valor_continuar, Toast.LENGTH_SHORT).show();
                    return;
                }
                borrarMarketCliente();
                try {
                    servicioSocket.definirPrecioPedido(Double.parseDouble(costo));
                    servicioSocket.enviarPedidoComprado(Double.parseDouble(costo));
                    imgBtnComprado.setEnabled(false);

                } catch (NumberFormatException nfe) {
                    Toast.makeText(MapaBaseActivity.this, R.string.msj_ingrese_valor_continuar, Toast.LENGTH_SHORT).show();
                    return;
                }
                agregarMarketCliente();
            }
            if (dialogPrecioPedido != null && dialogPrecioPedido.isShowing()) {
                dialogPrecioPedido.dismiss();
            }

        });
        builder.setView(v);
        return builder.create();
    }


    private PublicidadPresenter publicidadPresenter;

    public void peticionDePublicidad(int estado, int idSolicitud) {
        publicidadPresenter = new PublicidadPresenter();
        publicidadPresenter.getPublicidad(VariablesGlobales.NUM_ID_APLICATIVO, spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0),
                estado, idSolicitud, spLogin.getInt(VariablesGlobales.ID_VEHICULO, 0), spLogin.getInt(VariablesGlobales.ID_USUARIO, 0),
                respuestaPublicidad -> {
                    if (respuestaPublicidad != null) {
                        if (respuestaPublicidad.getEn() == 1) {
                            lyPublicidad.cargarPublicidad(respuestaPublicidad);
                        }
                    }
                }, this);

        lyPublicidad.setTimeViewListener(new LayoutPublicitario.TimeViewListener() {
            @Override
            public void onClosePublicidad() {

            }

            @Override
            public void otraPantalla(RespuestaPublicidad respuestaPublicidad) {
                Intent intent = new Intent(MapaBaseActivity.this, ComponentePublicitarioActivity.class);
                intent.putExtra("respuestaPublicitaria", respuestaPublicidad);
                startActivity(intent);
            }
        });
    }

    private Dialog dialogValidarUbicacion;

    public void mostrarValidarUbicacion(final int idSolicitud, String mensaje) {
        if (dialogValidarUbicacion != null && dialogValidarUbicacion.isShowing()) return;
        Builder builder = new Builder(MapaBaseActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_validacion_gps, null);
        final TextView tvMesaje = v.findViewById(R.id.tv_mensaje);
        tvMesaje.setText(mensaje);
        v.findViewById(R.id.btn_no).setOnClickListener(v12 -> {
            if (dialogValidarUbicacion != null && dialogValidarUbicacion.isShowing())
                dialogValidarUbicacion.dismiss();
            dialogoAbordo();
        });
        v.findViewById(R.id.btn_si).setOnClickListener(v1 -> {
            if (dialogValidarUbicacion != null && dialogValidarUbicacion.isShowing())
                dialogValidarUbicacion.dismiss();
            servicioSocket.enviarValidacion(idSolicitud);
            dialogoAbordo();
        });
        builder.setView(v);
        dialogValidarUbicacion = builder.create();
        dialogValidarUbicacion.show();
    }

    @Override
    public void respuestaValidacion(final String data) {
        runOnUiThread(() -> Toast.makeText(MapaBaseActivity.this, data, Toast.LENGTH_SHORT).show());
    }

    @Override
    public void corazonMalvadoCambiarOcupado() {
        runOnUiThread(this::cambiarOcupado);
    }

    private Dialog dialogCorazon;

    @Override
    public void corazonMalvadoDialogo(final boolean isBooleanFinish) {
        runOnUiThread(() -> {
            cambiarOcupado();
            if (dialogCorazon != null && dialogCorazon.isShowing())
                return;
            Builder builder = new Builder(MapaBaseActivity.this);
            builder.setCancelable(false);
            builder.setMessage(R.string.msj_corazon_problema)
                    .setTitle(R.string.str_error)
                    .setPositiveButton(R.string.aceptar, (dialog, id) -> {
                        if (isBooleanFinish) {
                            cerrarAplicativo();
                        }
                        if (dialogCorazon != null && dialogCorazon.isShowing())
                            dialogCorazon.dismiss();
                    });
            dialogCorazon = builder.create();
            dialogCorazon.show();
            ;
        });
    }

    @Override
    public void corazonMalvadoDialogoError(final boolean isLanzarError) {
        runOnUiThread(() -> {
            cambiarOcupado();
            if (dialogCorazon != null && dialogCorazon.isShowing())
                return;
            Builder builder = new Builder(MapaBaseActivity.this);
            builder.setCancelable(false);
            builder.setMessage(R.string.msj_corazon_problema)
                    .setTitle(R.string.str_error)
                    .setPositiveButton(R.string.aceptar, (dialog, id) -> {
                        if (dialogCorazon != null && dialogCorazon.isShowing())
                            dialogCorazon.dismiss();
                        if (isLanzarError) {
                            Log.e(TAG, null);
                        }
                    });
            dialogCorazon = builder.create();
            dialogCorazon.show();
        });
    }

    public void cancelNotification(Context ctx, int notifyId) {
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        if (nMgr != null) {
            nMgr.cancel(notifyId);
        }
    }

    public String directorio() {
        String intStorageDirectory = getFilesDir().toString();
        File file = new File(intStorageDirectory, "ktaxiDriver/Audio");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }
}