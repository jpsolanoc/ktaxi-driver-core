package com.kradac.conductor.vista;

import android.Manifest;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.AdaptadorEntidadFinanciera;
import com.kradac.conductor.adaptadoreslista.AdaptadorTablaRegistrarPago;
import com.kradac.conductor.extras.TratarImagenesGiro;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionRegistroPago;
import com.kradac.conductor.modelo.EntidadFinanciera;
import com.kradac.conductor.modelo.ItemRegistrarPago;
import com.kradac.conductor.modelo.PagosConductor;
import com.kradac.conductor.presentador.PresentadorRegistrarPago;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrarPagoActivity extends AppCompatActivity implements OnComunicacionRegistroPago {

    private static final int TAKE_PICTURE = 1;
    private static final String TAG = RegistrarPagoActivity.class.getName();
    private static final int SELECT_PICTURE = 2;
    @BindView(R2.id.appbar)
    Toolbar appbar;
    @BindView(R2.id.spn_entidad_financiera)
    Spinner spnEntidadFinanciera;
    @BindView(R2.id.btn_registrar)
    Button btnRegistrar;
    @BindView(R2.id.et_numero_ticket)
    EditText etNumeroTicket;
    @BindView(R2.id.et_monto_pago)
    EditText etMontoPago;
    @BindView(R2.id.et_fecha_pago)
    EditText etFechaPago;
    @BindView(R2.id.btn_archivo)
    Button btnArchivo;
    @BindView(R2.id.img_btn_foto)
    ImageButton imgBtnFoto;

    public String mCurrentPhotoPath;
    @BindView(R2.id.img_vaucher)
    ImageView imgVaucher;
    @BindView(R2.id.tv_numero_cuenta)
    TextView tvNumeroCuenta;
    @BindView(R2.id.tr_numero_cuenta)
    TableRow trNumeroCuenta;
    @BindView(R2.id.tv_documento)
    TextView tvDocumento;
    private SharedPreferences spLogin;
    AdaptadorEntidadFinanciera adaptadorEntidadFinanciera;
    private PresentadorRegistrarPago registrarPago;
    private ArrayList<PagosConductor.LD> datosPago;
    private boolean isImagenSeleccionada;
    private Utilidades utilidades;
    private String montoPago, montoDecuento;
    public Dialog pDialogo;
    private SharedPreferences spParametrosConfiguracion;
    private Window win;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_pago);
        ButterKnife.bind(this);
        win = getWindow();
        spParametrosConfiguracion = getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        Bundle bundle = getIntent().getExtras();
        utilidades = new Utilidades();
        cambiarPantallaEncendido();
        if (bundle != null) {
            datosPago = bundle.getParcelableArrayList("datosPago");
            double valorPagar = 0;
            double valorDescuento = 0;
            for (PagosConductor.LD ld : datosPago) {
                valorPagar = valorPagar + (ld.getDeuda() - ld.getObtenerPagoDescuento()) + ld.getMoraImpuestos();
                valorDescuento = valorDescuento + ld.getObtenerPagoDescuento();
            }
            montoDecuento = utilidades.dosDecimales(RegistrarPagoActivity.this,valorDescuento);
            montoPago = utilidades.dosDecimales(RegistrarPagoActivity.this,valorPagar);
            etMontoPago.setText(utilidades.dosDecimales(RegistrarPagoActivity.this,valorPagar).concat(ejecutarMoneda()));
        }
        setSupportActionBar(appbar);
        spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle("Registrar pago");
        }
        registrarPago = new PresentadorRegistrarPago(this);
        registrarPago.getListarEntidades(VariablesGlobales.NUM_ID_APLICATIVO, spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0), spLogin.getInt(VariablesGlobales.ID_USUARIO, 0));
        mostrarEspera("Cargando entidades bancarias.");
        spnEntidadFinanciera.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG, "onItemSelected: " + adaptadorEntidadFinanciera.getEntidadFinanciera(position));
                trNumeroCuenta.setVisibility(View.VISIBLE);
                tvNumeroCuenta.setText(adaptadorEntidadFinanciera.getEntidadFinanciera(position).getCuenta());
                tvDocumento.setText(adaptadorEntidadFinanciera.getEntidadFinanciera(position).getLabel())
                ;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnRegistrar.setEnabled(false);
    }

    public void cambiarPantallaEncendido() {
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
    }

    @Override
    public void respuestaEntidadFinanciera(EntidadFinanciera entidadFinanciera) {
        ocultarEspera();
        if (entidadFinanciera != null) {
            switch (entidadFinanciera.getEn()) {
                case 1:
                    tvDocumento.setText(entidadFinanciera.getLT().get(0).getLabel());
                    adaptadorEntidadFinanciera = new AdaptadorEntidadFinanciera(this, entidadFinanciera.getLT());
                    spnEntidadFinanciera.setAdapter(adaptadorEntidadFinanciera);
                    btnRegistrar.setEnabled(true);
                    break;
                default:
                    Builder builder = new Builder(this);
                    builder.setMessage(entidadFinanciera.getM())
                            .setTitle("Información")
                            .setCancelable(false)
                            .setPositiveButton("Aceptar", new OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                }
                            })
                            .show();


                    break;
            }
        } else {
            Toast.makeText(this, "No se pueden obtener las entidades financieras, intente mas tarde", Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Dialog dialogoVerificaPago;

    @OnClick(R2.id.btn_registrar)
    public void onViewClicked() {

    }

    /**
     * Crea un diálogo con personalizado
     *
     * @return Diálogo
     */
    public AlertDialog createDialogoVerifica() {
        final Builder builder = new Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialogo_registrar_pago, null);
        ListView listaVerificaPago = (ListView) v.findViewById(R.id.lista_verifica_pago);
        TextView tvNumeroTicket = (TextView) v.findViewById(R.id.tv_numero_ticket);
        TextView tvFechaPago = (TextView) v.findViewById(R.id.tv_fecha_pago);
        TextView tvMontoPago = (TextView) v.findViewById(R.id.tv_monto_pago);
        Button btnCancelar = (Button) v.findViewById(R.id.btn_cancelar);
        Button btnAceptar = (Button) v.findViewById(R.id.btn_aceptar);
        ArrayList<ItemRegistrarPago> itemRegistrarPagos = new ArrayList<>();
        itemRegistrarPagos.add(new ItemRegistrarPago("Pagaré de", "Monto"));
        for (PagosConductor.LD ld : datosPago) {
            double valor = (ld.getDeuda() - ld.getObtenerPagoDescuento()) + ld.getMoraImpuestos();
            itemRegistrarPagos.add(new ItemRegistrarPago(ld.getDescripccion(), utilidades.dosDecimales(RegistrarPagoActivity.this,valor).concat(ejecutarMoneda())));
        }
        AdaptadorTablaRegistrarPago adaptadorTablaRegistrarPago = new AdaptadorTablaRegistrarPago(this, itemRegistrarPagos);
        listaVerificaPago.setAdapter(adaptadorTablaRegistrarPago);

        tvNumeroTicket.setText(etNumeroTicket.getText().toString());
        tvFechaPago.setText(etFechaPago.getText().toString());
        tvMontoPago.setText(etMontoPago.getText().toString());

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = v.getId();
                if (i == R.id.btn_cancelar) {
                    if (dialogoVerificaPago != null && dialogoVerificaPago.isShowing()) {
                        dialogoVerificaPago.dismiss();
                    }

                } else if (i == R.id.btn_aceptar) {
                    if (dialogoVerificaPago != null && dialogoVerificaPago.isShowing()) {
                        dialogoVerificaPago.dismiss();
                    }
                    dialogoReafirmacion();

                }
            }
        };
        btnCancelar.setOnClickListener(clickListener);
        btnAceptar.setOnClickListener(clickListener);
        builder.setView(v);

        return builder.create();
    }

    private static final int MY_CAMERA_REQUEST_CODE = 100;

    public void provarPersmisosCamara() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_CAMERA_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    utilidades.customToast(this, "Ahora puede tomar una foto.");
                } else {
                    utilidades.customToast(this, "Si no activa el permiso de la cámara no podra adjuntar un archivo de imagen.");
                }
                return;
            }
        }
    }

    public void dialogoReafirmacion() {
        Builder builder = new Builder(this);
        builder.setMessage("Esta seguro de registrar su pago.")
                .setTitle("Alerta")
                .setPositiveButton("Si", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Log.e(TAG, "onClick: " + lIdpago());
                        registrarPago.registrar_pago(VariablesGlobales.NUM_ID_APLICATIVO,
                                spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0),
                                etNumeroTicket.getText().toString(),
                                spLogin.getInt(VariablesGlobales.ID_USUARIO, 0),
                                adaptadorEntidadFinanciera.getEntidadFinanciera(spnEntidadFinanciera.getSelectedItemPosition()).getIdEntidad(),
                                Double.parseDouble(montoDecuento),
                                datosPago.get(0).getImpuestos(),
                                Double.parseDouble(montoPago),
                                lIdpago(),
                                lMeses(),
                                lMora(),
                                lMoraImpuesto(),
                                lDescuento(),
                                lPago());
                        mostrarEspera("Registrando su pago en el sistema.\nEspere por favor.");
                    }
                })
                .setNegativeButton("No", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();


    }


    public int idEntidadRegistro = 0;

    @Override
    public void respuestaRegistrarpago(String dato) {
        if (dato != null) {
            try {
                JSONObject object = new JSONObject(dato);
                if (object.has("en")) {
                    if (object.getInt("en") == 1) {
                        idEntidadRegistro = object.getInt("idEntidadRegistro");
                        BitmapDrawable drawable = (BitmapDrawable) imgVaucher.getDrawable();
                        Bitmap bitmapEnvio = drawable.getBitmap();
                        persistImage(bitmapEnvio);
                    } else {
                        mensajeDialogo(object.getString("m"));
                    }
                } else {
                    ocultarEspera();
                    mensajeDialogo("No se pudo enviar intente nuevamente.");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                ocultarEspera();
                mensajeDialogo("No se pudo enviar intente nuevamente.");
            }
        } else {
            ocultarEspera();
            mensajeDialogo("No se pudo enviar intente nuevamente.");
        }
    }


    @Override
    public void respuestaEnviarImagen(String dato, String nombreImagen) {
        if (dato != null) {
            try {
                JSONObject object = new JSONObject(dato);
                if (object.has("en")) {
                    if (object.getInt("en") == 1) {
                        registrarPago.registrar_tikect(
                                idEntidadRegistro,
                                etNumeroTicket.getText().toString(),
                                nombreImagen,
                                Double.parseDouble(montoPago),
                                spLogin.getString(VariablesGlobales.PLACA_VEHICULO, ""),
                                spLogin.getString(VariablesGlobales.NOMBRES, "") + " " + spLogin.getString(VariablesGlobales.APELLIDOS, "")
                        );
                    } else {
                        mensajeDialogo(object.getString("m"));
                    }
                } else {
                    ocultarEspera();
                    mensajeDialogo("No se pudo enviar intente nuevamente.");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                ocultarEspera();
                mensajeDialogo("No se pudo enviar intente nuevamente.");
            }
        } else {
            ocultarEspera();
            mensajeDialogo("No se pudo enviar intente nuevamente.");
        }
    }

    @Override
    public void respuestaRegistrarTicket(String dato) {
        ocultarEspera();
        if (dato != null) {
            try {
                JSONObject object = new JSONObject(dato);
                if (object.has("en")) {
                    if (object.getInt("en") == 1) {
                        Builder builder = new Builder(this);
                        builder.setMessage(object.getString("m"))
                                .setCancelable(false)
                                .setTitle("Información")
                                .setPositiveButton("Aceptar", new OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        finish();
                                    }
                                }).show();
                    } else {
                        mensajeDialogo(object.getString("m"));
                    }
                } else {
                    mensajeDialogo("No se pudo enviar intente nuevamente.");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                mensajeDialogo("No se pudo enviar intente nuevamente.");
            }
        } else {
            mensajeDialogo("No se pudo enviar intente nuevamente.");
        }
    }

    public void mensajeDialogo(String mensaje) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setMessage(mensaje)
                .setTitle("Información")
                .setPositiveButton("Aceptar", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    private String lIdpago() {
        String dato = "";
        for (PagosConductor.LD ld : datosPago) {
            dato = dato.concat(String.valueOf(ld.getIdDeuda()).concat(","));
        }
        return dato.substring(0, dato.length() - 1);
    }

    private String lIdpagoImagen() {
        String dato = "P_";
        for (PagosConductor.LD ld : datosPago) {
            dato = dato.concat(String.valueOf(ld.getIdDeuda()).concat("_"));
        }
        return dato.substring(0, dato.length() - 1);
    }

    private String lMeses() {
        String dato = "";
        for (PagosConductor.LD ld : datosPago) {
            dato = dato.concat(String.valueOf(ld.getMeses()).concat(","));
        }
        return dato.substring(0, dato.length() - 1);
    }

    private String lMora() {
        String dato = "";
        for (PagosConductor.LD ld : datosPago) {
            dato = dato.concat(utilidades.dosDecimales(RegistrarPagoActivity.this,ld.getMora()).concat(","));
        }
        return dato.substring(0, dato.length() - 1);
    }

    private String lMoraImpuesto() {
        String dato = "";
        for (PagosConductor.LD ld : datosPago) {
            dato = dato.concat(utilidades.dosDecimales(RegistrarPagoActivity.this,ld.getMoraImpuestos()).concat(","));
        }
        return dato.substring(0, dato.length() - 1);
    }

    private String lDescuento() {
        String dato = "";
        for (PagosConductor.LD ld : datosPago) {
            dato = dato.concat(utilidades.dosDecimales(RegistrarPagoActivity.this,ld.getObtenerPagoDescuento()).concat(","));
        }
        return dato.substring(0, dato.length() - 1);
    }

    private String lPago() {
        String dato = "";
        for (PagosConductor.LD ld : datosPago) {
            double valor = (ld.getDeuda() - ld.getObtenerPagoDescuento()) + ld.getMoraImpuestos();
            dato = dato.concat(utilidades.dosDecimales(RegistrarPagoActivity.this,valor).concat(","));
        }
        return dato.substring(0, dato.length() - 1);
    }

    @OnClick({R2.id.btn_registrar, R2.id.btn_archivo, R2.id.img_btn_foto, R2.id.et_fecha_pago})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.btn_archivo) {
            if (etNumeroTicket.getText().toString().isEmpty()) {
                Toast.makeText(this, "Ingrese el número de documento para adjuntar un archivo.", Toast.LENGTH_LONG).show();
                return;
            }
            isImagenSeleccionada = false;
            imgVaucher.setImageBitmap(null);
            mCurrentPhotoPath = null;
            Intent intentPicture = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            startActivityForResult(intentPicture, SELECT_PICTURE);

        } else if (i == R.id.img_btn_foto) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            } else {
                if (etNumeroTicket.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Ingrese el número de documento para tomar la foto.", Toast.LENGTH_LONG).show();
                    return;
                }
                isImagenSeleccionada = false;
                imgVaucher.setImageBitmap(null);
                lanzarCamara(TAKE_PICTURE);
            }

        } else if (i == R.id.et_fecha_pago) {
            definirTiempo();

        } else if (i == R.id.btn_registrar) {
            Log.e(TAG, "LISTAS A ENVIAR : " + lIdpago() + "***" +
                    lMeses() + "***" +
                    lMora() + "***" +
                    lMoraImpuesto() + "***" +
                    lDescuento() + "***" +
                    lPago());
            if (etNumeroTicket.getText().toString().isEmpty()) {
                mensajeDialogo("Por favor primero ingrese el numero de ticket del deposito.");
                etNumeroTicket.requestFocus();
                return;
            }
            if (etFechaPago.getText().toString().isEmpty()) {
                mensajeDialogo("Por favor primero elija la fecha en que realizo el deposito.");
                etFechaPago.requestFocus();
                return;
            }
            if (utilidades.compararFechas(etFechaPago.getText().toString(), "yyyy/MM/dd")) {
                Toast.makeText(this, "Defina una fecha anterior o igual a la actual.", Toast.LENGTH_LONG).show();
                etFechaPago.setText("");
                definirTiempo();
                etFechaPago.requestFocus();
                return;
            }
            if (isImagenSeleccionada) {
                dialogoVerificaPago = createDialogoVerifica();
                dialogoVerificaPago.show();
                Log.e(TAG, "onViewClicked: " + montoDecuento);
            } else {
                mensajeDialogo("Por favor primero elija la imagen o foto del recibo de pago.");
            }


        }
    }


    /**
     * Sirve pra definir un tiempo para la gestión inicio y fin
     */

    public void definirTiempo() {
        new SlideDateTimePicker.Builder(getSupportFragmentManager())
                .setListener(new SlideDateTimeListener() {
                    @Override
                    public void onDateTimeSet(Date date) {
                        etFechaPago.setText(new Utilidades().getTimeFormat(date));
                    }

                    @Override
                    public void onDateTimeCancel() {
                        Toast.makeText(RegistrarPagoActivity.this, "Usted no a escojido aun una fecha y hora de compromiso.", Toast.LENGTH_LONG).show();
                    }
                })
                .setInitialDate(new Date())
                .setIs24HourTime(true)
                .build()
                .show();
    }

    public Camera.Size obtenerResolucion() {
        Camera mCamera = Camera.open();
        Camera.Parameters params = mCamera.getParameters();
        List<Camera.Size> sizes = params.getSupportedPictureSizes();
        Camera.Size mSize = null;
        boolean isPrimera = true;
        for (Camera.Size size : sizes) {
            if (isPrimera) {
                isPrimera = false;
                mSize = size;
            } else {
                if (mSize.width > size.width && mSize.height > size.height) {
                    mSize = size;
                }
            }
        }
        mCamera.stopPreview();
        mCamera.release();
        Log.e(TAG, "obtenerResolucion: " + mSize.width + " " + mSize.height);
        return mSize;

    }

    /**
     * Lanza la camara para tomar la fotografia y dejarla en memoria y no pierda los pixeles.
     *
     * @param idEvento
     */
    public void lanzarCamara(int idEvento) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Log.i(TAG, "IOException");
            }
            if (photoFile != null) {
                Camera.Size tamanio = obtenerResolucion();
                int alto = tamanio.width;
                int ancho = tamanio.height;
                cameraIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, alto * ancho);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, idEvento);
            }
        }
    }

    public void guardarDatoImagen(String dato) {
        SharedPreferences prefs = getSharedPreferences("IMAGEN", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("ruta", dato);
        editor.apply();
    }

    public String obtenerDatoImagen() {
        SharedPreferences prefs = getSharedPreferences("IMAGEN", Context.MODE_PRIVATE);
        return prefs.getString("ruta", null);
    }

    private File createImageFile() throws IOException {
        String imageFileName = lIdpagoImagen();
        Log.e(TAG, "createImageFile: " + imageFileName);
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        guardarDatoImagen(mCurrentPhotoPath);
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Uri selectedImage = data.getData();
                                if (selectedImage != null) {
                                    String marcaAgua = textoMarcaAgua();
                                    try {
                                        Bitmap bitmap = new TratarImagenesGiro().handleSamplingAndRotationBitmap(RegistrarPagoActivity.this, selectedImage);
                                        Bitmap bitFin = marcaDeAguaText(marcaAgua, bitmap);
                                        imgVaucher.setImageBitmap(bitFin);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    isImagenSeleccionada = true;
                                } else {
                                    Toast.makeText(RegistrarPagoActivity.this, "No se selecciono ningun archivo de imagen", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }
                });
            }
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "No se selecciono ningun archivo de imagen", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == TAKE_PICTURE) {
            if (resultCode == RESULT_OK) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (mCurrentPhotoPath == null) {
                                    mCurrentPhotoPath = obtenerDatoImagen();
                                }
                                if (mCurrentPhotoPath != null) {
                                    Uri uri = Uri.parse(mCurrentPhotoPath);
                                    String marcaAgua = textoMarcaAgua();
                                    try {
                                        Bitmap bitmap = new TratarImagenesGiro().handleSamplingAndRotationBitmap(RegistrarPagoActivity.this, uri);
                                        Bitmap bitFin = marcaDeAguaText(marcaAgua, bitmap);
                                        imgVaucher.setImageBitmap(bitFin);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    isImagenSeleccionada = true;
                                } else {
                                    Toast.makeText(RegistrarPagoActivity.this, "No se a tomado ninguna imagen", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }
                });
            }
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "No se a tomado ninguna imagen", Toast.LENGTH_LONG).show();
            }
        }
    }

    private String textoMarcaAgua(){
        return spLogin.getString(VariablesGlobales.CEDULA, " ").concat(":").concat(spLogin.getString(VariablesGlobales.PLACA_VEHICULO, " ").concat( ":\n")
                .concat(spLogin.getString(VariablesGlobales.NOMBRES, " ")).concat(" ").concat(spLogin.getString(VariablesGlobales.APELLIDOS, " ").concat(":\n")
                .concat(etNumeroTicket.getText().toString())));
    }

    private Bitmap marcaDeAguaText(String textToMarker, Bitmap entraBmp) {
        Bitmap bmp = entraBmp.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(bmp);

        Paint circle = new Paint();
        circle.setColor(Color.WHITE);
        circle.setFlags(Paint.ANTI_ALIAS_FLAG);

        Paint stroke = new Paint();
        stroke.setColor(Color.BLACK);
        stroke.setFlags(Paint.ANTI_ALIAS_FLAG);
        Paint text = new Paint(Paint.ANTI_ALIAS_FLAG);
        text.setColor(Color.RED);
        text.setTextSize(14 * 4);
        Rect bounds = new Rect();
        text.getTextBounds(textToMarker, 0, textToMarker.length(), bounds);
        canvas.drawText(textToMarker, 20, 60, text);

        return bmp;

    }

    /**
     * registrar pago
     * foto
     * ticke
     *
     * @param bitmap
     */


    private void persistImage(Bitmap bitmap) {
        File imageFile = null;
        try {
            imageFile = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }

//        mostrarCasillaCancelar.uploadImagen(imageFile.getAbsolutePath(), imageFile.getName());
        registrarPago.uploadImagen(imageFile.getAbsolutePath(), lIdpagoImagen());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (!etNumeroTicket.getText().toString().isEmpty()) {
                    confirmacionSalir();
                    return true;
                }
                if (!etFechaPago.getText().toString().isEmpty()) {
                    confirmacionSalir();
                    return true;
                }
                if (!etMontoPago.getText().toString().isEmpty()) {
                    confirmacionSalir();
                    return true;
                }
                if (isImagenSeleccionada) {
                    confirmacionSalir();
                    return true;
                }
                finish();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void confirmacionSalir() {
        Builder builder = new Builder(this);
        builder.setMessage("La información que ingreso se perderá y tendrá que volver a realizar el proceso de ingreso de los datos, desea salir ahora.")
                .setTitle("Alerta")
                .setPositiveButton("Si", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                })
                .setNegativeButton("No", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    /**
     * Muestra un dialogo de espera de cualquier activiadad de donde sea llamado
     *
     * @param titulo
     */
    public void mostrarEspera(String titulo) {
        pDialogo = new ProgressDialog(this);
        pDialogo.setCancelable(false);
        pDialogo.setTitle(titulo);
        pDialogo.show();
    }

    /**
     * Oculta el dialogo del sistema.
     */
    public void ocultarEspera() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (pDialogo != null) {
                            if (pDialogo.isShowing() && !isFinishing()) {
                                pDialogo.dismiss();
                            }
                        }
                    }
                }, 100);
            }
        });
    }

    public String ejecutarMoneda() {
        String dataRetorno = " Usd";
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 12:
                                if (objetoConfig.getInt("h") == 1) {
                                    dataRetorno = " ".concat(objetoConfig.getString("nb"));
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                Log.e(TAG, "presentarValorCobro: ", e);
            }
        }
        return dataRetorno;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("lista", datosPago);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        datosPago = savedInstanceState.getParcelableArrayList("lista");
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
        provarPersmisosCamara();
    }
}
