package com.kradac.conductor.vista;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.RvAdapterComentarios;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionComentarios;
import com.kradac.conductor.modelo.RespuestaComentarios;
import com.kradac.conductor.presentador.PresentadorComentarios;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ComentariosActivity extends AppCompatActivity implements OnComunicacionComentarios {

    @BindView(R2.id.appbar)
    Toolbar appbar;
    @BindView(R2.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R2.id.swipeRefresh)
    SwipyRefreshLayout swipeRefresh;
    @BindView(R2.id.tv_sin_datos)
    TextView tvSinDatos;
    @BindView(R2.id.perfil_default)
    CircularImageView perfilDefault;
    @BindView(R2.id.tv_name_conductor)
    TextView tvNameConductor;
    @BindView(R2.id.tv_fecha)
    TextView tvFecha;
    private PresentadorComentarios presentadorComentarios;
    private SharedPreferences spobtener;
    private RvAdapterComentarios rvAdapterComentarios;
    private RecyclerView.LayoutManager lManager;
    private Bitmap imagen;
    private String usuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentarios);
        ButterKnife.bind(this);
        setSupportActionBar(appbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle("Comentarios");
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            imagen = bundle.getParcelable("imagen");
            usuario = bundle.getString("usuario");
            if (usuario != null && imagen != null) {
                tvNameConductor.setText("Conductor: ".concat(usuario));
                perfilDefault.setImageBitmap(imagen);
                tvFecha.setText("Comentarios hasta: ".concat(new SimpleDateFormat("dd-MM-yyyy").format(new Date())));
            }
        }
        lManager = new LinearLayoutManager(this);
        myRecyclerView.setLayoutManager(lManager);
        rvAdapterComentarios = new RvAdapterComentarios(this, new ArrayList<RespuestaComentarios.Ob>());
        myRecyclerView.setAdapter(rvAdapterComentarios);
        spobtener = this.getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        presentadorComentarios = new PresentadorComentarios(this);
        swipeRefresh.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                swipeRefresh.setRefreshing(true);
            }
        });

        swipeRefresh.setColorSchemeResources(
                R.color.rojo,
                R.color.azul_dinero_electronico,
                R.color.colorPrimary,
                R.color.verde
        );

        swipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                swipeRefresh.setRefreshing(true);
                presentadorComentarios.consularMensajesCliente(spobtener.getInt(VariablesGlobales.ID_USUARIO, 0));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // ID del boton
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void RespuestaObtenerComentarios(final RespuestaComentarios respuestaComentarios) {
        swipeRefresh.setRefreshing(false);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (respuestaComentarios != null) {
                    switch (respuestaComentarios.getEn()) {
                        case 1:
                            if (respuestaComentarios.getOb() != null && respuestaComentarios.getOb().size() > 0) {
                                rvAdapterComentarios.addAll((ArrayList<RespuestaComentarios.Ob>) respuestaComentarios.getOb());
                                tvSinDatos.setVisibility(View.GONE);
                            }
                            break;
                        case 2:
                            if (!isFinishing()) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(ComentariosActivity.this);
                                builder.setMessage(respuestaComentarios.getM())
                                        .setTitle("Información")
                                        .setCancelable(false)
                                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                finish();
                                            }
                                        }).show();
                            } else {
                                finish();
                            }

                            tvSinDatos.setVisibility(View.VISIBLE);
                            break;
                    }
                } else {
                    tvSinDatos.setVisibility(View.VISIBLE);
                    Toast.makeText(ComentariosActivity.this, "No se puede recuperar la información intente mas tarde", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
