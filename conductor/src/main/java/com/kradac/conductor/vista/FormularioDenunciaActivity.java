package com.kradac.conductor.vista;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.TratarImagenesGiro;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.OnComunicacionReportarPirata;
import com.kradac.conductor.presentador.ReportarPirataPresentador;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FormularioDenunciaActivity extends AppCompatActivity implements View.OnClickListener, OnComunicacionReportarPirata {
    private static final String TAG = FormularioDenunciaActivity.class.getName();
    private static final int TAKE_PICTURE = 1;
    private static final int SELECT_PICTURE = 2;

    @BindView(R2.id.btn_enviar_denuncia)
    Button btnEnviarDenuncia;
    @BindView(R2.id.appbar)
    Toolbar appbar;
    @BindView(R2.id.btn_camara)
    Button btnCamara;
    @BindView(R2.id.btn_galeria)
    Button btnGaleria;
    private ImageView imgDenuncia;
    private ReportarPirataPresentador reportarPirataPresentador;
    private SharedPreferences preferences;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_denuncia);
        ButterKnife.bind(this);
        setSupportActionBar(appbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Reporte pirata");
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        imgDenuncia = (ImageView) findViewById(R.id.img_denuncia);
        preferences = getSharedPreferences("login", Context.MODE_PRIVATE);
        reportarPirataPresentador = new ReportarPirataPresentador(this, this);
        tomarFoto();
    }

    public void tomarFoto() {
        imgDenuncia.setImageBitmap(null);
        lanzarCamara(TAKE_PICTURE);
    }

    /**
     * Lanza la camara para tomar la fotografia y dejarla en memoria y no pierda los pixeles.
     *
     * @param idEvento
     */
    public void lanzarCamara(int idEvento) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Log.i(TAG, "IOException");
            }
            if (photoFile != null) {
                Camera.Size tamanio = obtenerResolucion();
                int alto = tamanio.width;
                int ancho = tamanio.height;
                cameraIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT, alto * ancho);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, idEvento);
            }
        }
    }

    public Camera.Size obtenerResolucion() {
        Camera mCamera = Camera.open();
        Camera.Parameters params = mCamera.getParameters();
        List<Camera.Size> sizes = params.getSupportedPictureSizes();
        Camera.Size mSize = null;
        boolean isPrimera = true;
        for (Camera.Size size : sizes) {
            if (isPrimera) {
                isPrimera = false;
                mSize = size;
            } else {
                if (mSize.width > size.width && mSize.height > size.height) {
                    mSize = size;
                }
            }
        }
        mCamera.stopPreview();
        mCamera.release();
        Log.e(TAG, "obtenerResolucion: " + mSize.width + " " + mSize.height);
        return mSize;

    }

    private String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        String imageFileName = "Ktaxi_conductor_denuncia" + new Date().getTime();
        Log.e(TAG, "createImageFile: " + imageFileName);
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }


    private EditText getEtPlaca() {
        return (EditText) findViewById(R.id.et_placa);
    }

    private EditText getEtColor() {
        return (EditText) findViewById(R.id.et_color);
    }

    private EditText getEtComentario() {
        return (EditText) findViewById(R.id.et_comentario);
    }


    @Override
    public void repsuestaDenuncia(String data) {
        if (data != null) {
            try {
                JSONObject object = new JSONObject(data);
                if (object.has("en")) {
                    if (object.getInt("en") == 1) {
                        reportarPirataPresentador.bitmapExtrae(persistImage(bitmapGloabal), String.valueOf(object.getInt("id")));
                    } else {
                        Toast.makeText(this, "No se puede enviar la denuncia intente nuevamente.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(this, "No se puede enviar la denuncia intente nuevamente.", Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, "No se puede enviar la denuncia intente nuevamente.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void respuestaFoto(String data) {
        ocultarDialogoEspera();
        try {
            JSONObject object = new JSONObject(data);
            if (object.has("en")) {
                if (object.getInt("en") == 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(object.getString("m"))
                            .setTitle("Información")
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    btnEnviarDenuncia.setEnabled(true);
                                    finish();
                                }
                            }).show();
                } else {
                    Toast.makeText(this, "No se puede enviar la denuncia intente nuevamente.", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(this, "No se puede enviar la denuncia intente nuevamente.", Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            Toast.makeText(this, "No se puede enviar la denuncia intente nuevamente.", Toast.LENGTH_LONG).show();
        }
    }

    @OnClick({R2.id.btn_enviar_denuncia, R2.id.btn_camara, R2.id.btn_galeria})
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btn_camara) {
            tomarFoto();

        } else if (i == R.id.btn_galeria) {
            isImagenSeleccionada = false;
            imgDenuncia.setImageBitmap(null);
            mCurrentPhotoPath = null;
            Intent intentPicture = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            startActivityForResult(intentPicture, SELECT_PICTURE);

        } else if (i == R.id.btn_enviar_denuncia) {
            if (!isImagenSeleccionada) {
                Toast.makeText(this, "Sin archivo de imagen no se puede enviar la denuncia.", Toast.LENGTH_SHORT).show();
                tomarFoto();
                return;
            }
            if (getEtPlaca().getText().toString().isEmpty()) {
                getEtPlaca().requestFocus();
                Toast.makeText(this, "Ingrese una placa para continuar.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (getEtPlaca().getText().toString().trim().length() < 6) {
                getEtPlaca().requestFocus();
                Toast.makeText(this, "Ingrese una placa válida.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (getEtColor().getText().toString().isEmpty()) {
                getEtColor().requestFocus();
                Toast.makeText(this, "Ingrese el color para continuar.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (getEtColor().getText().toString().trim().length() < 3) {
                getEtColor().requestFocus();
                Toast.makeText(this, "Ingrese un color válido.", Toast.LENGTH_SHORT).show();
                return;
            }
            if (getEtComentario().getText().toString().trim().length() < 10) {
                getEtComentario().requestFocus();
                Toast.makeText(this, "El mínimo de caracteres permitidos son diez..", Toast.LENGTH_SHORT).show();
                return;
            }

            Location locationEs;
            LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(FormularioDenunciaActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(FormularioDenunciaActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Active los permisos de localización", Toast.LENGTH_LONG).show();
                return;
            } else {
                locationEs = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (locationEs == null) {
                    locationEs = mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                if (locationEs == null) {
                    locationEs = mlocManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                }
            }
            if (locationEs != null) {
                reportarPirataPresentador.getReportarPirata(preferences.getInt("idUsuario", 0), preferences.getInt("idVehiculo", 0), getEtPlaca().getText().toString(), getEtColor().getText().toString(), getEtComentario().getText().toString(), locationEs.getLatitude(), locationEs.getLongitude());
            } else {
                reportarPirataPresentador.getReportarPirata(preferences.getInt("idUsuario", 0), preferences.getInt("idVehiculo", 0), getEtPlaca().getText().toString(), getEtColor().getText().toString(), getEtComentario().getText().toString(), 0, 0);
            }
            mostrarDialogoEspera();
            btnEnviarDenuncia.setEnabled(false);

        }
    }

    public void mostrarDialogoEspera() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Enviando datos...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void ocultarDialogoEspera() {
        if (pDialog != null) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // ID del boton
                mensajeSalir("¿Desea salir del reporte de pirata?. Perdera toda la información registrada.");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void mensajeSalir(String mensaje) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(mensaje)
                .setTitle("Alerta")
                .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                        overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        if (!isFinishing()) {
            builder.show();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            mensajeSalir("¿Desea salir del reporte de pirata?. Perderá toda la información registrada.");
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    private boolean isImagenSeleccionada = false;
    private Bitmap bitmapGloabal;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {

        if (requestCode == SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                if (selectedImage != null) {
                    try {
                        bitmapGloabal = new TratarImagenesGiro().handleSamplingAndRotationBitmap(FormularioDenunciaActivity.this, selectedImage);
                        imgDenuncia.setImageBitmap(bitmapGloabal);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    isImagenSeleccionada = true;
                } else {
                    isImagenSeleccionada = true;
                    Toast.makeText(FormularioDenunciaActivity.this, "No se selecciono ningun archivo de imagen", Toast.LENGTH_LONG).show();
                }
            }
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "No se seleccionó ningún archivo de imagen", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == TAKE_PICTURE) {
            if (resultCode == RESULT_OK) {
                if (mCurrentPhotoPath != null) {
                    Uri uri = Uri.parse(mCurrentPhotoPath);
                    try {
                        bitmapGloabal = new TratarImagenesGiro().handleSamplingAndRotationBitmap(FormularioDenunciaActivity.this, uri);
                        imgDenuncia.setImageBitmap(bitmapGloabal);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    isImagenSeleccionada = true;
                } else {
                    isImagenSeleccionada = false;
                    Toast.makeText(FormularioDenunciaActivity.this, "No se ha tomado ninguna imagen", Toast.LENGTH_LONG).show();
                }
            }
            if (resultCode == RESULT_CANCELED) {
                mCurrentPhotoPath = null;
                Toast.makeText(this, "No se a tomado ninguna imagen", Toast.LENGTH_LONG).show();
            }
        }
    }

    private String persistImage(Bitmap bitmap) {
        File imageFile = null;
        try {
            imageFile = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(TAG, "persistImage: ", e);
        }
        Log.e(TAG, "persistImage: " + imageFile.getAbsolutePath());
        return imageFile.getAbsolutePath();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }

}
