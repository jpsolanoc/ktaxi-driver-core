package com.kradac.conductor.vista;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.modelo.DatosNotificacion;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetalleBoletin extends AppCompatActivity {

    @BindView(R2.id.imv_foto_boletin)
    ImageView imvFotoBoletin;
    @BindView(R2.id.tv_asunto)
    TextView tvAsunto;
    @BindView(R2.id.tv_mesaje)
    TextView tvMesaje;
    @BindView(R2.id.accion_salir)
    Button accionSalir;
    @BindView(R2.id.tv_ver_detalle)
    TextView tvVerDetalle;
    private DatosNotificacion datosNotificacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_boletin);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            datosNotificacion = bundle.getParcelable("datosNotificacion");
            tvAsunto.setText(datosNotificacion.getAsunto());
            tvMesaje.setText(datosNotificacion.getBoletin());
            if (datosNotificacion.getUrl() != null&&!datosNotificacion.getUrl().isEmpty()) {
                subrayarTextoConAcciones(datosNotificacion.getVinculo(), tvVerDetalle);
                tvVerDetalle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            Uri uri = Uri.parse(datosNotificacion.getUrl());
                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        } catch (ActivityNotFoundException ignore) {
                        }
                    }
                });
            } else {
                tvVerDetalle.setVisibility(View.GONE);
            }
            cargarImagen(imvFotoBoletin, datosNotificacion.getImgBoletin(), this);
        }

    }
    public void subrayarTextoConAcciones(String text, TextView tvHide) {
        SpannableString spanString = new SpannableString(text);
        spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
        tvHide.setText(spanString);
    }

    private void cargarImagen(final ImageView imageView, String url, Context context) {
        DisplayImageOptions options = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.RGB_565).cacheOnDisk(true).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        imageLoader.displayImage(url, imageView, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                imageView.setImageBitmap(loadedImage);
            }
        });
    }

    @OnClick(R2.id.accion_salir)
    public void onViewClicked() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }
}
