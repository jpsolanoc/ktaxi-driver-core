package com.kradac.conductor.vista;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.RvAdapterPagos;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionPago;
import com.kradac.conductor.modelo.PagosConductor;
import com.kradac.conductor.presentador.PagoPresentador;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PagosActivity extends AppCompatActivity implements OnComunicacionPago {

    public Dialog pDialogo;

    private static final String TAG = PagosActivity.class.getName();
    @BindView(R2.id.appbar)
    Toolbar appbar;
    @BindView(R2.id.list_item_pago)
    RecyclerView listItemPago;
    @BindView(R2.id.btn_pagar_deudas)
    Button btnPagarDeudas;
    private RecyclerView.LayoutManager lManager;

    private RvAdapterPagos rvAdapterPagos;

    private PagoPresentador pagoPresentador;

    private SharedPreferences spLogin;
    private Utilidades utilidades = new Utilidades();
    private Window win;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagos);
        win = getWindow();
        ButterKnife.bind(this);
        utilidades = new Utilidades();
        spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        setSupportActionBar(appbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle("Pago");
        }
        lManager = new LinearLayoutManager(this);
        listItemPago.setLayoutManager(lManager);
        rvAdapterPagos = new RvAdapterPagos(this, new ArrayList<PagosConductor.LD>(), this);
        listItemPago.setAdapter(rvAdapterPagos);
        datosPago = new ArrayList<>();
        pagoPresentador = new PagoPresentador(this);
        cambiarPantallaEncendido();
        pagoPresentador.getListarDeudas(VariablesGlobales.NUM_ID_APLICATIVO, spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0), spLogin.getInt(VariablesGlobales.ID_USUARIO, 0));
        mostrarEspera("Cargando pagos pendientes.");

    }

    public void cambiarPantallaEncendido() {
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
    }

    @OnClick(R2.id.btn_pagar_deudas)
    public void onViewClicked() {
        if (datosPago != null && datosPago.size() > 0) {
            Intent intent = new Intent(this, RegistrarPagoActivity.class);
            intent.putExtra("datosPago", datosPago);
            startActivity(intent);
            finish();
        } else {
            mensajeDialogo("Usted debe seleccionar la deuda que desea cancelar.");
        }
    }

    public void mensajeDialogo(String mensaje) {
        Builder builder = new Builder(this);
        builder.setMessage(mensaje)
                .setTitle("Información")
                .setPositiveButton("Aceptar", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pago, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            finish();
            return true;
        } else if (i == R.id.informacion) {
            finish();
            Intent intent = new Intent(PagosActivity.this, NavegadorWeb.class);
            intent.putExtra("pagina", "http://ktaxifacilsegurorapido.kradac.com/tutos/pagos/");
            intent.putExtra("titulo", "Tutorial");
            startActivity(intent);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void repsuestaListarDeudas(PagosConductor pagosConductor) {
        ocultarEspera();
        if (pagosConductor != null) {
            switch (pagosConductor.getEn()) {
                case 1:
                    if (pagosConductor.getM() != null) {
                        dialogoInformativo(pagosConductor.getM());
                    }
                    rvAdapterPagos.addAll((ArrayList<PagosConductor.LD>) pagosConductor.getLD());
                    break;
                default:
                    Builder builder = new Builder(this);
                    builder.setMessage(pagosConductor.getM())
                            .setTitle("Información")
                            .setCancelable(false)
                            .setPositiveButton("Aceptar", new OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                }
                            }).show();
                    break;
            }
        } else {
            Toast.makeText(this, "No se pudo obtener la información", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public void dialogoInformativo(String mensaje) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(mensaje)
                .setTitle("Información")
                .setCancelable(false)
                .setPositiveButton("Aceptar", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Cancelar", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                }).setNeutralButton("Tutorial", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
                Intent intent = new Intent(PagosActivity.this, NavegadorWeb.class);
                intent.putExtra("pagina", "http://ktaxifacilsegurorapido.kradac.com/tutos/pagos/");
                intent.putExtra("titulo", "Tutorial");
                startActivity(intent);
            }
        }).show();


    }


    ArrayList<PagosConductor.LD> datosPago;
    double valorAPagar = 0;

    @Override
    public void clickDeudaPendiente(PagosConductor.LD deudaPendiente, boolean isChecked) {
        if (isChecked) {
            valorAPagar = valorAPagar + (deudaPendiente.getDeuda() - deudaPendiente.getObtenerPagoDescuento()) + deudaPendiente.getMoraImpuestos();
            btnPagarDeudas.setText("Registro de pago de:".concat(utilidades.dosDecimales(PagosActivity.this,valorAPagar)));
            datosPago.add(deudaPendiente);
        } else {
            for (PagosConductor.LD dP : datosPago) {
                if (dP.getIdDeuda() == deudaPendiente.getIdDeuda()) {
                    valorAPagar = valorAPagar - (deudaPendiente.getDeuda() - deudaPendiente.getObtenerPagoDescuento()) - deudaPendiente.getMoraImpuestos();
                    btnPagarDeudas.setText("Registro de pago de:".concat(utilidades.dosDecimales(PagosActivity.this,valorAPagar)));
                    datosPago.remove(deudaPendiente);
                    break;
                }
            }
        }
    }

    /**
     * Muestra un dialogo de espera de cualquier activiadad de donde sea llamado
     *
     * @param titulo
     */
    public void mostrarEspera(String titulo) {
        pDialogo = new ProgressDialog(this);
        pDialogo.setCancelable(false);
        pDialogo.setTitle(titulo);
        pDialogo.show();
    }

    /**
     * Oculta el dialogo del sistema.
     */
    public void ocultarEspera() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (pDialogo != null) {
                            if (pDialogo.isShowing() && !isFinishing()) {
                                pDialogo.dismiss();
                            }
                        }
                    }
                }, 100);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }


}
