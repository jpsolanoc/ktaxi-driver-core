package com.kradac.conductor.vista;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionCompraServiciosMapa;
import com.kradac.conductor.modelo.SaldoKtaxi;
import com.kradac.conductor.presentador.SaldoKtaxiPresentador;
import com.kradac.conductor.service.ServicioSockets;

import net.glxn.qrgen.android.QRCode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapaSaldoKtaxi extends AppCompatActivity implements OnComunicacionCompraServiciosMapa {


    private static final String TAG = MapaSaldoKtaxi.class.getName();
    @BindView(R2.id.appbar)
    Toolbar appbar;
    @BindView(R2.id.btn_comprar)
    Button btnComprar;
    @BindView(R2.id.btn_waze)
    ImageButton btnWaze;
    @BindView(R2.id.img_btn_location)
    ImageButton imgBtnLocation;
    @BindView(R2.id.imb_zoom_mas)
    ImageButton imbZoomMas;
    @BindView(R2.id.imb_zoom_menos)
    ImageButton imbZoomMenos;
    //    private MapView mapView;
    private ServicioSockets servicioSocket;
    private boolean mBound;
    private SaldoKtaxi.CompraServicios.LS dato;
    //    private MapboxMap mapBox;
    private Location locationGlobal;

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 4;

    private SharedPreferences spLogin;
    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ServicioSockets.LocalBinder binder = (ServicioSockets.LocalBinder) service;
            servicioSocket = binder.getService();
            servicioSocket.registerCliente(MapaSaldoKtaxi.this, "MapaSaldoKtaxi");
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_saldo_ktaxi);
        ButterKnife.bind(this);
        setSupportActionBar(appbar);
        final ActionBar ab = getSupportActionBar();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            dato = bundle.getParcelable("CompraServicios");
        }
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle("Mapa Compra.");
            ab.setSubtitle(dato.getEmpresa());
        }
        spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
//        mapView = (MapView) findViewById(R.id.mapView);
//        mapView.onCreate(savedInstanceState);
//        mapView.getMapAsync(new OnMapReadyCallback() {
//            @Override
//            public void onMapReady(MapboxMap mapboxMap) {
//                mapBox = mapboxMap;
//                mapBoxTrazarRutas = new MapBoxTrazarRutas(MapaSaldoKtaxi.this, mapboxMap);
//                centrarMarcadorMapa();
//
//            }
//        });
        pedirDatosServidor();
        cargaClick();
    }

    private void cargaClick() {
//        mapView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                int action = motionEvent.getAction();
//                switch (action) {
//                    case MotionEvent.ACTION_DOWN: {
//                    }
//                    case MotionEvent.ACTION_UP: {
//                        switch (contadorZoom) {
//                            case 1:
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                                    imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal, getBaseContext().getTheme()));
//                                } else {
//                                    imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal));
//                                }
//                                contadorZoom = 0;
//                                modoGpsRastreo(contadorZoom, locationGlobal);
//                                break;
//                            case 2:
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                                    imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal, getBaseContext().getTheme()));
//                                } else {
//                                    imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.gps_normal));
//                                }
//                                contadorZoom = 3;
//                                modoGpsRastreo(contadorZoom, locationGlobal);
//                                break;
//                            case 6:
//                                if (locationGlobal != null) {
//                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                                        imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo, getBaseContext().getTheme()));
//                                    } else {
//                                        imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo));
//                                    }
//                                    contadorZoom = 1;
//                                    modoGpsRastreo(contadorZoom, locationGlobal);
//                                }
//                                break;
//                        }
//                    }
//                }
//                return false;
//            }
//        });
    }

    // Add the mapView lifecycle to the activity's lifecycle methods
    @Override
    public void onResume() {
        super.onResume();
//        mapView.onResume();
        Intent intent = new Intent(this, ServicioSockets.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        new Utilidades().hideFloatingView(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
//        mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        mapView.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
//        mapView.onPause();
        if (mConnection != null) {
            unbindService(mConnection);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
//        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        mapView.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        mapView.onSaveInstanceState(outState);
    }


    private Dialog dialogComprar;

    @Override
    public void lanzarDialogoCompra() {
        if (dialogComprar != null) {
            if (dialogComprar.isShowing()) {
                return;
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_comprar, null);
        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        final EditText etValor = v.findViewById(R.id.et_valor_pagar);
        final TextView etInformacion = v.findViewById(R.id.tv_informacion);
        etInformacion.setText("En el establecimiento " + dato.getEmpresa() + " El saldo máximo que puede usar " + total);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etValor.getText().toString().isEmpty()) {
                    Toast.makeText(MapaSaldoKtaxi.this, "Ingrese el valor que desea pagar.", Toast.LENGTH_LONG).show();
                    etValor.requestFocus();
                    return;
                }
                if (Double.parseDouble(etValor.getText().toString()) > total) {
                    Toast.makeText(MapaSaldoKtaxi.this, "El valor maximo que puede usar es " + total, Toast.LENGTH_LONG).show();
                    etValor.requestFocus();
                    return;
                }
                if (dialogComprar != null) {
                    if (dialogComprar.isShowing()) {
                        dialogComprar.dismiss();
                        dialogComprar = null;
                    }
                }
                servicioSocket.compraServicio(etValor.getText().toString(), dato.getIdEmpresa());
                esperaCobro();

            }
        });
        Button btnCancelar = v.findViewById(R.id.btn_cancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogComprar != null) {
                    if (dialogComprar.isShowing()) {
                        dialogComprar.dismiss();
                        dialogComprar = null;
                    }
                }
            }
        });

        builder.setView(v);

        builder.create();

        dialogComprar = builder.show();
    }

    @Override
    public void aunNoEnLugar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Usted no se encuentra dentro del lugar que esta intentando realizar el pago. ¿Desea realizar el pago de todas formas?.")
                .setTitle("Alerta")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        lanzarDialogoCompra();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }

    private boolean isRuta = false;
//    private MarkerView markerTaxista;

    @Override
    public void onLocation(Location location) {
//        locationGlobal = location;
//        if (dato != null && location != null) {
//            dato.setDistancia(new Utilidades().calculationByDistance(new LatLng(location.getLatitude(), location.getLongitude()),
//                    new LatLng(dato.getLatitud(), dato.getLongitud())));
//            if (dato.getDistancia() <= 0.3) {
//                dato.setLugar(true);
//            } else {
//                dato.setLugar(false);
//            }
//            if (!isRuta && mapBox != null) {
//                Position origin = Position.fromCoordinates(location.getLongitude(), location.getLatitude());
//                Position destination = Position.fromCoordinates(dato.getLongitud(), dato.getLatitud());
//                mapBoxTrazarRutas.getRoute(origin, destination, "#3887be");
//
//                IconFactory iconFactory = IconFactory.getInstance(MapaSaldoKtaxi.this);
//                Icon icon;
//                icon = iconFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_taxy_map_te));
//                markerTaxista = mapBox.addMarker(new MarkerViewOptions()
//                        .position(new LatLng(location.getLatitude(), location.getLongitude()))
//                        .icon(icon)
//                        .anchor(0.5f, 0.5f)
//                        .title("Taxista")
//                        .snippet("T: " + spLogin.getString("nombres", " ") + " " + spLogin.getString("apellidos", " ")));
//                IconFactory iconFactory2 = IconFactory.getInstance(MapaSaldoKtaxi.this);
//                Icon icon2;
//                icon2 = iconFactory2.fromBitmap(BitmapFactory.decodeResource(getResources(), R.mipmap.pin_saldo_ktaxi_llegada));
//                MarkerOptions markerOptionInicio2 = new MarkerOptions()
//                        .position(new LatLng(dato.getLatitud(), dato.getLongitud()))
//                        .setIcon(icon2)
//                        .setTitle("Destino: " + dato.getEmpresa());
//                mapBox.addMarker(markerOptionInicio2);
//                isRuta = true;
//            }
//            if (markerTaxista != null) {
//                markerTaxista.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));
//
//                markerTaxista.setRotation(location.getBearing());
//            }
//        }
    }

    @Override
    public void onRespuestaCompraServicio(final String respuesta) {
        pedirDatosServidor();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (pDialog != null) {
                    if (pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                }
                if (respuesta != null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MapaSaldoKtaxi.this);
                    builder.setMessage(respuesta)
                            .setTitle("Informacion")
                            .setCancelable(false)
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                }
                            }).show();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MapaSaldoKtaxi.this);
                    builder.setMessage("La aperación no se pudo complatar intente nuevamente.")
                            .setTitle("Información")
                            .setCancelable(true)
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }).show();
                }
            }
        });
    }


    private ProgressDialog pDialog;

    public void esperaCobro() {
        if (pDialog != null) {
            if (pDialog.isShowing()) {
                return;
            }
        }
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Espere la confirmación del pago.");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: // ID del boton
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void pedirDatosServidor() {
        new SaldoKtaxiPresentador(null, null, this).saldoConsultar(spLogin.getInt("idVehiculo", 0), spLogin.getInt("idUsuario", 0), spLogin.getInt("idCiudad", 0));
    }


    @Override
    public void repsuestaListaSaldoKtaxi(SaldoKtaxi.ItemSaldoKtaxi itemSaldoKtaxi) {
        if (itemSaldoKtaxi != null) {
            if (itemSaldoKtaxi.getLS().size() > 0) {
                totalSaldo(itemSaldoKtaxi.getLS());
            } else {
                Toast.makeText(this, "No se pudo obtener los datos del saldo.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public double total = 0;

    public double totalSaldo(List<SaldoKtaxi.ItemSaldoKtaxi.LS> datos) {
        for (SaldoKtaxi.ItemSaldoKtaxi.LS ls : datos) {
            total = total + ls.getSaldo();
        }
        return total;
    }

    public void centrarMarcadorMapa() {
        if (locationGlobal != null) {
            if (contadorZoom == 6) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo, getBaseContext().getTheme()));
                } else {
                    imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo));
                }
                contadorZoom = 1;
                modoGpsRastreo(contadorZoom, locationGlobal);
            }
        }
    }

    private int contadorZoom = 6;

    @OnClick({R2.id.btn_waze, R2.id.img_btn_location, R2.id.imb_zoom_mas, R2.id.imb_zoom_menos, R2.id.btn_comprar})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.btn_waze) {
            if (dato != null) {
                String lat = String.valueOf(dato.getLatitud());
                String lon = String.valueOf(dato.getLongitud());
                try {
                    String url = "waze://?ll=" + lat + "," + lon + "&navigate=yes";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    Intent intent =
                            new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
                    startActivity(intent);
                }
            }

        } else if (i == R.id.img_btn_location) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
                return;
            } else {
                if (locationGlobal != null) {
                    switch (contadorZoom) {
                        case 0:
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.gps_rastreo_brujula, getBaseContext().getTheme()));
                            } else {
                                imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.gps_rastreo_brujula));
                            }
                            contadorZoom = 2;
                            modoGpsRastreo(contadorZoom, locationGlobal);
                            break;
                        case 1:
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.gps_rastreo_brujula, getBaseContext().getTheme()));
                            } else {
                                imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.gps_rastreo_brujula));
                            }
                            contadorZoom = 2;
                            modoGpsRastreo(contadorZoom, locationGlobal);
                            break;
                        case 2:
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo, getTheme()));
                            } else {
                                imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo));
                            }
                            contadorZoom = 1;
                            modoGpsRastreo(contadorZoom, locationGlobal);
                            break;
                        case 3:
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo, getTheme()));
                            } else {
                                imgBtnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_gps_rastreo));
                            }
                            contadorZoom = 1;
                            modoGpsRastreo(contadorZoom, locationGlobal);
                            break;
                        default:
                            break;
                    }
                } else {
                    Toast.makeText(this, "Esperando ubicación.", Toast.LENGTH_LONG).show();
                }
            }

        } else if (i == R.id.imb_zoom_mas) {//                if (mapBox != null) {
//                    if (locationGlobal != null) {
//                        mapBox.setCameraPosition(new CameraPosition.Builder()
//                                .target(new LatLng(locationGlobal.getLatitude(), locationGlobal.getLongitude()))
//                                .zoom(mapBox.getCameraPosition().zoom + 1)
//                                .build());
//                    }
//                }

        } else if (i == R.id.imb_zoom_menos) {//                if (mapBox != null) {
//                    if (locationGlobal != null) {
//                        mapBox.setCameraPosition(new CameraPosition.Builder()
//                                .target(new LatLng(locationGlobal.getLatitude(), locationGlobal.getLongitude()))
//                                .zoom(mapBox.getCameraPosition().zoom - 1)
//                                .build());
//                    }
//                }

        } else if (i == R.id.btn_comprar) {
            if (dato.isLugar()) {
                lanzarDialogoCompra();
            } else {
                aunNoEnLugar();
            }

        }
    }

    public void modoGpsRastreo(int modoRastreo, Location location) {
        switch (modoRastreo) {
            case 0:
                break;
            case 1:
//                if (mapBox != null) {
//                    if (location.hasBearing()) {
//                        mapBox.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
//                                .target(new LatLng(location))
//                                .zoom(16)
//                                .bearing(location.getBearing())
//                                .tilt(0)
//                                .build()), 200);
//                    } else {
//                        mapBox.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
//                                .target(new LatLng(location))
//                                .zoom(16)
//                                .bearing(0)
//                                .tilt(0)
//                                .build()), 200);
//                    }
//                }
                break;
            case 2:
//                if (mapBox != null) {
//                    if (location.hasBearing()) {
//                        mapBox.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
//                                .target(new LatLng(location))
//                                .zoom(17)
//                                .bearing(location.getBearing())
//                                .tilt(30)
//                                .build()), 2000);
//                    }
//                }
                break;
            case 3:
//                agregarMarcadorBoxTaxista(location);
                break;
            default:
                break;
        }
    }

    private Dialog dialogEsperaSaldoKtaxi;

    /**
     * Espera dialogo saldo Ktaxi
     *
     * @return Diálogo
     */
    public void createEsperaSaldoKtaxiDialogo(String pin, long cronometro) {
        if (dialogEsperaSaldoKtaxi != null) {
            if (dialogEsperaSaldoKtaxi.isShowing()) {
                return;
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        builder.setCancelable(false);
        View v = inflater.inflate(R.layout.dialog_espera_saldo_ktaxi, null);
        ImageView imgCodeQr = v.findViewById(R.id.img_code_qr);
        TextView tvCodigo = v.findViewById(R.id.tv_codigo_transaccion);
        tvCodigo.setText("Código transacción: \n".concat(pin));
        final TextView tvCronometro = v.findViewById(R.id.tv_cronometro);
        new CountDownTimer(cronometro, 1000) {
            public void onTick(long millisUntilFinished) {
                int segundo = (int) millisUntilFinished / 1000;
                if (segundo < 10) {
                    tvCronometro.setText("00:".concat("0" + String.valueOf(segundo)));
                } else {
                    tvCronometro.setText("00:".concat(String.valueOf(segundo)));
                }
            }

            public void onFinish() {
                tvCronometro.setText("Expirado!");
            }
        }.start();
        tvCodigo.setText(pin);
        Bitmap codeConfirmacion = QRCode.from(pin).bitmap();
        imgCodeQr.setImageBitmap(codeConfirmacion);
        builder.setView(v);
        dialogEsperaSaldoKtaxi = builder.create();
        dialogEsperaSaldoKtaxi.show();
    }


    @Override
    public void onRespuestaCompra(final String pin, final long cronometro) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (pDialog != null) {
                    if (pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                }
                createEsperaSaldoKtaxiDialogo(pin, cronometro);
            }
        });
    }

    @Override
    public void lanzarDialogoCompra(int posicion) {

    }

    @Override
    public void aunNoEnLugar(int posicion) {

    }

    @Override
    public void respuestaServidorServicios(SaldoKtaxi.CompraServicios compraServicios) {

    }
}
