package com.kradac.conductor.vista;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.VideoView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.OnComunicacionPublicidad;
import com.kradac.conductor.service.ServicioSockets;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoPublicidadActivity extends AppCompatActivity implements OnComunicacionPublicidad {

    private static final String TAG = VideoPublicidadActivity.class.getName();
    @BindView(R2.id.videoView)
    VideoView videoView;

    private ServicioSockets servicioSocket;
    private boolean mBound;
    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ServicioSockets.LocalBinder binder = (ServicioSockets.LocalBinder) service;
            servicioSocket = binder.getService();
            servicioSocket.registerCliente(VideoPublicidadActivity.this, "VideoPublicidadActivity");
            mBound = true;
            Log.e(TAG, "onServiceConnected: " + servicioSocket.uriPath + "::::" + servicioSocket.posicion);
            if (servicioSocket.posicion > 0) {
                setUpVideoView(servicioSocket.uriPath);
                videoView.seekTo(servicioSocket.posicion);
            } else {
                setUpVideoView(servicioSocket.uriPath);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        if (mBound) {
            servicioSocket.posicion = videoView.getCurrentPosition();
            Log.e(TAG, "onPause: " + servicioSocket.posicion);
            videoView.pause();
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, ServicioSockets.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        new Utilidades().hideFloatingView(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_publicidad);
        ButterKnife.bind(this);
    }

    private void setUpVideoView(String uriPath) {
//        if (!existeVideo(uriPath)) {
//            finish();
//            return;
//        }
        try {
            Uri uri = Uri.parse(uriPath);
            try {
                videoView.setVideoURI(uri);
                videoView.requestFocus();
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
                finish();
            }
            videoView.setOnPreparedListener(videoViewListener);
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    servicioSocket.posicion = 0;
                    servicioSocket.uriPath = null;
                    finish();
                }
            });
        } catch (NullPointerException e) {
//            servicioSocket.uriPath = "android.resource://" + getPackageName() + "/" + R.raw.ktaxi;
            finish();
        }


    }

    private MediaPlayer.OnPreparedListener videoViewListener =
            new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    videoView.start();
                }
            };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    public boolean existeVideo(String ruta) {
        Log.e(TAG, "existeVideo: " + ruta);
        File dir = new File(ruta);
        if (dir.exists() && dir.isDirectory()) {
            Log.e(TAG, "existeVideo: true ");
            return true;
        }
        servicioSocket.posicion = 0;
        servicioSocket.uriPath = null;
        Log.e(TAG, "existeVideo: false ");
        return false;
    }


}
