package com.kradac.conductor.vista;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.modelo.ItemHistorialCompras;
import com.kradac.conductor.modelo.ResponseValorar;
import com.kradac.conductor.presentador.DatosConductorPresenter;
import com.nostra13.universalimageloader.cache.memory.impl.FIFOLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Dell on 22/2/2017.
 */

public class DetalleCompraActivity extends AppCompatActivity {


    public static String urlImagenVehiculos = "http://ktaxiweb.kradac.com/ktaxi/img/uploads/vehiculos/";

    @BindView(R2.id.ivTaxi)
    ImageView ivTaxi;
    @BindView(R2.id.toolbar)
    Toolbar toolbar;
    @BindView(R2.id.app_bar)
    AppBarLayout appBar;
    @BindView(R2.id.tvRol)
    TextView tvRol;
    @BindView(R2.id.rbCalificacion)
    RatingBar rbCalificacion;
    @BindView(R2.id.ivEditarValoracion)
    ImageButton ivEditarValoracion;
    @BindView(R2.id.tvEditarValoracion)
    TextView tvEditarValoracion;
    @BindView(R2.id.llDatosConductor)
    LinearLayout llDatosConductor;
    @BindView(R2.id.tvEmpresa)
    TextView tvEmpresa;
    @BindView(R2.id.tvPlaca)
    TextView tvPlaca;
    @BindView(R2.id.tvRegistro)
    TextView tvRegistro;
    @BindView(R2.id.tvBarrio)
    TextView tvBarrio;
    @BindView(R2.id.tvCalles)
    TextView tvCalles;
    @BindView(R2.id.tvFecha)
    TextView tvFecha;
    @BindView(R2.id.tv_titulo_detalle)
    TextView tvTituloDetalle;
    @BindView(R2.id.tv_precio_pedido)
    TextView tvPrecioPedido;
    @BindView(R2.id.ly_precio_pedido)
    LinearLayout lyPrecioPedido;
    @BindView(R2.id.tv_descripcion_compra)
    TextView tvDescripcionCompra;
    @BindView(R2.id.ly_informacion)
    LinearLayout lyInformacion;
    @BindView(R2.id.btn_contactar)
    Button btnContactar;


    private int[] valoracion = {0, 5, 4, 3, 2, 1};

    private ItemHistorialCompras itemHistorialCompras;
    private View.OnClickListener onClickListenerEditarValoracion = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View view = inflater.inflate(R.layout.dialog_editar_valoracion, null, false);
            final RatingBar rbEditarValoracion = (RatingBar) view.findViewById(R.id.rbEditarValoracion);
            final EditText etEditarValoracion = (EditText) view.findViewById(R.id.etEditarObservacion);
            String observacion = itemHistorialCompras.getOb();
            if (observacion != null) {
                etEditarValoracion.setText(observacion.trim());
            }
            rbEditarValoracion.setRating(valoracion[Integer.valueOf(itemHistorialCompras.getVl())]);
            AlertDialog.Builder localBuilder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                localBuilder = new AlertDialog.Builder(DetalleCompraActivity.this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                localBuilder = new AlertDialog.Builder(DetalleCompraActivity.this);
            }
            localBuilder.setView(view);
            localBuilder.setCancelable(true);
            localBuilder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    valorarConductor(itemHistorialCompras.getIdPedido(), valoracion[(int) rbEditarValoracion.getRating()], etEditarValoracion.getText().toString().trim());
                }
            });

            localBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            localBuilder.show();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle("Detalle Compra");
        }
        initImageLoader();
        lyPrecioPedido.setVisibility(View.VISIBLE);
        tvTituloDetalle.setText("Detalle de la compra");
        lyInformacion.setVisibility(View.VISIBLE);
        itemHistorialCompras = (ItemHistorialCompras) getIntent().getSerializableExtra("itemHistorialCompras");
        itemHistorialCompras = ItemHistorialCompras.get(itemHistorialCompras.getIdCompra());
        cargarInformacionHistorial(itemHistorialCompras);
        tvEditarValoracion.setOnClickListener(onClickListenerEditarValoracion);
        btnContactar.setVisibility(View.GONE);
//        cargarDatosConductor(itemHistorialCompras);

    }

    private void initImageLoader() {
        int memoryCacheSize;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            int memClass = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE))
                    .getMemoryClass();
            memoryCacheSize = (memClass / 8) * 1024 * 1024;
        } else {
            memoryCacheSize = 2 * 1024 * 1024;
        }

        final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(5)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .memoryCacheSize(memoryCacheSize)
                .memoryCache(
                        new FIFOLimitedMemoryCache(memoryCacheSize - 1000000))
                .denyCacheImageMultipleSizesInMemory()
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .build();

        ImageLoader.getInstance().init(config);
    }


    private void cargarInformacionHistorial(ItemHistorialCompras itemHistorialCompras) {
        tvEmpresa.setText(itemHistorialCompras.getNbE());
        tvPlaca.setText(itemHistorialCompras.getPl());
        tvRegistro.setText(itemHistorialCompras.getR());
        tvBarrio.setText(itemHistorialCompras.getB());
        tvCalles.setText(itemHistorialCompras.getcP() + " y " + itemHistorialCompras.getcS());
        tvFecha.setText(itemHistorialCompras.getFecha());
        if (itemHistorialCompras.getVl() != null) {
            rbCalificacion.setRating(valoracion[Integer.valueOf(itemHistorialCompras.getVl())]);
        }
        tvPrecioPedido.setText(String.valueOf(itemHistorialCompras.getPres()));
        tvDescripcionCompra.setText(itemHistorialCompras.getPed());
//        cargarFoto(urlImagenVehiculos + itemHistorialCompras.getvImg(), ivTaxi);

        if (itemHistorialCompras.getNombreConductor() != null) {
            if (!itemHistorialCompras.getNombreConductor().trim().equals("")) {
                llDatosConductor.setVisibility(View.VISIBLE);
                tvRol.setText(itemHistorialCompras.getRol() + ":");
            }
        }


    }


    private void cargarFoto(String url, final ImageView imageView) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
        if (url != null && !url.equalsIgnoreCase("null") && !url.equalsIgnoreCase("")) {
            imageLoader.displayImage(url, imageView, options, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    super.onLoadingComplete(imageUri, view, loadedImage);
                    imageView.setImageBitmap(loadedImage);
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Modificar que esto aun no esta funcionandp
     *
     * @param idSolicitud
     * @param valoracionNueva
     * @param observacion
     */
    private void valorarConductor(int idSolicitud, final int valoracionNueva, final String observacion) {
        Log.e("DETALLECOMPRA", "valorarConductor: " + idSolicitud + ":");
        DatosConductorPresenter datosConductorPresenter = new DatosConductorPresenter();
        datosConductorPresenter.valorarConductorCompra(idSolicitud, valoracionNueva, observacion, new DatosConductorPresenter.ValorarConductorCompraListener() {
            @Override
            public void onResponse(ResponseValorar responseValorar) {
                itemHistorialCompras.setVl(String.valueOf(valoracionNueva));
                itemHistorialCompras.setOb(observacion);
                itemHistorialCompras.save();
                rbCalificacion.setRating(valoracion[valoracionNueva]);
                Toast.makeText(getApplicationContext(), responseValorar.getM(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(String error) {
                // Toast.makeText(getApplicationContext(),error,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onException() {
                Toast.makeText(getApplicationContext(), "Por favor intente más tarde", Toast.LENGTH_LONG).show();
            }
        });
    }

    @OnClick(R2.id.btn_contactar)
    public void onClick() {
        Intent intent = new Intent(this, SugerenciasActivity.class);
        intent.putExtra("isDesdeSolicitud", true);
        intent.putExtra("solicitudSeleccionada", itemHistorialCompras);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }
}
