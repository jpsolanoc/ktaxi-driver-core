package com.kradac.conductor.vista;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.OnComunicacionRecuperarContrasenia;
import com.kradac.conductor.modelo.MensajeGenerico;
import com.kradac.conductor.presentador.RecuperarContraseniaPresentador;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecuperarContraseniaActivity extends AppCompatActivity implements
        OnClickListener, OnComunicacionRecuperarContrasenia {
    @BindView(R2.id.input_layout_recuperar)
    TextInputLayout inputLayoutRecuperar;
    @BindView(R2.id.appbar)
    Toolbar appbar;
    private EditText txtCampoEnviar;
    private ProgressDialog pDialog;
    private RecuperarContraseniaPresentador contraseniaPresentador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_contrasenia);
        ButterKnife.bind(this);
        setSupportActionBar(appbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.recuperar_contrasenia);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        contraseniaPresentador = new RecuperarContraseniaPresentador(this);
        txtCampoEnviar = (EditText) findViewById(R.id.et_recuperar);
        Button btnenviar = (Button) findViewById(R.id.btnenviarolvidecntrasenia);
        btnenviar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if (v.getId() == R.id.btnenviarolvidecntrasenia) {
            if (!txtCampoEnviar.getText().toString().equals("")) {
                mostrarDialogo();
                contraseniaPresentador.recuperarContraseniaUsuario(txtCampoEnviar.getText().toString());
            } else {
                txtCampoEnviar.setError("Ingrese datos para continuar.");
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
//                showConfirmExit();
                finish();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    public void showConfirmExit() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("ALERTA");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setMessage("¿Realmente desea salir?");
        alertDialog.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        dialog.cancel();
                    }
                });
        alertDialog.setPositiveButton("Si",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        alertDialog.show();
    }

    public void mostrarDialogo() {
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Reestableciendo Contaseña");
        pDialog.setMessage("Espere por favor.");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                AlertDialog.Builder localBuilder = new AlertDialog.Builder(RecuperarContraseniaActivity.this);
                localBuilder.setIcon(R.mipmap.ic_launcher);
                localBuilder.setTitle("ALERTA");
                localBuilder
                        .setMessage("¿Esta seguro que desea cancelar el proceso?");
                localBuilder.setPositiveButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int paramAnonymous2Int) {
                                dialog.dismiss();
                            }
                        });
                localBuilder.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface paramAnonymous2DialogInterface,
                                    int paramAnonymous2Int) {
                                paramAnonymous2DialogInterface.dismiss();
                            }
                        });
                localBuilder.show();
            }
        });
        if (!isFinishing()) {
            pDialog.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
//                showConfirmExit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void confirmacionCambioContrasenia(MensajeGenerico generico) {
        if (!new Utilidades().isVerificarDialogo(pDialog)) {
            pDialog.dismiss();
        }
        if (generico != null) {
            switch (generico.getEstado()) {
                case 1:
                    this.aceptar(generico.getMensaje());
                    break;
                case -1:
                    Toast.makeText(RecuperarContraseniaActivity.this, generico.getMensaje(), Toast.LENGTH_LONG).show();
                    break;
                case -2:
                    Toast.makeText(RecuperarContraseniaActivity.this, generico.getMensaje(), Toast.LENGTH_LONG).show();
                    break;
                default:
                    Toast.makeText(RecuperarContraseniaActivity.this, generico.getMensaje(), Toast.LENGTH_LONG).show();
                    break;
            }
        } else {
            Toast.makeText(RecuperarContraseniaActivity.this, "No se puede cambiar la contraseña intente mas tarde.", Toast.LENGTH_LONG).show();
        }
    }


    public void aceptar(String mensaje) {
        AlertDialog.Builder dialogo = new AlertDialog.Builder(RecuperarContraseniaActivity.this);
        dialogo.setTitle("Información");
        dialogo.setMessage(mensaje);
        dialogo.setCancelable(false);
        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                finish();
            }
        });
        dialogo.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }

}
