package com.kradac.conductor.vista;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.ExpandableListAdapterSaldoPaquetes;
import com.kradac.conductor.extras.MetodosValidacion;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.modelo.RespuestaSaldoPaquetes;
import com.kradac.conductor.presentador.SaldoPaquetesPresentador;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SaldoPaquetesActivity extends AppCompatActivity implements SaldoPaquetesPresentador.OnComunicacionSaldoPaquetes {


    private static final String TAG = SaldoPaquetesActivity.class.getName();
    @BindView(R2.id.list_recorrido)
    ExpandableListView listRecorrido;
    @BindView(R2.id.appbar)
    Toolbar appbar;
    @BindView(R2.id.img_sin_datos)
    ImageView imgSinDatos;
    @BindView(R2.id.progress_espera)
    ProgressBar progressEspera;
    private SaldoPaquetesPresentador saldoPaquetesPresentador;
    private SharedPreferences spLogin;

    private ExpandableListAdapterSaldoPaquetes expandableListAdapterSaldoPaquetes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saldo_paquetes);
        ButterKnife.bind(this);
        setSupportActionBar(appbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setTitle("Mis saldos");
        }
        spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        saldoPaquetesPresentador = new SaldoPaquetesPresentador();
        Utilidades utilidades = new Utilidades();
        String timeStanD = utilidades.getTimeStanD();
        int idUsuario = spLogin.getInt("idUsuario", 0);
        String token = utilidades.SHA256(timeStanD + MetodosValidacion.MD5(String.valueOf(idUsuario)));
        String key = MetodosValidacion.MD5(utilidades.SHA256(String.valueOf(idUsuario)) + timeStanD);
        saldoPaquetesPresentador.cPaquetesObtener(idUsuario, token, key, timeStanD, this);
        progressEspera.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // ID del boton
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void respuestaCPaquetesObtener(RespuestaSaldoPaquetes respuestaSaldoPaquetes) {
        if (respuestaSaldoPaquetes != null) {
            if (respuestaSaldoPaquetes.getEn() == 1) {
                progressEspera.setVisibility(View.GONE);
                imgSinDatos.setVisibility(View.GONE);
                cargarExpandibleList((ArrayList<RespuestaSaldoPaquetes.LPt>) respuestaSaldoPaquetes.getLPt());
            } else {
                imgSinDatos.setVisibility(View.VISIBLE);
                progressEspera.setVisibility(View.GONE);
            }
        } else {
            imgSinDatos.setVisibility(View.VISIBLE);
            progressEspera.setVisibility(View.GONE);
        }
    }

    private HashMap<RespuestaSaldoPaquetes.LPt, List<RespuestaSaldoPaquetes.LP>> listDataHijo;

    public void cargarExpandibleList(final ArrayList<RespuestaSaldoPaquetes.LPt> lPts) {
        listDataHijo = new HashMap<>();
        for (RespuestaSaldoPaquetes.LPt lPt : lPts) {
            listDataHijo.put(lPt, lPt.getLP());
        }
        Log.e(TAG, "cargarExpandibleList: " + listDataHijo.size());
        expandableListAdapterSaldoPaquetes = new ExpandableListAdapterSaldoPaquetes(this, lPts, listDataHijo);
        listRecorrido.setAdapter(expandableListAdapterSaldoPaquetes);
        if (lPts.size() > 0) {
            if (lPts.get(0).getLP().size() > 0) {
                listRecorrido.expandGroup(0);
            } else {
                listRecorrido.expandGroup(1);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }
}
