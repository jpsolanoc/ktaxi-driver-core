package com.kradac.conductor.vista;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.ComponenteAdapter;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.modelo.Componente;
import com.kradac.conductor.modelo.ComponenteBase;
import com.kradac.conductor.modelo.ComponentePublicitario;
import com.kradac.conductor.modelo.ResponseApiComponente;
import com.kradac.conductor.presentador.ComponentePresenter;
import com.kradac.conductor.presentador.RespuestaComponente;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ComponentesActivity extends BaseActivity implements TabLayout.OnTabSelectedListener, ComponenteAdapter.OnItemClick {

    @BindView(R2.id.txtMensajeComponente)
    TextView txtMensajeComponente;
    private SwipyRefreshLayout swipeContainer;
    private RecyclerView recyclerView;
    private TabLayout tabLayout;
    private int idCiudad;
    private ComponenteAdapter adapter;
    private ProgressDialog progress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_componentes_publicitaios);
        ButterKnife.bind(this);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        tabLayout =findViewById(R.id.tabLayout);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView =  findViewById(R.id.my_recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);

        swipeContainer = findViewById(R.id.swipyrefreshlayout);
        swipeContainer.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                swipeContainer.setRefreshing(false);
                cargarComponentes(componenteLista.get(tabLayout.getSelectedTabPosition()));
            }
        });

        swipeContainer.setDirection(SwipyRefreshLayoutDirection.BOTTOM);
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        progress = new ProgressDialog(this);
        progress.setMessage("Cargando...");
        progress.setProgressStyle(android.R.style.Widget_DeviceDefault_Light_ProgressBar_Horizontal);

        Intent intent = getIntent();
        idCiudad = intent.getIntExtra("idCiudad", 0);
        cargarComponetesPublicitarios();
    }

    List<Componente> componenteLista = new ArrayList<>();

    private void cargarComponetesPublicitarios() {
        progress.show();
        ComponentePresenter componentePresenter = new ComponentePresenter();
        componentePresenter.obtenerComponetesPublicitariosCiudad(idCiudad, new ComponentePresenter.ComponentePublicitarioListener() {
            @Override
            public void error(String s) {
                Log.e("error", s);
            }

            @Override
            public void response(ComponentePublicitario componentePublicitario) {
                tabLayout.removeAllTabs();
                componenteBaseLista.clear();
                if(componentePublicitario.getEn() == 1) {
                    cargarBarraComponentesPublicitarios(componentePublicitario.getlC());
                    componenteLista = componentePublicitario.getlC();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }

    private void cargarBarraComponentesPublicitarios(List<Componente> componenteList) {
        for(Componente componente : componenteList) {
            TabLayout.Tab tab = tabLayout.newTab();
            tab.setText(componente.getNb());
            tab.setTag(componente.getT());
            tabLayout.addTab(tab);
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setOnTabSelectedListener(this);

        if(componenteList.size() > 0) {
            cargarComponentes(componenteList.get(0));
        }
    }

    List<ComponenteBase> componenteBaseLista = new ArrayList<>();

    private void cargarComponentes(Componente componente) {
        if(adapter!=null){
            adapter.updateAdapter();
        }
        SharedPreferences datosUsuario = getSharedPreferences("login", Context.MODE_PRIVATE);
        int idCliente = datosUsuario.getInt("idUsuario", 0);
        txtMensajeComponente.setText(componente.getD());
        if(!progress.isShowing()) {
            progress.show();
        }
        ComponentePresenter componentePresenter = new ComponentePresenter();
        componentePresenter.obtenerComponentes(idCliente, idCiudad, componente.getT(), componenteBaseLista.size(), 3, new ComponentePresenter.ObtenerComponentesListener() {
            @Override
            public void error(String s) {
                if(progress.isShowing()) {
                    progress.dismiss();
                }
            }

            @Override
            public void response(RespuestaComponente respuestaComponente) {

                if(respuestaComponente.getEn() == 1) {
                    leerComponentes(respuestaComponente.getlIC());
                    componenteBaseLista.addAll(respuestaComponente.getlIC());
                    if(componenteBaseLista.size()>0){
                        txtMensajeComponente.setVisibility(View.GONE);
                    }else{
                        txtMensajeComponente.setVisibility(View.VISIBLE);
                    }
                    if(adapter!=null){
                        adapter.updateAdapter();
                        if(componenteBaseLista.size()>0){
                            recyclerView.scrollToPosition(componenteBaseLista.size()-1);
                        }

                    }else{
                        adapter = new ComponenteAdapter(getApplicationContext(),componenteBaseLista, ComponentesActivity.this);
                        recyclerView.setAdapter(adapter);
                    }
                }else{
                    txtMensajeComponente.setVisibility(View.VISIBLE);
                }
                if(progress.isShowing()) {
                    progress.dismiss();
                }
            }
        });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if(componenteLista.size() > 0) {
            componenteBaseLista.clear();
            adapter.updateAdapter();
            cargarComponentes(componenteLista.get(tab.getPosition()));
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch(keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void OnClickItem(int position) {
        Intent intent = new Intent(this, DetalleComponenteActivity.class);
        intent.putExtra("componenteBase", componenteBaseLista.get(position));
        intent.putExtra("tipo", componenteLista.get(tabLayout.getSelectedTabPosition()).getT());
        startActivity(intent);
    }

    private void leerComponentes(List<ComponenteBase> componenteBaseLista){
        if(componenteBaseLista.size()==0)return;
        JSONArray e = new JSONArray();
        for(ComponenteBase i : componenteBaseLista) {
            e.put(i.getIdC());
        }
        SharedPreferences spobtener;
        spobtener = getSharedPreferences("login", Context.MODE_PRIVATE);
        ComponentePresenter componentePresenter=new ComponentePresenter();
        componentePresenter.marcarComponenteComoLeido(spobtener.getInt("idUsuario", 0), componenteLista.get(tabLayout.getSelectedTabPosition()).getT(), 1, e.toString(), new ComponentePresenter.ComponenteListener() {
            @Override
            public void error(String s) {
                Log.e("marcar componente",s);
            }

            @Override
            public void response(ResponseApiComponente responseApi) {

            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
