package com.kradac.conductor.vista;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryActivity;
import com.hbb20.CountryCodePicker;
import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.DialogMainSelector;
import com.kradac.conductor.extras.MetodosValidacion;
import com.kradac.conductor.interfaces.OnComunicacionAgregarTarjeta;
import com.kradac.conductor.modelo.PaisCode;
import com.kradac.conductor.modelo.ResponseApiCorto;
import com.kradac.conductor.modelo.ResponseGenerarToken;
import com.kradac.conductor.modelo.Solicitud;
import com.kradac.conductor.modelo.TarjetaCredito;
import com.kradac.conductor.presentador.CreditCardRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.hbb20.CountryActivity.REQUEST_SELECT_COUNTRY;

public class AgregarTarjeta extends BaseConexionService implements OnComunicacionAgregarTarjeta,View.OnFocusChangeListener {


    private static final String TAG = AgregarTarjeta.class.getName();
    public static final int AGREGAR_TARJETA = 1012;

    private final int TIPO_AGREGAR = 1;
    private final int TIPO_ELIMINAR = 2;

    @BindView(R2.id.txtNumeroTarjeta)
    TextInputEditText txtNumeroTarjeta;
    @BindView(R2.id.txtFechaTarjeta)
    TextInputEditText txtFechaTarjeta;
    @BindView(R2.id.txtCVVTarjeta)
    TextView txtCVVTarjeta;

    //@BindView(R2.id.btn_siguiente)
    // Button btnSiguiente;

    protected CountryCodePicker ccpLoadNumber;
    @BindView(R2.id.ccp_loadFullNumber)
    CountryCodePicker ccpLoadFullNumber;
    @BindView(R2.id.editText_loadCarrierNumber)
    TextView editTextLoadCarrierNumber;
    @BindView(R2.id.img_ayuda_vencimiento)
    ImageView imgAyudaVencimiento;
    @BindView(R2.id.img_ayuda_cvv)
    ImageView imgAyudaCvv;
    @BindView(R2.id.img_tarjeta)
    ImageView imgTarjeta;
    @BindView(R2.id.txtEmail)
    TextInputEditText txtEmail;
    @BindView(R2.id.txtPhoneNumber)
    TextInputEditText txtPhoneNumber;
    @BindView(R2.id.txtOptionalParameter)
    TextInputEditText txtOptionalParameter;
    @BindView(R2.id.lySinSolicitud)
    LinearLayout lySinSolicitud;
    @BindView(R2.id.btn_1)
    TextView btn1;
    @BindView(R2.id.btn_2)
    TextView btn2;
    @BindView(R2.id.btn_3)
    TextView btn3;
    @BindView(R2.id.btn_4)
    TextView btn4;
    @BindView(R2.id.btn_5)
    TextView btn5;
    @BindView(R2.id.btn_6)
    TextView btn6;
    @BindView(R2.id.btn_7)
    TextView btn7;
    @BindView(R2.id.btn_8)
    TextView btn8;
    @BindView(R2.id.btn_9)
    TextView btn9;
    @BindView(R2.id.btn_10)
    TextView btn10;
    @BindView(R2.id.btn_11)
    LinearLayout btn11;

    private BottomSheetDialog mBottomSheetDialog;
    protected BottomSheetBehavior behavior;
    protected View bottomSheet, bottomSheetT;
    private TarjetaCredito tarjetaCredito;
    private MenuItem itemEliminar;
    protected String numero;
    protected String numero_cvv;
    private int tamanioAnteriorFecha = 0;
    private Solicitud solicitud;
    private double costoCarrera;
    private double saldoKtaxi;
    private double propina;
    private double aCobrar;
    protected List<TextView> listaTextViews = new ArrayList<>();

    protected BottomSheetBehavior mBottomSheetBehavior1;


    @Override
    public void conexionCompleta() {
        super.conexionCompleta();
        registrarAcitvityServer(this, "AgregarTarjeta");
    }

    public void cerrarTecladoTextView(TextView edit) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(edit.getWindowToken(), 0);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_tarjeta);
        ButterKnife.bind(this);
        tarjetaCredito = getIntent().getParcelableExtra("tarjeta");
        if (tarjetaCredito != null) {
            // txtNumeroTarjeta.append(tarjetaCredito.getNumberMask());
            txtFechaTarjeta.append(tarjetaCredito.getExpirationMonth() + "/" + tarjetaCredito.getExpirationYear());
            setTitle("Cobro regsitro tarjeta crédito");
        }

        bottomSheetT = findViewById(R.id.bottom_sheet1);

        mBottomSheetBehavior1 = BottomSheetBehavior.from(bottomSheetT);

        listaTextViews.add(btn1);
        listaTextViews.add(btn2);
        listaTextViews.add(btn3);
        listaTextViews.add(btn4);
        listaTextViews.add(btn5);
        listaTextViews.add(btn6);
        listaTextViews.add(btn7);
        listaTextViews.add(btn8);
        listaTextViews.add(btn9);
        listaTextViews.add(btn10);

        cargarOnClickCVV();
        txtCVVTarjeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cerrarTecladoTextView(txtCVVTarjeta);
                numeroAleatorios();
                if (bottomSheetT.getVisibility()==View.GONE) {
                    bottomSheetT.setVisibility(View.VISIBLE);
                }
            }
        });



        txtFechaTarjeta.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int startp, int before, int count) {
                String dato = charSequence.toString();
                if (charSequence.toString().length() > 0) {
                    String start = charSequence.toString().substring(0, 1);
                    if (!start.equals("1") && !start.equals("0") && !start.equals("/") && charSequence.toString().length() == 1) {
                        txtFechaTarjeta.setText("");
                        txtFechaTarjeta.append("0" + start + "/");
                    } else {
                        if (!start.equals("1") && !start.equals("0") && !start.equals("/") && charSequence.toString().length() > 2) {
                            String fecha = txtFechaTarjeta.getText().toString();
                            if (fecha.contains("/")) {
                                if (fecha.split("/").length > 1) {
                                    txtFechaTarjeta.setText("");
                                    txtFechaTarjeta.append("0" + start + "/" + fecha.split("/")[1]);
                                }
                            }
                        }
                    }
                }

                if (charSequence.toString().length() == 2) {
                    if (txtFechaTarjeta.getText().toString().length() == 3) {
                        String start = charSequence.toString().substring(0, 1);
                        txtFechaTarjeta.setText("");
                        txtFechaTarjeta.append(start);

                    } else {
                        if (!txtFechaTarjeta.getText().toString().contains("/"))
                            if (startp >= tamanioAnteriorFecha) {
                                txtFechaTarjeta.append("/");
                            }
                    }
                }
                if (charSequence.toString().length() == 3 && !charSequence.toString().contains("/")) {
                    txtFechaTarjeta.setText("");
                    txtFechaTarjeta.append(charSequence.toString().substring(0, 2) + "/" + charSequence.toString().substring(2, 3));
                }
                tamanioAnteriorFecha = txtFechaTarjeta.getText().length();

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        ccpLoadFullNumber.changeDefaultLanguage(CountryCodePicker.Language.SPANISH);
        ccpLoadFullNumber.setDefaultCountryUsingNameCode("EC");
        ccpLoadFullNumber.setCountryForNameCode("EC");
        ccpLoadFullNumber.setShowPhoneCode(false);
        ccpLoadFullNumber.setAutoDetectedCountry(true);
        ccpLoadFullNumber.setCountryPreference(PaisCode.CODIGO_ECUADOR + "," + PaisCode.CODIGO_COLOMBIA + "," + PaisCode.CODIGO_PERU + "," + PaisCode.CODIGO_BOLIVIA + "," + PaisCode.CODIGO_ARGENTINA);
        ccpLoadFullNumber.setCountryAutoDetectionPref(CountryCodePicker.AutoDetectionPref.SIM_LOCALE_NETWORK);

//        if(usuario!=null){
//            ccpLoadFullNumber.setDefaultCountryUsingNameCode(usuario.getCodigoPais());
//            ccpLoadFullNumber.setCountryForNameCode(usuario.getCodigoPais());
//        }

        editTextLoadCarrierNumber.setText(ccpLoadFullNumber.getSelectedCountryName());

        ccpLoadFullNumber.setCcpClickable(false);

        ccpLoadFullNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(AgregarTarjeta.this, CountryActivity.class), REQUEST_SELECT_COUNTRY);
            }
        });


        bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // React to state change
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // React to dragging events
            }
        });

        imgAyudaVencimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottom(R.layout.bottomsheetsvencimiento);
            }
        });

        imgAyudaCvv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottom(R.layout.bottomsheetscvv);
            }
        });
        utilidades.hideFloatingView(this);
        validador();
        extraerData();
        if (solicitud == null) {
            activar(txtEmail);
        }
        bottomSheetT.setVisibility(View.GONE);
        txtNumeroTarjeta.setOnFocusChangeListener(this);
        txtFechaTarjeta.setOnFocusChangeListener(this);
        txtEmail.setOnFocusChangeListener(this);
        txtNumeroTarjeta.setOnFocusChangeListener(this);
        txtOptionalParameter.setOnFocusChangeListener(this);
    }


    public void onFocusChange(View v, boolean hasFocus) {
        Log.e(TAG, "onFocusChange: ENTRO"  );
        if (bottomSheetT.getVisibility()==View.VISIBLE){
            bottomSheetT.setVisibility(View.GONE);
        }
    }

    boolean containsArroba = false;

    public void activar(final EditText edit) {
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!containsArroba && s.toString().contains("@")) {
                    cargarCorreo();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (txtEmail.getText().toString().isEmpty()) {
                    containsArroba = true;
                    return;
                }

                containsArroba = txtEmail.getText().toString().contains("@");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    DialogMainSelector dialogMainSelector;

    private void cargarCorreo() {
        if (dialogMainSelector != null) {
            if (dialogMainSelector.isShowing()) {
                return;
            }
        }
        dialogMainSelector = new DialogMainSelector(AgregarTarjeta.this);
        dialogMainSelector.setMailSelectorListener(new DialogMainSelector.MailSelectorListener() {
            @Override
            public void onMainSelected(String main) {
                if (main.isEmpty()) return;
                String correo = txtEmail.getText().toString();
                if (!correo.isEmpty()) {
                    if (correo.contains("@")) {
                        String[] correoList = correo.split("@");
                        if (correoList.length > 0) {
                            txtEmail.setText(correoList[0] + main);
                        } else {
                            txtEmail.setText(main);
                        }
                    } else {
                        txtEmail.setText(txtEmail.getText().toString().replace("@", ""));
                        txtEmail.setText(txtEmail.getText().toString() + main);
                    }
                } else {
                    txtEmail.setText(main);
                }
                txtPhoneNumber.requestFocus();
            }
        });

        dialogMainSelector.setCancelable(false);
        dialogMainSelector.show();
    }

    public void extraerData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            costoCarrera = Double.parseDouble(bundle.getString("costoCarrera"));
            saldoKtaxi = Double.parseDouble(bundle.getString("saldoKtaxi"));
            propina = Double.parseDouble(bundle.getString("propina"));
            aCobrar = Double.parseDouble(bundle.getString("aCobrar"));
            solicitud = bundle.getParcelable("solicitud");
            if (solicitud != null) {
                lySinSolicitud.setVisibility(View.GONE);
            } else {
                lySinSolicitud.setVisibility(View.VISIBLE);
            }
        }

    }

    private int tamanioAnterior = 0;

    private void validador() {
        txtNumeroTarjeta.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int tamanioNuevo = txtNumeroTarjeta.getText().toString().length();
                if ((tamanioNuevo == 4 || tamanioNuevo == 9 || tamanioNuevo == 14 || tamanioNuevo == 19) && tamanioNuevo >= tamanioAnterior) {
                    txtNumeroTarjeta.append(" ");
                }
                if ((tamanioNuevo == 5 || tamanioNuevo == 10 || tamanioNuevo == 15 || tamanioNuevo == 20) && !charSequence.toString().substring(tamanioNuevo - 1, tamanioNuevo).equals(" ")) {
                    txtNumeroTarjeta.setText("");
                    txtNumeroTarjeta.append(charSequence.toString().substring(0, tamanioNuevo - 1) + " " + charSequence.toString().substring(tamanioNuevo - 1, tamanioNuevo));

                }
                tamanioAnterior = txtNumeroTarjeta.getText().toString().length();

                numero = charSequence.toString().replaceAll(" ", "");
                if (numero.length() >= 4) {
                    switch (TarjetaCredito.getTipoTarjeta(numero)) {
                        case TarjetaCredito.TIPO_MASTERCARD:
                            imgTarjeta.setImageResource(R.drawable.ic_card_mastercard);
                            break;
                        case TarjetaCredito.TIPO_VISA:
                            imgTarjeta.setImageResource(R.drawable.ic_card_visa);
                            break;
                        case TarjetaCredito.TIPO_AMEX:
                            imgTarjeta.setImageResource(R.drawable.ic_card_amex);
                            break;
                        case TarjetaCredito.TIPO_DINERS_CLUB:
                            imgTarjeta.setImageResource(R.drawable.diners);
                            break;
                        case TarjetaCredito.TIPO_DISCOVER:
                            imgTarjeta.setImageResource(R.drawable.discover);
                            break;
                    }
                } else {
                    imgTarjeta.setImageResource(R.mipmap.ic_icon_tarjeta_credito);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void showBottomT(int layout) {
        if (mBottomSheetBehavior1.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(layout, null);
        Button btnCerrarDescripcion = view.findViewById(R.id.btnCerrarDescripcion);
        btnCerrarDescripcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }

    private void showBottom(int layout) {
        if (behavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        mBottomSheetDialog = new BottomSheetDialog(this);
        View view = getLayoutInflater().inflate(layout, null);
        Button btnCerrarDescripcion = view.findViewById(R.id.btnCerrarDescripcion);
        btnCerrarDescripcion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                mBottomSheetDialog.dismiss();
            }
        });

        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.show();
        mBottomSheetDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mBottomSheetDialog = null;
            }
        });
    }


    private void generarToken(int tipo) {
        //        ocultarTeclado();
        CreditCardRequest creditCardRequest = new CreditCardRequest();
        String correo = "";
        String celular = "";
        String nombres = "";
        String apellidos = "";
        correo = Base64.encodeToString(spLogin.getString("correo", "").trim().getBytes(), Base64.DEFAULT).replace("\n", "");
        celular = Base64.encodeToString(spLogin.getString("celular", "").trim().getBytes(), Base64.DEFAULT).replace("\n", "");
        nombres = Base64.encodeToString(spLogin.getString("nombres", "").trim().getBytes(), Base64.DEFAULT).replace("\n", "");
        apellidos = Base64.encodeToString(spLogin.getString("apellidos", "").trim().getBytes(), Base64.DEFAULT).replace("\n", "");

        String imei = utilidades.getImei(getApplicationContext());
        String timeStanD = new Date().getTime() + "";
        String token = MetodosValidacion.SHA256(timeStanD + MetodosValidacion.MD5(imei));
        String key = MetodosValidacion.MD5(celular + spLogin.getInt("idUsuario", 0) + apellidos);
        mostrarEspera(getString(R.string.msj_verificando_datos));
        creditCardRequest.solicitarToken(spLogin.getInt("idUsuario", 0), imei, correo, celular, nombres, apellidos, token, key, timeStanD, new CreditCardRequest.CreditCardListener() {
            @Override
            public void onResponse(ResponseGenerarToken responseApi) {
                ocultarEspera();
                if (tipo == TIPO_AGREGAR) {
                    registrarTarjeta(responseApi);
                } else {
                    if (tipo == TIPO_ELIMINAR) {
                        eliminarTarjeta(responseApi);
                    }
                }
            }

            @Override
            public void error(String error) {
                Log.e(TAG, "onResponse: ERROR");
                ocultarEspera();
                Toast.makeText(AgregarTarjeta.this, error, Toast.LENGTH_SHORT).show();
            }
        });
    }



    private void registrarTarjeta(ResponseGenerarToken responseGenerarToken) {
        int op = Integer.parseInt(responseGenerarToken.getOp() + "" + txtCVVTarjeta.getText().toString().trim());

        String opEnvio = Base64.encodeToString(((((op + responseGenerarToken.getSum()) * responseGenerarToken.getMul()) * responseGenerarToken.getDiv()) + "").getBytes(), Base64.DEFAULT).replace("\n", "");

        CreditCardRequest creditCardRequest = new CreditCardRequest();

        String imei = utilidades.getImei(this);

        String numeroTarjeta = txtNumeroTarjeta.getText().toString().replaceAll(" ", "").trim();
        String number = Base64.encodeToString(numeroTarjeta.getBytes(), Base64.DEFAULT).replace("\n", "");
        String expirationMonth = Base64.encodeToString(txtFechaTarjeta.getText().toString().trim().split("/")[0].getBytes(), Base64.DEFAULT).replace("\n", "");
        String expirationYear = Base64.encodeToString(txtFechaTarjeta.getText().toString().trim().split("/")[1].getBytes(), Base64.DEFAULT).replace("\n", "");
        String email = spLogin.getString("correo", "").trim();
        String phoneNumber = "+" + ccpLoadFullNumber.getSelectedCountryCode() + spLogin.getString("celular", "").trim();
        if (solicitud == null) {
            email = txtEmail.getText().toString().trim();
            phoneNumber = "+" + ccpLoadFullNumber.getSelectedCountryCode() + txtPhoneNumber.getText().toString().trim();
        }
        String timeStanD = new Date().getTime() + "";
        String token = MetodosValidacion.SHA256(timeStanD + MetodosValidacion.MD5(imei));
        String key = MetodosValidacion.MD5(phoneNumber + spLogin.getInt("idUsuario", 0) + expirationYear);
        mostrarEspera(getString(R.string.msg_registrando_tarjeta));
        if (solicitud != null) {
            servicioSocket.cobroTarjetaCreditoDirecto(aCobrar, saldoKtaxi, propina, 0, solicitud, imei, responseGenerarToken.getCon(), number, expirationMonth, expirationYear, opEnvio);
        } else {
            servicioSocket.cobroTarjetaCreditoDirectoCalle(aCobrar, saldoKtaxi, propina, 0, solicitud, imei, responseGenerarToken.getCon(), number,
                    expirationMonth, expirationYear, opEnvio, email, phoneNumber, txtOptionalParameter.getText().toString());
        }


    }

    private void eliminarTarjeta(ResponseGenerarToken responseApi) {
        if (tarjetaCredito == null) return;
        String con = responseApi.getCon();
        String numberCar = tarjetaCredito.getNumberBase64();
        String conCar = tarjetaCredito.getCon();
        String imei = utilidades.getImei(getApplicationContext());
        String timeStanD = new Date().getTime() + "";
        String token = MetodosValidacion.SHA256(timeStanD + MetodosValidacion.MD5(imei));
        String key = MetodosValidacion.MD5(imei + spLogin.getInt("idUsuario", 0) + responseApi.getCon());

        CreditCardRequest creditCardRequest = new CreditCardRequest();


        creditCardRequest.eliminarTarjeta(spLogin.getInt("idUsuario", 0), imei, con, numberCar, conCar, token, key, timeStanD, new CreditCardRequest.EliminarCardListener() {
            @Override
            public void onResponse(ResponseApiCorto responseApi) {
                Toast.makeText(AgregarTarjeta.this, responseApi.getM(), Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void error(String error) {
                Toast.makeText(AgregarTarjeta.this, error, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @OnClick(R2.id.btnRegistrarTarjeta)
    public void onViewClicked() {
        validarCampos();
    }

    private void validarCampos() {
        String numeroTarjeta = txtNumeroTarjeta.getText().toString().trim();
        if (numeroTarjeta.isEmpty()) {
            txtNumeroTarjeta.setError(getString(R.string.msg_validacion_numero_tarjeta));
            return;
        }

        if (!validarNumeroTarjeta(numero)) {
            txtNumeroTarjeta.setError(getString(R.string.msg_validacion_numero_tarjeta2));
            return;
        }

        String fechaTarjeta = txtFechaTarjeta.getText().toString().trim();
        if (fechaTarjeta.isEmpty()) {
            txtFechaTarjeta.setError(getString(R.string.msg_validacion_fecha_expiracion_tarjeta));
            return;
        }
        if (!fechaTarjeta.contains("/")) {
            txtFechaTarjeta.setError(getString(R.string.msg_validacion_fecha_expiracion_tarjeta));
            return;
        }
        if (fechaTarjeta.length() != 5) {
            txtFechaTarjeta.setError(getString(R.string.msg_validacion_fecha_expiracion_tarjeta));
            return;
        }

        String cvv = txtCVVTarjeta.getText().toString().trim();
        if (cvv.isEmpty()) {
            txtCVVTarjeta.setError(getString(R.string.msg_validacion_cvv));
            return;
        }
        if (solicitud == null) {
            if (txtEmail.getText().toString().isEmpty()) {
                Toast.makeText(this, "Ingrese un email correcto.", Toast.LENGTH_SHORT).show();
                txtEmail.requestFocus();
                return;
            }
            if (txtPhoneNumber.getText().toString().isEmpty()) {
                Toast.makeText(this, "Ingrese un número telefonico correcto.", Toast.LENGTH_SHORT).show();
                txtPhoneNumber.requestFocus();
                return;
            }
            if (txtOptionalParameter.getText().toString().isEmpty()) {
                Toast.makeText(this, "Ingrese datos en parametro opcional.", Toast.LENGTH_SHORT).show();
                txtOptionalParameter.requestFocus();
                return;
            }
        }
        generarToken(TIPO_AGREGAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SELECT_COUNTRY && resultCode == RESULT_OK) {
            ccpLoadFullNumber.setCountryForPhoneCode(data.getIntExtra("codeInt", 1));
            editTextLoadCarrierNumber.setText(ccpLoadFullNumber.getSelectedCountryName());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean validarNumeroTarjeta(String ccNumber) {

        try {
            ccNumber = ccNumber.replaceAll("\\D", "");
            char[] ccNumberArry = ccNumber.toCharArray();

            int checkSum = 0;
            for (int i = ccNumberArry.length - 1; i >= 0; i--) {

                char ccDigit = ccNumberArry[i];

                if ((ccNumberArry.length - i) % 2 == 0) {
                    int doubleddDigit = Character.getNumericValue(ccDigit) * 2;
                    checkSum += (doubleddDigit % 9 == 0 && doubleddDigit != 0) ? 9 : doubleddDigit % 9;

                } else {
                    checkSum += Character.getNumericValue(ccDigit);
                }

            }

            return (checkSum != 0 && checkSum % 10 == 0);

        } catch (Exception e) {

            e.printStackTrace();

        }

        return false;
    }

    @Override
    public void respuestaCobroTarjetCredito(final int tipoError, final int estado, final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarEspera();
                Intent returnIntent = new Intent();
                if (tipoError != 0) {
                    mensajeTarjetaCredito(mensaje, 1);
                } else {
                    switch (estado) {
                        case 1:
                            returnIntent.putExtra("tipoError", tipoError);
                            returnIntent.putExtra("estado", estado);
                            returnIntent.putExtra("mensaje", mensaje);
                            setResult(RESULT_OK, returnIntent);
                            finish();
                            break;
                        case -1:
                            mensajeTarjetaCredito(mensaje, 3);
                            break;
                        case 2:
                            returnIntent.putExtra("tipoError", tipoError);
                            returnIntent.putExtra("estado", estado);
                            returnIntent.putExtra("mensaje", mensaje);
                            setResult(RESULT_OK, returnIntent);
                            finish();
                            break;
                        case 3:
                            mensajeTarjetaCredito(mensaje, 1);
                            break;
                    }
                }
            }
        });
    }

    public Dialog dialogMensajeTarjetaCredito;

    public void mensajeTarjetaCredito(String mensaje, final int idTipo) {
        if (dialogMensajeTarjetaCredito != null) {
            if (dialogMensajeTarjetaCredito.isShowing()) {
                return;
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(AgregarTarjeta.this);
        builder.setMessage(mensaje)
                .setTitle(R.string.informacion)
                .setCancelable(false)
                .setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });
        dialogMensajeTarjetaCredito = builder.create();
        if (!isFinishing()) {
            dialogMensajeTarjetaCredito.show();
        }
    }

    @Override
    public void envioCobro(final String dato, final int estado) {
        ocultarEspera();
        runOnUiThread(() -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(AgregarTarjeta.this);
            builder.setMessage(dato)
                    .setTitle(R.string.str_alerta_min)
                    .setPositiveButton(R.string.aceptar, (dialog, id) -> {
                        dialog.cancel();
                    }).show();

        });
    }

    public void generarNumero(String num) {
        if (numero_cvv != null) {
            txtCVVTarjeta.setText(numero_cvv + num);
            numero_cvv = numero_cvv + num;
        } else {
            txtCVVTarjeta.setText(num);
            numero_cvv = num;
        }


    }

    public void eliminarNumero(String num) {
        String resultado;
        if (num.length() > 1) {
            resultado = num.substring(0, num.length() - 1);
            txtCVVTarjeta.setText(resultado);
            numero_cvv = resultado;
        } else {
            txtCVVTarjeta.setText(null);
            numero_cvv = "";
        }


    }

    public void numeroAleatorios() {
        Random rm = new Random();
        int t = 1;
        int n = 10;
        int numeros[] = new int[10];
        int resutaldo[] = new int[10];


        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = i;
        }

        for (int i = 0; i < 10; i++) {
            t = rm.nextInt(n);
            resutaldo[i] = numeros[t];

            for (int j = t; j < numeros.length - 1; j++) {
                numeros[j] = numeros[j + 1];
            }
            n--;
        }
        for (int i = 0; i < resutaldo.length; i++) {
            listaTextViews.get(i).setText(String.valueOf(resutaldo[i]));

        }

    }

    public void cargarOnClickCVV() {
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtCVVTarjeta.getText().toString().length() < 4) {
                    generarNumero(btn1.getText().toString().trim());
                }

            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtCVVTarjeta.getText().toString().length() < 4) {
                    generarNumero(btn2.getText().toString().trim());
                }

            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtCVVTarjeta.getText().toString().length() < 4) {
                    generarNumero(btn3.getText().toString().trim());
                }

            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtCVVTarjeta.getText().toString().length() < 4) {
                    generarNumero(btn4.getText().toString().trim());
                }

            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtCVVTarjeta.getText().toString().length() < 4) {
                    generarNumero(btn5.getText().toString().trim());
                }

            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtCVVTarjeta.getText().toString().length() < 4) {
                    generarNumero(btn6.getText().toString().trim());
                }

            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtCVVTarjeta.getText().toString().length() < 4) {
                    generarNumero(btn7.getText().toString().trim());
                }

            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtCVVTarjeta.getText().toString().length() < 4) {
                    generarNumero(btn8.getText().toString().trim());
                }

            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtCVVTarjeta.getText().toString().length() < 4) {
                    generarNumero(btn9.getText().toString().trim());
                }

            }
        });
        btn10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtCVVTarjeta.getText().toString().length() < 4) {
                    generarNumero(btn10.getText().toString().trim());
                }

            }
        });
        btn11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtCVVTarjeta.getText().toString().trim().length() > 0) {
                    eliminarNumero(txtCVVTarjeta.getText().toString().trim());
                }

            }
        });
    }

}
