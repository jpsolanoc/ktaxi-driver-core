package com.kradac.conductor.vista;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.RvAdapterCompraServicios;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionCompraServicios;
import com.kradac.conductor.modelo.CompraServiciosCategorias;
import com.kradac.conductor.modelo.SaldoKtaxi;
import com.kradac.conductor.presentador.DetalleCompraServicioPresentador;
import com.kradac.conductor.presentador.SaldoKtaxiPresentador;
import com.kradac.conductor.service.ServicioSockets;

import net.glxn.qrgen.android.QRCode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetalleCompraServicioActivity extends AppCompatActivity implements OnComunicacionCompraServicios {

    private static final String TAG = DetalleCompraServicioActivity.class.getName();
    @BindView(R2.id.appbar)
    Toolbar appbar;
    @BindView(R2.id.lista_item)
    RecyclerView listaItem;
    @BindView(R2.id.progress_espera)
    ProgressBar progressEspera;


    private RvAdapterCompraServicios rvAdapterCompraServicios;
    private RecyclerView.LayoutManager lManager;
    private DetalleCompraServicioPresentador detalleCompraServicioPresentador;
    private SharedPreferences spLogin;
    private CompraServiciosCategorias.LC categoria;
    private ServicioSockets servicioSocket;
    private boolean mBound;
    private SaldoKtaxi.CompraServicios compraServicios;
    private Utilidades utilidades;

    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ServicioSockets.LocalBinder binder = (ServicioSockets.LocalBinder) service;
            servicioSocket = binder.getService();
            servicioSocket.registerCliente(DetalleCompraServicioActivity.this, "DetalleCompraServicioActivity");
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };


    public void pedirDatosServidor() {
        new SaldoKtaxiPresentador(null, this, null).saldoConsultar(spLogin.getInt("idVehiculo", 0), spLogin.getInt("idUsuario", 0), spLogin.getInt("idCiudad", 0));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_compra_servicio);
        ButterKnife.bind(this);
        setSupportActionBar(appbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
        spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        detalleCompraServicioPresentador = new DetalleCompraServicioPresentador(this);
        utilidades = new Utilidades();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            categoria = bundle.getParcelable("categoria");
            ab.setTitle(categoria.getCategoria());
        }
        lManager = new LinearLayoutManager(this);
        listaItem.setLayoutManager(lManager);
        pedirDatosServidor();
        getDatos();
        cargarDatos();
    }

    public void getDatos() {
        detalleCompraServicioPresentador.consultar_servicios(categoria.getIdCategoria(), spLogin.getInt("idVehiculo", 0), spLogin.getInt("idUsuario", 0), spLogin.getInt("idCiudad", 0));
        progressEspera.setVisibility(View.VISIBLE);
    }

    public void cargarDatos() {
        rvAdapterCompraServicios = new RvAdapterCompraServicios(this, new ArrayList<SaldoKtaxi.CompraServicios.LS>(), this);
        listaItem.setAdapter(rvAdapterCompraServicios);
    }

    private Dialog dialogComprar;


    @Override
    public void onLocation(Location location) {
        if (compraServicios != null && location != null) {
            ArrayList<SaldoKtaxi.CompraServicios.LS> listaServicios = new ArrayList<>();
            for (SaldoKtaxi.CompraServicios.LS ls : compraServicios.getLS()) {
//                ls.setDistancia(utilidades.calculationByDistance(new LatLng(location.getLatitude(), location.getLongitude()),
//                        new LatLng(ls.getLatitud(), ls.getLongitud())));
                if (ls.getDistancia() <= 0.3) {
                    ls.setLugar(true);
                } else {
                    ls.setLugar(false);
                }
                listaServicios.add(ls);
            }
            compraServicios.setLS(listaServicios);
            rvAdapterCompraServicios.addAll((ArrayList<SaldoKtaxi.CompraServicios.LS>) compraServicios.getLS());
        }
    }

    @Override
    public void respuestaServidorServicios(SaldoKtaxi.CompraServicios compraServicios) {
        progressEspera.setVisibility(View.GONE);
        if (compraServicios != null) {
            if (compraServicios.getEn() == 1) {
                this.compraServicios = compraServicios;
                rvAdapterCompraServicios.addAll((ArrayList<SaldoKtaxi.CompraServicios.LS>) compraServicios.getLS());
            } else {
                Builder builder = new Builder(this);
                builder.setMessage(compraServicios.getM())
                        .setTitle("Alerta")
                        .setPositiveButton("Reintentar", new OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                getDatos();
                            }
                        })
                        .setNegativeButton("Salir", new OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        }).show();
            }
        } else {
            Builder builder = new Builder(this);
            builder.setMessage("No se puede obtener los datos, ¿desea intentar nuevamente?.")
                    .setTitle("Alerta")
                    .setPositiveButton("Si", new OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            getDatos();
                        }
                    })
                    .setNegativeButton("No", new OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();
                        }
                    }).show();
        }

    }

    private ProgressDialog pDialog;

    public void esperaCobro() {
        if (pDialog != null) {
            if (pDialog.isShowing()) {
                return;
            }
        }
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Espere la confirmación del pago.");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void onRespuestaCompraServicio(final String respuesta) {
        pedirDatosServidor();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dialogEsperaSaldoKtaxi != null) {
                    if (dialogEsperaSaldoKtaxi.isShowing()) {
                        dialogEsperaSaldoKtaxi.dismiss();
                    }
                }
                if (respuesta != null) {
                    Builder builder = new Builder(DetalleCompraServicioActivity.this);
                    builder.setMessage(respuesta)
                            .setTitle("Informacion")
                            .setCancelable(false)
                            .setPositiveButton("Aceptar", new OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                }
                            }).show();
                } else {
                    Builder builder = new Builder(DetalleCompraServicioActivity.this);
                    builder.setMessage("La aperación no se pudo complatar intente nuevamente.")
                            .setTitle("Información")
                            .setCancelable(true)
                            .setPositiveButton("Aceptar", new OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            }).show();
                }
            }
        });

    }

    @Override
    public void lanzarDialogoCompra(final int posicion) {
        Log.e(TAG, "lanzarDialogoCompra: ");
        if (dialogComprar != null) {
            if (dialogComprar.isShowing()) {
                return;
            }
        }
        Builder builder = new Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_comprar, null);
        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        final EditText etValor = v.findViewById(R.id.et_valor_pagar);
        final TextView etInformacion = v.findViewById(R.id.tv_informacion);
        final SaldoKtaxi.CompraServicios.LS dato = rvAdapterCompraServicios.getDatos().get(posicion);
        etInformacion.setText("En el establecimiento " + dato.getEmpresa() + " El saldo maximo que puede usar " + total);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etValor.getText().toString().isEmpty()) {
                    Toast.makeText(DetalleCompraServicioActivity.this, "Ingrese el valor que desea pagar.", Toast.LENGTH_LONG).show();
                    etValor.requestFocus();
                    return;
                }
                if (Double.parseDouble(etValor.getText().toString()) > total) {
                    Toast.makeText(DetalleCompraServicioActivity.this, "El valor maximo que puede usar es " + total, Toast.LENGTH_LONG).show();
                    etValor.requestFocus();
                    return;
                }
                if (dialogComprar != null) {
                    if (dialogComprar.isShowing()) {
                        dialogComprar.dismiss();
                        dialogComprar = null;
                    }
                }
                servicioSocket.compraServicio(etValor.getText().toString(), dato.getIdEmpresa());
//                createEsperaSaldoKtaxiDialogo();
                esperaCobro();
            }
        });
        Button btnCancelar = v.findViewById(R.id.btn_cancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialogComprar != null) {
                    if (dialogComprar.isShowing()) {
                        dialogComprar.dismiss();
                        dialogComprar = null;
                    }
                }
            }
        });

        builder.setView(v);

        builder.create();

        dialogComprar = builder.show();
    }

    @Override
    public void aunNoEnLugar(final int posicion) {
        Builder builder = new Builder(this);
        builder.setMessage("Usted no se encuentra dentro del lugar que esta intentando realizar el pago. ¿Desea realizar el pago de todas formas?.")
                .setTitle("Alerta")
                .setPositiveButton("Aceptar", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        lanzarDialogoCompra(posicion);
                    }
                })
                .setNegativeButton("Cancelar", new OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, ServicioSockets.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        new Utilidades().hideFloatingView(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mConnection != null) {
            unbindService(mConnection);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home: // ID del boton
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void repsuestaListaSaldoKtaxi(SaldoKtaxi.ItemSaldoKtaxi itemSaldoKtaxi) {
        if (itemSaldoKtaxi != null) {
            if (itemSaldoKtaxi.getLS().size() > 0) {
                totalSaldo(itemSaldoKtaxi.getLS());
            } else {
                Toast.makeText(this, "No se pudo obtener los datos del saldo.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public double total = 0;

    public double totalSaldo(List<SaldoKtaxi.ItemSaldoKtaxi.LS> datos) {
        for (com.kradac.conductor.modelo.SaldoKtaxi.ItemSaldoKtaxi.LS ls : datos) {
            total = total + ls.getSaldo();
        }
        return total;
    }


    private Dialog dialogEsperaSaldoKtaxi;

    /**
     * Espera dialogo saldo Ktaxi
     *
     * @return Diálogo
     */
    public void createEsperaSaldoKtaxiDialogo(String pin,long cronometro) {
        if (dialogEsperaSaldoKtaxi != null) {
            if (dialogEsperaSaldoKtaxi.isShowing()) {
                return;
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        builder.setCancelable(false);
        View v = inflater.inflate(R.layout.dialog_espera_saldo_ktaxi, null);
        ImageView imgCodeQr = v.findViewById(R.id.img_code_qr);
        TextView tvCodigo = v.findViewById(R.id.tv_codigo_transaccion);
        tvCodigo.setText("Código transacción: \n".concat(pin));
        final TextView tvCronometro = v.findViewById(R.id.tv_cronometro);
        new CountDownTimer(cronometro, 1000) {
            public void onTick(long millisUntilFinished) {
                int segundo =(int) millisUntilFinished / 1000;
                if (segundo<10){
                    tvCronometro.setText("00:".concat("0"+String.valueOf(segundo)));
                }else{
                    tvCronometro.setText("00:".concat(String.valueOf(segundo)));
                }
            }

            public void onFinish() {
                tvCronometro.setText("Expirado!");
            }
        }.start();
        Bitmap codeConfirmacion = QRCode.from(pin).bitmap();
        imgCodeQr.setImageBitmap(codeConfirmacion);
        builder.setView(v);
        dialogEsperaSaldoKtaxi = builder.create();
        dialogEsperaSaldoKtaxi.show();
    }

    @Override
    public void onRespuestaCompra(final String pin,final long cronometro) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (pDialog != null) {
                    if (pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                }
                createEsperaSaldoKtaxiDialogo(pin,cronometro);
            }
        });
    }
}
