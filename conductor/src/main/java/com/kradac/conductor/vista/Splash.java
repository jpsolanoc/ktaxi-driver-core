package com.kradac.conductor.vista;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.kradac.conductor.R;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionMain;
import com.kradac.conductor.modelo.ConfiguracionServidor;
import com.kradac.conductor.presentador.IniciarSesionPresentador;
import com.kradac.conductor.presentador.MainPresentador;
import com.kradac.conductor.presentador.MapaPresenter;
import com.kradac.conductor.service.ServicioSockets;

import org.json.JSONException;
import org.json.JSONObject;

//import com.cumberland.weplansdk.WeplanSdk;


public class Splash extends BaseActivity implements OnComunicacionMain {

    private static final String TAG = Splash.class.getName();
    private static final int MY_PERMISSIONS = 2;
    private SharedPreferences spLogin;
    private Utilidades utilidades;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Dialog dialog;
    private int nivel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash);
        spLogin = getSharedPreferences("login", Context.MODE_PRIVATE);
        if (ServicioSockets.isRunService) {
            if (spLogin.getBoolean("logeado", false)) {
                new MapaPresenter(null, this).getConfiguracionTaximetro(spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0));
                Intent localIntent1 = new Intent(Splash.this, MapaBaseActivity.class);
                localIntent1.setAction("android.intent.action.SEND");
                startActivity(localIntent1);
                finish();
            }
        }
        utilidades = new Utilidades();
        TextView tvVersion = (TextView) findViewById(R.id.tvVersionApp);
        tvVersion.setText(utilidades.obtenerVersion(this));
        nivel = 0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (0 != ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) && 0 != ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) && 0 != ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS);
        } else if (0 != ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS);
        } else if (0 != ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS);
        } else if (0 != ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_PHONE_STATE},
                    MY_PERMISSIONS);
        } else {
            conectarServer();
        }

            new Utilidades().hideFloatingView(this);

    }

    public Dialog dialogActualizar;

    @Override
    protected void onPause() {
        super.onPause();
    }


    public boolean isPermisos[];

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        isPermisos = new boolean[permissions.length];
        switch (requestCode) {
            case MY_PERMISSIONS: {
                for (int i = 0; i < permissions.length; i++) {
                    switch (permissions[i]) {
                        case android.Manifest.permission.ACCESS_FINE_LOCATION:
                            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                                alternativaNoAceptaPermisos();
                                isPermisos[i] = false;
                            } else {
                                isPermisos[i] = true;
                            }
                            break;
                        case android.Manifest.permission.WRITE_EXTERNAL_STORAGE:
                            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                                alternativaNoAceptaPermisos();
                                isPermisos[i] = false;
                            } else {
                                isPermisos[i] = true;
                            }
                            break;
                        case Manifest.permission.READ_PHONE_STATE:
                            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                                alternativaNoAceptaPermisos();
                                isPermisos[i] = false;
                            } else {
                                isPermisos[i] = true;
                            }
                            break;
                    }
                }

            }
        }
        for (int j = 0; j < isPermisos.length; j++) {
            if (isPermisos[j]) {
                conectarServer();
            }
        }
    }

    public void alternativaNoAceptaPermisos() {
        Intent inten = new Intent();
        inten.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        inten.addCategory(Intent.CATEGORY_DEFAULT);
        inten.setData(Uri.parse("package:" + getPackageName()));
        inten.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        inten.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        inten.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(inten);
        utilidades.customToast(this, "Por favor active los accesos necesarios");
        finish();
    }

    private int contadorExcepciones = 0;

    public void conectarServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (!ServicioSockets.isRunService) {
                    if (utilidades.estaConectado(Splash.this)) {
                        if (utilidades.pingGoogle()) {
                            SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                            if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                                new MainPresentador(Splash.this, null, null, Splash.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_https());
                            } else {
                                new MainPresentador(Splash.this, null, null, Splash.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_https());
                            }
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
                                    builder.setMessage("Usted no cuenta con internet para navegación")
                                            .setTitle("Alerta")
                                            .setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    finish();
                                                    if (spLogin.getBoolean("logeado", false)) {
                                                        Intent localIntent1 = new Intent(Splash.this,
                                                                MapaBaseActivity.class);
                                                        localIntent1.setAction("android.intent.action.SEND");
                                                        startActivity(localIntent1);
                                                    } else {
                                                        Intent localIntent2 = new Intent(Splash.this,
                                                                IniciarSesionBaseActivity.class);
                                                        startActivity(localIntent2);
                                                    }
                                                }
                                            })
                                            .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    finish();
                                                }
                                            });
                                    if (!isFinishing()) {
                                        builder.show();
                                    }
                                }
                            });
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
                                builder.setMessage("Usted no tiene conexión a ninguna red")
                                        .setTitle("Alerta")
                                        .setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                finish();
                                                if (spLogin.getBoolean("logeado", false)) {
                                                    Intent localIntent1 = new Intent(Splash.this,
                                                            MapaBaseActivity.class);
                                                    localIntent1.setAction("android.intent.action.SEND");
                                                    startActivity(localIntent1);
                                                } else {
                                                    Intent localIntent2 = new Intent(Splash.this,
                                                            IniciarSesionBaseActivity.class);
                                                    startActivity(localIntent2);
                                                }
                                            }
                                        })
                                        .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                finish();
                                            }
                                        });
                                if (!isFinishing()) {
                                    builder.show();
                                }
                            }
                        });
                    }
                }
            }
        }).start();
    }

    @Override
    public void respuestaPin(boolean isRespuesta, String ipRespuesta) {
        SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
        if (isRespuesta) {
            new MainPresentador(this, null, null, this, null).obtenerConfiguracionServerNew(ipRespuesta, VariablesGlobales.NUM_ID_APLICATIVO, VariablesGlobales.ID_PLATAFORMA, VariablesGlobales.APP, utilidades.obtenerVersion(this));
            nivel = 0;
        } else {
            switch (nivel) {
                case 0:
                    nivel++;
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(this, null, null, this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_http());
                    } else {
                        new MainPresentador(this, null, null, this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_http());
                    }
                    break;
                case 1:
                    nivel++;
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(this, null, null, this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_ip().replace(":8080", ""));
                    } else {
                        new MainPresentador(this, null, null, this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_ip().replace(":8080", ""));
                    }

                    break;
                case 2:
                    nivel++;
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(this, null, null, this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_ip());
                    } else {
                        new MainPresentador(this, null, null, this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_ip());
                    }
                    break;
                case 3:
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(this, null, null, this, null).getHelp();
                    } else {
                        new MainPresentador(this, null, null, this, null).getHelp();
                    }
                    break;
            }
        }
    }

    @Override
    public void configuracionServidorNew(boolean isRespuesta) {
        if (isRespuesta) {
            finish();
            if (spLogin.getBoolean("logeado", false)) {
                new IniciarSesionPresentador(null).enviarMetaDataServer(Splash.this,
                        spLogin.getInt("idUsuario", 0));
                Intent localIntent1 = new Intent(Splash.this,
                        MapaBaseActivity.class);
                localIntent1.setAction("android.intent.action.SEND");
                startActivity(localIntent1);
            } else {
                Intent localIntent2 = new Intent(Splash.this,
                        IniciarSesionBaseActivity.class);
                startActivity(localIntent2);
            }
        } else {
            if (contadorExcepciones >= 3) {
                if (utilidades.estaConectado(this)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("No se pueden descargar las configuraciones iniciales.")
                            .setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                    if (spLogin.getBoolean("logeado", false)) {
                                        Intent localIntent1 = new Intent(Splash.this,
                                                MapaBaseActivity.class);
                                        localIntent1.setAction("android.intent.action.SEND");
                                        startActivity(localIntent1);
                                    } else {
                                        Intent localIntent2 = new Intent(Splash.this,
                                                IniciarSesionBaseActivity.class);
                                        startActivity(localIntent2);
                                    }
                                }
                            })
                            .setNegativeButton("Salir", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                }
                            });
                    if (!isFinishing()) {
                        builder.show();
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Compruebe su conexión a internet.")
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                    if (spLogin.getBoolean("logeado", false)) {
                                        Intent localIntent1 = new Intent(Splash.this,
                                                MapaBaseActivity.class);
                                        localIntent1.setAction("android.intent.action.SEND");
                                        startActivity(localIntent1);
                                    } else {
                                        Intent localIntent2 = new Intent(Splash.this,
                                                IniciarSesionBaseActivity.class);
                                        startActivity(localIntent2);
                                    }
                                }
                            });
                    if (!isFinishing()) {
                        builder.show();
                    }
                }
            } else {
                try {
                    SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                    ConfiguracionServidor configuracionServidor = ConfiguracionServidor.createConfiguracionServidor(spConfigServerNew.getString(VariablesGlobales.CONFIG_IP_NEW, VariablesGlobales.CONFIGURACION_INICIAL));
                    new MainPresentador(this, null, null, this, null).obtenerConfiguracionServerNew(configuracionServidor.getDns().getDnsE(), VariablesGlobales.NUM_ID_APLICATIVO, VariablesGlobales.ID_PLATAFORMA, VariablesGlobales.APP, utilidades.obtenerVersion(this));
                    contadorExcepciones++;
                } catch (NullPointerException e) {
                    e.getMessage();
                }
            }

        }


    }

    @Override
    public void respuestaHelp(String json) {
        if (json != null) {
            try {
                final JSONObject object = new JSONObject(json);
                if (object.has("en")) {
                    switch (object.getInt("en")) {
                        case 0:
                            nivel = 0;
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Thread.sleep(5000);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                                                if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                                                    new MainPresentador(Splash.this, null, null, Splash.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_https());
                                                } else {
                                                    new MainPresentador(Splash.this, null, null, Splash.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_https());
                                                }
                                                try {
                                                    Toast.makeText(Splash.this, object.getString("m"), Toast.LENGTH_LONG).show();
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                            break;
                        case 1:
                            if (dialogActualizar != null) {
                                if (dialogActualizar.isShowing()) {
                                    return;
                                }
                            }
                            AlertDialog.Builder builder = new AlertDialog.Builder(this);
                            builder.setMessage(object.getString("m"))
                                    .setCancelable(false)
                                    .setTitle("Alerta")
                                    .setPositiveButton("Actualizar", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            utilidades.calificarApp(Splash.this);
                                            finish();
                                        }
                                    });
                            dialogActualizar = builder.create();
                            if (!isFinishing()) {
                                dialogActualizar.show();
                            }

                            break;
                        case 2:
                            new MainPresentador(this, null, null, this, null).getConfiuracionInicial(object.getString("url"));
                            break;
                    }
                }
            } catch (JSONException e) {

                nivel = 0;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(5000);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                                        new MainPresentador(Splash.this, null, null, Splash.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_https());
                                    } else {
                                        new MainPresentador(Splash.this, null, null, Splash.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_https());
                                    }
                                }
                            });
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        } else {
            nivel = 0;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(5000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                                if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                                    new MainPresentador(Splash.this, null, null, Splash.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_https());
                                } else {
                                    new MainPresentador(Splash.this, null, null, Splash.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_https());
                                }
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }


    public void alternativaFallaRespuestaServer(boolean isGoogle) {
//        if (checkPlayServices()){
        if (!isGoogle) {

            utilidades.customToast(Splash.this, getString(R.string.cargar_experiencias));
        }
        if (spLogin.getBoolean("logeado", false)) {
            Intent localIntent1 = new Intent(Splash.this, MapaBaseActivity.class);
            localIntent1.setAction("android.intent.action.SEND");
            startActivity(localIntent1);
        } else {
            Intent localIntent2 = new Intent(Splash.this, IniciarSesionBaseActivity.class);
            startActivity(localIntent2);
        }
        finish();
//        }
    }

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                if (utilidades.isVerificarDialogo(dialog)) {
                    dialog = googleAPI.getErrorDialog(this, result,
                            PLAY_SERVICES_RESOLUTION_REQUEST);
                    dialog.setCancelable(false);
                    if (!isFinishing()) {
                        dialog.show();
                    }

                }
            }
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PLAY_SERVICES_RESOLUTION_REQUEST:
//                if (checkPlayServices()){
                alternativaFallaRespuestaServer(true);
//                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
