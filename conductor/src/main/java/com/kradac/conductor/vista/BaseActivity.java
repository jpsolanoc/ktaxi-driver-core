package com.kradac.conductor.vista;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.kradac.conductor.extras.UrlKtaxiConductor;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.modelo.ConfiguracionServidor;

/**
 * Created by Ing.John Patricio Solano Cabrera on 21/3/2017.
 * 0979256768
 * jpsolanoc@gmail.com
 * johnps_1987@hotmail.com
 * Developer
 */

public abstract class BaseActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private UrlKtaxiConductor urlKtaxiConductor;
    private GoogleApiClient mGoogleApiClient;
    private Internet mReceiver;
    public Dialog dialogEspera;
    private static final String DIALOG_ERROR = "dialog_error";
    private boolean mResolvingError = false;
    private Context context;
    public static final int REQUEST_LOCATION = 34;
    private static final int REQUEST_RESOLVE_ERROR = 35;
    private ProgressDialog pDialog;
    LocationManager locationManager;
    LocationRequest locationRequest;
    GoogleApiClient googleApiClient;
    LocationSettingsRequest.Builder locationSettingsRequest;
    PendingResult<LocationSettingsResult> pendingResult;
    public boolean isOpenConfiguracionGps;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mReceiver = new Internet();
        urlKtaxiConductor = new UrlKtaxiConductor();
        context = this;
        SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
        ConfiguracionServidor configuracionServidor = ConfiguracionServidor.createConfiguracionServidor(spConfigServerNew.getString(VariablesGlobales.CONFIG_IP_NEW, VariablesGlobales.CONFIGURACION_INICIAL));
        if (configuracionServidor.getIpUsada() == null || configuracionServidor.getIpUsada().equals("")) {
            UrlKtaxiConductor urlKtaxiConductor = new UrlKtaxiConductor();
            if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                VariablesGlobales.setIpServer(spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true), urlKtaxiConductor.getUrl_produccion_https());
            } else {
                VariablesGlobales.setIpServer(spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true), urlKtaxiConductor.getUrl_desarrollo_https());
            }

        } else {
            VariablesGlobales.setIpServer(spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true), configuracionServidor.getIpUsada());
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */,
                        this /* OnConnectionFailedListener */)
                .addApi(LocationServices.API)
                .build();
    }


    public UrlKtaxiConductor getUrlKtaxiConductor() {
        return urlKtaxiConductor;
    }

    public void setUrlKtaxiConductor(UrlKtaxiConductor urlKtaxiConductor) {
        this.urlKtaxiConductor = urlKtaxiConductor;
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (mResolvingError) {
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                mGoogleApiClient.connect();
            }
        } else {
            showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (resultCode == RESULT_CANCELED) {
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        mEnableGps();
                    }
                }
                break;
        }
    }

    private void showErrorDialog(int errorCode) {
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "errordialog");
    }

    public void onDialogDismissed() {
        mResolvingError = false;
    }

    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() {
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GoogleApiAvailability.getInstance().getErrorDialog(
                    this.getActivity(), errorCode, REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((BaseActivity) getActivity()).onDialogDismissed();
        }
    }

    /**
     * Registra un reciver que es el encargado de verificar si se tiene o no internet en la app
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mEnableGps();
        }
        if (mReceiver != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver(mReceiver, intentFilter);
        }
    }

    public void mEnableGps() {
        if (!isOpenConfiguracionGps) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
            googleApiClient.connect();
            mLocationSetting();
            isOpenConfiguracionGps = true;
        }
    }

    /**
     * Se desregistra el reciver para cuando esta la actividad en pausa
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
    }

    /**
     * Clase encargada de recivir eventos en los cuales se encuantra la conexión a internet
     */
    public class Internet extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || intent.getExtras() == null)
                return;
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = manager.getActiveNetworkInfo();
            if (ni != null && ni.getState() == NetworkInfo.State.CONNECTED) {
                cambioInterent(true);
            } else if (intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, Boolean.FALSE)) {
                cambioInterent(false);
            }
        }
    }

    /**
     * Cuando se quiere dectar el cambio del internet
     *
     * @param isIntenet
     */
    public void cambioInterent(boolean isIntenet) {

    }

    /**
     * Muestra un dialogo de espera de cualquier activiadad de donde sea llamado
     *
     * @param titulo
     */
    public void mostrarEspera(String titulo) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialogEspera = new ProgressDialog(context);
                dialogEspera.setCancelable(false);
                dialogEspera.setTitle(titulo);
                dialogEspera.show();
            }
        });
    }

    /**
     * Oculta el dialogo del sistema.
     */
    public void ocultarEspera() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dialogEspera != null && dialogEspera.isShowing()) dialogEspera.dismiss();
            }
        });
    }

    public void mLocationSetting() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationSettingsRequest = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        mResult();

    }

    public void mResult() {
        pendingResult = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, locationSettingsRequest.build());
        pendingResult.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                isOpenConfiguracionGps = false;
                Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(BaseActivity.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_LOCATION);
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }

        });
    }


    public void probarGPSInicio() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mEnableGps();
        }
    }



}
