package com.kradac.conductor.vista;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.VideoView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.wigets.GifImageView;
import com.kradac.wigets.extra.VariablesPublicidad;
import com.kradac.wigets.modelo.RespuestaPublicidad;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ComponentePublicitarioActivity extends AppCompatActivity {

    private int stopPosition;
    public boolean isAtras;

    private static final String TAG = ComponentePublicitarioActivity.class.getName();
    @BindView(R2.id.gif_image_view)
    GifImageView gifImageView;
    @BindView(R2.id.img_publicidad)
    ImageView imgPublicidad;
    @BindView(R2.id.video_view_publicidad)
    VideoView videoViewPublicidad;
    @BindView(R2.id.imb_close)
    ImageButton imbClose;
    @BindView(R2.id.fgm_video)
    FrameLayout fgmVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_componente_publicitario);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            RespuestaPublicidad respuestaPublicidad = bundle.getParcelable("respuestaPublicitaria");
            cargarPublicidad(respuestaPublicidad);
        } else {
            finish();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (videoViewPublicidad.isPlaying()) {
            stopPosition = videoViewPublicidad.getCurrentPosition(); //stopPosition is an int
            videoViewPublicidad.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
        Log.d(TAG, "onResume called");
        if (stopPosition != 0) {
            videoViewPublicidad.seekTo(stopPosition);
            videoViewPublicidad.start(); //Or use resume() if it doesn't work. I'm not sure
        }
    }

    @OnClick(R2.id.imb_close)
    public void onClick() {
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (isAtras) {
                    finish();
                }
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void cargarPublicidad(RespuestaPublicidad respuestaPublicidad) {
        if (respuestaPublicidad != null && respuestaPublicidad.getB() != null) {
            switch (respuestaPublicidad.getB().getTipo()) {
                case VariablesPublicidad.RECURSO_VIDEO:
                    imgPublicidad.setVisibility(View.GONE);
                    gifImageView.setVisibility(View.GONE);
                    if (respuestaPublicidad.getRutaRecurso() != null && !respuestaPublicidad.getRutaRecurso().isEmpty())
                        cargarVideo(Uri.parse("file:" + respuestaPublicidad.getRutaRecurso()));
                    else
                        finish();
                    break;
                case VariablesPublicidad.RECURSO_IMAGEN:
                    gifImageView.setVisibility(View.GONE);
                    fgmVideo.setVisibility(View.GONE);
                    videoViewPublicidad.setVisibility(View.GONE);
                    if (respuestaPublicidad.getB().getRecurso() != null && !respuestaPublicidad.getB().getRecurso().isEmpty())
                        cargarImagen(respuestaPublicidad.getB().getRecurso());
                    else
                        finish();
                    break;
                case VariablesPublicidad.RECURSO_GIF:
                    imgPublicidad.setVisibility(View.GONE);
                    gifImageView.setVisibility(View.VISIBLE);
                    fgmVideo.setVisibility(View.GONE);
                    if (respuestaPublicidad.getRutaRecurso() != null && !respuestaPublicidad.getRutaRecurso().isEmpty())
                        cargarGIF(Uri.parse("file:" + respuestaPublicidad.getRutaRecurso()));
                    else
                        finish();
                    break;
            }
            if (respuestaPublicidad.getB().getTiempo() > 0) {
                if (respuestaPublicidad.getB().getCerrado() == 3) {
                    imbClose.setVisibility(View.VISIBLE);
                    isAtras = true;
                } else {
                    tiempoBoton(respuestaPublicidad.getB().getTiempo(), respuestaPublicidad.getB().getCerrado());
                }

            }
        }
    }

    public void cargarVideo(Uri uri) {
        Log.e(TAG, "cargarVideo: " + uri.getPath());
        try {
            videoViewPublicidad.setVisibility(View.VISIBLE);
            videoViewPublicidad.setVideoURI(uri);
            videoViewPublicidad.requestFocus();
            videoViewPublicidad.start();
            videoViewPublicidad.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer arg0) {
                    arg0.setLooping(true);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void cargarImagen(String url) {
        imgPublicidad.setVisibility(View.VISIBLE);
        new Utilidades().cargarImagenPublicitaria(imgPublicidad, url, this, null);
    }

    public void cargarGIF(Uri uri) {
        gifImageView.setGifImageUri(uri);
        gifImageView.setVisibility(View.VISIBLE);
    }

    public void tiempoBoton(int timeTotal, final int cerrado) {
        CountDownTimer countDownTimer = new CountDownTimer(timeTotal * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                switch (cerrado) {
                    case VariablesPublicidad.CERRADO_BOTON_AL_CUMPLIR_TIEMPO:
                        imbClose.setVisibility(View.VISIBLE);
                        isAtras = true;
                        break;
                    case VariablesPublicidad.CERRADO_AL_CUMPLIR_TIEMPO:
                        finish();
                        break;
                }
            }
        };
        countDownTimer.start();
    }


}
