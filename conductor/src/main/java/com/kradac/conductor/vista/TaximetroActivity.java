package com.kradac.conductor.vista;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComuniacionTaximetro;
import com.kradac.conductor.service.ServicioSockets;
import com.kradac.conductor.service.TaximetroService;
import com.zj.btsdk.BluetoothService;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by John on 09/01/2017.
 */

public class TaximetroActivity extends AppCompatActivity implements OnComuniacionTaximetro {

    private static final String TAG = TaximetroService.class.getName();
    @BindView(R2.id.video_view)
    VideoView videoView;
    @BindView(R2.id.tv_sub_total)
    TextView tvSubTotal;
    @BindView(R2.id.tv_hora_inicio)
    TextView tvHoraInicio;
    @BindView(R2.id.tv_hora_fin)
    TextView tvHoraFin;
    @BindView(R2.id.tv_distancia_recorrida)
    TextView tvDistanciaRecorrida;
    @BindView(R2.id.tv_velocidad)
    TextView tvVelocidad;
    @BindView(R2.id.tv_tiempo)
    TextView tvTiempo;
    @BindView(R2.id.tv_cosoto_arranque)
    TextView tvCosotoArranque;
    @BindView(R2.id.tv_valor_distancia)
    TextView tvValorDistancia;
    @BindView(R2.id.tv_tiempo_espera)
    TextView tvTiempoEspera;
    @BindView(R2.id.tv_costo_tiempo)
    TextView tvCostoTiempo;
    @BindView(R2.id.btn_finalizar)
    Button btnFinalizar;
    @BindView(R2.id.btn_imprimir)
    Button btnImprimir;
    @BindView(R2.id.btn_minimizar_taximetro)
    Button btnMinimizarTaximetro;
    @BindView(R2.id.tv_log_distancia_total)
    TextView tvLogDistanciaTotal;
    @BindView(R2.id.tv_log)
    TextView tvLog;
    @BindView(R2.id.floatin_btn)
    FloatingActionButton floatinBtn;
    @BindView(R2.id.row_tiempo_espera)
    TableRow rowTiempoEspera;

    private ServicioSockets servicioSocket;
    private Utilidades utilidades;
    private boolean isTaximetro;
    private boolean mBound;
    private BluetoothService mService = null;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private SharedPreferences spLogin;
    private int valorEditarCostoC;

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ServicioSockets.LocalBinder binder = (ServicioSockets.LocalBinder) service;
            servicioSocket = binder.getService();
            servicioSocket.registerCliente(TaximetroActivity.this, "TaximetroActivity");
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    private String totalCarrera;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taximetro);
        ButterKnife.bind(this);

        spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        utilidades = new Utilidades();
        btnImprimir.setVisibility(View.GONE);
//        String uriPath = "android.resource://" + getPackageName() + "/" + R.raw.ktaxi_ec;
//        Uri uri = Uri.parse(uriPath);
//        videoView.setVisibility(View.GONE);
//        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            videoView.setVideoURI(uri);
//            videoView.requestFocus();
//            videoView.start();
//        }
        if (!isTaximetro) {
//            tvHoraInicio.setText(TaximetroService.obtenerHora());
            isTaximetro = true;
        }
        mService = new BluetoothService(this, mHandler);
        if (!mService.isAvailable()) {
            utilidades.customToastCorto(this, "Bluetooth no esta activo");
        }
        tvLogDistanciaTotal.setText("0");

        IntentFilter filter1 = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mBroadcastReceiver1, filter1);
    }

    @OnClick({R2.id.btn_finalizar, R2.id.btn_imprimir, R2.id.btn_minimizar_taximetro})
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btn_finalizar) {
            tvSubTotal.setText("$ " + totalCarrera);
            isTaximetro = false;
            tvHoraFin.setText(utilidades.obtenerHora());
            servicioSocket.finalizarTaximetro();
            btnFinalizar.setEnabled(false);
            btnMinimizarTaximetro.setText("SALIR");

        } else if (i == R.id.btn_imprimir) {
            imprimirTexto();
        } else if (i == R.id.btn_minimizar_taximetro) {
            Intent startMain = new Intent(TaximetroActivity.this, MapaBaseActivity.class);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);

        }
    }

    public void imprimirTexto() {
        tvHoraFin.setText(utilidades.getHoraActual());
        String msg = "";
        String lang = getString(R.string.strLang);
        byte[] cmd = new byte[3];
        cmd[0] = 0x1b;
        cmd[1] = 0x21;
        cmd[2] |= 0x10;
        mService.write(cmd);
        cmd[2] &= 0xEF;
        mService.write(cmd);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        DecimalFormat formatoDosDecimales1 = new DecimalFormat("#.##");
        msg = "****KRADAC****\n" + "****Original****\n" +
                spLogin.getString("nombres", " ") + " " + spLogin.getString("apellidos", " ") +
                "\nRUC: 1104267788" +
                "\ntelefono:" + spLogin.getString("celular", " ") +
                "\nSerial:TQ-241" + "\nLOJA\n" +
                "NoTQ-2413" + "\n-----------" +
                "\nAut. SRI.No: TQ-2413" +
                "\nValido:18/MAR/16" +
                "\nPlaca:" + spLogin.getString("placaVehiculo", "") +
                "\nTaxi:" + spLogin.getString("regMunVehiculo", "") +
                "\nFecha:" + sdf.format(new Date()) +
                "\nEcu 911\n-------\n" +
                "Hr.Inici:" + tvHoraInicio.getText() +
                "\nHr.Final:" + tvHoraFin.getText() +
                "\nDist.Rec:" + tvDistanciaRecorrida.getText() +
                "\n\nTipo Tarifa:1" +
                "\nTIEMPO SEG:" + tvTiempo.getText() +
                "\nBANDERAZO:" + tvCosotoArranque.getText() +
                "\nVr.INI.DIST:" + tvValorDistancia.getText() +
                "\nVr.INIC.TIEM:" + tvCostoTiempo.getText() +
                "\nSubtotal:" + totalCarrera +
                "\n\nIVA:0" +
                "\nPAGA:" + totalCarrera +
                "\n****KRADAC****\n\n";
        mService.sendMessage(msg, "GBK");
        utilidades.customToastCorto(TaximetroActivity.this, "Imprimiendo");
        stopService(new Intent(TaximetroActivity.this, TaximetroService.class));
        tvHoraFin.setText(utilidades.obtenerHora());
        tvSubTotal.setText("$ " + totalCarrera);
        isTaximetro = false;
        btnFinalizar.setEnabled(false);
        btnImprimir.setEnabled(false);
        btnMinimizarTaximetro.setText("SALIR");
    }


    @Override
    public void notificarRastreo(final int editarCostoC,final int tipo, final String tiempoEspera, final String valorTiempo, final String velocidadtxt,
                                 final String distanciaRec, final String valorPrincipal, final String valDistancia,
                                 final String cronometro, final String horaInicioTxt, final String costoArranque,
                                 final String totalCarrea) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                valorEditarCostoC = editarCostoC;
                if (tipo == 1) {
                    rowTiempoEspera.setVisibility(View.VISIBLE);
                } else {
                    rowTiempoEspera.setVisibility(View.GONE);
                }
                TaximetroActivity.this.totalCarrera = totalCarrea;
                tvHoraInicio.setText(horaInicioTxt);
                tvDistanciaRecorrida.setText(distanciaRec);
                tvVelocidad.setText(velocidadtxt);
                tvTiempo.setText(cronometro);
                tvCosotoArranque.setText(costoArranque);
                tvValorDistancia.setText(valDistancia);
                tvTiempoEspera.setText(tiempoEspera);
                tvCostoTiempo.setText(valorTiempo);
                tvSubTotal.setText(valorPrincipal);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, ServicioSockets.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        new Utilidades().hideFloatingView(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(serviceConnection);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mService.isBTopen() == false) {
            try {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            } catch (NullPointerException e) {
                utilidades.customToastCorto(this, "El teléfono no cuenta con Bluetooth.");
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mService != null) {
            mService.stop();
            mService = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_taximetro, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.menu_bluethoo) {
            Intent serverIntent = new Intent(TaximetroActivity.this, DeviceListActivity.class);
            startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            return true;
        } else if (i == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private boolean isEnlazado;
    /**
     * hilo para verficar el estado de la impresora
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothService.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            isEnlazado = true;
                            Toast.makeText(getApplicationContext(), "Conectado correctamente",
                                    Toast.LENGTH_SHORT).show();
                            btnImprimir.setVisibility(View.VISIBLE);
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            break;
                        case BluetoothService.STATE_LISTEN:
                            break;
                        case BluetoothService.STATE_NONE:
                            break;
                    }
                    break;
                case BluetoothService.MESSAGE_CONNECTION_LOST:
                    isEnlazado = false;
                    Toast.makeText(getApplicationContext(), "Se perdió conexión en el dispositivo",
                            Toast.LENGTH_SHORT).show();
                    btnImprimir.setVisibility(View.GONE);
                    break;
                case BluetoothService.MESSAGE_UNABLE_CONNECT:
                    isEnlazado = false;
                    Toast.makeText(getApplicationContext(), "No se a podido conectar con el dispositivo",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }

    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    utilidades.mostrarMensajeCorto(this, "Bluetooth abierto");
                } else {
                    utilidades.mostrarMensajeCorto(this, "No se puede utilizar la impresora si no se enciende el Bluethoo");
                }
                break;
            case REQUEST_CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras()
                            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice con_dev = mService.getDevByMac(address);
                    mService.connect(con_dev);
                }
                break;
        }
    }

    @OnClick({R2.id.btn_finalizar, R2.id.btn_imprimir, R2.id.btn_minimizar_taximetro})
    public void onViewClicked(View view) {
        int i = view.getId();
        if (i == R.id.btn_finalizar) {
        } else if (i == R.id.btn_imprimir) {
        } else if (i == R.id.btn_minimizar_taximetro) {
        }
    }


    @OnClick(R2.id.floatin_btn)
    public void onClick() {
        if (mService.isBTopen() == false) {
            try {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            } catch (NullPointerException e) {
                utilidades.customToastCorto(this, "El teléfono no cuenta con Bluetooth.");
            }
        } else {
            if (!isEnlazado) {
                Intent serverIntent = new Intent(TaximetroActivity.this, DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            } else {
                Toast.makeText(this, "Ya estas enlazado a la impresora.", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        mService.stop();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        break;
                }

            }
        }
    };
}
