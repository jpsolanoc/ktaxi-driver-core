package com.kradac.conductor.vista;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kradac.calendarview.CalendarMounth;
import com.kradac.calendarview.CustomCalendarView;
import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.HistorialRecyclerAdapter;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.OnComunicacionHistorial;
import com.kradac.conductor.modelo.ItemHistorialCompras;
import com.kradac.conductor.modelo.ItemHistorialSolicitud;
import com.kradac.conductor.presentador.HistorialModificado;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistorialActivity extends AppCompatActivity implements OnComunicacionHistorial {

    public static final String NOMBRE_PREFERENCIAS = "preferenciasV1";
    private static final String TAG = HistorialActivity.class.getName();
    @BindView(R2.id.imb_atras)
    ImageButton imbAtras;
    @BindView(R2.id.tv_titulo)
    TextView tvTitulo;
    @BindView(R2.id.customCalendarView)
    CustomCalendarView customCalendarView;
    @BindView(R2.id.tvMensajeHistorial)
    TextView tvMensajeHistorial;
    @BindView(R2.id.tvCantidad)
    TextView tvCantidad;
    @BindView(R2.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R2.id.skProgres_historial)
    ProgressBar skProgresHistorial;

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }


    private LinearLayoutManager linearLayoutManager;
    private HistorialRecyclerAdapter adapter;
    private int idUsuario;
    private HistorialModificado historialPresentador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_cliente);
        ButterKnife.bind(this);
        SharedPreferences spobtener;
        spobtener = getSharedPreferences("login", Context.MODE_PRIVATE);
        idUsuario = spobtener.getInt("idUsuario", 0);
        linearLayoutManager = new LinearLayoutManager(this);
        historialPresentador = new HistorialModificado(this);
        myRecyclerView.setLayoutManager(linearLayoutManager);
        imbAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        cargarEventoCalendario();
        historialPresentador.obtenerCuantosTotal(idUsuario);
    }


    private CalendarMounth calendarMounth;


    public void cargarEventoCalendario() {
        customCalendarView.setCalendarListener(new CustomCalendarView.CalendarListener() {
            @Override
            public void onCalendarCurrent(CalendarMounth calendarMounth) {
                HistorialActivity.this.calendarMounth = calendarMounth;
                limpiarLista();
                historialPresentador.cuantosPorAnioMes(idUsuario, calendarMounth.getYear(), calendarMounth.getMounth(), 1);
                mostrarEspera();
            }

            @Override
            public void onCalendarLastChanged(CalendarMounth calendarMounth) {
                HistorialActivity.this.calendarMounth = calendarMounth;
                limpiarLista();
                historialPresentador.cuantosPorAnioMes(idUsuario, calendarMounth.getYear(), calendarMounth.getMounth(), 1);
                mostrarEspera();
            }

            @Override
            public void onCalendarNextChanged(CalendarMounth calendarMounth) {
                HistorialActivity.this.calendarMounth = calendarMounth;
                limpiarLista();
                historialPresentador.cuantosPorAnioMes(idUsuario, calendarMounth.getYear(), calendarMounth.getMounth(), 1);
                mostrarEspera();

            }

            @Override
            public void onDayClicked(int day) {
                if (calendarMounth != null) {
                    ArrayList<ItemHistorialSolicitud> items = (ArrayList<ItemHistorialSolicitud>) ItemHistorialSolicitud.getAllDia(day, calendarMounth.getMounth(), calendarMounth.getYear(), idUsuario);
                    if (items != null) {
                        if (items.size() > 0) {
                            agregarLista(items);
                        } else {
                            limpiarLista();
                        }
                    }
                }
            }
        });
    }


    @Override
    public void respuestaCuantosPorAnioMes(final int numero, final int anio, final int mes) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (numero != 0) {
                    Log.e(TAG, "run numHis: " + numHistorialSolicitudes(anio, mes) + " Numero del servidor" + numero);
                    if (numHistorialSolicitudes(anio, mes) != numero) {
                        ItemHistorialSolicitud.deleteAllMesAnio(mes, anio, idUsuario);
                        historialPresentador.obtenerPorAnioMes(idUsuario, anio, mes, 1, 0, numero);
                    } else {
                        ocultarEspera();
                        if (ItemHistorialSolicitud.getAllMesAnio(mes, anio, idUsuario) != null) {
                            agregarLista((ArrayList<ItemHistorialSolicitud>) ItemHistorialSolicitud.getAllMesAnio(mes, anio, idUsuario));
                        } else {
                            Builder builder = new Builder(HistorialActivity.this);
                            builder.setMessage("Tuvimos un problema con su conexión a internet, por favor intente de nuevo mas tarde.")
                                    .setTitle("Información")
                                    .setCancelable(false)
                                    .setPositiveButton("Aceptar", new OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            finish();
                                        }
                                    }).show();
                        }
                    }
                } else if (numero < 0) {
                    limpiarLista();
                    ocultarEspera();
                    Builder builder = new Builder(HistorialActivity.this);
                    builder.setMessage("Tuvimos un problema con su conexión a internet, por favor intente de nuevo.")
                            .setTitle("Alerta")
                            .setPositiveButton("Reintentar", new OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    limpiarLista();
                                    historialPresentador.cuantosPorAnioMes(idUsuario, calendarMounth.getYear(), calendarMounth.getMounth(), 1);
                                    mostrarEspera();
                                }
                            })
                            .setNegativeButton("Aceptar", new OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    finish();
                                }
                            }).show();


                } else {
                    limpiarLista();
                    ocultarEspera();
                }
            }
        });

    }

    public int numHistorialSolicitudes(final int anio, final int mes) {
        Log.e(TAG, "numHistorialCompras: " + anio + " m" + mes);
        if (ItemHistorialSolicitud.getAllMesAnio(mes, anio, idUsuario) != null) {
            return ItemHistorialSolicitud.getAllMesAnio(mes, anio, idUsuario).size();
        } else {
            return 0;
        }

    }

    @Override
    public void respuestaObtenerPorAnioMes(final ArrayList<ItemHistorialSolicitud> historiales, final int anio, final int mes) {
        ocultarEspera();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (historiales != null) {
                    guardarHistorialBL(historiales, mes, anio);
                    agregarLista(historiales);
                } else {
                    if (!isFinishing()) {
                        limpiarLista();
                        AlertDialog.Builder builder = new AlertDialog.Builder(HistorialActivity.this);
                        builder.setMessage("Tuvimos un problema con su conexión a internet, por favor intente de nuevo mas tarde.")
                                .setTitle("Información")
                                .setCancelable(false)
                                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        finish();
                                    }
                                }).show();
                    }
                }
            }
        });
    }

    private void agregarLista(final ArrayList<ItemHistorialSolicitud> historiales) {
        Log.e(TAG, "agregarLista: " + historiales);
        adapter = new HistorialRecyclerAdapter(historiales);
        myRecyclerView.setAdapter(adapter);
        tvMensajeHistorial.setVisibility(View.GONE);
        myRecyclerView.setVisibility(View.VISIBLE);
        tvCantidad.setVisibility(View.VISIBLE);
        tvCantidad.setText("Total mes: " + historiales.size());
    }

    private void limpiarLista() {
        List<ItemHistorialSolicitud> list = new ArrayList<>();
        adapter = new HistorialRecyclerAdapter(list);
        myRecyclerView.setAdapter(adapter);
        myRecyclerView.setVisibility(View.GONE);
        tvCantidad.setVisibility(View.GONE);
        tvMensajeHistorial.setVisibility(View.VISIBLE);
    }


    public void guardarHistorialBL(final ArrayList<ItemHistorialSolicitud> historiales, int mes, int anio) {
        Log.e(TAG, "guardarHistorialBL: " + historiales.toString());
        if (historiales != null) {
            if (!historiales.isEmpty()) {
                for (ItemHistorialSolicitud historialSolicitud : historiales) {
                    historialSolicitud.setIdCliente(idUsuario);
                    historialSolicitud.setMes(mes);
                    historialSolicitud.setAnio(anio);
                    historialSolicitud.save();
                }
            }
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    public void mostrarEspera() {
        skProgresHistorial.setVisibility(View.VISIBLE);
        customCalendarView.setEnabled(false);
    }


    public void ocultarEspera() {
        skProgresHistorial.setVisibility(View.GONE);
        customCalendarView.setEnabled(true);
    }

    @Override
    public void totalHistoricoSolicitud(final int total) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (total >= 0) {
                    tvTitulo.setText("Total siempre: ".concat(String.valueOf(total)));
                }
            }
        });
    }

    @Override
    public void respuestaCuantosPorAnioMesCompras(int numero, int anio, int mes) {
    }


    @Override
    public void respuestaObtenerPorAnioMesCompras(ArrayList<ItemHistorialCompras> historiales, int anio, int mes) {
    }

    @Override
    public void totalHistoricoSolicitudCompras(int total) {

    }
}
