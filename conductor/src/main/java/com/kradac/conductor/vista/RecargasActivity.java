package com.kradac.conductor.vista;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.OnComunicacionRecargas;
import com.kradac.conductor.service.ServicioSockets;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecargasActivity extends AppCompatActivity implements OnComunicacionRecargas {

    @BindView(R2.id.spinner)
    Spinner spinner;
    @BindView(R2.id.appbar)
    Toolbar appbar;
    ServicioSockets servicioSocket;
    private boolean mBound;
    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ServicioSockets.LocalBinder binder = (ServicioSockets.LocalBinder) service;
            servicioSocket = binder.getService();
            servicioSocket.registerCliente(RecargasActivity.this, "RecargasActivity");
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recargas);
        ButterKnife.bind(this);
        setSupportActionBar(appbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle(getString(R.string.recargar_titulo));
            ab.setSubtitle(getString(R.string.app_name));
        }



        List<String> spinnerArray = new ArrayList<>();
        spinnerArray.add("Claro");
        spinnerArray.add("Movistar");
        spinnerArray.add("CNT");
        spinnerArray.add("Tuenti");


        ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(this,
                android.R.layout.simple_spinner_dropdown_item,
                spinnerArray);

        spinner.setAdapter(spinnerArrayAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, ServicioSockets.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        new Utilidades().hideFloatingView(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mConnection != null) {
            unbindService(mConnection);
            mConnection = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
