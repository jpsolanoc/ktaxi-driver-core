package com.kradac.conductor.vista;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v13.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.onComunicacionSugerencia;
import com.kradac.conductor.modelo.ItemHistorialSolicitud;
import com.kradac.conductor.modelo.RespuestaContactenos;
import com.kradac.conductor.presentador.SugerenciaPresentador;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SugerenciasActivity extends AppCompatActivity implements View.OnClickListener, onComunicacionSugerencia {
    @BindView(R2.id.tv_titulo_sugerencia)
    TextView tvTituloSugerencia;
    @BindView(R2.id.etMotivo)
    EditText etMotivo;
    @BindView(R2.id.appbar)
    Toolbar appbar;
    private EditText etSugerencia;
    private SharedPreferences spobtener;
    private Button btnEnviarSugerencia;
    private ProgressDialog pDialog;
    private AlertDialog confirmaSalida;
    private Utilidades utilidades;
    private ItemHistorialSolicitud itemHistorialSolicitud;
    private SugerenciaPresentador sugerenciaPresentador;

    private boolean isDesdeSolicitud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugerencias);
        ButterKnife.bind(this);
        utilidades = new Utilidades();
        setSupportActionBar(appbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle("Sugerencia.");
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isDesdeSolicitud = bundle.getBoolean("isDesdeSolicitud");
            if (isDesdeSolicitud) {
                itemHistorialSolicitud = bundle.getParcelable("solicitudSeleccionada");
                tvTituloSugerencia.setText("Déjanos tu comentario de lo sucedido en esta solicitud.");
            }
        }

        sugerenciaPresentador = new SugerenciaPresentador(this, this);
        etSugerencia = (EditText) findViewById(R.id.etComentarios);
        btnEnviarSugerencia = (Button) findViewById(R.id.btnAceptarSugerencia);
        btnEnviarSugerencia.setOnClickListener(this);
        spobtener = this.getSharedPreferences("login", Context.MODE_PRIVATE);

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btnAceptarSugerencia) {
            if (etMotivo.getText().toString().isEmpty()) {
                Toast.makeText(this, "Ingrese datos en el campo motivo para continuar.", Toast.LENGTH_SHORT).show();
                etMotivo.requestFocus();
                return;
            }
            if (etMotivo.getText().toString().length() < 4) {
                Toast.makeText(this, "Escriba un motivo de mínimo 4 caracteres.", Toast.LENGTH_SHORT).show();
                etMotivo.requestFocus();
                return;
            }
            if (isSoloNumeros(etMotivo.getText().toString())) {
                Toast.makeText(this, "El motivo debe contener letras.", Toast.LENGTH_SHORT).show();
                etMotivo.requestFocus();
                return;
            }
            String sugerencia = etSugerencia
                    .getText().toString().trim();
            if (sugerencia.isEmpty()) {
                Toast.makeText(this, "Ingrese datos en el campo mensaje para continuar.", Toast.LENGTH_SHORT).show();
                etSugerencia.requestFocus();
                return;
            }
            if (sugerencia.length() < 6) {
                Toast.makeText(this, "Escriba un mensaje de mínimo 6 caracteres.", Toast.LENGTH_SHORT).show();
                etSugerencia.requestFocus();
                return;
            }
            if (isSoloNumeros(sugerencia)) {
                Toast.makeText(this, "El mensaje debe contener letras.", Toast.LENGTH_SHORT).show();
                etSugerencia.requestFocus();
                return;
            }
            Location location = localizacionActual();
            if (isDesdeSolicitud) {
                sugerenciaPresentador.enviarContactenosV2(3, spobtener.getInt("idUsuario", 0),
                        itemHistorialSolicitud.getIdSolicitud(), spobtener.getString("nombres", " ") + " " + spobtener.getString("apellidos", " "),
                        spobtener.getString("correo", " "), spobtener.getString("celular", " "), etMotivo.getText().toString(), etSugerencia.getText().toString(),
                        location.getLatitude(), location.getLongitude(), "");
                mostrarDialogoEspera("Enviando comentario de la solicitud.");
            } else {
                sugerenciaPresentador.enviarContactenosV2(9, spobtener.getInt("idUsuario", 0),
                        0, spobtener.getString("nombres", " ") + " " + spobtener.getString("apellidos", " "),
                        spobtener.getString("correo", " "), spobtener.getString("celular", " "), etMotivo.getText().toString(), etSugerencia.getText().toString(),
                        location.getLatitude(), location.getLongitude(), "");
                mostrarDialogoEspera("Enviando Sugerencia.");
            }
            btnEnviarSugerencia.setEnabled(false);

        }
    }

    public boolean isSoloNumeros(String str) {
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    public Location localizacionActual() {
        Location retorno = null;
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        } else {
            retorno = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (retorno != null) {
                return retorno;
            } else {
                return mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void mostrarDialogoEspera(String mensaje) {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(mensaje);
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                AlertDialog.Builder localBuilder = new AlertDialog.Builder(SugerenciasActivity.this);
                localBuilder.setIcon(R.mipmap.ic_launcher);
                localBuilder.setTitle("ALERTA");
                localBuilder.setCancelable(false);
                localBuilder
                        .setMessage("¿Está seguro de cancelar el envió?");
                localBuilder.setPositiveButton("Si",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int paramAnonymous2Int) {
                                btnEnviarSugerencia.setEnabled(true);
                                dialog.dismiss();
                            }
                        });
                localBuilder.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    DialogInterface paramAnonymous2DialogInterface,
                                    int paramAnonymous2Int) {
                                paramAnonymous2DialogInterface.dismiss();
                                mostrarDialogoEspera("Enviando Sugerencia");
                            }
                        });
                confirmaSalida = localBuilder.show();
            }
        });
        if (!isFinishing()) {
            pDialog.show();
        }


    }

    @Override
    public void respuestaSugerecia(RespuestaContactenos sugerencia) {
        if (sugerencia != null) {
            if (!utilidades.isVerificarDialogo(pDialog)) {
                pDialog.dismiss();
            }
            if (sugerencia.getEstado() == 1) {
                utilidades.customToastCorto(SugerenciasActivity.this, sugerencia.getMensaje());
                etSugerencia.setText("");
                finish();
            }
            btnEnviarSugerencia.setEnabled(true);
        } else {
            utilidades.customToastCorto(SugerenciasActivity.this, getString(R.string.problemas_conexion));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }

}
