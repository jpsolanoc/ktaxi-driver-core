package com.kradac.conductor.vista;

import android.Manifest;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.AdaptadorNumerosTelefono;
import com.kradac.conductor.dialog.DialogosGenericos;
import com.kradac.conductor.extras.UrlKtaxiConductor;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionIniciarSesion;
import com.kradac.conductor.modelo.ConfiguracionServidor;
import com.kradac.conductor.modelo.DatosLogin;
import com.kradac.conductor.modelo.ItemTelefono;
import com.kradac.conductor.presentador.IniciarSesionPresentador;
import com.kradac.conductor.presentador.MainPresentador;
import com.kradac.conductor.presentador.MapaPresenter;
import com.kradac.conductor.presentador.RegistrarTokenPush;
import com.kradac.conductor.service.ServicioSockets;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class IniciarSesionBaseActivity extends BaseConexionService implements
        OnClickListener, OnComunicacionIniciarSesion {
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private static final int DELAY_SERVER = 5000;
    private static final String TAG = IniciarSesionBaseActivity.class.getName();
    @BindView(R2.id.button2)
    Button button2;
    @BindView(R2.id.appbar)
    Toolbar toolbar;
    @BindView(R2.id.txtuarioLog)
    EditText txtUsuarioemail;
    @BindView(R2.id.txtContraniaLog)
    EditText txtclavUsuario;
    @BindView(R2.id.mostrarContrasenia)
    CheckBox mostrarContrasenia;
    @BindView(R2.id.tv_estado_internet)
    EditText tvEstadoInternet;
    @BindView(R2.id.btnAcceder)
    Button btnAcceder;

    private boolean isInternet = true;
    private ProgressDialog pDialog;
    private Utilidades utilidades;
    private Dialog dialogo;
    private int contadorDeveloper = 0;
    private IniciarSesionPresentador iniciarSesionPresentador;
    private DatosLogin datosLoginSeleccionados;
    private int auxIdUsuario;
    private Dialog dialogoGps;
    private static final int MY_PERMISSIONS_REQUEST_CALL = 3;
    private int nivel = 0;

    @Override
    public void conexionCompleta() {
        super.conexionCompleta();
        registrarAcitvityServer(this, "IniciarSesionBaseActivity");
        servicioSocket.probarIniciarConexion();
        if (!servicioSocket.getmSocket().connected()) {
            servicioSocket.iniciarConexion();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_sesion);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle(R.string.str_inciar_sesion);
        }
        iniciarSesionPresentador = new IniciarSesionPresentador(this);
        mBound = false;
        txtUsuarioemail.setInputType(InputType.TYPE_CLASS_TEXT);
        utilidades = new Utilidades();
        mostrarContrasenia.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!isChecked) {
                txtclavUsuario.setTransformationMethod(PasswordTransformationMethod.getInstance());
            } else {
                txtclavUsuario.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });

        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(new View(this).getWindowToken(), 0);
        }
        Intent intent = new Intent(this, ServicioSockets.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL);
        }
    }


    public void registrarDesarrollador(int versionCode, String nameVersion) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View registrarObservacion = null;
        final Builder builder = new Builder(this);
        if (null != inflater)
            registrarObservacion = inflater.inflate(R.layout.dialog_clave_server, null, false);
        if (registrarObservacion != null) {
            final TextView tv_info_version = registrarObservacion.findViewById(R.id.tv_info_version);
            final EditText etClaveDev = registrarObservacion.findViewById(R.id.etClaveDev);
            final Button btnEntrar = registrarObservacion.findViewById(R.id.btnAceptarDev);
            final Button btnCanButton = registrarObservacion.findViewById(R.id.btnCancelarDev);
            final CheckBox chMostrarCla = registrarObservacion.findViewById(R.id.chMostrar);
            String version = getString(R.string.str_version_compilacion) + versionCode + getString(R.string.str_version) + nameVersion;
            tv_info_version.setText(version);
            builder.setCancelable(false);
            chMostrarCla.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (!isChecked) {
                    etClaveDev.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    etClaveDev.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            });

            btnEntrar.setOnClickListener(v -> {
                if (etClaveDev.getText().toString().equalsIgnoreCase(VariablesGlobales.STR_AVANZADO)) {
                    dialogo.dismiss();
                    cambiarIpServer();
                } else {
                    utilidades.customToastCorto(IniciarSesionBaseActivity.this, getString(R.string.negacion_desarrollo));
                }
            });
            btnCanButton.setOnClickListener(v -> dialogo.dismiss());
            builder.setTitle(getString(R.string.opciones_avanzadas));
            builder.setView(registrarObservacion);
            dialogo = builder.show();
        }

    }


    public void cambiarIpServer() {
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_configuracion_avanzada, null);
        Builder alertDialogBuilder = new Builder(this);
        alertDialogBuilder.setView(promptsView);
        if (promptsView != null) {
            final RadioGroup rg = promptsView.findViewById(R.id.GroupRadio);
            final CheckBox modeAvanzado = promptsView.findViewById(R.id.checkBox_opciones_avanzadaas);
            final RadioButton modeImpresion = promptsView.findViewById(R.id.check_Impresion);
            final RadioButton modeSensor = promptsView.findViewById(R.id.chb_sensor);
            final CheckBox modeTaximetro = promptsView.findViewById(R.id.check_taximetro);
            final CheckBox modebluetooth = promptsView.findViewById(R.id.chb_moduloBluetooth);
            final CheckBox modeDineroElectronico = promptsView.findViewById(R.id.check_dinero_electronico);
            final EditText etIpDineroElectronico = promptsView.findViewById(R.id.et_ip_dinero);
            int numSelecc;
            SharedPreferences ipConfig = getSharedPreferences("configIP", Context.MODE_PRIVATE);
            if (!ipConfig.getBoolean("modoAvanzado", false)) {
                modeAvanzado.setChecked(false);
            } else {
                modeAvanzado.setChecked(true);
            }
            if (!ipConfig.getBoolean("modoAvanzadoBluetooth", false)) {
                modebluetooth.setChecked(false);
                modeImpresion.setEnabled(false);
                modeSensor.setEnabled(false);
            } else {
                modebluetooth.setChecked(true);
                modeImpresion.setEnabled(true);
                modeSensor.setEnabled(true);
            }
            if (!ipConfig.getBoolean("modoAvanzadoImpresion", false)) {
                modeImpresion.setChecked(false);
            } else {
                modeImpresion.setChecked(true);
            }
            if (!ipConfig.getBoolean("modoAvanzadoSensor", false)) {
                modeSensor.setChecked(false);
            } else {
                modeSensor.setChecked(true);
            }

            if (!ipConfig.getBoolean("modoAvanzadoTaximetro", false)) {
                modeTaximetro.setChecked(false);
            } else {
                modeTaximetro.setChecked(true);
            }
            if (!ipConfig.getBoolean("modoAvanzadoDineroElectronico", false)) {
                modeDineroElectronico.setChecked(false);
            } else {
                modeDineroElectronico.setChecked(true);
            }
            SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
            if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                numSelecc = R.id.rbtProcuccion;
            } else {
                numSelecc = R.id.rbtDesarrollo;
            }
            modebluetooth.setOnCheckedChangeListener((compoundButton, b) -> {
                if (b) {
                    modeSensor.setEnabled(true);
                    modeImpresion.setEnabled(true);
                } else {
                    modeSensor.setEnabled(false);
                    modeImpresion.setEnabled(false);
                }
            });
            etIpDineroElectronico.setText(ipConfig.getString("ipDineroEletronico", "173.192.13.6:8080"));
            rg.clearCheck();
            rg.check(numSelecc);
            rg.setOnCheckedChangeListener((group, checkedId) -> {
                SharedPreferences spConfigServerNew1 = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = spConfigServerNew1.edit();
                UrlKtaxiConductor urlKtaxiConductor = new UrlKtaxiConductor();
                if (checkedId == R.id.rbtDesarrollo) {
                    editor.putBoolean(VariablesGlobales.AMBIENTE, false);
                    editor.apply();
                    VariablesGlobales.setIpServer(spConfigServerNew1.getBoolean(VariablesGlobales.AMBIENTE, true), urlKtaxiConductor.getUrl_desarrollo_https());

                } else if (checkedId == R.id.rbtProcuccion) {
                    editor.putBoolean(VariablesGlobales.AMBIENTE, true);
                    editor.apply();
                    VariablesGlobales.setIpServer(spConfigServerNew1.getBoolean(VariablesGlobales.AMBIENTE, true), urlKtaxiConductor.getUrl_produccion_https());

                }
            });
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("Guardar", (dialog, id) -> {
                SharedPreferences prefs = getSharedPreferences("configIP", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putInt("configuracionServer", 0);
                if (modeAvanzado.isChecked()) {
                    editor.putBoolean("modoAvanzado", true);
                } else {
                    editor.putBoolean("modoAvanzado", false);
                }
                if (modebluetooth.isChecked()) {
                    editor.putBoolean("modoAvanzadoBluetooth", true);
                } else {
                    editor.putBoolean("modoAvanzadoBluetooth", false);
                }
                if (modeImpresion.isChecked()) {
                    editor.putBoolean("modoAvanzadoImpresion", true);
                } else {
                    editor.putBoolean("modoAvanzadoImpresion", false);
                }
                if (modeSensor.isChecked()) {
                    editor.putBoolean("modoAvanzadoSensor", true);
                } else {
                    editor.putBoolean("modoAvanzadoSensor", false);
                }
                if (modeTaximetro.isChecked()) {
                    editor.putBoolean("modoAvanzadoTaximetro", true);
                } else {
                    editor.putBoolean("modoAvanzadoTaximetro", false);
                }
                if (modeDineroElectronico.isChecked()) {
                    editor.putBoolean("modoAvanzadoDineroElectronico", true);
                } else {
                    editor.putBoolean("modoAvanzadoDineroElectronico", false);
                }
                editor.putString("ipDineroEletronico", etIpDineroElectronico.getText().toString());
                editor.apply();
                new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(VariablesGlobales.IP_SERVICIOS_SOCKET);
            });
            alertDialogBuilder.setNegativeButton("Cancelar", (dialog, id) -> dialog.cancel());

            alertDialogBuilder.create();
            alertDialogBuilder.show();
        }
    }

    public void probarInicio() {
        SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
        if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
            btnAcceder.setBackgroundResource(R.drawable.btn_inicio_sesion_recupera_contra);
        } else {
            btnAcceder.setBackgroundResource(R.drawable.btn_inicio_sesion_recupera_contra_desarrollo);
        }
    }

    @Override
    public void configuracionServidorNew(final boolean isEstado) {
        runOnUiThread(() -> {
            servicioSocket.cambiarIpServer();
            SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
            if (isEstado) {
                if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                    utilidades.customToast(IniciarSesionBaseActivity.this, "Se cambio de servidor correctamente a producción.");
                    btnAcceder.setBackgroundResource(R.drawable.btn_inicio_sesion_recupera_contra);
                } else {
                    utilidades.customToast(IniciarSesionBaseActivity.this, "Se cambio de servidor correctamente a desarrollo.");
                    btnAcceder.setBackgroundResource(R.drawable.btn_inicio_sesion_recupera_contra_desarrollo);
                }
            } else {
                utilidades.customToast(IniciarSesionBaseActivity.this, "No se pudo cambiar de servidor intente mas tarde o nuevamente");
                SharedPreferences.Editor editor = spConfigServerNew.edit();
                editor.putBoolean(VariablesGlobales.AMBIENTE, true);
                editor.apply();
                ConfiguracionServidor configuracionServidor = ConfiguracionServidor.createConfiguracionServidor(spConfigServerNew.getString(VariablesGlobales.CONFIG_IP_NEW, VariablesGlobales.CONFIGURACION_INICIAL));
                if (configuracionServidor.getIpUsada() == null || configuracionServidor.getIpUsada().equals("")) {
                    UrlKtaxiConductor urlKtaxiConductor = new UrlKtaxiConductor();
                    VariablesGlobales.setIpServer(spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true), urlKtaxiConductor.getUrl_produccion_https());
                } else {
                    VariablesGlobales.setIpServer(spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true), configuracionServidor.getIpUsada());
                }
            }
        });
    }

    @Override
    public void respuestaPin(boolean isRespuesta, String ipRespuesta) {
        SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
        if (isRespuesta) {
            new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).obtenerConfiguracionServerNew(ipRespuesta, VariablesGlobales.NUM_ID_APLICATIVO, VariablesGlobales.ID_PLATAFORMA, VariablesGlobales.APP, utilidades.obtenerVersion(IniciarSesionBaseActivity.this));
            nivel = 0;
        } else {
            switch (nivel) {
                case 0:
                    nivel++;
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_http());
                    } else {
                        new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_http());
                    }
                    break;
                case 1:
                    nivel++;
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_ip().replace(":8080", ""));
                    } else {
                        new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_ip().replace(":8080", ""));
                    }

                    break;
                case 2:
                    nivel++;
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_ip());
                    } else {
                        new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_ip());
                    }
                    break;
                case 3:
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(null, this, null, this, null).getHelp();
                    } else {
                        new MainPresentador(null, this, null, this, null).getHelp();
                    }
                    break;
            }
        }
    }

    @Override
    public void respuestaHelp(String json) {
        if (json != null) {
            try {
                final JSONObject object = new JSONObject(json);
                if (object.has("en")) {
                    switch (object.getInt("en")) {
                        case 0:
                            nivel = 0;
                            new Handler().postDelayed(() -> {
                                SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                                if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                                    new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_https());
                                } else {
                                    new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_https());
                                }
                                try {
                                    Toast.makeText(IniciarSesionBaseActivity.this, object.getString("m"), Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }, DELAY_SERVER);
                            break;
                        case 1:
                            Builder builder = new Builder(this);
                            builder.setMessage(object.getString("m"))
                                    .setTitle("Alerta")
                                    .setPositiveButton("Actualizar", (dialog, id) -> {
                                        dialog.cancel();
                                        utilidades.calificarApp(IniciarSesionBaseActivity.this);
                                        finish();
                                    }).show();
                            break;
                        case 2:
                            new MainPresentador(null, IniciarSesionBaseActivity.this, null, this, null).getConfiuracionInicial(object.getString("url"));
                            break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                nivel = 0;
                new Handler().postDelayed(() -> {
                    SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                    if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                        new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_https());
                    } else {
                        new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_https());
                    }
                }, DELAY_SERVER);
            }
        } else {
            nivel = 0;
            new Handler().postDelayed(() -> {
                SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
                if (spConfigServerNew.getBoolean(VariablesGlobales.AMBIENTE, true)) {
                    new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_produccion_https());
                } else {
                    new MainPresentador(null, IniciarSesionBaseActivity.this, null, IniciarSesionBaseActivity.this, null).getConfiuracionInicial(getUrlKtaxiConductor().getUrl_desarrollo_https());
                }
            }, DELAY_SERVER);
        }
    }


    @OnClick({R2.id.btnAcceder, R2.id.btnolvidemicontrasenia, R2.id.button2})
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btnAcceder) {
            if (!isInternet) {
                utilidades.mostrarMensajeCorto(this, "No tiene encendido Wifi o plan de internet.");
                return;
            }
            if ((txtUsuarioemail.getText().toString().equals(""))) {
                utilidades.mostrarMensajeCorto(this, getString(R.string.ingrese_usuario));
                txtUsuarioemail.requestFocus();
            } else if (txtclavUsuario.getText().toString().equals("")) {
                utilidades.mostrarMensajeCorto(this, getString(R.string.ingrese_contrasenia));
                txtclavUsuario.requestFocus();
            } else {
                mostrarDialogo(getString(R.string.validando_user), getString(R.string.espera), true);
                if (mBound) {
                    servicioSocket.isLogin = true;
                    servicioSocket.iniciarSesion(txtUsuarioemail.getText().toString().trim(), txtclavUsuario.getText().toString().trim());
                }
//                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
//                        return;
//                    } else {
//                        //                        tomaFotoLogin();
//                    }
            }

        } else if (i == R.id.btnolvidemicontrasenia) {
            Intent registro = new Intent(IniciarSesionBaseActivity.this,
                    RecuperarContraseniaActivity.class);
            startActivity(registro);

        } else if (i == R.id.button2) {
            Intent preRegistro = new Intent(IniciarSesionBaseActivity.this, PreRegistroBaseActivity.class);
            startActivity(preRegistro);

        }


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.iniciar_sesion, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.acerca_de) {
            new DialogosGenericos().mostrarAcercaDe(this);
            return true;
        } else if (i == R.id.contactenos) {
            startActivity(new Intent(IniciarSesionBaseActivity.this, ContactenosActivity.class));
            return true;
        } else if (i == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (!spLogin.getBoolean(VariablesGlobales.LOGEADO, false)) {
            if (mBound) {
                servicioSocket.salirSistema();
            }
        }
    }

    public void mostrarDialogo(String titulo, String mensaje, boolean cancelable) {
        pDialog = new ProgressDialog(this);
        pDialog.setTitle(titulo);//"Validando Usuario"
        pDialog.setMessage(mensaje);//"Espere por favor."
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
        if (cancelable) {
            pDialog.setCancelable(true);
            pDialog.setOnCancelListener(dialog -> {
                Builder localBuilder = new Builder(IniciarSesionBaseActivity.this);
                localBuilder.setIcon(R.mipmap.ic_launcher);
                localBuilder.setTitle(R.string.str_alerta_may);
                localBuilder
                        .setMessage(getString(R.string.cancelar_proceso));
                localBuilder.setPositiveButton(getString(R.string.aceptar),
                        (dialog1, paramAnonymous2Int) -> dialog1.dismiss());
                localBuilder.setNegativeButton(getString(R.string.cancelar),
                        (paramAnonymous2DialogInterface, paramAnonymous2Int) -> paramAnonymous2DialogInterface.dismiss());
                localBuilder.show();
            });
        }
    }

    public void cerrarDialogo() {
        if (pDialog != null) {
            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }

    public void modoDesarrollo(View v) {
        contadorDeveloper++;
        if (contadorDeveloper >= 20) {
            int versionCode = utilidades.obtenerVersionCode(this);
            String versionName = utilidades.obtenerVersion(this);
            registrarDesarrollador(versionCode, versionName);
            contadorDeveloper = 0;
        }
    }

    public void dialogoCerrarSesion() {
        Builder alertDialog = new Builder(this);
        alertDialog.setTitle(getString(R.string.informacion));
        alertDialog
                .setMessage(getString(R.string.sesion_otro_dispositivo));
        alertDialog.setNegativeButton(getString(R.string.aceptar),
                (dialog, which) -> {
                    SharedPreferences spobtener = getBaseContext().getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
                    servicioSocket.desconectarUserRemoto(spobtener.getInt(VariablesGlobales.ID_VEHICULO, 0),
                            spobtener.getInt(VariablesGlobales.ID_CIUDAD, 0), spobtener.getInt(VariablesGlobales.ID_EMPRESA, 0), spobtener.getInt("idUsuario", 0), spobtener.getString("nombres", "") + " " + spobtener.getString("apellidos", ""));
                    mostrarDialogo(getString(R.string.cerrando_sesion), getString(R.string.espera), false);
                    dialog.cancel();
                });

        alertDialog.setPositiveButton(getString(R.string.cancelar),
                (dialog, which) -> dialog.cancel());
        alertDialog.show();
    }


    public void seleccionarTipoEquipo(final ArrayList<DatosLogin> datosLogins) {
        CharSequence[] array = new CharSequence[datosLogins.size()];
        int con = 0;
        for (DatosLogin s : datosLogins) {
            array[con] = s.getPlacaVehiculo() + "\n" + s.getEmpresa() + "\n" + s.getCiudad();
            con++;
        }
        Builder builder = new Builder(this);
        String titulo = getString(R.string.seleccione_unidad);
        builder.setTitle(titulo);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setCancelable(false);
        builder.setSingleChoiceItems(array, -1, (dialog, which) -> {
            int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
            servicioSocket.loguearOperadorPorIdVehiculo(datosLogins.get(selectedPosition).getIdVehiculo(),
                    datosLogins.get(selectedPosition).getIdCiudad(),
                    datosLogins.get(selectedPosition).getIdEmpresa(),
                    datosLogins.get(selectedPosition).getIdUsuario());
            datosLoginSeleccionados = datosLogins.get(selectedPosition);
            if (mBound) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(R.string.str_cancelar,
                (dialog, which) -> dialog.cancel());
        builder.show();
    }


    @Override
    public void iniciarSesionServidor(final String dato) {
        runOnUiThread(() -> {
            if (dato != null) {
                SharedPreferences.Editor localEditor = spLogin.edit();
                Intent localIntent2;
                if (!dato.equalsIgnoreCase("")) {
                    try {
                        JSONObject usuarioJSON = new JSONObject(dato);
                        Log.e("datosVehiculo", usuarioJSON.toString());
                        if (usuarioJSON.has("estado")) {
                            switch (usuarioJSON.getInt("estado")) {
                                case 0:
                                    validarImei(usuarioJSON.getString("mensaje"));
                                    auxIdUsuario = usuarioJSON.getInt("iU");
                                    break;
                                case 1:
                                    Toast.makeText(getBaseContext(), usuarioJSON.getString("mensaje"), Toast.LENGTH_LONG).show();
                                    cerrarDialogo();
                                    dialogoCerrarSesion();
                                    localEditor.putString("usuario", usuarioJSON.getJSONObject("usuario").getString("usuario"));
                                    localEditor.putString("contrasenia", txtclavUsuario.getText().toString());
                                    localEditor.putInt("idUsuario", usuarioJSON.getJSONObject("usuario").getInt("idUsuario"));
                                    localEditor.putInt("idVehiculo", usuarioJSON.getJSONObject("usuario").getInt("idVehiculo"));
                                    localEditor.putInt("idEquipo", usuarioJSON.getJSONObject("usuario").getInt("idEquipo"));
                                    localEditor.putString("{nombres", usuarioJSON.getJSONObject("usuario").getString("nombres"));
                                    localEditor.putString("apellidos", usuarioJSON.getJSONObject("usuario").getString("apellidos"));
                                    localEditor.putString("correo", usuarioJSON.getJSONObject("usuario").getString("correo"));
                                    localEditor.putString("celular", usuarioJSON.getJSONObject("usuario").getString("celular"));
                                    if (usuarioJSON.getJSONObject("usuario").has("cedula")) {
                                        localEditor.putString("cedula", usuarioJSON.getJSONObject("usuario").getString("cedula"));
                                    }
                                    localEditor.putString("empresa", usuarioJSON.getJSONObject("usuario").getString("empresa"));
                                    localEditor.putString("placaVehiculo", usuarioJSON.getJSONObject("usuario").getString("placaVehiculo"));
                                    localEditor.putString("ciudad", (usuarioJSON.getJSONObject("usuario").has("ciudad")) ? usuarioJSON.getJSONObject("usuario").getString("ciudad") : "");
                                    localEditor.putInt("tipoVehiculo", usuarioJSON.getJSONObject("usuario").getInt("tipoVehiculo"));
                                    localEditor.putString("regMunVehiculo", usuarioJSON.getJSONObject("usuario").getString("regMunVehiculo"));
                                    localEditor.putString("marcaVehiculo", usuarioJSON.getJSONObject("usuario").getString("marcaVehiculo"));
                                    localEditor.putString("modeloVehiculo", usuarioJSON.getJSONObject("usuario").getString("modeloVehiculo"));
                                    localEditor.putInt(VariablesGlobales.ID_CIUDAD, usuarioJSON.getJSONObject("usuario").getInt(VariablesGlobales.ID_CIUDAD));
                                    localEditor.putInt(VariablesGlobales.ID_EMPRESA, usuarioJSON.getJSONObject("usuario").getInt(VariablesGlobales.ID_EMPRESA));
                                    localEditor.putInt("anioVehiculo", usuarioJSON.getJSONObject("usuario").getInt("anioVehiculo"));
                                    localEditor.putInt("unidadVehiculo", usuarioJSON.getJSONObject("usuario").getInt("unidadVehiculo"));
                                    localEditor.putString("imagenConductor", usuarioJSON.getJSONObject("usuario").getString("imagenConductor"));
                                    localEditor.putBoolean("logeado", false);
                                    localEditor.apply();
                                    break;
                                case 3:
                                    new MapaPresenter(null, IniciarSesionBaseActivity.this).getConfiguracionTaximetro(this.spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0));
//                                        if (getDataTerminosCondiciones()) {
//                                            envioKeyFirebasePush();
//                                            saludoBienvenida();
//                                            finish();
//                                        } else {
//                                            terminosCondiciones();
                                    Toast.makeText(getBaseContext(), usuarioJSON.getString("mensaje"), Toast.LENGTH_LONG).show();
                                    localEditor.putBoolean("logeado", true);
                                    localEditor.commit();
                                    localIntent2 = new Intent(IniciarSesionBaseActivity.this, MapaBaseActivity.class);
                                    localIntent2.putExtra("iniciarSeccion", 1);
                                    localIntent2.setAction("android.intent.action.SEND");
                                    IniciarSesionBaseActivity.this.startActivity(localIntent2);
                                    iniciarSesionPresentador.enviarMetaDataServer(IniciarSesionBaseActivity.this, spLogin.getInt("idUsuario", 0));
                                    cerrarDialogo();
//                                        }

                                    break;
                                case 4:
                                    ArrayList<DatosLogin> listaDatosLogin = new ArrayList<>();
                                    for (int i = 0; i < usuarioJSON.getJSONArray("usuarios").length(); i++) {
                                        JSONObject usuariosDentro = usuarioJSON.getJSONArray("usuarios").getJSONObject(i);
                                        DatosLogin datosLogin = new DatosLogin(
                                                usuariosDentro.getString("usuario"),
                                                txtclavUsuario.getText().toString(),
                                                usuariosDentro.getInt("idUsuario"),
                                                usuariosDentro.getInt("idVehiculo"),
                                                usuariosDentro.getInt("idEquipo"),
                                                usuariosDentro.getString("nombres"),
                                                usuariosDentro.getString("apellidos"),
                                                usuariosDentro.getString("correo"),
                                                usuariosDentro.getString("celular"),
                                                (usuariosDentro.has("cedula")) ? usuariosDentro.getString("cedula") : "",
                                                usuariosDentro.getString("empresa"),
                                                usuariosDentro.getString("placaVehiculo"),
                                                usuariosDentro.getString("regMunVehiculo"),
                                                usuariosDentro.getString("marcaVehiculo"),
                                                usuariosDentro.getString("modeloVehiculo"),
                                                usuariosDentro.getInt(VariablesGlobales.ID_CIUDAD),
                                                (usuariosDentro.has("ciudad") ? usuariosDentro.getString("ciudad") : ""),
                                                usuariosDentro.getInt(VariablesGlobales.ID_EMPRESA),
                                                usuariosDentro.getInt("anioVehiculo"),
                                                usuariosDentro.getInt("unidadVehiculo"),
                                                usuariosDentro.getString("imagenConductor"),
                                                false);
                                        listaDatosLogin.add(datosLogin);
                                    }
                                    seleccionarTipoEquipo(listaDatosLogin);
                                    break;
                                case 5:
                                    Toast.makeText(getBaseContext(), usuarioJSON.getString("mensaje"), Toast.LENGTH_LONG).show();
                                    cerrarDialogo();
                                    dialogoCerrarSesion();
                                    localEditor.putString("usuario", datosLoginSeleccionados.getUsuario());
                                    localEditor.putString("contrasenia", datosLoginSeleccionados.getContrasenia());
                                    localEditor.putInt("idUsuario", datosLoginSeleccionados.getIdUsuario());
                                    localEditor.putInt("idVehiculo", datosLoginSeleccionados.getIdVehiculo());
                                    localEditor.putInt("idEquipo", datosLoginSeleccionados.getIdEquipo());
                                    localEditor.putString("nombres", datosLoginSeleccionados.getNombres());
                                    localEditor.putString("apellidos", datosLoginSeleccionados.getApellidos());
                                    localEditor.putString("correo", datosLoginSeleccionados.getCorreo());
                                    localEditor.putString("ciudad", datosLoginSeleccionados.getCiudad());
                                    localEditor.putString("celular", datosLoginSeleccionados.getCelular());
                                    if (datosLoginSeleccionados.getCedula() != null) {
                                        localEditor.putString("cedula", datosLoginSeleccionados.getCedula());
                                    }
                                    localEditor.putString("empresa", datosLoginSeleccionados.getEmpresa());
                                    localEditor.putString("placaVehiculo", datosLoginSeleccionados.getPlacaVehiculo());
                                    localEditor.putInt("tipoVehiculo", datosLoginSeleccionados.getTipoVehiculo());
                                    localEditor.putString("regMunVehiculo", datosLoginSeleccionados.getRegMunVehiculo());
                                    localEditor.putString("marcaVehiculo", datosLoginSeleccionados.getMarcaVehiculo());
                                    localEditor.putString("modeloVehiculo", datosLoginSeleccionados.getModeloVehiculo());
                                    localEditor.putInt(VariablesGlobales.ID_CIUDAD, datosLoginSeleccionados.getIdCiudad());
                                    localEditor.putInt(VariablesGlobales.ID_EMPRESA, datosLoginSeleccionados.getIdEmpresa());
                                    localEditor.putInt("anioVehiculo", datosLoginSeleccionados.getAnioVehiculo());
                                    localEditor.putInt("unidadVehiculo", datosLoginSeleccionados.getUnidadVehiculo());
                                    localEditor.putString("imagenConductor", datosLoginSeleccionados.getImagenConductor());
                                    localEditor.putBoolean("logeado", false);
                                    localEditor.commit();
                                    break;
                                case 7:
                                    Toast.makeText(getBaseContext(), usuarioJSON.getString("mensaje"), Toast.LENGTH_LONG).show();
                                    localEditor.putString("usuario", datosLoginSeleccionados.getUsuario());
                                    localEditor.putString("contrasenia", datosLoginSeleccionados.getContrasenia());
                                    localEditor.putInt("idUsuario", datosLoginSeleccionados.getIdUsuario());
                                    localEditor.putInt("idVehiculo", datosLoginSeleccionados.getIdVehiculo());
                                    localEditor.putInt("idEquipo", datosLoginSeleccionados.getIdEquipo());
                                    localEditor.putString("nombres", datosLoginSeleccionados.getNombres());
                                    localEditor.putString("apellidos", datosLoginSeleccionados.getApellidos());
                                    localEditor.putString("correo", datosLoginSeleccionados.getCorreo());
                                    localEditor.putString("celular", datosLoginSeleccionados.getCelular());
                                    if (datosLoginSeleccionados.getCedula() != null) {
                                        localEditor.putString("cedula", datosLoginSeleccionados.getCedula());
                                    }
                                    localEditor.putString("ciudad", datosLoginSeleccionados.getCiudad());
                                    localEditor.putString("empresa", datosLoginSeleccionados.getEmpresa());
                                    localEditor.putString("placaVehiculo", datosLoginSeleccionados.getPlacaVehiculo());
                                    localEditor.putInt("tipoVehiculo", datosLoginSeleccionados.getTipoVehiculo());
                                    localEditor.putString("regMunVehiculo", datosLoginSeleccionados.getRegMunVehiculo());
                                    localEditor.putString("marcaVehiculo", datosLoginSeleccionados.getMarcaVehiculo());
                                    localEditor.putString("modeloVehiculo", datosLoginSeleccionados.getModeloVehiculo());
                                    localEditor.putInt(VariablesGlobales.ID_CIUDAD, datosLoginSeleccionados.getIdCiudad());
                                    localEditor.putInt(VariablesGlobales.ID_EMPRESA, datosLoginSeleccionados.getIdEmpresa());
                                    localEditor.putInt("anioVehiculo", datosLoginSeleccionados.getAnioVehiculo());
                                    localEditor.putInt("unidadVehiculo", datosLoginSeleccionados.getUnidadVehiculo());
                                    localEditor.putString("imagenConductor", datosLoginSeleccionados.getImagenConductor());
                                    localEditor.putBoolean("logeado", true);
                                    localEditor.commit();
                                    iniciarSesionPresentador.enviarMetaDataServer(IniciarSesionBaseActivity.this, spLogin.getInt("idUsuario", 0));
                                    cerrarDialogo();
//                                        if (getDataTerminosCondiciones()) {
                                    envioKeyFirebasePush();
                                    saludoBienvenida();
                                    localIntent2 = new Intent(IniciarSesionBaseActivity.this, MapaBaseActivity.class);
                                    localIntent2.putExtra("iniciarSeccion", 1);
                                    localIntent2.setAction("android.intent.action.SEND");
                                    IniciarSesionBaseActivity.this.startActivity(localIntent2);
                                    finish();
//                                        } else {
//                                            terminosCondiciones();
//                                        }
                                    new MapaPresenter(null, IniciarSesionBaseActivity.this).getConfiguracionTaximetro(this.spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0));
                                    break;
                                case 6:
                                    Toast.makeText(getBaseContext(), usuarioJSON.getString("mensaje"), Toast.LENGTH_LONG).show();
                                    localEditor.putString("usuario", usuarioJSON.getJSONObject("usuario").getString("usuario"));
                                    localEditor.putString("contrasenia", txtclavUsuario.getText().toString());
                                    localEditor.putInt("idUsuario", usuarioJSON.getJSONObject("usuario").getInt("idUsuario"));
                                    localEditor.putInt("idVehiculo", usuarioJSON.getJSONObject("usuario").getInt("idVehiculo"));
                                    localEditor.putInt("idEquipo", usuarioJSON.getJSONObject("usuario").getInt("idEquipo"));
                                    localEditor.putString("nombres", usuarioJSON.getJSONObject("usuario").getString("nombres"));
                                    localEditor.putString("apellidos", usuarioJSON.getJSONObject("usuario").getString("apellidos"));
                                    localEditor.putString("correo", usuarioJSON.getJSONObject("usuario").getString("correo"));
                                    localEditor.putString("celular", usuarioJSON.getJSONObject("usuario").getString("celular"));
                                    localEditor.putString("ciudad", (usuarioJSON.getJSONObject("usuario").has("ciudad")) ? usuarioJSON.getJSONObject("usuario").getString("ciudad") : "");
                                    if (usuarioJSON.getJSONObject("usuario").has("cedula")) {
                                        localEditor.putString("cedula", usuarioJSON.getJSONObject("usuario").getString("cedula"));
                                    }
                                    localEditor.putString("empresa", usuarioJSON.getJSONObject("usuario").getString("empresa"));
                                    localEditor.putString("placaVehiculo", usuarioJSON.getJSONObject("usuario").getString("placaVehiculo"));
                                    localEditor.putInt("tipoVehiculo", usuarioJSON.getJSONObject("usuario").getInt("tipoVehiculo"));
                                    localEditor.putString("regMunVehiculo", usuarioJSON.getJSONObject("usuario").getString("regMunVehiculo"));
                                    localEditor.putString("marcaVehiculo", usuarioJSON.getJSONObject("usuario").getString("marcaVehiculo"));
                                    localEditor.putString("modeloVehiculo", usuarioJSON.getJSONObject("usuario").getString("modeloVehiculo"));
                                    localEditor.putInt(VariablesGlobales.ID_CIUDAD, usuarioJSON.getJSONObject("usuario").getInt(VariablesGlobales.ID_CIUDAD));
                                    localEditor.putInt(VariablesGlobales.ID_EMPRESA, usuarioJSON.getJSONObject("usuario").getInt(VariablesGlobales.ID_EMPRESA));
                                    localEditor.putInt("anioVehiculo", usuarioJSON.getJSONObject("usuario").getInt("anioVehiculo"));
                                    localEditor.putInt("unidadVehiculo", usuarioJSON.getJSONObject("usuario").getInt("unidadVehiculo"));
                                    localEditor.putString("imagenConductor", usuarioJSON.getJSONObject("usuario").getString("imagenConductor"));
                                    localEditor.putBoolean("logeado", true);
                                    localEditor.commit();
                                    if (!usuarioJSON.getJSONObject("usuario").getString(VariablesGlobales.ID_CIUDAD).equalsIgnoreCase("") &&
                                            !usuarioJSON.getJSONObject("usuario").getString(VariablesGlobales.ID_EMPRESA).equalsIgnoreCase("") && usuarioJSON.getJSONObject("usuario").getInt("idVehiculo") != 0) {
                                        cerrarDialogo();
                                        new MapaPresenter(null, IniciarSesionBaseActivity.this).getConfiguracionTaximetro(this.spLogin.getInt(VariablesGlobales.ID_CIUDAD, 0));
                                        iniciarSesionPresentador.enviarMetaDataServer(IniciarSesionBaseActivity.this, spLogin.getInt("idUsuario", 0));
//                                            if (getDataTerminosCondiciones()) {
                                        localIntent2 = new Intent(IniciarSesionBaseActivity.this, MapaBaseActivity.class);
                                        localIntent2.putExtra("iniciarSeccion", 1);
                                        localIntent2.setAction("android.intent.action.SEND");
                                        IniciarSesionBaseActivity.this.startActivity(localIntent2);
                                        envioKeyFirebasePush();
                                        saludoBienvenida();
                                        finish();
//                                            } else {
//                                                terminosCondiciones();
//                                            }
                                    } else {
                                        Toast.makeText(getBaseContext(), getString(R.string.datos_vacios), Toast.LENGTH_LONG).show();
                                    }
                                    break;
                                case -1:
                                    dialogoMensajeServer(usuarioJSON.getString("mensaje"));
                                    break;
                                case -2:
                                    dialogoMensajeServer(usuarioJSON.getString("mensaje"));
                                    break;
                                case -7:
                                    respuestaRegistrarImei(usuarioJSON.toString());
                                    break;
                                default:
                                    dialogoMensajeServer("ERROR");
                                    break;
                            }
                        } else {
                            dialogoMensajeServer(usuarioJSON.getString("mensaje"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                cerrarDialogo();
            }
        });
    }

    public Dialog dialogoRegistroImei;

    @Override
    public void respuestaRegistrarImei(String dato) {
        if (dato != null) {
            try {
                if (dialogoRegistroImei != null) {
                    if (dialogoRegistroImei.isShowing()) {
                        return;
                    }
                }
                ArrayList<ItemTelefono> itemTelefonos = new ArrayList<>();
                JSONObject object = new JSONObject(dato);
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View viewInformacion = inflater.inflate(R.layout.dialog_registro_dispositivo, null, false);
                final TextView tvTitulo = viewInformacion.findViewById(R.id.tv_titulo);
                tvTitulo.setText(R.string.informacion);
                final TextView tvMensaje = viewInformacion.findViewById(R.id.tv_mensaje);
                final ListView listLlamar = viewInformacion.findViewById(R.id.list_telefonos);
                if (object.has("m")) {
                    tvMensaje.setText(object.getString("m"));
                } else if (object.has("mensaje")) {
                    tvMensaje.setText(object.getString("mensaje"));
                }
                if (object.has("lT")) {
                    JSONArray array = object.getJSONArray("lT");
                    if (array.length() > 0) {
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject dat = (JSONObject) array.get(i);
                            ItemTelefono itemTelefono = new ItemTelefono(dat.getString("n"), dat.getString("t"));
                            itemTelefonos.add(itemTelefono);
                        }
                    } else {
                        listLlamar.setVisibility(View.GONE);
                    }
                }

                final Builder builder = new Builder(this);
                builder.setView(viewInformacion);
                builder.setCancelable(false);
                dialogoRegistroImei = builder.show();
                final Button btnSalir = viewInformacion.findViewById(R.id.btnAceptarDev);
                btnSalir.setOnClickListener(v -> {
                    if (dialogoRegistroImei != null) {
                        if (dialogoRegistroImei.isShowing()) {
                            dialogoRegistroImei.dismiss();
                        }
                    }
                });
                AdaptadorNumerosTelefono adaptadorNumerosTelefono = new AdaptadorNumerosTelefono(this, itemTelefonos, IniciarSesionBaseActivity.this);
                listLlamar.setAdapter(adaptadorNumerosTelefono);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void clickItemLlamar() {
        if (dialogoRegistroImei != null) {
            if (dialogoRegistroImei.isShowing()) {
                dialogoRegistroImei.dismiss();
            }
        }
    }

    @Override
    public void onGpsDisableNewApi(final Status status) {
        runOnUiThread(() -> {
            try {
                status.startResolutionForResult(IniciarSesionBaseActivity.this, 0x1);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onGpsEnable() {
        runOnUiThread(() -> {
            if (utilidades.isVerificarDialogo(dialogoGps)) {
                Builder alertDialog = new Builder(IniciarSesionBaseActivity.this);
                alertDialog.setTitle(getString(R.string.servicio_hubicaion_desabilitado));
                alertDialog.setCancelable(false);
                alertDialog
                        .setMessage(getString(R.string.mensaje_hubicaion_desabilitado));
                alertDialog.setPositiveButton(R.string.str_ajustes,
                        (dialog, which) -> {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                            dialog.dismiss();

                        });
                alertDialog.setNegativeButton(R.string.salir, (dialog, which) -> finish());
                dialogoGps = alertDialog.show();
            }
        });
    }

    @Override
    public void cambioGpsNew(final Status status) {
        runOnUiThread(() -> {
            try {
                if (status != null) {
                    if (utilidades.isVerificarDialogo(dialogoGps)) {
                        status.startResolutionForResult(IniciarSesionBaseActivity.this, 0x1);
                    }
                } else {
                    if (!isFinishing()) {
                        if (!utilidades.isVerificarDialogo(dialogoGps)) {
                            Builder alertDialog = new Builder(IniciarSesionBaseActivity.this);
                            alertDialog.setTitle(getString(R.string.servicio_hubicaion_desabilitado));
                            alertDialog.setCancelable(false);
                            alertDialog
                                    .setMessage(getString(R.string.mensaje_hubicaion_desabilitado));
                            alertDialog.setPositiveButton(R.string.str_ajustes,
                                    (dialog, which) -> {
                                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                        startActivity(intent);
                                        dialog.dismiss();
                                    });
                            alertDialog.setNegativeButton(R.string.salir, (dialog, which) -> dialog.dismiss());
                            dialogoGps = alertDialog.show();
                        }
                    }
                }
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void cambioGps() {
        runOnUiThread(() -> {
            if (utilidades.isVerificarDialogo(dialogoGps)) {
                Builder alertDialog = new Builder(IniciarSesionBaseActivity.this);
                alertDialog.setTitle(getString(R.string.servicio_hubicaion_desabilitado));
                alertDialog.setCancelable(false);
                alertDialog
                        .setMessage(getString(R.string.mensaje_hubicaion_desabilitado));
                alertDialog.setPositiveButton(R.string.str_ajustes,
                        (dialog, which) -> {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                            dialog.dismiss();

                        });
                alertDialog.setNegativeButton(R.string.salir, (dialog, which) -> finish());
                dialogoGps = alertDialog.show();
            }
        });
    }


    public void dialogoMensajeServer(String dato) {
        Builder dialogo = new Builder(IniciarSesionBaseActivity.this);
        dialogo.setTitle(R.string.str_alerta_min);
        dialogo.setMessage(dato);
        dialogo.setCancelable(false);
        dialogo.setPositiveButton(R.string.aceptar, (dialogo1, id) -> dialogo1.dismiss());
        if (!isFinishing()) {
            dialogo.show();
        }
    }

    public void validarImei(String mensajeServer) {
        final Builder dialogo = new Builder(this);
        dialogo.setTitle(R.string.str_alerta_min);
        dialogo.setMessage(mensajeServer);
        dialogo.setCancelable(false);
        dialogo.setPositiveButton(R.string.str_si, (dialogo1, id) -> {
            //codigo
            Location locationEs;
            LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(IniciarSesionBaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(IniciarSesionBaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            } else {
                assert mlocManager != null;
                locationEs = mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
            if (locationEs != null) {
                iniciarSesionPresentador.enviarValidarImei(IniciarSesionBaseActivity.this, auxIdUsuario, locationEs.getLatitude(), locationEs.getLongitude());
            } else {
                iniciarSesionPresentador.enviarValidarImei(IniciarSesionBaseActivity.this, auxIdUsuario, 0, 0);
            }

        });
        dialogo.setNegativeButton(R.string.str_no, (dialogo1, id) -> {
            //codigo
            dialogo1.dismiss();
        });
        if (!isFinishing()) {
            dialogo.show();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    utilidades.customToast(this, getString(R.string.str_ahora_puedes_llamar));
                } else {
                    utilidades.customToast(this, getString(R.string.str_noactiva_permisos_llamada));
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    tomaFotoLogin();
                    mostrarDialogo(getString(R.string.validando_user), getString(R.string.espera), true);
                    if (mBound) {
                        servicioSocket.isLogin = true;
                        servicioSocket.iniciarSesion(txtUsuarioemail.getText().toString().trim(), txtclavUsuario.getText().toString().trim());
                    }
                } else {
                    utilidades.customToast(this, getString(R.string.msj_permiso_camara));
                }
                return;
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!spLogin.getBoolean(VariablesGlobales.LOGEADO, false)) {
            stopService(new Intent(this, ServicioSockets.class));
        }
        if (mConnection != null) {
            try {
                unbindService(mConnection);
            } catch (IllegalArgumentException ignored) {
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        registrarAcitvityServer(this, "IniciarSesionBaseActivity");
        probarInicio();
        new Utilidades().hideFloatingView(this);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void cambioInterent(boolean isIntenet) {
        this.isInternet = isIntenet;
        if (!isIntenet) {
            tvEstadoInternet.setVisibility(View.VISIBLE);
            tvEstadoInternet.setEnabled(false);
        } else {
            tvEstadoInternet.setVisibility(View.GONE);
        }
        super.cambioInterent(isIntenet);
    }

    private Dialog dialogOtraApp;

    @Override
    public void otraApp(final String mensaje, final String paquete, final boolean isDesinstalar) {
        runOnUiThread(() -> {
            if (dialogOtraApp != null) {
                if (dialogOtraApp.isShowing()) {
                    return;
                }
            }
            Builder builder = new Builder(IniciarSesionBaseActivity.this);
            builder.setMessage(mensaje)
                    .setTitle(R.string.str_alerta_min)
                    .setPositiveButton(R.string.aceptar, (dialog, id) -> dialog.cancel());
            if (isDesinstalar) {
                builder.setNegativeButton(R.string.str_desinstalar, (dialog, id) -> {
                    dialog.cancel();
                    utilidades.startInstalledAppDetailsActivity(IniciarSesionBaseActivity.this, paquete);
                });
                dialogOtraApp = builder.create();
                dialogOtraApp.show();
            } else {
                if (utilidades.verificarEjecucionApp(paquete, IniciarSesionBaseActivity.this)) {
                    builder.setNegativeButton(R.string.str_forzar_cierre, (dialog, id) -> {
                        dialog.cancel();
                        utilidades.startInstalledAppDetailsActivity(IniciarSesionBaseActivity.this, paquete);
                    });
                    dialogOtraApp = builder.create();
                    dialogOtraApp.show();
                }
            }
        });
    }

    public void envioKeyFirebasePush() {
        SharedPreferences spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        int idUsuario = spLogin.getInt(VariablesGlobales.ID_USUARIO, 0);
        boolean isLogin = spLogin.getBoolean("logeado", false);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "envioKeyFirebasePush: " + refreshedToken);
        if (refreshedToken != null && isLogin) {
            new RegistrarTokenPush(this).registrarToken(idUsuario, refreshedToken, true);
        }
    }

    public void saludoBienvenida() {
        SharedPreferences spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        String nombre = spLogin.getString(VariablesGlobales.NOMBRES, "");
        if (mBound) {
            servicioSocket.reproducirTextAudio.speak("Bienvenido, " + nombre + " esperamos que tengas una excelente jornada laboral.");
        }
    }

    public void terminosCondiciones() {
        new DialogosGenericos().terminosCondiciones(this, isAcepto -> {
            if (isAcepto) {
                envioKeyFirebasePush();
                saludoBienvenida();
                finish();
                setDataTerminosCondiciones(true);
                startActivity(new Intent(IniciarSesionBaseActivity.this, MapaBaseActivity.class));
            } else {
                finish();
            }
        });
    }


    public void setDataTerminosCondiciones(boolean isTerminoCondiciones) {
        SharedPreferences spTerminosCondiciones = getSharedPreferences("terminos_condiciones", MODE_PRIVATE);
        SharedPreferences.Editor editor = spTerminosCondiciones.edit();
        editor.putBoolean("isTerminosCondiciones", isTerminoCondiciones);
        editor.apply();
    }

    public boolean getDataTerminosCondiciones() {
        SharedPreferences spTerminosCondiciones = getSharedPreferences("terminos_condiciones", MODE_PRIVATE);
        return spTerminosCondiciones.getBoolean("isTerminosCondiciones", false);
    }
}