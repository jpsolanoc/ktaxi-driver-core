package com.kradac.conductor.vista;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.service.ServicioSockets;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConfiguracionActivity extends AppCompatActivity {

    @BindView(R2.id.rg_mapa)
    RadioGroup rgMapa;
    @BindView(R2.id.appbar)
    Toolbar appbar;
    @BindView(R2.id.chk_solicitudes_normales)
    CheckBox chkSolicitudesNormales;
    @BindView(R2.id.chk_solicitudes_pedidos)
    CheckBox chkSolicitudesPedidos;
    @BindView(R2.id.chk_solicitudes_quiosco)
    CheckBox chkSolicitudesQuiosco;
    @BindView(R2.id.rb_mapa_osm)
    RadioButton rbMapaOsm;
    @BindView(R2.id.rb_mapa_mapbox)
    RadioButton rbMapaMapbox;
    @BindView(R2.id.rb_voz_activa)
    RadioButton rbVozActiva;
    @BindView(R2.id.rb_voz_desactiva)
    RadioButton rbVozDesactiva;
    @BindView(R2.id.rg_voz)
    RadioGroup rgVoz;
    @BindView(R2.id.rb_pantalla_activa)
    RadioButton rbPantallaActiva;
    @BindView(R2.id.rb_pantalla_desactiva)
    RadioButton rbPantallaDesactiva;
    @BindView(R2.id.rg_pantalla)
    RadioGroup rgPantalla;
    @BindView(R2.id.rb_orientacion_positiva)
    RadioButton rbOrientacionPositiva;
    @BindView(R2.id.rb_orientacion_negativa)
    RadioButton rbOrientacionNegativa;
    @BindView(R2.id.rg_orientacion)
    RadioGroup rgOrientacion;
    @BindView(R2.id.rb_cfg_mapa_normal)
    RadioButton rbCfgMapaNormal;
    @BindView(R2.id.rb_mapa_automatica)
    RadioButton rbMapaAutomatica;
    @BindView(R2.id.rg_configuracion_mapa)
    RadioGroup rgConfiguracionMapa;
    @BindView(R2.id.rb_automatico)
    RadioButton rbAutomatico;
    @BindView(R2.id.rb_no_reproducir)
    RadioButton rbNoReproducir;
    @BindView(R2.id.rg_audio_automatico)
    RadioGroup rgAudioAutomatico;
    @BindView(R2.id.rb_taximetro_activo)
    RadioButton rbTaximetroActivo;
    @BindView(R2.id.rb_taximetro_desactivo)
    RadioButton rbTaximetroDesactivo;
    @BindView(R2.id.rg_mostrar_taximetro)
    RadioGroup rgMostrarTaximetro;
    @BindView(R2.id.rb_mapa_nocturnos)
    RadioButton rbMapaNocturnos;
    @BindView(R2.id.rb_mostrar_boton)
    RadioButton rbMostrarBoton;
    @BindView(R2.id.rb_ocultar_boton)
    RadioButton rbOcultarBoton;
    @BindView(R2.id.rg_boton_flotante)
    RadioGroup rgBotonFlotante;
    @BindView(R2.id.rb_silenciar_llamada)
    RadioButton rbSilenciarLlamada;
    @BindView(R2.id.rb_no_silenciar_llamada)
    RadioButton rbNoSilenciarLlamada;
    @BindView(R2.id.rg_silenciar_en_llamada)
    RadioGroup rgSilenciarEnLlamada;
    @BindView(R2.id.rb_push_activar)
    RadioButton rbPushActivar;
    @BindView(R2.id.rb_push_desactivar)
    RadioButton rbPushDesactivar;
    @BindView(R2.id.rg_push_defecto)
    RadioGroup rgPushDefecto;
    @BindView(R2.id.ly_push)
    LinearLayout lyPush;
    @BindView(R2.id.rg_mapa_google)
    RadioButton rgMapaGoogle;


    private SharedPreferences configuracionApp;
    private SharedPreferences.Editor editorConfiguracion;
    private Utilidades utilidades;
    private boolean mBound;
    private ServicioSockets servicioSocket;
    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ServicioSockets.LocalBinder binder = (ServicioSockets.LocalBinder) service;
            servicioSocket = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, ServicioSockets.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        new Utilidades().hideFloatingView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mConnection != null) {
            unbindService(mConnection);
        }
    }

    private SharedPreferences spLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion_app);
        ButterKnife.bind(this);
        spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        utilidades = new Utilidades();
        assert appbar != null;
        setSupportActionBar(appbar);
        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle(getResources().getString(R.string.actividad_configuracion));
        }
        configuracionApp = getSharedPreferences(VariablesGlobales.CONFIGURACION_APP, Context.MODE_PRIVATE);
        editorConfiguracion = configuracionApp.edit();
        determinarInicio();
        rgMapa.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_mapa_mapbox) {
                    editorConfiguracion.putInt(VariablesGlobales.TIPO_MAPA, 2);
                    editorConfiguracion.apply();
                    setUltimoUsado(2,ConfiguracionActivity.this);
                    if (mBound) {
                        servicioSocket.enviarEvento(1, 1);
                    }

                } else if (checkedId == R.id.rb_mapa_osm) {
                    editorConfiguracion.putInt(VariablesGlobales.TIPO_MAPA, 3);
                    editorConfiguracion.apply();
                    setUltimoUsado(3,ConfiguracionActivity.this);
                    if (mBound) {
                        servicioSocket.enviarEvento(1, 2);
                    }

                } else if (checkedId == R.id.rg_mapa_google) {
                    editorConfiguracion.putInt(VariablesGlobales.TIPO_MAPA, 1);
                    editorConfiguracion.apply();
                    setUltimoUsado(1,ConfiguracionActivity.this);
                    if (mBound) {
                        servicioSocket.enviarEvento(1, 3);
                    }

                }
            }
        });

        rgConfiguracionMapa.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_cfg_mapa_normal) {
                    editorConfiguracion.putInt(VariablesGlobales.CONFIGURACION_ESTILO_MAPA, 1);
                    editorConfiguracion.apply();

                } else if (checkedId == R.id.rb_mapa_nocturnos) {
                    editorConfiguracion.putInt(VariablesGlobales.CONFIGURACION_ESTILO_MAPA, 2);
                    editorConfiguracion.apply();

                } else if (checkedId == R.id.rb_mapa_automatica) {
                    editorConfiguracion.putInt(VariablesGlobales.CONFIGURACION_ESTILO_MAPA, 3);
                    editorConfiguracion.apply();

                }
            }
        });

        rgVoz.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_voz_activa) {
                    editorConfiguracion.putBoolean(VariablesGlobales.ASISTENTE_VOZ, true);
                    editorConfiguracion.apply();

                } else if (checkedId == R.id.rb_voz_desactiva) {
                    editorConfiguracion.putBoolean(VariablesGlobales.ASISTENTE_VOZ, false);
                    editorConfiguracion.apply();

                }
            }
        });

        rgPantalla.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_pantalla_activa) {
                    editorConfiguracion.putBoolean(VariablesGlobales.CONFIGURACION_PANTALLA, true);
                    editorConfiguracion.apply();

                } else if (checkedId == R.id.rb_pantalla_desactiva) {
                    editorConfiguracion.putBoolean(VariablesGlobales.CONFIGURACION_PANTALLA, false);
                    editorConfiguracion.apply();

                }
            }
        });


        rgOrientacion.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_orientacion_negativa) {
                    editorConfiguracion.putBoolean(VariablesGlobales.CONFIGURACION_ORIENTACION, true);
                    editorConfiguracion.apply();

                } else if (checkedId == R.id.rb_orientacion_positiva) {
                    editorConfiguracion.putBoolean(VariablesGlobales.CONFIGURACION_ORIENTACION, false);
                    editorConfiguracion.apply();

                }
            }
        });

        rgBotonFlotante.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_mostrar_boton) {
                    editorConfiguracion.putBoolean(VariablesGlobales.CONFIGURACION_BOTON_FLOTANTE, true);
                    editorConfiguracion.apply();

                } else if (checkedId == R.id.rb_ocultar_boton) {
                    editorConfiguracion.putBoolean(VariablesGlobales.CONFIGURACION_BOTON_FLOTANTE, false);
                    editorConfiguracion.apply();

                }
            }
        });

        rgAudioAutomatico.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_automatico) {
                    editorConfiguracion.putBoolean(VariablesGlobales.CONFIGURACION_CHAT_AUDIOS, true);
                    editorConfiguracion.apply();

                } else if (checkedId == R.id.rb_no_reproducir) {
                    editorConfiguracion.putBoolean(VariablesGlobales.CONFIGURACION_CHAT_AUDIOS, false);
                    editorConfiguracion.apply();

                }
            }
        });


        rgMostrarTaximetro.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_taximetro_activo) {
                    editorConfiguracion.putBoolean(VariablesGlobales.CONFIGURACION_TAXIMETRO, true);
                    editorConfiguracion.apply();

                } else if (checkedId == R.id.rb_taximetro_desactivo) {
                    editorConfiguracion.putBoolean(VariablesGlobales.CONFIGURACION_TAXIMETRO, false);
                    editorConfiguracion.apply();

                }
            }
        });

        rgSilenciarEnLlamada.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_silenciar_llamada) {
                    editorConfiguracion.putBoolean(VariablesGlobales.EN_LLAMADA, true);
                    editorConfiguracion.apply();

                } else if (checkedId == R.id.rb_no_silenciar_llamada) {
                    editorConfiguracion.putBoolean(VariablesGlobales.EN_LLAMADA, false);
                    editorConfiguracion.apply();
                }
            }
        });

        chkSolicitudesNormales.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                editorConfiguracion.putBoolean(VariablesGlobales.SOLICITUDES_NORMALES, b);
                editorConfiguracion.apply();
            }
        });
        chkSolicitudesPedidos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                editorConfiguracion.putBoolean(VariablesGlobales.SOLICITUDES_PEDIDOS, b);
                editorConfiguracion.apply();
            }
        });

        chkSolicitudesQuiosco.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                editorConfiguracion.putBoolean(VariablesGlobales.SOLICITUDES_QUIOSCO, b);
                editorConfiguracion.apply();
            }
        });
        chkSolicitudesNormales.setEnabled(false);

        int tipoV = spLogin.getInt("tipoVehiculo", 0);
        if (tipoV == 5 || tipoV == 4) {
            lyPush.setVisibility(View.VISIBLE);
        } else {
            lyPush.setVisibility(View.GONE);
        }

        rgPushDefecto.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_push_activar) {
                    editorConfiguracion.putBoolean(VariablesGlobales.OPCION_PUSH, true);
                    editorConfiguracion.apply();

                } else if (checkedId == R.id.rb_push_desactivar) {
                    editorConfiguracion.putBoolean(VariablesGlobales.OPCION_PUSH, false);
                    editorConfiguracion.apply();

                }
            }
        });


    }

    public void determinarInicio() {
        switch (configuracionApp.getInt(VariablesGlobales.TIPO_MAPA, configuracionMapaInicial())) {
            case 1:
                rbMapaMapbox.setChecked(false);
                rbMapaOsm.setChecked(false);
                rgMapaGoogle.setChecked(true);
                break;
            case 2:
                rbMapaMapbox.setChecked(true);
                rbMapaOsm.setChecked(false);
                rgMapaGoogle.setChecked(false);
                break;
            case 3:
                rbMapaMapbox.setChecked(false);
                rbMapaOsm.setChecked(true);
                rgMapaGoogle.setChecked(false);
                break;
        }
        if (configuracionApp.getBoolean(VariablesGlobales.OPCION_PUSH, true)) {
            rbPushActivar.setChecked(true);
            rbPushDesactivar.setChecked(false);
        } else {
            rbPushDesactivar.setChecked(true);
            rbPushActivar.setChecked(false);
        }
        if (configuracionApp.getBoolean(VariablesGlobales.EN_LLAMADA, true)) {
            rbSilenciarLlamada.setChecked(true);
            rbNoSilenciarLlamada.setChecked(false);
        } else {
            rbNoSilenciarLlamada.setChecked(true);
            rbSilenciarLlamada.setChecked(false);
        }

        if (configuracionApp.getBoolean(VariablesGlobales.SOLICITUDES_NORMALES, true)) {
            chkSolicitudesNormales.setChecked(true);
        } else {
            chkSolicitudesNormales.setChecked(false);
        }
        if (configuracionApp.getBoolean(VariablesGlobales.SOLICITUDES_PEDIDOS, true)) {
            chkSolicitudesPedidos.setChecked(true);
        } else {
            chkSolicitudesPedidos.setChecked(false);
        }
        if (configuracionApp.getBoolean(VariablesGlobales.SOLICITUDES_QUIOSCO, true)) {
            chkSolicitudesQuiosco.setChecked(true);
        } else {
            chkSolicitudesQuiosco.setChecked(false);
        }
        if (configuracionApp.getBoolean(VariablesGlobales.ASISTENTE_VOZ, true)) {
            rbVozActiva.setChecked(true);
            rbVozDesactiva.setChecked(false);
        } else {
            rbVozDesactiva.setChecked(true);
            rbVozActiva.setChecked(false);
        }
        if (configuracionApp.getBoolean(VariablesGlobales.CONFIGURACION_PANTALLA, true)) {
            rbPantallaActiva.setChecked(true);
            rbPantallaDesactiva.setChecked(false);
        } else {
            rbPantallaDesactiva.setChecked(true);
            rbPantallaActiva.setChecked(false);
        }
        if (configuracionApp.getBoolean(VariablesGlobales.CONFIGURACION_ORIENTACION, true)) {
            rbOrientacionNegativa.setChecked(true);
            rbOrientacionPositiva.setChecked(false);
        } else {
            rbOrientacionNegativa.setChecked(false);
            rbOrientacionPositiva.setChecked(true);
        }

        if (configuracionApp.getBoolean(VariablesGlobales.CONFIGURACION_BOTON_FLOTANTE, true)) {
            rbMostrarBoton.setChecked(true);
            rbOcultarBoton.setChecked(false);
        } else {
            rbMostrarBoton.setChecked(false);
            rbOcultarBoton.setChecked(true);
        }


        if (configuracionApp.getBoolean(VariablesGlobales.CONFIGURACION_CHAT_AUDIOS, true)) {
            rbAutomatico.setChecked(true);
            rbNoReproducir.setChecked(false);
        } else {
            rbAutomatico.setChecked(false);
            rbNoReproducir.setChecked(true);
        }

        if (configuracionApp.getBoolean(VariablesGlobales.CONFIGURACION_TAXIMETRO, true)) {
            rbTaximetroActivo.setChecked(true);
            rbTaximetroDesactivo.setChecked(false);
        } else {
            rbTaximetroActivo.setChecked(false);
            rbTaximetroDesactivo.setChecked(true);
        }

        switch (configuracionApp.getInt(VariablesGlobales.CONFIGURACION_ESTILO_MAPA, 3)) {
            case 1:
                rbCfgMapaNormal.setChecked(true);
                rbMapaAutomatica.setChecked(false);
                rbMapaNocturnos.setChecked(false);
                break;
            case 2:
                rbCfgMapaNormal.setChecked(false);
                rbMapaAutomatica.setChecked(false);
                rbMapaNocturnos.setChecked(true);
                break;
            case 3:
                rbCfgMapaNormal.setChecked(false);
                rbMapaAutomatica.setChecked(true);
                rbMapaNocturnos.setChecked(false);
                break;
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onDestroy() {
        isFinActividad();
        super.onDestroy();
    }

    public boolean isFinActividad() {
        if (!chkSolicitudesPedidos.isChecked() && !chkSolicitudesQuiosco.isChecked() && !chkSolicitudesNormales.isChecked()) {
            chkSolicitudesNormales.setChecked(true);
            editorConfiguracion.putBoolean(VariablesGlobales.SOLICITUDES_NORMALES, true);
            editorConfiguracion.commit();
            return false;
        } else {
            finish();
            return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isFinActividad()) {
                    utilidades.customToastCorto(this, "Debe seleccionar algún item de tipo de solicitud");
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!isFinActividad()) {
            utilidades.customToastCorto(this, "Debe seleccionar algún item de tipo de solicitud");
        }
        return super.onKeyDown(keyCode, event);
    }

    public void setUltimoUsado(int ultimoUsado,Context context) {
        configuracionApp = context.getSharedPreferences(VariablesGlobales.CONFIGURACION_APP, Context.MODE_PRIVATE);
        editorConfiguracion = configuracionApp.edit();
        editorConfiguracion.putInt(VariablesGlobales.TIPO_MAPA_ULTIMO_USADO, ultimoUsado);
        editorConfiguracion.apply();
    }

    public int getUltimoUsado(Context context) {
        configuracionApp = context.getSharedPreferences(VariablesGlobales.CONFIGURACION_APP, Context.MODE_PRIVATE);
        return configuracionApp.getInt(VariablesGlobales.TIPO_MAPA_ULTIMO_USADO, 1);
    }

    private SharedPreferences spParametrosConfiguracion;

    public int configuracionMapaInicial() {
        spParametrosConfiguracion = getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 40://Para configurar el incio del mapa
                                if (objetoConfig.getInt("h") == 1) { //Habilitado
                                    return Integer.parseInt(objetoConfig.getString("vd"));
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.getMessage();
                return 1;
            }
        }
        return 1;
    }

}

