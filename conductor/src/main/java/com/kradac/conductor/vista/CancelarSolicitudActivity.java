package com.kradac.conductor.vista;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionCancelar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CancelarSolicitudActivity extends BaseConexionService implements OnComunicacionCancelar {

    @BindView(R2.id.appbar)
    Toolbar appbar;
    @BindView(R2.id.tv_comentario)
    EditText tvComentario;
    @BindView(R2.id.btn_cancelar)
    Button btnCancelar;
    @BindView(R2.id.btn_aceptar)
    Button btnAceptar;
    @BindView(R2.id.rB_eventualidad_seis)
    RadioButton rBEventualidadSeis;
    @BindView(R2.id.rB_eventualidad_ocho)
    RadioButton rBEventualidadOcho;
    @BindView(R2.id.rB_eventualidad_cuatro)
    RadioButton rBEventualidadCuatro;
    @BindView(R2.id.rB_eventualidad_nueve)
    RadioButton rBEventualidadNueve;
    @BindView(R2.id.rbg_eventualidades)
    RadioGroup rbgEventualidades;
    @BindView(R2.id.ly_calificar)
    LinearLayout lyCalificar;
    @BindView(R2.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R2.id.texto_contador)
    TextView textoContador;
    @BindView(R2.id.tv_titulo_cancelar)
    TextView tvTituloCancelar;

    private int idEventualidad, calificacion;
    private boolean isQuiosko;
    private Activity context;

    @Override
    public void conexionCompleta() {
        super.conexionCompleta();
        registrarAcitvityServer(context, "CancelarSolicitudActivity");
        presentarEventualidades();
        cargarEstrellas();
        if (mostrarCasillaCancelar()) {
            tvComentario.setVisibility(View.VISIBLE);
            textoContador.setVisibility(View.VISIBLE);
        } else {
            tvComentario.setVisibility(View.GONE);
            textoContador.setVisibility(View.GONE);
        }
        if (servicioSocket.solicitudSeleccionada != null && servicioSocket.solicitudSeleccionada.isPedido()) {
            tvTituloCancelar.setText(R.string.msj_cancelar_compra);
            tvTituloCancelar.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Window win = getWindow();
        context = this;
        spParametrosConfiguracion = getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
        setContentView(R.layout.activity_cancelar_solicitud);
        ButterKnife.bind(context);
        setSupportActionBar(appbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.cancelar);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        utilidades = new Utilidades();
        lyCalificar.setVisibility(View.GONE);
        textoContador.setText(R.string.msj_minimo_doce_caracteres);

        tvComentario.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String tamanoString = String.valueOf(12 - s.length());
                if (Integer.parseInt(tamanoString) <= 0) {
                    textoContador.setVisibility(View.GONE);
                } else {
                    textoContador.setVisibility(View.VISIBLE);
                    textoContador.setText(getString(R.string.str_minimo_caracteres).concat(" " + tamanoString + " ").concat(getString(R.string.str_caracteres)));
                }

            }
        });
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isQuiosko = bundle.getBoolean("isQuiosko");
        }
        if (isQuiosko) {
            rBEventualidadOcho.setVisibility(View.GONE);
            rBEventualidadCuatro.setVisibility(View.GONE);
            rBEventualidadNueve.setVisibility(View.GONE);
        }

    }

    @OnClick({R2.id.btn_cancelar, R2.id.btn_aceptar})
    public void onClick(View view) {
        Intent intent = new Intent();
        int i = view.getId();
        if (i == R.id.btn_cancelar) {
            intent.putExtra("m", "n");
            setResult(3, intent);
            finish();

        } else if (i == R.id.btn_aceptar) {
            if (mostrarCasillaCancelar()) {
                if (tvComentario.getText().toString().trim().length() < 12) {
                    utilidades.customToast(context, getString(R.string.msj_minimo_caracteres_comentario));
                    return;
                }
            }
            if (servicioSocket.solicitudSeleccionada != null && servicioSocket.solicitudSeleccionada.isPedido()) {
                servicioSocket.cancelarPedido(idEventualidad, 7, servicioSocket.solicitudSeleccionada, true,
                        tvComentario.getText().toString());
                intent.putExtra("m", "s");
                setResult(3, intent);
                finish();
            } else {
                if (idEventualidad != 0) {
                    servicioSocket.cancelarSolicitud(idEventualidad, 7,
                            servicioSocket.solicitudSeleccionada, true, calificacion,
                            tvComentario.getText().toString());
                    intent.putExtra("m", "s");
                    setResult(3, intent);
                    finish();
                } else {
                    utilidades.customToastCorto(context, getString(R.string.msj_seleccione_eventualidad));
                }
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registrarAcitvityServer(context, "CancelarSolicitudActivity");
        utilidades.hideFloatingView(context);
    }

    public void presentarEventualidades() {
        if (mBound) {
            if (servicioSocket.solicitudSeleccionada != null) {
                if (servicioSocket.solicitudSeleccionada.getIdPedido() == 0) {
                    if (!isQuiosko) {
                        if (servicioSocket.solicitudSeleccionada.isBotonAbordo()) {
                            rBEventualidadNueve.setVisibility(View.GONE);
                            rBEventualidadSeis.setVisibility(View.VISIBLE);
                        } else {
                            rBEventualidadSeis.setVisibility(View.GONE);
                        }
                    }
                    rbgEventualidades.setOnCheckedChangeListener((group, checkedId) -> {
                        if (checkedId == R.id.rB_eventualidad_seis) {
                            idEventualidad = 6;
                            lyCalificar.setVisibility(View.VISIBLE);

                        } else if (checkedId == R.id.rB_eventualidad_ocho) {
                            idEventualidad = 8;
                            lyCalificar.setVisibility(View.GONE);

                        } else if (checkedId == R.id.rB_eventualidad_cuatro) {
                            idEventualidad = 4;
                            lyCalificar.setVisibility(View.GONE);

                        } else if (checkedId == R.id.rB_eventualidad_nueve) {
                            idEventualidad = 9;
                            lyCalificar.setVisibility(View.GONE);

                        }
                    });
                } else {
                    if (servicioSocket.isEnviarComprarPedido()) {
                        rbgEventualidades.setVisibility(View.GONE);
                        if (servicioSocket.isEnviarllevandoPedido()) {
                            idEventualidad = 9;
                        } else {
                            idEventualidad = 8;
                        }
                    }
                }
            }
        }
    }

    public void cargarEstrellas() {
        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            try {
                Hashtable<Integer, Integer> contenedor = new Hashtable<>();
                contenedor.put(0, 5);
                contenedor.put(1, 5);
                contenedor.put(2, 4);
                contenedor.put(3, 3);
                contenedor.put(4, 2);
                contenedor.put(5, 1);
                calificacion = contenedor.get((int) rating);
            } catch (NullPointerException e) {
                e.printStackTrace();
                calificacion = 0;
            }
        });
    }

    public boolean mostrarCasillaCancelar() {
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 15:
                                if (objetoConfig.getInt("h") == 1) {
                                    return false;
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.getMessage();
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // ID del boton
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

