package com.kradac.conductor.vista;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.AdaptadorListaSolicitudes;
import com.kradac.conductor.adaptadoreslista.GridAdapterSeleccionarTiempo;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.interfaces.OnComunicacionListaAdapter;
import com.kradac.conductor.interfaces.OnComunicacionSocketLista;
import com.kradac.conductor.modelo.DatosEnvioTaximetro;
import com.kradac.conductor.modelo.DatosTaximetro;
import com.kradac.conductor.modelo.Destino;
import com.kradac.conductor.modelo.MensajeDeuda;
import com.kradac.conductor.modelo.ModeloItemTiempos;
import com.kradac.conductor.modelo.RespuestaConfiguracion;
import com.kradac.conductor.modelo.Solicitud;
import com.kradac.conductor.presentador.MapaPresenter;
import com.kradac.conductor.presentador.PublicidadPresenter;
import com.kradac.conductor.service.TaximetroService;
import com.kradac.wigets.LayoutPublicitario;
import com.kradac.wigets.extra.VariablesPublicidad;
import com.kradac.wigets.modelo.RespuestaPublicidad;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.srain.cube.views.GridViewWithHeaderAndFooter;

public class ListaSolicitudesActivity extends BaseConexionService implements OnComunicacionSocketLista, OnComunicacionListaAdapter {
    public static boolean estadoLista = false;
    public ArrayList<Solicitud> listaSolicitudes;
    public Activity activity = this;
    public boolean isEstadoButton;
    public int numeroPasajeros = 1;
    public Dialog pDialogo;
    @BindView(R2.id.tv_sub_total)
    TextView tvSubTotal;
    @BindView(R2.id.btn_mostrar_taximetro)
    Button btnMostrarTaximetro;
    @BindView(R2.id.tv_hora_inicio)
    TextView tvHoraInicio;
    @BindView(R2.id.tv_hora_fin)
    TextView tvHoraFin;
    @BindView(R2.id.tv_distancia_recorrida)
    TextView tvDistanciaRecorrida;
    @BindView(R2.id.tv_velocidad)
    TextView tvVelocidad;
    @BindView(R2.id.tv_tiempo)
    TextView tvTiempo;
    @BindView(R2.id.tv_cosoto_arranque)
    TextView tvCosotoArranque;
    @BindView(R2.id.tv_valor_distancia)
    TextView tvValorDistancia;
    @BindView(R2.id.tv_tiempo_espera)
    TextView tvTiempoEspera;
    @BindView(R2.id.tv_valor_tiempo)
    TextView tvValorTiempo;
    @BindView(R2.id.ly_taximetro)
    LinearLayout lyTaximetro;
    @BindView(R2.id.img_logo_ktaxi)
    ImageView imgLogoKtaxi;
    @BindView(R2.id.tv_sin_solicitud)
    TextView tvSinSolicitud;
    @BindView(R2.id.ly_publicidad)
    LayoutPublicitario lyPublicidad;
    @BindView(R2.id.row_tiempo_espera)
    TableRow rowTiempoEspera;
    private View inclTaximetro;
    private AdaptadorListaSolicitudes miAdaptador;
    private ProgressDialog pDialog;
    private Dialog dialogoTiempo, dialogoEspera, alerDialogoCancelar, dialogoCompras, dialogoGps;
    private Solicitud solicitudSeleccionada;
    private int tamanioLista, getTiempo;
    private Button btnOffLine;
    private SharedPreferences spDatoDineroElectronico;
    private boolean isPantalla = false;
    private Window win;
    private double propina = 0, saldoKtaxi = 0;
    private FloatingActionButton fab_estado;
    private DatosTaximetro datosTaximetro;
    private SharedPreferences spParametrosConfiguracion;
    private ActionBar ab;
    private String totalCarreraCajaTexto = "0";
    private Dialog dialogCobro;
    private ProgressDialog progressDialog;
    private double costo = 0;
    private double valorMaximo = 10;
    private Dialog dialogoVoucherPin;
    private Dialog dialogoDineroElectronico;
    private boolean isDialogoEspera;
    private String totalCarrera;
    private PublicidadPresenter publicidadPresenter;

    private int valorEditarCostoC;

    @Override
    public void conexionCompleta() {
        super.conexionCompleta();
        registrarAcitvityServer(this, "ListaSolicitudesActivity");
        if (spEstadobtn.getBoolean(VariablesGlobales.ESTADO_BTN, true)) {
            cambiarEstadoLibre();
        } else {
            if (servicioSocket.isTaximetroRun) {
                datosTaximetro = DatosTaximetro.obtenerDatosTaximetro(ListaSolicitudesActivity.this);
                if (datosTaximetro != null && datosTaximetro.getR().getVisible() == 1) {
                    inclTaximetro.setVisibility(View.VISIBLE);
                } else {
                    inclTaximetro.setVisibility(View.GONE);
                    lyTaximetro.setVisibility(View.GONE);
                }
            } else {
                inclTaximetro.setVisibility(View.GONE);
            }
            cambiarEstadoOcupado();
        }
        estadoServicio();
    }


    private String totalCarreraTemporal;
    private Dialog dialogOtraApp;

    public void estadoServicio() {
        if (mBound) {
            if (spEstadobtn.getBoolean(VariablesGlobales.ESTADO_BTN, false)) {
                servicioSocket.consultarSolicitudesEntrantes();
                servicioSocket.retomarCarreras();
            }
            servicioSocket.estadoSolicitud();
            servicioSocket.estadoPedido();
            servicioSocket.cancelNotification(ListaSolicitudesActivity.this, 4);
        }
    }

    public void mostrarSinSolicitud() {
        tvSinSolicitud.setVisibility(View.VISIBLE);
        imgLogoKtaxi.setVisibility(View.VISIBLE);
    }

    public void ocultarSinSolicitud() {
        tvSinSolicitud.setVisibility(View.GONE);
        imgLogoKtaxi.setVisibility(View.GONE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        win = getWindow();
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
                | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
        setContentView(R.layout.activity_lista_solicitudes);
        ButterKnife.bind(this);
        datosTaximetro = DatosTaximetro.obtenerDatosTaximetro(this);
        inclTaximetro = (View) findViewById(R.id.incl_taximetro);
        Toolbar toolbar = (Toolbar) findViewById(R.id.appbar);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
//            WebView myWebView = (WebView) this.findViewById(R.id.web_wiew_publicidad);
//            myWebView.loadUrl("http://173.192.13.14:8080/banner/");
        }
        setSupportActionBar(toolbar);
        ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle("Lista Solicitudes");
            if (!utilidades.isDesarrolloPorduccion(this)) {
                ab.setBackgroundDrawable(new ColorDrawable(utilidades.getColorWrapper(this, R.color.desarrollo)));
            }
        }
        btnOffLine = (Button) findViewById(R.id.btn_off_line_lis);
        ListView listView = (ListView) findViewById(R.id.listSolicitudes);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                utilidades.neutralizarDialogo(dialogoTiempo);
                if (listaSolicitudes != null) {
                    try {
                        solicitudSeleccionada = listaSolicitudes.get(position);
                        if (solicitudSeleccionada != null) {
                            seleccionarTiempo();
                        }
                    } catch (IndexOutOfBoundsException e) {
                        listaSolicitudes.clear();
                    }
                }
            }

        });
        listaSolicitudes = new ArrayList<>();
        miAdaptador = new AdaptadorListaSolicitudes(ListaSolicitudesActivity.this, listaSolicitudes, this);
        listView.setAdapter(miAdaptador);
        if (getIntent().getExtras() != null) {
            isPantalla = getIntent().getExtras().getBoolean("pantalla");
            tamanioLista = getIntent().getExtras().getInt("tamanioLista");
            if (getIntent().getExtras().getInt("tamanio") > 0) {
                ocultarSinSolicitud();
            }
        }

        fab_estado = (FloatingActionButton) findViewById(R.id.fab);
        fab_estado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spEstadobtn.getBoolean(VariablesGlobales.ESTADO_BTN, false)) {
                    lyPublicidad.hide();
                    if (!utilidades.isVertical(ListaSolicitudesActivity.this)) {
                        peticionDePublicidad(VariablesPublicidad.OCUPADO_SIN_CARRERA, 0);
                    }
                    cambiarEstadoOcupado();
                    activarTaximetro();

                } else {
                    if (servicioSocket.isTaximetroRun) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                        builder.setMessage("¿Desea terminar la carrera y realizar el cobro?.")
                                .setCancelable(true)
                                .setTitle("Información")
                                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        servicioSocket.finalizarTaximetro();
                                        stopService(new Intent(ListaSolicitudesActivity.this, TaximetroService.class));
                                        inclTaximetro.setVisibility(View.GONE);
                                        presentarValorCobro();
                                        guardarDatosTaximetro(); // Aqui es donde se guarda los datos del táximetro.
                                        tvHoraInicio.setText("");
                                        tvDistanciaRecorrida.setText("");
                                        tvVelocidad.setText("");
                                        tvTiempo.setText("");
                                        tvCosotoArranque.setText("");
                                        tvValorDistancia.setText("");
                                        tvTiempoEspera.setText("");
                                        tvValorTiempo.setText("");
                                        tvSubTotal.setText("");
                                        if (!utilidades.isVertical(ListaSolicitudesActivity.this)) {
                                            peticionDePublicidad(VariablesPublicidad.LIBRE_SIN_CARRERA, 0);
                                        }
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                }).show();
                    } else {
                        presentarValorCobro();
                        if (!utilidades.isVertical(ListaSolicitudesActivity.this)) {
                            peticionDePublicidad(VariablesPublicidad.LIBRE_SIN_CARRERA, 0);
                        }
                    }
                }
            }
        });
        spParametrosConfiguracion = getSharedPreferences(VariablesGlobales.PARAMETROS_CONFIGURACION, Context.MODE_PRIVATE);
        cambioColorLista(listView);
    }


    public void cambiarIcono(boolean isEstado) {
        if (isEstado) {
            imgLogoKtaxi.setImageResource(R.drawable.logo_ktaxi_normal);
        } else {
            imgLogoKtaxi.setImageResource(R.drawable.logo_ktaxi_nocturno);
        }

    }

    public void cambioColorLista(ListView listView) {
        switch (spConfiguracionApp.getInt(VariablesGlobales.CONFIGURACION_ESTILO_MAPA, 3)) {
            case 1:
                listView.setBackgroundColor(getResources().getColor(R.color.blanco));
                cambiarIcono(true);
                break;
            case 2:
                listView.setBackgroundColor(getResources().getColor(R.color.seekbarfree));
                cambiarIcono(false);
                break;
            case 3:
                if (!utilidades.compararHorasFin(null, null)) {
                    listView.setBackgroundColor(getResources().getColor(R.color.seekbarfree));
                    cambiarIcono(false);
                } else {
                    listView.setBackgroundColor(getResources().getColor(R.color.blanco));
                    cambiarIcono(true);
                }
                break;
        }
        lyPublicidad.hide();
    }

    public void comprobarEstado() {
        if (spEstadobtn.getBoolean(VariablesGlobales.ESTADO_BTN, false)) {
            cambiarEstadoLibre();
        } else {
            cambiarEstadoOcupado();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registrarAcitvityServer(this, "ListaSolicitudesActivity");
        //comprobarEstado();
        estadoLista = true;
        estadoServicio();
        utilidades.hideFloatingView(this);
    }

    /**
     * Cambia ha estado ocupado la plicación
     */
    public void cambiarEstadoOcupado() {
        fab_estado.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.rojo)));
        fab_estado.setImageResource(R.mipmap.ic_ocupado_lista);
        mostrarSinSolicitud();
        SharedPreferences.Editor editor = spEstadobtn.edit();
        editor.putBoolean(VariablesGlobales.ESTADO_BTN, false);
        editor.apply();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cambiarPantallaEncendido();
                miAdaptador.clear();
                if (mBound) {
                    servicioSocket.isEstadoBoton = false;
                    servicioSocket.desactivaEnNuevaSolicitud();
                    servicioSocket.estadoDelTaxi(6);
                    servicioSocket.isEstadoAbordo = false;
                    servicioSocket.isEstadoBoton = false;
                    if (servicioSocket.estadoBoton <= 1) {
                        servicioSocket.estadoBotonDelTaxi(1);
                    }
                }

            }
        });
    }

    /**
     * Cambia el estado a libre de la aplicación
     */
    public void cambiarEstadoLibre() {
        fab_estado.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.verde)));
        fab_estado.setImageResource(R.mipmap.ic_libre_lista);
        final SharedPreferences.Editor editor = spEstadobtn.edit();
        editor.putBoolean(VariablesGlobales.ESTADO_BTN, true);
        editor.apply();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mBound) {
                    servicioSocket.activaEnNuevaSolicitud();
                    servicioSocket.consultarSolicitudesEntrantes();
                    servicioSocket.retomarCarreras();
                    servicioSocket.estadoDelTaxi(1);
                    servicioSocket.estadoBotonDelTaxi(0);
                    servicioSocket.isEstadoAbordo = false;
                    servicioSocket.isEstadoBoton = true;
                    int idTaximetro = spEstadobtn.getInt(VariablesGlobales.PARAMETRO_TAXIMETRO_TEMPORAL, 0);
                    DatosEnvioTaximetro datosEnvioTaximetro = null;
                    if (DatosEnvioTaximetro.getAll() != null && DatosEnvioTaximetro.getAll().size() > 0) {
                        datosEnvioTaximetro = DatosEnvioTaximetro.getAll().get(0);
                        totalCarrera = datosEnvioTaximetro.getTotalCarrera();
                    }
                    if (totalCarrera != null && idTaximetro != 0) {
                        if (datosEnvioTaximetro != null) {
                            new MapaPresenter(null, ListaSolicitudesActivity.this).finalizarTaximetro(
                                    idTaximetro,
                                    servicioSocket.getIdSolicitudSeleccionada(),
                                    servicioSocket.getLocationService().getLatitude(),
                                    servicioSocket.getLocationService().getLongitude(),
                                    Double.parseDouble(totalCarrera),
                                    Double.parseDouble(totalCarreraCajaTexto),
                                    datosEnvioTaximetro.getHoraInicio(),
                                    datosEnvioTaximetro.getHoraFin(),
                                    datosEnvioTaximetro.getTarifaArranque(),
                                    datosEnvioTaximetro.getTiempoTotal(),
                                    datosEnvioTaximetro.getTiempoEspera(),
                                    datosEnvioTaximetro.getValorTiempo(),
                                    datosEnvioTaximetro.getDistanciaRecorrida(),
                                    datosEnvioTaximetro.getValorDistancia());
                        }
                        DatosEnvioTaximetro.deleteAll();
                        editor.putInt(VariablesGlobales.PARAMETRO_TAXIMETRO_TEMPORAL, 0);
                        editor.apply();
                    }
                    totalCarrera = null;
                }

            }
        });


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                finish();
                startActivity(new Intent(ListaSolicitudesActivity.this, MapaBaseActivity.class));
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // ID del boton
                finish();
                startActivity(new Intent(ListaSolicitudesActivity.this, MapaBaseActivity.class));
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void dialogoCancelar(final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (utilidades.isVerificarDialogo(alerDialogoCancelar)) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                    alertDialogBuilder.setCancelable(true);
                    alertDialogBuilder.setMessage(mensaje)
                            .setTitle(getString(R.string.informacion))
                            .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                    if (!isFinishing()) {
                        alerDialogoCancelar = alertDialogBuilder.show();
                    }
                }
            }
        });

    }

    @Override
    public void nuevasolicitud(final ArrayList<Solicitud> listaSolicitu, final boolean isPantallaH) {
        try {
            listaSolicitudes = listaSolicitu;
            runOnUiThread(new Runnable() {
                              @Override
                              public void run() {
                                  if (listaSolicitudes != null) {
                                      if (listaSolicitudes.size() > 0) {
                                          if (ab != null) {
                                              ab.setSubtitle("Solicitudes disponibles: " + listaSolicitudes.size());
                                          }
                                          ocultarSinSolicitud();
                                          if (miAdaptador != null) {
                                              miAdaptador.clear();
                                              try {
                                                  miAdaptador.addAll(listaSolicitudes);
                                              } catch (ConcurrentModificationException e) {
                                                  e.printStackTrace();
                                              }
                                          }
                                          if (spConfiguracionApp.getBoolean(VariablesGlobales.CONFIGURACION_PANTALLA, true)) {
                                              if (isPantallaH) {
                                                  if (solicitudSeleccionada == null) {
                                                      try {
                                                          solicitudSeleccionada = listaSolicitudes.get(0);
                                                          seleccionarTiempo();
                                                      } catch (IndexOutOfBoundsException e) {
                                                          e.getMessage();
                                                      }
                                                  }
                                              }
                                          }

                                      } else {
                                          if (ab != null) {
                                              ab.setSubtitle("Solicitudes disponibles: " + listaSolicitudes.size());
                                          }
                                          mostrarSinSolicitud();
                                          miAdaptador.clear();
                                          if (dialogoTiempo != null) {
                                              if (dialogoTiempo.isShowing()) {
                                                  dialogoTiempo.dismiss();
                                              }
                                          }
                                          if (dialogoCompras != null) {
                                              if (dialogoCompras.isShowing()) {
                                                  dialogoCompras.dismiss();
                                              }
                                          }
                                      }
                                  } else {
                                      if (ab != null) {
                                          ab.setSubtitle("Solicitudes disponibles: " + listaSolicitudes.size());
                                      }
                                  }
                              }
                          }
            );
        } catch (ConcurrentModificationException | IllegalArgumentException e) {
            e.getMessage();
        }
    }

    @Override
    public void solicitudCallCenter(Solicitud solicitud) {
        if (solicitud.getIdSolicitud() <= 0) {
            finish();
        }
    }

    @Override
    public void clienteAceptoTiempo(final boolean isAceptado, final Solicitud solSelected, final String razon) {
        utilidades.neutralizarDialogo(pDialog);
        utilidades.neutralizarDialogo(dialogoEspera);
        utilidades.neutralizarDialogo(dialogoTiempo);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isAceptado) {
                    servicioSocket.estadoBotonDelTaxi(3);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                    startActivity(new Intent(ListaSolicitudesActivity.this, MapaBaseActivity.class));
                    finish();
                }
            }
        });
    }

    @Override
    public void clienteCanceloSolicitud(final Solicitud solicitudSeleccionada, final String razonCancelacion, boolean isCalificar) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                utilidades.neutralizarDialogo(pDialog);
                utilidades.neutralizarDialogo(dialogoEspera);
                utilidades.neutralizarDialogo(dialogoTiempo);
                utilidades.customToastSolicitud(ListaSolicitudesActivity.this, razonCancelacion, R.drawable.ic_error_toast, R.drawable.design_toast_personalizado);
                liberarAlCancelar();
            }
        });
    }

    @Override
    public void clienteCanceloPedido(final Solicitud solicitudSeleccionada, final String razonCancelacion, boolean isCalificar) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                utilidades.neutralizarDialogo(pDialog);
                utilidades.neutralizarDialogo(dialogoEspera);
                utilidades.neutralizarDialogo(dialogoTiempo);
                utilidades.customToastSolicitud(ListaSolicitudesActivity.this, razonCancelacion, R.drawable.ic_error_toast, R.drawable.design_toast_personalizado);
                liberarAlCancelar();
            }
        });
    }


    public void liberarAlCancelar() {
        if (mBound) {
            cambiarEstadoLibre();
            servicioSocket.activarAlCancelar();
            clearPantallaEncendido();
        }
    }


    public void cambiarPantallaEncendido() {
        win.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        win.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
    }

    public void clearPantallaEncendido() {
        win.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void mensajeAlertaCancelar(final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                servicioSocket.estadoBotonDelTaxi(0);
                dialogoCancelar(mensaje);
            }
        });
    }

    @Override
    public void tiempoEnviado(final int tiempo, final Solicitud solicitudSelec) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                solicitudSeleccionada = solicitudSelec;
                if (pDialog != null) {
                    if (!pDialog.isShowing()) {
                        mostrarDialogoEspera();
                    }
                }
                if (pDialog == null) {
                    mostrarDialogoEspera();
                }
            }
        });
    }

    @Override
    public void solicitudAtendida(final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                utilidades.neutralizarDialogo(pDialog);
                utilidades.neutralizarDialogo(dialogoEspera);
                utilidades.neutralizarDialogo(dialogoTiempo);
                dialogoCancelar(mensaje);
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        utilidades.neutralizarDialogo(alerDialogoCancelar);
                    }
                }, 150000);
                solicitudSeleccionada = null;
                servicioSocket.solicitudSeleccionada = null;
                servicioSocket.estadoPedido = 0;
                servicioSocket.estadoSolicitud = 0;
                servicioSocket.estadoBotonDelTaxi(0);
            }
        });
    }

    @Override
    public void alertasDialogos(int idSolicitud) {
        if (dialogoTiempo != null && solicitudSeleccionada != null) {
            if (solicitudSeleccionada.getIdSolicitud() == idSolicitud) {
                dialogoTiempo.dismiss();
                dialogoTiempo = null;
                solicitudSeleccionada = null;
                servicioSocket.solicitudSeleccionada = null;
            }
        }
    }


    @Override
    public void cambiarEstado(final boolean conectado) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!conectado) {
                    btnOffLine.setVisibility(View.VISIBLE);
                } else {
                    btnOffLine.setVisibility(View.GONE);
                    servicioSocket.consultarSolicitudesEntrantes();

                }
            }
        });
    }

    @Override
    public void configuracionServidorHelp(final String mensaje) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                builder.setMessage(mensaje)
                        .setTitle("Alerta")
                        .setPositiveButton("Actualizar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                utilidades.calificarApp(ListaSolicitudesActivity.this);
                            }
                        }).show();
            }
        });

    }


    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if (dialogoTiempo != null) {
            if (dialogoTiempo.isShowing()) {
                if (mBound) {
                    solicitudSeleccionada = null;
                    servicioSocket.borrarCarreraSeleccionada();
                    dialogoTiempo.dismiss();
                    dialogoTiempo = null;
                }
            }
        }
        utilidades.showFloatingView(this, false);
    }

    public void mostrarDialogoEspera() {
        if (utilidades.isVerificarDialogo(pDialog)) {
            if (servicioSocket.solicitudSeleccionada != null) {
                cambiarPantallaEncendido();
                pDialog = new ProgressDialog(activity);
                pDialog.setMessage(getString(R.string.esperando_confirmacion));
                if (solicitudSeleccionada.getIdPedido() != 0) {
                    pDialog.setCancelable(false);
                } else {
                    pDialog.setCancelable(true);
                    pDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            if (utilidades.isVerificarDialogo(dialogoEspera)) {
                                AlertDialog.Builder localBuilder = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                                localBuilder.setIcon(R.mipmap.ic_launcher);
                                localBuilder.setTitle("ALERTA");
                                localBuilder.setCancelable(false);
                                localBuilder
                                        .setMessage(getString(R.string.cancelar_solicitud));
                                localBuilder.setPositiveButton("Si",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog,
                                                                int paramAnonymous2Int) {
                                                dialog.dismiss();
                                                if (solicitudSeleccionada != null && servicioSocket.isClienteAceptoTiempo()) {
                                                    servicioSocket.cancelarSolicitud(12, 7, solicitudSeleccionada, false, 0, null);
                                                } else {
                                                    servicioSocket.cancelarSolicitud(10, 7, solicitudSeleccionada, false, 0, null);
                                                }
                                                servicioSocket.activaEnNuevaSolicitud();
                                                solicitudSeleccionada = null;
                                                utilidades.neutralizarDialogo(dialogoTiempo);
                                                utilidades.neutralizarDialogo(pDialog);
                                                servicioSocket.estadoPedido = 0;
                                                servicioSocket.estadoBotonDelTaxi(0);
                                            }
                                        });
                                localBuilder.setNegativeButton("No",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(
                                                    DialogInterface paramAnonymous2DialogInterface,
                                                    int paramAnonymous2Int) {
                                                paramAnonymous2DialogInterface.dismiss();
                                                mostrarDialogoEspera();
                                            }
                                        });
                                dialogoEspera = localBuilder.show();
                            }
                        }
                    });
                }
                if (!isFinishing()) {
                    pDialog.show();
                }
            }

        }

    }

    public SupportMapFragment mapFragmentDestino;

    public void cargarDestino(View viewInformacion, Solicitud solicitud) {
        LinearLayout lyDestino = viewInformacion.findViewById(R.id.ly_destino_ld);
//        mapFragmentDestino = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_destino);
        if (solicitud.getlD() != null && solicitud.getlD().size() > 0) {
            lyDestino.setVisibility(View.VISIBLE);
            Destino destino = solicitud.getlD().get(0);
            TextView tvDestinoBarrio = viewInformacion.findViewById(R.id.tv_destino_barrio);
            TextView tvDestinoCallePrincipal = viewInformacion.findViewById(R.id.tv_destino_calle_principal);
            TextView tvDestinoCalleSecundaria = viewInformacion.findViewById(R.id.tv_destino_barrio_calle_secundaria);
            TextView tvDestinoReferencia = viewInformacion.findViewById(R.id.tv_destino_refencia);
            TextView tvCostoDestino = viewInformacion.findViewById(R.id.tv_destino_costo);
            TextView tvTiempoDestino = viewInformacion.findViewById(R.id.tv_destino_tiempo);
            TextView tvDistanciaDestino = viewInformacion.findViewById(R.id.tv_destino_distancia);
            tvCostoDestino.setText(destino.getDesC().toString());
            tvTiempoDestino.setText(destino.getDesT());
            tvDistanciaDestino.setText(destino.getDesDis().toString().concat(" Km"));
            String barrioD = destino.getDesBar().isEmpty() ? "" : ("Barrio. ".concat(destino.getDesBar()));
            String cpD = destino.getDesCp().isEmpty() ? "" : (" CP. ".concat(destino.getDesCp()));
            String csD = destino.getDesCs().isEmpty() ? "" : (" CS. ".concat(destino.getDesCs()));
            String rfD = destino.getDesRef().isEmpty() ? "" : (" RF. ".concat(destino.getDesRef()));
            String destinoText = barrioD.concat(cpD).concat(csD).concat(rfD);
            tvDestinoBarrio.setText(destinoText);
            tvDestinoCallePrincipal.setText(destino.getDesCp());
            tvDestinoCalleSecundaria.setText(destino.getDesCs());
            tvDestinoReferencia.setText(destino.getDesRef());
//            if (mapFragmentDestino != null) {
//                mapFragmentDestino.getMapAsync(new OnMapReadyCallback() {
//                    @Override
//                    public void onMapReady(GoogleMap map) {
//                        LatLng locationDestino = new LatLng(destino.getLtD(), destino.getLgD());
//                        LatLng locationCliente = new LatLng(solicitud.getLatitud(), solicitud.getLongitud());
//                        agregarMarcador(map, locationDestino, false, solicitud.getBarrio());
//                        agregarMarcador(map, locationCliente, true, solicitud.getNombresCliente());
//                        centrarMarket(map, solicitud);
//                        map.getUiSettings().setCompassEnabled(false);
//                        map.getUiSettings().setZoomControlsEnabled(false);
//                        map.getUiSettings().setMapToolbarEnabled(false);
//                    }
//                });
//            } else {
//                Toast.makeText(this, "Error - Map Fragment was null!!", Toast.LENGTH_SHORT).show();
//            }

        } else {
            lyDestino.setVisibility(View.GONE);
        }
        viewInformacion.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                limpiarFragmentoMapa();
            }
        });
    }


    public void agregarMarcador(GoogleMap map, LatLng locationCliente, boolean isCliente, String desBar) {
        MarkerOptions mapketOption = new MarkerOptions()
                .position(locationCliente)
                .title("Destino")
                .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(),
                        isCliente ? R.mipmap.pin_usuario_mini : R.mipmap.pin_destino_mini)))
                .snippet(isCliente ? "Barrio: ".concat(desBar) : "Destino: ".concat(desBar));
        map.addMarker(mapketOption);
    }


    public void centrarMarket(GoogleMap mMap, Solicitud solicitudSeleccionada) {
        if (mMap != null && solicitudSeleccionada.getlD() != null && solicitudSeleccionada.getlD().size() > 0) {
            Destino destino = solicitudSeleccionada.getlD().get(0);
            LatLng latLngInicial = new LatLng(destino.getLtD(), destino.getLgD());
            LatLng latLngFinal = new LatLng(solicitudSeleccionada.getLatitud(), solicitudSeleccionada.getLongitud());
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(latLngInicial).include(latLngFinal);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(builder.build(), 30);
            mMap.moveCamera(cameraUpdate);
        }
    }

    public void limpiarFragmentoMapa() {
        try {
            if (mapFragmentDestino != null)
                getSupportFragmentManager().beginTransaction().remove(mapFragmentDestino).commit();
        } catch (IllegalStateException e) {
            Log.e("ListaSolicitudes", "limpiarFragmentoMapa: " + e.getMessage());
        }


    }

    public void seleccionarTiempo() {
        limpiarFragmentoMapa();
        if (solicitudSeleccionada != null) {
            if (!solicitudSeleccionada.isPedido()) {
                if (utilidades.isVerificarDialogo(dialogoTiempo)) {
                    ArrayList<ModeloItemTiempos> itemTiemposList = new ArrayList<>();
                    final int[] arrayData;
                    if (solicitudSeleccionada.getTiempos().size() == 0) {
                        arrayData = new int[4];
                        itemTiemposList.add(new ModeloItemTiempos("1 minuto"));
                        itemTiemposList.add(new ModeloItemTiempos("3 minutos"));
                        itemTiemposList.add(new ModeloItemTiempos("5 minutos"));
                        itemTiemposList.add(new ModeloItemTiempos("7 minutos"));
                        arrayData[0] = 1;
                        arrayData[1] = 3;
                        arrayData[2] = 5;
                        arrayData[3] = 7;
                    } else {
                        arrayData = new int[solicitudSeleccionada.getTiempos().size()];
                        for (int i = 0; i < solicitudSeleccionada.getTiempos().size(); i++) {
                            if (solicitudSeleccionada.getTiempos().get(i) == 1) {
                                itemTiemposList.add(new ModeloItemTiempos(solicitudSeleccionada.getTiempos().get(i) + " minuto"));
                            } else {
                                itemTiemposList.add(new ModeloItemTiempos(solicitudSeleccionada.getTiempos().get(i) + " minutos"));
                            }
                            arrayData[i] = solicitudSeleccionada.getTiempos().get(i);
                        }
                    }
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View viewInformacion = inflater.inflate(R.layout.item_seleccionar_tiempos, null, false);
                    cargarDestino(viewInformacion, solicitudSeleccionada);
                    LinearLayout linearColor = viewInformacion.findViewById(R.id.linearColor);
                    if (solicitudSeleccionada.getColor() != null) {
                        linearColor.setBackgroundColor(Color.parseColor(solicitudSeleccionada.getColor()));
                    } else {
                        linearColor.setBackgroundColor(Color.parseColor("#dddddd"));
                    }
                    TextView tvServicio = viewInformacion.findViewById(R.id.tv_servicio);
                    utilidades.definirNombreServicio(solicitudSeleccionada, tvServicio);
                    ImageView imageEstadoGps = viewInformacion.findViewById(R.id.img_estado_gps);
                    if (RespuestaConfiguracion.isActivarIconoEstadoGpsSolicitud(this)) {
                        imageEstadoGps.setVisibility(View.VISIBLE);
                        imageEstadoGps.setImageResource(utilidades.iconEstadoGpsEnSolicitud(solicitudSeleccionada.getConP()));
                    } else {
                        imageEstadoGps.setVisibility(View.GONE);
                    }
                    ScrollView rlColor = viewInformacion.findViewById(R.id.srcv_fondo);
                    ImageView imageFormaPago = viewInformacion.findViewById(R.id.imv_icon);
                    imageFormaPago.setImageResource(utilidades.iconFormaPagoSolicitud(solicitudSeleccionada.getTipoFormaPago()));
                    ImageView tipoSolicitud = viewInformacion.findViewById(R.id.btnInfoMapa);
                    if (!solicitudSeleccionada.isPedido()) {
                        tipoSolicitud.setImageResource(utilidades.iconTipoSolitud(0));
                    } else {
                        tipoSolicitud.setImageResource(utilidades.iconTipoSolitud(solicitudSeleccionada.getT()));
                    }
                    rlColor.setBackgroundDrawable(utilidades.drawableFondoSolicitud(solicitudSeleccionada.isQuiosco(),
                            solicitudSeleccionada.getTipoFormaPago(), solicitudSeleccionada.isPedido(), solicitudSeleccionada.getT(), this));
                    ImageButton buttonCancelar = viewInformacion.findViewById(R.id.accion_cerrar);
                    buttonCancelar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            utilidades.neutralizarDialogo(dialogoTiempo);
                            solicitudSeleccionada = null;
                            if (mBound) {
                                servicioSocket.rechazarSolicitud();
                                servicioSocket.borrarCarreraSeleccionada();
                                servicioSocket.activaEnNuevaSolicitud();
                                servicioSocket.estadoBotonDelTaxi(0);
                                servicioSocket.retomarCarreras();

                            }
                        }
                    });
                    final EditText et_cobro_carrera = viewInformacion.findViewById(R.id.tv_cobro_carrera);
                    TextView tvPropina = viewInformacion.findViewById(R.id.tv_propina);
                    if (solicitudSeleccionada != null && solicitudSeleccionada.getPropina() != 0) {
                        tvPropina.setVisibility(View.VISIBLE);
                        tvPropina.setText("Propina: ".concat(String.valueOf(solicitudSeleccionada.getPropina())));
                    } else {
                        tvPropina.setVisibility(View.GONE);
                    }
                    TextView tvSaldoKtaxi = viewInformacion.findViewById(R.id.tv_saldo_ktaxi);
                    tvSaldoKtaxi.setVisibility(View.GONE);
                    if (solicitudSeleccionada != null && solicitudSeleccionada.getSaldo() != 0) {
                        tvSaldoKtaxi.setVisibility(View.GONE);
                        tvSaldoKtaxi.setText("Saldo ktaxi: ".concat(String.valueOf(solicitudSeleccionada.getSaldo())));
                    } else {
                        tvSaldoKtaxi.setVisibility(View.GONE);
                    }
                    Button buttonIgnorar = (Button) viewInformacion.findViewById(R.id.btn_ignorar);
                    et_cobro_carrera.setVisibility(View.GONE);
                    boolean isCobro = false;
                    String dataConfig = servicioSocket.getSpParametrosConfiguracion().getString(VariablesGlobales.DATOS_CONFIGURACION, "");
                    final boolean isCobroFin = isCobro;
                    TextView tvBarrio = viewInformacion.findViewById(R.id.tv_barrio);
                    if (utilidades.ocularMostrarCampos(solicitudSeleccionada.getBarrio())) {
                        tvBarrio.setVisibility(View.VISIBLE);
                        tvBarrio.setText(solicitudSeleccionada.getBarrio());
                    } else {
                        tvBarrio.setVisibility(View.GONE);
                    }
                    TextView tvCallePrincipal = viewInformacion.findViewById(R.id.tv_calle_principal);
                    if (utilidades.ocularMostrarCampos(solicitudSeleccionada.getCallePrincipal())) {
                        tvCallePrincipal.setVisibility(View.VISIBLE);
                        tvCallePrincipal.setText(solicitudSeleccionada.getCallePrincipal());
                    } else {
                        tvCallePrincipal.setVisibility(View.GONE);
                    }
                    TextView tvCalleSecundaria = viewInformacion.findViewById(R.id.tv_calle_secundaria);
                    if (utilidades.ocularMostrarCampos(solicitudSeleccionada.getCalleSecundaria())) {
                        tvCalleSecundaria.setVisibility(View.VISIBLE);
                        tvCalleSecundaria.setText(solicitudSeleccionada.getCalleSecundaria());
                    } else {
                        tvCalleSecundaria.setVisibility(View.GONE);
                    }
                    ((TextView) viewInformacion.findViewById(R.id.tv_distancia_tiempo)).setText(utilidades.tiempoDistancia(solicitudSeleccionada));


                    final GridViewWithHeaderAndFooter grid = viewInformacion.findViewById(R.id.grid_time);
                    grid.setAdapter(new GridAdapterSeleccionarTiempo(this, itemTiemposList));

                    final Button btnEnvioTiempo = (Button) viewInformacion.findViewById(R.id.btn_envio_tiempo);
                    getTiempo = arrayData[arrayData.length - 1];
                    getTiempo = getTiempo + 1;
                    btnEnvioTiempo.setText(getTiempo + " minutos");

                    viewInformacion.findViewById(R.id.img_btn_mas).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (getTiempo >= arrayData[arrayData.length - 1] + 30) {
                                utilidades.mostrarMensajeCorto(ListaSolicitudesActivity.this, "No puede exceder el límite máximo");
                            } else {
                                getTiempo = getTiempo + 1;
                                btnEnvioTiempo.setText(getTiempo + " minutos");
                            }

                        }
                    });
                    viewInformacion.findViewById(R.id.img_btn_menos).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (getTiempo <= arrayData[0]) {
                                utilidades.mostrarMensajeCorto(ListaSolicitudesActivity.this, "No puede exceder el límite mínimo");
                            } else {
                                getTiempo = getTiempo - 1;
                                btnEnvioTiempo.setText(getTiempo + " minutos");
                            }

                        }
                    });


                    btnEnvioTiempo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!isCobroFin) {
                                if (mBound) {
                                    servicioSocket.desactivaEnNuevaSolicitud();
                                    servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdSolicitud(), 1);
                                    mostrarDialogoEspera();
                                    servicioSocket.estadoBotonDelTaxi(2);
                                    servicioSocket.enviarTiempo(getTiempo, 0);
                                }
                                utilidades.neutralizarDialogo(dialogoTiempo);
                            } else {
                                if (et_cobro_carrera.getText().length() >= 1) {
                                    double cobroEnviar = 0;
                                    try {
                                        cobroEnviar = Double.parseDouble(et_cobro_carrera.getText().toString());
                                    } catch (NumberFormatException ex) {
                                        utilidades.customToastCorto(ListaSolicitudesActivity.this, "El valor ingresado no es valido");
                                        return;
                                    }
                                    if (mBound) {
                                        servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdSolicitud(), 1);
                                        mostrarDialogoEspera();
                                        servicioSocket.estadoBotonDelTaxi(2);
                                        servicioSocket.enviarTiempo(getTiempo, cobroEnviar);
                                    }
                                    utilidades.neutralizarDialogo(dialogoTiempo);
                                } else {
                                    utilidades.customToastCorto(ListaSolicitudesActivity.this, "ingrese el valor de la carrera");
                                }
                            }
                        }
                    });

                    grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            if (!isCobroFin) {
                                if (mBound) {
                                    servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdSolicitud(), 1);
                                    int selectedPosition = position;
                                    mostrarDialogoEspera();
                                    servicioSocket.estadoBotonDelTaxi(2);
                                    servicioSocket.enviarTiempo(arrayData[selectedPosition], 0);
                                }
                                utilidades.neutralizarDialogo(dialogoTiempo);
                            } else {
                                if (et_cobro_carrera.getText().length() >= 1) {
                                    double cobroEnviar = 0;
                                    try {
                                        cobroEnviar = Double.parseDouble(et_cobro_carrera.getText().toString());
                                    } catch (NumberFormatException ex) {
                                        utilidades.customToastCorto(ListaSolicitudesActivity.this, "El valor ingresado no es valido");
                                        return;
                                    }
                                    if (mBound) {
                                        servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdSolicitud(), 1);
                                        int selectedPosition = position;
                                        mostrarDialogoEspera();
                                        servicioSocket.estadoBotonDelTaxi(2);
                                        servicioSocket.enviarTiempo(arrayData[selectedPosition], cobroEnviar);
                                    }
                                    utilidades.neutralizarDialogo(dialogoTiempo);
                                } else {
                                    utilidades.customToastCorto(ListaSolicitudesActivity.this, "ingrese el valor de la carrera");
                                }
                            }
                        }
                    });
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setView(viewInformacion);
                    builder.setCancelable(false);
                    if (!isFinishing()) {
                        dialogoTiempo = builder.show();
                    }
                    buttonIgnorar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mBound) {
                                servicioSocket.eliminarDeLaLista(solicitudSeleccionada);
                                servicioSocket.rechazarSolicitud();
                                servicioSocket.borrarCarreraSeleccionada();
                                servicioSocket.activaEnNuevaSolicitud();
                                servicioSocket.estadoBotonDelTaxi(0);
                            }
                            utilidades.neutralizarDialogo(dialogoTiempo);
                            solicitudSeleccionada = null;

                        }
                    });
                }
            } else {
                switch (solicitudSeleccionada.getT()) {
                    case 1:
                        if (utilidades.isVerificarDialogo(dialogoCompras)) {
                            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View viewInformacion = inflater.inflate(R.layout.dialog_seleccionar_precio_pedido,
                                    null, false);
                            final ScrollView scrollView = (ScrollView) viewInformacion.findViewById(R.id.scrv_pedido);
                            LinearLayout linearColor = viewInformacion.findViewById(R.id.linearColor);
                            if (solicitudSeleccionada.getColor() != null) {
                                linearColor.setBackgroundColor(Color.parseColor(solicitudSeleccionada.getColor()));
                            } else {
                                linearColor.setBackgroundColor(Color.parseColor("#dddddd"));
                            }
                            TextView tvServicio = viewInformacion.findViewById(R.id.tv_servicio);
                            utilidades.definirNombreServicio(solicitudSeleccionada, tvServicio);
                            scrollView.setBackgroundDrawable(utilidades.drawableFondoSolicitud(solicitudSeleccionada.isQuiosco(),
                                    solicitudSeleccionada.getTipoFormaPago(), solicitudSeleccionada.isPedido(), solicitudSeleccionada.getT(), this));
                            final TextView txtCliente = (TextView) viewInformacion.findViewById(R.id.txt_nombre_cliente);
                            final TextView txtPedido = (TextView) viewInformacion.findViewById(R.id.txt_pedido_com);
                            final TextView txtLugarCompra = (TextView) viewInformacion.findViewById(R.id.tv_lugar_compra);
                            final TextView txtLugarEntrega = (TextView) viewInformacion.findViewById(R.id.tv_lugar_entrega);
                            final Button btnAceptar = (Button) viewInformacion.findViewById(R.id.btn_aceptar_compra);
                            final Button btnCancelar = (Button) viewInformacion.findViewById(R.id.btn_cancelar_compra);
                            final RadioButton radioButton_uno = (RadioButton) viewInformacion.findViewById(R.id.rb_precio_one);
                            radioButton_uno.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, solicitudSeleccionada.getLp().get(0)));
                            final RadioButton radioButton_dos = (RadioButton) viewInformacion.findViewById(R.id.rb_precio_two);
                            radioButton_dos.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, solicitudSeleccionada.getLp().get(1)));
                            final RadioButton radioButton_tre = (RadioButton) viewInformacion.findViewById(R.id.rb_precio_there);
                            radioButton_tre.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, solicitudSeleccionada.getLp().get(2)));
                            final RadioButton radioButton_cuatro = (RadioButton) viewInformacion.findViewById(R.id.rb_precio_four);
                            final LinearLayout ly_precios_otro = (LinearLayout) viewInformacion.findViewById(R.id.ly_precio_otros);
                            final TextView tv_precios_otro = (TextView) viewInformacion.findViewById(R.id.tv_precio_otro);
                            final ImageButton btn_precio_aumentar = viewInformacion.findViewById(R.id.btn_precio_aumentar);
                            final ImageButton btn_precio_disminuir = viewInformacion.findViewById(R.id.btn_precio_disminuir);
                            ly_precios_otro.setVisibility(View.GONE);
                            txtCliente.setText(solicitudSeleccionada.getNombresCliente());
                            txtPedido.setText(solicitudSeleccionada.getPedido());
                            txtLugarCompra.setText(solicitudSeleccionada.getLugarCompra());
                            txtLugarEntrega.setText(solicitudSeleccionada.getBarrio());
                            final double[] precio = new double[1];
                            precio[0] = 0;
                            radioButton_uno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(0);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_dos.setChecked(false);
                                        radioButton_tre.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_dos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(1);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_uno.setChecked(false);
                                        radioButton_tre.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_tre.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(2);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_uno.setChecked(false);
                                        radioButton_dos.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_cuatro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        ly_precios_otro.setVisibility(View.VISIBLE);
                                        tv_precios_otro.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, solicitudSeleccionada.getLp().get(2)));
                                        precio[0] = solicitudSeleccionada.getLp().get(2);
                                        radioButton_uno.setChecked(false);
                                        radioButton_dos.setChecked(false);
                                        radioButton_tre.setChecked(false);

                                    }
                                }
                            });

                            btn_precio_aumentar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    double total = Double.parseDouble(tv_precios_otro.getText().toString());
                                    if (total >= solicitudSeleccionada.getM()) {
                                        utilidades.customToastCorto(ListaSolicitudesActivity.this, "No se puede aumentar mas la cantidad");
                                        btn_precio_aumentar.setEnabled(false);
                                        return;
                                    }
                                    btn_precio_disminuir.setEnabled(true);
                                    String retorno = utilidades.dosDecimales(ListaSolicitudesActivity.this, (total + solicitudSeleccionada.getAu()));
                                    tv_precios_otro.setText(retorno);
                                    precio[0] = total + solicitudSeleccionada.getAu();


                                }
                            });

                            btn_precio_disminuir.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    double total = Double.parseDouble(tv_precios_otro.getText().toString());
                                    if (total <= solicitudSeleccionada.getLp().get(0)) {
                                        utilidades.customToastCorto(ListaSolicitudesActivity.this, "No se puede reducir la cantidad");
                                        btn_precio_disminuir.setEnabled(false);
                                        return;
                                    }
                                    if (total > solicitudSeleccionada.getLp().get(0)) {
                                        btn_precio_aumentar.setEnabled(true);
                                        String retorno = utilidades.dosDecimales(ListaSolicitudesActivity.this, (total - solicitudSeleccionada.getAu()));
                                        tv_precios_otro.setText(retorno);
                                        precio[0] = total - solicitudSeleccionada.getAu();
                                    }
                                }
                            });

                            AlertDialog.Builder builder = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                            builder.setView(viewInformacion);
                            if (!isFinishing()) {
                                dialogoCompras = builder.show();
                            }
                            btnCancelar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    utilidades.neutralizarDialogo(dialogoCompras);
                                    solicitudSeleccionada = null;
                                    if (mBound) {
                                        servicioSocket.rechazarSolicitud();
                                        servicioSocket.borrarCarreraSeleccionada();
                                        servicioSocket.activaEnNuevaSolicitud();
                                        servicioSocket.estadoBotonDelTaxi(0);
                                        servicioSocket.retomarCarreras();
                                    }
                                }
                            });
                            btnAceptar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (mBound) {
                                        if (precio[0] != 0) {
                                            servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdPedido(), 2);
                                            servicioSocket.enviarAceptarPedido(precio[0]);
                                            mostrarDialogoEspera();
                                            utilidades.neutralizarDialogo(dialogoCompras);
                                            servicioSocket.estadoBotonDelTaxi(2);
                                        } else {
                                            utilidades.customToastCorto(ListaSolicitudesActivity.this, getString(R.string.precio_antes_envio));
                                        }
                                    }
                                }
                            });
                        }
                        break;
                    case 2:
                        if (utilidades.isVerificarDialogo(dialogoCompras)) {
                            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View viewInformacion = inflater.inflate(R.layout.dialog_seleccionar_precio_encomienda, null, false);
                            final ScrollView scrollView = (ScrollView) viewInformacion.findViewById(R.id.scrv_pedido);
                            LinearLayout linearColor = viewInformacion.findViewById(R.id.linearColor);
                            if (solicitudSeleccionada.getColor() != null) {
                                linearColor.setBackgroundColor(Color.parseColor(solicitudSeleccionada.getColor()));
                            } else {
                                linearColor.setBackgroundColor(Color.parseColor("#dddddd"));
                            }
                            TextView tvServicio = viewInformacion.findViewById(R.id.tv_servicio);
                            utilidades.definirNombreServicio(solicitudSeleccionada, tvServicio);
                            scrollView.setBackgroundDrawable(utilidades.drawableFondoSolicitud(solicitudSeleccionada.isQuiosco(),
                                    solicitudSeleccionada.getTipoFormaPago(), solicitudSeleccionada.isPedido(), solicitudSeleccionada.getT(), this));
                            final TextView tvDesde = (TextView) viewInformacion.findViewById(R.id.tv_desde);
                            final TextView tvEntregar = (TextView) viewInformacion.findViewById(R.id.tv_entregar);
                            final TextView tvCallePrincipal = (TextView) viewInformacion.findViewById(R.id.tv_calle_principal);
                            final TextView tvCalleSecundaria = (TextView) viewInformacion.findViewById(R.id.tv_calle_secundaria);
                            final TextView tvReferecnia = (TextView) viewInformacion.findViewById(R.id.tv_referecnia);
                            final TextView tvEncomienda = (TextView) viewInformacion.findViewById(R.id.tv_encomienda);
                            final Button btnAceptar = (Button) viewInformacion.findViewById(R.id.btn_aceptar_compra);
                            final Button btnCancelar = (Button) viewInformacion.findViewById(R.id.btn_cancelar_compra);
                            final RadioButton radioButton_uno = (RadioButton) viewInformacion.findViewById(R.id.rb_precio_one);
                            radioButton_uno.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, solicitudSeleccionada.getLp().get(0)));
                            final RadioButton radioButton_dos = (RadioButton) viewInformacion.findViewById(R.id.rb_precio_two);
                            radioButton_dos.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, solicitudSeleccionada.getLp().get(1)));
                            final RadioButton radioButton_tre = (RadioButton) viewInformacion.findViewById(R.id.rb_precio_there);
                            radioButton_tre.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, solicitudSeleccionada.getLp().get(2)));
                            final RadioButton radioButton_cuatro = (RadioButton) viewInformacion.findViewById(R.id.rb_precio_four);
                            final LinearLayout ly_precios_otro = (LinearLayout) viewInformacion.findViewById(R.id.ly_precio_otros);
                            final TextView tv_precios_otro = (TextView) viewInformacion.findViewById(R.id.tv_precio_otro);
                            final Button btn_precio_aumentar = (Button) viewInformacion.findViewById(R.id.btn_precio_aumentar);
                            final Button btn_precio_disminuir = (Button) viewInformacion.findViewById(R.id.btn_precio_disminuir);
                            ly_precios_otro.setVisibility(View.GONE);
                            tvDesde.setText(solicitudSeleccionada.getbE());
                            tvEntregar.setText(solicitudSeleccionada.getBarrio());
                            tvCallePrincipal.setText(solicitudSeleccionada.getCallePrincipal());
                            tvCalleSecundaria.setText(solicitudSeleccionada.getCalleSecundaria());
                            tvReferecnia.setText(solicitudSeleccionada.getReferencia());
                            tvEncomienda.setText(solicitudSeleccionada.getPedido());
                            final double[] precio = new double[1];
                            precio[0] = 0;
                            radioButton_uno.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(0);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_dos.setChecked(false);
                                        radioButton_tre.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_dos.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(1);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_uno.setChecked(false);
                                        radioButton_tre.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_tre.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        precio[0] = solicitudSeleccionada.getLp().get(2);
                                        ly_precios_otro.setVisibility(View.GONE);
                                        radioButton_uno.setChecked(false);
                                        radioButton_dos.setChecked(false);
                                        radioButton_cuatro.setChecked(false);
                                    }
                                }
                            });
                            radioButton_cuatro.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                    if (isChecked) {
                                        ly_precios_otro.setVisibility(View.VISIBLE);
                                        tv_precios_otro.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, solicitudSeleccionada.getLp().get(0)));
                                        precio[0] = solicitudSeleccionada.getLp().get(2);
                                        radioButton_uno.setChecked(false);
                                        radioButton_dos.setChecked(false);
                                        radioButton_tre.setChecked(false);

                                    }
                                }
                            });

                            btn_precio_aumentar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    double total = Double.parseDouble(tv_precios_otro.getText().toString());
                                    if (total == solicitudSeleccionada.getM()) {
                                        utilidades.customToastCorto(ListaSolicitudesActivity.this, "No se puede aumentar mas la cantidad");
                                        btn_precio_aumentar.setEnabled(false);
                                        return;
                                    }
                                    btn_precio_disminuir.setEnabled(true);
                                    String retorno = utilidades.dosDecimales(ListaSolicitudesActivity.this, total + solicitudSeleccionada.getAu());
                                    tv_precios_otro.setText(retorno);
                                    precio[0] = total + solicitudSeleccionada.getAu();
                                }
                            });

                            btn_precio_disminuir.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    double total = Double.parseDouble(tv_precios_otro.getText().toString());
                                    if (total == solicitudSeleccionada.getLp().get(0)) {
                                        utilidades.customToastCorto(ListaSolicitudesActivity.this, "No se puede reducir la cantidad");
                                        btn_precio_disminuir.setEnabled(false);
                                        return;
                                    }
                                    if (total > solicitudSeleccionada.getLp().get(0)) {
                                        btn_precio_aumentar.setEnabled(true);
                                        String retorno = utilidades.dosDecimales(ListaSolicitudesActivity.this, total - solicitudSeleccionada.getAu());
                                        tv_precios_otro.setText(retorno);
                                        precio[0] = total - solicitudSeleccionada.getAu();
                                    }
                                }
                            });

                            AlertDialog.Builder builder = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                            builder.setView(viewInformacion);
                            dialogoCompras = builder.show();
                            btnCancelar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    utilidades.neutralizarDialogo(dialogoCompras);
                                    solicitudSeleccionada = null;
                                    if (mBound) {
                                        servicioSocket.rechazarSolicitud();
                                        servicioSocket.borrarCarreraSeleccionada();
                                        servicioSocket.activaEnNuevaSolicitud();
                                        servicioSocket.estadoBotonDelTaxi(0);
                                        servicioSocket.retomarCarreras();
                                    }
                                }
                            });
                            btnAceptar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (mBound) {
                                        if (precio[0] != 0) {
                                            servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdPedido(), 2);
                                            servicioSocket.enviarAceptarPedido(precio[0]);
                                            mostrarDialogoEspera();
                                            utilidades.neutralizarDialogo(dialogoCompras);
                                            servicioSocket.estadoBotonDelTaxi(2);
                                        } else {
                                            utilidades.customToastCorto(ListaSolicitudesActivity.this, getString(R.string.precio_antes_envio));
                                        }
                                    }
                                }
                            });

                        }
                        break;
                }

            }
        } else {
            utilidades.customToastCorto(ListaSolicitudesActivity.this, getString(R.string.solicitud_atendida));
            servicioSocket.estadoBotonDelTaxi(0);
            utilidades.neutralizarDialogo(dialogoTiempo);
            servicioSocket.estadoBoton = 0;
            solicitudSeleccionada = null;
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        estadoLista = false;
        utilidades.neutralizarDialogo(pDialog);
        utilidades.neutralizarDialogo(dialogoTiempo);
        utilidades.neutralizarDialogo(dialogoEspera);
        utilidades.neutralizarDialogo(alerDialogoCancelar);
        utilidades.neutralizarDialogo(dialogoCompras);
    }

    @Override
    public void masTiempo(Solicitud solicitud) {
        solicitudSeleccionada = solicitud;
        if (solicitudSeleccionada != null) {
            seleccionarTiempo();
        }
    }

    @Override
    public void enviarTiempo(int tiempo, Solicitud solicitud) {
        solicitudSeleccionada = solicitud;
        if (mBound) {
            servicioSocket.desactivaEnNuevaSolicitud();
            servicioSocket.seleccionarSolicitud(solicitudSeleccionada.getIdSolicitud(), 1);
            mostrarDialogoEspera();
            servicioSocket.estadoBotonDelTaxi(2);
            servicioSocket.enviarTiempo(tiempo, 0);
        }
        utilidades.neutralizarDialogo(dialogoTiempo);
    }

    /**
     * cobrar carrera
     * Presenta el dialogo para el cambio de clave del usuario
     */
    public void presentarValorCobro() {
        if (utilidades.isVerificarDialogo(dialogCobro)) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View viewInformacion = inflater.inflate(R.layout.dialog_cobro_forma_pago, null, false);
            final ImageView img_tipo = viewInformacion.findViewById(R.id.img_tipo_cobro);
            final ImageView img_salir = viewInformacion.findViewById(R.id.img_salir);

            final Button btnVaucher = viewInformacion.findViewById(R.id.btn_vaucher);
            final Button btnVaucherPin = viewInformacion.findViewById(R.id.btn_envio_pin);
            final Button btnClienteUido = viewInformacion.findViewById(R.id.btn_cliente_uido);
            final Button btnPayPhone = viewInformacion.findViewById(R.id.btn_pay_phone);
            final Button btnElectronico = viewInformacion.findViewById(R.id.btn_electronico);
            final Button btnEfectivo = viewInformacion.findViewById(R.id.btn_efectivo);
            final Button btnMasPasajero = viewInformacion.findViewById(R.id.btn_mas_pasajero);
            final Button btnMenosPasajero = viewInformacion.findViewById(R.id.btn_menos_pasajero);

            TableRow filaSaldoKtaxi = viewInformacion.findViewById(R.id.fila_saldo_ktaxi);
            LinearLayout lyPasajero = viewInformacion.findViewById(R.id.ly_pasajeros); //Se define si se muestra o no
            LinearLayout lyVoucher = viewInformacion.findViewById(R.id.ly_voucher); //Se define si se muestra o no
            definirPasajero(lyPasajero);
            if (mBound) {
                if (servicioSocket.solicitudSeleccionada != null && servicioSocket.solicitudSeleccionada.getTipoFormaPago() == 1) {
                    lyVoucher.setVisibility(View.VISIBLE);
                }
            }
            definirIconoFormaDePago(img_tipo);
            final EditText etCostoCarrera = viewInformacion.findViewById(R.id.et_costo_carrera);
            final EditText etSaldoKtaxi = viewInformacion.findViewById(R.id.et_saldo_ktaxi);
            final EditText etPropina = viewInformacion.findViewById(R.id.et_propina);
            final EditText etValorCobrar = viewInformacion.findViewById(R.id.et_valor_cobro);

            if (valorEditarCostoC == 0) {
                etCostoCarrera.setEnabled(false);
            } else {
                etCostoCarrera.setEnabled(true);
            }

            if (totalCarreraTemporal != null) {
                totalCarrera = totalCarreraTemporal;
            }
            if (totalCarrera != null) {
                totalCarreraTemporal = totalCarrera;
            }
            etValorCobrar.setEnabled(false);
            TextView tvNumPasajeros = viewInformacion.findViewById(R.id.tv_num_pasajero);
            if (solicitudSeleccionada != null) {
                etSaldoKtaxi.setText(String.valueOf(solicitudSeleccionada.getSaldo()));
                etPropina.setText(String.valueOf(solicitudSeleccionada.getPropina()));
                if (totalCarrera != null && !totalCarrera.isEmpty()) {
                    etCostoCarrera.setText(totalCarrera);
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty()) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty()) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty()) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                } else {
                    etCostoCarrera.setText("0");
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty()) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty()) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty()) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                }
            } else {
                filaSaldoKtaxi.setVisibility(View.GONE);
                if (totalCarrera != null && !totalCarrera.isEmpty()) {
                    etCostoCarrera.setText(totalCarrera);
                    etSaldoKtaxi.setText(String.valueOf(0.0));
                    etPropina.setText(String.valueOf(0.0));
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty() && !etCostoCarrera.getText().toString().equals(".")) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty() && !etSaldoKtaxi.getText().toString().equals(".")) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty() && !etPropina.getText().toString().equals(".")) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                } else {
                    etCostoCarrera.setText(String.valueOf(0.0));
                    etSaldoKtaxi.setText(String.valueOf(0.0));
                    etPropina.setText(String.valueOf(0.0));
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty() && !etCostoCarrera.getText().toString().equals(".")) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty() && !etSaldoKtaxi.getText().toString().equals(".")) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty() && !etPropina.getText().toString().equals(".")) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                }
            }


            etSaldoKtaxi.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty() && !etCostoCarrera.getText().toString().equals(".")) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty() && !etSaldoKtaxi.getText().toString().equals(".")) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty() && !etPropina.getText().toString().equals(".")) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            etPropina.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty() && !etCostoCarrera.getText().toString().equals(".")) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty() && !etSaldoKtaxi.getText().toString().equals(".")) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty() && !etPropina.getText().toString().equals(".")) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
            etCostoCarrera.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    double costoCarrera = 0;
                    double saldoKtaxi = 0;
                    double propina = 0;
                    if (!etCostoCarrera.getText().toString().isEmpty()) {
                        costoCarrera = Double.parseDouble(etCostoCarrera.getText().toString());
                    }
                    if (!etSaldoKtaxi.getText().toString().isEmpty()) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    }
                    if (!etPropina.getText().toString().isEmpty()) {
                        propina = Double.parseDouble(etPropina.getText().toString());
                    }
                    etValorCobrar.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, totalCarrera(costoCarrera, saldoKtaxi, propina)));
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            definirHindCobro(etCostoCarrera);
            definirHindCobro(etSaldoKtaxi);
            definirHindCobro(etPropina);
            definirHindCobro(etValorCobrar);

            btnMasPasajero.setOnClickListener(clickPasajero(tvNumPasajeros));
            btnMenosPasajero.setOnClickListener(clickPasajero(tvNumPasajeros));

            btnVaucher.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            btnVaucherPin.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            btnClienteUido.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            btnPayPhone.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            btnElectronico.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            btnEfectivo.setOnClickListener(definirFormaPago(etCostoCarrera, etValorCobrar, etSaldoKtaxi, etPropina, tvNumPasajeros));
            img_salir.setOnClickListener(definirFormaPago(null, null, null, null, null));

            definirFormaDePago(lyVoucher, btnElectronico, btnPayPhone);

            AlertDialog.Builder builder = new AlertDialog.Builder(ListaSolicitudesActivity.this);
            builder.setView(viewInformacion);
            builder.setCancelable(false);
            dialogCobro = builder.show();
        }
    }

    public void definirFormaDePago(LinearLayout lyVoucher, Button electronico, Button payPhone) {
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 17:
                                if (objetoConfig.getInt("h") == 1) {
                                    electronico.setVisibility(View.VISIBLE);
                                } else {
                                    electronico.setVisibility(View.GONE);
                                }
                                break;
                            case 18:
                                if (objetoConfig.getInt("h") == 1) {
                                    if (servicioSocket.solicitudSeleccionada != null && servicioSocket.solicitudSeleccionada.getTipoFormaPago() != 1) {
                                        lyVoucher.setVisibility(View.GONE);
                                    } else {
                                        if (servicioSocket.solicitudSeleccionada != null) {
                                            lyVoucher.setVisibility(View.VISIBLE);
                                        } else {
                                            lyVoucher.setVisibility(View.GONE);
                                        }

                                    }

                                } else {
                                    if (mBound) {
                                        if (servicioSocket.solicitudSeleccionada != null && servicioSocket.solicitudSeleccionada.getTipoFormaPago() == 1) {
                                            lyVoucher.setVisibility(View.VISIBLE);
                                        } else {
                                            lyVoucher.setVisibility(View.GONE);
                                        }
                                    }
                                }
                                break;
                            case 19:
                                if (objetoConfig.getInt("h") == 1) {
                                    electronico.setVisibility(View.VISIBLE);
                                } else {
                                    electronico.setVisibility(View.GONE);
                                }
                                break;
                            case 20:
                                if (objetoConfig.getInt("h") == 1) {
                                    payPhone.setVisibility(View.VISIBLE);
                                } else {
                                    payPhone.setVisibility(View.GONE);

                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {

            }
            if (solicitudSeleccionada != null) {
                switch (solicitudSeleccionada.getTipoFormaPago()) {
                    case VariablesGlobales.VOUCHER:
                        lyVoucher.setVisibility(View.VISIBLE);
                        break;
                    case VariablesGlobales.DINERO_ELECTRONICO:
                        electronico.setVisibility(View.VISIBLE);
                        break;
                    case VariablesGlobales.PAYPHONE:
                        payPhone.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }
    }

    public double totalCarrera(double costoCarrera, double saldoKtaxi, double propina) {
        double total = costoCarrera - saldoKtaxi;
        if (total < 0) {
            return propina;
        } else {
            return total + propina;
        }
    }

    public View.OnClickListener clickPasajero(final TextView tvNumPasajero) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numero = Integer.parseInt(tvNumPasajero.getText().toString());
                int i = v.getId();
                if (i == R.id.btn_mas_pasajero) {
                    if (numero == 6) {
                        Toast.makeText(ListaSolicitudesActivity.this, "Este es el numero maximo permitido", Toast.LENGTH_LONG).show();
                        return;
                    }
                    numero = numero + 1;
                    tvNumPasajero.setText(String.valueOf(numero));

                } else if (i == R.id.btn_menos_pasajero) {
                    if (numero == 1) {
                        Toast.makeText(ListaSolicitudesActivity.this, "Este es el numero mínimo permitido", Toast.LENGTH_LONG).show();
                        return;
                    }
                    numero = numero - 1;
                    tvNumPasajero.setText(String.valueOf(numero));

                }
            }
        };
    }

    /**
     * Sirve para determinar que los campos ingresados tengan datos validos para poder segir con la forma de pago
     *
     * @param editText
     * @return
     */
    public boolean provarCampos(EditText editText) {
        if (editText.getText().toString().isEmpty()) {
            return false;
        }
        if (editText.getText().toString().equals(".")) {
            return false;
        }
        return true;
    }

    /**
     * Define el la imagen que se va a mortarar en la forma de pago
     *
     * @param img_tipo
     */
    public void definirIconoFormaDePago(ImageView img_tipo) {
        if (mBound) {
            switch (servicioSocket.getTipoFormaPago()) {
                case VariablesGlobales.VOUCHER:
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        img_tipo.setBackgroundDrawable(ContextCompat.getDrawable(this, R.mipmap.voucherapp));
                    } else {
                        img_tipo.setBackground(ContextCompat.getDrawable(this, R.mipmap.voucherapp));
                    }
                    break;
                case VariablesGlobales.DINERO_ELECTRONICO:
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        img_tipo.setBackgroundDrawable(ContextCompat.getDrawable(this, R.mipmap.ic_dinero_electronico));
                    } else {
                        img_tipo.setBackground(ContextCompat.getDrawable(this, R.mipmap.ic_dinero_electronico));
                    }
                    break;
                case VariablesGlobales.PAYPHONE:
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        img_tipo.setBackgroundDrawable(ContextCompat.getDrawable(this, R.mipmap.ic_icon_payphone));
                    } else {
                        img_tipo.setBackground(ContextCompat.getDrawable(this, R.mipmap.ic_icon_payphone));
                    }
                    break;
                case VariablesGlobales.TARJETA_CREDITO:
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        img_tipo.setBackgroundDrawable(ContextCompat.getDrawable(this, R.mipmap.ic_icon_tarjeta_credito));
                    } else {
                        img_tipo.setBackground(ContextCompat.getDrawable(this, R.mipmap.ic_icon_tarjeta_credito));
                    }
                    break;
                case VariablesGlobales.SALDO_KTAXI:
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        img_tipo.setBackgroundDrawable(ContextCompat.getDrawable(this, R.mipmap.ic_saldo_ktaxi));
                    } else {
                        img_tipo.setBackground(ContextCompat.getDrawable(this, R.mipmap.ic_saldo_ktaxi));
                    }
                    break;
                default:
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        img_tipo.setBackgroundDrawable(ContextCompat.getDrawable(this, R.mipmap.ic_efectivo));
                    } else {
                        img_tipo.setBackground(ContextCompat.getDrawable(this, R.mipmap.ic_efectivo));
                    }
                    break;
            }
        }
    }

    /***
     * OnClick de forma de pago aqui es donde se define el comporataiento de cada uno de los botones de forma de pago
     * @return
     */
    public View.OnClickListener definirFormaPago(final EditText costoCarrera,
                                                 final EditText costoFinal, final EditText etSaldoKtaxi, final EditText EtPropina,
                                                 final TextView TvNumeroPasajero) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (costoCarrera != null) {
                    if (!provarCampos(costoCarrera)) {
                        Toast.makeText(ListaSolicitudesActivity.this, "El valor ingresado no es correcto", Toast.LENGTH_LONG).show();
                        costoCarrera.setText("0.0");
                        costoCarrera.requestFocus();
                        return;
                    }
                }
                if (costoFinal != null) {
                    if (provarCampos(costoFinal)) {
                        costo = Double.parseDouble(costoFinal.getText().toString());
                    } else {
                        Toast.makeText(ListaSolicitudesActivity.this, "El valor ingresado no es correcto", Toast.LENGTH_LONG).show();
                        costoFinal.setText("0.0");
                        costoFinal.requestFocus();
                        return;
                    }
                }
                if (etSaldoKtaxi != null) {
                    if (provarCampos(etSaldoKtaxi)) {
                        saldoKtaxi = Double.parseDouble(etSaldoKtaxi.getText().toString());
                    } else {
                        Toast.makeText(ListaSolicitudesActivity.this, "El valor ingresado no es correcto", Toast.LENGTH_LONG).show();
                        etSaldoKtaxi.setText("0.0");
                        etSaldoKtaxi.requestFocus();
                        return;
                    }
                }
                if (EtPropina != null) {
                    if (provarCampos(EtPropina)) {
                        propina = Double.parseDouble(EtPropina.getText().toString());
                    } else {
                        Toast.makeText(ListaSolicitudesActivity.this, "El valor ingresado no es correcto", Toast.LENGTH_LONG).show();
                        EtPropina.setText("0.0");
                        EtPropina.requestFocus();
                        return;
                    }
                }
                if (TvNumeroPasajero != null) {
                    numeroPasajeros = Integer.parseInt(TvNumeroPasajero.getText().toString());
                }
                int i = v.getId();
                if (i == R.id.btn_vaucher) {
                    cobroVaucher(costo, saldoKtaxi, propina, numeroPasajeros);

                } else if (i == R.id.btn_envio_pin) {
                    dialogoVoucherPin = createVoucherPinDialogo(costo, saldoKtaxi, propina, numeroPasajeros);
                    dialogoVoucherPin.show();

                } else if (i == R.id.btn_cliente_uido) {
                    cobroVoucherClienteUido(costo, saldoKtaxi, propina, numeroPasajeros);

                } else if (i == R.id.btn_pay_phone) {
                } else if (i == R.id.btn_electronico) {
                    dialogoDineroElectronico = createDineroElectronicoDialogo(costo, saldoKtaxi, propina, numeroPasajeros);
                    dialogoDineroElectronico.show();

                } else if (i == R.id.btn_efectivo) {
                    cobroEfectivo(costo, saldoKtaxi, propina, numeroPasajeros);

                } else if (i == R.id.img_salir) {
                    if (dialogCobro != null && dialogCobro.isShowing()) {
                        dialogCobro.dismiss();
                    }

                }
            }
        };
    }

    /**
     * Crea un diálogo con personalizado
     *
     * @return Diálogo
     */
    public AlertDialog createVoucherPinDialogo(final double costo, final double saldoKtaxi,
                                               final double propina, final int numeroPasajero) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialogo_pin_voucher, null);
        final EditText etPinVaucher = v.findViewById(R.id.et_pin_carrera);
        Button btnRegresar = v.findViewById(R.id.btn_regresar);
        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = v.getId();
                if (i == R.id.btn_regresar) {
                    if (dialogoVoucherPin != null && dialogoVoucherPin.isShowing()) {
                        dialogoVoucherPin.dismiss();
                    }

                } else if (i == R.id.btn_aceptar) {
                    cobroVaucherPin(costo, etPinVaucher.getText().toString(), saldoKtaxi, propina, numeroPasajero);
                    if (dialogoVoucherPin != null && dialogoVoucherPin.isShowing()) {
                        dialogoVoucherPin.dismiss();
                    }

                }
            }
        };
        btnRegresar.setOnClickListener(onClickListener);
        btnAceptar.setOnClickListener(onClickListener);
        builder.setView(v);
        return builder.create();
    }

    /**
     * Crea un diálogo con personalizado
     *
     * @return Diálogo
     */
    public AlertDialog createDineroElectronicoDialogo(final double costo,
                                                      final double saldoKtaxi, final double propina, final int numeroPasajero) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View v = inflater.inflate(R.layout.dialogo_dinero_electronico, null);
        final EditText etUsuario = v.findViewById(R.id.et_usuario_dinero_electronico);
        final EditText etPin = v.findViewById(R.id.et_pin_dinero_electronico);
        Button btnRegresar = v.findViewById(R.id.btn_regresar);
        Button btnAceptar = v.findViewById(R.id.btn_aceptar);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = v.getId();
                if (i == R.id.btn_regresar) {
                    if (dialogoDineroElectronico != null && dialogoDineroElectronico.isShowing()) {
                        dialogoDineroElectronico.dismiss();
                    }

                } else if (i == R.id.btn_aceptar) {
                    cobroDineroElectronico(costo, etUsuario.getText().toString(), etPin.getText().toString(), saldoKtaxi, propina, numeroPasajero);
                    if (dialogoDineroElectronico != null && dialogoDineroElectronico.isShowing()) {
                        dialogoDineroElectronico.dismiss();
                    }

                }
            }
        };
        btnRegresar.setOnClickListener(onClickListener);
        btnAceptar.setOnClickListener(onClickListener);
        builder.setView(v);
        return builder.create();

    }

    /**
     * Muestra un dialogo de espera de cualquier activiadad de donde sea llamado
     *
     * @param titulo
     */
    public void mostrarEspera(String titulo) {
        pDialogo = new ProgressDialog(this);
        pDialogo.setCancelable(false);
        pDialogo.setTitle(titulo);
        if (!isFinishing()) {
            pDialogo.show();
        }
        isDialogoEspera = true;
    }

    /**
     * Oculta el dialogo del sistema.
     */
    public void ocultarEspera() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler(Looper.myLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (pDialogo != null) {
                                if (pDialogo.isShowing()) {
                                    pDialogo.dismiss();
                                    isDialogoEspera = false;
                                }
                            }
                        } catch (IllegalArgumentException e) {
                            e.getMessage();
                        }

                    }
                }, 100);
            }
        });
    }

    public void cobroEfectivo(final double costo, final double saldoKtaxi,
                              final double propina, final int numeroPasajero) {
        if (servicioSocket.getTipoFormaPago() != 0) {
            AlertDialog.Builder dialogo = new AlertDialog.Builder(ListaSolicitudesActivity.this);
            dialogo.setTitle("Información");
            if (servicioSocket.getTipoFormaPago() == VariablesGlobales.SALDO_KTAXI) {
                dialogo.setMessage("Esta solicitud se realizo con saldo ktaxi,es necesario pagar la diferencia con efectivo.");
                dialogo.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            } else {
                dialogo.setMessage("Esta solicitud se realizo con otra forma de pago, desea cobrar con efectivo");
            }
            dialogo.setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (costo > valorMaximo) {
                        DecimalFormat df = new DecimalFormat("####0.00");
                        final AlertDialog.Builder dialogo = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                        dialogo.setTitle("Alerta");
                        dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
                        dialogo.setCancelable(false);
                        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                dialogCobro.dismiss();
                                if (servicioSocket.getIdPedidoSeleccionado() != 0) {
                                    servicioSocket.efectivoCobrarConDriverPedido(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                                    mostrarEspera("Enviando datos.");
                                } else if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                                    servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                                    mostrarEspera("Enviando datos.");
                                } else {
                                    servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                                    mostrarEspera("Enviando datos.");
                                }
                                totalCarreraCajaTexto = String.valueOf(costo);
                            }
                        });
                        dialogo.setNegativeButton("Corregir", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogo1, int id) {
                                dialogo1.dismiss();
                            }
                        });
                        dialogo.show();
                    } else {
                        totalCarreraCajaTexto = String.valueOf(costo);
                        dialogCobro.dismiss();
                        if (servicioSocket.getIdPedidoSeleccionado() != 0) {
                            servicioSocket.efectivoCobrarConDriverPedido(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                        } else if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                            servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                        } else {
                            servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                        }
                    }
                }
            }).show();
        } else {
            if (costo > valorMaximo) {
                DecimalFormat df = new DecimalFormat("####0.00");
                final AlertDialog.Builder dialogo = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                dialogo.setTitle("Alerta");
                dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
                dialogo.setCancelable(false);
                dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        dialogCobro.dismiss();
                        totalCarreraCajaTexto = String.valueOf(costo);
                        if (servicioSocket.getIdPedidoSeleccionado() != 0) {
                            servicioSocket.efectivoCobrarConDriverPedido(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                            mostrarEspera("Enviando datos.");
                        } else if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                            servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                            mostrarEspera("Enviando datos.");
                        } else {
                            servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                            mostrarEspera("Enviando datos.");
                        }
                    }
                });
                dialogo.setNegativeButton("Corregir", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {
                        dialogo1.dismiss();
                    }
                });
                dialogo.show();
            } else {
                totalCarreraCajaTexto = String.valueOf(costo);
                dialogCobro.dismiss();
                if (servicioSocket.getIdPedidoSeleccionado() != 0) {
                    servicioSocket.efectivoCobrarConDriverPedido(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                    mostrarEspera("Enviando datos.");
                } else if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                    servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                    mostrarEspera("Enviando datos.");
                } else {
                    servicioSocket.efectivoCobrar(costo, saldoKtaxi, propina, numeroPasajero, solicitudSeleccionada);
                    mostrarEspera("Enviando datos.");
                }
            }
        }
    }

    public void definirPasajero(LinearLayout lyPasajero) {
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 17:
                                if (objetoConfig.getInt("h") == 1) {
                                    lyPasajero.setVisibility(View.VISIBLE);
                                } else {
                                    lyPasajero.setVisibility(View.GONE);
                                }
                                break;
                        }
                    } else {
                        lyPasajero.setVisibility(View.GONE);
                    }
                }
            } catch (JSONException e) {
                lyPasajero.setVisibility(View.GONE);
            }
        } else {
            lyPasajero.setVisibility(View.GONE);
        }
    }

    public void cobroVaucher(final double costo, final double saldoKtaxi,
                             final double propina, final int numeroPasajero) {
        if (costo > valorMaximo) {
            DecimalFormat df = new DecimalFormat("####0.00");
            final AlertDialog.Builder dialogo = new AlertDialog.Builder(ListaSolicitudesActivity.this);
            dialogo.setTitle("Alerta");
            dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
            dialogo.setCancelable(false);
            dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                        servicioSocket.voucherCobraVaucherConConfirmacion(costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                        dialogCobro.dismiss();
                        progressDialog = new ProgressDialog(ListaSolicitudesActivity.this);
                        progressDialog.setMessage("Esperando confirmación cliente");
                        progressDialog.setIndeterminate(false);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        totalCarreraCajaTexto = String.valueOf(costo);
                    } else {
                        Toast.makeText(ListaSolicitudesActivity.this, "Usted no puede realizar el cobro con voucher ya que la solicitud no fue realizada con esta forma de pago.", Toast.LENGTH_LONG).show();
                    }
                }
            });
            dialogo.setNegativeButton("Corregir", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    dialogo1.dismiss();
                }
            });
            dialogo.show();
        } else {
            if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                servicioSocket.voucherCobraVaucherConConfirmacion(costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                dialogCobro.dismiss();
                progressDialog = new ProgressDialog(ListaSolicitudesActivity.this);
                progressDialog.setMessage("Esperando confirmación cliente");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                progressDialog.show();
                totalCarreraCajaTexto = String.valueOf(costo);
            } else {
                Toast.makeText(ListaSolicitudesActivity.this, "Usted no puede realizar el cobro con voucher ya que la solicitud no fue realizada con esta forma de pago.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void cobroVaucherPin(final double costo, final String pin, final double saldoKtaxi,
                                final double propina, final int numeroPasajero) {
//        if (costo < 1 && costo != 0) {
//            Toast.makeText(this, "El valor a cobrar debe ser superior a 1", Toast.LENGTH_LONG).show();
//            return;
//        }
        if (costo > valorMaximo) {
            DecimalFormat df = new DecimalFormat("####0.00");
            final AlertDialog.Builder dialogo = new AlertDialog.Builder(ListaSolicitudesActivity.this);
            dialogo.setTitle("Alerta");
            dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
            dialogo.setCancelable(false);
            dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                        servicioSocket.voucherConPin(Integer.parseInt(pin), costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                        dialogCobro.dismiss();
                        progressDialog = new ProgressDialog(ListaSolicitudesActivity.this);
                        progressDialog.setMessage("Esperando confirmación");
                        progressDialog.setIndeterminate(false);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        totalCarreraCajaTexto = String.valueOf(costo);
                    }
                }
            });
            dialogo.setNegativeButton("Corregir", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    dialogo1.dismiss();
                }
            });
            dialogo.show();
        } else {
            if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                servicioSocket.voucherConPin(Integer.parseInt(pin), costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                dialogCobro.dismiss();
                progressDialog = new ProgressDialog(ListaSolicitudesActivity.this);
                progressDialog.setMessage("Esperando confirmación cliente");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                progressDialog.show();
                totalCarreraCajaTexto = String.valueOf(costo);
            }
        }
    }

    public void cobroVoucherClienteUido(final double costo, final double saldoKtaxi,
                                        final double propina, final int numeroPasajero) {
//        if (costo < 1 && costo != 0) {
//            Toast.makeText(this, "El valor a cobrar debe ser superior a 1", Toast.LENGTH_LONG).show();
//            return;
//        }
        if (costo > valorMaximo) {
            DecimalFormat df = new DecimalFormat("####0.00");
            final AlertDialog.Builder dialogo = new AlertDialog.Builder(ListaSolicitudesActivity.this);
            dialogo.setTitle("Alerta");
            dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
            dialogo.setCancelable(false);
            dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                        servicioSocket.voucherCobraVaucherClienteUido(costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                        dialogCobro.dismiss();
                        progressDialog = new ProgressDialog(ListaSolicitudesActivity.this);
                        progressDialog.setMessage("Esperando confirmación");
                        progressDialog.setIndeterminate(false);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        totalCarreraCajaTexto = String.valueOf(costo);
                    }
                }
            });
            dialogo.setNegativeButton("Corregir", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    dialogo1.dismiss();
                }
            });
            dialogo.show();
        } else {
            if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                servicioSocket.voucherCobraVaucherClienteUido(costo, servicioSocket.getIdSolicitudSeleccionada(), saldoKtaxi, propina, numeroPasajero);
                dialogCobro.dismiss();
                progressDialog = new ProgressDialog(ListaSolicitudesActivity.this);
                progressDialog.setMessage("Esperando confirmación cliente");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                progressDialog.show();
                totalCarreraCajaTexto = String.valueOf(costo);
            }
        }
    }

    public void cobroDineroElectronico(final double costo, final String usuario,
                                       final String pin, final double saldoKtaxi, final double propina, final int numeroPasajero) {
        if (costo > valorMaximo) {
            DecimalFormat df = new DecimalFormat("####0.00");
            final AlertDialog.Builder dialogo = new AlertDialog.Builder(ListaSolicitudesActivity.this);
            dialogo.setTitle("Alerta");
            dialogo.setMessage("¿Está seguro que el valor es: " + df.format(utilidades.round(costo, 2)) + "?");
            dialogo.setCancelable(false);
            dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                        servicioSocket.DineroElectronicoCobrar(costo, servicioSocket.getIdSolicitudSeleccionada(), false, usuario, pin, saldoKtaxi, propina, numeroPasajero);
                        dialogCobro.dismiss();
                        presentarDialogoCobro("Esperando confirmación");
                        totalCarreraCajaTexto = String.valueOf(costo);
                    }
                }
            });
            dialogo.setNegativeButton("Corregir", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogo1, int id) {
                    dialogo1.dismiss();
                }
            });
            dialogo.show();
        } else {
            if (servicioSocket.getIdSolicitudSeleccionada() != 0) {
                servicioSocket.DineroElectronicoCobrar(costo, servicioSocket.getIdSolicitudSeleccionada(), false, usuario, pin, saldoKtaxi, propina, numeroPasajero);
                dialogCobro.dismiss();
                presentarDialogoCobro("Esperando confirmación");
                totalCarreraCajaTexto = String.valueOf(costo);
            }
        }
    }

    /**
     * En base a la configuración del servidor se setean los valores del hind o muestra iniciales de cada unos de las cajas de texto
     *
     * @param etHind
     */

    public void definirHindCobro(EditText etHind) {
        String dataConfig = spParametrosConfiguracion.getString(VariablesGlobales.DATOS_CONFIGURACION, "");
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 12:
                                if (objetoConfig.getInt("h") == 1) {
                                    String[] data = objetoConfig.getString("vd").split(",");
                                    if (utilidades.compararHorasFin(data[2], data[3])) {
                                        etHind.setHint("Ejm: " + data[0] + " " + objetoConfig.getString("nb"));
                                        valorMaximo = Double.parseDouble(data[0]) * 10;
                                    } else {
                                        etHind.setHint("Ejm: " + data[1] + " " + objetoConfig.getString("nb"));
                                        valorMaximo = Double.parseDouble(data[1]) * 10;
                                    }
                                } else {
                                    if (utilidades.compararHorasFin(null, null)) {
                                        etHind.setHint("Ejm: 1.25 Usd");
                                    } else {
                                        etHind.setHint("Ejm: 1.40 Usd");
                                    }
                                }
                                break;
                            default:
                                if (utilidades.compararHorasFin(null, null)) {
                                    etHind.setHint("Ejm: 1.25 Usd");
                                } else {
                                    etHind.setHint("Ejm: 1.40 Usd");
                                }
                                break;
                        }
                    } else {
                        if (utilidades.compararHorasFin(null, null)) {
                            etHind.setHint("Ejm: 1.25 Usd");
                        } else {
                            etHind.setHint("Ejm: 1.40 Usd");
                        }
                    }
                }
            } catch (JSONException e) {
                if (utilidades.compararHorasFin(null, null)) {
                    etHind.setHint("Ejm: 1.25 Usd");
                } else {
                    etHind.setHint("Ejm: 1.40 Usd");
                }
            }
        } else {
            if (utilidades.compararHorasFin(null, null)) {
                etHind.setHint("Ejm: 1.25 Usd");
            } else {
                etHind.setHint("Ejm: 1.40 Usd");
            }
        }
    }

    public void preguntarEstadobateria() {
        PowerManager powerManager = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (powerManager.isPowerSaveMode()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Usted se encuentra en modo ahorro de energía, si usa este modo puede producir que la aplicación no trabaje de forma eficiente.")
                        .setTitle("Alerta")
                        .setPositiveButton("Cambiar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                utilidades.abrirIntentBatery(ListaSolicitudesActivity.this);
                            }
                        })
                        .setNegativeButton("Ignorar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
            }
        }
    }

    public void presentarDialogoCobro(String texto) {
        if (dialogCobro != null) {
            if (dialogCobro.isShowing()) {
                dialogCobro.dismiss();
            }
        }
//        if (utilidades.isVerificarDialogo(progressDialog)) {
        progressDialog = new ProgressDialog(ListaSolicitudesActivity.this);
        progressDialog.setMessage(texto);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
//        }

    }

    @Override
    public void notificarRastreo(final int editarCostoC, final int tipo, final String tiempoEspera, final String valorTiempo,
                                 final String velocidadtxt, final String distanciaRec, final String valorPrincipal,
                                 final String valDistancia, final String cronometro, final String horaInicioTxt,
                                 final String costoArranque, final String totalCarrea) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                valorEditarCostoC = editarCostoC;

                if (tipo == 1) {
                    rowTiempoEspera.setVisibility(View.VISIBLE);
                } else {
                    rowTiempoEspera.setVisibility(View.GONE);
                }
                ListaSolicitudesActivity.this.totalCarrera = utilidades.dosDecimales(ListaSolicitudesActivity.this, utilidades.valorDecimalTaximetro(Double.parseDouble(totalCarrea), datosTaximetro.getR().getvV()));
                tvSubTotal.setText(utilidades.dosDecimales(ListaSolicitudesActivity.this, utilidades.valorDecimalTaximetro(Double.parseDouble(valorPrincipal), datosTaximetro.getR().getvV())));
                tvHoraInicio.setText(horaInicioTxt);
                tvDistanciaRecorrida.setText(distanciaRec);
                tvVelocidad.setText(velocidadtxt);
                tvTiempo.setText(cronometro);
                tvCosotoArranque.setText(costoArranque);
                tvValorDistancia.setText(valDistancia);
                tvTiempoEspera.setText(tiempoEspera);
                tvValorTiempo.setText(valorTiempo);
            }
        });
    }

    public void activarTaximetro() {
        if (datosTaximetro != null) {
            if (spConfiguracionApp.getBoolean(VariablesGlobales.CONFIGURACION_TAXIMETRO, true) && mBound) {
                servicioSocket.iniciarTaximetro();
                totalCarrera = null;
                totalCarreraTemporal = null;
                totalCarreraCajaTexto = null;
                if (datosTaximetro.getR().getVisible() == 1) {
                    inclTaximetro.setVisibility(View.VISIBLE);
                }
                if (servicioSocket.getIdSolicitudSeleccionada() == 0) {
                    new MapaPresenter(null, this).iniciarTaximetro(2, 0, servicioSocket.getLocationService().getLatitude(), servicioSocket.getLocationService().getLongitude(), spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), spLogin.getInt(VariablesGlobales.ID_VEHICULO, 0));
                } else {
                    new MapaPresenter(null, this).iniciarTaximetro(1, servicioSocket.getIdSolicitudSeleccionada(), servicioSocket.getLocationService().getLatitude(), servicioSocket.getLocationService().getLongitude(), spLogin.getInt(VariablesGlobales.ID_USUARIO, 0), spLogin.getInt(VariablesGlobales.ID_VEHICULO, 0));
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("totalCarreraTemporal", totalCarreraTemporal);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        solicitudSeleccionada = savedInstanceState.getParcelable("solicitudSeleccionada");
        totalCarreraTemporal = savedInstanceState.getString("totalCarreraTemporal");
    }

    @OnClick(R2.id.btn_mostrar_taximetro)
    public void onViewClicked() {
        if (lyTaximetro.getVisibility() == View.VISIBLE) {
            lyTaximetro.setVisibility(View.GONE);
        } else {
            lyTaximetro.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void solicitudAtendidaACK(final MensajeDeuda mensajeDeuda) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                utilidades.neutralizarDialogo(pDialog);
                utilidades.neutralizarDialogo(dialogoEspera);
                utilidades.neutralizarDialogo(dialogoTiempo);
                dialogoEspera = createDialogoMensaje(mensajeDeuda.getL().getP().getMs());
                dialogoEspera.setCancelable(false);
                dialogoEspera.show();
            }
        });
    }

    /**
     * Crea un diálogo con personalizado
     *
     * @return Diálogo
     */
    public AlertDialog createDialogoMensaje(String mensaje) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();

        View v = inflater.inflate(R.layout.deudas_pendientes, null);

        TextView tvMensaje = (TextView) v.findViewById(R.id.tv_mensaje);

        TextView tvMensajeEspera = (TextView) v.findViewById(R.id.tv_mensaje_espera);
        tvMensajeEspera.setVisibility(View.VISIBLE);

        ImageView imageTaxista = (ImageView) v.findViewById(R.id.img_taxista);

        ProgressBar progressBarEspera = (ProgressBar) v.findViewById(R.id.progress_espera);

        Button btnAceptar = (Button) v.findViewById(R.id.btn_aceptar);
        btnAceptar.setVisibility(View.GONE);


        tvMensaje.setText(mensaje);

        builder.setView(v);

        return builder.create();
    }

    @Override
    public void otraApp(final String mensaje, final String paquete,
                        final boolean isDesinstalar, final boolean isCambioOcupado) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (dialogOtraApp != null) {
                    if (dialogOtraApp.isShowing()) {
                        return;
                    }
                }
                if (isCambioOcupado) {
                    cambiarEstadoOcupado();
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                builder.setMessage(mensaje)
                        .setTitle("Alerta")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                if (isDesinstalar) {
                    builder.setNegativeButton("Desinstalar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            utilidades.startInstalledAppDetailsActivity(ListaSolicitudesActivity.this, paquete);
                        }
                    });
                    dialogOtraApp = builder.create();
                    dialogOtraApp.show();
                } else {
                    if (utilidades.verificarEjecucionApp(paquete, ListaSolicitudesActivity.this)) {
                        builder.setNegativeButton("Forzar Cierre", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                utilidades.startInstalledAppDetailsActivity(ListaSolicitudesActivity.this, paquete);
                            }
                        });
                        dialogOtraApp = builder.create();
                        dialogOtraApp.show();
                    }
                }
            }
        });
    }

    @Override
    public void envioCobro(final String dato, final int estado) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ocultarEspera();
                AlertDialog.Builder builder = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                switch (estado) {
                    case 1:
                        servicioSocket.setCobroEfectivo(0, "", null);
                        cambiarEstadoLibre();
                        return;
                    case -1:
                        builder.setMessage(dato)
                                .setTitle("Alerta")
                                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        presentarValorCobro();
                                    }
                                });
                        if (!isFinishing()) {
                            builder.show();
                        }
                        return;
                    case -2:
                        servicioSocket.setCobroEfectivo(0, "", solicitudSeleccionada);
                        servicioSocket.setCobroTarjetaCredito(0, "", solicitudSeleccionada);
                        builder.setMessage(dato)
                                .setTitle("Alerta")
                                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        presentarValorCobro();
                                    }
                                });
                        if (!isFinishing()) {
                            builder.show();
                        }
                        return;
                }
            }
        });
    }

    @OnClick(R2.id.ly_publicidad)
    public void onClick() {
    }

    public void peticionDePublicidad(int estado, int idSolicitud) {
        publicidadPresenter = new PublicidadPresenter();
        publicidadPresenter.getPublicidad(VariablesGlobales.NUM_ID_APLICATIVO, spLogin.getInt("idCiudad", 0),
                estado, idSolicitud, spLogin.getInt("idVehiculo", 0), spLogin.getInt("idUsuario", 0), new PublicidadPresenter.OnComunicacionComponentePublicitario() {
                    @Override
                    public void respuesta(RespuestaPublicidad respuestaPublicidad) {
                        if (respuestaPublicidad != null) {
                            if (respuestaPublicidad.getEn() == 1) {
//                                respuestaPublicidad.getB().setForma(1);
//                                respuestaPublicidad.getB().setTipo(1);
                                lyPublicidad.cargarPublicidad(respuestaPublicidad);
                            }
                        }
                    }
                }, this);

        lyPublicidad.setTimeViewListener(new LayoutPublicitario.TimeViewListener() {
            @Override
            public void onClosePublicidad() {

            }

            @Override
            public void otraPantalla(RespuestaPublicidad respuestaPublicidad) {
                Intent intent = new Intent(ListaSolicitudesActivity.this, ComponentePublicitarioActivity.class);
                intent.putExtra("respuestaPublicitaria", respuestaPublicidad);
                startActivity(intent);
            }
        });
    }

    private void guardarDatosTaximetro() {
        DatosEnvioTaximetro datosEnvioTaximetro = new DatosEnvioTaximetro();
        datosEnvioTaximetro.setHoraInicio(tvHoraInicio.getText().toString());
        datosEnvioTaximetro.setHoraFin(utilidades.obtenerHora());
        datosEnvioTaximetro.setDistanciaRecorrida(tvDistanciaRecorrida.getText().toString());
        datosEnvioTaximetro.setTiempoTotal(tvTiempo.getText().toString());
        datosEnvioTaximetro.setTarifaArranque(tvCosotoArranque.getText().toString());
        datosEnvioTaximetro.setValorDistancia(tvValorDistancia.getText().toString());
        datosEnvioTaximetro.setTiempoEspera(tvTiempoEspera.getText().toString());
        datosEnvioTaximetro.setValorTiempo(tvValorTiempo.getText().toString());
        datosEnvioTaximetro.setTotalCarrera(totalCarrera);
        datosEnvioTaximetro.save();
    }

    @Override
    public void corazonMalvadoCambiarOcupado() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cambiarEstadoOcupado();
            }
        });
    }

    private Dialog dialogCorazon;

    @Override
    public void corazonMalvadoDialogo(final boolean isBooleanFinish) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cambiarEstadoOcupado();
                if (dialogCorazon != null && dialogCorazon.isShowing())
                    return;
                AlertDialog.Builder builder = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                builder.setCancelable(false);
                builder.setMessage("Hemos detectado inconvenientes en el dispositivo y Ktaxi Conductor no funciona correctamente.")
                        .setTitle("Error")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (isBooleanFinish) {
                                    Intent intent = new Intent(ListaSolicitudesActivity.this, MapaBaseActivity.class);
                                    intent.putExtra("isCorazonCerrar", true);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.left_in, R.anim.left_out);
                                }
                                if (dialogCorazon != null && dialogCorazon.isShowing())
                                    dialogCorazon.dismiss();
                            }
                        });
                dialogCorazon = builder.create();
                dialogCorazon.show();
                ;
            }
        });
    }

    @Override
    public void corazonMalvadoDialogoError(final boolean isLanzarError) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cambiarEstadoOcupado();
                if (dialogCorazon != null && dialogCorazon.isShowing())
                    return;
                AlertDialog.Builder builder = new AlertDialog.Builder(ListaSolicitudesActivity.this);
                builder.setCancelable(false);
                builder.setMessage("Hemos detectado inconvenientes en el dispositivo y Ktaxi Conductor no funciona correctamente.")
                        .setTitle("Error")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (dialogCorazon != null && dialogCorazon.isShowing())
                                    dialogCorazon.dismiss();
                                if (isLanzarError) {
                                    Log.e("ERROR: ", null);
                                }

                            }
                        });
                dialogCorazon = builder.create();
                dialogCorazon.show();
                ;
            }
        });
    }


}