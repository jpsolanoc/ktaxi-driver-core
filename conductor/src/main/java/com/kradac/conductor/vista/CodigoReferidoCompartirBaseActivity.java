package com.kradac.conductor.vista;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.adaptadoreslista.CodigoReferidoAdapter;
import com.kradac.conductor.adaptadoreslista.InsigniaAdapter;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.modelo.CodigoReferido;
import com.kradac.conductor.modelo.Insignia;
import com.kradac.conductor.modelo.ResponseListarCodigosReferido;
import com.kradac.conductor.presentador.CanjearCodigoReferidoRequest;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CodigoReferidoCompartirBaseActivity extends BaseActivity {


    SharedPreferences spLogin;
    @BindView(R2.id.tvTitulo)
    TextView tvTitulo;
    @BindView(R2.id.tvMensaje)
    TextView tvMensaje;
    @BindView(R2.id.llSinCodigos)
    LinearLayout llSinCodigos;
    @BindView(R2.id.tvTituloCodigo)
    TextView tvTituloCodigo;
    @BindView(R2.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R2.id.tvTituloReferidos)
    TextView tvTituloReferidos;
    @BindView(R2.id.recyclerViewInsignias)
    RecyclerView recyclerViewInsignias;
    @BindView(R2.id.llCodigos)
    LinearLayout llCodigos;
    @BindView(R2.id.appbar)
    Toolbar appbar;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codigo_referido_compartir);
        spLogin = getSharedPreferences(VariablesGlobales.LOGIN, Context.MODE_PRIVATE);
        ButterKnife.bind(this);
        setSupportActionBar(appbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle("Referido");
            ab.setDisplayHomeAsUpEnabled(true);
        }
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerViewInsignias.setLayoutManager(new LinearLayoutManager(this));
        obtenerCodigos();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // ID del boton
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void obtenerCodigos() {
        mostrarEspera(getString(R.string.msg_consultando_codigos));
        CanjearCodigoReferidoRequest codigoReferidoRequest = new CanjearCodigoReferidoRequest();
        codigoReferidoRequest.listarCodigos(spLogin.getInt("idUsuario", 0), new CanjearCodigoReferidoRequest.ListarCodigoReferidoListerner() {
            @Override
            public void onSuccess(ResponseListarCodigosReferido respuesta) {
                ocultarEspera();
                llSinCodigos.setVisibility(View.VISIBLE);
                llCodigos.setVisibility(View.GONE);

                if (respuesta.getEn() == 1) {
                    tvTitulo.setText(respuesta.getTitulo());
                    tvMensaje.setText(respuesta.getMensaje());
                    tvTituloCodigo.setText(respuesta.getLabel());
                    if (respuesta.getlC().size() > 0) {
                        llSinCodigos.setVisibility(View.GONE);
                        llCodigos.setVisibility(View.VISIBLE);
                    }
                    CodigoReferidoAdapter adapter = new CodigoReferidoAdapter(respuesta.getlC(), new CodigoReferidoAdapter.CodigoReferidoListener() {
                        @Override
                        public void onClick(final CodigoReferido codigoReferido) {
                            // String array for alert dialog multi choice items
                            String[] opciones = new String[]{
                                    "CLIENTE",
                                    "CONDUCTOR",
                            };
                            AlertDialog.Builder builder = new AlertDialog.Builder(CodigoReferidoCompartirBaseActivity.this);
                            builder.setTitle("REFERIR")
                                    .setItems(opciones, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (which == 0) {
                                                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                                sharingIntent.setType("text/plain");
                                                sharingIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.str_codigo_referido_solicitud) + " " + "https://play.google.com/store/apps/details?id=com.kradac.ktaxi&referrer=" + codigoReferido.getCodigo());
                                                sharingIntent.putExtra(Intent.EXTRA_HTML_TEXT, "https://play.google.com/store/apps/details?id=com.kradac.ktaxi&referrer=" + codigoReferido.getCodigo());
                                                startActivity(Intent.createChooser(sharingIntent, getString(R.string.str_codigo_referido)));
                                            } else {
                                                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                                sharingIntent.setType("text/plain");
                                                sharingIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.str_codigo_referido_solicitud_conductor) + " " + "https://play.google.com/store/apps/details?id=com.kradac.ktaxy_driver&referrer=" + codigoReferido.getCodigo());
                                                sharingIntent.putExtra(Intent.EXTRA_HTML_TEXT, "https://play.google.com/store/apps/details?id=com.kradac.ktaxy_driver&referrer=" + codigoReferido.getCodigo());
                                                startActivity(Intent.createChooser(sharingIntent, getString(R.string.str_codigo_referido)));
                                            }
                                            dialog.dismiss();
                                        }
                                    })
                                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                            builder.show();

                        }
                    }, CodigoReferidoCompartirBaseActivity.this);
                    recyclerView.setAdapter(adapter);


                    InsigniaAdapter adapterInsiginas = new InsigniaAdapter(respuesta.getlI(), getApplicationContext(), new InsigniaAdapter.OnItemClick() {
                        @Override
                        public void OnClickItem(Insignia insignia) {
                            Intent intent = new Intent(CodigoReferidoCompartirBaseActivity.this, DetalleInsigniaActivity.class);
                            intent.putExtra("insignia", insignia);
                            startActivity(intent);
                        }
                    });
                    recyclerViewInsignias.setAdapter(adapterInsiginas);

                    if (respuesta.getlI().size() == 0) {
                        tvTituloReferidos.setVisibility(View.GONE);
                    }
                } else {
                    Toast.makeText(CodigoReferidoCompartirBaseActivity.this, respuesta.getM(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onException() {
                ocultarEspera();
                Toast.makeText(CodigoReferidoCompartirBaseActivity.this, "No se pudo cargar los resultados.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }


}
