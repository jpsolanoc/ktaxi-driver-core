package com.kradac.conductor.vista;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.modelo.ConfiguracionServidor;
import com.kradac.conductor.modelo.Insignia;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetalleInsigniaActivity extends AppCompatActivity {


    @BindView(R2.id.content)
    LinearLayout content;
    @BindView(R2.id.ivInsignia)
    ImageView ivInsignia;
    @BindView(R2.id.tvTitulo)
    TextView tvTitulo;
    @BindView(R2.id.tvMensaje)
    TextView tvMensaje;

    private ConfiguracionServidor configuracionServidor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_insignia);
        ButterKnife.bind(this);

        Insignia insignia = (Insignia) getIntent().getSerializableExtra("insignia");
        tvTitulo.setText(insignia.getNumero() + " " + insignia.getNombre());
        tvMensaje.setText(insignia.getMensajeInsignia());
        ImageLoader imageLoader = ImageLoader.getInstance();
        SharedPreferences spConfigServerNew = getSharedPreferences(VariablesGlobales.CONFIG_IP_NEW, Context.MODE_PRIVATE);
        configuracionServidor = ConfiguracionServidor.createConfiguracionServidor(spConfigServerNew.getString(VariablesGlobales.CONFIG_IP_NEW, VariablesGlobales.CONFIGURACION_INICIAL));
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        cargarImagen(ivInsignia, configuracionServidor.getDns().getImgC() + "uploads/insignias/" + insignia.getInsignia());
    }

    @OnClick(R2.id.content)
    public void onViewClicked() {
        finish();
    }

    private void cargarImagen(final ImageView imageView, String url) {
        DisplayImageOptions options = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.RGB_565).cacheOnDisk(true).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
        imageLoader.displayImage(url, imageView, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                imageView.setImageBitmap(loadedImage);
            }
        });
    }
}
