package com.kradac.conductor.vista;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NavegadorWeb extends AppCompatActivity {

    @BindView(R2.id.web_view_info)
    WebView webViewInfo;
    @BindView(R2.id.appbar)
    Toolbar appbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegador_web);
        ButterKnife.bind(this);
        setSupportActionBar(appbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
            ab.setTitle("Información app.");
        }
        WebSettings webSettings = webViewInfo.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webViewInfo.setWebViewClient(new WebViewClient());
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            webViewInfo.loadUrl(bundle.getString("pagina"));
        } else {
            webViewInfo.loadUrl("https://www.ktaxi.com.ec/");
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: // ID del boton
                finish();
                overridePendingTransition(R.anim.left_in, R.anim.left_out);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (webViewInfo.canGoBack()) {
                        webViewInfo.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Utilidades().hideFloatingView(this);
    }
}
