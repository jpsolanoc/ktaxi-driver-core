package com.kradac.conductor.vista;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.kradac.conductor.R;
import com.kradac.conductor.extras.CameraPreview;
import com.kradac.conductor.extras.TratarImagenesGiro;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CamaraActivity extends AppCompatActivity {

    private Camera mCamera;
    private CameraPreview mPreview;
    private String rutaFoto;

    private String nameFoto() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
            String currentTimeStamp = dateFormat.format(new Date());
            return "K_img_" + currentTimeStamp + ".jpg";
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            FileOutputStream outStream = null;
            try {
                String dir_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
                rutaFoto = dir_path.concat(nameFoto());
                outStream = new FileOutputStream(rutaFoto);
                outStream.write(data);
                outStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                camera.stopPreview();
                camera.release();
                camera = null;
                Uri uri = Uri.parse(rutaFoto);
                Bitmap bitmap = null;
                try {
                    Bitmap decodeFile = BitmapFactory.decodeFile(rutaFoto);
                    bitmap = new TratarImagenesGiro().handleSamplingAndRotationBitmap(CamaraActivity.this, getImageUri(decodeFile));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Toast.makeText(CamaraActivity.this, "Guardando imagen", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(CamaraActivity.this, FormularioDenunciaActivity.class);
                intent.putExtra("ruta", persistImage(bitmap));
                startActivity(intent);
                finish();
            }
        }
    };

    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        String path = MediaStore.Images.Media.insertImage(getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camara);
        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        mPreview = new CameraPreview(this, mCamera);

        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
        Button captureButton = (Button) findViewById(R.id.button_capture);
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCamera.takePicture(null, null, mPicture);
                    }
                }
        );
    }

    private String persistImage(Bitmap bitmap) {
        File imageFile = null;
        try {
            imageFile = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile.getAbsolutePath();
    }

    private File createImageFile() throws IOException {
        String imageFileName = "Ktaxi_conductor" + new Date().getTime();
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );
        return image;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                mCamera.stopPreview();
                mCamera.release();
                mCamera = null;
                finish();
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
