package com.kradac.conductor.vista;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.kradac.conductor.fragment.CompraServicios;
import com.kradac.conductor.fragment.CompraServiciosTarjetaCredito;
import com.kradac.conductor.fragment.SaldoKtaxi;
import com.kradac.conductor.fragment.SaldoTarjetaCredito;
import com.kradac.conductor.fragment.TransaccionesSaldoKtaxiFragment;
import com.kradac.conductor.fragment.TransaccionesTarjetaCreditoKtaxiFragment;

public class TarjetaCreditoPager extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    private SaldoTarjetaCredito fragmentSaldoKtaxi;
    //    private CompraServiciosTarjetaCredito fragmentCompraServicio;
    private TransaccionesTarjetaCreditoKtaxiFragment transaccionesSaldoKtaxiFragment;

    public TarjetaCreditoPager(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                fragmentSaldoKtaxi = new SaldoTarjetaCredito();
//                fragmentCompraServicio = null;
                transaccionesSaldoKtaxiFragment = null;
                return fragmentSaldoKtaxi;
//            case 1:
//                fragmentSaldoKtaxi = null;
//                transaccionesSaldoKtaxiFragment = null;
//                fragmentCompraServicio = new CompraServiciosTarjetaCredito();
//                return fragmentCompraServicio;
            case 1:
                fragmentSaldoKtaxi = null;
                transaccionesSaldoKtaxiFragment = new TransaccionesTarjetaCreditoKtaxiFragment();
                return transaccionesSaldoKtaxiFragment;
            default:
                return null;
        }
    }


    public SaldoTarjetaCredito getFragmentSaldoKtaxi() {
        return fragmentSaldoKtaxi;
    }


    public TransaccionesTarjetaCreditoKtaxiFragment getTransaccionesSaldoKtaxiFragment() {
        return transaccionesSaldoKtaxiFragment;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
