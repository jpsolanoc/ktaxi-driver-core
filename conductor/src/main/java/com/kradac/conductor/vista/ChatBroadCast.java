package com.kradac.conductor.vista;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.kradac.conductor.R;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.interfaces.OnHandleEnviarAudio;
import com.kradac.conductor.interfaces.onComunicaionMensajesBroadCast;
import com.kradac.conductor.modelo.ChatMessage;
import com.kradac.conductor.service.ServicioSockets;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class ChatBroadCast extends BaseConexionService implements onComunicaionMensajesBroadCast,
        OnHandleEnviarAudio {

    public ArrayList<ChatMessage>chatCallCenter,chatBroadCastEmpresa,chatBroadCast;
    com.kradac.conductor.fragment.PagerAdapter adapter;

    @Override
    public void conexionCompleta() {
        super.conexionCompleta();
        registrarAcitvityServer(this, "ChatActivityBroadCast");
        servicioSocket.obtenerMensajesCallCenter();
        servicioSocket.obtenerMensajesBroadCastEmpresa();
        servicioSocket.obtenerMensajesBroadCast();
    }

    public ChatBroadCast() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_broad_cast);
        ButterKnife.bind(this);
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("CALL CENTER"));
        tabLayout.addTab(tabLayout.newTab().setText("EMPRESA"));
        tabLayout.addTab(tabLayout.newTab().setText("TODOS"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = findViewById(R.id.pager);
        adapter = new com.kradac.conductor.fragment.PagerAdapter
                (getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        registrarAcitvityServer(this, "ChatActivityBroadCast");
        new Utilidades().hideFloatingView(this);
    }

    @Override
    public void onGrabarEnviarAudio(String nameAudio,int fragment) {
        switch (fragment){
            case 1:
                servicioSocket.enviarAudioCallCenter(nameAudio);
                break;
            case 2:
                servicioSocket.enviarAudioBroasCastEmpresa(nameAudio);
                break;
            case 3:
                servicioSocket.enviarAudioBroasCast(nameAudio);
                break;
            default:
                break;
        }
    }

    @Override
    public ServicioSockets onServicioSocket() {
        return servicioSocket;
    }

    @Override
    public void mensajesCallCenter(ArrayList<ChatMessage> chatHistory) {
        chatCallCenter = chatHistory;
        if (adapter.getTab1()!=null){
            adapter.getTab1().mensajesUsuarioCallCenter();
        }

    }

    @Override
    public void mensajesBroadCastEmpresa(ArrayList<ChatMessage> chatHistory) {
        chatBroadCastEmpresa = chatHistory;
        if (adapter.getTab2()!=null){
            adapter.getTab2().mensajesUsuarioEmpresa();
        }

    }

    @Override
    public void mensajesBroadCast(ArrayList<ChatMessage> chatHistory) {
        chatBroadCast = chatHistory;
        if (adapter.getTab3()!=null){
            adapter.getTab3().mensajesUsuarioTodos();
        }

    }

    @Override
    public ArrayList<ChatMessage> getChatCallCenter() {
        return chatCallCenter;
    }
    @Override
    public ArrayList<ChatMessage> getChatBroadCastEmpresa() {
        return chatBroadCastEmpresa;
    }
    @Override
    public ArrayList<ChatMessage> getChatBroadCast() {
        return chatBroadCast;
    }
}
