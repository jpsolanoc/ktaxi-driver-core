package com.kradac.conductor.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.kradac.conductor.R;
import com.kradac.conductor.R2;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.modelo.Solicitud;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DialogInformacionUsuario extends BottomSheetDialogFragment {
    @BindView(R2.id.tv_encabezado)
    TextView tvEncabezado;
    @BindView(R2.id.tv_codigo)
    TextView tvCodigo;
    @BindView(R2.id.tvCliente)
    TextView tvCliente;
    @BindView(R2.id.tv_lugar_inicio)
    TextView tvLugarInicio;
    @BindView(R2.id.tv_lugar_compra)
    TextView tvLugarCompra;
    @BindView(R2.id.tv_info_pedido_enca)
    TextView tvInfoPedidoEnca;
    @BindView(R2.id.tv_info_pedido)
    TextView tvInfoPedido;
    @BindView(R2.id.layaut_info_pedido)
    LinearLayout layautInfoPedido;
    @BindView(R2.id.tv_calle_principal)
    TextView tvCallePrincipal;
    @BindView(R2.id.etCallePrincipal)
    TextView etCallePrincipal;
    @BindView(R2.id.tv_interseccion)
    TextView tvInterseccion;
    @BindView(R2.id.et_intersecion)
    TextView etIntersecion;
    @BindView(R2.id.tv_barrio)
    TextView tvBarrio;
    @BindView(R2.id.et_barrio)
    TextView etBarrio;
    @BindView(R2.id.tv_referencia)
    TextView tvReferencia;
    @BindView(R2.id.et_referencia)
    TextView etReferencia;
    @BindView(R2.id.tv_codigo_dos)
    TextView tvCodigoDos;
    @BindView(R2.id.fondoLayout)
    ScrollView fondoLayout;
    Unbinder unbinder;

    private Solicitud solicitudSeleccionada;
    private String dataConfig;
    private Utilidades utilidades;

    public DialogInformacionUsuario() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            solicitudSeleccionada = bundle.getParcelable("solicitud");
            dataConfig = bundle.getString("datosConfiguracion");
        }
        utilidades = new Utilidades();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.dialog_informacion_solicitud, container, false);
        unbinder = ButterKnife.bind(this, view);
        cargarData();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public boolean isCampoVacio(TextView textView) {
        return textView.getText().toString().trim().equals("");
    }

    public void cargarData() {
        tvCodigo.setVisibility(View.VISIBLE);
        tvCodigoDos.setVisibility(View.VISIBLE);
        if (solicitudSeleccionada.getIdPedido() != 0) {
            tvCodigo.setText(String.valueOf(solicitudSeleccionada.getIdPedido()));
            tvCodigoDos.setText(String.valueOf(solicitudSeleccionada.getIdPedido()));
        } else {
            tvCodigo.setText(String.valueOf(solicitudSeleccionada.getIdSolicitud()));
            tvCodigoDos.setText(String.valueOf(solicitudSeleccionada.getIdSolicitud()));
        }
        if (solicitudSeleccionada.getIdSolicitud() == 0) {
            tvCallePrincipal.setText(getString(R.string.str_direccion));
            fondoLayout.setBackgroundResource(R.color.plomo_cambio);
        }
        String datosConfiguracion[] = new String[4];
        datosConfiguracion[0] = "Calle Principal";
        datosConfiguracion[1] = "Intersección";
        datosConfiguracion[2] = "Barrio";
        datosConfiguracion[3] = "Referencia";
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 5: //calle principal
                                if (objetoConfig.getInt("h") == 1) {
                                    datosConfiguracion[0] = objetoConfig.getString("nb");
                                }
                                break;
                            case 6: //Intersección
                                if (objetoConfig.getInt("h") == 1) {
                                    datosConfiguracion[1] = objetoConfig.getString("nb");
                                }
                                break;
                            case 7: //barrio
                                if (objetoConfig.getInt("h") == 1) {
                                    datosConfiguracion[2] = objetoConfig.getString("nb");
                                }
                                break;
                            case 8: //Referencia
                                if (objetoConfig.getInt("h") == 1) {
                                    datosConfiguracion[3] = objetoConfig.getString("nb");
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        tvCallePrincipal.setText(datosConfiguracion[0]);
        tvReferencia.setText(datosConfiguracion[1]);
        tvBarrio.setText(datosConfiguracion[2]);
        tvReferencia.setText(datosConfiguracion[3]);
        tvCliente.setText(solicitudSeleccionada.getNombresCliente());
        etCallePrincipal.setText(solicitudSeleccionada.getCallePrincipal());
        etIntersecion.setText(solicitudSeleccionada.getCalleSecundaria());
        etBarrio.setText(solicitudSeleccionada.getBarrio());
        etReferencia.setText(solicitudSeleccionada.getReferencia());
        if (isCampoVacio(etCallePrincipal) || !utilidades.ocularMostrarCampos(solicitudSeleccionada.getCallePrincipal())) {
            etCallePrincipal.setVisibility(View.GONE);
            tvCallePrincipal.setVisibility(View.GONE);
        }
        if (isCampoVacio(etIntersecion) || !utilidades.ocularMostrarCampos(solicitudSeleccionada.getCalleSecundaria())) {
            etIntersecion.setVisibility(View.GONE);
            tvInterseccion.setVisibility(View.GONE);
        }
        if (isCampoVacio(etBarrio) || !utilidades.ocularMostrarCampos(solicitudSeleccionada.getBarrio())) {
            etBarrio.setVisibility(View.GONE);
            tvBarrio.setVisibility(View.GONE);
        }
        if (isCampoVacio(etReferencia) || !utilidades.ocularMostrarCampos(solicitudSeleccionada.getReferencia())) {
            etReferencia.setVisibility(View.GONE);
            tvReferencia.setVisibility(View.GONE);
        }
        tvInfoPedido.setText(solicitudSeleccionada.getPedido());
        tvLugarCompra.setText(solicitudSeleccionada.getLugarCompra());
        if (solicitudSeleccionada.getIdPedido() != 0) {
            if (solicitudSeleccionada.getT() == 1) {
                layautInfoPedido.setVisibility(View.VISIBLE);
                tvEncabezado.setText(getString(R.string.str_solicitud_compra));
            } else {
                layautInfoPedido.setVisibility(View.VISIBLE);
                //tvEncabezado.setText(getString(R.string.str_solicitud_encomienda));
                tvEncabezado.setText("prueba");
                tvLugarInicio.setText(getString(R.string.str_retiro_encomienda));
                tvInfoPedidoEnca.setText(R.string.str_informacion);
            }

        } else {
            layautInfoPedido.setVisibility(View.GONE);
            tvEncabezado.setText(getString(R.string.str_solicitud_taxi));
        }
    }
}
