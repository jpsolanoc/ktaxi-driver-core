package com.kradac.conductor.dialog;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kradac.conductor.R;
import com.kradac.conductor.extras.Utilidades;
import com.kradac.conductor.modelo.Destino;
import com.kradac.conductor.modelo.Solicitud;
import com.kradac.conductor.service.ServicioSockets;
import com.kradac.conductor.vista.MapaBaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.mapbox.mapboxsdk.style.layers.Property.NONE;

public class DialogosGenericos {

    private static final String TAG = DialogosGenericos.class.getSimpleName();

    public interface OnComunicacionTerminosCondiciones {
        void respuestaTerminosCondiciones(boolean isAcepto);
    }

    public void mostrarAcercaDe(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewInformacion = inflater.inflate(R.layout.dialog_informacion_aplicacion, null, false);
        final TextView tvInformacionVersion = (TextView) viewInformacion.findViewById(R.id.tv_info_version);
        tvInformacionVersion.setText("Version: ".concat(new Utilidades().obtenerVersion(context)));
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(viewInformacion);
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.aceptar),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    public void terminosCondiciones(final Context context, final OnComunicacionTerminosCondiciones onComunicacionTerminosCondiciones) {
        Utilidades utilidades = new Utilidades();
        Dialog dialogTerminosCondicones = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewInformacion = inflater.inflate(R.layout.dialog_terminos_condiciones, null, false);
        final CheckBox chkTerminoCondiciones = viewInformacion.findViewById(R.id.chk_termino_condiciones);
        TextView tvTerminoCondiciones = viewInformacion.findViewById(R.id.tv_terminos_condiciones);
        utilidades.subrayarTextoConAcciones(tvTerminoCondiciones.getText().toString(), tvTerminoCondiciones);
        tvTerminoCondiciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("http://www.kradac.com/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                context.startActivity(intent);
            }
        });
        viewInformacion.findViewById(R.id.btn_aceptar_terminos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkTerminoCondiciones.isChecked()) {
                    onComunicacionTerminosCondiciones.respuestaTerminosCondiciones(true);
                } else {
                    Toast.makeText(context, "Usted debe seleccionar que acepta los términos y condiciones para continuar.", Toast.LENGTH_LONG).show();
                }
            }
        });
        viewInformacion.findViewById(R.id.btn_cancelar_terminos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onComunicacionTerminosCondiciones.respuestaTerminosCondiciones(false);
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(viewInformacion);
        builder.setCancelable(false);
        dialogTerminosCondicones = builder.create();
        dialogTerminosCondicones.show();
    }

    public interface OnComunicacionCliente {
        void mostrarCliente();
    }

    public SupportMapFragment mapFragment;

    public Dialog mostrarInformaciosnUsuario(Activity activity, Context context, Solicitud solicitudSeleccionada, String dataConfig, final OnComunicacionCliente onComunicacionCliente) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View viewInformacion = inflater.inflate(R.layout.dialog_informacion_solicitud, null, false);
        final TextView cliente = viewInformacion.findViewById(R.id.tvCliente);
        final TextView tvCodigo = viewInformacion.findViewById(R.id.tv_codigo);
        final TextView tvCodigoDos = viewInformacion.findViewById(R.id.tv_codigo_dos);
        final TextView tv_encabezado = viewInformacion.findViewById(R.id.tv_encabezado);
        final LinearLayout fondo = viewInformacion.findViewById(R.id.fondoLayout);
        final TextView callePrincipalL = viewInformacion.findViewById(R.id.tv_calle_principal);
        final TextView callePrincipal = viewInformacion.findViewById(R.id.etCallePrincipal);

        final TextView interseccion = viewInformacion.findViewById(R.id.et_intersecion);
        final TextView interseccionL = viewInformacion.findViewById(R.id.tv_interseccion);

        final TextView barrio = viewInformacion.findViewById(R.id.et_barrio);
        final TextView barrioL = viewInformacion.findViewById(R.id.tv_barrio);
        final TextView referencia = viewInformacion.findViewById(R.id.et_referencia);
        final TextView referenciaL = viewInformacion.findViewById(R.id.tv_referencia);
        final LinearLayout layout_info_pedido = viewInformacion.findViewById(R.id.layaut_info_pedido);
        final TextView tvLugarCompra = viewInformacion.findViewById(R.id.tv_lugar_compra);
        final TextView tvLugarInicio = viewInformacion.findViewById(R.id.tv_lugar_inicio);
        final TextView tvInfoPedidoEncabezado = viewInformacion.findViewById(R.id.tv_info_pedido_enca);
        final TextView info_pedido = viewInformacion.findViewById(R.id.tv_info_pedido);
        final Button btnUbicarCliente = viewInformacion.findViewById(R.id.btn_ubicar_mapa);
        LinearLayout lyDestino = viewInformacion.findViewById(R.id.ly_destino_ld);

        if (solicitudSeleccionada.getlD() != null && solicitudSeleccionada.getlD().size() > 0) {

            TextView tvDestinoBarrio = viewInformacion.findViewById(R.id.tv_destino_barrio);
            TextView tvDestinoCallePrincipal = viewInformacion.findViewById(R.id.tv_destino_calle_principal);
            TextView tvDestinoCalleSecundaria = viewInformacion.findViewById(R.id.tv_destino_barrio_calle_secundaria);
            TextView tvDestinoReferencia = viewInformacion.findViewById(R.id.tv_destino_refencia);
            Destino destino = solicitudSeleccionada.getlD().get(0);
            String barrioD = destino.getDesBar().isEmpty() ? "" : ("Barrio. ".concat(destino.getDesBar()));
            String cpD = destino.getDesCp().isEmpty() ? "" : (" CP. ".concat(destino.getDesCp()));
            String csD = destino.getDesCs().isEmpty() ? "" : (" CS. ".concat(destino.getDesCs()));
            String rfD = destino.getDesRef().isEmpty() ? "" : (" RF. ".concat(destino.getDesRef()));
            String destinoText = barrioD.concat(cpD).concat(csD).concat(rfD);
            tvDestinoBarrio.setText(destinoText);
            tvDestinoCallePrincipal.setText(destino.getDesCp());
            tvDestinoCalleSecundaria.setText(destino.getDesCs());
            tvDestinoReferencia.setText(destino.getDesRef());
            TextView tvCostoDestino = viewInformacion.findViewById(R.id.tv_destino_costo);
            TextView tvTiempoDestino = viewInformacion.findViewById(R.id.tv_destino_tiempo);
            TextView tvDistanciaDestino = viewInformacion.findViewById(R.id.tv_destino_distancia);
            tvCostoDestino.setText(destino.getDesC().toString());
            tvTiempoDestino.setText(destino.getDesT());
            tvDistanciaDestino.setText(destino.getDesDis().toString().concat(" Km"));
        } else {
            lyDestino.setVisibility(View.GONE);
        }
        mapFragment = ((SupportMapFragment) ((AppCompatActivity) activity).getSupportFragmentManager().findFragmentById(R.id.map_dialog));
        mapFragment.getView().setVisibility(View.GONE);
        if (mapFragment != null) {
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap map) {
                    LatLng locationCliente = new LatLng(solicitudSeleccionada.getLatitud(), solicitudSeleccionada.getLongitud());
                    MarkerOptions mapketOption = new MarkerOptions()
                            .position(locationCliente)
                            .title("Cliente")
                            .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                    .decodeResource(context.getResources(),
                                            R.mipmap.ic_user_map_osm)))
                            .snippet(solicitudSeleccionada.getNombresCliente());
                    map.addMarker(mapketOption);
                    map.moveCamera(CameraUpdateFactory.newCameraPosition(
                            new CameraPosition.Builder()
                                    .target(locationCliente)
                                    .zoom(17)
                                    .tilt(0)
                                    .build()));
                    map.getUiSettings().setZoomGesturesEnabled(false);
                    map.getUiSettings().setCompassEnabled(false);
                    map.getUiSettings().setZoomControlsEnabled(false);
                    map.getUiSettings().setMapToolbarEnabled(false);

                }
            });
        } else {
            Toast.makeText(context, "Error - Map Fragment was null!!", Toast.LENGTH_SHORT).show();
        }


        tvCodigo.setVisibility(View.VISIBLE);
        tvCodigoDos.setVisibility(View.VISIBLE);
        if (solicitudSeleccionada.getIdPedido() != 0) {
            tvCodigo.setText(String.valueOf(solicitudSeleccionada.getIdPedido()));
            tvCodigoDos.setText(String.valueOf(solicitudSeleccionada.getIdPedido()));
        } else {
            tvCodigo.setText(String.valueOf(solicitudSeleccionada.getIdSolicitud()));
            tvCodigoDos.setText(String.valueOf(solicitudSeleccionada.getIdSolicitud()));
        }

        if (solicitudSeleccionada.getIdSolicitud() == 0) {
            callePrincipalL.setText(context.getString(R.string.str_direccion));
            fondo.setBackgroundResource(R.color.plomo_cambio);
        }

        viewInformacion.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                Log.e(TAG, "onViewAttachedToWindow: ");
            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                Log.e(TAG, "onViewDetachedFromWindow: ");
                try {
                    ((AppCompatActivity) activity).getSupportFragmentManager().beginTransaction().remove(mapFragment).commit();
                } catch (IllegalStateException i) {
                    Log.e(TAG, "onViewDetachedFromWindow: " + i.getMessage());
                }

            }
        });
        String datosConfiguracion[] = new String[4];
        datosConfiguracion[0] = "Calle Principal";
        datosConfiguracion[1] = "Intersección";
        datosConfiguracion[2] = "Barrio";
        datosConfiguracion[3] = "Referencia";
        if (dataConfig.length() > 0) {
            try {
                JSONArray jsonArray = new JSONArray(dataConfig);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject objetoConfig = jsonArray.getJSONObject(i);
                    if (objetoConfig.has("id")) {
                        switch (objetoConfig.getInt("id")) {
                            case 5: //calle principal
                                if (objetoConfig.getInt("h") == 1) {
                                    datosConfiguracion[0] = objetoConfig.getString("nb");
                                }
                                break;
                            case 6: //Intersección
                                if (objetoConfig.getInt("h") == 1) {
                                    datosConfiguracion[1] = objetoConfig.getString("nb");
                                }
                                break;
                            case 7: //barrio
                                if (objetoConfig.getInt("h") == 1) {
                                    datosConfiguracion[2] = objetoConfig.getString("nb");
                                }
                                break;
                            case 8: //Referencia
                                if (objetoConfig.getInt("h") == 1) {
                                    datosConfiguracion[3] = objetoConfig.getString("nb");
                                }
                                break;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        callePrincipalL.setText(datosConfiguracion[0]);
        interseccionL.setText(datosConfiguracion[1]);
        barrioL.setText(datosConfiguracion[2]);
        referenciaL.setText(datosConfiguracion[3]);
        cliente.setText(solicitudSeleccionada.getNombresCliente());
        callePrincipal.setText(solicitudSeleccionada.getCallePrincipal());
        interseccion.setText(solicitudSeleccionada.getCalleSecundaria());
        barrio.setText(solicitudSeleccionada.getBarrio());
        referencia.setText(solicitudSeleccionada.getReferencia());

        if (solicitudSeleccionada.getCalleSecundaria().isEmpty() ||
                solicitudSeleccionada.getCalleSecundaria() == null ||
                solicitudSeleccionada.getCalleSecundaria().equals("NONE")) {
            interseccion.setVisibility(View.GONE);
            interseccionL.setVisibility(View.GONE);
        }else{
            interseccion.setVisibility(View.VISIBLE);
            interseccionL.setVisibility(View.VISIBLE);
        }

        if (isCampoVacio(interseccion)&& interseccion.getText().toString().trim().equals("NONE")) {
            interseccion.setVisibility(View.GONE);
            interseccionL.setVisibility(View.GONE);
        }
        if (isCampoVacio(barrio)) {
            barrio.setVisibility(View.GONE);
            barrioL.setVisibility(View.GONE);
        }
        if (isCampoVacio(referencia)) {
            referencia.setVisibility(View.GONE);
            referenciaL.setVisibility(View.GONE);
        }

        btnUbicarCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onComunicacionCliente.mostrarCliente();
                ((AppCompatActivity) activity).getSupportFragmentManager().beginTransaction().remove(mapFragment).commit();
            }
        });
        info_pedido.setText(solicitudSeleccionada.getPedido());
        tvLugarCompra.setText(solicitudSeleccionada.getLugarCompra());
        if (solicitudSeleccionada.getIdPedido() != 0) {
            if (solicitudSeleccionada.getT() == 1) {
                layout_info_pedido.setVisibility(View.VISIBLE);
                tv_encabezado.setText(context.getString(R.string.str_solicitud_compra));

            } else {
                layout_info_pedido.setVisibility(View.VISIBLE);
                tv_encabezado.setText(context.getString(R.string.str_solicitud_encomienda));
                tvLugarInicio.setText(context.getString(R.string.str_retiro_encomienda));
                tvInfoPedidoEncabezado.setText(R.string.str_informacion);
            }

        } else {
            layout_info_pedido.setVisibility(View.GONE);
            tv_encabezado.setText(context.getString(R.string.str_solicitud_taxi));
        }

        builder.setView(viewInformacion);
        builder.setCancelable(true);
        return builder.create();
    }

    private boolean isCampoVacio(TextView textView) {
        return textView.getText().toString().trim().equals("");
    }

    public void dialogoContactoSoporte(Activity context, Utilidades utilidades) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        @SuppressLint("InflateParams") final View viewInformacion = inflater.inflate(R.layout.dialog_contactenos_soporte, null, false);
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final ImageView accion_salir = viewInformacion.findViewById(R.id.accion_salir);
        final ImageView imbWhatshapp = viewInformacion.findViewById(R.id.imb_whatshapp);
        final ImageView imbTelefono = viewInformacion.findViewById(R.id.imb_telefono);
        builder.setView(viewInformacion);
        builder.setCancelable(false);
        final Dialog finalDialog = builder.create();
        if (!context.isFinishing()) {
            finalDialog.show();
        }
        accion_salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalDialog.dismiss();
            }
        });
        imbWhatshapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numero = "0991503740";
                if (numero.length() == 10) {
                    if (utilidades.estaInstaladaAplicacion("com.whatsapp", context)) {
                        String url = "https://api.whatsapp.com/send?phone=".concat(utilidades.obtenerNumero(numero));
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        context.startActivity(i);
                    } else {
                        Toast.makeText(context, "Usted no tiene instalado whatsapp", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(context, "El número que intenta enviar el mensaje no es válido.", Toast.LENGTH_LONG).show();
                }
            }
        });
        imbTelefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CALL_PHONE}, MapaBaseActivity.MY_PERMISSIONS_REQUEST_CALL);
                } else {
                    context.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:+593991503740")));
                }
            }
        });
    }


    public void afirmarPosiblesSoliciudes(Activity context, ServicioSockets servicioSocket, Location locacionDefault) {
        AlertDialog.Builder dialogo = new AlertDialog.Builder(context);
        dialogo.setTitle("Información");
        dialogo.setMessage("¿Está seguro que desea enviar el aviso de posibles solicitudes?");
        dialogo.setCancelable(false);
        dialogo.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                if (locacionDefault != null) {
                    servicioSocket.enviarEvento(locacionDefault, 2, 0, 0);
                }
            }
        });
        dialogo.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                //codigo
                dialogo1.dismiss();
            }
        });
        if (!context.isFinishing()) {
            dialogo.show();
        }
    }


}
