package com.kradac.wigets.modelo;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.kradac.wigets.extra.VariablesPublicidad;

public class RespuestaPublicidad implements Parcelable {


    private B b;
    private int en;
    private String rutaRecurso;

    public String getRutaRecurso() {
        return rutaRecurso;
    }

    public void setRutaRecurso(String rutaRecurso) {
        this.rutaRecurso = rutaRecurso;
    }

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }

    public int getEn() {
        return en;
    }

    public void setEn(int en) {
        this.en = en;
    }

    public static class B implements Parcelable {
        private String recurso;
        private int tipo;
        private int cerrado;
        private int forma;
        private int tiempo;
        private int idBannerConductor;

        public String getRecurso() {
            return recurso;
        }

        public void setRecurso(String recurso) {
            this.recurso = recurso;
        }

        public int getTipo() {
            return tipo;
        }

        public void setTipo(int tipo) {
            this.tipo = tipo;
        }

        public int getCerrado() {
            return cerrado;
        }

        public void setCerrado(int cerrado) {
            this.cerrado = cerrado;
        }

        public int getForma() {
            return forma;
        }

        public void setForma(int forma) {
            this.forma = forma;
        }

        public int getTiempo() {
            return tiempo;
        }

        public void setTiempo(int tiempo) {
            this.tiempo = tiempo;
        }

        public int getIdBannerConductor() {
            return idBannerConductor;
        }

        public void setIdBannerConductor(int idBannerConductor) {
            this.idBannerConductor = idBannerConductor;
        }

        @Override
        public String toString() {
            return "B{" +
                    "recurso='" + recurso + '\'' +
                    ", tipo=" + tipo +
                    ", cerrado=" + cerrado +
                    ", forma=" + forma +
                    ", tiempo=" + tiempo +
                    ", idBannerConductor=" + idBannerConductor +
                    '}';
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.recurso);
            dest.writeInt(this.tipo);
            dest.writeInt(this.cerrado);
            dest.writeInt(this.forma);
            dest.writeInt(this.tiempo);
            dest.writeInt(this.idBannerConductor);
        }

        public B() {
        }

        protected B(Parcel in) {
            this.recurso = in.readString();
            this.tipo = in.readInt();
            this.cerrado = in.readInt();
            this.forma = in.readInt();
            this.tiempo = in.readInt();
            this.idBannerConductor = in.readInt();
        }

        public static final Creator<B> CREATOR = new Creator<B>() {
            @Override
            public B createFromParcel(Parcel source) {
                return new B(source);
            }

            @Override
            public B[] newArray(int size) {
                return new B[size];
            }
        };
    }


    public static RespuestaPublicidad createRespuestPublicidad(String respuestaPublicidad) {
        Gson obGson = new Gson();
        return obGson.fromJson(respuestaPublicidad, RespuestaPublicidad.class);
    }

    public static String jsonRespuestaPublicidad(RespuestaPublicidad respuestaPublicidad) {
        Gson gson = new Gson();
        return gson.toJson(respuestaPublicidad);
    }

    public static void guardarRespuestaPublicidad(String respuestaPublicidad, Context context) {
        SharedPreferences spSolicitud = context.getSharedPreferences(VariablesPublicidad.SP_RESPUESTA_PUBLICIDAD, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = spSolicitud.edit();
        editor.putString(VariablesPublicidad.SP_DATA_RESPUESTA_PUBLCIDAD, respuestaPublicidad);
        editor.apply();
    }

    public static RespuestaPublicidad getRespuestaPublicidad(Context context) {
        SharedPreferences spSolicitud = context.getSharedPreferences(VariablesPublicidad.SP_RESPUESTA_PUBLICIDAD, Context.MODE_PRIVATE);
        return createRespuestPublicidad(spSolicitud.getString(VariablesPublicidad.SP_DATA_RESPUESTA_PUBLCIDAD, ""));
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.b, flags);
        dest.writeInt(this.en);
        dest.writeString(this.rutaRecurso);
    }

    public RespuestaPublicidad() {
    }

    protected RespuestaPublicidad(Parcel in) {
        this.b = in.readParcelable(B.class.getClassLoader());
        this.en = in.readInt();
        this.rutaRecurso = in.readString();
    }

    public static final Creator<RespuestaPublicidad> CREATOR = new Creator<RespuestaPublicidad>() {
        @Override
        public RespuestaPublicidad createFromParcel(Parcel source) {
            return new RespuestaPublicidad(source);
        }

        @Override
        public RespuestaPublicidad[] newArray(int size) {
            return new RespuestaPublicidad[size];
        }
    };
}
