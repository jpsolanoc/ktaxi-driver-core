package com.kradac.wigets.extra;

public class VariablesPublicidad {

    public static final int FORMA_DENTRO_PANTALLA = 1;
    public static final int FORMA_OTRA_PANTALLA = 2;

    public static final int CERRADO_BOTON_AL_CUMPLIR_TIEMPO = 1;
    public static final int CERRADO_AL_CUMPLIR_TIEMPO = 2;
    public static final int CERRADO_BOTON_SIEMPRE = 3;

    public static final int RECURSO_VIDEO = 1;
    public static final int RECURSO_IMAGEN = 2;
    public static final int RECURSO_GIF = 3;

    public static final int LIBRE_SIN_CARRERA = 1;
    public static final int LIBRE_CON_CARRERA = 2;
    public static final int OCUPADO_SIN_CARRERA = 3;
    public static final int AL_PRECIONAR_BOTON_ABORDO = 4;

    public static final String URL_SERVIDOR_PRODCUCCION = "https://ktaxifacilsegurorapido.kradac.com/";


    public static final String SP_RESPUESTA_PUBLICIDAD = "respuesta_publicidad";
    public static final String SP_DATA_RESPUESTA_PUBLCIDAD = "data_respuesta_publicidad";
}
