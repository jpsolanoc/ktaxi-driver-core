package com.kradac.wigets.extra;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.view.Surface;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;


public class Utilidades {

    public void cargarImagenPublicitaria(final ImageView imageView, String url, Context context, final ProgressBar progressBar) {
        DisplayImageOptions options = new DisplayImageOptions.Builder().bitmapConfig(Bitmap.Config.RGB_565).cacheOnDisk(true).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(context));
        imageLoader.displayImage(url, imageView, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                super.onLoadingComplete(imageUri, view, loadedImage);
                imageView.setImageBitmap(loadedImage);
                if (progressBar != null)
                    progressBar.setVisibility(View.GONE);
            }
        });
    }

    public boolean isVertical(Context context) {
        int rotacion = context.getResources().getConfiguration().orientation;
        if (rotacion == Surface.ROTATION_0 || rotacion == Surface.ROTATION_180) {
            return true;
        } else {
            return false;
        }
    }

    public String writeResponseBodyToDisk(ResponseBody body, int tipo) {
        try {
            String tipoArchivo = "publicidad.jpg";
            switch (tipo) {
                case 1:
                    tipoArchivo = "publicidad.mp4";
                    break;
                case 3:
                    tipoArchivo = "publicidad.gif";
                    break;
            }

            File futureStudioIconFile = new File(Environment.getExternalStorageDirectory() + File.separator + tipoArchivo);
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] fileReader = new byte[4096];
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);
                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                }
                outputStream.flush();
                return futureStudioIconFile.getAbsolutePath();
            } catch (IOException e) {
                return null;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return null;
        }
    }
}
