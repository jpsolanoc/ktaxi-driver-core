package com.kradac.wigets;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.VideoView;

import com.kradac.wigets.extra.Utilidades;
import com.kradac.wigets.extra.VariablesPublicidad;
import com.kradac.wigets.modelo.RespuestaPublicidad;
import com.kradac.wigets.presentador.PublicidadPresenter;


public class LayoutPublicitario extends BaseView {

    private ImageView imgPublicidad;
    private ImageView imbClose;
    private VideoView videoViewPublicidad;
    private TimeViewListener timeViewListener;
    private GifImageView gifImageView;

    private PublicidadPresenter publicidadPresenter;

    public GifImageView getGifImageView() {
        return gifImageView;
    }

    public void setGifImageView(GifImageView gifImageView) {
        this.gifImageView = gifImageView;
    }

    public void setTimeViewListener(TimeViewListener timeViewListener) {
        this.timeViewListener = timeViewListener;
    }

    public interface TimeViewListener {
        void onClosePublicidad();

        void otraPantalla(RespuestaPublicidad respuestaPublicidad);
    }

    public VideoView getVideoViewPublicidad() {
        return videoViewPublicidad;
    }

    public void setVideoViewPublicidad(VideoView videoViewPublicidad) {
        this.videoViewPublicidad = videoViewPublicidad;
    }

    public ImageView getImgPublicidad() {
        return imgPublicidad;
    }

    public void setImgPublicidad(ImageView imgPublicidad) {
        this.imgPublicidad = imgPublicidad;
    }

    public ImageView getImbClose() {
        return imbClose;
    }

    public void setImbClose(ImageView imbClose) {
        this.imbClose = imbClose;
    }

    public LayoutPublicitario(Context context) {
        super(context);
        init();
    }

    public LayoutPublicitario(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LayoutPublicitario(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        publicidadPresenter = new PublicidadPresenter();
        inflate(getContext(), R.layout.banert_publicidad, this);
        this.imgPublicidad = findViewById(R.id.img_publicidad);
        this.imbClose = findViewById(R.id.imb_close);
        this.videoViewPublicidad = findViewById(R.id.video_view_publicidad);
        this.gifImageView = findViewById(R.id.gif_image_view);
        this.imbClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                animarClickView(imbClose);
                if (timeViewListener != null) {
                    timeViewListener.onClosePublicidad();
                }
                videoViewPublicidad.pause();
                gifImageView.setVisibility(GONE);
                imgPublicidad.setVisibility(GONE);
                videoViewPublicidad.setVisibility(GONE);
                hide();
            }
        });
    }

    public void cargarPublicidad(final RespuestaPublicidad respuestaPublicidad) {
        if (respuestaPublicidad != null && respuestaPublicidad.getB() != null) {
            switch (respuestaPublicidad.getB().getTipo()) {
                case VariablesPublicidad.RECURSO_VIDEO:
                    imgPublicidad.setVisibility(View.GONE);
                    gifImageView.setVisibility(View.GONE);
                    publicidadPresenter.dowloadArchivo("https://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mp4",
//                    publicidadPresenter.dowloadArchivo(respuestaPublicidad.getB().getRecurso( ),
                            VariablesPublicidad.RECURSO_VIDEO, new PublicidadPresenter.OnComunicacionDescarPublicidad() {
                                @Override
                                public void respuesta(String ruta) {
                                    if (ruta != null && !ruta.isEmpty()) {
                                        respuestaPublicidad.setRutaRecurso(ruta);
                                        if (respuestaPublicidad.getB().getForma() == VariablesPublicidad.FORMA_DENTRO_PANTALLA) {
                                            cargarVideo(Uri.parse("file:" + respuestaPublicidad.getRutaRecurso()));
                                        } else {
                                            if (timeViewListener != null) {
                                                timeViewListener.otraPantalla(respuestaPublicidad);
                                            }
                                        }
                                    }

                                }
                            });
                    break;
                case VariablesPublicidad.RECURSO_IMAGEN:
                    gifImageView.setVisibility(View.GONE);
                    videoViewPublicidad.setVisibility(View.GONE);
                    if (respuestaPublicidad.getB().getForma() == VariablesPublicidad.FORMA_DENTRO_PANTALLA) {
//                        cargarImagen("https://image.ibb.co/fwAJz8/banner_pequen_o_quevedo.png");
                        if (respuestaPublicidad.getB().getRecurso() != null && !respuestaPublicidad.getB().getRecurso().isEmpty()) {
                            cargarImagen(respuestaPublicidad.getB().getRecurso());
                        }
                    } else {
                        if (timeViewListener != null) {
                            timeViewListener.otraPantalla(respuestaPublicidad);
                        }
                    }

                    break;
                case VariablesPublicidad.RECURSO_GIF:
                    gifImageView.setVisibility(View.GONE);
                    videoViewPublicidad.setVisibility(View.GONE);
//                    publicidadPresenter.dowloadArchivo("https://media3.giphy.com/media/kEKcOWl8RMLde/giphy.gif",
                    publicidadPresenter.dowloadArchivo(respuestaPublicidad.getB().getRecurso(),
                            VariablesPublicidad.RECURSO_GIF, new PublicidadPresenter.OnComunicacionDescarPublicidad() {
                                @Override
                                public void respuesta(String ruta) {
                                    if (ruta != null && !ruta.isEmpty()) {
                                        respuestaPublicidad.setRutaRecurso(ruta);
                                        if (respuestaPublicidad.getB().getForma() == VariablesPublicidad.FORMA_DENTRO_PANTALLA) {
                                            cargarGIF(Uri.parse("file:" + respuestaPublicidad.getRutaRecurso()));
                                        } else {
                                            if (timeViewListener != null) {
                                                timeViewListener.otraPantalla(respuestaPublicidad);
                                            }
                                        }
                                    }
                                }
                            });
                    break;
            }
            if (respuestaPublicidad.getB().getTiempo() > 0) {
                if (respuestaPublicidad.getB().getCerrado() == 3) {
                    imbClose.setVisibility(View.VISIBLE);
                } else {
                    tiempoBoton(respuestaPublicidad.getB().getTiempo(), respuestaPublicidad.getB().getCerrado());
                }

            }
        }
    }

    public void cargarVideo(Uri uri) {
        try {
            videoViewPublicidad.setVideoURI(uri);
            videoViewPublicidad.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer arg0) {
                    videoViewPublicidad.requestFocus();
                    videoViewPublicidad.start();
                    arg0.setLooping(true);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!isShow) {
            show();
            videoViewPublicidad.setVisibility(View.VISIBLE);
        }

    }


    public void cargarImagen(String url) {
        imgPublicidad.setVisibility(View.VISIBLE);
        new Utilidades().cargarImagenPublicitaria(imgPublicidad, url, getContext(), null);
        if (!isShow) {
            show();
        }
    }

    public void cargarGIF(Uri uri) {
        gifImageView.setGifImageUri(uri);
        gifImageView.setVisibility(View.VISIBLE);
        if (!isShow) {
            show();
        }

    }

    public void tiempoBoton(int timeTotal, final int cerrado) {
        CountDownTimer countDownTimer = new CountDownTimer(timeTotal * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                switch (cerrado) {
                    case VariablesPublicidad.CERRADO_BOTON_AL_CUMPLIR_TIEMPO:
                        imbClose.setVisibility(View.VISIBLE);
                        break;
                    case VariablesPublicidad.CERRADO_AL_CUMPLIR_TIEMPO:
                        hide();
                        break;
                }
            }
        };
        countDownTimer.start();
    }


}
