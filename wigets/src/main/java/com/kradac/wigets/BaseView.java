package com.kradac.wigets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public abstract class BaseView extends RelativeLayout {

    protected boolean isShow;
    private ResizeListener resizeListener;

    public ResizeListener getResizeListener() {
        return resizeListener;
    }

    public void setResizeListener(ResizeListener resizeListener) {
        this.resizeListener = resizeListener;
    }

    public interface ResizeListener{
        void  onResise(int expandOrCollapse);
        void  onResiseStart();
    }

    public BaseView(Context context) {
        super(context);
    }

    public BaseView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BaseView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BaseView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    protected void expand(final View v) {
        TranslateAnimation anim;
        anim = new TranslateAnimation(0.0f, 0.0f, v.getHeight(), 0.0f);
        v.setVisibility(View.VISIBLE);
        anim.setDuration(300);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(resizeListener!=null){
                    resizeListener.onResise(1);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        anim.setInterpolator(new AccelerateInterpolator(0.5f));
        v.startAnimation(anim);
    }

    protected void collapse(final View v) {
        TranslateAnimation anim;
        anim = new TranslateAnimation(0.0f, 0.0f, 0.0f, v.getHeight());
        Animation.AnimationListener collapselistener = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if(resizeListener!=null){
                    resizeListener.onResiseStart();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

                if(resizeListener!=null){
                    resizeListener.onResise(animation.getRepeatCount());
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setVisibility(View.GONE);
                if(resizeListener!=null){
                    resizeListener.onResise(2);
                }

            }
        };

        anim.setAnimationListener(collapselistener);

        anim.setDuration(400);
        anim.setInterpolator(new AccelerateInterpolator(0.5f));
        v.startAnimation(anim);
    }

    public void show(){
        isShow=true;
        expand(this);
        Log.e("SHOW ENTRO", "show: "  );
    }
    public void hide(){
        isShow=false;
        collapse(this);
    }

    public boolean isShow() {
        return isShow;
    }


    protected void animarClickView(View view){
        view.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.clic_anim));

    }

}
