package com.kradac.wigets.presentador;

import android.util.Log;

import com.kradac.wigets.api.ApiPublicidad;
import com.kradac.wigets.extra.Utilidades;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PublicidadPresenter extends Presenter {

    private static final String TAG = PublicidadPresenter.class.getName();

    public interface OnComunicacionDescarPublicidad {
        void respuesta(String ruta);
    }

    public void dowloadArchivo(String url, final int tipo, final OnComunicacionDescarPublicidad onComunicacionDescarPublicidad) {
        ApiPublicidad.OnComponentesPublicitarios service = retrofit.create(ApiPublicidad.OnComponentesPublicitarios.class);
        Call<ResponseBody> call = service.downloadFileWithDynamicUrlSync(url);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    onComunicacionDescarPublicidad.respuesta(new Utilidades().writeResponseBodyToDisk(response.body(), tipo));
                } else {
                    onComunicacionDescarPublicidad.respuesta(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                onComunicacionDescarPublicidad.respuesta(null);
            }
        });
    }
}
