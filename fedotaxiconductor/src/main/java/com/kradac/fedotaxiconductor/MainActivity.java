package com.kradac.fedotaxiconductor;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.kradac.conductor.extras.VariablesGlobales;
import com.kradac.conductor.vista.Splash;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        VariablesGlobales.setNumIdAplicativo(5);
        finish();
        startActivity(new Intent(this, Splash.class));
    }
}
